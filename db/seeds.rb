# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Create a User GOD
ActiveRecord::Base.transaction do

	god = UsersModule::V1::User.new(
	  phone_number: '1111111111',
	  first_name: 'God',
	  last_name: 'Admin',
	  referral_code: 'GOD123',
    referral_limit: 5
	)
	god.provider = UsersModule::V1::Provider.new
  god.profile = UsersModule::V1::Profile.new
	god.provider.message = UsersModule::V1::ProvidersModule::V1::Message.new
	password = UsersModule::V1::PasswordHelper.create_hash('password')
	god.provider.generic = UsersModule::V1::ProvidersModule::V1::Generic.new(password: password)

	role = UsersModule::V1::Role.create(role_name: 'God Role', permissions: [
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50])

	god.roles << role
	email = UsersModule::V1::Email.new(
		email_id: 'god@imli.com',
		account_type: UsersModule::V1::AccountType::EMPLOYEE
	)
	email.verified = true
	god.emails << email

	employee = UsersModule::V1::Employee.new(
		team: 'God Team',
		designation: 'Bhagwan'
	)
	god.employee = employee
	god.save!

  # Activate User
  god.otp_verified!		# Inactive -> pending
  god.activate!				# Pending -> Active
  # Approve employee
  e = god.employee
  e.created!					# Inactive -> Pending
  e.approved!					# Pending -> Active

  god.save!

  # Initialize a dummy Benefit
  benefit = CommonModule::V1::Benefit.new({
    name: 'GOD_BENEFIT',
    details: 'Benefit will be applied after shipping',
    condition: 'IMLI_BANK',
    reward_value: 25,
    reward_type: 'PERCENTAGE'
    })
  benefit.save!
  benefit.activate!

  # Initialize a trial plan
  plan = CommonModule::V1::Plan.new({
    name: 'Ample Sample',
    plan_key: 1,
    details: 'Free 90 days membership',
    duration: 90,
    validity_label: '90 Days',
    fee: 0.0
    })
  plan.save!
  plan.activate!

  # This is to populate dummy company, brand, sub brand, product, brand pack, distributor,
  # inventory, inventory brand pack, cash and carry, warehouse & warehouse brand pack
  company = SupplyChainModule::V1::Company.new(
  	name: 'God Company',
    initials: 'GCC',
    account_details: '1234567890'
  )
  company.save!

  brand = MasterProductModule::V1::Brand.new(
  	name: 'God Brand',
    initials: 'BGC'
  )
  brand.company = company
  brand.save!

  sub_brand = MasterProductModule::V1::SubBrand.new(
  	name: 'God Sub brand',
    initials: 'SBGC'
  )
  sub_brand.brand = brand
  sub_brand.save!

  variant = MasterProductModule::V1::Variant.new(
    name: 'God initials',
    initials: 'VGI'
  )
  variant.save!

  product = MasterProductModule::V1::Product.new(
  	name: 'God Product',
    initials: 'PGC'
  )
  product.sub_brand = sub_brand
  product.variant = variant
  product.save!

  department = CategorizationModule::V1::Department.new(
    label: 'God Department'
  )
  department.save!

  category = CategorizationModule::V1::Category.new(
    label: 'God Category',
    department: department
  )
  category.save!

  sub_category = CategorizationModule::V1::Category.new(
    label: 'God Sub Category',
    parent_category: category
  )
  sub_category.save!

  brand_pack = MasterProductModule::V1::BrandPack.new(
  	sku: 'God Brand Pack',
    sub_category: sub_category
  )
  brand_pack.product = product
  brand_pack.save!

  distributor = SupplyChainModule::V1::Distributor.new(
  	name: 'God Distributor',
  )
  distributor.save!

  inventory = SupplyChainModule::V1::Inventory.new(
  	name: 'God Inventory'
  )
  inventory.distributor = distributor
  inventory.save!

  inventory_brand_pack = InventoryProductModule::V1::InventoryBrandPack.new()
  inventory_pricing = InventoryPricingModule::V1::InventoryPricing.new()
  inventory_brand_pack.inventory_pricing = inventory_pricing
  inventory_brand_pack.inventory = inventory
  inventory_brand_pack.brand_pack = brand_pack
  inventory_brand_pack.save!

  cash_and_carry = SupplyChainModule::V1::CashAndCarry.new(
  	name: 'God cash and carry'
  )
  cash_and_carry.save!

  warehouse = SupplyChainModule::V1::Warehouse.new(
  	name: 'God Warehouse'
  )
  warehouse.cash_and_carry = cash_and_carry
  warehouse.save!

  warehouse_brand_pack = WarehouseProductModule::V1::WarehouseBrandPack.new()
  warehouse_pricing = WarehousePricingModule::V1::WarehousePricing.new()
  warehouse_brand_pack.warehouse_pricing = warehouse_pricing
  warehouse_brand_pack.warehouse = warehouse
  warehouse_brand_pack.inventory_brand_packs << inventory_brand_pack
  warehouse_brand_pack.brand_pack = brand_pack
  warehouse_brand_pack.save!

  seller = SupplyChainModule::V1::Seller.new(
    name: 'God Seller'
  )
  seller.save!

  seller_brand_pack = SellerProductModule::V1::SellerBrandPack.new()
  seller_pricing = SellerPricingModule::V1::SellerPricing.new()
  seller_brand_pack.seller_pricing = seller_pricing
  seller_brand_pack.seller = seller
  seller_brand_pack.warehouse_brand_packs << warehouse_brand_pack
  seller_brand_pack.brand_pack = brand_pack
  seller_brand_pack.save!


  area_service = AddressModule::V1::AreaService.new
  request = {
    area: 'God Area',
    pincode: '101010',
    city: 'God City',
    state: 'God State',
    country: 'God Country'
  }
  area_service.create_area(request)
  
  # marketplace_brand_pack = MarketplaceProductModule::V1::MarketplaceBrandPack.new(
  #   # selling_price: "10",
  #   # mrp: "20",
  #   # discount: "10"
  # )
  # marketplace_brand_pack.brand_pack = brand_pack
  # marketplace_brand_pack.seller_brand_packs << seller_brand_pack
  # marketplace_brand_pack.save!

  # mpsp = MarketplaceProductModule::V1::MarketplaceSellingPack.new(
  #   # selling_price: "100",
  #   # mrp: "200",
  #   # discount: "20",
  #   # max_quantity: "100"
  #   )
  # mpsp.marketplace_brand_packs << marketplace_brand_pack
  # mpsp.save!
end

# This migration comes from supply_chain_engine_engine
#
# creating table marketplace_brand_pack_seller_brand_pack
#
class CreateMarketplaceBrandPackSellerBrandPack < ActiveRecord::Migration
	#
	# create table marketplace_brand_pack_seller_brand_pack
	# Through table for MPBP & MPSP
	#
  def up
    create_table :marketplace_brand_pack_seller_brand_packs do |t|
    	t.belongs_to :seller_brand_pack
      t.belongs_to :marketplace_brand_pack
      
      t.timestamps null: false
    end

    add_index :marketplace_brand_pack_seller_brand_packs, [:seller_brand_pack_id], name: :idx_through_sbp
    add_index :marketplace_brand_pack_seller_brand_packs, [:marketplace_brand_pack_id], name: :idx_through_mpbp_sbp
  end

  #
  # drop table marketplace_brand_pack_seller_brand_pack
  #
  def down
  	drop_table :marketplace_brand_pack_seller_brand_pack
  end
end

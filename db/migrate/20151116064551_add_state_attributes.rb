#
# Migration to add state_attributes column to orders table
#
class AddStateAttributes < ActiveRecord::Migration
  #
  # Function to add state_attributes column in orders Table
  #
  def up
    add_column :orders, :state_attributes, :string, default: ''
  end

  #
  # Function to remove state_attributes column from orders Table
  #
  def down
    remove_column :orders, :state_attributes
  end
end

class AddVendorCodeToDistributor < ActiveRecord::Migration
  #
  # Function to add vendor code in distributor table
  #
  def up
    add_column :distributors, :vendor_code, :integer
  end

  #
  # Function to roll back migration
  #
  def down
    remove_column :distributors, :vendor_code
  end
end

class AddIsInterStateToInventoryOrder < ActiveRecord::Migration

  #
  # Function to add is_inter_state column to inventory order
  #
  def up
    add_column :inventory_orders, :is_inter_state, :boolean
  end

  #
  # Function to roll back the migration
  #
  def down
    remove_column :inventory_orders, :is_inter_state
  end
end

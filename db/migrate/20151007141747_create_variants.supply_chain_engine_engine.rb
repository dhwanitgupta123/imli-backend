# This migration comes from supply_chain_engine_engine (originally 20151007135031)
#
# creating table variants
#
class CreateVariants < ActiveRecord::Migration
  #
  # variants table
  #
  # name: name of variant
  # initials: initials of variant
  #
  def up
    create_table :variants do |t|
      t.string :name
      t.string :initials

      t.timestamps null: false
    end
  end

  #
  # delete table variants
  #
  def down
    drop_table :variants
  end
end

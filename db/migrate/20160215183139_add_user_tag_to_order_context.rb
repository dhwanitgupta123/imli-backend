class AddUserTagToOrderContext < ActiveRecord::Migration
  #
  # up: add :xxxxhdpi_large style in images
  #     and reproccessing others as quality change from 90 -> 80
  #
  def up
    order_contexts = MarketplaceOrdersModule::V1::OrderContext.all
    return if order_contexts.blank?

    order_contexts.each do |order_context|
      order_context.order_user_tag = MarketplaceOrdersModule::V1::OrderUserTags.get_id_by_order_user_tag('CUSTOMER')
      order_context.save
    end
  end

  def down
    order_contexts = MarketplaceOrdersModule::V1::OrderContext.all
    return if order_contexts.blank?

    order_contexts.each do |order_context|
      order_context.order_user_tag = nil
      order_context.save
    end 
  end
end

#
# Class to add pricing related column to order_products
#
class AddPricingToOrderProducts < ActiveRecord::Migration
  # 
  # Function to add pricing related column to order_products
  #
  def up
    add_column :order_products, :mrp, :decimal, default: 0
    add_column :order_products, :selling_price, :decimal, default: 0
    add_column :order_products, :savings, :decimal, default: 0
    add_column :order_products, :vat, :decimal, default: 0
    add_column :order_products, :service_tax, :decimal, default: 0
    add_column :order_products, :taxes, :decimal, default: 0
    add_column :order_products, :discount, :decimal, default: 0
    order_products = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProduct.all
    marketplace_selling_pack_dao = MarketplaceProductModule::V1::MarketplaceSellingPackDao.new
    if order_products.present?
      order_products.each do |order_product|
        pricing = marketplace_selling_pack_dao.get_savings_and_pricing_for_mpsp(order_product.quantity, order_product.marketplace_selling_pack)
        order_product.mrp = pricing[:mrp]
        order_product.selling_price = pricing[:selling_price]
        order_product.savings = pricing[:savings]
        order_product.vat = pricing[:vat]
        order_product.service_tax = pricing[:service_tax]
        order_product.taxes = pricing[:taxes]
        order_product.discount = pricing[:discount]
        order_product.save!
      end
    end
  end

  #
  # Function to roll back migration
  #
  def down
    remove_column :order_products, :mrp
    remove_column :order_products, :selling_price
    remove_column :order_products, :savings
    remove_column :order_products, :vat
    remove_column :order_products, :service_tax
    remove_column :order_products, :taxes
    remove_column :order_products, :discount
  end
end
  

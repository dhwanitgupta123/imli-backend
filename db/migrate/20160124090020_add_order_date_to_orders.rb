#
# Migration to add order_date column to Orders table
#
class AddOrderDateToOrders < ActiveRecord::Migration

  ORDER_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
  ORDER_MODEL = MarketplaceOrdersModule::V1::Order

  #
  # Function to add order_date column to Orders
  #
  def up
    add_column :orders, :order_date, :datetime

    # For all previous exsisting orders, fetch their order date from
    # state_attributes OR created_date and then populate order_date
    all_orders = ORDER_MODEL.all
    all_orders.each do |order|
      order_date = get_order_date(order)
      order.order_date = DateTime.strptime(order_date.to_s, '%s')
      order.save!
    end
  end

  #
  # Function to roll back the migration
  #
  def down
    remove_column :orders, :order_date
  end

  #
  # Get date of ORDER
  # Fetch its placed order time. IF not present, then return CREATED time.
  #
  # @param order [Object] [Order model object fro which date is to be computed]
  #
  # @return [EpochTime] [Time of order in epoch standard]
  #
  def get_order_date(order)
    return nil if order.blank?
    # Fetch order placed date with the fallback on created_at date
    state_attributes_hash = get_state_attributes_hash(order.state_attributes)
    if state_attributes_hash.present?
      state_hash = state_attributes_hash[ORDER_STATES::PLACED[:value].to_s]
      return state_hash['time'] if (state_hash.present? && state_hash['time'].present?)
    end
    return order.created_at.to_i
  end

  #
  # Get state attributes hash from state_attributes string
  # This is a n-level MAP traversal, which returns below subtree
  # (if its a hash) or string if it can't be converted to a proper JSON
  #
  # @param state_attributes_string [String] [string of state attributes]
  #
  # @return [JSON] [Hash made from passed string]
  #
  def get_state_attributes_hash(state_attributes_string)
    return {} if state_attributes_string.blank?
    begin
      state_attributes_hash = JSON.parse(state_attributes_string)
      attr_hash = {}
      state_attributes_hash.each do |key, value|
        attr_value = get_state_attributes_hash(value)
        attr_hash[key.to_s] = attr_value
      end
      return attr_hash
    rescue
      return state_attributes_string
    end
  end
end

class ReproccessImages < ActiveRecord::Migration
  #
  # up: add :xxxxhdpi_large style in images
  #     and reproccessing others as quality change from 90 -> 80
  #
  def up
    if Rails.env == 'production'
      images = ImageServiceModule::V1::Image.all
      images.each do |image|
        image.image_s3.reprocess! :xxxxhdpi_large
        image.image_s3.reprocess! :xxxhdpi_large
        image.image_s3.reprocess! :xxxhdpi_medium
        image.image_s3.reprocess! :xxhdpi_large
        image.image_s3.reprocess! :xhdpi_large
        image.image_s3.reprocess! :xhdpi_medium
        image.image_s3.reprocess! :hdpi_large
      end
    end
  end
end

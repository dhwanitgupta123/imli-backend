class AddCancelledInwardStateToStock < ActiveRecord::Migration
  #
  # Migration to crrect the data for inward of cancelled orders
  #
  def up
    order_state = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
    order_stock_helper = MarketplaceOrdersModule::V1::OrderStockingHelper
    order_mapper = MarketplaceOrdersModule::V1::OrderMapper
    wbp_service = WarehouseProductModule::V1::WarehouseBrandPackService
    cart_service = MarketplaceOrdersModule::V1::CartService
    warehouse_stock_service = WarehouseProductModule::V1::WarehouseStockService
    cancelled_order_states = [order_state::ORDER_CANCELLED[:value], order_state::CANCELLED_BY_ADMIN[:value]]

    cancelled_orders = MarketplaceOrdersModule::V1::Order.where(status: cancelled_order_states)

    warehouse_brand_pack_service = wbp_service.new
    cart_service = cart_service.new
    warehouse_stock_service = warehouse_stock_service.new

    cancelled_orders.each do |order|
      order_stock_helper.inward_warehouse_brand_packs(order.cart)
      active_order_products = cart_service.get_active_order_products_in_cart(order.cart)
      brand_packs_by_quantity = order_mapper.map_products_to_array(active_order_products)
      if brand_packs_by_quantity.present?
        warehouse = brand_packs_by_quantity.first[:brand_pack].warehouse_brand_packs.first.warehouse
        brand_packs_by_quantity.each do |param|
          warehouse_brand_pack = warehouse_brand_pack_service.get_wbp_by_parameters(param[:brand_pack], warehouse)
          pricing = warehouse_brand_pack.warehouse_pricings.first
          pricing.warehouse_stocks << warehouse_stock_service.inward_warehouse_brand_pack(-param[:units_to_procure])
        end
      end
    end
  end
end

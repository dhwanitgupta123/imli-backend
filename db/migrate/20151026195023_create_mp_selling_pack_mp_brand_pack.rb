# This migration comes from supply_chain_engine_engine
#
# creating table marketplace_selling_pack_marketplace_brand_packs
#
class CreateMpSellingPackMpBrandPack < ActiveRecord::Migration
	#
	# create table marketplace_selling_pack_marketplace_brand_packs
	# Through table for MPBP & MPSP
	#
  def up
    create_table :marketplace_selling_pack_marketplace_brand_packs do |t|
    	t.belongs_to :marketplace_selling_pack
      t.belongs_to :marketplace_brand_pack
      t.integer :quantity, default: 0
      
      t.timestamps null: false
    end

    add_index :marketplace_selling_pack_marketplace_brand_packs, [:marketplace_selling_pack_id], name: :idx_through_mpsp
    add_index :marketplace_selling_pack_marketplace_brand_packs, [:marketplace_brand_pack_id], name: :idx_through_mpbp
  end

  #
  # drop table marketplace_selling_pack_marketplace_brand_packs
  #
  def down
  	drop_table :marketplace_selling_pack_marketplace_brand_packs
  end
end

# This migration comes from supply_chain_engine_engine
#
# create table sellers
#
class CreateSellers < ActiveRecord::Migration
  #
  # table sellers
  #  
  # name: seller name
  # 
  def up
    create_table :sellers do |t|
      t.string :name
      t.integer :status

      t.timestamps null: false
    end
  end

  # 
  # delete table sellers
  # 
  def down
    drop_table :sellers
  end
end
#
# Migration to add benefit_limit column to users table
#
class AddBenefitLimitInUsers < ActiveRecord::Migration

  CACHE = CommonModule::V1::Cache
  #
  # Function to add benefit_limit column in users Table
  #
  def up
    add_column :users, :benefit_limit, :integer, default: 0

    # populating benefit limit to pre defined value 
    UsersModule::V1::User.find_each do |user|
      user.benefit_limit = CACHE.read('BENEFIT_LIMIT')
      user.save!
    end
  end
  
  #
  # Function to roll back the migration
  #
  def down
    remove_column :users, :benefit_limit
  end
end

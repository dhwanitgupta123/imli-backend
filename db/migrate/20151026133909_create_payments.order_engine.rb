# This migration comes from order_engine
#
# create table payments
#
class CreatePayments < ActiveRecord::Migration
  #
  # create table payments
  #
  # mode: ship_by date
  # grand_total: grand total is total without the discount
  # total_vat_tax: total VAT tax levied
  # total_service_tax: total Service tax levied
  # payment_mode_savings: total savings user got due to the choice of payment mode
  # discount: discount which the user got on the whole transcation
  # gift_card: Gift card, if used by the user while payment
  # total_savings: Total savings on the order which the user made
  # net_total: Net total which the user needs to pay for the order
  # status: status of the payment
  # 
  def up
    create_table :payments do |t|
      t.integer :mode
      t.decimal :grand_total, default: 0
      t.decimal :total_vat_tax, default: 0
      t.decimal :total_service_tax, default: 0
      t.decimal :payment_mode_savings, default: 0
      t.decimal :discount, default: 0
      t.decimal :gift_card, default: 0
      t.decimal :total_savings, default: 0
      t.decimal :net_total, default: 0
      t.integer :status

      t.references :order, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  # 
  # delete payments table
  # 
  def down
    drop_table :payments
  end
end
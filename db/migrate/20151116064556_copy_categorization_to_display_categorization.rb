#
# Migration to add images column to marketplace selling pack table
#
class CopyCategorizationToDisplayCategorization < ActiveRecord::Migration
  #
  # Function to add images column in marketplace selling pack Table
  #
  def up
    departments = CategorizationModule::V1::Department.all
    if departments.present?
      departments.each do |department|
        display_department = CategorizationModule::V1::MpspDepartment.new
        display_department.label = department.label
        display_department.status = department.status
        display_department.priority = department.priority
        display_department.description = department.description
        display_department.images = department.images
        display_department.save
        update_display_categories(department.categories, display_department)
      end
    end
  end

  def update_display_categories(categories, display_department)
    if categories.present?
      categories.each do |category|
        display_category = CategorizationModule::V1::MpspCategory.new
        display_category.label = category.label
        display_category.priority = category.priority
        display_category.mpsp_department = display_department
        display_category.status = category.status
        display_category.save
        update_display_sub_categories(category.sub_categories, display_category)
        display_department.mpsp_categories <<  display_category
      end
      display_department.save
    end
  end
  
  def update_display_sub_categories(sub_categories, display_category)
    if sub_categories.present?
      sub_categories.each do |sub_category|
        display_sub_category = CategorizationModule::V1::MpspCategory.new
        display_sub_category.label = sub_category.label
        display_sub_category.priority = sub_category.priority
        display_sub_category.mpsp_parent_category = display_category
        display_sub_category.status = sub_category.status
        display_sub_category.save
        add_mpsp_in_mpsp_sub_categories(sub_category.brand_packs, display_sub_category)
        display_category.mpsp_sub_categories << display_sub_category
      end
      display_category.save
    end
  end

  def add_mpsp_in_mpsp_sub_categories(brand_packs, display_sub_category)
    if brand_packs.present?
      brand_packs.each do |brand_pack|
        if brand_pack.marketplace_brand_packs.present? && brand_pack.marketplace_brand_packs[0].marketplace_selling_packs.present?
          mpsp = brand_pack.marketplace_brand_packs[0].marketplace_selling_packs[0]
          mpsp.mpsp_sub_category = display_sub_category
          mpsp.save
        end
      end
    end
  end
  #
  # Function to roll back the migration
  #
  def down
    mpsps = MarketplaceProductModule::V1::MarketplaceSellingPack.all
    if mpsps.present?
      mpsps.each do |mpsp|
        mpsp.mpsp_sub_category = nil
        mpsp.save
      end
    end
    CategorizationModule::V1::MpspCategory.delete_all
    CategorizationModule::V1::MpspDepartment.delete_all
  end
end

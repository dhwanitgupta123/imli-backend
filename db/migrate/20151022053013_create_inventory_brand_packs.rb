class CreateInventoryBrandPacks < ActiveRecord::Migration
  
  def up
    create_table :inventory_brand_packs do |t|
      t.integer :status

      t.references :inventory, index: true, foreign_key: true
      t.references :brand_pack, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  def down
    drop_table :inventory_brand_packs
  end
end

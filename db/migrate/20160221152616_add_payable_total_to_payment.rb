class AddPayableTotalToPayment < ActiveRecord::Migration
  #
  # Function to add payable_total to Payment
  #
  def up
    add_column :payments, :payable_total, :decimal, default: BigDecimal.new('0')
    add_column :payments, :payment_type, :integer

    all_payments = MarketplaceOrdersModule::V1::Payment.all
    all_payments.each do |payment|
      payment.payable_total = payment.net_total
      payment.payment_type = 1
      payment.save!
    end
  end

  #
  # Function to roll back migration
  #
  def down
    remove_column :payments, :payable_total
    remove_column :payments, :payment_type
  end
end

class AddPaymentInstanceToPayments < ActiveRecord::Migration
  def up
    # This column represent if payment done before order place (online mode of payment)
    # or after order place (cash on delivery, card on delivery or via links)
    #
    # therefore two values possible = 1 or 2 
    # where 1 represents PRE_ORDER_PAYMENT and 2 represents POST_ORDER_PAYMENT
    #
    add_column :payments, :payment_instance, :integer

    populate_payment_instance
  end

  # 
  # Currently we are saving payment.mode = 1 (DEBIT_CARD) for online payment via APP/Web
  # and other available methods are post_order
  # 
  # hence assigning payment_instance to PRE_ORDER if payment.mode = 1 else POST_ORDER
  #
  def populate_payment_instance
    payments = MarketplaceOrdersModule::V1::Payment.all

    return if payments.blank?

    pre_order_instance = MarketplaceOrdersModule::V1::PaymentInstances.get_id_by_instance('PRE_ORDER')
    post_order_instance = MarketplaceOrdersModule::V1::PaymentInstances.get_id_by_instance('POST_ORDER')

    payments.each do |payment|
      mode = payment.mode
      # 1 DEBIT_CARD
      if payment.mode == 1
        payment.payment_instance = pre_order_instance
        payment.mode = MarketplaceOrdersModule::V1::PaymentModes::UNKNOWN
      else
        payment.payment_instance = post_order_instance
        # 6 = EMAIL_LINK_CITRUS, 7 = EMAIL_LINK_RAZOR_PAY
        if payment.mode == 6 || payment.mode == 7
          payment.mode = MarketplaceOrdersModule::V1::PaymentModes::LINK
        end 
      end
      payment.save!
      update_transaction(payment, mode)
    end
  end

  # 
  # Updating gateway field in transactions table
  #
  def update_transaction(payment, mode)
    transactions = TransactionsModule::V1::Transaction.where(payment_id: payment.id)
    return if transactions.count == 0

    transactions.each do |transaction|

      transaction_data = transaction.transaction_data

      if transaction.signature.present?
        transaction.gateway = TransactionsModule::V1::GatewayType::CITRUS_WEB
      elsif transaction_data.present? && transaction_data['gateway'] == '3'
        if mode == 6
          transaction.gateway = TransactionsModule::V1::GatewayType::CITRUS_WEB
        elsif mode == 7
          transaction.gateway = TransactionsModule::V1::GatewayType::RAZORPAY
        elsif mode == 5
          transaction.gateway = TransactionsModule::V1::GatewayType::CASH
        else
          transaction.gateway = TransactionsModule::V1::GatewayType::M_SWIPE
        end
      elsif transaction_data.present? && transaction_data['gateway'] == 2
        transaction.gateway = TransactionsModule::V1::GatewayType::RAZORPAY 
      else
        # Considering all failed transaction with signature and transaction data nil  via RAZORPAY
        transaction.gateway = TransactionsModule::V1::GatewayType::RAZORPAY
      end

      #### Updating key payment_id to reference_id for post_order
      # replacing transaction_data['cod']['payment_id'] -> transaction_data['cod']['reference_id']
      # replacing transaction_data['cod'] -> transaction_data['post_order']
      #
      if transaction.transaction_data.present? && transaction.transaction_data["cod"].present?
        cod_hash = transaction.transaction_data["cod"]
        cod_hash["reference_id"] = cod_hash["payment_id"] if cod_hash["payment_id"].present?
        cod_hash.except!("payment_id")
        transaction_data = transaction.transaction_data
        transaction_data.except!("cod")
        transaction_data["post_order"] = cod_hash
        transaction.transaction_data = transaction_data
      end
      transaction.save!
    end
  end

  def down
    remove_column :payments, :payment_instance

    transactions = TransactionsModule::V1::Transaction.all

    transactions.each do |transaction|
      if transaction.transaction_data.present? && transaction.transaction_data["post_order"].present?
        post_order_hash = transaction.transaction_data["post_order"]
        post_order_hash["payment_id"] = post_order_hash["reference_id"] if post_order_hash["reference_id"].present?
        post_order_hash.except!("reference_id")

        transaction_data = transaction.transaction_data
        transaction_data["cod"] = post_order_hash
        transaction_data.except!("post_order")
        transaction.transaction_data = transaction_data

        transaction.save
      end
    end
  end
end

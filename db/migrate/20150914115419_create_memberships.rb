class CreateMemberships < ActiveRecord::Migration
  #
  # up: Create table for model Membership
  #
  # starts_at [DateTime] to store starting date
  # expires_at [DateTime] to store the expiry date/end date
  # workflow_state [Integer] to store the current state of membership
  #   (check model for workflow details)
  # plan [Integer] to store foreign key to Plan table
  #
  def up
    create_table :memberships do |t|
      t.datetime :starts_at
      t.datetime :expires_at
      t.integer :workflow_state
      t.references :plan, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  #
  # down: Drop table Memberships
  # Used for db rollback
  #
  def down
    drop_table :memberships
  end
end

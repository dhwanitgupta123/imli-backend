class CreateDescriptions < ActiveRecord::Migration
  
  #
  # up: creating desctiptions table
  # 
  # heading: String title of description
  # parent: parent description of the current description
  # data: Array of string containg data about the description
  # 
  def up

    remove_column :marketplace_selling_packs, :description

    create_table :descriptions do |t|

      t.references :parent, index: true

      t.references :description_of, polymorphic: true, index: true

      t.string :heading
      t.string :data, array: true, default: []

      t.timestamps null: false
    end

  end

  #
  # down: Drop descriptions table
  #
  def down
    drop_table :descriptions
    add_column :marketplace_selling_packs, :description, :text
  end
end

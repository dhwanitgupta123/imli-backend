class CreateInventories < ActiveRecord::Migration
  
  #
  # create inventories table
  #
  # name: name of inventory of distributor
  #
  def up
    create_table :inventories do |t|
      t.string :name
      t.integer :status

      t.references :distributor, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  #
  # delete inventories table
  #
  def down
  	drop_table :inventories
  end
end

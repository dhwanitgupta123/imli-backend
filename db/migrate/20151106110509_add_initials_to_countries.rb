#
# Migration to add a coloumn to countries table 
#
class AddInitialsToCountries < ActiveRecord::Migration
  #
  # Function to add initials column in countries Table
  #
  def up
    add_column :countries, :initials, :string
  end

  #
  # Function to roll back the migration
  #
  def down
    remove_column :countries, :initials
  end
end

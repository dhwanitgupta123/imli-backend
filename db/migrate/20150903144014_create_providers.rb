class CreateProviders < ActiveRecord::Migration
  
  # 
  # [up description]
  # 
  # create table providers to store provider reference
  # 
  # session (string) to store session token
  # 
  # 
  def up
    create_table :providers do |t|
      t.references :user, index: true, foreign_key: true
      t.string :session_token
      t.timestamps null: false
    end
  end

  def down
    drop_table :providers
  end
end

# This migration comes from supply_chain_engine_engine
#
# creating table seller_brand_packs
#
class CreateSellerBrandPacks < ActiveRecord::Migration
  #
  # seller_brand_packs table
  #
  def up
    create_table :seller_brand_packs do |t|
      t.integer :status
      t.references :seller, index: true, foreign_key: true
      t.references :brand_pack, index: true, foreign_key: true
      
      t.timestamps null: false

      #TODO t.references :marketplace_brand_pack, index: true, foreign_key: true
    end
  end

  #
  # delete table seller_brand_packs
  #
  def down
    drop_table :seller_brand_packs
  end
end

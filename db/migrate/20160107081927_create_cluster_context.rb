class CreateClusterContext < ActiveRecord::Migration
  def up
    # 
    # up: create table cluster_contexts
    # 
    # level_id: [integer] define the level identifier of cluster level
    # level: [integer] level of cluster
    # cluster: [reference] reference to clusters table
    # 
    create_table :cluster_contexts do |t|
      t.integer :level_id
      t.integer :level

      t.references :cluster, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  #
  # drop cluster_contexts table
  # 
  def down
    drop_table :cluster_contexts
  end
end

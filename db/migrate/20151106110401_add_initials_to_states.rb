#
# Migration to add a coloumn to states table 
#
class AddInitialsToStates < ActiveRecord::Migration
  #
  # Function to add initials column in states Table
  #
  def up
    add_column :states, :initials, :string
  end

  #
  # Function to roll back the migration
  #
  def down
    remove_column :states, :initials
  end
end

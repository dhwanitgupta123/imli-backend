class CreatePackagingBox < ActiveRecord::Migration
  #
  # up: create tables packaging boxes
  # 
  def up
    create_table :packaging_boxes do |t|
      t.decimal :length, default: 0
      t.decimal :width, default: 0
      t.decimal :height, default: 0
      t.decimal :cost, default: 0
      t.string :label
      t.integer :status

      t.timestamps null: false
    end
  end

  #
  # down: Drop table Packaging Boxes
  # Used for db rollback
  #
  def down
    drop_table :packaging_boxes
  end
end

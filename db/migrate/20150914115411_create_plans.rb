class CreatePlans < ActiveRecord::Migration
  #
  # up: Create table for model Plan
  #
  # name [String] to store the plan name
  # details [String] to store other details of plan
  # duration [Integer] to store its validity in days (ex: 30 days, 365 days, etc.)
  # validity_label [String] to store display label of validity details (ex: One Year validity)
  # fee [Integer] to store fee charged for this plan
  # status [Integer] to store status of the Plan (ex: Active, Inactive, etc.)
  #
  def up
    create_table :plans do |t|
      t.string :name
      t.text :details
      t.integer :duration # In Days
      t.text :validity_label
      t.integer :fee
      t.integer :status

      t.timestamps null: false
    end
  end

  #
  # down: Drop table Plans
  # Used for db rollback
  #
  def down
    drop_table :plans
  end
end

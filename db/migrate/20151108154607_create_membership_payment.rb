# This migration comes from order_engine
#
# create table membership payments
#
class CreateMembershipPayment < ActiveRecord::Migration

  #
  # create table payments
  #
  # mode: ship_by date
  # grand_total: grand total is total membership fees
  # refund : prev membership refund amount
  # billing_amount: grand total - prev membership refund amount
  # total_vat_tax: total VAT tax levied
  # total_service_tax: total Service tax levied
  # payment_mode_savings: total savings user got due to the choice of payment mode
  # discount: discount which the user got on the whole transcation
  # gift_card: Gift card, if used by the user while payment
  # total_savings: Total savings on the order which the user made
  # net_total: Net total which the user needs to pay for the order
  # status: status of the payment
  # 
  def up
    create_table :membership_payments do |t|
      t.integer :mode
      t.decimal :grand_total, default: 0
      t.decimal :billing_amount, default: 0
      t.decimal :refund
      t.decimal :total_vat_tax, default: 0
      t.decimal :total_service_tax, default: 0
      t.decimal :payment_mode_savings, default: 0
      t.decimal :discount, default: 0
      t.decimal :gift_card, default: 0
      t.decimal :total_savings, default: 0
      t.decimal :net_total, default: 0
      t.integer :status

      t.references :membership, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  # 
  # delete membership_payments table
  # 
  def down
    drop_table :membership_payments
  end
end

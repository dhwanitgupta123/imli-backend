#
# Migration to create inventory_order_payments table
#
class CreateInventoryOrderPayment < ActiveRecord::Migration
  #
  # Function to create inventory_order_payment table with following attributes
  # total_mrp: total mrp of order
  # total_cost_price: total cost price of PO
  # cst: cst on PO
  # cst_value: cst value on PO
  # octrai: octrai on PO
  # octrai_value: octrai value on PO
  # total_tax: total tax on PO
  # net_total: net total to be paid for PO
  # 
  def up
    create_table :inventory_order_payments do |t|
      t.decimal :total_mrp, default: 0.0
      t.decimal :total_cost_price, default: 0.0
      t.decimal :total_cst_value, default: 0.0
      t.decimal :total_octrai_value, default: 0.0
      t.decimal :total_vat_value, default: 0.0
      t.decimal :total_tax, default: 0.0
      t.decimal :net_total, default: 0.0
      t.integer :status

      t.references :inventory_order, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  # 
  # delete inventory_order_payments table
  # 
  def down
    drop_table :inventory_order_payments
  end
end
#
# Migration to create inventory order products
#
class CreateInventoryOrderProduct < ActiveRecord::Migration
  #
  # function to create inventory_order_products table with following attributes
  # quantity ordered, integer stores the quntity of a product ordered
  # quantity received, integer, stores the quantity of a product received
  # mrp, decimal, mrp of IBP
  # cost_price, decimal, cost price of IBP
  # Vat, decimal, vat tax 
  # vat value, decimal, value of vat on thet IBP
  # service tax, decimal, service tax on IBP
  # service tax value, decimal service tax value on IBP
  #
  def up
    create_table :inventory_order_products do |t|
      t.integer :quantity_ordered, default: 0
      t.integer :quantity_received
      t.decimal :mrp, default: 0.0
      t.decimal :cost_price, default: 0.0
      t.decimal :vat, default: 0.0
      t.decimal :vat_tax_value, default: 0.0
      t.decimal :service_tax, default: 0.0
      t.decimal :service_tax_value, default: 0.0
      t.decimal :cst, default: 0.0
      t.decimal :cst_value, default: 0.0
      t.decimal :octrai, default: 0.0
      t.decimal :octrai_value, default: 0.0
      t.integer :buying_status

      t.references :inventory_order, index: true, foreign_key: true
      t.references :inventory_brand_pack, index: true, foreign_key: true

      t.timestamps null: false
    end

    #
    # Drop inventory_order_products table
    #
    def down
      drop_table :inventory_order_products
    end
  end
end

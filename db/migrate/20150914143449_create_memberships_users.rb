class CreateMembershipsUsers < ActiveRecord::Migration
  #
  # up: Create table MembershipsUsers (many-to-many relationship)
  #
  # user_id [Integer] reference to User table
  # membership_id [Integer] reference to Membership table
  #
  def up
    create_table :memberships_users do |t|
      t.belongs_to :membership, index: true
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
  end

  #
  # down: Drop table MembershipsUsers
  # Used for db rollback
  #
  def down
    drop_table :memberships_users
  end
end
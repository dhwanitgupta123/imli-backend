#
# Migration to add a coloumn to cities table 
#
class AddInitialsToCities < ActiveRecord::Migration
  #
  # Function to add initials column in cities Table
  #
  def up
    add_column :cities, :initials, :string
  end

  #
  # Function to roll back the migration
  #
  def down
    remove_column :cities, :initials
  end
end

class AddFieldsToImage < ActiveRecord::Migration

  IMAGE_STATES = ImageServiceModule::V1::ModelStates::ImageStates

  #
  # Adding status and resource column
  # and assiging values
  # 
  def up
    add_column :images, :status, :integer
    add_column :images, :resource, :string

    brand_pack = MasterProductModule::V1::BrandPack
    mpsp = MarketplaceProductModule::V1::MarketplaceSellingPack
    mpsp_department = CategorizationModule::V1::MpspDepartment
    department = CategorizationModule::V1::Department

    mark_resource(brand_pack.all, brand_pack.name)
    mark_resource(department.all, department.name)
    mark_resource(mpsp_department.all, mpsp_department.name)
    mark_resource(mpsp.all, mpsp.name)
    
  end

  #
  # Populating resource and status field of existing images
  # 
  def mark_resource(elements, resource_name)

    elements.each do |element|
      image_ids  = element.images
      if image_ids.present?
        image_ids.each do |image_id|
          image = ImageServiceModule::V1::Image.find_by(id: image_id)
          if image.present?
            image.resource = resource_name
            image.status = IMAGE_STATES::ACTIVE
            image.save
          end
        end
      end
    end
  end

  #
  # Deleting columns on rollback
  # 
  def down
    remove_column :images, :status
    remove_column :images, :resource
  end
end

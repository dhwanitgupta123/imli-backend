class CreateMpspDepartments < ActiveRecord::Migration
  # 
  # up create table department
  # 
  # label: [string] used to store name of the department
  # image_url: [string] contains S3 url of the image for the department
  # status: [integer] to store the current state of department
  # 
  def up
    create_table :mpsp_departments do |t|
      t.string :label
      t.integer :status
      t.string :description
      t.string :priority
      t.integer :images, array: true, default: []
      
      t.timestamps null: false
    end
  end

  #
  # down: Drop table Department
  # Used for db rollback
  #
  def down
    drop_table :mpsp_departments
  end
end

class CreateEmployees < ActiveRecord::Migration
  #
  # up: Create table Employees
  #
  # designation [String] to store the employee designation in the company
  # team [String] to store the team name
  # manager_id [Integer] to store its manager employee id (tree structure), Nil in case of root
  # workflow_state [Integer] to store its current state (Active/Pending/.. check model for reference)
  # user_id [Integer] reference to user table
  #
  def up
    create_table :employees do |t|
      t.string :designation
      t.string :team
      t.references :manager, index: true
      t.integer :workflow_state

      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  #
  # down: Drop table Employee
  # Used for db rollback
  #
  def down
    drop_table :employees
  end

end

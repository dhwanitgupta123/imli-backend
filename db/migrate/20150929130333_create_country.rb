class CreateCountry < ActiveRecord::Migration
  # 
  # creates countries table
  # 
  # name: country name
  # 
  def up
    create_table :countries do |t|
      
      t.string :name

      t.timestamps null: false
    end
  end

  # 
  # delete countries table
  # 
  def down
      drop_table :countries
  end
end

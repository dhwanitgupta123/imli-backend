# This migration comes from supply_chain_engine_engine
#
# creating table marketplace_brand_packs
#
class CreateMarketplaceBrandPack < ActiveRecord::Migration
	#
  # create table marketplace_brand_packs
  #
  # status: status of marketplace_brand_packs
  # selling price: selling price of product
  # mrp: mrp of product
  # savings: savings on product
  # discount: discount amount
  # is_on_sale: if product is on sale
  # is_ladded_pricing_active: if ladder pricing is active on the product
  #
  def change
    create_table :marketplace_brand_packs do |t|
    	t.integer :status
    	t.references :brand_pack, index: true, foreign_key: true
    	t.boolean :is_on_sale, :default  => false
    	t.boolean :is_ladder_pricing_active, :default  => false

    	t.timestamps null: false
    end
  end

  #
  # delete marketplace_brand_packs table
  #
  def down
    drop_table :marketplace_brand_packs
  end
end

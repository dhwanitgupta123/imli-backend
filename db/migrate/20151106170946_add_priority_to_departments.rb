#
# Class to add priority column to departments
#
class AddPriorityToDepartments < ActiveRecord::Migration
  # 
  # Function to add priority column to departments table
  #
  def up
    add_column :departments, :priority, :integer
  end

  #
  # Function to roll back migration
  #
  def down
    remove_column :departments, :priority
  end
end

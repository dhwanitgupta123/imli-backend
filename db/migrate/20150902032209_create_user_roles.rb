class CreateUserRoles < ActiveRecord::Migration
  #
  # up: Create table UserRoles
  #
  # user_id [Integer] reference to User table
  # role_id [Integer] reference to Role table
  #
  def up
    create_table :user_roles do |t|
      t.belongs_to :user, index: true
      t.belongs_to :role, index: true

      t.timestamps null: false
    end
  end

  #
  # down: Drop table UserRoles
  # Used for db rollback
  #
  def down
    drop_table :user_roles
  end
end
class CreateAddresses < ActiveRecord::Migration
  # 
  # [up description]
  # 
  # create table addresses to store the delivery addres sof the user
  # 
  # nickname (string) to give a name to the address
  # address1 (string) store the line1 of address which is compuslory
  # address2 (string) stores the line 2 of address
  # landmark (string) to store the nearest landmark of the address
  # 
  # 
  def up
    create_table :addresses do |t|

      t.string :nickname
      t.string :address_line1
      t.string :address_line2
      t.string :landmark
      t.integer :address_type
      
      t.references :user, index: true, foreign_key: true
      t.references :area, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  # 
  # [down description]
  # 
  # drop table address for the rollback
  # 
  def down
      drop_table :addresses
  end
end

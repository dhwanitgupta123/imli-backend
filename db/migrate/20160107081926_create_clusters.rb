class CreateClusters < ActiveRecord::Migration
  # 
  # up: create table clusters
  # 
  # label: [string] used to store label of clusters
  # images: [array] to save image id array
  # cluster_type: [integer] cluster type
  # description: [string] this is description about the cluster
  # status: [integer] to store the current state of category
  # 
  def up
    create_table :clusters do |t|
      t.string :label
      t.integer :status
      t.integer :cluster_type
      t.integer :images, array: true, default: []
      t.text :description

      t.timestamps null: false
    end
  end

  #
  # drop table clusers
  # 
  def down
    drop_table :clusters
  end
end

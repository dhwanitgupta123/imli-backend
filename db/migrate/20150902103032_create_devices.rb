class CreateDevices < ActiveRecord::Migration
  # 
  # [up description]
  # 
  # create table device to store the info of the devices using the app
  # device_id (string) store the unique id of the device, could be null if web app
  # platform (string) store the name of platform on which the product is being used
  # app_version (string) stores the version of app which the user is using
  # last_login (string) stores when the user logged in last from the device
  # user_id (reference) stores the foreign key reference to user table 
  # 
  def up
    create_table :devices do |t|
      t.string :device_id
      t.string :platform
      t.string :app_version
      t.timestamp :last_login
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  # 
  # [down description]
  # 
  # delete table device for rollback
  # 
  def down
    drop_table :devices
  end
end

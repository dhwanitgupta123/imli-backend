#
# Migration to add images column to marketplace selling pack table
#
class AddImagesInMarketplaceSellingPack < ActiveRecord::Migration
  #
  # Function to add images column in marketplace selling pack Table
  #
  def up
    add_column :marketplace_selling_packs, :images, :integer, array: true, default: []
  end
  
  #
  # Function to roll back the migration
  #
  def down
    remove_column :marketplace_selling_packs, :images
  end
end

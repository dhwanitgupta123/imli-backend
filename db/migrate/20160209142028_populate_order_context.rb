#
# Migration to populate Order context for all the currently
# present Order
#
class PopulateOrderContext < ActiveRecord::Migration

  #
  # ADD all entries for Order Context
  #
  def up
    populate_order_context
  end

  #
  # Delete all entries for Order Context
  #
  def down
    MarketplaceOrdersModule::V1::OrderContext.delete_all
  end

  # 
  # Populate order context in mongo table OrderContext with respect to all
  # the currently present order ids
  # Value to populate:
  #   * order_id: id of the order
  #   * platform_type: 'UNKNOWN'
  # 
  def populate_order_context
    orders = MarketplaceOrdersModule::V1::Order.all

    unknown_platform_type = CommonModule::V1::PlatformType.get_id_by_type('UNKNOWN')

    orders.each do |order|
      id = order.id
      order_context_dao = MarketplaceOrdersModule::V1::OrderContextDao.new
      order_context = order_context_dao.create_order_context({
        order_id: id,
        platform_type: unknown_platform_type
      })
    end
  end

end
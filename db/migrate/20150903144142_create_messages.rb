class CreateMessages < ActiveRecord::Migration
  # 
  # [up description]
  # create table message
  # otp (string) to store the otp provided to customer for login
  # expires_at (timestamp) to store the time when otp expires
  # resend_count (integer) to store how many times user has requested for resend
  # 
  def up
    create_table :messages do |t|
      t.string :otp
      t.timestamp :expires_at
      t.references :provider, index: true, foreign_key: true
      t.integer :resend_count
      t.timestamps null: false
    end
  end
  # 
  # [down description]
  # 
  # drop table sms for rollback
  # 
  # 
  def down
      drop_table :message
  end
end

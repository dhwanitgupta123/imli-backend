class AddDateTimeSlotsToOrders < ActiveRecord::Migration
  #
  # Function to add date and time column to orders table
  #
  def up
    add_column :orders, :preferred_delivery_date, :date
    add_column :orders, :time_slot_id, :integer, references: :time_slots
  end #End of up

  #
  # Function to remove name column from addresses Table
  #
  def down
    remove_column :orders, :preferred_delivery_date
    remove_column :orders, :time_slot_id
  end
end

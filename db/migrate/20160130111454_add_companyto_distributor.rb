class AddCompanytoDistributor < ActiveRecord::Migration
  #
  # Function to add reference to distributor with company
  #
  def up
    add_reference :distributors, :company, index: true
    add_foreign_key :distributors, :companies
  end

  #
  # Function to roll back the migration
  #
  def down
    remove_foreign_key :distributors, :companies
    remove_column :distributors, :company_id
  end
end

class SequentialVendorCode < ActiveRecord::Migration
  #
  # Migration to create sequence for the Brand pack code
  #
  def up
    max_vendor_code = SupplyChainModule::V1::Distributor.maximum('vendor_code')
    if max_vendor_code.blank?
      max_vendor_code = '1'
    else
      max_vendor_code = (max_vendor_code + 1).to_s
    end
    say "Creating sequence for  vendor code starting at max_vendor_code"
    execute 'CREATE SEQUENCE vendor_code_seq START ' + max_vendor_code + ';'
    say "Adding NEXTVAL('vendor_code_seq') to column vendor_code"
    execute "ALTER TABLE distributors ALTER COLUMN vendor_code SET DEFAULT NEXTVAL('vendor_code_seq');"
  end

  #
  # Function to rollback the migration
  #
  def down
    execute 'DROP SEQUENCE IF EXISTS vendor_code_seq CASCADE;'
  end
end

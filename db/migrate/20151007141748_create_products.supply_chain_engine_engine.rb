# This migration comes from supply_chain_engine_engine (originally 20151007135031)
#
# creating table product
#
class CreateProducts < ActiveRecord::Migration
  #
  # products table
  #
  # name: product title
  # logo_url: product image url
  #
  def up
    create_table :products do |t|
      t.string :name
      t.string :logo_url
      t.string :initials
      t.string :product_code
      t.integer :status
      t.timestamps null: false

      t.references :sub_brand, index: true, foreign_key: true
      t.references :variant, index: true, foreign_key: true
    end
  end

  #
  # delete table products
  #
  def down
    drop_table :products
  end
end
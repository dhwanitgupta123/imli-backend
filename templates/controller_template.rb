#
# Module to handle all the functionalities related to MODULENAME
#
module MODULENAMEModule
  #
  # Version1 for MODULENAME module
  #
  module V1
    #
    # CONTROLLERNAME controller service class to route panel API calls to the CONTROLLERNAMEApi
    #
    class CONTROLLERNAMEPLURALController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_CAPSCONTROLLERNAME = MODULENAMEModule::V1::AddCONTROLLERNAMEApi
      UPDATE_CAPSCONTROLLERNAME = MODULENAMEModule::V1::UpdateCONTROLLERNAMEApi
      GET_ALL_CAPSCONTROLLERNAME = MODULENAMEModule::V1::GetAllCONTROLLERNAMEApi
      GET_CAPSCONTROLLERNAME_API = MODULENAMEModule::V1::GetCONTROLLERNAMEApi
      CHANGE_CAPSCONTROLLERNAME_STATE_API = MODULENAMEModule::V1::ChangeCONTROLLERNAMEStateApi
      #
      # Initialize helper classes
      #
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMMON_EVENTS = ENGINENAMECommonModule::V1::CommonEvents

      #
      # before actions to be executed
      #
      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end
      #
      # initialize CONTROLLERNAMEsController Class
      #
      def initialize
      end

      #
      # function to add small_controller_name
      #
      # post /add_small_controller_name
      #
      # Request:: request object contain
      #   * small_controller_name_params [hash] it contains name
      #
      # Response::
      #   * If action succeed then returns Success Status code with create small_controller_name
      #    else returns BadRequest response
      #
      def add_small_controller_name
        add_small_controller_name_api = ADD_CAPSCONTROLLERNAME.new(@deciding_params)
        response = add_small_controller_name_api.enact(small_controller_name_params)
        send_response(response)
      end

      swagger_controller :small_controller_name, 'MODULENAMEModule APIs'
      swagger_api :add_small_controller_name do
        summary 'It creates the small_controller_name with given input'
        notes 'create small_controller_name'
        param :body, :small_controller_name_request, :small_controller_name_request, :required, 'create_small_controller_name request'
        response :bad_request, 'if request params are incorrect or api fails to create small_controller_name'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :small_controller_name_request do
        description 'request schema for /create_category API'
        property :small_controller_name, :small_controller_name_hash, :required, 'small_controller_name model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :small_controller_name_hash do
        description 'details of small_controller_name'
        property :name, :string, :required, 'name of small_controller_name'
      end

      #
      # function to update small_controller_name
      #
      # post /update_small_controller_name
      #
      # Request:: request object contain
      #   * small_controller_name_params [hash] it contains name
      #
      # Response::
      #   * If action succeed then returns Success Status code with update small_controller_name
      #    else returns BadRequest response
      #
      def update_small_controller_name
        update_small_controller_name_api = UPDATE_CAPSCONTROLLERNAME.new(@deciding_params)
        response = update_small_controller_name_api.enact(small_controller_name_params.merge({ id: get_id_from_params }))
        send_response(response)
      end

      swagger_controller :small_controller_name, 'MODULENAMEModule APIs'
      swagger_api :update_small_controller_name do
        summary 'It updates the small_controller_name with given input'
        notes 'update small_controller_name'
        param :path, :id, :integer, :required, 'small_controller_name to update'
        param :body, :update_small_controller_name_request, :update_small_controller_name_request, :required, 'create_small_controller_name request'
        response :bad_request, 'if request params are incorrect or api fails to create small_controller_name'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :update_small_controller_name_request do
        description 'request schema for /create_category API'
        property :small_controller_name, :small_controller_name_hash, :required, 'small_controller_name model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      #
      # function to get all small_controller_names
      #
      # GET /get_all_small_controller_names
      #
      # Response::
      #   * list of small_controller_names
      #
      def get_all_small_controller_names
        get_small_controller_names_api = GET_ALL_CAPSCONTROLLERNAME.new(@deciding_params)
        response = get_small_controller_names_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :small_controller_name, 'MODULENAMEModule APIs'
      swagger_api :get_all_small_controller_names do
        summary 'It returns details of all the small_controller_names'
        notes 'get_all_small_controller_names API returns the details of all the small_controller_names'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all small_controller_name'
        param :query, :per_page, :integer, :optional, 'how many small_controller_name to display in a page'
        param :query, :state, :integer, :optional, 'to fetch small_controller_names based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to change the status of small_controller_name
      #
      # PUT /small_controller_name/state/:id
      #
      # Request::
      #   * small_controller_name_id [integer] id of small_controller_name of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        change_small_controller_name_state_api = CHANGE_CAPSCONTROLLERNAME_STATE_API.new(@deciding_params)
        response = change_small_controller_name_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :small_controller_name, 'MODULENAMEModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of small_controller_name'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the small_controller_name & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'small_controller_name_id to be deactivated/activated'
        param :body, :change_small_controller_name_state_request, :change_small_controller_name_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or small_controller_name not found or it fails to change state'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_small_controller_name_state_request do
        description 'request schema for /change_state API'
        property :small_controller_name, :change_small_controller_name_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_small_controller_name_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      #
      # function to delete brand
      #
      # DELETE /small_controller_name/state/:id
      #
      # Request::
      #   * small_controller_name_id [integer] id of small_controller_name which is to be deleted
      #
      # Response::
      #   * sends ok response to the panel
      #
      def delete_small_controller_name
        change_small_controller_name_state_api = CHANGE_CAPSCONTROLLERNAME_STATE_API.new(@deciding_params)
        response = change_small_controller_name_state_api.enact({ 
                      id: get_id_from_params, 
                      event: COMMON_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :small_controller_name, 'MODULENAMEModule APIs'
      swagger_api :delete_small_controller_name do
        summary 'It delete the small_controller_name'
        notes 'delete_small_controller_name API change the status of small_controller_name to soft delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :path, :id, :integer, :required, 'small_controller_name_id to be deleted'
        response :bad_request, 'wrong parameters or small_controller_name not found or it fails to delete'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a small_controller_name
      #
      # GET /small_controller_names/:id
      #
      # Request::
      #   * small_controller_name_id [integer] id of small_controller_name of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_small_controller_name
        get_small_controller_name_api = GET_CAPSCONTROLLERNAME_API.new(@deciding_params)
        response = get_small_controller_name_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :small_controller_name, 'MODULENAMEModule APIs'
      swagger_api :get_small_controller_name do
        summary 'It return a small_controller_name'
        notes 'get_small_controller_name API return a single small_controller_name corresponding to the id'
        param :path, :id, :integer, :required, 'small_controller_name_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or small_controller_name not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end


      ###############################
      #       Private Functions     #
      ###############################

      private 

      def small_controller_name_params
        params.require(:small_controller_name).permit(:id, :name)
      end

      def change_state_params
        params.require(:small_controller_name).permit(:event).merge({ id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end
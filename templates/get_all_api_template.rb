#
# Module to handle all the functionalities related to MODULENAME
#
module MODULENAMEModule
  #
  # Version1 for MODULENAME module
  #
  module V1
    #
    # GetCONTROLLERNAME api, it validates the request, call the
    # small_controller_name service and return the JsonResponse
    #
    class GetAllCONTROLLERNAMEApi < ENGINENAMEBaseModule::V1::BaseApi
      CAPSCONTROLLERNAME_SERVICE = MODULENAMEModule::V1::CONTROLLERNAMEService
      CAPSCONTROLLERNAME_MAPPER = MODULENAMEModule::V1::CONTROLLERNAMEMapper
      CAPSCONTROLLERNAME_RESPONSE_DECORATOR = MODULENAMEModule::V1::CONTROLLERNAMEResponseDecorator

      def initialize(params='')
        @params = params
        super
      end

      #
      # Return all the small_controller_name
      #
      # @return [JsonResponse]
      #
      def enact(request)
        small_controller_name_service_class = CAPSCONTROLLERNAME_SERVICE.new(@params)

        begin
          response = small_controller_name_service_class.get_all_small_controller_name(request)
          get_all_small_controller_names_api_response = CAPSCONTROLLERNAME_MAPPER.get_all_small_controller_names_api_response_array(response)
          return CAPSCONTROLLERNAME_RESPONSE_DECORATOR.create_get_all_small_controller_names_response(get_all_small_controller_names_api_response)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return CAPSCONTROLLERNAME_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end
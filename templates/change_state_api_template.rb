#
# Module to handle all the functionalities related to MODULENAME
#
module MODULENAMEModule
  #
  # Version1 for MODULENAME module
  #
  module V1
    #
    # ChangeCONTROLLERNAMEStateApi api, it validates the request, call the
    # small_controller_name service to change the state of given small_controller_name id
    #
    class ChangeCONTROLLERNAMEStateApi < ENGINENAMEBaseModule::V1::BaseApi
      CAPSCONTROLLERNAME_SERVICE = MODULENAMEModule::V1::CONTROLLERNAMEService
      CAPSCONTROLLERNAME_MAPPER = MODULENAMEModule::V1::CONTROLLERNAMEMapper
      CAPSCONTROLLERNAME_RESPONSE_DECORATOR = MODULENAMEModule::V1::CONTROLLERNAMEResponseDecorator

      def initialize(params='')
        @params = params
        super
      end

      #
      # Function takes input corresponding to change small_controller_name state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, change state params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        small_controller_name_service_class = CAPSCONTROLLERNAME_SERVICE.new(@params)
        begin
          validate_request(request)
          small_controller_name = small_controller_name_service_class.change_small_controller_name_state({ id: request[:id], event: request[:event].to_i })
          change_small_controller_name_state_api_response = CAPSCONTROLLERNAME_MAPPER.change_small_controller_name_state_api_response_hash(small_controller_name)
          return CAPSCONTROLLERNAME_RESPONSE_DECORATOR.create_change_small_controller_name_state_response(change_small_controller_name_state_api_response)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return CAPSCONTROLLERNAME_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return CAPSCONTROLLERNAME_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        end
      end

      private

      #
      # Validates the required params of change state
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Request should contain small_controller_name id')
        end
        if request.blank? || request[:event].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end
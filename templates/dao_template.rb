#
# Module to handle all the functionalities related to MODULENAME
#
module MODULENAMEModule
  #
  # Version1 for MODULENAME module
  #
  module V1
    #
    # Data Access Object to interact with CONTROLLERNAMEModel
    #
    class CONTROLLERNAMEDao < ENGINENAMEBaseModule::V1::BaseDao

      CAPSCONTROLLERNAME_MODEL = MODULENAMEModule::V1::CONTROLLERNAME

      def initialize(params = '')
        @params = params
        super
      end

      def create(args)
        small_controller_name_model = CAPSCONTROLLERNAME_MODEL
        create_model(args, small_controller_name_model, 'CONTROLLERNAME')
      end

      def update(args, small_controller_name)
        update_model(args, small_controller_name, 'CONTROLLERNAME')
      end

      def get_by_id(id)
        get_model_by_id(CAPSCONTROLLERNAME_MODEL, id)
      end

      def get_all(pagination_params = {})
        get_all_element(CAPSCONTROLLERNAME_MODEL, pagination_params)
      end
    end
  end
end

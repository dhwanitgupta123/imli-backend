require 'route_constraints'
ENGINENAMEEngine::Engine.routes.draw do
  # Predicate for versioning based on app version
  match_app_version_predicate = Proc.new do |version, request|
    CommonModule::V1::RoutingPredicates.match_app_version(version, request)
  end
  # Route for MODULE_NAME MODULE with versioning and constraint matching
  scope module: :small_module_name_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do

      post 'small_controller_names/new'   => 'small_controller_names#add_small_controller_name'
      put 'small_controller_names/:id'    => 'small_controller_names#update_small_controller_name'
      put 'small_controller_names/state/:id' => 'small_controller_names#change_state'
      get 'small_controller_names/:id'    => 'small_controller_names#get_small_controller_name'
      delete 'small_controller_names/:id' => 'small_controller_names#delete_small_controller_name'
      get 'small_controller_names'        =>  'small_controller_names#get_all_small_controller_names'

    end
  end
end
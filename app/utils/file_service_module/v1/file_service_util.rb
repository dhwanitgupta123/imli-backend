#
# Module to handle functionalities related to file upload to S3
#
module FileServiceModule
  #
  # Version1 for file service module
  #
  module V1
    #
    # module contains general utils to upload data or file to s3
    #
    class FileServiceUtil

      CUSTOM_ERRORS = CommonModule::V1::CustomErrors

      #
      # Upload the data to S3
      #
      # @param [Hash] args contains
      #   :data [String] String to write
      #   :key [key] key of the object
      #   :options [Hash] hash contains other options
      #
      # @return [String] public url of the file uploaded
      #
      def upload_data(args)
        args = symbolize_hash_keys(args)
        s3_object = S3_BUCKET.objects[args[:key]]
        begin
          s3_object.write(args[:data], symbolize_hash_keys(args[:options]))
        rescue => e
          raise CUSTOM_ERRORS::S3FailedToUpload.new(e)
        end
        return s3_object.public_url.to_s
      end

      #
      # Upload the file to S3
      #
      # @param [Hash] args contains
      #  :data [String] file_path
      #  :key [key] key of the object
      #  :options [Hash] hash contains other options
      #
      # @return [String] public url of the file uploaded
      #
      # @error [FileNotExists] if given file_path not exists
      # @error [S3FailedToUpload] if it fails to upload file
      #
      def upload_file(args)
        args = symbolize_hash_keys(args)
        s3_object = S3_BUCKET.objects[args[:key]]
        begin
          s3_object.write(Pathname.new(args[:file_path]), symbolize_hash_keys(args[:options]))
        rescue => e
          raise CUSTOM_ERRORS::S3FailedToUpload.new(e)
        end
        return s3_object.public_url.to_s
      end

      #
      # This function sumbolie the keys of the input hash
      # 
      # @param [Hash] input_hash
      #
      # @return [Hash] hash with keys as symbols
      #
      def symbolize_hash_keys(input_hash)
        input_hash.keys.each do |key| 
          input_hash[key.to_sym] = input_hash.delete(key)
        end
        return input_hash
      end

      #
      # This function create the unique file path
      # logic :- 
      #  1. get the epoc time, convert it into string
      #  2. append 0's to make epoc string of 12 character
      #  3. split the epoc at every 4 character
      #  4. join the array by '/' to create the directory
      #  5. now add the key at the end of directory with '_'
      #
      #  example: key = 'a.txt'
      #  epoc_time = 1445260932
      #  epoc = '1445260932' // length 10
      #  epoc_key_in_twelve_digits = '00' + epoc = '001445260932'
      #  divide_epoc_at_every_four_digit = ['0014', '4526' , '0932']
      #  join_time = '0014/4526/0932'
      #  key = '0014/4526/0932_a.txt' 
      #  
      #  Linux filesystem ext3 can normally only handle 32,000 subfolders in a folder
      #  so if start creating each document in same directory then it will break after
      #  32000 file uploads
      #  Assuming in 1000 sec (approx 17 minutes) we are not creating more than 32000 files
      #  therefore breaking the 12 digit epoc in to 3 substring of each 4 digit
      #  
      #  According to the this logic we can save 32000 File in 16 minutes, 1024000000 files in 116 days
      #  and 327680000 million files in 31709 years
      # 
      # The above calculation is for filesystem ext3 for ext4 replace 32000 to 64000
      # 
      # @param key [String] file name
      # 
      # @return file_path [String] unique file path
      #
      def get_s3_key(key)

        epoc_time = Time.now.to_i
        epoc = epoc_time.to_s

        epoc_length = epoc.length

        epoc_key_in_twelve_digits = '0'*(12 - epoc_length) + epoc

        divide_epoc_at_every_four_digit = epoc_key_in_twelve_digits.scan(/..../)

        join_time = divide_epoc_at_every_four_digit.join('/')
        file_path = join_time + '_' + key
        return file_path
      end
    end
  end
end

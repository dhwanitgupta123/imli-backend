#
# Common Module which caters common functionality
#
module CommonModule
  #
  # Version 1 for Pagination
  #
  module V1
    #
    # Class pagination to apply filter based on page, per_page & other filters
    #
    class Pagination
      # TO-DO: 
      # Remove these normal variables and use only prefixed
      # variables in future

      # if no page_no is specified
      PAGE_NO = 1

      # if length of page is not specified
      PER_PAGE = 20

      # sort object based on which attribute
      SORT_BY = 'id'

      # default sort order Ascending/Descending
      ORDER = 'ASC'

      # Default values
      DEFAULT_PAGE_NO = 1
      DEFAULT_PER_PAGE = 20
      DEFAULT_SORT_BY = 'id'
      DEFAULT_ORDER = 'ASC'
      # Infinited number of per page results (Value of 2**32)
      INFINITE_PER_PAGE = 2**32
      INFINITE_PAGE_NO = 1
      # allowed orders
      ALLOWED_ORDERS = %w(ASC, DESC)
      CATEGORIZATION_SORT_BY = 'priority'
    end
  end
end

# Utility to store all kind of status codes used overall
module CommonModule
  module V1
    class StatusCodes < BaseModule::V1::BaseUtil
      SUCCESS = 200
      NO_CONTENT = 204
      BAD_REQUEST = 400
      UN_AUTHORIZED = 401
      FORBIDDEN = 403
      NOT_FOUND = 404
      AUTHENTICATION_TIMEOUT = 419
      UNPROCESSABLE_ENTITY = 422
      PRE_CONDITION_REQUIRED = 428
      TOO_MANY_REQUESTS = 429
      INTERNAL_SERVER_ERROR = 500
    end
  end
end
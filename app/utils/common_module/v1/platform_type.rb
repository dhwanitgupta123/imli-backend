# Module contain all common functionalities
module CommonModule
  # Version1 for common module
  module V1
    # This class contain all the platform types
    class PlatformType < BaseModule::V1::BaseUtil

      PLATFORM_TYPES = [
        {
          id: 1,
          type: 'UNKNOWN',
          label: 'UNKNOWN'
        },
        {
          id: 2,
          type: 'ANDROID',
          label: 'ANDROID'
        },
        {
          id: 3,
          type: 'WEB',
          label: 'WEB'
        }
      ]

      #
      # Get display name for platform type by id
      # If no label found, then return 'UNKNOWN'
      #
      def self.get_display_platform_type_by_id(id)
        return nil if id.blank?
        platform_type = PLATFORM_TYPES.select {|platform_type| platform_type[:id] == id.to_i}
        return platform_type[0][:label] if platform_type.present?
        return 'UNKNOWN'
      end

      #
      # Get ID of platform type by its type-string
      # If passed string is 'nil', then return id for default platform type: WEB
      #
      def self.get_id_by_type(platform_type_string)
        default_id = 3 # id of WEB
        if platform_type_string.blank?
          return default_id
        end
        platform_type = PLATFORM_TYPES.select {|platform_type| platform_type[:type] == platform_type_string.to_s}
        return platform_type[0][:id] if platform_type.present?
        return default_id
      end

    end # End of class
  end
end

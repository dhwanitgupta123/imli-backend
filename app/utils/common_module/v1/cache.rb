require 'json'
module CommonModule
  module V1
    class Cache < BaseModule::V1::BaseUtil
      #
      # Function to write keys & their values in the cache
      #
      # Parameters::
      #   * key: Constant name
      #   * value : Value of constant
      #   * namespace: Namespace to be append with key
      #
      def self.write(args)
        return false if args.blank?
        args[:key] = args[:namespace] + ":" + args[:key] if args[:namespace].present?

        # Store it in redis
        $redis.set(args[:key], args[:value])
        # Set expiry time if it is present
        set_expire(args[:key], args[:expire]) if args[:expire].present?
      end

      #
      # Function to read values of corresponding key from cache
      #
      # Parameters::
      #   * key: Constant name
      #
      # @return [value] of the key given as parameters
      #
      def self.read(key, namespace = '')
        key = namespace + ':' + key if namespace.present?
        $redis.get(key)
      end

      # 
      # Return the value as int
      # 
      # Parameters:
      #   * key: Constant name
      # 
      # @return value of the key in int format
      def self.read_int(key)
        val = self.read(key)
        if val.present?
          val.to_i
        end
      end

      #
      # Return the value as JSON
      #
      # Parameters:
      #   * key: Key for which value is to be fetched from cache
      #
      # @return value of the key in JSON format
      #
      def self.read_json(key, namespace = '')
        key = namespace + ':' + key if namespace.present?
        val = self.read(key)
        if val.present?
          begin   # JSON parsing may throw error
            return JSON.parse(val)
          rescue
            return {}
          end
        end
      end

      #
      # Read value from Cache and try to convert it to array
      #
      # Parameters:
      #   * key: Key for which value is to be fetched from cache
      #
      # @return value of the key in Array format
      #
      def self.read_array(key)
        val = self.read(key)
        begin
          if val.present?
            return eval(val)
          end
        rescue
          return []
        end
        return []
      end

      #
      # Function to display all keys stored in cache
      #
      # @return [Hash] of the keys stored in cache
      #
      def self.get_keys
        # Return all keys stored in Redis
        $redis.keys
      end

      # 
      # Retrun true if key exits in cache
      # 
      # @param key [String] Constant name
      # 
      # @return [Boolean] true if exists else false
      #
      def self.exists?(key)
        $redis.exists(key)
      end

      # 
      # Delete the key from cache
      # 
      # @param key [String] Constant name
      # 
      # @return [Integer] 0 if key not present and del function fails else 1 
      # 
      def self.delete(key, namespace = '')
        key = namespace + ':' + key if namespace.present?
        $redis.del(key)
      end

      # Function set expiry time for key if it is already present in cache
      # 
      # Parameters 
      #   *key: Constant name
      #   *time_to_live: Integer value in seconds to set TTL
      # 
      def self.set_expire(key, time_to_live)
        $redis.expire(key, time_to_live)
      end
    end
  end
end

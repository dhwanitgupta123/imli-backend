module CommonModule
  module V1
    # 
    # Enums corresponding to the possible benefits and benefit types
    # 
    class BenefitEnum

      #
      # Name of benefit with corresponding class that will be called while
      # applying the benefit
      # 
      BENEFIT_ENUM = [
        {
          id: 1,
          benefit: 'ADD_AMPLE_CREDIT_TO_USER',
          benefit_class: CommonModule::V1::BenefitsModule::V1::UserAmpleCreditBenefit
        },
        {
          id: 2,
          benefit: 'ADD_AMPLE_CREDIT_TO_REFERRER',
          benefit_class: CommonModule::V1::BenefitsModule::V1::ReferrerAmpleCreditBenefit
        }
      ]

      #
      # Type of benefit
      # 
      BENEFIT_TYPE_ENUM = [
        {
          type: 1,
          label: 'AMPLE_CREDITS'
        }
      ]
      # 
      # Return benefit_id  by given benefit as input
      #
      # @param benefit [String] benefit
      # 
      # @return [Integer] id of the benefit
      #
      def self.get_benefit_id_by_benefit(benefit)
        benefit_enum = BENEFIT_ENUM.select {|benefit_row| benefit_row[:benefit] == benefit.to_s}
        return benefit_enum[0][:id] if benefit_enum.present?
        return nil
      end

      # 
      # Return benefit_class  by given benefit_id as input
      #
      # @param benefit_id [Integer] benefit_id
      # 
      # @return [String] benefit_class
      #
      def self.get_benefit_class_by_benefit_id(benefit_id)
        benefit_enum = BENEFIT_ENUM.select {|benefit_row| benefit_row[:id] == benefit_id.to_i}
        return benefit_enum[0][:benefit_class] if benefit_enum.present?
        return nil
      end

      # 
      # Return benefit_type by given label as input
      #
      # @param label [String] type
      # 
      # @return [Integer] type of benefit
      #
      def self.get_benefit_type_by_label(label)
        benefit_type_enum = BENEFIT_TYPE_ENUM.select {|benefit_row| benefit_row[:label] == label.to_s}
        return benefit_type_enum[0][:type] if benefit_type_enum.present?
        return nil
      end
    end
  end
end

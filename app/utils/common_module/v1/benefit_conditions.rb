module CommonModule
  module V1
    # 
    # BenefitConditions has enums corresponding to the possible types of benefit conditions
    # 
    class BenefitConditions
      MAX_ADDRESS    = 'max_address'
    end
  end
end

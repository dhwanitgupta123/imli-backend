# Utility used for logging exceptions and messages
# Currently using Raven sentry as internal functionality
# to store the logs.
# @author sumit<sumitchhuttani007@gmail.com>
module CommonModule
  module V1
    class Logger < BaseModule::V1::BaseUtil

      #
      # Log exceptions. currently using Raven
      # @param e [Exception] Exception to be logged
      #
      def self.log_exception(e)
        Raven.capture_exception(e)
      end

      # Error levels are in following order :
      #
      # debug (the least serious)
      # info
      # warning
      # error
      # fatal (the most serious)
      
      #
      # Log message with level as debug
      # @param message [String] message to be logged
      #
      def self.debug(message)
        Raven.capture_message(message, level: 'debug')
      end

      #
      # Log message with level as info
      # @param message [String] message to be logged
      #
      def self.info(message)
        Raven.capture_message(message, level: 'info')
      end

      #
      # Log message with level as warning
      # @param message [String] message to be logged
      #
      def self.warn(message)
        Raven.capture_message(message, level: 'warning')
      end

      #
      # Log message with level as error
      # @param message [String] message to be logged
      #
      def self.error(message)
        Raven.capture_message(message, level: 'error')
      end

      #
      # Log message with level as fatal
      # @param message [String] message to be logged
      #
      def self.fatal(message)
        Raven.capture_message(message, level: 'fatal')
      end

    end
  end
end
# Module contain all common functionalities
module CommonModule
  # Version1 for common module
  module V1
    # This class contain all the regex utilities
    class RegexUtil < BaseModule::V1::BaseUtil

      #Regex to validate Pincode
      PINCODE_REGEX = /\A\d+\z/

      #Regex to validate a generic email id
      VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

      #Regex to validate an email id as a gmail id
      VALID_GMAIL_REGEX = /\A[\w+\-.]+@gmail\.com\z/i

      #Regex to validate a phone number being a 10-digit number
      VALID_PHONE_NUMBER_REGEX = /\A\[1..9\]|\d{10}\z/
    end
  end
end

# Utility to store all kind templates
module CommonModule
  module V1
    class TemplateTypes
      SINGLE_ORDER_PACKING_DETAILS = {
        type: 'SINGLE_ORDER_PACKING_DETAILS',
        template: 'single_order_packing_details',
        relative_location: '/marketplace_orders_module/v1/single_order_packing_details.liquid'
      }
      ORDER_INVOICE = {
        type: 'ORDER_INVOICE',
        template: 'order_invoice',
        relative_location: '/marketplace_orders_module/v1/order_invoice.liquid'
      }
    end
  end
end

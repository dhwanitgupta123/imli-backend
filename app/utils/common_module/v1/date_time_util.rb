module CommonModule
  module V1

    class DateTimeUtil < BaseModule::V1::BaseUtil

      #
      # Comparator to compare two Time objects based on their HH::MM::SS
      # It will return -1, 0 and 1
      #   * -1, if lhs < rhs
      #   *  0, if lhs == rhs
      #   *  1, if lhs > rhs
      #
      # @param time1 [Time] [Time 1]
      # @param time2 [Time] [Time 2]
      #
      # @return [Integer] [-1,0, 1 based upon time1 is less than, equal or greater than time2]
      #
      def self.compare(time1, time2)
        # Convert to string of hours+minutes+seconds+nanoseconds
        # Since, we are comparing time, we need to convert both to current ZONE time
        comparable_time1 = time1.localtime.strftime("%H%M%S%N")
        comparable_time2 = time2.localtime.strftime("%H%M%S%N")

        return comparable_time1 < comparable_time2 ? -1 : (comparable_time1 == comparable_time2 ? 0 : 1)
      end

      #
      # Convert Integer to seconds
      #
      # @param value [Integer] [value of HOUR which is to be converted to seconds]
      #
      # @return [Integer] [value in seconds]
      #
      def self.convert_to_seconds(value)
        # Forex: 4 --> 4*60*60 = 14400
        return value.to_i * 60 * 60
      end

      #
      # Convert epoc to DateTime. If utc is true, then convert it to UTC timezone
      # UTC is required to store it in DB
      #
      # @param epoch [String] [Epoch seconds]
      # @param utc = false [Boolean] [whether conversion to UTC needed or not]
      #
      # @return [DateTime] [epoch seconds to DateTime object]
      #
      def self.convert_epoch_to_datetime(epoch, utc = false)
        return nil if epoch.blank?
        date_time = Time.zone.at(epoch.to_i).to_datetime
        if utc.present?
          date_time = date_time.utc
        end
        return date_time
      end

      #
      # Convert epoc to Time. If utc is true, then convert it to UTC timezone
      # UTC is required to store it in DB
      #
      # @param epoch [String] [Epoch seconds]
      # @param utc = false [Boolean] [whether conversion to UTC needed or not]
      #
      # @return [Time] [epoch seconds to Time object]
      #
      def self.convert_epoch_to_time(epoch, utc = false)
        return nil if epoch.blank?
        time = Time.zone.at(epoch.to_i)
        if utc.present?
          time = time.utc
        end
        return time
      end

      #
      # Convert DateTime to epoch seconds
      #
      # @param date_time [DateTime] [DateTime object which is to be converted]
      #
      # @return [Integer] [datetime converted to epoch seconds]
      #
      def self.convert_date_time_to_epoch(date_time)
        return nil if date_time.blank?
        return date_time.to_time.to_i
      end

      #
      # Convert Time to epoch seconds
      #
      # @param date_time [Time] [Time object which is to be converted]
      #
      # @return [Integer] [time converted to epoch seconds]
      #
      def self.convert_time_to_epoch(time)
        return nil if time.blank?
        return time.to_i
      end

      #
      # Get current time (Time class) in UTC
      #
      # @param utc = false [Boolean] [whether conversion to UTC needed or not]
      #
      # @return [Time] [Time object with current time in utc]
      #
      def self.get_current_time(utc = false)
        time = Time.zone.now.to_time
        if utc.present?
          time = time.utc
        end
        return time
      end

    end
  end
end

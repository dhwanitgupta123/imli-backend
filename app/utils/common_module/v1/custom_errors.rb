# Module to contain all kind of custom errors raised/rescued
# during the operation
module CommonModule
  module V1
    module CustomErrors
      class UnAuthorizedUserError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = UnAuthorizedUserError
          @message = message
        end
      end

      class UnAuthenticatedUserError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = UnAuthenticatedUserError
          @message = message
        end
      end

      class InsufficientDataError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = InsufficientDataError
          @message = message
        end
      end

      class InvalidDataError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = InvalidDataError
          @message = message
        end
      end

      class RunTimeError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = RunTimeError
          @message = message
        end
      end

      class ExistingUserError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = ExistingUserError
          @message = message
        end
      end

      class WrongOtpError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = WrongOtpError
          @message = message
        end
      end

      class AuthenticationTimeoutError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = AuthenticationTimeoutError
          @message = message
        end
      end

      class ResourceNotFoundError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = ResourceNotFoundError
          @message = message
        end
      end

      class WrongPasswordError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = WrongPasswordError
          @message = message
        end
      end

      class ReachedMaximumLimitError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = ReachedMaximumLimitError
          @message = message
        end
      end

      class WrongReferralCodeError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = WrongReferralCodeError
          @message = message
        end
      end

      class PreConditionRequiredError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = PreConditionRequiredError
          @message = message
        end
      end

      class InvalidArgumentsError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = InvalidArgumentsError
          @message = message
        end
      end

      class RecordNotFoundError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = RecordNotFoundError
          @message = message
        end
      end

      class RecordAlreadyExistsError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = RecordAlreadyExistsError
          @message = message
        end
      end

      class CategoryNotFoundError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = CategoryNotFoundError
          @message = message
        end
      end

      class DepartmentNotFoundError < BaseModule::V1::BaseErrors
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = DepartmentNotFoundError
          @message = message
        end
      end

      class ExistingEmployeeError < StandardError
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = ExistingEmployeeError
          @message = message
        end
      end

      class EmployeeNotFoundError < StandardError
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = EmployeeNotFoundError
          @message = message
        end
      end

      class UserNotFoundError < StandardError
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = UserNotFoundError
          @message = message
        end
      end

      class EmailNotFoundError < StandardError
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = EmailNotFoundError
          @message = message
        end
      end

      class EmailAlreadyVerifiedError < StandardError
        attr_accessor :failed_action, :message
        def initialize(message="")
          @failed_action = EmailAlreadyVerifiedError
          @message = message
        end
      end

      class S3BucketNotFound < StandardError
        attr_accessor :failed_action, :message
        def initialize(message='')
          @failed_action = S3BucketNotFound
          @message = message
        end
      end

      class S3FailedToUpload < StandardError
        attr_accessor :failed_action, :message
        def initialize(message='')
          @failed_action = S3FailedToUpload
          @message = message
        end
      end

      class FileNotFound < StandardError
        attr_accessor :failed_action, :message
        def initialize(message='')
          @failed_action = FileNotFound
          @message = message
        end
      end

      class ImageUploadFailed < StandardError
        attr_accessor :failed_action, :message
        def initialize(message='')
          @failed_action = ImageUploadFailed
          @message = message
        end
      end

      class DirectoryNotFound < StandardError
        attr_accessor :failed_action, :message
        def initialize(message='')
          @failed_action = DirectoryNotFound
          @message = message
        end
      end

      class S3ObjectNotFoundError < StandardError
        attr_accessor :failed_action, :message
        def initialize(message='')
          @failed_action = S3ObjectNotFoundError
          @message = message
        end
      end
    end
  end
end

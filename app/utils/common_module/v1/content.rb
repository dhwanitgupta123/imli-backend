module CommonModule
  module V1
    class Content < BaseModule::V1::BaseUtil
      # Model CRUD content
      OK_RESPONSE = 'Success'
      CREATED_SUCCESSFULLY = '%{model} created successfully'
      CREATION_FAILED = 'Failed to create %{model}'
      UPDATION_FAILED = 'Failed to update %{model}'
      # Access Control Content
      UNAUTHENTIC_USER = 'User not authenticated'
      UNAUTHORIZED_USER = 'Access Denied! Not authorized to access this API'
      INSUFFICIENT_DATA = 'Insufficient data passed'
      #When OTP or email token Expired
      AUTHENTICATION_TIMEOUT = 'Authentication request timeout'
      # when generic provider exists
      EXISTING_USER = 'User is already signed up'
      # InternalServerError
      RUN_TIME_ERROR = 'Internal server error occurred'
      # OTP doesn't matches
      WRONG_OTP = 'Wrong OTP sent'
      # when resource not found
      NOT_FOUND = 'Resource not found'
      # when transaction status is invalid
      INVALID_STATUS = 'Invalid transaction status'
      # When password doesn't matches
      WRONG_PASSWORD = 'Wrong Password'
      # When too many requests
      TOO_MANY_REQUESTS = 'You have reached the maximum limit'
      #When User is still in inactive state
      INACTIVE_STATE = 'You are still in Inactive state'
      # When user is still in pending state
      PENDING_STATE = 'You are still in Pending state'
      # Email is already verified
      EMAIL_ALREADY_VERIFIED = 'Email is already verified'
      # Email sent successfully
      EMAIL_SENT_SUCCESS = 'Email sent successfully'
      # Employee Signup and SignIn content
      EXISTING_EMPLOYEE = 'Employee is already registered'
      # When we are unable to process/find relevant information from the data passed by user
      INVALID_DATA_PASSED = 'Invalid data passed'
      # When User is trying to active but is in Inactive state instead of pending
      PRE_CONDITION_REQUIRED = 'Transition to next state is not allowed'
      # when pincode present already
      NON_EXISTING_PINCODE = 'Pincode does not exist, first create the pincode '
      # when referral code limit has reached 0
      REFERRAL_EXPIRED = 'Referral code has reached the limit'
      # when user uses his own referral code
      BEING_SMART = 'Nice try! ;)'
      # when invalid email is sent
      INVALID_EMAIL_ID = 'Not a valid Email ID'
      # Email id not found for authentication token
      EMAIL_NOT_FOUND = 'Email ID not registered'
      # when auth token has expired to verify email id
      TOKEN_EXPIRED = 'Authentication request expired'
      # when email is already verified
      EMAIL_VERIFIED = 'Email ID is already verified'
      # when user not found
      USER_NOT_FOUND = 'User not found'
      # when employee is in pending state
      EMPLOYEE_PENDING = 'You are still in pending state'
      # when authentication token not found
      TOKEN_NOT_FOUND = 'Authentication token must be present'
      # when wrong phone no. is give
      WRONG_PHONE_NUMBER = 'Wrong Phone Number'
      # when manager phone number is wrong
      WRONG_MANAGER_PHONE_NUMBER = 'Wrong Manager Phone Number'
      # when session token is not found
      SESSION_TOKEN_NOT_FOUND = 'Session token not found'
      # when provider not found
      NO_PROVIDER = 'Provider not found'
      # when gateway is not found
      WRONG_GATEWAY = 'Payment gateway not found'
      # otp has expired
      OTP_EXPIRED = 'OTP has expired'
      # when employee is already registered as a customer
      ALREADY_REGISTERED = 'You are already registered, please Login'
      # when wrong provider type is sent
      INVALID_PROVIDER_TYPE = 'Invalid provider type'
      # when user assigns role to himself
      ROLE_ASSIGN_NOT_ALLOWED = 'Not allowed to assign role to yourself'
      # wrong referral code
      WRONG_REFERRAL_CODE = 'Wrong Referral Code'
      # when message provider not found
      MESSAGE_PROVIDER_MISSING = 'Message provider not found'
      # when no user found corresponding to a session token
      NO_USER = 'User not found'
      # when user is not an employee
      NOT_AN_EMPLOYEE = 'User is not registered as an employee'
      # when generic provider not found
      PLEASE_LOGIN = 'Please register first with your password'
      # when user is not registered with message provider type
      REQUEST_OTP = 'Please register first with your phone number'
      # when Password provider is not presernt
      PLEASE_REGISTER = 'Please register with us'
      # when user is still in pending state
      PENDING_USER = 'User is in pending state'
      # when email is not registered
      EMAIL_NOT_REGISTERED = 'Email ID is not registered'
      # email already registered
      EMAIL_ALREADY_REGISTERED = 'Email ID has already been taken'
      # when user is not allowed to add more addresses
      ADDRESS_NOT_ALLOWED = 'Not allowed to add more addresses'
      # when benefits are not found
      BENEFITS_NOT_FOUND = 'No benefits not found'
      # When category is not present
      CATEGORY_NOT_PRESENT = 'Category does not exist'
      # When sub category not present
      SUB_CATEGORY_NOT_PRESENT = 'Sub Category does not exist'
      # When category is still inactive
      NON_ACTIVE_CATEGORY = 'Category not Active'
      # No subcategories exist for selected category
      NO_SUB_CATEGORIES = 'No sub categories exists for selected category'
      # When department is not present
      DEPARTMENT_NOT_PRESENT = 'Department does not exist'
      # when no payment pending membership is present
      NO_PAYMENT_PENDING_MEMBERSHIP = 'No Payment pending membership found'
      # When user is blocked by the system
      BLOCKED_USER = 'You are blocked for %{time}'
      # Role added successfully
      ROLE_ADDED = 'Role added successfully'
      # Role removed successfully
      ROLE_REMOVED = 'Role removed successfully'
      # Page Not Found
      PAGE_NOT_FOUND = 'Page Not Found'
      # Parameter missing in POST/PUT requests
      PARAMETER_MISSING = "Request parameters missing"
      # Citrus transaction failed due to signature mismatch
      CITRUS_TRANSACTION_FAILED = "Signature Verification Failed"
      # When MAP settings resource not found
      MAP_SETTINGS_NOT_FOUND = "Map Settings not found"
      # Field must present
      FIELD_MUST_PRESENT = "%{field} must be present"
      # Field has already been taken
      FIELD_ALREADY_TAKEN =  "%{field} has already been taken"
      # Field can't be zero or negative
      FIELD_ONLY_POSITIVE = "%{field} can't be zero or negative"
      # Field can't be negative
      FIELD_NON_NEGATIVE = "%{field} can't be negative or blank" 
      # Not found generic content
      RESOURCE_NOT_FOUND = "%{resource} not found"
      # Event not defined
      EVENT_NOT_DEFINED = "%{event} event not defined"
      # REFERRAL_LIMIT_INCREASED
      REFERRAL_LIMIT_INCREASED = 'Referral limit increased successfully'
      # Plan hash must present
      PLAN_HASH_MUST_PRESENT = "Plan Hash must present"
      # PLAN not found
      PLAN_NOT_FOUND = "Plan not found"
      # Benefit not found
      BENEFIT_NOT_FOUND = "Benefit not found"
      # Benefits array must be present
      BENEFITS_ARRAY_MUST_PRESENT = "Benefits array must be present"
      # Benefit must be active
      BENEFIT_MUST_BE_ACTIVE = "Benefit %{id}: %{name} must be active"
      # when fields should be equal
      SHOULD_BE_EQUAL = "%{field} should be equal"
      # company reference is missing
      COMPANY_MISSING = 'Reference to company is missing'
      # when company not found
      COMPANY_NOT_FOUND = 'Company not found'
      # company name is missing
      COMPANY_NAME_MISSING = 'Name of Company not provided'
      # Brand reference is missing
      BRAND_MISSING = 'Reference to brand is missing'
      # when brand not found
      BRAND_NOT_FOUND = 'Brand not found'
      # sub brand reference is missing
      SUB_BRAND_MISSING = 'Reference to sub brand is missing'
      # when sub brand not found
      SUB_BRAND_NOT_FOUND = 'Sub brand not found'
      # Product reference is missing
      PRODUCT_MISSING = 'Reference to product is missing'
      # when product not found
      PRODUCT_NOT_FOUND = 'Product not found'
      # No product foud for given category. Shown when user
      # asks for list of products corresponding to a product
      NO_PRODUCT_EXISTS = 'Sorry, No product is available'
      # reference to variant no provided
      VARIANT_MISSING = 'Reference to variant not provided'
      # variant not found
      VARIANT_NOT_FOUND = 'Variant not found'
      # details of variant missing
      VARIANT_DETAILS_MISSING = 'Variant name and initials not provided'
      # MBP reference is missing
      BRAND_PACK_MISSING = 'Reference to Brand Pack not provided.'
      # MBP not found
      BRAND_PACK_NOT_FOUND = 'Brand Pack not found.'
      # BP is inactive
      INACTIVE_BP = 'Brand Pack is inactive'
      # reference of distributor is missing
      DISTRIBUTOR_MISSING = 'Reference of distributor is not provided'
      # Name of distributor is missing
      DISTRIBUTOR_NAME_MISSING = 'Name of distributor is missing'
      # When distributor is not found
      VENDOR_MISSING = 'Vendor not found'
      # Inventory reference missing
      INVENTORY_MISSING = 'Reference to Inventory not provided'
      # inventory not found
      INVENTORY_NOT_FOUND = 'Inventory not found'
      # name of inventory is missing
      INVENTORY_NAME_MISSING = 'Name of inventory not provided'
      # When wrong reference of address is given of a inventory
      WRONG_INVENTORY_ADDRESS_REFERENCE = 'Address does not belong to this Inventory'
      # IBP reference missing
      INVENTORY_BRAND_PACK_MISSING = 'Reference to Inventory Brand Pack not provided.'
      # IBP not found
      INVENTORY_BRAND_PACK_NOT_FOUND = 'Inventory Brand Pack not found.'
      # when no initiated puchase order is present
      NO_INITIATED_PO = 'No initiated Purchase Order'
      # when PO is not received yet
      PO_NOT_RECEIVED = 'Purchase order not yet received'
      # When PO is not verified
      PO_NOT_VERIFIED = 'Purchase order is not verified'
      # when no active inventory order product present
      NO_INVENTORY_ORDER_PRODUCT_ADDED = 'Purchase order cannot be placed, no Products added'
      # when given id of IBP doesn't belong to the respective Inventory
      WRONG_INVENTORY_ORDER_PRODUCT = 'Inventory Brand Pack does not belong to this Inventory'
      # when inventory order product id doesn't belong to inventory order
      WRONG_INVENTORY_ORDER_PRODUCT_REFERENCE = 'Inventory order product does not belong to this order'
      # When wrong tuple of brand pack & Inventory brand pack sent as request for WBP 
      BRAND_PACK_NOT_FOUND_IN_INVENTORY = 'No Brand pack found in respective Inventory'
      # C&C reference is missing
      CASH_AND_CARRY_MISSING = 'Reference to cash and carry not provided'
      # C&C not found
      CASH_AND_CARRY_NOT_FOUND = 'Cash and Carry not found'
      # When name of C&C is not provided
      CASH_AND_CARRY_NAME_MISSING = 'Name of cash and carry is not provided'
      # warehouse Reference missing
      WAREHOUSE_MISSING = 'Reference to warehouse not provided.'
      # when Inventory order is not placed by that warehouse
      NOT_WAREHOUSE_INVENTORY_ORDER = 'Purchase order not created in this warehouse'
      # Warehouse not found
      WAREHOUSE_NOT_FOUND = 'Warehouse not found'
      # When name of a warehouse is not provided
      WAREHOUSE_NAME_MISSING = 'Name of warehouse is not provided'
      # WBP reference missing
      WAREHOUSE_BRAND_PACK_MISSING = 'Reference to Warehouse Brand Pack not provided.' 
      # WBP not found
      WAREHOUSE_BRAND_PACK_NOT_FOUND = 'Warehouse Brand Pack not found'
      # When wrong tuple of brand pack & Warehouse brand pack sent as request for SBP 
      BRAND_PACK_NOT_FOUND_IN_WAREHOUSE = 'No Brand pack found in respective Warehouse'
      # When wrong reference of address is given of a warehouse
      WRONG_WAREHOUSE_ADDRESS_REFERENCE = 'Address does not belong to this Warehouse'
      # when given id of WBP doesn't belong to the respective warehouse
      WRONG_WAREHOUSE_BRAND_PACK = 'Warehouse Brand Pack does not belong to this Warehouse'
      # when stock of WBP is not present
      STOCK_NOT_AVAILABLE = '%{product} is out of stock'
      # Seller reference missing
      SELLER_MISSING = 'Reference to Seller not provided.' 
      # Seller not found
      SELLER_NOT_FOUND = 'Seller not found'
      # When name of a seller is not provided
      SELLER_NAME_MISSING = 'Name of seller is not provided'
      # SBP reference missing
      SELLER_BRAND_PACK_MISSING = 'Reference to Seller Brand Pack not provided.' 
      # SBP not found
      SELLER_BRAND_PACK_NOT_FOUND = 'Seller Brand Pack not found'
      # When wrong tuple of brand pack & Seller brand pack sent as request for MPBP 
      BRAND_PACK_NOT_FOUND_IN_SELLER = 'No Brand pack found with respective Seller'
      # MPBP reference missing
      MARKETPLACE_BRAND_PACK_MISSING = 'Reference to Marketplace Brand Pack not provided'
      # MPBP is inactive
      INACTIVE_MPBP = 'Marketplace Brand Pack is inactive'
      # MPSP reference missing
      MARKETPLACE_SELLING_PACK_MISSING = 'Reference to Marketplace Selling Pack not provided'
      # When dirty bit is being set true & product is active
      DIRTY_BIT_SET = 'Product is active, can not set testing mode on'
      # event to trigger change is missing in request
      EVENT_MISSING = 'Event not found.'
      # not allowed to change the status
      EVENT_NOT_ALLOWED = 'Event not allowed.'
      # not a valid event
      INVALID_EVENT = 'Not a valid event.'
      # not a valid state
      INVALID_STATE = 'Not a valid model state'
      # Order can be ASC/DESC
      WRONG_ORDER = 'Order can be only ASC/DESC'
      # When sort by attribute is not a table column
      VALID_SORT_BY = 'Please select a correct sort by'
      # when quantity of MPBP mssing to make a MPSP
      QUANTITY_MISSING = 'Please specify the quantity of Marketplace brand packs'
      # Order reference missing
      ORDER_ID_MISSING = 'Reference to Order ID not provided.'
      #Request blank
      REQUEST_BLANK = 'Request cannot be blank'
      # CART creation failed
      CART_CREATION_FAILED = 'Unable to create User cart'
      # No Active CART present
      NO_ACTIVE_CART = 'No Active Cart present'
      # Cart does not belong to user
      INVALID_CART = 'Invalid Cart'
      # No Active Order present
      NO_ACTIVE_ORDER = 'No Active Order present'
      # Invalid Order id
      ORDER_NOT_BELONG_TO_USER = 'Order does not belong to user'
      # Invalid Payment Mode
      INVALID_PAYMENT_MODE = 'Invalid Payment Mode'
      # No order associated with payment
      NO_ORDER_FOR_PAYMENT = 'No Order associated with this Payment'
      NOT_VALID_PAYMENT = 'Not valid payment for this User'
      NO_POST_ORDER_PAYMENT = 'Not a POST_ORDER payment'
      NO_PENDING_PAYMENT = 'No pending payment'
      # address not found
      ADDRESS_NOT_FOUND = 'Address not found'
      # Invalid Address of user
      INVALID_ADDRESS = 'Address not belongs to user'
      # when ladder pricing details are more than max_quantity
      WRONG_MAX_QUANTITY = 'Max quantity should be more than the levels of ladder pricing'
      #Pincode does not meet Regex and is invalid
      INVALID_PINCODE = 'Pincode should be of 6 digits'
      #Non-existent area
      NON_EXISTENT_AREA = 'Area does not exist'
      #Area cannot be nil
      AREA_MISSING = 'Area cannot be nil'
      #Event param missing
      MISSING_EVENT_PARAM = 'Please specify event'
      #Pincode param missing
      MISSING_PINCODE_PARAM = 'Please specify pincode'
      # when elastic search repository is missing
      MISSING_REPOSITORY = 'ElasticSearch: Repository does not exist'
      # when elastic search document is missing
      MISSING_DOCUMENT = 'ElasticSearch: Document does not exist'
      # when elastic search fails to add document
      UNABLE_TO_ADD_DOCUMENT = 'ElasticSearch: Add document operation for the record failed'
      # when image id is not present while creating model
      IMAGE_LINKING_FAILED = 'Image linking failed please upload images again'
      # when image with input id not ppresent
      INVALID_IMAGE_ID = 'Image not present'
      # must be active
      MUST_BE_ACTIVE = "%{name} must be active"
      # when payment ID of membership payment is not valid
      INVALID_PAYMENT_ID = 'Invalid Payment ID'
      # invalid parameter
      INVALID_PARAMETER = "%{field} is not a valid parameter"
      # field not found
      REQUESTED_FIELD_NOT_FOUND = '%{field} not found'
      # only numbers allowed
      ONLY_NUMBERS_ALLOWED = 'Only Numbers allowed in %{field}'
      # Use default time of product aggregation report generation if time period given 
      # is wrong or not provided
      USE_DEFAULT_TIME_PERIOD = 'Using Default Time (6am yesterday to 6am today)'
      # transaction id missing
      INVALID_TRANSACTION_ID = 'Invalid Transaction ID'
      # invalid transaction status
      INVALID_TRANSACTION_STATUS = 'Invalid Transaction Status'
      # when email id is already taken
      UNIQUE_EMAIL = 'Email ID has already been taken'
      # Invalid pincode query
      INVALID_PINCODE_QUERY = 'Query length must be greater than 3'
      # problem while storing User suggestion
      PROBLEM_SAVING_SUGGESTION = 'Sorry! There was a problem recording your suggestion'
      # Email id verified successfuly
      EMAIL_VERIFIED_SUCCESS = 'Your Email id has been verified successfully'
      # membership expired
      MEMBERSHIP_EXPIRED = 'Your Membership has been Expired, please renew it'

      # cluster reference is missing
      CLUSTER_MISSING = 'Cluster reference is missing'
      # when cluster label missing
      CLUSTER_LABEL_MISSING = 'Cluster label is missing'

      # Create password but unauthenticated
      CREATE_PASSWORD_UNAUTHENTICATED = 'User should be authenticated'
      # when forgot password called without having a password in DB
      USER_SHOULD_HAVE_PASSWORD = 'User should have password first'
      # When user ask for creating password without authenticating beforehand
      PLEASE_REAUTHENTICATE = 'Please re-authenticate before resetting password'
      # When user tries to login with password, but had never created his password
      NOT_REGISTERED_WITH_PASSWORD = 'You are not registered with us using password'
      # when not able to upload file due to unknown reason
      NOT_ABLE_TO_UPLOAD_FILE = 'Not able to upload the file'
      # When brand pack code is missing
      BRAND_PACK_CODE_MISSING = 'Brand pack code is missing'
      # When field is missing
      FIELD_MISSING = '%{field} is missing'
      # when bp corresponding to code not found
      BRAND_PACK_CODE_NOT_PRESENT = 'Brand pack not found for code: %{code}'
      # when distributor corresponding to code not found
      VENDOR_CODE_NOT_PRESENT = 'Vendor not found for code: %{code}'
      # s3 object not present corresponding to the key
      INVALID_S3_KEY = 'Invalid S3 key, S3 object not found'
      # when invalid duration is present
      INVALID_DURATION = 'Invalid duration provided'
      # Time slot expired
      TIME_SLOT_EXPIRED = 'Selected Time Slot Expired'
      # Invalid Time slot
      INVALID_TIME_SLOT = 'Invalid Time Slot'
      # Invalid Order Address
      NOT_VALID_ORDER_ADDRESS = 'Not valid address for this order'
      # Invalid order update address request
      NOT_VALID_REQUEST_FOR_ADDRESS_UPDATE = 'Not valid request for address update'
      # Cart not belongs to current Order
      NOT_VALID_ORDER_CART = 'Not valid cart for this order'
      # Invalid cart update for order request
      NOT_VALID_REQUEST_FOR_CART_UPDATE = 'Not valid request for cart update'
      # When status for stocking is diff from allowed states
      NOT_A_VALID_STOCKING_STATE = 'Not a valid status for stocking'
      # when description not found
      DESCRIPTION_NOT_FOUND = 'Description not found'
      # invalid description
      INVALID_ROOT_DESCRIPTION = 'It is not a root description'
      # when there is invalid resource mapping
      INVALID_RESOURCE_MAPPING = 'Resource not present'
      # when description already exits
      DESCRIPTION_EXISTS = 'Description already exists'
      # when user trying to apply referral code more than once
      ALREADY_REFERRED = 'Referral already applied'
    end
  end
end

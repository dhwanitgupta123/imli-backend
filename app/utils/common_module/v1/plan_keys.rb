module CommonModule
  module V1
    # 
    # PlanNames has enums corresponding to the possible types of Plan model
    #
    class PlanKeys
      TRIAL      = 1
      AMPLE_GO   = 2
      AMPLE_MORE = 3
      AMPLE_PRO  = 4
    end
  end
end

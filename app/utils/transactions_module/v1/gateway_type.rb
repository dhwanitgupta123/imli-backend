module TransactionsModule
  module V1
    class GatewayType
      CITRUS_WEB = 1
      RAZORPAY = 2
      M_SWIPE = 3
      CASH = 4

      #
      # This function return display gateway name
      #
      # @param type [Integer] type of gateway
      # 
      # @return [String] display label for gateway
      #
      def self.get_display_gateway_by_type(type)
        case type.to_i
        when CITRUS_WEB
          return 'CITRUS'
        when RAZORPAY
          return 'RAZORPAY'
        when M_SWIPE
          return 'M_SWIPE'
        when CASH
          return 'CASH'
        end
      end
    end
  end
end

module TransactionsModule
  module V1
    class TransactionStatus
      SUCCESS = 1
      FAILED = 2
      CANCELLED = 3
    end
  end
end

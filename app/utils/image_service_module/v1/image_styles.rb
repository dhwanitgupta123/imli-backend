module ImageServiceModule
  module V1
    module ImageStyles
      PLAN_IMAGE_STYLE_FILTERS = {
        xxxhdpi_small: '192x192>',
        xhdpi_medium: '230x230>',
        xhdpi_small: '96x96>',
        mdpi_large: '344x344>',
        mdpi_medium: '115x115>'
      }
    end
  end
end

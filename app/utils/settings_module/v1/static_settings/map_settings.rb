module SettingsModule
  #
  # Version1 for settings module 
  #
  module V1
    #
    # This is an interface which enforce all the validators to extend it
    # and implement validate function
    # 
    module StaticSettings

      class MapSettings < SettingsModule::V1::StaticSettings::BaseStaticSettings

        GENERAL_HELPER = CommonModule::V1::GeneralHelper


        def initialize(params = '')
          @settings_name = 'MAP_SETTINGS'
        end

        def fetch
          super
        end

        def load_and_store
          map_settings = load
          # Further parse MAP settings as it contains only
          # path to the file whose constants are needed
          generated_map = generate_map_from_map_settings(map_settings)
          # Write into cache
          store(generated_map)
          return generated_map
        end


        ###############################
        #       Private Functions     #
        ###############################

        private

        #
        # Fetch MAP settings as per the values (class names) given in passed Hash parameter
        # HASH contains key value pair where
        #   - key corresponds to KEY to be put in final JSON
        #   - value corresponds to ClassName from where values/constants to be fetched
        # Final JSON::
        #   - key: key sent in original passed hash
        #   - value: Hash of constants fetched from corresponding classes
        #
        # @param map_settings [JSON] [Hash containing key value pair for fetching map settings]
        #
        # @return [JSON] [Final Hash containing all constants from specified class names]
        #
        def generate_map_from_map_settings(map_settings)
          if map_settings.blank?
            return {}
          end
          map = {}
          map_settings.each do |key, value|
            map_class = GENERAL_HELPER.string_to_class(value)
            if map_class.present?
              constants_map = fetch_map_for_class(map_class)
              map[key.to_s] = constants_map
            end
          end
          return map
        end

        #
        # Fetch Hash of constants with respect to the given class
        # Iterate over all the global constants of the given class and return a hash
        # comprising { constant_key: constant_value }
        #
        # @param map_class [Object] [Class for which constants need to be fetched]
        #
        # @return [JSON] [Hash containing all constants of given class]
        #
        def fetch_map_for_class(map_class)
          constants_map = {}
          map_class.constants.each do |var|
            constant_value = map_class.const_get(var)
            constants_map[var.to_s] = constant_value.to_s
          end
          return constants_map
        end

      end # End of class
    end
  end
end

module SettingsModule
  #
  # Version1 for settings module 
  #
  module V1
    #
    # This is an interface which enforce all the validators to extend it
    # and implement validate function
    # 
    module StaticSettings

      class ConstantSettings < SettingsModule::V1::StaticSettings::BaseStaticSettings

        CACHE_UTIL = CommonModule::V1::Cache

        def initialize(params = '')
          super
        end

        def fetch
          create_map_of_all_required_constants
        end

        def load_and_store
          # Nothing to do.
          # For now, there is no need to store constants before hand, as the complete MAP
          # is fetched from constants already stored in Redis
        end

        ###############################
        #       Private Functions     #
        ###############################

        private

        #
        # Function to get map of all relevant constants which are required
        # on mobile App.
        # Fetch values of constants from CACHE, because there source might change
        #
        def create_map_of_all_required_constants
          constants = {
            BASIC_DELIVERY_COST: CACHE_UTIL.read('BASIC_DELIVERY_COST'),
            MINIMUM_CART_VALUE_FOR_FREE_DELIVERY: CACHE_UTIL.read('MINIMUM_CART_VALUE_FOR_FREE_DELIVERY'),
            FREE_DELIVERY_COST: CACHE_UTIL.read('FREE_DELIVERY_COST'),
            FIRST_ORDER_DELIVERY_COST: CACHE_UTIL.read('FIRST_ORDER_DELIVERY_COST'),
            CUSTOMER_CARE_NUMBER: CACHE_UTIL.read('CUSTOMER_CARE_NUMBER'),
            HOST: CACHE_UTIL.read('HOST')
          }
          return constants
        end

      end # End of class
    end
  end
end

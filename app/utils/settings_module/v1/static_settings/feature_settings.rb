module SettingsModule
  #
  # Version1 for settings module 
  #
  module V1
    #
    # This is an interface which enforce all the validators to extend it
    # and implement validate function
    # 
    module StaticSettings

      class FeatureSettings < SettingsModule::V1::StaticSettings::BaseStaticSettings

        def initialize(params = '')
          @settings_name = 'FEATURE_SETTINGS'
        end

        def fetch
          super
        end

        def load_and_store
          feature_settings = load
          # Store above fetched settings in Redis
          store(feature_settings)
          return feature_settings
        end

      end # End of class
    end
  end
end

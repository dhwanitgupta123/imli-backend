module SettingsModule
  #
  # Version1 for settings module 
  #
  module V1
    #
    # This is an interface which enforce all the validators to extend it
    # and implement validate function
    # 
    class BaseSettings

      CACHE_UTIL = CommonModule::V1::Cache
      SETTINGS_UTIL = SettingsModule::V1::SettingsUtil

      def initialize(params = '')
      end

      #
      # Fetch the settings from CACHE based on the setting_name
      # Note: If setting is not found from cache, then it will be again fetched
      # and stored in cahce
      # 
      # @return [JSON] [corresponding setting]
      #
      def fetch
        cache_key = SETTINGS_UTIL.get_cache_key_for_settings(@settings_name)
        feature = CACHE_UTIL.read_json(cache_key)
        if feature.present?
          return feature
        else
          # Call corresponding class's load_and_store method
          # Note: self.load_and_store does not call base setting method, instead it calls
          # corresponding child class method
          return self.load_and_store
        end
      end

      #
      # Load settings from Distributed Store OR local YML file
      #
      # @return [JSON] [Hash of fetched settings]
      #
      def load
        settings = SETTINGS_UTIL.fetch_settings_from_distributed_store(@settings_name)
        if settings.blank?
          settings = SETTINGS_UTIL.fetch_settings_from_local_file(@settings_name)
        end
        return settings
      end

      #
      # Store settings into CACHE with proper key name (prefix + setting_name)
      #
      # @param value [JSON] [Hash of the settings value]
      #
      def store(value)
        # Write into cache
        cache_key = SETTINGS_UTIL.get_cache_key_for_settings(@settings_name)
        CACHE_UTIL.write({
          key: cache_key,
          value: JSON.generate(value) # Store values after stringify the JSON
        })
      end

      #
      # This function will be overrided by implementations of this interface 
      #
      def load_and_store
        raise 'load_and_store method not implemented'
      end

    end # End of class
  end
end

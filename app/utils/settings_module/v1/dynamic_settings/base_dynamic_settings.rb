module SettingsModule
  #
  # Version1 for settings module 
  #
  module V1
    #
    # This is an interface which enforce all the validators to extend it
    # and implement validate function
    # 
    module DynamicSettings

      class BaseDynamicSettings < SettingsModule::V1::BaseSettings

      end # End of class
    end
  end
end

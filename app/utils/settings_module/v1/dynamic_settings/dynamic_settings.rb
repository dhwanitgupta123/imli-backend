module SettingsModule
  #
  # Version1 for settings module 
  #
  module V1
    #
    # This is an interface which enforce all the validators to extend it
    # and implement validate function
    #
    module DynamicSettings

      class DynamicSettings < SettingsModule::V1::DynamicSettings::BaseDynamicSettings

        def initialize(params = '')
          @settings_name = 'DYNAMIC_SETTINGS'
        end

        def fetch
          super
        end

        def load_and_store
          dynamic_settings = load
          # Store above fetched settings in Redis
          store(dynamic_settings)
        end

      end # End of class
    end
  end
end

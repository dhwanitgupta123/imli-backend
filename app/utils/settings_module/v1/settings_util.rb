module SettingsModule
  module V1
    class SettingsUtil < BaseModule::V1::BaseUtil
      # Add versioned classes in global constants
      SETTINGS_MAP = SettingsModule::V1::SettingsMap
      DISTRIBUTED_STORE_UTIL = DistributedStoreModule::V1::DistributedStoreUtil

      MAP_SETTINGS = SettingsModule::V1::StaticSettings::MapSettings
      FEATURE_SETTINGS = SettingsModule::V1::StaticSettings::FeatureSettings
      DYNAMIC_SETTINGS = SettingsModule::V1::DynamicSettings::DynamicSettings

      #
      # Fetch Settings from DISTRIBUTED STORE (forex: Consul)
      #
      # @param key [String] [Name of the setting]
      #
      # @return [JSON] [Hash of the settings values]
      #
      def self.fetch_settings_from_distributed_store(key)
        settings_relative_url = SETTINGS_MAP.get_settings_relative_store_url(key)
        settings = DISTRIBUTED_STORE_UTIL.fetch_environment_specific_config_from_distributed_store(settings_relative_url)
        return settings if settings.present?
        return nil
      end

      #
      # Fetch Settings from local YML file
      #
      # @param key [String] [Name of the setting]
      #
      # @return [JSON] [Hash of the settings values]
      #
      def self.fetch_settings_from_local_file(key)
        settings = {}
        settings_local_path = SETTINGS_MAP.get_settings_local_path(key)
        settings_file_path = Rails.root.join(settings_local_path).to_s
        if File.file?(settings_file_path)
          settings = YAML.load_file(settings_file_path)[Rails.env]
          puts '[SUCCESS]: YML - Settings fetched for ' + key.to_s + ' from local file path: ' + settings_file_path.to_s
        end
        return settings
      end

      #
      # Get cache key for the settings name
      # Actually, Settings are stored in cache when the Rails initializes
      # They are stored in cache with a prefix
      #
      # @param setting_name [String] [Name of the settings like 'MAP_SETTINGS', 'FEATURE_SETTINGS']
      #
      # @return [String] [Cache key corresponds to the setting_name, forex: 'imli:shield:settings:MAP_SETTINGS']
      #
      def self.get_cache_key_for_settings(setting_name)
        return SETTINGS_MAP.get_cache_key_for_settings(setting_name)
      end


      #
      # Get complete settings and store locally (in Redis cache)
      # This function is called when the Rails initialize. It will fetch all
      # relevant settings from CONSUL/local file and store it in Cahe
      #
      def self.get_and_locally_store_complete_settings
        # Printing lines to distinguish logs printed during fetching
        # each individual setting
        puts '--------------------------------------------'
        get_and_locally_store_feature_settings
        puts '--------------------------------------------'
        get_and_locally_store_map_settings
        puts '--------------------------------------------'
        get_and_locally_store_dynamic_settings
      end

      ###############################
      #       Private Functions     #
      ###############################

      private


      #
      # Get FEATURE settings from Consul (with fallback on local yml file)
      # and store it in local REDIS cache
      #
      def self.get_and_locally_store_feature_settings
        feature_settings = FEATURE_SETTINGS.new
        feature_settings.load_and_store
      end

      #
      # Get MAP settings from Consul (with fallback on local yml file)
      # and store it in local REDIS cache
      #
      def self.get_and_locally_store_map_settings
        map_settings = MAP_SETTINGS.new
        map_settings.load_and_store
      end

      #
      # Get MAP settings from Consul (with fallback on local yml file)
      # and store it in local REDIS cache
      #
      def self.get_and_locally_store_dynamic_settings
        map_settings = DYNAMIC_SETTINGS.new
        map_settings.load_and_store
      end

    end
  end
end
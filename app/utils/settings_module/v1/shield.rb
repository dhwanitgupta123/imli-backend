module SettingsModule
  module V1
    #
    # SHIELD is a wrapper to fetch feature values based on certain factors.
    # Currently, SHIELD is supporting 2 kind of values:
    # - Feature value : Static values based on factors (currently hard coded
    #   values, but in future can be marketplace, location, platform specific, etc.)
    # - Treatment value : Dynamic values based on the requester. It inbuilt uses split gem
    #   to persist and randomize treatment values based on user_id. In future, it should
    #   also be marketplace, location, platform specific, etc.
    #
    # @author [sumit]
    #
    class Shield < BaseModule::V1::BaseUtil

      SPLIT = SettingsModule::V1::Split
      SETTINGS_UTIL = SettingsModule::V1::SettingsUtil
      FEATURE_SETTINGS = SettingsModule::V1::StaticSettings::FeatureSettings

      #
      # Get Treatment Value corresponding to the requester
      # Currently the deciding factor is only 'user_id'. Later on, it can
      # be extended to Marketplace, Location, Platform, etc.
      #
      # @param key [String] [Key for which SHIELD treatment value is to be fetched]
      #
      # @return [String] [Value of the treatment]
      #
      def self.get_treatment_value(key)
        return nil if key.blank?
        split = SPLIT.new
        split.start_split_experiment(key)
      end

      #
      # Start SHIELD experiment for a particular user
      # Currently the deciding factor is only 'user_id'. Later on, it can
      # be extended to Marketplace, Location, Platform, etc.
      #
      # @param key [String] [Name of the experiment for which SHIELD experiment is to be started]
      #
      # @return [String] [Value of the experiment]
      #
      def self.start_experiment(experiment_name)
        return nil if experiment_name.blank?
        split = SPLIT.new
        split.start_split_experiment(experiment_name)
      end

      #
      # Finish SHIELD experiment for a particular user
      # Currently the deciding factor is only 'user_id'. Later on, it can
      # be extended to Marketplace, Location, Platform, etc.
      #
      # @param key [String] [Name of the experiment for which SHIELD experiment is to be finished]
      #
      # @return [String] [Value of the experiment]
      #
      def self.finish_experiment(experiment_name)
        split = SPLIT.new
        split.finish_split_experiment(experiment_name)
      end

      #
      # Get static feature value for a particular key
      #
      # @param key [String] [Key for which value is to be fetched]
      #
      # @return [String] [Value corresponds to the given key]
      #
      def self.get_feature_value(key)
        return nil if key.blank?
        feature_settings = FEATURE_SETTINGS.new
        features = feature_settings.fetch
        if features.present? && features[key.to_s].present?
          return features[key.to_s].to_s # Convert all feature values to string
        end
        return nil
      end

    end
  end
end
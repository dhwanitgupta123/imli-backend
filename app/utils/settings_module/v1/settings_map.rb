module SettingsModule
  module V1
    class SettingsMap < BaseModule::V1::BaseUtil


      SHIELD_NAMESPACE = 'imli:shield:settings'

      SETTINGS_ENUM = [
        {
          id: 1,
          key: 'MAP_SETTINGS',
          label: 'Map Settings',
          local_path: 'config/initializers/settings_module/map_settings.yml',
          relative_store_url: '/default/default/default/default/MAP_SETTINGS'
        },
        {
          id: 2,
          key: 'FEATURE_SETTINGS',
          label: 'Feature Settings',
          local_path: 'config/initializers/settings_module/feature_settings.yml',
          relative_store_url: '/default/default/default/default/FEATURES_SETTINGS'
        },
        {
          id: 3,
          key: 'DYNAMIC_SETTINGS',
          label: 'Dynamic Settings',
          local_path: 'config/initializers/settings_module/dynamic_settings.yml',
          relative_store_url: '/default/default/default/default/DYNAMIC_SETTINGS'
        }
      ]

      #
      # Get Cache Key for settings.
      # Settings are stored in Cache when Rails initializes with some prefix
      # foex: 'imli:shield:settings:MAP_SETTINGS'
      #
      def self.get_cache_key_for_settings(key)
        cache_key = SHIELD_NAMESPACE + ':' + key.to_s
        return cache_key
      end

      #
      # Get settings relative distributed store URL
      #
      # @param key [String] [key for the settings, ex: 'MAP_SETTINGS']
      # 
      # @return [String] [store url for the setting]
      #
      def self.get_settings_relative_store_url(key)
        settings = get_settings_by_key(key)
        return settings[:relative_store_url] if settings.present?
        return ''
      end

      #
      # Get settings relative Local YML file path
      #
      # @param key [String] [key for the settings, ex: 'MAP_SETTINGS']
      # 
      # @return [String] [local path for the setting]
      #
      def self.get_settings_local_path(key)
        settings = get_settings_by_key(key)
        return settings[:local_path] if settings.present?
        return ''
      end


      ###############################
      #       Private Functions     #
      ###############################

      private

      def self.get_settings_by_id(id)
        settings = SETTINGS_ENUM.select {|setting| setting[:id] == id.to_i}
        return settings[0] if settings.present?
        return {}
      end

      def self.get_settings_by_key(key)
        settings = SETTINGS_ENUM.select {|setting| setting[:key] == key.to_s}
        return settings[0] if settings.present?
        return {}
      end

    end
  end
end
#
# This module is responsible to handle all the pre-request validations
# like user state, user membership state etc 
#
module ValidationsModule
  #
  # Version1 for validation module 
  #
  module V1
    #
    # Validate if user membership is not in expired state 
    #
    class UserMembershipValidator < ValidationsModule::V1::ValidationInterface      

      CONTENT_UTIL = CommonModule::V1::Content
      MEMBERSHIP_DAO = UsersModule::V1::MembershipDao
      # 
      # Validate if given user is not in expired state
      #
      # @param user [ModelObject] User to check status for
      # 
      # @return [Hash] containg result if result is false then it also contain corresponding message 
      #
      def validate(user)
        membership_dao = MEMBERSHIP_DAO.new({})
        user_membership = membership_dao.get_pending_or_active_membership(user)
        return { result: true } if user_membership.present?
        return { result: false, message: CONTENT_UTIL::MEMBERSHIP_EXPIRED }
      end
    end
  end
end
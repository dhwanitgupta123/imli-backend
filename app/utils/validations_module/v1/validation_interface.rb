#
# This module is responsible to handle all the pre-request validations
# like user state, user membership state etc 
#
module ValidationsModule
  #
  # Version1 for validation module 
  #
  module V1
    #
    # This is an interface which enforce all the validators to extend it
    # and implement validate function
    # 
    class ValidationInterface

      #
      # This function will be overrided by implementations of this interface 
      #
      def validate(params)
        raise 'validate method is not implemented'
      end
    end
  end
end

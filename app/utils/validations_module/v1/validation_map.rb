#
# This module is responsible to handle all the pre-request validations
# like user state, user membership state etc 
#
module ValidationsModule
  #
  # Version1 for validation module 
  #
  module V1
    #
    # This module have map of validation number with Validator Class
    #
    module ValidationMap
      
      VALIDATORS = [
        { 
          id: 1, 
          validator: ValidationsModule::V1::ActiveUserValidator
        },
        {
          id: 2,
          validator: ValidationsModule::V1::UserMembershipValidator
        }
      ]

      # 
      # This function returns the validator corresponding to the given validation
      #
      # @param validation [Integer] validation number
      # 
      # @return [ValidationInterfaceClass] Class implementing functionality of ValidationInterface
      #
      def self.get_validator_by_validation(validation)

        validator = VALIDATORS.select{|validator| validator[:id] == validation.to_i}

        if validator.present?
          return validator[0][:validator]
        end

        return nil
      end
    end
  end
end

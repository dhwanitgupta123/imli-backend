#
# This module is responsible to handle all the pre-request validations
# like user state, user membership state etc 
#
module ValidationsModule
  #
  # Version1 for validation module 
  #
  module V1
    #
    # Validate if user is active or not 
    #
    class ActiveUserValidator < ValidationsModule::V1::ValidationInterface      

      CONTENT_UTIL = CommonModule::V1::Content
      # 
      # Validate if given user is in active state or not
      #
      # @param user [ModelObject] User to check status for
      # 
      # @return [Hash] containg result if result is false then it also contain corresponding message 
      #
      def validate(user)
        return { result: true } if user.active?
        return { result: false, message: CONTENT_UTIL::INACTIVE_STATE }
      end
    end
  end
end
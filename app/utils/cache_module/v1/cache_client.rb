#
# This module is responsible to handle all model caches
#
module CacheModule
  #
  # Version1 for validation module 
  #
  module V1
    #
    # mpsp cache client
    # 
    class CacheClient

      CACHE_UTILS = CommonModule::V1::Cache

      #
      # This function will be overrided by implementations of this interface 
      #
      def self.get(params, namespace)
        key = params[:key].to_s
        value = CACHE_UTILS.read(key, namespace)
        
        return nil if  value.nil?

        return Marshal.load(value)
      end

      #
      # This function will be overrided by implementations of this interface 
      #
      def self.set(params, namespace, ttl = nil)
        key = params[:key].to_s
        value = params[:value]

        CACHE_UTILS.write({ namespace: namespace, key: key, value: Marshal.dump(value), expire: ttl}) if ttl.present?

        CACHE_UTILS.write({ namespace: namespace, key: key, value: Marshal.dump(value)})
      end

      def self.remove(params, namespace)
        key = params[:key].to_s
        CACHE_UTILS.delete(key, namespace)
      end
    end
  end
end
module CommunicationsModule
  module V1
    module Mailers
      module V1
        class EmailType
          WELCOME = {
            type: 'WELCOME',
            template: 'welcome'
          }
          PLACED_ORDER = {
            type: 'PLACED_ORDER',
            template: 'placed_order'
          }
          AGGREGATED_PRODUCTS = {
            type: 'AGGREGATED_PRODUCTS',
            template: 'aggregated_orders'
          }
          SINGLE_ORDER_PACKING_DETAILS = {
            type: 'SINGLE_ORDER_PACKING_DETAILS',
            template: 'single_order_packing_details'
          }
          PENDING_USERS = {
            type: 'ALL_PENDING_USERS',
            template: 'all_pending_users'
          }
          EMAIL_INVITES = {
            type: 'EMAIL_INVITES',
            template: 'email_invites'
          }
          PRODUCT_SUGGESTION = {
            type: 'PRODUCT_SUGGESTION',
            template: 'product_suggestion'
          }
          INVITE = {
            type: 'INVITE',
            template: 'invite_mail'
          }
        end
      end
    end
  end
end

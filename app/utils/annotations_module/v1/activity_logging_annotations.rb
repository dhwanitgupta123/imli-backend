module AnnotationsModule
  module V1
    module ActivityLoggingAnnotations
      include Annotations

      #
      # Annotation, this will add annotation corresponding to the function
      #
      def _LogActivity_
        annotate_method(:log_activity, true)
      end

      # 
      # This function handle before annotation functionality
      #
      def pre_handle_acitivty_log_annotation(controller)
        return if !annotated?(controller.action_name)
        @api_spawn_time = Time.zone.now
      end

      # 
      # This function handle post annotation functionality
      #
      def post_handle_activity_log_annotation(controller)

        return if !annotated?(controller.action_name)

        epoc_time_before_action = @api_spawn_time.to_f
        epoc_time_after_completion = Time.zone.now.to_f
        time_to_execute_api = epoc_time_after_completion - epoc_time_before_action # in seconds
        time_to_execute_api = time_to_execute_api * 1000 # in milli seconds

        @access_control_service.log_request(
            {
              request: request,
              response: @response_data, 
              status_code: @status_code,
              controller: controller,
              time: time_to_execute_api,
              api_spawn_time: @api_spawn_time
            }
          )
      end

      # 
      # This function check if the method is annotated or not and return boolean result
      #
      def annotated?(action_name)
        return false if self.class.annotation_get(action_name.to_sym, :log_activity).nil?
        return true
      end
    end
  end
end

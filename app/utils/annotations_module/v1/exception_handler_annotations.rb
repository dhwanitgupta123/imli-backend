module AnnotationsModule
  module V1
    module ExceptionHandlerAnnotations
      include Annotations

      #
      # Annotation, this will add annotation corresponding to the function
      #
      def _ExceptionHandler_
        annotate_method(:exception, true, AnnotationsModule::V1::ExceptionHandlerAnnotations.method("execute"))
      end

      # 
      # This funntion check if this is a leave jump_point or not 
      # and correspondingly call other jp or call final call
      # 
      # @param jp_context [Hash] it contain context of jump point and proc
      #
      #
      def self.execute(jp_context)
        custom_error_util = CommonModule::V1::CustomErrors
        base_response_decorator = BaseModule::V1::BaseResponseDecorator

        begin
          Annotations.proceed(jp_context)
        rescue custom_error_util::InvalidArgumentsError => e
          return base_response_decorator.create_response_invalid_arguments_error(e.message)
        rescue custom_error_util::InsufficientDataError => e
          return base_response_decorator.create_response_bad_request(e.message)
        rescue custom_error_util::ResourceNotFoundError => e
          return base_response_decorator.create_response_bad_request(e.message)
        rescue custom_error_util::RecordAlreadyExistsError => e
          return base_response_decorator.create_response_pre_condition_required(e.message)
        rescue custom_error_util::RunTimeError => e
          return base_response_decorator.create_response_runtime_error(e.message)
        rescue => e
          return base_response_decorator.create_response_runtime_error(e.message)
        end
      end
    end
  end
end

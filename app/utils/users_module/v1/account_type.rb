module UsersModule
  module V1
    # Account type is linked with email-id. A user can have
    # multiple email-ids registered with the company, but with
    # unique account type
    class AccountType < BaseModule::V1::BaseUtil
      CUSTOMER = 1
      EMPLOYEE = 2
      VENDOR = 3
    end
  end
end

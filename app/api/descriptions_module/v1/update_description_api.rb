#
# Module to handle functionalities related to descriptions
#
module DescriptionsModule
  module V1
    
    class UpdateDescriptionApi < BaseModule::V1::BaseApi

      DESCRIPTION_SERVICE = DescriptionsModule::V1::DescriptionService
      DESCRIPTION_RESPONSE_DECORATOR = DescriptionsModule::V1::DescriptionResponseDecorator

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Execute the request and return response
      #
      # @return [Response] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)
        validate_request(request)
        description_service = DESCRIPTION_SERVICE.new(@params)
        description = description_service.transactional_update_descriptions(request)
        return DESCRIPTION_RESPONSE_DECORATOR.create_description_response(description) 
      end

      # 
      # Validate incoming request
      #
      # @param request [JSON] request hash
      #
      # @raise [InsufficientDataError] [if description id is not present]
      #
      def validate_request(request)
        error_array = []
        error_array.push('Description Id') if request[:id].blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end
    end
  end
end

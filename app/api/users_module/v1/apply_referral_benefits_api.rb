#
# Module to handle functionalities related to User
#
module UsersModule
  module V1
    
    #
    # API to apply referral code benefits to user
    #
    class ApplyReferralBenefitsApi < BaseModule::V1::BaseApi

      USER_SERVICE = UsersModule::V1::UserService
      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator

      def initialize(params = '')
        @params = params
      end

      #
      # Validate request hash and apply benefits
      #
      # @return [Response] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)
        begin
          validate_request(request)
          user_service = USER_SERVICE.new(@params)
          user = user_service.transactional_apply_refferal_benefits(request[:referral_code])
          return USER_RESPONSE_DECORATOR.create_ok_response(user)
        rescue CUSTOM_ERROR_UTIL::WrongReferralCodeError => e
          USER_RESPONSE_DECORATOR.create_wrong_referral_code_request(e.message)
        end
      end

      #
      # Function to validate check user request
      #
      def validate_request(request)
        if request[:referral_code].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Referral Code'})
        end
      end

    end
  end
end
#
# Module to handle functionalities related to User
#
module UsersModule
  module V1
    
    #
    # API to check whether user is registered with us or not
    #
    class CheckUserApi < BaseModule::V1::BaseApi

      USER_SERVICE = UsersModule::V1::UserService
      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator

      def initialize(params = '')
        @params = params
      end

      #
      # Validate user request hash and verify whether it belongs to 
      # any of the registered user
      #
      # @return [Response] [response to be sent to the user]
      #
      def enact(request)
        begin
          validate_check_user_request(request)
          user_service = USER_SERVICE.new(@params)
          response = user_service.check_user(request[:user])
          return USER_RESPONSE_DECORATOR.create_check_user_response(response)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return USER_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        end
      end

      #
      # Function to validate check user request
      #
      def validate_check_user_request(request)
        if request[:user].blank? || request[:user][:phone_number].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Phone Number'})
        end
      end

    end
  end
end
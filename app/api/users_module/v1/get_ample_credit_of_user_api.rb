#
# Module to handle functionalities related to User
#
module UsersModule
  module V1
    
    #
    # Get current balance in ample credit of user
    #
    class GetAmpleCreditOfUserApi < BaseModule::V1::BaseApi

      USER_SERVICE = UsersModule::V1::UserService
      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator
      AMPLE_CREDIT_MAPPER = CreditsModule::V1::AmpleCreditMapper

      def initialize(params = '')
        @params = params
      end

      #
      # Return current ample credits of user
      #
      # @return [JsonResponse] ample credit response of user
      #
      _ExceptionHandler_
      def enact
        user_service = USER_SERVICE.new(@params)
        ample_credit = user_service.get_current_ample_credit
        ample_credit = AMPLE_CREDIT_MAPPER.map_ample_credit_to_hash(ample_credit)
        return USER_RESPONSE_DECORATOR.create_ample_credit_response(ample_credit)
      end
    end
  end
end
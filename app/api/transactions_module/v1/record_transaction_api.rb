#
# Module to handle functionalities related to transactions
#
module TransactionsModule
  module V1
    
    class RecordTransactionApi < BaseModule::V1::BaseApi

      PAYMENT_TRANSACTIONS_SERVICE = TransactionsModule::V1::PaymentTransactionsService
      PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR = TransactionsModule::V1::PaymentTransactionsResponseDecorator

      def initialize(params = '')
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @return [Response] [response to be sent to the user]
      #
      def enact(request)
        begin
          validate_record_transaction_request(request)
          payment_transactions_service = PAYMENT_TRANSACTIONS_SERVICE.new(@params)
          response = payment_transactions_service.record_transaction(request)
          return response
        rescue CUSTOM_ERROR_UTIL::RunTimeError => e
          return PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        rescue => e
          # Log exception in Sentry if unable to record Post Order transaction
          # Since, this API will be later on used by DeliveryBoys Payment App and hence
          # debugging will be difficult
          SENTRY_LOGGER.log_exception(e)
          return PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        end
      end

      #
      # Function to validate record transation request
      #
      def validate_record_transaction_request(request)
        error_array = []
        error_array.push('Payment ID') if request[:payment_id].blank?
        error_array.push('Payment type') if request[:payment_type].blank?
        error_array.push('Transaction status') if request[:status].blank?
        error_array.push('Gateway') if request[:gateway].blank?
        error_array.push('Post Order details') if request[:post_order].blank?
        error_array.push('Post Order Reference ID') if request[:post_order].present? && request[:post_order][:reference_id].blank?
        error_array.push('Post Order payment Mode') if request[:post_order].present? && request[:post_order][:payment_mode].blank?
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: error_string })
        end
      end

    end
  end
end
#
# Module to handle functionalities related to settings
#
module SettingsModule
  module V1
    
    class GetDynamicSettingsApi < BaseModule::V1::BaseApi

      SETTING_SERVICE = SettingsModule::V1::SettingService
      SETTINGS_MAPPER = SettingsModule::V1::SettingsMapper
      SETTING_RESPONSE_DECORATOR = SettingsModule::V1::SettingResponseDecorator

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Execute the request and return response
      #
      # @return [Response] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact
        settings_service = SETTING_SERVICE.new(@params)
        response = settings_service.get_all_dynamic_settings
        get_dynamic_settings_response = SETTINGS_MAPPER.get_dynamic_settings_response_hash(response)
        return SETTING_RESPONSE_DECORATOR.create_dynamic_settings_response(get_dynamic_settings_response)
      end

    end
  end
end

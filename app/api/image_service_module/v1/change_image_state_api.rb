#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    #
    # This API is responsible to change state of image
    #
    class ChangeImageStateApi < BaseModule::V1::BaseApi

      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      IMAGE_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      IMAGE_SERVICE = ImageServiceModule::V1::ImageService
      IMAGE_SERVICE_DECORATOR = ImageServiceModule::V1::ImageServiceResponseDecorator

      def initialize(params)
        @params = params
        super
      end

      # 
      # Delete image will change state of image to soft_delete
      # 
      # This API will serve for both change_state and delete_image routes
      # **delete_image -> supported when action is delete_image and event is SOFT_DELETE
      # **change_state -> change state API support all state transitions except SOFT_DELETE
      #
      # @param image_id [Integer] image id
      # 
      # @return [JSONResponse] response
      #
      def enact(request)
        begin
          validate_request(request)
          image_service = IMAGE_SERVICE.new(@params)
          image = image_service.change_image_state(request)
          return IMAGE_SERVICE_DECORATOR.create_success_response
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return IMAGE_SERVICE_DECORATOR.create_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidArgumentsError, CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          return IMAGE_SERVICE_DECORATOR.create_response_bad_request(e.message)
        end
      end

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request[:event].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == IMAGE_MODEL_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end

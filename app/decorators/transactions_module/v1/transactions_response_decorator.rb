#
# Module to handle functionalities related to transactions
#
module TransactionsModule
  #
  # Version1 for transactions module
  #
  module V1

    class TransactionsResponseDecorator < BaseModule::V1::BaseResponseDecorator

      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes

      def self.create_transaction_status_response(status)
        response = {
          payload: {
            transaction: 
            {
              status: status
            }
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end
    end
  end
end

module SettingsModule
  module V1
    class SettingResponseDecorator < BaseModule::V1::BaseResponseDecorator
      # Define utils globally with versioning
      # Kept them as global constants, so that they are specific
      # to this class only
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content

      # 
      # Send ok response code back to new user or new provider with phone_number
      # 
      def self.create_static_settings_response(settings)
        response = {
          payload: {
            settings: settings
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Send ok response code back to new user or new provider with phone_number
      # 
      def self.create_dynamic_settings_response(settings)
        response = {
          payload: {
            dynamic_settings: settings
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end


    end
  end
end
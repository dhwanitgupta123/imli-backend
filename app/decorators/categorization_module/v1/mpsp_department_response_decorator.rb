module CategorizationModule
  module V1
    #
    # class to generate DepartmentService responses & other functionalities
    #
    # @author [anuj]
    #
    class MpspDepartmentResponseDecorator < BaseModule::V1::BaseResponseDecorator
      #
      #defining global variable with versioning
      #
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      MPSP_CATEGORY_SERVICE_HELPER = CategorizationModule::V1::MpspCategoryServiceHelper
      MPSP_DEPARTMENT_SERVICE_HELPER = CategorizationModule::V1::MpspDepartmentServiceHelper

      #
      # Send ok response code when mpsp_department is created successfully
      #
      def self.create_ok_response(mpsp_department)
        mpsp_department = MPSP_DEPARTMENT_SERVICE_HELPER.create_mpsp_department_hash(mpsp_department)
        response = {
          payload: {
            department: mpsp_department
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Create response of a mpsp_department with all the mpsp_categories
      # 
      def self.create_mpsp_department_details_response(mpsp_department)     
        mpsp_categories = MPSP_CATEGORY_SERVICE_HELPER.get_mpsp_categories_hash(mpsp_department.mpsp_categories)
        mpsp_department = MPSP_DEPARTMENT_SERVICE_HELPER.create_mpsp_department_hash(mpsp_department)
        mpsp_department_with_categories = mpsp_department.merge(mpsp_categories: mpsp_categories)
        response = {
          payload: {
            department: mpsp_department_with_categories
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Create response with a list of all the mpsp_department
      # 
      def self.create_all_mpsp_departments_response(args)
        mpsp_departments = MPSP_DEPARTMENT_SERVICE_HELPER.get_mpsp_departments_hash(args[:mpsp_departments])
        response = {
          payload: {
            departments: mpsp_departments
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: args[:page_count]
          }
        }
        return response
      end

      # 
      # Send Bad-Request response code back to user
      # 
      def self.create_response_mpsp_department_not_found_error(_message = '')
        response = {
          error: { message: CONTENT_UTIL::DEPARTMENT_NOT_PRESENT },
          response: RESPONSE_CODES_UTIL::BAD_REQUEST
        }
        return response
      end

      # 
      # Returns aggregated mpsp_department, mpsp_categories and sub_mpsp_categories
      #
      # @param aggregated_data [Model] Joined mpsp_department, mpsp_categories and sub_mpsp_categories
      # 
      # @return [JsonResponse]
      #
      def self.create_aggregated_data_response(aggregated_data)
        aggregated_data = MPSP_DEPARTMENT_SERVICE_HELPER.get_aggregated_data_array(aggregated_data)
        response = {
          payload: {
            departments: aggregated_data
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end
    end
  end
end

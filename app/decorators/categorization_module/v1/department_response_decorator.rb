module CategorizationModule
  module V1
    #
    # class to generate DepartmentService responses & other functionalities
    #
    # @author [anuj]
    #
    class DepartmentResponseDecorator < BaseModule::V1::BaseResponseDecorator
      #
      #defining global variable with versioning
      #
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      CATEGORY_SERVICE_HELPER = CategorizationModule::V1::CategoryServiceHelper
      DEPARTMENT_SERVICE_HELPER = CategorizationModule::V1::DepartmentServiceHelper
      IMAGE_SERVIC_HELPER = ImageServiceModule::V1::ImageServiceHelper

      #
      # Send ok response code when department is created successfully
      #
      def self.create_ok_response(department)
        department = create_department_hash(department)
        response = {
          payload: {
            department: department
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Create response of a department with all the categories
      # 
      def self.create_department_details_response(department)     
        categories = CATEGORY_SERVICE_HELPER.get_categories_hash(department.categories)
        response = {
          payload: {
            department: {
              id: department.id,
              label: department.label,
              image_url: department.image_url,
              status: department.status,
              description: department.description,
              priority: department.priority,
              images: IMAGE_SERVIC_HELPER.get_images_hash_by_ids(department.images),
              categories: categories
            }
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Create response with a list of all the department
      # 
      def self.create_all_departments_response(args)
        departments = DEPARTMENT_SERVICE_HELPER.get_departments_hash(args[:departments])
        response = {
          payload: {
            departments: departments
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: args[:page_count]
          }
        }
        return response
      end

      # 
      # Send Bad-Request response code back to user
      # 
      def self.create_response_department_not_found_error(_message = '')
        response = {
          error: { message: CONTENT_UTIL::DEPARTMENT_NOT_PRESENT },
          response: RESPONSE_CODES_UTIL::BAD_REQUEST
        }
        return response
      end

      #
      # create hash of deparment object attributes
      # 
      def self.create_department_hash(department)
        response = {
          id: department.id,
          label: department.label,
          image_url: department.image_url,
          description: department.description,
          priority: department.priority,
          status: department.status,
          images: IMAGE_SERVIC_HELPER.get_images_hash_by_ids(department.images)
        }
        return response
      end

      # 
      # Returns aggregated department, categories and sub_categories
      #
      # @param aggregated_data [Model] Joined department, categories and sub_categories
      # 
      # @return [JsonResponse]
      #
      def self.create_aggregated_data_response(aggregated_data)
        aggregated_data = DEPARTMENT_SERVICE_HELPER.get_aggregated_data_array(aggregated_data)
        response = {
          payload: {
            departments: aggregated_data
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end
    end
  end
end

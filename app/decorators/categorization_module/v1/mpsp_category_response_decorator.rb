#
# Module handles all the functionality related to Categorization of products
# 
module CategorizationModule
  #
  # Categorization module Version 1
  # 
  module V1 
    #
    # Class to generate CategoryService responses & other functionalities
    # 
    # @author [anuj]
    #
    class MpspCategoryResponseDecorator < BaseModule::V1::BaseResponseDecorator

      #
      #defining global variable with versioning
      # 
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      MPSP_CATEGORY_SERVICE_HELPER = CategorizationModule::V1::MpspCategoryServiceHelper
      MARKETPLACE_SELLING_PACKS = MarketplaceProductModule::V1::MarketplaceSellingPack
      MARKETPLACE_SELLING_PACK_MAPPER = MarketplaceProductModule::V1::MarketplaceSellingPackMapper

      # 
      # Send ok response code when mpsp_category is created successfully
      # 
      def self.create_ok_mpsp_category_response(mpsp_category)
        mpsp_category = MPSP_CATEGORY_SERVICE_HELPER.create_mpsp_category_hash(mpsp_category)
        response = {
          payload: {
            category: mpsp_category
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Send ok response code when sub category is created successfully
      # 
      def self.create_ok_mpsp_sub_category_response(mpsp_sub_category)
        mpsp_sub_category = MPSP_CATEGORY_SERVICE_HELPER.create_mpsp_sub_category_hash(mpsp_sub_category)
        response = {
          payload: {
            sub_category: mpsp_sub_category
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Create response with a list of all the mpsp_categories
      # 
      def self.create_all_mpsp_categories_response(args)
        mpsp_categories = MPSP_CATEGORY_SERVICE_HELPER.get_mpsp_categories_hash(args[:mpsp_categories])
        response = {
          payload: {
            categories: mpsp_categories
          },
          meta: {
            page_count: args[:page_count]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Create response with a list of all the sub mpsp_categories
      # 
      def self.create_all_mpsp_sub_categories_response(args)
        mpsp_sub_categories = MPSP_CATEGORY_SERVICE_HELPER.get_mpsp_sub_categories_hash(args[:mpsp_sub_categories])
        response = {
          payload: {
            sub_categories: mpsp_sub_categories
          },
          meta: {
            page_count: args[:page_count]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Create response of a mpsp_category with all the sub categories if Department is present
      # Create response of sub category if mpsp_department is not present
      # 
      def self.create_mpsp_category_details_response(mpsp_category)
        if mpsp_category.mpsp_department.present?
          mpsp_sub_categories = MPSP_CATEGORY_SERVICE_HELPER.get_mpsp_sub_categories_hash(mpsp_category.mpsp_sub_categories)
          mpsp_category = MPSP_CATEGORY_SERVICE_HELPER.create_mpsp_category_hash(mpsp_category)
          mpsp_category[:mpsp_sub_categories] = mpsp_sub_categories
          response = {
            payload: {
              category: mpsp_category
            },
            response: RESPONSE_CODES_UTIL::SUCCESS
          }
        else
          marketplace_selling_packs = MARKETPLACE_SELLING_PACK_MAPPER.map_marketplace_selling_packs_to_array(mpsp_category.marketplace_selling_packs)
          mpsp_sub_category = MPSP_CATEGORY_SERVICE_HELPER.create_mpsp_sub_category_hash(mpsp_category)
          mpsp_sub_category[:marketplace_selling_packs] = marketplace_selling_packs
          response = {
            payload: {
              sub_category: mpsp_sub_category
            },
            response: RESPONSE_CODES_UTIL::SUCCESS
          }
        end
        return response
      end

      # 
      # Send Bad-Request response code back to user
      # 
      def self.create_response_mpsp_category_not_found_error(_message = '')
        response = {
          error: { message: CONTENT_UTIL::CATEGORY_NOT_PRESENT },
          response: RESPONSE_CODES_UTIL::BAD_REQUEST
        }
        return response
      end
    end
  end
end

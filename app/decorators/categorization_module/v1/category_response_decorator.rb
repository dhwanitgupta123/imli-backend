#
# Module handles all the functionality related to Categorization of products
# 
module CategorizationModule
  #
  # Categorization module Version 1
  # 
  module V1 
    #
    # Class to generate CategoryService responses & other functionalities
    # 
    # @author [anuj]
    #
    class CategoryResponseDecorator < BaseModule::V1::BaseResponseDecorator

      #
      #defining global variable with versioning
      # 
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      CATEGORY_SERVICE_HELPER = CategorizationModule::V1::CategoryServiceHelper
      BRAND_PACK_MAPPER = MasterProductModule::V1::BrandPackMapper

      # 
      # Send ok response code when category is created successfully
      # 
      def self.create_ok_category_response(category)
        category = create_category_hash(category)
        response = {
          payload: {
            category: category
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Send ok response code when sub category is created successfully
      # 
      def self.create_ok_sub_category_response(sub_category)
        sub_category = create_sub_category_hash(sub_category)
        response = {
          payload: {
            sub_category: sub_category
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Create response with a list of all the categories
      # 
      def self.create_all_categories_response(args)
        categories = CATEGORY_SERVICE_HELPER.get_categories_hash(args[:categories])
        response = {
          payload: {
            categories: categories
          },
          meta: {
            page_count: args[:page_count]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Create response with a list of all the sub categories
      # 
      def self.create_all_sub_categories_response(args)
        sub_categories = CATEGORY_SERVICE_HELPER.get_sub_categories_hash(args[:sub_categories])
        response = {
          payload: {
            sub_categories: sub_categories
          },
          meta: {
            page_count: args[:page_count]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Create response of a category with all the sub categories if Department is present
      # Create response of sub category if department is not present
      # 
      def self.create_category_details_response(category)
        if category.department.present?
          sub_categories_hash = CATEGORY_SERVICE_HELPER.get_sub_categories_hash(category.sub_categories)
          category_hash = create_category_hash(category)
          category_hash = category_hash.merge({
            sub_categories: sub_categories_hash
            })

          response = {
            payload: {
              category: category_hash
            },
            response: RESPONSE_CODES_UTIL::SUCCESS
          }
        else
          sub_category_hash = create_sub_category_hash(category)

          response = {
            payload: {
              sub_category: sub_category_hash
            },
            response: RESPONSE_CODES_UTIL::SUCCESS
          }
        end
        return response
      end

      # 
      # Send Bad-Request response code back to user
      # 
      def self.create_response_category_not_found_error(_message = '')
        response = {
          error: { message: CONTENT_UTIL::CATEGORY_NOT_PRESENT },
          response: RESPONSE_CODES_UTIL::BAD_REQUEST
        }
        return response
      end

      # 
      # create hash of category object attributes
      # 
      def self.create_category_hash(category)
        response = {
          id: category.id,
          department_id: category.department_id,
          label: category.label,
          image_url: category.image_url,
          description: category.description,
          priority: category.priority,
          status: category.status
        }
        return response
      end

      #
      # create hash of sub category object attributes
      # 
      def self.create_sub_category_hash(sub_category)
        response = {
          id: sub_category.id,
          category_id: sub_category.parent_category_id,
          label: sub_category.label,
          image_url: sub_category.image_url,
          description: sub_category.description,
          priority: sub_category.priority,
          status: sub_category.status
        }
        return response
      end
    end
  end
end

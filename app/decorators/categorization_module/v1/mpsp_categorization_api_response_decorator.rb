module CategorizationModule
  module V1
    #
    # class to generate DepartmentService responses & other functionalities
    #
    # @author [kartik]
    #
    class MpspCategorizationApiResponseDecorator < BaseModule::V1::BaseResponseDecorator
      #
      #defining global variable with versioning
      #
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      MPSP_CATEGORIZATION_API_SERVICE_HELPER = CategorizationModule::V1::MpspCategorizationApiHelper
      MPSP_CATEGORY_SERVICE_HELPER = CategorizationModule::V1::MpspCategoryServiceHelper
      MARKETPLACE_SELLING_PACK_HELPER = MarketplaceProductModule::V1::MarketplaceSellingPackMapper
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper
      LADDER = MarketplacePricingModule::V1::MarketplaceSellingPackLadderPricingMapper

      # 
      # Send ok response code when mpsp_departments with details are fetched successfully
      #
      def self.create_mpsp_department_details_response(args)
        mpsp_departments = MPSP_CATEGORIZATION_API_SERVICE_HELPER.get_mpsp_departments_hash(args[:mpsp_departments])
        response = {
          payload: {
            departments: mpsp_departments
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: args[:page_count]
          }
        }
        return response
      end

      def self.create_products_and_filters_response(mpsp_sub_categories, products_array)
        mpsp_sub_categories_filter = MPSP_CATEGORY_SERVICE_HELPER.get_mpsp_sub_categories_hash(mpsp_sub_categories)
        display_products = get_display_product_array(products_array)
        #display_products = MARKETPLACE_SELLING_PACK_HELPER.map_marketplace_selling_packs_to_array(products_array)
        response = {
          payload: {
            filters: {
              categories: mpsp_sub_categories_filter
            },
            products: display_products
          }
        }
        return response
      end

      def self.get_display_product_array(products)
        return [] if products.blank?
        display_products_array = []
        products.each do |product|
          display_products_array.push(create_display_product_hash(product))
        end
        return display_products_array
      end

      def self.create_display_product_hash(mpsp)
        display_product = {}
        display_product['id'] = mpsp.id
        # Fallback criteria: Search for display_name, description, images and display_pack_size from MPSP. If not found, then
        # fallback over Brand-Pack
        display_product['display_name'] = mpsp[:display_name]
        display_product['description'] = mpsp[:description].to_s
        display_product['display_pack_size'] = mpsp[:display_pack_size]
        display_product['max_quantity'] = mpsp[:max_quantity]
        display_product['dirty_bit'] = mpsp[:dirty_bit]
        
        # Temporarily keeping image fetching in brgin-rescue block
        begin
          images = mpsp.images
        rescue => e
          images = []
        end
        display_product['images'] = IMAGE_SERVICE_HELPER.get_images_hash_by_ids(images)

        display_product['is_on_sale'] = mpsp.is_on_sale
        display_product['is_ladder_pricing_active'] = mpsp.is_ladder_pricing_active

        mpsp_pricing = mpsp.marketplace_selling_pack_pricing
        display_product_pricing = {}
        display_product_pricing['mrp'] = mpsp_pricing.mrp
        display_product_pricing['base_selling_price'] = mpsp_pricing.base_selling_price
        display_product_pricing['savings'] = mpsp_pricing.savings
        display_product_pricing['discount'] = mpsp_pricing.discount


        mpsp_ladder_pricings = mpsp_pricing.marketplace_selling_pack_ladder_pricings
        # Populate ladder pricing if present
        if mpsp.is_ladder_pricing_active
          # Get sorted ladder only upto max quantity
          display_product_ladder = LADDER.map_marketplace_selling_pack_ladder_pricing_to_hash(mpsp_ladder_pricings, mpsp[:max_quantity])

          # display_product_ladder = []
          # mpsp_ladder_pricings.each do |mpsp_ladder|
          #   ladder = {}
          #   ladder[:quantity] = mpsp_ladder[:quantity]
          #   ladder[:selling_price] = mpsp_ladder[:selling_price]
          #   ladder[:additional_savings] = mpsp_ladder[:additional_savings]
          #   display_product_ladder.push(ladder)
          # end

          display_product_pricing['ladder_pricing'] = display_product_ladder
        end
        display_product['pricing'] = display_product_pricing
        return display_product
      end

    end
  end
end
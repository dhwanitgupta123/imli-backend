module UsersModule
  module V1
    #
    # Class to create JSON response of Profile
    #
    class ProfileResponseDecorator < BaseModule::V1::BaseResponseDecorator

      # 
      # Function to create hash of profile
      #
      def self.get_profile_hash(profile)
        return {} if profile.blank?
        response = {
          savings: profile.savings || '',
          pincode: profile.pincode || ''
        }
        return response
      end
    end
  end
end

module UsersModule
  module V1
    #
    # Class to create JSON response of membership
    #
    class MembershipResponseDecorator < BaseModule::V1::BaseResponseDecorator

      MEMBERSHIP_STATE = UsersModule::V1::ModelStates::MembershipStates
      PLAN_HELPER = CommonModule::V1::PlanHelper
      MEMBERSHIP_PAYMENT = UsersModule::V1::MembershipModule::V1::MembershipPaymentHelper

      # 
      # Function to create  hash of active/pending membership of a user
      #
      def self.get_user_membership_hash(memberships)
        membership_array = []
        return membership_array if memberships.blank?
        memberships = memberships.sort{|a,b| a.workflow_state <=> b.workflow_state}
        memberships.each do |membership|
          if membership.pending? || membership.active? || membership.payment_pending?
            membership_array.push(get_membership_hash(membership))
          end
        end
        return membership_array
      end

      # 
      # function to create hash of membership object
      #
      def self.get_membership_hash(membership)
        return {} if membership.blank?
        if membership.plan.present?
          plan = PLAN_HELPER.get_plan_details(membership.plan)
        end
        if membership.membership_payment.present?
          payment = MEMBERSHIP_PAYMENT.get_payment_details(membership.membership_payment)
        end
        response = {
          id: membership.id,
          starts_at: membership.starts_at.to_i || '',
          expires_at: membership.expires_at.to_i || '',
          status: membership.workflow_state || '',
          quantity: membership.quantity || 0,
          membership_payment: payment || {},
          plan: plan || {}
        }
        return response
      end
    end
  end
end

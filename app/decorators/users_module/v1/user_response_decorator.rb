module UsersModule
  module V1
    class UserResponseDecorator < BaseModule::V1::BaseResponseDecorator
      # Define utils globally with versioning
      # Kept them as global constants, so that they are specific
      # to this class only
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper
      ADDRESS_HELPER = AddressModule::V1::AddressHelper
      PROFILE_DECORATOR = UsersModule::V1::ProfileResponseDecorator
      EMAIL_DECORATOR = UsersModule::V1::EmailDecorator
      MEMBERSHIP_DECORATOR = UsersModule::V1::MembershipResponseDecorator
      ORDER_DAO = MarketplaceOrdersModule::V1::OrderDao
      USER_EVENTS = UsersModule::V1::ModelStates::UserEvents

      # 
      # Send ok response code back to new user or new provider with phone_number
      # 
      def self.create_ok_response(user)
        response = {
          payload: {
            user: {
              id: user.id,
              phone_number: user.phone_number,
              status: user.workflow_state
            }
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # Send ok response, with ampe credit information about user
      # 
      def self.create_ample_credit_response(ample_credit)
        {
          payload: {
            ample_credit: ample_credit
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      # 
      # Send ok response code to user when activated
      # 
      def self.create_active_user_status_response(user)

        addresses = ADDRESS_HELPER.get_active_addresses_hash(user.addresses)
        emails = EMAIL_DECORATOR.get_customer_type_email_hash(user.emails)
        memberships = MEMBERSHIP_DECORATOR.get_user_membership_hash(user.memberships)
        profile = PROFILE_DECORATOR.get_profile_hash(user.profile)
        order_dao = ORDER_DAO.new
        is_first_order = order_dao.is_first_order_of_user?(user)
        is_password_set = USER_SERVICE_HELPER.is_password_set?(user)
        is_first_time_password = USER_SERVICE_HELPER.is_first_time_password?(user)

        response = {
          payload: {
            user: {
              id: user.id,
              session_token: user.provider.session_token,
              is_first_order: is_first_order,
              is_password_set: is_password_set,
              is_first_time_password: is_first_time_password,
              first_name: user.first_name || '',
              last_name: user.last_name || '',
              phone_number: user.phone_number || '',
              referral_code: user.referral_code || '',
              referral_limit: user.referral_limit || '',
              benefit_limit: user.benefit_limit,
              status: user.workflow_state || '',
              addresses: addresses || [],
              emails: emails || [],
              memberships: memberships || [],
              profile: profile || ''
            }
          },
          :response => RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      def self.create_user_membership_payment_details_response(user, payment_mode_details = {})
        memberships = MEMBERSHIP_DECORATOR.get_user_membership_hash(user.memberships)
        payment_details = 
        response = {
          payload: {
            user: {
              id: user.id,
              memberships: memberships || []
            },
            payment_details: payment_mode_details
          },
          :response => RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      def self.create_user_membership_response(user)
        memberships = MEMBERSHIP_DECORATOR.get_user_membership_hash(user.memberships)
        payment_details = 
        response = {
          payload: {
            user: {
              id: user.id,
              memberships: memberships || []
            }
          },
          :response => RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Send ok response code back to user with current state
      # 
      def self.create_user_status_response(user)
        order_dao = ORDER_DAO.new
        is_first_order = order_dao.is_first_order_of_user?(user)
        is_password_set = USER_SERVICE_HELPER.is_password_set?(user)
        is_first_time_password = USER_SERVICE_HELPER.is_first_time_password?(user)
        response = {
          payload: {
            user: {
              id: user.id,
              status: user.workflow_state,
              is_first_order: is_first_order,
              is_password_set: is_password_set,
              is_first_time_password: is_first_time_password,
              referral_code: user.referral_code,
              referral_limit: user.referral_limit
            }
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Send ok response code back to new user with phone_number & session_token
      # 
      def self.create_session_token_response(user)
        order_dao = ORDER_DAO.new
        is_first_order = order_dao.is_first_order_of_user?(user)
        is_password_set = USER_SERVICE_HELPER.is_password_set?(user)
        is_first_time_password = USER_SERVICE_HELPER.is_first_time_password?(user)
        response = {
          payload: {
            user: {
              id: user.id,
              session_token: user.provider.session_token,
              is_first_order: is_first_order,
              is_password_set: is_password_set,
              is_first_time_password: is_first_time_password,
              status: user.workflow_state,
              referral_code: user.referral_code,
              referral_limit: user.referral_limit
            }
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Send ok response code back to new user with phone_number & session_token
      # 
      def self.create_response_email_already_verified(message = '')
        message = message.present? ? message : CONTENT_UTIL::EMAIL_ALREADY_VERIFIED
        response = {
          payload: { message: message },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Send Bad-Request response code back to user
      # 
      def self.create_wrong_otp_request(message = '')
        message = message.present? ? message : CONTENT_UTIL::WRONG_OTP
        response = {
          error: { message: message },
          response: RESPONSE_CODES_UTIL::BAD_REQUEST
        }
        return response
      end

      # 
      # Send Bad-Request response code back to user
      # 
      def self.create_wrong_referral_code_request(message = '')
        message = message.present? ? message : CONTENT_UTIL::WRONG_REFERRAL_CODE
        response = {
          error: { message: message },
          response: RESPONSE_CODES_UTIL::BAD_REQUEST
        }
        return response
      end

      # 
      # Send Bad-Request response code back to user
      # 
      def self.create_wrong_password_request(message = '')
        message = message.present? ? message : CONTENT_UTIL::WRONG_PASSWORD
        response = {
          error: { message: message},
          response: RESPONSE_CODES_UTIL::BAD_REQUEST
        }
        return response
      end

      #
      # Send HTTP-UnAuthorized response code back to user
      # 
      def self.create_exisiting_user_error(message = '')
        message = message.present? ? message : CONTENT_UTIL::EXISTING_USER
        response = {
          error: { message: message },
          response: RESPONSE_CODES_UTIL::UN_AUTHORIZED
        }
        return response
      end

      # 
      # Send HTTP-UnAuthorized (unauthenticated) response code back to user
      # 
      def self.create_response_unauthenticated_user(message = '')
        message = message.present? ? message : CONTENT_UTIL::UNAUTHENTIC_USER
        response = {
          error: { message: message },
          response: RESPONSE_CODES_UTIL::UN_AUTHORIZED
        }
        return response
      end

      #
      # send response email token sent successfully to user via different medium
      #
      def self.create_response_token_email_sent_successfully
        response = {
          payload: { message: CONTENT_UTIL::EMAIL_SENT_SUCCESS },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # send blocked user response to user, giving time remaining to get unblocked
      # 
      # @param time_remaining [String] time till than user is blocked
      # 
      # @return [hash] response consisting of BLOCKED_USER response
      # 
      def self.create_blocked_user_response(time_remaining)
        response = {
          error: { message: CONTENT_UTIL::BLOCKED_USER % { time:time_remaining } },
          response: RESPONSE_CODES_UTIL::UN_AUTHORIZED
        }
        return response
      end

      #
      # send response containing users information
      #
      # @param users [Array] [Array of User objects]
      #
      def self.create_users_detail_response(users)
        users_array = USER_SERVICE_HELPER.get_users_array(users)
        response = {
          payload: {
            users: users_array
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      #
      # send response containing success of role addition
      #
      def self.create_role_added_response
        response = {
          payload: { message: CONTENT_UTIL::ROLE_ADDED },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # send response containing success of role removal from an user
      #
      def self.create_role_removed_response
        response = {
          payload: { message: CONTENT_UTIL::ROLE_REMOVED },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # Create response with a list of all the users
      #
      def self.create_all_users_paginate_response(args)
        users = USER_SERVICE_HELPER.get_all_users_details_array(args[:users])
        response = {
          payload: {
            users: users
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        if (args[:page_count])
          response[:meta] = {
            page_count: args[:page_count]
          }
        end
        return response
      end

      #
      # Create response for password creation request
      #
      def self.create_password_response(user)
        response = {
          payload: { message: 'Password updated successfully' },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # Create response with user details if present
      #
      def self.create_check_user_response(args)
        is_user = args[:is_user]
        user = args[:user]
        response = {
          payload: {
            is_user: is_user,
            user: get_user_details(user)
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # Function to create hash of user details but only with specific
      # relevant parameters
      #
      # @return [Hash] [Hash of all relevant User details]
      #
      def self.get_user_details(user)
        return {} if user.blank?
        return {
          id: user.id,
          first_name: user.first_name,
          last_name: user.last_name,
          phone_number: user.phone_number,
          status: user.workflow_state
        }
      end

      #
      # Function to fetch user details for ORDER details
      #
      # @param user [Object] [user for which details are to be fetched]
      # 
      # @return [JSON] [Hash of user details]
      #
      def self.get_user_details_for_order(user)
        return {} if user.blank?
        user_hash = get_user_details(user)
        primary_email = USER_SERVICE_HELPER.get_primary_mail(user)
        user_hash = user_hash.merge({
          email_id: primary_email.email_id
          })
        return user_hash
      end

      #
      # Function to create hash of user details but only with all
      # relevant parameters and allowed events
      #
      # @return [Hash] [Hash of all relevant User details]
      #
      def self.get_all_user_details(user)
        allowed_user_events = USER_EVENTS.get_allowed_events(user)
        return {} if user.blank?
        return {
          id: user.id,
          first_name: user.first_name,
          last_name: user.last_name,
          phone_number: user.phone_number,
          status: user.workflow_state,
          allowed_events: allowed_user_events
        }
      end

      # 
      # Response for expired referral code
      # 
      # @return [type] [description]
      def self.create_expired_referral_code_response(message = '')
        response = {
          error: { message: message },
          response: RESPONSE_CODES_UTIL::BAD_REQUEST
        }
        return response
      end

      # 
      # Fucntion to create success response
      # 
      def self.create_success_response(message = '')
        response = {
          payload: { message: message },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end
    end
  end
end

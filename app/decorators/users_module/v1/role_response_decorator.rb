module UsersModule
  module V1
    class RoleResponseDecorator < BaseModule::V1::BaseResponseDecorator
      # Define utils globally with versioning
      # Kept them as global constants, so that they are specific
      # to this class only
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      ROLE_SERVICE_HELPER = UsersModule::V1::RoleServiceHelper

      
      def self.create_ok_response(role)
        response = {
          payload: {
            role: get_role_details(role)
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      def self.create_response_user_roles(roles)
        response = {
          payload: {
            roles: ROLE_SERVICE_HELPER.get_roles_array(roles)
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      def self.create_response_roles_tree(roles)
        response = {
          payload: {
            roles: ROLE_SERVICE_HELPER.get_roles_tree_hash(roles)
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      def self.create_permissions_response(permissions = [])
        response = {
          payload: {
            permissions: permissions
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end


      def self.get_role_details(role)
        return {
          id: role.id,
          name: role.role_name,
          permissions: role.permissions
        }
      end
    end
  end
end
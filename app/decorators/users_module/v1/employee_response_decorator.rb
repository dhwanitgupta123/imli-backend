module UsersModule
  module V1
    class EmployeeResponseDecorator < BaseModule::V1::BaseResponseDecorator
      # Define utils globally with versioning
      # Kept them as global constants, so that they are specific
      # to this class only
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL_UTIL = CommonModule::V1::Content

      # 
      # Send ok response code back to new user or new provider with phone_number
      # 
      def self.create_ok_response_for_employee(user, employee)
        response = {
          :payload => {
            :employee => {
              :phone_number => user.phone_number,
              :first_name => user.first_name,
              :last_name => user.last_name,
              :session_token => user.provider.session_token,
              :team => user.employee.team,
              :designation => user.employee.designation,
              :manager => user.employee.manager,
              :status => user.employee.workflow_state
            }
          },
          :response => RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Send ok response code back to user with current state
      # 
      def self.create_employee_state_response(employee)
        response = {
          :payload => {
            :employee => {
              :status => employee.workflow_state
            }
          },
          :response => RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Send ok response code back to new user with phone_number & session_token
      # 
      def self.create_session_token_response(session_token, employee)
        response = {
          :payload => {
            :employee => {
              :session_token => session_token,
              :status => employee.workflow_state
            }
          },
          :response => RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end



      # 
      # Send Pre conditioned with state pending required response code back to user
      # 
      def self.create_response_still_pending_state(_message = '')
        response = {
          :error => {:message => CONTENT_UTIL::PENDING_STATE},
          :response => RESPONSE_CODES_UTIL::PRE_CONDITION_REQUIRED
        }
        return response
      end

      # 
      # Send Pre conditioned required with state inactive response code back to user
      # 
      def self.create_response_still_inactive_state(_message = '')
        response = {
          :error => {:message => CONTENT_UTIL::INACTIVE_STATE},
          :response => RESPONSE_CODES_UTIL::PRE_CONDITION_REQUIRED
        }
        return response
      end

      #
      # Send HTTP-UnAuthorized response code back to user
      # 
      def self.create_exisiting_employee_error(_message = '')
        response = {
          :error => {:message => CONTENT_UTIL::EXISTING_EMPLOYEE},
          :response => RESPONSE_CODES_UTIL::UN_AUTHORIZED
        }
        return response
      end

    end
  end
end
module CommonModule
  module V1
    class ProductSuggesterResponseDecorator < BaseModule::V1::BaseResponseDecorator
      # Define utils globally with versioning
      # Kept them as global constants, so that they are specific
      # to this class only
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      PRODUCT_SUGGESTER_HELPER = CommonModule::V1::ProductSuggesterHelper

      #
      # Create OK response
      #
      def self.create_product_suggestion_ok_response
        response = {
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Create response with a list of all the suggestions
      # 
      def self.create_all_suggestions_response(args)
        suggestions = PRODUCT_SUGGESTER_HELPER.get_product_suggestions_hash(args[:suggestions])
        response = {
          payload: {
            suggestions: suggestions
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: args[:page_count]
          }
        }
        return response
      end
    end
  end
end
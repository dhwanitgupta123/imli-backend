module CommonModule
  module V1
    class ConstantsResponseDecorator < BaseModule::V1::BaseResponseDecorator

      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes

      #
      # Create cluster constants response
      # 
      def self.create_cluster_constant_response(cluster_constants)
        return {
          payload: {
            constants: {
              cluster_constants: cluster_constants
            }    
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end
    end
  end
end

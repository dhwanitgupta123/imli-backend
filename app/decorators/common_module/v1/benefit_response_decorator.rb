module CommonModule
  module V1
    class BenefitResponseDecorator < BaseModule::V1::BaseResponseDecorator
      # Define utils globally with versioning
      # Kept them as global constants, so that they are specific
      # to this class only
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      BENEFIT_HELPER = CommonModule::V1::BenefitHelper

      #
      # Create OK response with BENEFIT hash
      #
      def self.create_response_benefit_hash(benefit)
        response = {
          payload: {
            benefit: BENEFIT_HELPER.get_benefit_details(benefit)
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # Create OK response with Array of BENEFITs
      #
      def self.create_response_benefits_array(benefits)
        response = {
          payload: {
            benefits: BENEFIT_HELPER.get_benefits_array(benefits)
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end


    end
  end
end
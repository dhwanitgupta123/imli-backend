#
# Module to handle functionalities related to transactions
#
module DescriptionsModule
  #
  # Version1 for transactions module
  #
  module V1

    class DescriptionResponseDecorator < BaseModule::V1::BaseResponseDecorator

      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes

      def self.create_description_response(description) 
        {
          payload: 
          {
            description: description
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end
    end
  end
end

#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    #
    # Create response for image service
    #
    class ImageServiceResponseDecorator < BaseModule::V1::BaseResponseDecorator
      # Define utils globally with versioning
      # Kept them as global constants, so that they are specific
      # to this class only
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes

      def self.create_image_response(images)
        {
          payload:
          {
            images: images
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end
    end
  end
end

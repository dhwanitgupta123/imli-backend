module AnalyzerModule
  module V1
    module UserAnalyzer
      module V1
        class UserActivityAnalyzer < AnalyzerModule::V1::ActivityAnalyzer

          # 
          # Initializing all the constants from the cache 
          # 
          def initialize
            super

            @cache_util = CommonModule::V1::Cache
            @content_util = CommonModule::V1::Content
            @custom_errors_util = CommonModule::V1::CustomErrors
            @general_helper = CommonModule::V1::GeneralHelper
            
            @last_x_login_requests = @cache_util.read_int('LAST_X_LOGIN_REQUESTS') || 3
                 
            @user_block_time_hours   = @cache_util.read_int('USER_BLOCK_TIME_HOUR') || 24
          
            @user_activity_track_min = @cache_util.read_int('USER_ACTIVITY_TRACK_MIN') || 10

            @valid_errors = [@content_util::AUTHENTICATION_TIMEOUT, @content_util::WRONG_PASSWORD, @content_util::WRONG_OTP] 
          end
      
          # 
          # Analyze the user activity, if it finds last three consecutive fails
          # user login request then it will block the user for  user_block_time_hours
          # 
          # @param phone_number [String] unique id for user
          # 
          def analyze(phone_number)

            begin
              
              current_user_activity = @activity_log.activities_in_last_x_time_with_api_name_by_order(
                {
                  :time => @user_activity_track_min.minutes,
                  :phone_number => phone_number,
                  :api_name => "users_api::login",
                  :order => "desc"
                }
              )

              if current_user_activity.present? && 
                current_user_activity.count >= @last_x_login_requests
                if has_previous_invalid_actvities?(current_user_activity)
                  block_user(phone_number)
                end
              end
            rescue @custom_errors_util::InsufficientDataError => e
              return false
            end
          end

          # 
          # Add user phone number as key to cache and value to current time
          # to indicate that user is blocked
          # 
          # @param phone_number [String] unique id for user
          # 
          def block_user(phone_number)
          
            key = @general_helper.get_blocked_user_key(phone_number)
            if @cache_util.read(key).blank?
              @cache_util.write({key: key, value: Time.zone.now})
              @cache_util.set_expire(key, @user_block_time_hours.hour.to_i)
            end
          end

          #  
          # Function returns if user need to be blocked or not
          # 
          # @param current_user_activity [Array] SearchResult
          # 
          # @return [Boolean] true if user need to be blocked else false
          # 
          def has_previous_invalid_actvities?(current_user_activity)
    
            (0..@last_x_login_requests-1).each do |x|
              if current_user_activity[x].response_error && 
                !check_for_valid_errors(current_user_activity[x].response_error)
                
                return false
              end
            end

            return true
          end

          # 
          # Check if response_error is present in valid error list
          # 
          # @param response_error [String] error string
          # 
          # @return [Boolean] true if present else false
          # 
          def check_for_valid_errors(response_error)
            @valid_errors.include? response_error
          end
        end
      end
    end
  end
end
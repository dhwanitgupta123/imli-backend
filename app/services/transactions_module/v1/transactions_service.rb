#
# Module to handle functionalities related to transactions
#
module TransactionsModule
  #
  # Version1 for transactions module
  #
  module V1
    #
    # This class is responsible to handle transaction related queries
    #
    class TransactionsService < BaseModule::V1::BaseService

      TRANSACTIONS_RESPONSE_DECORATOR = TransactionsModule::V1::TransactionsResponseDecorator
      TRANSACTIONS_DAO = TransactionsModule::V1::TransactionsDao
      TRANSACTION_STATUS = TransactionsModule::V1::TransactionStatus
      PAYMENT_TYPES = TransactionsModule::V1::PaymentTypes
      USER_SERVICE = UsersModule::V1::UserService
      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper
      SENTRY_LOGGER = CommonModule::V1::Logger
      CONTENT_UTIL = CommonModule::V1::Content
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      def initialize(params)
        @params = params
        @transaction_dao = TRANSACTIONS_DAO.new(@params)
        @user_service = USER_SERVICE.new(@params)
        @currency = "INR"
      end

      def get_transaction_status(request)
        if request.blank? || request[:id].blank?
          return TRANSACTIONS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(
            CONTENT_UTIL::INVALID_TRANSACTION_ID)
        end

        begin
          transaction = @transaction_dao.get_transaction_by_id(request[:id])
          TRANSACTIONS_RESPONSE_DECORATOR.create_transaction_status_response(transaction.status)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError
          TRANSACTIONS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(
            CONTENT_UTIL::INVALID_TRANSACTION_ID)
        end
      end
    end
  end
end

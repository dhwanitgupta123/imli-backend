module TransactionsModule
  module V1
    class PaymentTransactionsService < BaseModule::V1::BaseService

      CONTENT = CommonModule::V1::Content
      PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR = TransactionsModule::V1::PaymentTransactionsResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      GATEWAY_TYPE = TransactionsModule::V1::GatewayType
      CITRUS_PAYMENT_SERVICE = PaymentModule::V1::CitrusPaymentService
      RAZORPAY_PAYMENT_SERVICE = PaymentModule::V1::RazorpayPaymentService
      POST_ORDER_PAYMENT_SERVICE = PaymentModule::V1::PostOrderPaymentService
      TRANSACTION_STATUS = TransactionsModule::V1::TransactionStatus
      TRANSACTION_SERVICE = TransactionsModule::V1::TransactionsService

      def initialize(params = '')
        @params = params
      end

      #
      # API function to inititiate trasaction
      #
      def initiate_transaction(initiate_transaction_params)
        begin
          validate_initiate_request(initiate_transaction_params)
          return enact_initiation(initiate_transaction_params)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
          return PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        end
      end

      #
      # Service function to initiate transaction based on the gate way
      #
      def enact_initiation(initiate_transaction_params)
        if initiate_transaction_params[:gateway].to_i == GATEWAY_TYPE::CITRUS_WEB
          ApplicationLogger.info('Transaction initiated with Citrus web gateway')
          citrus_payment_service = CITRUS_PAYMENT_SERVICE.new(@params)
          response = citrus_payment_service.generate_url(initiate_transaction_params)
          return PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR.create_citrus_response(response)
        elsif initiate_transaction_params[:gateway].to_i == GATEWAY_TYPE::RAZORPAY
          ApplicationLogger.info('Transaction initiated with Razorpay web gateway')
          razorpay_payment_service = RAZORPAY_PAYMENT_SERVICE.new(@params)
          return razorpay_payment_service.initiate_transaction(initiate_transaction_params)
        else
          return PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(CONTENT::WRONG_GATEWAY)
        end
      end

      #
      # API function to update trasaction
      #
      def update_transaction(update_transaction_params)
        begin
          validate_update_request(update_transaction_params)
          return enact_update(update_transaction_params)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      #
      # Service function to update transaction based on the gate way
      #
      def enact_update(update_transaction_params)
        if update_transaction_params[:gateway].to_i == GATEWAY_TYPE::CITRUS_WEB
          citrus_payment_service = CITRUS_PAYMENT_SERVICE.new(@params)
          ApplicationLogger.info('Transaction updated with Citrus web gateway')
          return citrus_payment_service.update_transaction(update_transaction_params)
        elsif update_transaction_params[:gateway].to_i == GATEWAY_TYPE::RAZORPAY
          ApplicationLogger.info('Transaction updated with Razorpay gateway')
          razorpay_payment_service = RAZORPAY_PAYMENT_SERVICE.new(@params)
          return razorpay_payment_service.update_transaction(update_transaction_params)
        else
          return PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(CONTENT::WRONG_GATEWAY)
        end
      end

      #
      # Service function to record transaction
      # Currently, supporting only POST_ORDER
      #
      # @param record_transaction_params [JSON] [Transaction hash specific to payment mode]
      #
      # @return [JSON] [Response to be sent to the user]
      #
      def record_transaction(record_transaction_params)
        ApplicationLogger.info('Transaction Recorded with POST_ORDER gateway')
        post_order_payment_service = POST_ORDER_PAYMENT_SERVICE.new(@params)
        return post_order_payment_service.record_post_order_transaction(record_transaction_params)
      end

      private

      #
      # Function to validate transation initiation request
      #
      def validate_initiate_request(initiate_transaction_params)
        error_array = []
        error_array.push('Payment ID') if initiate_transaction_params.blank? || initiate_transaction_params[:payment_id].blank?
        error_array.push('Payment type') if initiate_transaction_params[:payment_type].blank?
        error_array.push('Gateway') if initiate_transaction_params[:gateway].blank?
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::FIELD_MUST_PRESENT%{ field: error_string })
        end
      end

      #
      # Function to validate update transaction request
      #
      def validate_update_request(update_transaction_params)
        error_array = []
        error_array.push('Transaction ID') if update_transaction_params.blank? || update_transaction_params[:id].blank?
        error_array.push('Transaction status') if update_transaction_params[:status].blank?
        error_array.push('Gateway') if update_transaction_params[:gateway].blank?
        error_array.push('Razorpay details') if update_transaction_params[:gateway].to_i == GATEWAY_TYPE::RAZORPAY && update_transaction_params[:status].to_i == TRANSACTION_STATUS::SUCCESS && update_transaction_params[:razorpay].blank?
        error_array.push('Razorpay payment ID') if update_transaction_params[:gateway].to_i == GATEWAY_TYPE::RAZORPAY && update_transaction_params[:razorpay].present? && update_transaction_params[:status].to_i == TRANSACTION_STATUS::SUCCESS && update_transaction_params[:razorpay][:payment_id].blank?
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::FIELD_MUST_PRESENT%{ field: error_string })
        end
      end
    end
  end
end

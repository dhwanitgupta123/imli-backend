module PaymentModule
  module V1
    class PostOrderPaymentService < BaseModule::V1::BaseService

      TRANSACTIONS_DAO = TransactionsModule::V1::TransactionsDao
      USER_SERVICE = UsersModule::V1::UserService
      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper
      TRANSACTION_STATUS = TransactionsModule::V1::TransactionStatus
      PAYMENT_TYPES = TransactionsModule::V1::PaymentTypes
      SENTRY_LOGGER = CommonModule::V1::Logger
      TRANSACTIONS_RESPONSE_DECORATOR = TransactionsModule::V1::TransactionsResponseDecorator

      def initialize(params)
        @params = params
        @user_service = USER_SERVICE.new(@params)
        @transaction_dao = TRANSACTIONS_DAO.new
        @currency = "INR"
      end

      #
      # Function to record transaction when gateway is Card on Delivery
      # This function will create a TRANSACTION Object with given payment attributes
      # and then call corresponding payment handler to handle post_order payment
      #
      def record_post_order_transaction(request)
        payment = get_payment(request[:payment_id], request[:payment_type])
        transaction_status = request[:status]
        transaction_args = create_transaction_args(payment, request[:payment_type], request[:gateway])
        transaction = @transaction_dao.create_transaction(transaction_args.merge(transaction_data: request))
        ApplicationLogger.debug('COD Transaction recorded, id: ' + transaction.id.to_s + ' for payment id: ' + payment.id.to_s + ' with COD reference id: ' + request[:post_order][:reference_id] + ' and mode: ' + request[:post_order][:payment_mode] + ' by user: ' + transaction.user_id.to_s)
        post_order_transaction_params = {
          transaction: transaction,
          transaction_status: request[:status],
          mode: request[:post_order][:payment_mode]
        }
        response = handle_post_order_payment(post_order_transaction_params)
        update_transaction_args = {
          status: transaction_status
        }
        update_transaction_args['payment_id'] = response[:payment].id if payment.id != response[:payment].id

        @transaction_dao.update_transaction(update_transaction_args, transaction)
        return TRANSACTIONS_RESPONSE_DECORATOR.create_transaction_status_response(transaction_status)
      end

      #
      # Handle COD payment transaction. Call corresponding payment handler to handle
      # COD payment.
      #
      # @param post_order_transaction_params [JSON] [Hash containing transaction and its status]
      #
      def handle_post_order_payment(post_order_transaction_params)
        transaction = post_order_transaction_params[:transaction]

        begin
          ApplicationLogger.debug('Transaction completed: ' + transaction.id.to_s + ' for type: ' + transaction.payment_type.to_s)
          payment_handler = PAYMENT_TYPES.get_payment_handler_from_payment_type(transaction.payment_type)
          payment_handler_class = payment_handler.new(@params)
          response = payment_handler_class.handle_post_order_payment(post_order_transaction_params[:transaction_status], { payment_id: transaction.payment_id, mode: post_order_transaction_params[:mode] })
          return response
        rescue => e
          SENTRY_LOGGER.log_exception(e)
          ApplicationLogger.debug('Unable to update the payment for  transaction id ' + transaction.id.to_s + ' , ' + e.message)
          raise CUSTOM_ERROR_UTIL::RunTimeError.new('Unable to record transaction')
        end
      end

      #
      # Function to fetch payment object corresponding to the payment type
      #
      def get_payment(payment_id, payment_type)
        payment_dao = PAYMENT_TYPES.get_payment_dao_from_payment_type(payment_type)
        payment_dao_class = payment_dao.new
        return payment_dao_class.get_payment_from_id(payment_id)
      end

      #
      # Function to create args for transaction
      #
      def create_transaction_args(payment, payment_type, gateway)
        # For COD, we are storing user id of the User who is making this request (Panel user/delivery boy's app user)
        user = @user_service.get_user_from_user_id(USER_SESSION[:user_id])
        user_emails = USER_SERVICE_HELPER.get_primary_mail(user)
        primary_email = user_emails.present? ? user_emails.email_id : nil
        
        # TODO: We should call handler.get_total_payable_amount(payment_id) 
        amount = payment.net_total.round(2).to_f
        
        return {
          payment_id: payment.id,
          payment_type: payment_type,
          amount: amount,
          user_email_id: primary_email,
          user_id: user.id,
          currency: @currency,
          gateway: gateway
        }
      end

    end
  end
end

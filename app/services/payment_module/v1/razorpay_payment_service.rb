module PaymentModule
  module V1
    class RazorpayPaymentService

      RAZORPAY_PAYMENT_RESPONSE_DECORATOR = PaymentModule::V1::RazorpayPaymentResponseDecorator
      TRANSACTIONS_DAO = TransactionsModule::V1::TransactionsDao
      USER_SERVICE = UsersModule::V1::UserService
      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper
      TRANSACTION_STATUS = TransactionsModule::V1::TransactionStatus
      PAYMENT_TYPES = TransactionsModule::V1::PaymentTypes
      SENTRY_LOGGER = CommonModule::V1::Logger
      TRANSACTIONS_RESPONSE_DECORATOR = TransactionsModule::V1::TransactionsResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      def initialize(params)
        @params = params
        @user_service = USER_SERVICE.new(@params)
        @transactions_dao = TRANSACTIONS_DAO.new
        @currency = "INR";
      end

      #
      # function to initiate the transaction
      #
      def initiate_transaction(request)
        payment = get_payment(request[:payment_id], request[:payment_type])
        transaction_args = create_transaction_args(payment, request[:payment_type])
        transaction = @transactions_dao.create_transaction(transaction_args)
        ApplicationLogger.debug('Transaction initiated, id: ' + transaction.id.to_s + ' for payment id: ' + payment.id.to_s)
        return RAZORPAY_PAYMENT_RESPONSE_DECORATOR.create_ok_response(
          {
            transaction_id: transaction.id.to_s
          }
        )
      end

      #
      # Function to update the transaction based on the response from the gateway
      # Update payment object based on the response from the gateway
      #
      def update_transaction(request)
        ApplicationLogger.info('UpdateTransaction request: ' + request.to_s)
        transaction = @transactions_dao.get_transaction_by_id(request[:id])
        payment_handler = PAYMENT_TYPES.get_payment_handler_from_payment_type(transaction.payment_type)
        payment_handler_class = payment_handler.new(@params)
        transaction_status = request[:status].to_i
        if request[:status].to_i ==  TRANSACTION_STATUS::SUCCESS
          ApplicationLogger.debug('Payment capture process started for Transaction id: ' + transaction.id.to_s + ' Razorpay Payment ID is: ' + request[:razorpay][:payment_id])
          amount_in_paise = (transaction.amount * 100).to_i
          razorpay_response = capture_razorpay_amount(request[:razorpay][:payment_id], amount_in_paise)
          response = razorpay_response[:response]
          transaction_status = razorpay_response[:transaction_status]
          request = request.merge({ razorpay: response.to_json })
        end
        begin
          ApplicationLogger.debug('Transaction completed: ' + transaction.id.to_s + ' for type: ' + transaction.payment_type.to_s)
          payment_handler_class.handle_payment(transaction_status, { payment_id: transaction.payment_id, user_id: transaction.user_id })
          @transactions_dao.update_transaction({status: transaction_status, transaction_data: request}, transaction)
        rescue => e
          SENTRY_LOGGER.log_exception(e)
          ApplicationLogger.debug('Unable to update the payment for  transaction id ' + transaction.id.to_s + ' , ' + e.message)
        end
        return TRANSACTIONS_RESPONSE_DECORATOR.create_transaction_status_response(transaction_status)
      end

      #
      # Function to fetch payment object corresponding to the payment type
      #
      def get_payment(payment_id, payment_type)
        payment_dao = PAYMENT_TYPES.get_payment_dao_from_payment_type(payment_type)
        payment_dao_class = payment_dao.new
        return payment_dao_class.get_payment_from_id(payment_id)
      end

      #
      # Function to create args for transaction
      #
      def create_transaction_args(payment, payment_type)
        user = @user_service.get_user_from_user_id(USER_SESSION[:user_id])
        user_emails = USER_SERVICE_HELPER.get_primary_mail(user)
        primary_email = user_emails.email_id
        
        # TODO: We should call handler.get_total_payable_amount(payment_id) 
        amount = payment.net_total.round(2).to_f
        
        return {
          payment_id: payment.id,
          payment_type: payment_type,
          amount: amount,
          user_email_id: primary_email,
          user_id: user.id,
          currency: @currency,
          gateway: TransactionsModule::V1::GatewayType::RAZORPAY
        }
      end

      #
      # Function to capure payment of razorpay
      #
      def capture_razorpay_amount(payment_id, amount_in_paise)
        begin
          response = Razorpay::Payment.fetch(payment_id).capture({ amount: amount_in_paise.to_s })
          ApplicationLogger.debug('Payment captured for payment id: ' + payment_id + ' with following response: ' + response.to_json)
          case response.status
          when 'captured'
            transaction_status = TRANSACTION_STATUS::SUCCESS
          else
            transaction_status = TRANSACTION_STATUS::FAILED
          end
        rescue Razorpay::BadRequestError => e
          ApplicationLogger.debug('Razorpay::BadRequestError ' + e.message)
          transaction_status = TRANSACTION_STATUS::FAILED
        end
        return { transaction_status: transaction_status, response: response }
      end
    end
  end
end

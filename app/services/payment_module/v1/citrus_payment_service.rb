module PaymentModule
  module V1
    class CitrusPaymentService

      CITRUS_PAYMENT_UTILS = PaymentModule::V1::CitrusPaymentUtils
      CITRUS_PAYMENT_RESPONSE_DECORATOR = PaymentModule::V1::CitrusPaymentResponseDecorator
      TRANSACTION_MODEL = TransactionsModule::V1::Transaction
      TRANSACTIONS_DAO = TransactionsModule::V1::TransactionsDao
      USER_SERVICE = UsersModule::V1::UserService
      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper
      TRANSACTION_STATUS = TransactionsModule::V1::TransactionStatus
      PAYMENT_TYPES = TransactionsModule::V1::PaymentTypes
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      SENTRY_LOGGER = CommonModule::V1::Logger
      TRANSACTIONS_RESPONSE_DECORATOR = TransactionsModule::V1::TransactionsResponseDecorator
      CONTENT_UTIL = CommonModule::V1::Content


      def initialize(params)
        @params = params
        @user_service = USER_SERVICE.new(@params)
        @citrus_payment_utils_class = CITRUS_PAYMENT_UTILS.new
        @transactions_dao = TRANSACTIONS_DAO.new
        @secret_key = APP_CONFIG['config']['CITRUS_SECRET_KEY']
        @access_key = APP_CONFIG['config']['CITRUS_ACCESS_KEY']
        @vanityUrl = APP_CONFIG['config']['CITRUS_VANITY_URL']
        @return_url = APP_CONFIG['config']['CITRUS_RETURN_URL']
        @web_view_url = APP_CONFIG['config']['CITRUS_WEB_VIEW']
        @citrus_url = APP_CONFIG['config']['CITRUS_BASE_URL'] + @vanityUrl
        # double check to make sure in production orignal amount should be taken
        @max_transaction_amount = APP_CONFIG['config']['MAX_TRANSACTION_AMOUNT'] if Rails.env != 'production'
        @currency = "INR";
      end

      def generate_url(request)

        begin
          validate_request(request)
  
          payment = get_payment(request[:payment_id], request[:payment_type])
          
          transaction_args = create_transaction_args(payment, request[:payment_type])

          transaction = @transactions_dao.create_transaction(transaction_args)
        
          merchantTxnId = transaction.id.to_s  
          orderAmount = transaction.amount
        
          data = @vanityUrl + orderAmount.to_s + merchantTxnId + @currency

          securitySignature = @citrus_payment_utils_class.hmac_sha1(data, @secret_key)
        
          transaction = @transactions_dao.update_transaction({signature: securitySignature}, transaction)
          # Log Transaction initiation just before returning final response
          ApplicationLogger.debug('Transaction initiated, id: ' + transaction.id.to_s + ' for payment id: ' + payment.id.to_s)
          return CITRUS_PAYMENT_RESPONSE_DECORATOR.create_ok_response({
              url: @web_view_url,
              transaction_id: transaction.id.to_s,
              payment_status: payment.status
            })
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            return CITRUS_PAYMENT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
          rescue CUSTOM_ERROR_UTIL::InsufficientDataError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
            return CITRUS_PAYMENT_RESPONSE_DECORATOR.create_response_bad_request(e.message)
          end
      end

      def get_payment(payment_id, payment_type)
        payment_dao = PAYMENT_TYPES.get_payment_dao_from_payment_type(payment_type)
        payment_dao_class = payment_dao.new
        return payment_dao_class.get_payment_from_id(payment_id)
      end

      def create_transaction_args(payment, payment_type)
        user = @user_service.get_user_from_user_id(USER_SESSION[:user_id])
        user_emails = USER_SERVICE_HELPER.get_primary_mail(user)
        primary_email = user_emails.email_id
        
        # TODO: We should call handler.get_total_payable_amount(payment_id) 
        amount = payment.net_total.to_f

        if @max_transaction_amount.present?
          amount = @max_transaction_amount.to_f
        end
        
        return {
          payment_id: payment.id,
          payment_type: payment_type,
          amount: amount,
          user_email_id: primary_email,
          user_id: user.id,
          currency: @currency,
          gateway: TransactionsModule::V1::GatewayType::CITRUS_WEB
        }
      end

      def get_web_view(request)
        transaction_id = request[:transaction_id]
        transaction = @transactions_dao.get_transaction_by_id(transaction_id)
        return { 
          transaction: transaction,
          citrus_url: @citrus_url, 
          return_url: @return_url
        }
      end

      def get_transaction_info(request)
        verification_data = ''
             
        verification_data = verification_data + request[:TxId].to_s if request[:TxId].present? 
        verification_data = verification_data + request[:TxStatus].to_s if request[:TxStatus].present?
        verification_data = verification_data + request[:amount].to_s if request[:amount].present?
        verification_data = verification_data + request[:pgTxnNo].to_s if request[:pgTxnNo].present? 
        verification_data = verification_data + request[:issuerRefNo].to_s if request[:issuerRefNo].present?
        verification_data = verification_data + request[:authIdCode].to_s if request[:authIdCode].present? 
        verification_data = verification_data + request[:firstName].to_s if request[:firstName].present?
        verification_data = verification_data + request[:lastName].to_s if request[:lastName].present?
        verification_data = verification_data + request[:pgRespCode].to_s if request[:pgRespCode].present?
        verification_data = verification_data + request[:addressZip].to_s if request[:addressZip].present?        

        signature = @citrus_payment_utils_class.hmac_sha1(verification_data, @secret_key)

        transaction = @transactions_dao.get_transaction_by_id(request[:TxId])

        payment_handler = PAYMENT_TYPES.get_payment_handler_from_payment_type(transaction.payment_type)
        payment_handler_class = payment_handler.new(@params)

        transaction_status = TRANSACTION_STATUS::FAILED

        if signature == request[:signature]
          case request[:TxStatus].upcase
          when "SUCCESS"
            transaction_status = TRANSACTION_STATUS::SUCCESS
          when "FAIL"
            transaction_status = TRANSACTION_STATUS::FAILED
          when "CANCEL", "CANCELED"
            transaction_status = TRANSACTION_STATUS::CANCELLED
          end
          response = request
        else
          transaction_status = TRANSACTION_STATUS::FAILED
          response = {"Error" => "Transaction Failed","Reason" => "Signature Verification Failed"}
          ApplicationLogger.debug('Signature mismatch for ' + transaction.id.to_s)
        end

        begin
          ApplicationLogger.debug('Transaction completed: ' + transaction.id.to_s + ' for type: ' + transaction.payment_type.to_s)
          payment_handler_class.handle_payment(transaction_status, { payment_id: transaction.payment_id, user_id: transaction.user_id })
        rescue => e
          # We will not throw any ERROR, since Transaction was complete
          # and will let user decide his next step (retry transaction, pay offline, etc. etc.)
          SENTRY_LOGGER.log_exception(e)
          ApplicationLogger.debug('Unable to update the payment for  transaction id ' + transaction.id.to_s + ' , ' + e.message)
        end

        @transactions_dao.update_transaction({status: transaction_status, transaction_data: request}, transaction)

        return response
      end

      #
      # Function to update the transaction based on the response from the gateway
      # Update payment object based on the response from the gateway
      #
      def update_transaction(request)       
        transaction = @transactions_dao.get_transaction_by_id(request[:id])
        payment_handler = PAYMENT_TYPES.get_payment_handler_from_payment_type(transaction.payment_type)
        payment_handler_class = payment_handler.new(@params)
        if request[:status].to_i == TRANSACTION_STATUS::CANCELLED
          transaction_status = request[:status].to_i
          begin
            ApplicationLogger.debug('Transaction completed: ' + transaction.id.to_s + ' for type: ' + transaction.payment_type.to_s)
            payment_handler_class.handle_payment(transaction_status, { payment_id: transaction.payment_id, user_id: transaction.user_id })
            @transactions_dao.update_transaction({status: transaction_status, transaction_data: request}, transaction)
          rescue => e
            # We will not throw any ERROR, since Transaction was complete
            # and will let user decide his next step (retry transaction, pay offline, etc. etc.)
            SENTRY_LOGGER.log_exception(e)
            ApplicationLogger.debug('Unable to update the payment for  transaction id ' + transaction.id.to_s + ' , ' + e.message)
          end
          return TRANSACTIONS_RESPONSE_DECORATOR.create_transaction_status_response(transaction_status)
        end
        return TRANSACTIONS_RESPONSE_DECORATOR.create_response_invalid_data_passed(CONTENT_UTIL::INVALID_TRANSACTION_STATUS)
      end

      private

      # 
      # Validate incoming request
      #
      # @param request [JSON] [Hash containing cart id and address id ]
      #
      # @raise [InsufficientDataError] [if cart id or address id not present]
      #
      def validate_request(request)
        error_array = []
        error_array.push('Payment id') if request[:payment_id].blank?
        error_array.push('Payment type') if request[:payment_type].blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end
    end
  end
end

module CreditsModule
  module V1
    class AmpleCreditService < BaseModule::V1::BaseService

      def initialize(params = '')
        @params = params
        @ample_credit_dao = CreditsModule::V1::AmpleCreditDao.new(params)
      end

      # 
      # This function credits the amount to ample_credit
      #
      # @param args [Hash] consist of ample_credit and amount to add
      #
      def credit(args)
        ample_credit = args[:ample_credit] 
        amount_to_credit = args[:amount]

        current_balance = ample_credit.balance

        ApplicationLogger.debug('Initiated credit to ample_credit id ' + ample_credit.id.to_s + ' with amount ' + amount_to_credit.to_s)
        
        ample_credit = @ample_credit_dao.update({
            balance: current_balance + amount_to_credit 
          }, ample_credit)

        ApplicationLogger.debug('Credited successfully')

        return ample_credit
      end

      # 
      # This function debit the amount to ample_credit
      #
      # @param args [Hash] consist of ample_credit and amount to substract
      #
      def debit(args)
        ample_credit = args[:ample_credit]  
        amount_to_debit = args[:amount]
        
        current_balance = ample_credit.balance

        ApplicationLogger.debug('Initiated debit to ample_credit id ' + ample_credit.id.to_s + ' with amount ' + amount_to_debit.to_s)
        
        ample_credit = @ample_credit_dao.update({
            balance: current_balance - amount_to_debit 
          }, ample_credit)

        ApplicationLogger.debug('Debited successfully')

        return ample_credit
      end

      # 
      # This function gets current ample_credit balance
      #
      # @param user [Model] User model
      # 
      # @return [BigDecimal] current balance in ample credit
      # 
      def get_current_balance(user)
        
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::USER_NOT_FOUND) if user.blank?

        users_ample_credit = user.ample_credit
        
        return users_ample_credit.balance

      end
    end
  end
end

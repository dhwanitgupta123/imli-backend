module CommunicationsModule
  module V1
    module Notifications
      module V1
        class ParseNotificationService
          require 'parse-ruby-client'
          APPLICATION_ID = APP_CONFIG['config']['APPLICATION_ID'].freeze
          API_KEY = APP_CONFIG['config']['API_KEY'].freeze
          CLIENT_KEY = APP_CONFIG['config']['CLIENT_KEY'].freeze
          ORGANIZATION_CHANNEL = 'buyample'.freeze
          PUSH_TYPE_DEFAULT = 'android'.freeze

          attr_accessor :username, :type, :data, :push

          # Intialize Parse at the time of Class Load itself
          Parse.init application_id: APPLICATION_ID,
          api_key: API_KEY,
          quiet: false

          def self.initialize_params(params, template)
            template = get_rendered_template(params, template)
            @username = params['username']
            @type = @username.present? ? 'individual' : 'channel'
            @body = template
            @title = params[:title]
            @action = params[:action]
            @data = params[:data]
          end

          def self.get_rendered_template(params, template)
            #  TO-DO initialized all the files when server starts to reduce I/O
            file = File.join(Rails.root,
                             'app',
                             'views',
                             'communications_module',
                             'notification_templates',
                             template + '.liquid'
                            )
            file = File.read(file)
            template = Liquid::Template.parse(file)
            template = template.render(params)
            return template
          end

          def self.send(notification_attributes, template)
            # initialize push object
            initialize_params(notification_attributes, template)
            user_channel = notification_attributes['username']
            if Rails.env != 'production'
              user_channel = Rails.env + '_' + user_channel
            end
            data = {
              'title': @title,
              'body': @body,
              'data': @data,
              'action': @action
            }
            push = Parse::Push.new(data, user_channel)
            push.type = PUSH_TYPE_DEFAULT
            response = push.save
            Rails.logger.info "ParseNotification:Response:#{response.inspect}"
            response
          end
        end
      end
    end
  end
end

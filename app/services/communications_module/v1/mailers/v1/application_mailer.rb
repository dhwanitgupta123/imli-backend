module CommunicationsModule
  module V1
    module Mailers
      module V1
        class ApplicationMailer < ActionMailer::Base
          default from: CommonModule::V1::Cache.read('EMAIL_FROM')
          layout 'mailer'
        end
      end
    end
  end
end
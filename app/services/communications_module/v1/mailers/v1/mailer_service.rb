module CommunicationsModule
  module V1
    module Mailers
      module V1
        class MailerService < ApplicationMailer
          CACHE_UTIL = CommonModule::V1::Cache
          API_KEY = APP_CONFIG['config']['SENDGRID_API_KEY']
          FROM = CACHE_UTIL.read('EMAIL_FROM')

          # 
          # function to render respective sms template
          #
          def render_mail_template(mail_attributes)
            file = File.join(Rails.root,
                             'app',
                             'views',
                             'communications_module',
                             'email_templates',
                             mail_attributes[:template_name] + '.liquid'
                            )
            file = File.read(file)
            @template = Liquid::Template.parse(file)
            template = @template.render(mail_attributes)

            return template
          end

          # 
          # Function to render welcome email template
          # 
          # Parameters::
          #   * user [object] to which the email has to be sent
          # 
          def send_customize_email(mail_attributes)
            assets = initialize_mailer_assets
            mail_attributes = mail_attributes.merge(assets)
            if mail_attributes.present?
              if mail_attributes['multiple'] == true
                if mail_attributes['to_email_id'].is_a?(String)
                  mail_attributes['to_email_id'] = JSON.parse(mail_attributes['to_email_id'])
                end
                to = mail_attributes['to_email_id']
              else
                to = %("#{mail_attributes["to_first_name"]}" <#{mail_attributes["to_email_id"]}>)
              end
              template = render_mail_template(mail_attributes)
              subject = mail_attributes['subject']
              send_mail_attributes = { 
                :to => to,
                :subject => subject, 
                :template => template
              }

              if mail_attributes['from'].present?
                send_mail_attributes = send_mail_attributes.merge({from: mail_attributes['from']})
              end
              if mail_attributes['pdf_needed'] == true
                wicked_pdf_instance = WickedPdf.new
                @pdf = wicked_pdf_instance.pdf_from_string(template)
                @temp_file_path=Rails.root.join((mail_attributes['pdf_name'] || 'attachment.pdf')).to_s + Time.zone.now.to_f.to_s
                File.open(@temp_file_path, 'wb') do |file|
                  file << @pdf
                  file.close
                end
                send_mail_attributes = send_mail_attributes.merge({ 
                  :pdf_needed => true,
                  :pdf_name => mail_attributes['pdf_name'] || 'attachment.pdf'
                  })
              end
              send_email(send_mail_attributes)
            end
          end

          # 
          # Function to send email to the user
          # 
          # Parameters::
          #   * args [hash] It contains:
          #       * to: email address of user to whom mail has to be sent
          #       * subject: subject of the email
          #       * template: content of email to be sent to the user 
          #       * pdf_needed: [Boolean] (optional) 
          #       * pdf_name: [String] (optional) should contain file_name ending 
          #            with .pdf for good customer experience
          # 
          def send_email(args)
            client = SendGrid::Client.new(api_key: API_KEY)
            if args[:from].present?
              email = SendGrid::Mail.new do |m|
                m.to      = args[:to]
                m.from    = args[:from]
                m.subject = args[:subject]
                m.html    = args[:template]
              end
            else
              email = SendGrid::Mail.new do |m|
                m.to      = args[:to]
                m.from    = FROM
                m.subject = args[:subject]
                m.html    = args[:template]
              end
            end
            if args[:pdf_needed] == true
              attachment = get_pdf_attributes(args)
              email.add_attachment(@temp_file_path, args[:pdf_name])
            end
            client.send(email)
            if args[:pdf_needed] == true
              if File.file?(@temp_file_path)
               File.delete(@temp_file_path)
              end
            end
          end

          #
          # Function to initialize mailer assets like images to be used, etc from Cache
          # 
          def initialize_mailer_assets
            assets = {
              'AMPLE_SQUARE_LOGO'=> CACHE_UTIL.read('AMPLE_SQUARE_LOGO'),
              'AMPLE_ORIGINAL_PURPLE_LOGO'=> CACHE_UTIL.read('AMPLE_ORIGINAL_PURPLE_LOGO'),
              'AMPLE_BW_LOGO'=> CACHE_UTIL.read('AMPLE_BW_LOGO'),
              'AMPLE_MEMBERSHIP_IMAGE'=> CACHE_UTIL.read('AMPLE_MEMBERSHIP_IMAGE'),
              'FACEBOOK_ICON'=> CACHE_UTIL.read('FACEBOOK_ICON'),
              'FACEBOOK_PURPLE_ICON'=> CACHE_UTIL.read('FACEBOOK_PURPLE_ICON'),
              'TWITTER_ICON'=> CACHE_UTIL.read('TWITTER_ICON'),
              'TWITTER_PURPLE_ICON'=> CACHE_UTIL.read('TWITTER_PURPLE_ICON'),
              'INSTAGRAM_ICON'=> CACHE_UTIL.read('INSTAGRAM_ICON'),
              'INSTAGRAM_PURPLE_ICON'=> CACHE_UTIL.read('INSTAGRAM_PURPLE_ICON'),
              'FACEBOOK_URL'=> CACHE_UTIL.read('FACEBOOK_URL'),
              'TWITTER_URL'=> CACHE_UTIL.read('TWITTER_URL'),
              'INSTAGRAM_URL'=> CACHE_UTIL.read('INSTAGRAM_URL'),
              'AMPLE_BLUE_LOGO' => CACHE_UTIL.read('AMPLE_BLUE_LOGO'),
              'AMPLE_TROLLEY' => CACHE_UTIL.read('AMPLE_TROLLEY'),
              'TICK_IMAGE' => CACHE_UTIL.read('TICK_IMAGE'),
              'BUYAMPLE_HOMEPAGE' => CACHE_UTIL.read('BUYAMPLE_HOMEPAGE'),
              'APP_LINK' => CACHE_UTIL.read('APP_LINK'),
              'INVITE_EMAIL_FROM' => CACHE_UTIL.read('INVITE_EMAIL_FROM'),
              'REFERRAL_CODE' => CACHE_UTIL.read('REFERRAL_CODE'),
              'SELLER_NAME' => CACHE_UTIL.read('SELLER_NAME'),
              'SELLER_ADDRESS_LINE_1' => CACHE_UTIL.read('SELLER_ADDRESS_LINE_1'),
              'SELLER_ADDRESS_LINE_2' => CACHE_UTIL.read('SELLER_ADDRESS_LINE_2'),
              'TIN_VAT_NUMBER' => CACHE_UTIL.read('TIN_VAT_NUMBER')
            }
            return assets
          end

          # 
          # get_pdf_attributes function to serve as a helper to make a pdf attachment of requested
          #  
          def get_pdf_attributes(args)
            if args[:pdf_needed] == true
              pdf_name = (args[:pdf_name].blank?) ? 'attachment.pdf' : args[:pdf_name]
              attachments[pdf_name] = {mime_type: CACHE_UTIL.read('PDF_MIME_TYPE'),
                                           content: @pdf }
              return attachments
            else
              return nil
            end
          end
          
        end
      end
    end
  end
end

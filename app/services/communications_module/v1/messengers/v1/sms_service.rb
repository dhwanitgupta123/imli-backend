module CommunicationsModule
  module V1
    module Messengers
      module V1
        class SmsService < BaseModule::V1::BaseService
          require 'gupshup'

          #
          # Function to setup sms template
          #
          # Parameters::
          #   * message attributes [object] contains message & phone number
          #
          def self.send_sms(message_attributes, template_name)
            template = render_template(message_attributes, template_name)
            send_message({
              message: template,
              phone_number: '91' + message_attributes['phone_number']
            })
          end

          # 
          # function to render respective sms template
          #
          def self.render_template(message_attributes, template_name)
            file = File.join(Rails.root,
                             'app',
                             'views',
                             'communications_module',
                             'sms_templates',
                             template_name + '.liquid'
                            )
            file = File.read(file)
            @template = Liquid::Template.parse(file)
            template = @template.render(message_attributes)
            return template
          end

          #
          # function to send SMS to user
          #
          # Parameters::
          #   * sms_attributes [hash] it contains
          #       * message: text to be sent to the user
          #       * phone_number: phone number to which sms has to be sent
          #
          def self.send_message(sms_attributes)
            gup_shup = Gupshup::Enterprise.new(
              APP_CONFIG['config']['SMS_SERVICE_USERNAME'], APP_CONFIG['config']['SMS_SERVICE_PASSWORD'])
            gup_shup.send_text_message(
              sms_attributes[:message],
              sms_attributes[:phone_number]
            )
          end
        end
      end
    end
  end
end

module CategorizationModule
  module V1
    #
    # Categorization Api Service class to implement CRUD for categorization APIs
    #
    class MpspCategorizationApiService
      #
      # Defining global variables with versioning
      #
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      MPSP_DEPARTMENT_SERVICE = CategorizationModule::V1::MpspDepartmentService
      MPSP_CATEGORY_SERVICE = CategorizationModule::V1::MpspCategoryService
      MPSP_CATEGORY_SERVICE_HELPER = CategorizationModule::V1::MpspCategoryServiceHelper
      MPSP_CATEGORIZATION_API_HELPER = CategorizationModule::V1::MpspCategorizationApiHelper
      MPSP_CATEGORY_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::CategoryStates
      MPSP_CATEGORY_API_RESPONSE_DECORATOR = CategorizationModule::V1::MpspCategorizationApiResponseDecorator
      MPSP_CATEGORY_DAO = CategorizationModule::V1::MpspCategoryDao
      MARKETPLACE_SELLING_PACK_DAO = MarketplaceProductModule::V1::MarketplaceSellingPackDao
      
      #
      # initialize MpspCategorizationApiService Class
      #
      def initialize(params)
        @params = params
      end

      #
      # function to get all active mpsp_department details
      #
      # Response::
      #   * sends response with all the active mpsp_department details
      #
      # @return [response]
      #
      def get_all_mpsp_departments_details(paginate_params)
        begin
          mpsp_department_details = get_mpsp_department_details(paginate_params)
          MPSP_CATEGORY_API_RESPONSE_DECORATOR.create_mpsp_department_details_response(mpsp_department_details)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          MPSP_CATEGORY_API_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # Get all active products corresponding to given mpsp_category
      #
      # @param params [JSON] [Hash containing mpsp_category id and paginate parameters]
      # Hash:
      #   - id : mpsp_category id
      #   - paginate_params: parameters for pagination
      #
      # @return [Response] [response to be sent to the user]
      def get_products_by_mpsp_category(params)
        begin

          response = get_products_for_mpsp_category(params)
          return response
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          MPSP_CATEGORY_API_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          MPSP_CATEGORY_API_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError, CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          MPSP_CATEGORY_API_RESPONSE_DECORATOR.create_not_found_error(e.message)
        end
      end

      #
      # Get all active products corresponding to given mpsp_category but with FILTERS
      # applied over them.
      #
      # @param params [JSON] [Hash containing mpsp_category id and paginate parameters with FILTERS]
      # Hash:
      #   - id : mpsp_category id
      #   - product_params: containing {:filters, :page_no, :per_page, :order...}
      #
      # @return [Response] [response to be sent to the user]
      def get_products_by_filter(params)
        begin
          response = get_products_after_applying_filters(params)
          return response
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          MPSP_CATEGORY_API_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          MPSP_CATEGORY_API_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError, CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          MPSP_CATEGORY_API_RESPONSE_DECORATOR.create_not_found_error(e.message)
        end
      end
      #
      # Function to get all active products corresponding to given mpsp_category
      #
      # @param params [JSON] [Hash containing mpsp_category id and paginate parameters]
      # Hash:
      #   - id : mpsp_category id
      #   - paginate_params: parameters for pagination
      #
      # @return [Response] [response to be sent to the user]
      # 
      # @raise [InsufficientDataError] [if passed parameters were insufficient enough to process]
      # @raise [PreConditionRequiredError] [requested mpsp_category is not active anymore]
      # @raise [ResourceNotFoundError] [if no submpsp_categories exist for requested mpsp_category]
      # @raise [CategoryNotFoundError] [if no mpsp_category exist for passed mpsp_category]
      #
      def get_products_for_mpsp_category(params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'MpspCategory ID'}) if params[:id].blank?
        mpsp_category = fetch_and_validate_mpsp_category(params[:id])
        # Fetch all submpsp_categories
        mpsp_sub_categories = fetch_mpsp_sub_categories_for_mpsp_category(mpsp_category)

        # Fetch all ACTIVE products for sub-mpsp_categories
        products_array_and_update_mpsp_sub_categories = get_products_for_mpsp_sub_categories(mpsp_sub_categories, params[:testing])
        products_array = products_array_and_update_mpsp_sub_categories[:products_array]
        mpsp_sub_categories = products_array_and_update_mpsp_sub_categories[:mpsp_sub_categories]
        return MPSP_CATEGORY_API_RESPONSE_DECORATOR.create_products_and_filters_response(mpsp_sub_categories, products_array)
      end

      #
      # Get all active products corresponding to given mpsp_category but with FILTERS
      # applied over them.
      #
      # @param params [JSON] [Hash containing mpsp_category id and paginate parameters with FILTERS]
      # Hash:
      #   - id : mpsp_category id
      #   - product_params: containing {:filters, :page_no, :per_page, :order...}
      #
      # @return [Response] [response to be sent to the user]
      #
      # @raise [InsufficientDataError] [if passed parameters were insufficient enough to process]
      # @raise [PreConditionRequiredError] [requested mpsp_category is not active anymore]
      # @raise [ResourceNotFoundError] [if no submpsp_categories exist for requested mpsp_category]
      # @raise [CategoryNotFoundError] [if no mpsp_category exist for passed mpsp_category]
      #
      def get_products_after_applying_filters(params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'MpspCategory ID'}) if params[:id].blank?
        mpsp_category = fetch_and_validate_mpsp_category(params[:id])
        # Fetch all submpsp_categories
        mpsp_sub_categories = fetch_mpsp_sub_categories_for_mpsp_category(mpsp_category)

        # Check for filters passed in request
        # Need to modify the logic to optimize DB query
        # Also, Add pagination to fetch MPSPs using where query
        # Open Issue: https://github.com/imlitech/imli-backend/issues/183
        if (params[:product_params].present? && params[:product_params][:filters].present?)
          filters = params[:product_params][:filters]
          filtered_mpsp_sub_categories = fetch_mpsp_sub_categories_from_filters(filters)
          # Override sub mpsp_categories only if filtered sub mpsp_categories are SUBSET of overall sub mpsp_categories
          if filtered_mpsp_sub_categories.present? && (filtered_mpsp_sub_categories - mpsp_sub_categories).empty?
            mpsp_sub_categories = filtered_mpsp_sub_categories
          end
        end
        # Fetch all ACTIVE products for sub-mpsp_categories
        products_array_and_update_mpsp_sub_categories = get_products_for_mpsp_sub_categories(mpsp_sub_categories)
        products_array = products_array_and_update_mpsp_sub_categories[:products_array]
        mpsp_sub_categories = products_array_and_update_mpsp_sub_categories[:mpsp_sub_categories]
        return MPSP_CATEGORY_API_RESPONSE_DECORATOR.create_products_and_filters_response(mpsp_sub_categories, products_array)
      end

      #
      # Fetch all active MPSPs for given sub mpsp_categories
      #
      # @param mpsp_sub_categories [Array] [Array of Sub mpsp_categories Object]
      #
      # @return [Array] [Array of all active MPSP/display products]
      #
      # @raise [ResourceNotFoundError] [if no products found after search]
      #
      def get_products_for_mpsp_sub_categories(mpsp_sub_categories, testing = false)
        mpsp_category_service = MPSP_CATEGORY_SERVICE.new(@params)
        products_array = []
        mpsp_sub_categories = MPSP_CATEGORY_DAO.get_aggregated_mpsp_sub_categories(mpsp_sub_categories, testing)
        # Fetch products for each active mpsp_sub_categories
        if mpsp_sub_categories.present?
          mpsp_sub_categories.each do |mpsp_sub_category|
            display_products = mpsp_category_service.get_display_products_for_mpsp_sub_category(mpsp_sub_category, testing)
            # Deleting sub_category if active display product is 0 in that mpsp_sub_category
            if display_products.blank?
              mpsp_sub_categories = mpsp_sub_categories - [mpsp_sub_category]
            end
            # Removing dupliates by taking union of previous array and result array
            # Array Union Property: http://ruby-doc.org/core-2.2.3/Array.html#7C-method
            products_array = products_array | display_products
          end
        end

        return {products_array: products_array, mpsp_sub_categories: mpsp_sub_categories} if products_array.blank?

        products_array = MARKETPLACE_SELLING_PACK_DAO.get_aggregated_products(products_array)
        products_array = products_array.sort{|a,b|
          [a.mpsp_sub_category.priority, b.marketplace_selling_pack_pricing.savings] <=>
          [b.mpsp_sub_category.priority, a.marketplace_selling_pack_pricing.savings] }

        return {products_array: products_array, mpsp_sub_categories: mpsp_sub_categories}
      end

      #
      # Fetch and validate mpsp_category
      #
      # @param mpsp_category_id [Integer] [id for which mpsp_category is to be fetched]
      #
      # @return [Object] [MpspCategory Object w.r.t. passed id]
      #
      # @raise [CategoryNotFoundError] [if no corresponding mpsp_category exists]
      # @raise [PreConditionRequiredError] [if found mpsp_category is not Active]
      def fetch_and_validate_mpsp_category(mpsp_category_id)
        mpsp_category_service = MPSP_CATEGORY_SERVICE.new(@params)
        mpsp_category = mpsp_category_service.get_mpsp_category_by_id(mpsp_category_id)
        if mpsp_category.status !=  MPSP_CATEGORY_MODEL_STATES::ACTIVE
          raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(CONTENT_UTIL::NON_ACTIVE_CATEGORY)
        end
        return mpsp_category
      end

      #
      # Fetch sub mpsp_categories for a given mpsp_category
      #
      # @param mpsp_category [Object] [mpsp_category for which sub-mpsp_categories is to be fetched]
      #
      # @return [Array] [Array of Sub-MpspCategory Object w.r.t. passed mpsp_category]
      #
      # @raise [PreConditionRequiredError] [if no corresponding sub-mpsp_category exists]
      #
      def fetch_mpsp_sub_categories_for_mpsp_category(mpsp_category)
        mpsp_sub_categories = mpsp_category.mpsp_sub_categories.order('priority ASC')
 
        if mpsp_sub_categories.blank?
          raise CUSTOM_ERRORS_UTIL::ResourceNotFoundError.new(CONTENT_UTIL::NO_SUB_CATEGORIES + ' : ' + mpsp_category.id.to_s)
        end
        return mpsp_sub_categories
      end

      #
      # Fetch sub mpsp_categories for a given FILTER
      #
      # @param filter [JSON] [JSON Hash containg array of mpsp_sub_categories IDs]
      #
      # @return [Array] [Array of Sub-MpspCategory Object w.r.t. passed filter hash]
      #
      def fetch_mpsp_sub_categories_from_filters(filters)
        mpsp_sub_categories_ids = filters[:mpsp_sub_categories]
        return [] unless mpsp_sub_categories_ids.present?
        # Fetch sub mpsp_category corresponding to passed IDs
        mpsp_category_service = MPSP_CATEGORY_SERVICE.new(@params)
        mpsp_sub_categories_array = []
        mpsp_sub_categories_ids.each do |id|
          begin
            mpsp_sub_category = mpsp_category_service.get_mpsp_category_by_id(id)
            mpsp_sub_categories_array.push(mpsp_sub_category)
          rescue CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
            # Do nothing. Just ignore that filter
          end
        end
        return mpsp_sub_categories_array
      end

      #
      # function to get mpsp_department details
      #
      # Response::
      #   * sends response with all the mpsp_departments details and page count
      #
      # @return [response]
      #
      def get_mpsp_department_details(paginate_params)
        mpsp_department_service = MPSP_DEPARTMENT_SERVICE.new(@params)
        mpsp_category_service = MPSP_CATEGORY_SERVICE.new(@params)

        active_mpsp_departments_array = []
        mpsp_departments_response = mpsp_department_service.get_active_mpsp_departments(paginate_params)
        page_count = mpsp_departments_response[:page_count]
        active_mpsp_departments = mpsp_departments_response[:mpsp_departments]

        active_mpsp_departments.each do |mpsp_department|
          active_mpsp_categories = mpsp_category_service.get_active_mpsp_categories_by_mpsp_department(mpsp_department, paginate_params[:testing])
          unless active_mpsp_categories.blank?
            active_mpsp_categories_hash = MPSP_CATEGORY_SERVICE_HELPER.get_mpsp_categories_hash(active_mpsp_categories)
            mpsp_department_details = MPSP_CATEGORIZATION_API_HELPER.map_mpsp_department_to_hash(mpsp_department, active_mpsp_categories_hash)
            active_mpsp_departments_array.push(mpsp_department_details)
          end
        end
        return { mpsp_departments: active_mpsp_departments_array, page_count: page_count }
      end
    end
  end
end

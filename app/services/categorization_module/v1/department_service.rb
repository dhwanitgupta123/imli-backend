module CategorizationModule
  module V1
    #
    # Department Service class to implement CRUD for department model
    #
    class DepartmentService
      #
      # Defining global variables with versioning
      #
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      DEPARTMENT_EVENTS = CategorizationModule::V1::ModelStates::V1::DepartmentEvents
      DEPARTMENT_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::DepartmentStates
      CATEGORY_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::CategoryStates
      CONTENT_UTIL = CommonModule::V1::Content
      IMAGE_SERVICE = ImageServiceModule::V1::ImageService

      #
      # initialize DepartmentService Class
      #
      def initialize(params)
        @params = params
        @department_model = CategorizationModule::V1::Department
        @department_response_decorator = CategorizationModule::V1::DepartmentResponseDecorator
        @image_service = IMAGE_SERVICE.new
      end

      #
      # function to create new department
      #
      # Parameters::
      #   * department_params [hash] it contains label & image url of the department
      #
      # @return [response]
      def create_department(department_params)
        begin
          department = create_department_with_details(department_params) 
          @department_response_decorator.create_ok_response(department)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @department_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @department_response_decorator.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # function to update department
      #
      # Parameters::
      #   * department_params [hash] it contains label & image url of the department to be updated
      #
      # @return [response]
      def update_department(department_params)
        begin
          department = update_department_details(department_params)
          @department_response_decorator.create_ok_response(department)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @department_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @department_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::DepartmentNotFoundError => e
          @department_response_decorator.create_response_department_not_found_error(e.message)
        end
      end

      #
      # function to change status of department
      #
      # Parameters::
      #   * change_state_params [hash] 
      #     * id of department of which status is to be changed
      #     * event to trigger to change the status
      #
      # @return [response]
      def change_state(change_state_params)
        begin
          department = change_state_by_id(change_state_params)
          @department_response_decorator.create_ok_response(department)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @department_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::DepartmentNotFoundError => e
          @department_response_decorator.create_response_department_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          @department_response_decorator.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @department_response_decorator.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # function to get data of a department
      #
      # Parameters::
      #   * department_id [integer] id of department whose details are requested
      #
      # @return [response]
      def get_department(department_id)
        begin
          department_details = get_department_details(department_id)
          @department_response_decorator.create_department_details_response(department_details)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @department_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::RunTimeError => e
          @department_response_decorator.create_response_runtime_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::DepartmentNotFoundError => e
          @department_response_decorator.create_response_department_not_found_error(e.message)
        end
      end

      #
      # function to get label of all the departments
      #
      # Response::
      #   * sends response with all the department labels
      #
      # @return [response]
      def get_all_departments(paginate_params)
        begin
          all_departments = get_all_departments_detail(paginate_params)
          @department_response_decorator.create_all_departments_response(all_departments)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @department_response_decorator.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # Create department with the parameters passed
      #
      # Parameters::
      #   * department_params [Hash] Hash of department related data, label, image_url
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #
      # @return [Department Object] created department object
      #
      def create_department_with_details(department_param)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Label' }) if department_param[:label].blank?
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Priority' }) if department_param[:priority].blank?
        department = @department_model.create_department(department_param)
        return department
      end

      #
      # Update department with the parameters passed
      #
      # Parameters::
      #   * department_params [Hash] Hash of department related data, label, image_url
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::InsufficientDataError if label/id is missing
      #
      # @return [Department Object] updated department object
      #
      def update_department_details(department_param)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Department' }) if department_param[:id].blank?
        department = get_department_by_id(department_param[:id])
        department = @department_model.update_department(department_param, department)
        return department
      end

      #
      # change status of department
      #
      # Parameters::
      #   * change_state_params [hash] 
      #     * id of department of which status is to be changed
      #     * event to trigger to change the status
      #
      # Raise Errors::
      #   * CustomErrors::DepartmentNotFoundError, if department not found
      #   * CustomErrors::InsufficientDataError, if id is not present
      #   * CustomErrors::PreConditionRequiredError, if transition is not allowed
      #
      # @return [Department Object] created Category object
      #
      def change_state_by_id(change_state_params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Department' }) if change_state_params[:id].blank?
        department = get_department_by_id(change_state_params[:id])
        case change_state_params[:event].to_i
        when DEPARTMENT_EVENTS::ACTIVATE
          department.trigger_event('activate')
        when DEPARTMENT_EVENTS::DEACTIVATE
          department.trigger_event('deactivate')
        when DEPARTMENT_EVENTS::SOFT_DELETE
          department.trigger_event('soft_delete')
        else
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::INVALID_EVENT)
        end
        return department
      end
      
      #
      # Get details of the department
      #
      # Parameters::
      #   * department_id [integer] id of department whose details to be find
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::DepartmentNotFoundError, if department not found
      #   * CustomErrors::InsufficientDataError, if id is not present
      #
      # @return [Department Object] created Category object
      #
      def get_department_details(department_id)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Department' }) if department_id.blank?
        department_details = get_department_by_id(department_id)
        return department_details
      end

      #
      # function to check the presence of a department based on it's id
      #
      # Parameters::
      #   * department_id [integer] id of department to be searched
      #
      # Raise Errors::
      #   * CustomErrors::DepartmentNotFoundError if department not found
      #
      # @return [department Object]
      def get_department_by_id(department_id)
        department = @department_model.find_by_department_id(department_id)
        return department.present? ? department : (
          raise CUSTOM_ERRORS_UTIL::DepartmentNotFoundError.new(CONTENT_UTIL::DEPARTMENT_NOT_PRESENT))
      end

      #
      # function to get list of all the departments
      #
      # @return [array of department object]
      def get_all_departments_detail(paginate_params)
        if paginate_params[:order].present? && paginate_params[:order] != 'ASC' && paginate_params[:order] != 'DESC'
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::WRONG_ORDER)
        end
        if paginate_params[:sort_by].present? && !@department_model.attribute_names.include?(paginate_params[:sort_by])
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::VALID_SORT_BY)
        end
        all_departments = @department_model.paginated_departments(paginate_params)
        return all_departments
      end

      # 
      # function to get list of all the active departments
      # 
      # @return [array of department object]
      # 
      def get_active_departments(paginate_params)
        paginate_params[:state] = DEPARTMENT_MODEL_STATES::ACTIVE

        #setting unlimited per_page count
        paginate_params[:page_no] = CommonModule::V1::Pagination::PAGE_NO
        paginate_params[:per_page] = CommonModule::V1::Pagination::INFINITE_PER_PAGE
        all_departments = get_all_departments_detail(paginate_params)
        return all_departments
      end

      #
      # This function join the department, categories and sub_categories record
      # and return the deprtments -> categories -> sub_caeegories tree
      # 
      # @return [Json Response]
      #
      def get_all_departments_categories_and_sub_categories
        aggregated_data = @department_model.includes(categories: :sub_categories).
        where.not(status: DEPARTMENT_MODEL_STATES::DELETED).
        where.not(categories: { status: CATEGORY_MODEL_STATES::DELETED })

        return @department_response_decorator.create_aggregated_data_response(aggregated_data)
      end
    end
  end
end

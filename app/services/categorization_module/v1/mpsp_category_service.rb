module CategorizationModule
  module V1
    #
    # MpspCategory Service class to implement CRUD for mpsp_category model
    #
    class MpspCategoryService
      #
      # Defining global variables with versioning
      #
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      MPSP_CATEGORY_EVENTS = CategorizationModule::V1::ModelStates::V1::CategoryEvents
      MPSP_CATEGORY_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::CategoryStates
      BRAND_PACK_SERVICE = MasterProductModule::V1::BrandPackService
      CONTENT_UTIL = CommonModule::V1::Content
      MPSP_CATEGORY_DAO = CategorizationModule::V1::MpspCategoryDao
      MPSP_CATEGORIZATION_API_SERVICE = CategorizationModule::V1::MpspCategorizationApiService
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates
      GENERAL_HELPER = CommonModule::V1::GeneralHelper

      #
      # initialize MpspCategoryService Class
      #
      def initialize(params)
        @params = params
        @mpsp_category_service = CategorizationModule::V1::MpspCategoryService
        @mpsp_department_service = CategorizationModule::V1::MpspDepartmentService
        @mpsp_category_response_decorator = CategorizationModule::V1::MpspCategoryResponseDecorator
        @mpsp_department_response_decorator = CategorizationModule::V1::MpspDepartmentResponseDecorator
        @mpsp_category_model = CategorizationModule::V1::MpspCategory
      end

      #
      # function to create new mpsp_category
      #
      # Parameters::
      #   * mpsp_category_params [hash] it contains label & image url of the mpsp_category
      #
      # @return [response]
      #
      def create_mpsp_category(mpsp_category_params)
        begin
          mpsp_category = create_mpsp_category_with_details(mpsp_category_params)
          @mpsp_category_response_decorator.create_ok_mpsp_category_response(mpsp_category)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @mpsp_category_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @mpsp_category_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::DepartmentNotFoundError => e
          @mpsp_department_response_decorator.create_response_mpsp_department_not_found_error(e.message)
        end
      end

      #
      # function to create new sub mpsp_category
      #
      # Parameters::
      #   * mpsp_category_params [hash] it contains label & image url of the sub mpsp_category
      #
      # @return [response]
      #
      def create_mpsp_sub_category(category_params)
        begin
          mpsp_sub_category = create_mpsp_sub_category_with_details(category_params)
          @mpsp_category_response_decorator.create_ok_mpsp_sub_category_response(mpsp_sub_category)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @mpsp_category_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @mpsp_category_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          @mpsp_category_response_decorator.create_response_mpsp_category_not_found_error(e.message)
        end
      end

      #
      # function to update mpsp_category
      #
      # Parameters::
      #   * mpsp_category_params [hash] it contains label & image url of the mpsp_category to be updated
      #
      # @return [response]
      def update_mpsp_category(mpsp_category_params)
        begin
          mpsp_category = update_mpsp_category_details(mpsp_category_params)
          @mpsp_category_response_decorator.create_ok_mpsp_category_response(mpsp_category)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @mpsp_category_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @mpsp_category_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          @mpsp_category_response_decorator.create_response_mpsp_category_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::DepartmentNotFoundError => e
          @mpsp_department_response_decorator.create_response_mpsp_department_not_found_error(e.message)
        end
      end

      #
      # function to change status of a mpsp_category
      #
      # Parameters::
      #   * change_state_params [hash] 
      #     * id of mpsp_category of which status is to be changed
      #     * event to trigger to change the status
      #
      # @return [response]
      def change_state(change_state_params)
        begin
          mpsp_category = change_state_by_id(change_state_params)
          if mpsp_category.mpsp_department.present?
            @mpsp_category_response_decorator.create_ok_mpsp_category_response(mpsp_category)
          else
            @mpsp_category_response_decorator.create_ok_mpsp_sub_category_response(mpsp_category)
          end            
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @mpsp_category_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          @mpsp_category_response_decorator.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          @mpsp_category_response_decorator.create_response_mpsp_category_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @mpsp_category_response_decorator.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # function to update mpsp_category
      #
      # Parameters::
      #   * mpsp_category_params [hash] it contains label & image url of the mpsp_category to be updated
      #
      # @return [response]
      def update_mpsp_sub_category(mpsp_sub_category_params)
        begin
          mpsp_sub_category = update_mpsp_sub_category_details(mpsp_sub_category_params)
          @mpsp_category_response_decorator.create_ok_mpsp_sub_category_response(mpsp_sub_category)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @mpsp_category_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @mpsp_category_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          @mpsp_category_response_decorator.create_response_mpsp_category_not_found_error(e.message)
        end
      end

      #
      # function to get data of a mpsp_category
      #
      # Parameters::
      #   * mpsp_category_id [integer] id of mpsp_category of which information is requested
      #
      # @return [response]
      #
      def get_mpsp_category(mpsp_category_id)
        begin
          mpsp_category_details = get_mpsp_category_details(mpsp_category_id)
          @mpsp_category_response_decorator.create_mpsp_category_details_response(mpsp_category_details)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @mpsp_category_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::RunTimeError => e
          @mpsp_category_response_decorator.create_response_runtime_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          @mpsp_category_response_decorator.create_response_mpsp_category_not_found_error(e.message)
        end
      end

      #
      # function to get label of all the categories
      #
      # Response::
      #   * sends response with all the categories labels
      #
      # @return [response] 
      def get_all_mpsp_categories(paginate_params)
        begin
          all_mpsp_categories = get_all_mpsp_categories_detail(paginate_params)
          @mpsp_category_response_decorator.create_all_mpsp_categories_response(all_mpsp_categories)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @mpsp_category_response_decorator.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # function to get label of all the sub categories
      #
      # Response::
      #   * sends response with all the sub categories labels
      #
      # @return [response] 
      def get_all_mpsp_sub_categories(paginate_params)
        begin
          all_mpsp_sub_categories = get_all_mpsp_sub_categories_detail(paginate_params)
          @mpsp_category_response_decorator.create_all_mpsp_sub_categories_response(all_mpsp_sub_categories)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @mpsp_category_response_decorator.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # Create mpsp_category with the parameters passed
      #
      # Parameters::
      #   * mpsp_category_parms [Hash] Hash of mpsp_category related data, label, image_url
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::DepartmentNotFoundError, if mpsp_department not found
      #   * CustomErrors::InsufficientDataError, if mpsp_department id is not present
      #
      # @return [MpspCategory Object] created MpspCategory object
      #
      def create_mpsp_category_with_details(mpsp_category_params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'MpspDepartment' }) if mpsp_category_params[:mpsp_department_id].blank?
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Label' }) if mpsp_category_params[:label].blank?
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Priority' }) if mpsp_category_params[:priority].blank?
        mpsp_department = get_mpsp_department_by_id(mpsp_category_params[:mpsp_department_id])
        if unique_label?(mpsp_department.mpsp_categories, mpsp_category_params[:label])
          mpsp_category = @mpsp_category_model.create_mpsp_category(mpsp_category_params)
          mpsp_category.mpsp_department = mpsp_department
          mpsp_category.save_mpsp_category
          return mpsp_category
        else
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_ALREADY_TAKEN%{ field: 'Label' })
        end
      end

      #
      # Create sub mpsp_category with the parameters passed 
      #
      # Parameters::
      #   * mpsp_category_parms [Hash] Hash of mpsp_category related data, label, image_url
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::MpspCategoryNotFound, if mpsp_category not found
      #   * CustomErrors::InsufficientDataError, if mpsp_category id is not present
      #
      # @return [MpspCategory Object] created MpspCategory object
      #
      def create_mpsp_sub_category_with_details(mpsp_category_params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'MpspCategory' }) if mpsp_category_params[:mpsp_category_id].blank?
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Label' }) if mpsp_category_params[:label].blank?
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Priority' }) if mpsp_category_params[:priority].blank?
        mpsp_category = get_mpsp_category_by_id(mpsp_category_params[:mpsp_category_id])
        if unique_label?(mpsp_category.mpsp_sub_categories, mpsp_category_params[:label])
          mpsp_category.mpsp_sub_categories << @mpsp_category_model.create_mpsp_category(mpsp_category_params)
          return mpsp_category.mpsp_sub_categories.last
        else
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_ALREADY_TAKEN%{ field: 'Label' })
        end
      end

      #
      # Update mpsp_category with the parameters passed
      #
      # Parameters::
      #   * mpsp_category_params [Hash] Hash of mpsp_category related data, label, image_url
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::InsufficientDataError if label/id is missing
      #
      # @return [mpsp_category Object] updated mpsp_category object
      #
      def update_mpsp_category_details(mpsp_category_param)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'MpspCategory' }) if mpsp_category_param[:id].blank?
        mpsp_department = get_mpsp_department_by_id(mpsp_category_param[:mpsp_department_id]) if mpsp_category_param[:mpsp_department_id].present?
        mpsp_category = get_mpsp_category_by_id(mpsp_category_param[:id])
        mpsp_category.update_mpsp_category(mpsp_category_param)
        return mpsp_category
      end

      #
      # change status of mpsp_category
      #
      # Parameters::
      #   * change_state_params [hash] 
      #     * id of mpsp_category of which status is to be changed
      #     * event to trigger to change the status
      #
      # Raise Errors::
      #   * CustomErrors::DepartmentNotFoundError, if mpsp_category not found
      #   * CustomErrors::InsufficientDataError, if id is not present
      #   * CustomErrors::PreConditionRequiredError, if transition is not allowed
      #
      # @return [MpspDepartment Object] created MpspCategory object
      #
      def change_state_by_id(change_state_params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'MpspCategory' }) if change_state_params[:id].blank?
        mpsp_category = get_mpsp_category_by_id(change_state_params[:id])
        case change_state_params[:event].to_i
        when MPSP_CATEGORY_EVENTS::ACTIVATE
          mpsp_category.trigger_event('activate')
          if mpsp_category.mpsp_department.present?
            MPSP_CATEGORY_DAO.activate_products_within_categories(mpsp_category)
          else
            MPSP_CATEGORY_DAO.activate_products_within_sub_categories(mpsp_category)
          end
        when MPSP_CATEGORY_EVENTS::DEACTIVATE
          mpsp_category.trigger_event('deactivate')
          if mpsp_category.mpsp_department.present?
            MPSP_CATEGORY_DAO.deactivate_products_within_categories(mpsp_category)
          else
            MPSP_CATEGORY_DAO.deactivate_products_within_sub_categories(mpsp_category)
          end
        when MPSP_CATEGORY_EVENTS::SOFT_DELETE
          mpsp_category.trigger_event('soft_delete')
          if mpsp_category.mpsp_department.present?
            MPSP_CATEGORY_DAO.deactivate_products_within_categories(mpsp_category)
          else
            MPSP_CATEGORY_DAO.deactivate_products_within_sub_categories(mpsp_category)
          end
        else
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::INVALID_EVENT)
        end
        return mpsp_category
      end

      #
      # Update mpsp_category with the parameters passed
      #
      # Parameters::
      #   * mpsp_category_params [Hash] Hash of mpsp_category related data, label, image_url
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::InsufficientDataError if label/id is missing
      #
      # @return [mpsp_category Object] updated mpsp_category object
      #
      def update_mpsp_sub_category_details(mpsp_sub_category_param)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Sub MpspCategory' }) if mpsp_sub_category_param[:id].blank?
        mpsp_category = get_mpsp_category_by_id(mpsp_sub_category_param[:mpsp_category_id]) if mpsp_sub_category_param[:mpsp_category_id].present?
        mpsp_sub_category = get_mpsp_category_by_id(mpsp_sub_category_param[:id])

        raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(
          CONTENT_UTIL::SUB_CATEGORY_NOT_PRESENT) if mpsp_sub_category.mpsp_parent_category_id.blank?
        mpsp_sub_category.update_mpsp_category(mpsp_sub_category_param)
        return mpsp_sub_category
      end

      #
      # Get details of the  mpsp_category
      #
      # Parameters::
      #   * mpsp_category_id [integer] id of mpsp_category of which information is requested
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::MpspCategoryNotFound, if mpsp_category not found
      #   * CustomErrors::InsufficientDataError, if mpsp_category id is not present
      #
      # @return [MpspCategory Object] created MpspCategory object
      #
      def get_mpsp_category_details(mpsp_category_id)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'MpspCategory' }) if mpsp_category_id.blank?
        mpsp_category_details = get_mpsp_category_by_id(mpsp_category_id)
        return mpsp_category_details
      end

      #
      # function to check the presence of a mpsp_category based on it's id
      #
      # Parameters::
      #   *mpsp_category_id [ineteger] id of mpsp_category to be searched
      #
      # @return [mpsp_category object]
      def get_mpsp_category_by_id(mpsp_category_id)
        mpsp_category = @mpsp_category_model.find_by_mpsp_category_id(mpsp_category_id)
        return mpsp_category.present? ? mpsp_category : (raise CUSTOM_ERRORS_UTIL::CategoryNotFoundError.new(CONTENT_UTIL::CATEGORY_NOT_PRESENT))
      end

      #
      # function to call mpsp_department_exists in MpspDepartmentService
      #
      # Parameters::
      #   *mpsp_department_id [ineteger] id of mpsp_category to be searched
      #
      # @return [mpsp_department object]
      def get_mpsp_department_by_id(mpsp_department_id)
        mpsp_department_service = @mpsp_department_service.new(@params)
        mpsp_department = mpsp_department_service.get_mpsp_department_by_id(mpsp_department_id)
        return mpsp_department
      end

      #
      # function to get list of all the mpsp_categories
      #
      # @return [array of mpsp_categories object]
      def get_all_mpsp_categories_detail(paginate_params)
        valid_paginate_params(paginate_params)
        all_mpsp_categories = @mpsp_category_model.paginated_mpsp_categories(paginate_params)
        return all_mpsp_categories
      end

      #
      # function to get list of all the  sub categories
      #
      # @return [array of sub categories object]
      def get_all_mpsp_sub_categories_detail(paginate_params)
        valid_paginate_params(paginate_params)
        all_mpsp_sub_categories = @mpsp_category_model.paginated_mpsp_sub_categories(paginate_params)
        return all_mpsp_sub_categories
      end

      # 
      # function to get list of all the  active categories under a mpsp_department
      # 
      # @return [array of categories object]
      def get_active_mpsp_categories_by_mpsp_department(mpsp_department, testing=false)
        mpsp_categorization_api_service = MPSP_CATEGORIZATION_API_SERVICE.new(@params)
        all_mpsp_categories = @mpsp_category_model.get_active_mpsp_categories_by_mpsp_department(mpsp_department)
        active_mpsp_categories = all_mpsp_categories.dup
        active_mpsp_categories.each do |mpsp_category|
          mpsp_sub_category_and_products = mpsp_categorization_api_service.get_products_for_mpsp_sub_categories(mpsp_category.mpsp_sub_categories, testing)
          if mpsp_sub_category_and_products[:mpsp_sub_categories].blank?
            active_mpsp_categories = active_mpsp_categories - [mpsp_category]
          end
        end
        return active_mpsp_categories
      end

      #
      # Get display products/MPSPs for a given sub category
      # Return empty array if it is not active
      #
      # @param mpsp_sub_category [Object] it is output of join on tables Active SubMpspCategory, Active BrandPacks,
      #                               Active MPBP and Active MPSP
      #
      # @return [Array] [Array of display products/MPSPs]
      def get_display_products_for_mpsp_sub_category(mpsp_sub_category, testing=false)
        return [] if mpsp_sub_category.blank?
        return mpsp_sub_category.marketplace_selling_packs unless GENERAL_HELPER.string_to_boolean(testing)
        active_products = mpsp_sub_category.marketplace_selling_packs.where("status = ? OR dirty_bit = ?", COMMON_MODEL_STATES::ACTIVE, true)
        return active_products
      end

      # 
      # function to validate the paginated params
      #
      def valid_paginate_params(paginate_params)
        if paginate_params[:order].present? && paginate_params[:order] != 'ASC' && paginate_params[:order] != 'DESC'
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::WRONG_ORDER)
        end
        if paginate_params[:sort_by].present? && !@mpsp_category_model.attribute_names.include?(paginate_params[:sort_by])
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::VALID_SORT_BY)
        end
      end

      #
      # Function to check if Label is unique in it's parent
      #
      def unique_label?(categories, label)
        return true if categories.blank?
        categories.each do |category|
          if category.label == label
            return false
          end
        end
        return true
      end
    end
  end
end

module CategorizationModule
  module V1
    #
    # MpspDepartment Service class to implement CRUD for mpsp_department model
    #
    class MpspDepartmentService
      #
      # Defining global variables with versioning
      #
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      MPSP_DEPARTMENT_EVENTS = CategorizationModule::V1::ModelStates::V1::DepartmentEvents
      MPSP_DEPARTMENT_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::DepartmentStates
      CATEGORY_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::CategoryStates
      MPSP_DEPARTMENT_DAO = CategorizationModule::V1::MpspDepartmentDao
      CONTENT_UTIL = CommonModule::V1::Content
      IMAGE_SERVICE = ImageServiceModule::V1::ImageService

      #
      # initialize MpspDepartmentService Class
      #
      def initialize(params)
        @params = params
        @mpsp_department_model = CategorizationModule::V1::MpspDepartment
        @mpsp_department_response_decorator = CategorizationModule::V1::MpspDepartmentResponseDecorator
        @image_service = IMAGE_SERVICE.new
      end

      #
      # function to create new mpsp_department
      #
      # Parameters::
      #   * mpsp_department_params [hash] it contains label & image url of the mpsp_department
      #
      # @return [response]
      def create_mpsp_department(mpsp_department_params)
        begin
          mpsp_department = create_mpsp_department_with_details(mpsp_department_params) 
          @mpsp_department_response_decorator.create_ok_response(mpsp_department)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @mpsp_department_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @mpsp_department_response_decorator.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # function to update mpsp_department
      #
      # Parameters::
      #   * mpsp_department_params [hash] it contains label & image url of the mpsp_department to be updated
      #
      # @return [response]
      def update_mpsp_department(mpsp_department_params)
        begin
          mpsp_department = update_mpsp_department_details(mpsp_department_params)
          @mpsp_department_response_decorator.create_ok_response(mpsp_department)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @mpsp_department_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @mpsp_department_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::DepartmentNotFoundError => e
          @mpsp_department_response_decorator.create_response_mpsp_department_not_found_error(e.message)
        end
      end

      #
      # function to change status of mpsp_department
      #
      # Parameters::
      #   * change_state_params [hash] 
      #     * id of mpsp_department of which status is to be changed
      #     * event to trigger to change the status
      #
      # @return [response]
      def change_state(change_state_params)
        begin
          mpsp_department = change_state_by_id(change_state_params)
          @mpsp_department_response_decorator.create_ok_response(mpsp_department)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @mpsp_department_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::DepartmentNotFoundError => e
          @mpsp_department_response_decorator.create_response_mpsp_department_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          @mpsp_department_response_decorator.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @mpsp_department_response_decorator.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # function to get data of a mpsp_department
      #
      # Parameters::
      #   * mpsp_department_id [integer] id of mpsp_department whose details are requested
      #
      # @return [response]
      def get_mpsp_department(mpsp_department_id)
        begin
          mpsp_department_details = get_mpsp_department_details(mpsp_department_id)
          @mpsp_department_response_decorator.create_mpsp_department_details_response(mpsp_department_details)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @mpsp_department_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::RunTimeError => e
          @mpsp_department_response_decorator.create_response_runtime_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::DepartmentNotFoundError => e
          @mpsp_department_response_decorator.create_response_mpsp_department_not_found_error(e.message)
        end
      end

      #
      # function to get label of all the mpsp_departments
      #
      # Response::
      #   * sends response with all the mpsp_department labels
      #
      # @return [response]
      def get_all_mpsp_departments(paginate_params)
        begin
          all_mpsp_departments = get_all_mpsp_departments_detail(paginate_params)
          @mpsp_department_response_decorator.create_all_mpsp_departments_response(all_mpsp_departments)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @mpsp_department_response_decorator.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # Create mpsp_department with the parameters passed
      #
      # Parameters::
      #   * mpsp_department_params [Hash] Hash of mpsp_department related data, label, image_url
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #
      # @return [MpspDepartment Object] created mpsp_department object
      #
      def create_mpsp_department_with_details(mpsp_department_param)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Label' }) if mpsp_department_param[:label].blank?
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Priority' }) if mpsp_department_param[:priority].blank?
        mpsp_department = @mpsp_department_model.create_mpsp_department(mpsp_department_param)
        return mpsp_department
      end

      #
      # Update mpsp_department with the parameters passed
      #
      # Parameters::
      #   * mpsp_department_params [Hash] Hash of mpsp_department related data, label, image_url
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::InsufficientDataError if label/id is missing
      #
      # @return [MpspDepartment Object] updated mpsp_department object
      #
      def update_mpsp_department_details(mpsp_department_param)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'MpspDepartment' }) if mpsp_department_param[:id].blank?
        mpsp_department = get_mpsp_department_by_id(mpsp_department_param[:id])
        mpsp_department = @mpsp_department_model.update_mpsp_department(mpsp_department_param, mpsp_department)
        return mpsp_department
      end

      #
      # change status of mpsp_department
      #
      # Parameters::
      #   * change_state_params [hash] 
      #     * id of mpsp_department of which status is to be changed
      #     * event to trigger to change the status
      #
      # Raise Errors::
      #   * CustomErrors::DepartmentNotFoundError, if mpsp_department not found
      #   * CustomErrors::InsufficientDataError, if id is not present
      #   * CustomErrors::PreConditionRequiredError, if transition is not allowed
      #
      # @return [MpspDepartment Object] created Category object
      #
      def change_state_by_id(change_state_params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'MpspDepartment' }) if change_state_params[:id].blank?
        mpsp_department = get_mpsp_department_by_id(change_state_params[:id])
        case change_state_params[:event].to_i
        when MPSP_DEPARTMENT_EVENTS::ACTIVATE
          mpsp_department.trigger_event('activate')
          MPSP_DEPARTMENT_DAO.activate_products_within_department(mpsp_department)
        when MPSP_DEPARTMENT_EVENTS::DEACTIVATE
          mpsp_department.trigger_event('deactivate')
          MPSP_DEPARTMENT_DAO.deactivate_products_within_department(mpsp_department)
        when MPSP_DEPARTMENT_EVENTS::SOFT_DELETE
          mpsp_department.trigger_event('soft_delete')
          MPSP_DEPARTMENT_DAO.deactivate_products_within_department(mpsp_department)
        else
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::INVALID_EVENT)
        end
        return mpsp_department
      end
      
      #
      # Get details of the mpsp_department
      #
      # Parameters::
      #   * mpsp_department_id [integer] id of mpsp_department whose details to be find
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::DepartmentNotFoundError, if mpsp_department not found
      #   * CustomErrors::InsufficientDataError, if id is not present
      #
      # @return [MpspDepartment Object] created Category object
      #
      def get_mpsp_department_details(mpsp_department_id)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'MpspDepartment' }) if mpsp_department_id.blank?
        mpsp_department_details = get_mpsp_department_by_id(mpsp_department_id)
        return mpsp_department_details
      end

      #
      # function to check the presence of a mpsp_department based on it's id
      #
      # Parameters::
      #   * mpsp_department_id [integer] id of mpsp_department to be searched
      #
      # Raise Errors::
      #   * CustomErrors::DepartmentNotFoundError if mpsp_department not found
      #
      # @return [mpsp_department Object]
      def get_mpsp_department_by_id(mpsp_department_id)
        mpsp_department = @mpsp_department_model.find_by_mpsp_department_id(mpsp_department_id)
        return mpsp_department.present? ? mpsp_department : (
          raise CUSTOM_ERRORS_UTIL::DepartmentNotFoundError.new(CONTENT_UTIL::DEPARTMENT_NOT_PRESENT))
      end

      #
      # function to get list of all the mpsp_departments
      #
      # @return [array of mpsp_department object]
      def get_all_mpsp_departments_detail(paginate_params)
        if paginate_params[:order].present? && paginate_params[:order] != 'ASC' && paginate_params[:order] != 'DESC'
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::WRONG_ORDER)
        end
        if paginate_params[:sort_by].present? && !@mpsp_department_model.attribute_names.include?(paginate_params[:sort_by])
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::VALID_SORT_BY)
        end
        all_mpsp_departments = @mpsp_department_model.paginated_mpsp_departments(paginate_params)
        return all_mpsp_departments
      end

      # 
      # function to get list of all the active mpsp_departments
      # 
      # @return [array of mpsp_department object]
      # 
      def get_active_mpsp_departments(paginate_params)
        paginate_params[:state] = MPSP_DEPARTMENT_MODEL_STATES::ACTIVE

        #setting unlimited per_page count
        paginate_params[:page_no] = CommonModule::V1::Pagination::PAGE_NO
        paginate_params[:per_page] = CommonModule::V1::Pagination::INFINITE_PER_PAGE
        all_mpsp_departments = get_all_mpsp_departments_detail(paginate_params)
        return all_mpsp_departments
      end

      #
      # This function join the mpsp_department, categories and sub_categories record
      # and return the deprtments -> categories -> sub_caeegories tree
      # 
      # @return [Json Response]
      #
      def get_all_mpsp_departments_categories_and_sub_categories
        aggregated_data = @mpsp_department_model.includes(mpsp_categories: :mpsp_sub_categories).
          where.not(status: MPSP_DEPARTMENT_MODEL_STATES::DELETED).
          where.not(mpsp_categories: { status: CATEGORY_MODEL_STATES::DELETED })

        return @mpsp_department_response_decorator.create_aggregated_data_response(aggregated_data)
      end
    end
  end
end

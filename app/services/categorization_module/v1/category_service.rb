module CategorizationModule
  module V1
    #
    # Category Service class to implement CRUD for category model
    #
    class CategoryService
      #
      # Defining global variables with versioning
      #
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CATEGORY_EVENTS = CategorizationModule::V1::ModelStates::V1::CategoryEvents
      CATEGORY_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::CategoryStates
      BRAND_PACK_SERVICE = MasterProductModule::V1::BrandPackService
      CONTENT_UTIL = CommonModule::V1::Content

      #
      # initialize CategoryService Class
      #
      def initialize(params)
        @params = params
        @category_service = CategorizationModule::V1::CategoryService
        @department_service = CategorizationModule::V1::DepartmentService
        @category_response_decorator = CategorizationModule::V1::CategoryResponseDecorator
        @department_response_decorator = CategorizationModule::V1::DepartmentResponseDecorator
        @category_model = CategorizationModule::V1::Category
      end

      #
      # function to create new category
      #
      # Parameters::
      #   * category_params [hash] it contains label & image url of the category
      #
      # @return [response]
      #
      def create_category(category_params)
        begin
          category = create_category_with_details(category_params)
          @category_response_decorator.create_ok_category_response(category)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @category_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @category_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::DepartmentNotFoundError => e
          @department_response_decorator.create_response_department_not_found_error(e.message)
        end
      end

      #
      # function to create new sub category
      #
      # Parameters::
      #   * category_params [hash] it contains label & image url of the sub category
      #
      # @return [response]
      #
      def create_sub_category(category_params)
        begin
          sub_category = create_sub_category_with_details(category_params)
          @category_response_decorator.create_ok_sub_category_response(sub_category)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @category_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @category_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          @category_response_decorator.create_response_category_not_found_error(e.message)
        end
      end

      #
      # function to update category
      #
      # Parameters::
      #   * category_params [hash] it contains label & image url of the category to be updated
      #
      # @return [response]
      def update_category(category_params)
        begin
          category = update_category_details(category_params)
          @category_response_decorator.create_ok_category_response(category)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @category_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @category_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          @category_response_decorator.create_response_category_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::DepartmentNotFoundError => e
          @department_response_decorator.create_response_department_not_found_error(e.message)
        end
      end

      #
      # function to change status of a category
      #
      # Parameters::
      #   * change_state_params [hash] 
      #     * id of category of which status is to be changed
      #     * event to trigger to change the status
      #
      # @return [response]
      def change_state(change_state_params)
        begin
          category = change_state_by_id(change_state_params)
          if category.department.present?
            @category_response_decorator.create_ok_category_response(category)
          else
            @category_response_decorator.create_ok_sub_category_response(category)
          end            
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @category_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          @category_response_decorator.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          @category_response_decorator.create_response_category_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @category_response_decorator.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # function to update category
      #
      # Parameters::
      #   * category_params [hash] it contains label & image url of the category to be updated
      #
      # @return [response]
      def update_sub_category(sub_category_params)
        begin
          sub_category = update_sub_category_details(sub_category_params)
          @category_response_decorator.create_ok_sub_category_response(sub_category)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @category_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @category_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          @category_response_decorator.create_response_category_not_found_error(e.message)
        end
      end

      #
      # function to get data of a category
      #
      # Parameters::
      #   * category_id [integer] id of category of which information is requested
      #
      # @return [response]
      #
      def get_category(category_id)
        begin
          category_details = get_category_details(category_id)
          @category_response_decorator.create_category_details_response(category_details)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          @category_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::RunTimeError => e
          @category_response_decorator.create_response_runtime_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          @category_response_decorator.create_response_category_not_found_error(e.message)
        end
      end

      #
      # function to get label of all the categories
      #
      # Response::
      #   * sends response with all the categories labels
      #
      # @return [response] 
      def get_all_categories(paginate_params)
        begin
          all_categories = get_all_categories_detail(paginate_params)
          @category_response_decorator.create_all_categories_response(all_categories)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @category_response_decorator.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # function to get label of all the sub categories
      #
      # Response::
      #   * sends response with all the sub categories labels
      #
      # @return [response] 
      def get_all_sub_categories(paginate_params)
        begin
          all_sub_categories = get_all_sub_categories_detail(paginate_params)
          @category_response_decorator.create_all_sub_categories_response(all_sub_categories)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          @category_response_decorator.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # Create category with the parameters passed
      #
      # Parameters::
      #   * category_parms [Hash] Hash of category related data, label, image_url
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::DepartmentNotFoundError, if department not found
      #   * CustomErrors::InsufficientDataError, if department id is not present
      #
      # @return [Category Object] created Category object
      #
      def create_category_with_details(category_params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Department' }) if category_params[:department_id].blank?
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Label' }) if category_params[:label].blank?
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Priority' }) if category_params[:priority].blank?
        department = get_department_by_id(category_params[:department_id])
        if unique_label?(department.categories, category_params[:label])
          category = @category_model.create_category(category_params)
          category.department = department
          category.save_category
          return category
        else
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_ALREADY_TAKEN%{ field: 'Label' })
        end
      end

      #
      # Create sub category with the parameters passed 
      #
      # Parameters::
      #   * category_parms [Hash] Hash of category related data, label, image_url
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::CategoryNotFound, if category not found
      #   * CustomErrors::InsufficientDataError, if category id is not present
      #
      # @return [Category Object] created Category object
      #
      def create_sub_category_with_details(category_params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Category' }) if category_params[:category_id].blank?
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Label' }) if category_params[:label].blank?
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Priority' }) if category_params[:priority].blank?
        category = get_category_by_id(category_params[:category_id])
        if unique_label?(category.sub_categories, category_params[:label])
          category.sub_categories << @category_model.create_category(category_params)
          return category.sub_categories.last
        else
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_ALREADY_TAKEN%{ field: 'Label' })
        end
      end

      #
      # Update category with the parameters passed
      #
      # Parameters::
      #   * category_params [Hash] Hash of category related data, label, image_url
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::InsufficientDataError if label/id is missing
      #
      # @return [category Object] updated category object
      #
      def update_category_details(category_param)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Category' }) if category_param[:id].blank?
        department = get_department_by_id(category_param[:department_id]) if category_param[:department_id].present?
        category = get_category_by_id(category_param[:id])
        category.update_category(category_param)
        return category
      end

      #
      # change status of category
      #
      # Parameters::
      #   * change_state_params [hash] 
      #     * id of category of which status is to be changed
      #     * event to trigger to change the status
      #
      # Raise Errors::
      #   * CustomErrors::DepartmentNotFoundError, if category not found
      #   * CustomErrors::InsufficientDataError, if id is not present
      #   * CustomErrors::PreConditionRequiredError, if transition is not allowed
      #
      # @return [Department Object] created Category object
      #
      def change_state_by_id(change_state_params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Category' }) if change_state_params[:id].blank?
        category = get_category_by_id(change_state_params[:id])
        case change_state_params[:event].to_i
        when CATEGORY_EVENTS::ACTIVATE
          category.trigger_event('activate')
        when CATEGORY_EVENTS::DEACTIVATE
          category.trigger_event('deactivate')
        when CATEGORY_EVENTS::SOFT_DELETE
          category.trigger_event('soft_delete')
        else
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::INVALID_EVENT)
        end
        return category
      end

      #
      # Update category with the parameters passed
      #
      # Parameters::
      #   * category_params [Hash] Hash of category related data, label, image_url
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::InsufficientDataError if label/id is missing
      #
      # @return [category Object] updated category object
      #
      def update_sub_category_details(sub_category_param)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Sub Category' }) if sub_category_param[:id].blank?
        category = get_category_by_id(sub_category_param[:category_id]) if sub_category_param[:category_id].present?
        sub_category = get_category_by_id(sub_category_param[:id])
        sub_category.update_category(sub_category_param)
        return sub_category
      end

      #
      # Get details of the  category
      #
      # Parameters::
      #   * category_id [integer] id of category of which information is requested
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError if creation failed (mainly due to validation)
      #   * CustomErrors::CategoryNotFound, if category not found
      #   * CustomErrors::InsufficientDataError, if category id is not present
      #
      # @return [Category Object] created Category object
      #
      def get_category_details(category_id)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Category' }) if category_id.blank?
        category_details = get_category_by_id(category_id)
        return category_details
      end

      #
      # function to check the presence of a category based on it's id
      #
      # Parameters::
      #   *category_id [ineteger] id of category to be searched
      #
      # @return [category object]
      def get_category_by_id(category_id)
        category = @category_model.find_by_category_id(category_id)
        return category.present? ? category : (raise CUSTOM_ERRORS_UTIL::CategoryNotFoundError.new(CONTENT_UTIL::CATEGORY_NOT_PRESENT))
      end

      #
      # function to call department_exists in DepartmentService
      #
      # Parameters::
      #   *department_id [ineteger] id of category to be searched
      #
      # @return [department object]
      def get_department_by_id(department_id)
        department_service = @department_service.new(@params)
        department = department_service.get_department_by_id(department_id)
        return department
      end

      #
      # function to get list of all the categories
      #
      # @return [array of categories object]
      def get_all_categories_detail(paginate_params)
        valid_paginate_params(paginate_params)
        all_categories = @category_model.paginated_categories(paginate_params)
        return all_categories
      end

      #
      # function to get list of all the  sub categories
      #
      # @return [array of sub categories object]
      def get_all_sub_categories_detail(paginate_params)
        valid_paginate_params(paginate_params)
        all_sub_categories = @category_model.paginated_sub_categories(paginate_params)
        return all_sub_categories
      end

      # 
      # function to get list of all the  active categories under a department
      # 
      # @return [array of categories object]
      def get_active_categories_by_department(department)
        all_categories = @category_model.get_active_categories_by_department(department)
        return all_categories
      end

      #
      # Get display products/MPSPs for a given sub category
      # Return empty array if it is not active
      #
      # @param sub_category [Object] it is output of join on tables Active SubCategory, Active BrandPacks,
      #                               Active MPBP and Active MPSP
      #
      # @return [Array] [Array of display products/MPSPs]
      def get_display_products_for_subcategory(sub_category)

        display_products = []

        brand_packs = sub_category.brand_packs
        brand_pack_service = BRAND_PACK_SERVICE.new
        brand_packs.each do |brand_pack|
          mp_selling_packs = brand_pack_service.get_marketplace_selling_packs_for_brand_pack(brand_pack)
          display_products = display_products | mp_selling_packs
        end
        return display_products
      end

      # 
      # function to validate the paginated params
      #
      def valid_paginate_params(paginate_params)
        if paginate_params[:order].present? && paginate_params[:order] != 'ASC' && paginate_params[:order] != 'DESC'
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::WRONG_ORDER)
        end
        if paginate_params[:sort_by].present? && !@category_model.attribute_names.include?(paginate_params[:sort_by])
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::VALID_SORT_BY)
        end
      end

      #
      # Function to check if Label is unique in it's parent
      #
      def unique_label?(categories, label)
        return true if categories.blank?
        categories.each do |category|
          if category.label == label
            return false
          end
        end
        return true
      end
    end
  end
end

#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    #
    # This class is responsible to upload image to S3
    #
    class ImageService < BaseModule::V1::BaseService
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      GENERAL_HELPER = CommonModule::V1::GeneralHelper
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper
      IMAGE_MODEL = ImageServiceModule::V1::Image
      IMAGE_SERVICE_DECORATOR = ImageServiceModule::V1::ImageServiceResponseDecorator
      IMAGE_MODEL_EVENTS = ImageServiceModule::V1::ModelStates::ImageEvents
      MAX_IMAGE_PRIORITY = 2**32

      def initialize(params = '')
        @params = params
      end

      #
      # Given directory it uploads all the images in that directory to local path of rails server
      # and returns the hash of image_id and priority
      #
      # @param image_folder [String] image directory containing image files
      #
      # @return [Array] array of hash where each hash contain image_id and priority
      # 
      def upload_images_from_directory(image_folder)
        raise CUSTOM_ERRORS_UTIL::DirectoryNotFound.new('Directory not found, ' + image_folder) unless File.directory?(image_folder)
        
        #
        # Image name are according to their priority example
        # suppose in folder 470 there are 3 images then name of these
        # files should be 470_1.extension, 470_2.extension and 470_3.extension
        # digit after folder define the priority
        #
        all_files = Dir.glob(image_folder + '/*')

        images_array = []
        all_files.each do |file|
          next unless File.file?(file)
          priority = get_priority_from_file_path(file)
          image_model = IMAGE_MODEL.new
          image_model.image_local = File.open(file)
          image_model.save!
          images_array.push({'image_id' => image_model.id, 'priority' => priority})
        end
        return images_array
      end

      #
      # Assuming file_path contain file_name ended with 'folder/folder_{#priority}.ext'
      # example file_path = /home/imli_images/470/470_1.jpg
      # then split_file_path = ['/home/imli', 'images/470/470', '1.jpg']
      # file_name_postfix = '1.jpg'
      # file_priority = 1
      # 
      def get_priority_from_file_path(file_path)
        split_file_path = file_path.split('_')
        return MAX_IMAGE_PRIORITY if split_file_path.length <= 1
        file_name_postfix = split_file_path[-1]
        split_file_path_by_dot = file_name_postfix.split('.')
        return MAX_IMAGE_PRIORITY if split_file_path_by_dot.length <= 1
        file_priority = split_file_path_by_dot[0]
        return file_priority
      end

      #
      # upload image locally
      #
      # @param args [Hash] hash of files
      #
      # @return [JsonResponse] list of images hash containing image_id and image_url
      #                         or error response if it fails to upload
      #
      def upload_image(args)
        if args.blank?
          return IMAGE_SERVICE_DECORATOR.create_response_invalid_data_passed('should contain valid image')
        end

        images_array = []

        args_keys = args.keys
        args_keys.each do |key|
          image_model = IMAGE_MODEL.new
          begin
            image_model.image_s3 = args[key]
            image_model.save!
            cdn_image_url = IMAGE_SERVICE_HELPER.get_cdn_url(image_model.image_s3)
            images_array.push({ image_id: image_model.id, image_url: cdn_image_url })
          rescue => e
            return IMAGE_SERVICE_DECORATOR.create_response_runtime_error(e.message)
          end
        end

        return IMAGE_SERVICE_DECORATOR.create_image_response(images_array)
      end

      # 
      # This function change image state
      #
      # @param image_id [Integer] Image id
      # @param event [Integer]  allowed image event
      # 
      # @return [Model] image object if succesfully updated the status
      # 
      # @error ResourceNotFoundError if image not found with id
      #        InvalidArgumentsError if event is not valid
      #        PreConditionRequiredError if transition not allowed
      #
      def change_image_state(args)
        
        image = get_image_by_id(args[:id])

        event = args[:event]

        raise CUSTOM_ERRORS_UTIL::ResourceNotFoundError.new(CONTENT::INVALID_IMAGE_ID) if image.blank?

        begin
          case event.to_i
            when IMAGE_MODEL_EVENTS::ACTIVATE
              image.activate!
            when IMAGE_MODEL_EVENTS::DEACTIVATE
              image.deactivate!
            when IMAGE_MODEL_EVENTS::SOFT_DELETE
              image.soft_delete!
            else
              raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
          end
        rescue Workflow::NoTransitionAllowed => e
          raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(e.message)
        end
        return image
      end
      #
      # This invokes the common image worker to move local image to s3
      #
      # @param [Array] array of image_ids
      #
      def upload_async(args)
        common_image_worker = ImageServiceModule::V1::CommonImageWorker
        common_image_worker.perform_async(args)
      end

      #
      # This function takes the array of image_ids and move the local save image
      # to s3 corresponding to the image_ids
      #
      # @params args [Array] array containing image id's to move
      #
      def sync_image_with_remote_storage(args)

        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new('args can not be blank') if args.blank?

        args.each do |image_id|
          image = get_image_by_id(image_id)
          next if image.nil?
          if image.image_local.present?
            begin
              image.image_s3 = File.open(image.image_local.path)
              image.save!
              delete_file(image.image_local.path)
              image.image_local = nil
              image.save!
              IMAGE_SERVICE_HELPER.update_cached_urls(image)
            rescue => e
              raise CUSTOM_ERRORS_UTIL::RunTimeError.new(e.message)
            end
          end
        end
        return true
      end

      #
      # Return image if image corresponding to image_id is present else return nil
      #
      # @param [String] image id
      #
      def get_image_by_id(image_id)
        IMAGE_MODEL.find_by(id: image_id)
      end

      #
      # Delete file if File exists
      #
      # @param path [String] local path
      #
      def delete_file(path)
        File.delete(path) if File.file?(path)
      end
    end
  end
end

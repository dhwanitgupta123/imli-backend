#
# This module handle all functionalities of scripts 
#
module ScriptsModule
  #
  # Version1 fror scripts module 
  #
  module V1
    class ScriptsService < BaseModule::V1::BaseService

      require 'csv'

      AMAZON_BASE_URL = 'http://www.amazon.in/dp/'
      MARKETPLACE_SELLING_PACK_DAO = MarketplaceProductModule::V1::MarketplaceSellingPackDao
      PYTHON_PATH = File.join(File.dirname(__FILE__),'get_others_price.py')
      OUTPUT_FILE_PATH = File.join(Rails.root,'tmp/product_codes_output.csv')
      OUTPUT_FILE_S3_KEY = 'product_codes/output.csv'
      FILE_SERVICE_UTIL = FileServiceModule::V1::FileServiceUtil
      CACHE_UTIL = CommonModule::V1::Cache

     def initialize(params = '')
        @params = params
      end

      # 
      # This function reads the csv file from the input file path
      # and crawl each url from different sources and output the
      # price comparision between ample, bigbasket and amazon
      #
      # @param file_path [String] input csv path
      # 
      # @return [String] s3 file path
      #
      def initiate_crawler(file_path)

        @marketplace_selling_pack_dao = MARKETPLACE_SELLING_PACK_DAO.new

        output_file = CSV.open(OUTPUT_FILE_PATH, 'w')

        header = ['BP_Code', 'Mpsp_Id', 'Product_Name', 'Big_Basket_Url', 'Amazon_Url', 'Purplle_Url', 'Our_Price', 'Big_Basket_Price', 
                  'Amazon_Price', 'Purplle_Price']
        output_file << header

        first_row = 0

        CSV.foreach(file_path) do |row|

          if first_row == 0
            first_row = 1
            next
          end

          output_row = []
          output_row.push(row[0])
          output_row.push(row[1])
          output_row.push(row[2])
          big_basket_url = 'N/A'

          if row[3].present?
            big_basket_url = row[3]
          end

          output_row.push(big_basket_url)

          amazon_url = 'N/A'

          if row[4].present?
            amazon_url = AMAZON_BASE_URL + row[4]
          end

          output_row.push(amazon_url)

          purplle_url = 'N/A'

          if row[5].present?
            purplle_url = row[5]
          end

          output_row.push(purplle_url)

          our_price = 'N/A'

          if Rails.env == 'production'
            our_price = get_our_price(row[1])

            if our_price.blank?
              output_row[1] = output_row[1] + '# Not A Valid MPSP ID' if output_row[1].present?
              output_row.push('')
              output_row.push('')
              output_row.push('')
              output_file << output_row
              next
            end
          end
          
          output_row.push(our_price.to_s)
          
          #
          # Running python script to get amazon and big basket prices
          # 
          others_price = %x(python #{PYTHON_PATH} #{amazon_url} #{big_basket_url} #{purplle_url})
          others_price = others_price[0..-2]
          others_price = others_price.split('#')
          
          output_row.push(others_price[0])
          output_row.push(others_price[1])
          output_row.push(others_price[2])
          output_file << output_row
        end
        output_file.close

        upload_file_to_s3
      end

      # 
      # This function upload file to s3
      #
      def upload_file_to_s3
        options = {
          acl: CACHE_UTIL.read('ACCESS_CONTROL_LISTS')
        }
        file_service_util = FILE_SERVICE_UTIL.new
        file_service_util.upload_file( { 
            key: OUTPUT_FILE_S3_KEY,
            file_path: OUTPUT_FILE_PATH,
            options: options
          })
      end

      # 
      # This function returns ample price
      #
      # @param mpsp_id [Integer] mpsp id
      # 
      # @return [String] base selling price of mpsp
      #
      def get_our_price(mpsp_id)

        mpsp = @marketplace_selling_pack_dao.get_by_id(mpsp_id)
        return nil if mpsp.blank?

        pricing = mpsp.marketplace_selling_pack_pricing

        return pricing.base_selling_price
      end

      #
      # This function initialize craler asynchronously 
      #
      def initiate_crawler_async(file_path)
        ScriptsModule::V1::PriceComparatorWorker.perform_async(file_path)
      end
    end
  end
end

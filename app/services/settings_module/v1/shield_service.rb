module SettingsModule
  module V1
    class ShieldService < BaseModule::V1::BaseService

      SHIELD = SettingsModule::V1::Shield

      # 
      # Initialize Shield service with required params
      #
      def initialize(params)
        @params = params
      end

      #
      # Get Treatment Value corresponding to the requester
      # Currently the deciding factor is only 'user_id'. Later on, it can
      # be extended to Marketplace, Location, Platform, etc.
      #
      # @param key [String] [Key for which SHIELD treatment value is to be fetched]
      #
      # @return [String] [Value of the treatment]
      #
      def self.get_treatment_value(key)
        SHIELD.get_treatment_value(key)
      end

      #
      # Start SHIELD experiment for a particular user
      # Currently the deciding factor is only 'user_id'. Later on, it can
      # be extended to Marketplace, Location, Platform, etc.
      #
      # @param key [String] [Name of the experiment for which SHIELD experiment is to be started]
      #
      # @return [String] [Value of the experiment]
      #
      def self.start_experiment(experiment_name)
        SHIELD.start_experiment(experiment_name)
      end

      #
      # Finish SHIELD experiment for a particular user
      # Currently the deciding factor is only 'user_id'. Later on, it can
      # be extended to Marketplace, Location, Platform, etc.
      #
      # @param key [String] [Name of the experiment for which SHIELD experiment is to be finished]
      #
      # @return [String] [Value of the experiment]
      #
      def self.finish_experiment(experiment_name)
        SHIELD.finish_experiment(experiment_name)
      end

      #
      # Get static feature value for a particular key
      #
      # @param key [String] [Key for which value is to be fetched]
      #
      # @return [String] [Value corresponds to the given key]
      #
      def self.get_feature_value(key)
        SHIELD.get_feature_value(key)
      end

    end
  end
end
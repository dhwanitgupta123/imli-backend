require 'json'
module SettingsModule
  module V1
    class SettingService < BaseModule::V1::BaseService
      # Add versioned classed in global constants
      SHIELD_SERVICE = SettingsModule::V1::ShieldService

      MAP_SETTINGS = SettingsModule::V1::StaticSettings::MapSettings
      FEATURE_SETTINGS = SettingsModule::V1::StaticSettings::FeatureSettings
      CONSTANT_SETTINGS = SettingsModule::V1::StaticSettings::ConstantSettings
      DYNAMIC_SETTINGS = SettingsModule::V1::DynamicSettings::DynamicSettings


      # Constants containing file directory path

      # 
      # Initialize Employee service with required params
      #
      # @param params [Hash] Hash object of parameters
      #
      def initialize(params)
        @params = params
        super
      end

      #
      # Get all the settings required on App side
      # Keys:
      #   - MAP --> contains all maps of states, events, etc.
      #   - FEATURES --> contains feature enable/disable keys
      #   - CONSTANTS --> contains all relevant constants needed on App side
      #
      # @return [type] [description]
      #
      def get_all_static_settings
        begin
          # Fetch settings from Map files
          map_settings_instance = MAP_SETTINGS.new(@params)
          feature_settings_instance = FEATURE_SETTINGS.new(@params)
          constant_settings_instance = CONSTANT_SETTINGS.new(@params)
          map_settings  = map_settings_instance.fetch
          feature_settings = feature_settings_instance.fetch
          required_constants = constant_settings_instance.fetch

          settings = {}
          settings['MAP'] = map_settings
          settings['FEATURES'] = feature_settings
          settings['CONSTANTS'] = required_constants
          return settings
        rescue => e
          return {}
        end
      end

      #
      # Fetch dynamic settings map, and fetch user specific values for the map
      #
      # @return [JSON] [Hash of user specific dynamic settings]
      #
      def get_all_dynamic_settings
        # Call Shield Service to fetch user specific settings
        dynamic_settings_instance = DYNAMIC_SETTINGS.new(@params)
        dynamic_settings = dynamic_settings_instance.fetch

        return {} if dynamic_settings.blank?
        # As per the dynamic settings map (used for filtering/whitelisting)
        # fetch corresponding user specific values
        user_specific_settings = fetch_user_specific_settings(dynamic_settings)
        return user_specific_settings
      end

      #
      # Fetch user specific values for given filtered map
      # Filtered Map: Hash containing
      #   - key: Name of the feature
      #   - value: Name of the experiment
      #
      # @param filtered_map [JSON] [Hash containing whitelisted values whose values are to be fetched]
      #
      # @return [JSON] [Hash of user specific settings with key as setting_name and value as feature value]
      #
      def fetch_user_specific_settings(filtered_map)
        return {} if filtered_map.blank?
        user_specific_settings = {}
        filtered_map.each do |key, value|
          treatment_value = SHIELD_SERVICE.get_treatment_value(value.to_s) || '0'
          user_specific_settings[key.to_s] = treatment_value
        end
        return user_specific_settings
      end

    end
  end
end
module UsersModule
  module V1
    # Service layer to handle logic related to user roles
    class RoleService < BaseModule::V1::BaseService
      # Defining versioning globally as constants
      USER_SERVICE = UsersModule::V1::UserService
      ACCESS_CONTROL_SERVICE = UsersModule::V1::AccessControlService
      ROLE_MODEL = UsersModule::V1::Role
      ROLE_RESPONSE_DECORATOR = UsersModule::V1::RoleResponseDecorator
      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      #
      # Initializing role service
      # @param params [JSON] [parameters required for versioning at various levels]
      #
      # @return [type] [description]
      def initialize(params)
        @params = params
      end

      #
      # Create Role with given params
      #
      # @param args [Hash] Hash containing role_name and permissions array
      #
      # @return [Response] response to be sent to the user
      #
      def create_role(role_params)
        begin
          response = create_role_with_params(role_params)
          return response
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError
          ROLE_RESPONSE_DECORATOR.create_response_bad_request
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          ROLE_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      #
      # Logic for creating role based on given params
      #
      # @param args [Hash] Hash containing role_name and permissions array
      #
      # @return [Response] response to be sent to the user
      #
      # @raise [InsufficientDataError] if role_name or permissions array is not present in hash passed
      # @raise [InvalidArgumentsError] if unable to create Role based on given params
      def create_role_with_params(role_params)
        initialize_params(role_params)
        validate_role_params
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new('Parent Role not specified') if role_params[:parent_id].blank?
        begin
          parent_role = get_role_from_role_id(role_params[:parent_id])
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Parent Role does not exist')
        end
        role = ROLE_MODEL.new_role({
          role_name: @role_name,
          permissions: @permissions,
          parent: parent_role
          })
        role.save_role
        return ROLE_RESPONSE_DECORATOR.create_ok_response(role)
      end

      #
      # Update Role with given params
      #
      # @param args [Hash] Hash containing role_id and role_params
      # (role_params consists role_name and permissions array)
      #
      # @return [Response] response to be sent to the user
      #
      def update_role(params)
        begin
          response = update_role_with_params(params)
          return response
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          ROLE_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          ROLE_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      #
      # Logic for updating role based on given params
      #
      # @param args [Hash] Hash containing role_id and role_params ( role_name and permissions array )
      #
      # @return [Response] response to be sent to the user
      #
      # @raise [InsufficientDataError] if role_name or permissions array is not present in hash passed
      # @raise [InvalidArgumentsError] if unable to find Role based on role_id OR unable to save role because
      # of invalid parameters
      def update_role_with_params(params)
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new('No Arguments passed') if params.blank?
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new('Role id not present') if params[:role_id].blank?
        initialize_params(params[:role_params])
        role = get_role_from_role_id(params[:role_id])
        validate_role_params
        role.update_role({
          role_name: @role_name,
          permissions: @permissions
          })
        role.save_role
        return ROLE_RESPONSE_DECORATOR.create_ok_response(role)
      end

      #
      # Get role of a particular user. Requested user can request for only his roles
      # User id is already been extracted out from session-token passed in request
      #
      # @return [Response] response to be sent to the user
      #
      def get_user_role
        begin
          response = get_roles_associated_with_user
          return response
        rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError
          USER_RESPONSE_DECORATOR.create_response_unauthenticated_user
        end
      end

      #
      # Logic for getting role associated with a particular user
      # User id is already been extracted out from session-token passed in request
      #
      # @return [Response] response to be sent to the user
      #
      # @raise [UnAuthenticatedUserError] if no user exists for passed session token
      #
      def get_roles_associated_with_user
        user_service = USER_SERVICE.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])

        roles = user_service.get_roles_of_user(user)
        return ROLE_RESPONSE_DECORATOR.create_response_user_roles(roles)
      end

      #
      # Get complete Roles tree under the requested user role
      # User id is already been extracted out from session-token passed in request
      #
      # @return [Response] response to be sent to the user
      #
      def get_roles_tree
        begin
          response = get_roles_tree_of_user
          return response
        rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError
          USER_RESPONSE_DECORATOR.create_response_unauthenticated_user
        end
      end

      #
      # Logic for getting complete Roles tree under the requested user role
      # User id is already been extracted out from session-token passed in request
      #
      # @return [Response] response to be sent to the user
      #
      # @raise [UnAuthenticatedUserError] if no user exists for passed session token
      #
      def get_roles_tree_of_user
        user_service = USER_SERVICE.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])

        roles = user_service.get_roles_of_user(user)
        return ROLE_RESPONSE_DECORATOR.create_response_roles_tree(roles)
      end

      #
      # Get all users who have the given role
      #
      # @param args [Hash] Hash containing role_id
      #
      # @return [Response] response to be sent to the user
      #
      def get_users_for_role(role_id)
        begin
          response = get_users_associated_with_role(role_id)
          return response
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          ROLE_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          ROLE_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      #
      # Logic for getting all users associated with a particular role
      #
      # @param args [Hash] Hash containing role_id
      #
      # @return [Response] response to be sent to the user
      #
      # @raise [InsufficientDataError] if role_id is not present in the passed hash
      # @raise [InvalidArgumentsError] if unable to find Role based on given role_id
      #
      def get_users_associated_with_role(role_id)
        if role_id.blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new
        end
        role = get_role_from_role_id(role_id)
        users = role.get_users
        return USER_RESPONSE_DECORATOR.create_users_detail_response(users)
      end

      #
      # Get all available permissions
      # It calls access control service to fetch all available permissions
      # and then create a response with that permissions array
      #
      # @return [Response] response to be sent to the user
      #
      def get_all_permissions
        begin
          access_control_service = ACCESS_CONTROL_SERVICE.new(@params)
          permissions = access_control_service.get_all_permissions
          response = ROLE_RESPONSE_DECORATOR::create_permissions_response(permissions)
          return response
        rescue
          ROLE_RESPONSE_DECORATOR.create_response_runtime_error('Unable to fetch permissions')
        end
      end

      #
      # Get Role associated with passed role_id
      # @param role_id [Integer] [id for which Role is to be found]
      #
      # @return [Role] [Role object associated with the passed id]
      #
      # @raise [InvalidArgumentsError] [if no role found]
      #
      def get_role_from_role_id(role_id)
        ROLE_MODEL.find_by_role_id(role_id)
      end

      # 
      # Get Role with passed parameters (WHERE query)
      #
      def get_role_with_parameters(params)
        ROLE_MODEL.get_role_with_parameters(params)
      end

      #
      # Create Root role with passed parameters
      #
      def create_root_role_with_parameters(params)
        ROLE_MODEL.create_root_role_with_parameters(params)
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # [Initialize parameters]
      # @param role_params [Hash] [Hash containing role_name and permissions array]
      #
      # @raise [InsufficientDataError] [if Hash passed is blank]
      #
      def initialize_params(role_params)
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new('Role parameters not present') if role_params.blank?
        @role_name = role_params[:role_name] if role_params[:role_name].present?
        @permissions = role_params[:permissions] if role_params[:permissions].present?
      end

      #
      # Validate role parameters.
      #
      # @raise [InsufficientDataError] [if role_name or permissions are blank]
      def validate_role_params
        if @role_name.blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new('Role name is blank')
        end
        if @permissions.blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new('Permissions are blank')
        end
      end

    end
  end
end
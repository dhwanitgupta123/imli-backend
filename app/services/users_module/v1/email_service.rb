module UsersModule
  module V1
    class EmailService < BaseModule::V1::BaseService
      #
      # Initialize EmailService class.
      # Mention all the classes which are going to be used in this
      # file in global scoped variables
      #
      def initialize
        @email_model = UsersModule::V1::Email
        @custom_error_util = CommonModule::V1::CustomErrors
        @content_util = CommonModule::V1::Content
      end


      # 
      # Add email for the user
      # @param args [Hash] Hash of parameters required for email creation
      # Must Params:
      #  * email_id : email id of the useer
      #  * account_type : account type of the user, customer/employee/vendor,etc etc.
      #  * user : user to which this created email should belong to
      # 
      # @return [Email object] if successfully created
      # @raise [@custom_error_util::RunTimeError] if creation failed
      def add_email_for_user(args)
        if args[:email][:is_primary]
          change_primary_email_of_user(args[:user])
        end
        email = @email_model.new
        email.email_id = args[:email][:email_id]
        email.is_primary = args[:email][:is_primary]
        email.account_type = args[:account_type]
        email.verified = false
        email.user = args[:user]
        # save changes. Raises exception if failed
        email.save_email
        return email
      end

      # 
      # Function to change other user email id's is primary tag 
      # if they are set to true
      #
      def change_primary_email_of_user(user)
        emails = user.emails
        emails.each do |email|
          email.is_primary = false
          email.save_email
        end
      end


      # 
      # Authenticate email address
      # @param email_token [String] token associated with the email
      # 
      # @return [User object] user object associated with the email token
      #
      # @raise [@custom_error_util::InvalidDataError] if token passed is invalid
      # (no user or email is associated with it)
      def authenticate_email(email_token)
        begin
          email = get_email_by_authentication_token(email_token)
          raise @custom_error_util::InvalidDataError.new(@content_util::EMAIL_NOT_FOUND) if email.blank?

          if token_expired?(email)
            raise @custom_error_util::AuthenticationTimeoutError.new(@content_util::TOKEN_EXPIRED)
          end

          # Fetch user corresponding to email object
          # To validate whether this email is associated with active valid user
          user = get_user_for_email(email)

          # Verify the email address
          raise @custom_error_util::EmailAlreadyVerifiedError.new(@content_util::EMAIL_VERIFIED) if email_verified?(email)
          email.set_verify_to_true

          #Save the changes
          email.save_email

          return user
        rescue @custom_error_util::UserNotFoundError, @custom_error_util::InvalidDataError => e
          raise @custom_error_util::InvalidDataError.new(e.message)
        end
      end

      #
      # Get Email corresponding to passed email id
      #
      # @param email_id [String] Email id of the user
      #
      # @return [Email] [email object associated with email id]
      #
      # @raise [EmailNotFoundError] [when email not found associated ]
      def get_email_from_email_id(email_id)
        email = @email_model.find_by_email_id(email_id)
        return email
      end

      #
      # Get email with accout type
      # @param email_id [String] [email id to be searched]
      # @param account_type [Integer] [account type to be matched]
      #
      # @return [Object] [Email Object matching above mentioned criteria]
      #
      def get_email_with_type(email_id, account_type)
        emails = @email_model.where(email_id: email_id, account_type: account_type)
        if emails.present?
          return emails.first
        end
      end

      #
      # Assign new authentication token to email
      #
      # @param email [Email] [Email object]
      #
      # @raise [RunTimeError] [if unable to save to DB]
      def assign_new_authentication_token(email)
        if token_expired?(email)
          # Create new authentication token and increase its expiry time.
          email.set_authentication_token
          email.set_expires_at
          email.save_email
        end
        # ELSE
        # No Action :: If token is not expired, then let the token and expiry time
        # be same. This is to stop attacks/misuse v/s very rare cases
        # of bad customer experience
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # Fetch email object corresponding to provided authentication token
      # @param email_token [String] authentication token associated with the email
      # 
      # @return [Email Object] if email found with respect to token passed
      # @raise [@custom_error_util::InvalidDataError] if record not found
      def get_email_by_authentication_token(token)
        begin
          email = @email_model.find_by_authentication_token(token)
          return email
        rescue ActiveRecord::RecordNotFound
          raise @custom_error_util::InvalidDataError.new(@content_util::EMAIL_NOT_FOUND)
        end
      end

      # 
      # Fetch user corresponding to given email
      # @param email [Email Object] email of the user
      # 
      # @return [User Object] user associated with that email
      # @raise [@custom_error_util::UserNotFoundError] If no user associated with that email
      def get_user_for_email(email)
        user = email.user
        if user.present?
          return user
        else
          raise @custom_error_util::UserNotFoundError.new(@content_util::USER_NOT_FOUND)
        end
      end

      # 
      # Check whether email verified is set or not
      # @param email [Object] Email Object which need to be verified
      # 
      # @return [Boolean] true if verified
      # Returns the value of verified parameter of email model
      def email_verified?(email)
        if email.verified.blank?
          return false
        end
        return email.verified
      end

      # 
      # Validate email token expiry
      # @param email [Object] Email object which needs to validate
      # 
      # @return [true] if token validated
      # @raise [AuthenticationTimeoutError] if token already expired
      def token_expired?(email)
        if (Time.zone.now > email.expires_at)
          return true
        else
          return false
        end
      end

    end
  end
end

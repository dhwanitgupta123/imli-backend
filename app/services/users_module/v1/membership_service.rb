module UsersModule
  module V1
    #
    # Memberhsip service to handle the membership related logic 
    #
    class MembershipService < BaseModule::V1::BaseService

      PLAN_KEYS= CommonModule::V1::PlanKeys
      PLAN_STATES = CommonModule::V1::ModelStates::V1::PlanStates
      MEMBERSHIP_STATES = UsersModule::V1::ModelStates::MembershipStates
      MEMBERSHIP_EVENTS = UsersModule::V1::ModelStates::MembershipEvents
      MEMBERSHIP = UsersModule::V1::Membership
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      PAYMENT_SERVICE = UsersModule::V1::MembershipModule::V1::MembershipPaymentService
      PROFILE_SERVICE = UsersModule::V1::ProfileService
      MEMBERSHIP_HELPER = UsersModule::V1::MembershipHelper
      MEMBERSHIP_DAO = UsersModule::V1::MembershipDao
      MEMBERSHIP_REWARD = 30

      # 
      # initialize required classes
      # 
      def initialize(params)
        @params = params
        @plan_service = CommonModule::V1::PlanService
        @user_service = UsersModule::V1::UserService
        @membership_response_decorator = UsersModule::V1::MembershipResponseDecorator
        @user_response_decorator = UsersModule::V1::UserResponseDecorator
      end

      # 
      # [subscribe_to_membership API function]
      # @param membership_params [hash] has plan_id, required
      # 
      # @return [MembershipModel] membership object
      # 
      def subscribe_to_membership(membership_params)
        begin
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Plan' }) if membership_params.first[:plan].blank?
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Plan ID' }) if membership_params.first[:plan][:id].blank?
          user_service = @user_service.new(@params)
          user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
          membership_details = transactional_subscribe_user_to_membership(membership_params.first, user)
          user.memberships << membership_details[:membership]
          user.save_user
        return @user_response_decorator.create_user_membership_payment_details_response(user, membership_details[:payment_details])
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return @membership_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidArgumentsError, CUSTOM_ERRORS_UTIL::InvalidDataError => e
          return @membership_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return @membership_response_decorator::create_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          return @membership_response_decorator.create_response_bad_request(e.message)
        end
      end

      # 
      # [payment_initiate API function]
      # @param membership_params [hash] has payment mode, required
      # 
      # @return [MembershipModel] membership object
      # 
      def payment_initiate(membership_params)
        begin
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Payment' }) if membership_params.first[:payment].blank?
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Payment mode' }) if membership_params.first[:payment][:mode].blank?
          user_service = @user_service.new(@params)
          user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
          response = transactional_payment_initiate(membership_params.first, user)
        return response
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return @membership_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidArgumentsError, CUSTOM_ERRORS_UTIL::InvalidDataError => e
          return @membership_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return @membership_response_decorator::create_not_found_error(e.message)
        end
      end

      # 
      # [payment_success API function]
      # @param membership_params [hash] has payment ID, required
      # 
      # @return [MembershipModel] membership object
      # 
      def payment_success(membership_params)
        begin
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Payment' }) if membership_params.first[:payment].blank?
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Payment ID' }) if membership_params.first[:payment][:id].blank?
          user_service = @user_service.new(@params)
          user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
          membership = transactional_payment_success(membership_params.first, user)
        return @user_response_decorator.create_user_membership_response(user) if membership.present?
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return @membership_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidArgumentsError, CUSTOM_ERRORS_UTIL::InvalidDataError => e
          return @membership_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return @membership_response_decorator::create_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          return @membership_response_decorator.create_response_bad_request(e.message)
        end
      end

      # 
      # Transactional function to subscribe user to function
      #
      def transactional_subscribe_user_to_membership(membership_params, user)
        transactional_function = Proc.new do |args|
          return subscribe_user_to_membership(args[:membership_params], args[:user])
        end
        transaction_status = TransactionHelper.new({
          :function => transactional_function,
          :args => { membership_params: membership_params, user: user }
          })
        transaction_status.run();
      end

      # 
      # logic function for subscribe_to_membership API function
      #  
      # @param membership_params [hash] has plan_id, required
      # 
      # @return [MembershipModel] membership object
      # 
      def subscribe_user_to_membership(membership_params, user)
        plan_service = @plan_service.new(@params)
        plan_id = membership_params[:plan][:id]
        requested_plan = plan_service.get_plan_by_id(plan_id)
        if requested_plan.status != PLAN_STATES::ACTIVE
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT::MUST_BE_ACTIVE % { name: 'Plan' })
        end

        active_user_membership = get_active_user_membership(user)
        membership_args = {
          plan: requested_plan,
          starts_at: Time.zone.now,
          expires_at: Time.zone.now + requested_plan.duration.days,
          quantity:  1
        }
        membership_details = validate_existing_membership(active_user_membership, membership_args)
        return membership_details
      end

      # 
      # Transactional function to rollback DB instances in case of some error
      # during payment initiation of membership subscription
      #
      def transactional_payment_initiate(membership_params, user)
        transactional_function = Proc.new do |args|
          membership = initiate_membership_payment(args[:membership_params], args[:user])
          return @user_response_decorator.create_user_membership_response(user)
        end
        transaction_status = TransactionHelper.new({
          :function => transactional_function,
          :args => { membership_params: membership_params, user: user }
          })
        transaction_status.run();
      end

      # 
      # Function to initiate payment for a memberhsip subscription
      #   * It fetches the payment pending membership of user
      #   * Calculates the final payment amount based on the payment mode
      #   * returns the updated membership
      #
      def initiate_membership_payment(membership_params, user)
        payment = PAYMENT_SERVICE.new
        membership_model = MEMBERSHIP.new
        membership_dao = MEMBERSHIP_DAO.new(@params)
        user_membership = membership_dao.get_payment_pending_membership(user)
        user_membership.membership_payment = payment.final_membership_payment(user_membership, membership_params[:payment])
        user_membership.save_membership
        return user_membership
      end

      # 
      # Transactional function when payment of membership subscription is successful
      #
      def transactional_payment_success(membership_params, user)
        transactional_function = Proc.new do |args|
          return success_membership_payment(args[:membership_params], args[:user])
        end
        transaction_status = TransactionHelper.new({
          :function => transactional_function,
          :args => { membership_params: membership_params, user: user }
          })
        transaction_status.run();
      end

      # 
      # Function when payment was successful
      #  * It validates if the Payment id is of correct membership
      #  * Sets Starts at & expires at attribute of membership and updates it
      #  * If users active membership has a same plan as the new one, it updates membership args accordingly
      #  * Activates the membership
      #
      def success_membership_payment(membership_params, user)
        payment = PAYMENT_SERVICE.new
        membership_model = MEMBERSHIP.new
        profile_service = PROFILE_SERVICE.new(@params)
        membership_dao = MEMBERSHIP_DAO.new(@params)
        user_membership = membership_dao.get_payment_pending_membership(user)

        starts_at = Time.zone.now
        expires_at = starts_at + user_membership.plan.duration.days

        payment = payment.get_payment_by_id(membership_params[:payment])
        if user_membership.membership_payment == payment
          active_membership = membership_dao.get_only_active_membership(user)
          if active_membership.present?
            if(active_membership.plan == user_membership.plan)
              starts_at = active_membership.starts_at
              expires_at = active_membership.expires_at + user_membership.plan.duration.days 
            end
          end
          expire_active_and_pending_membership(user)
          membership_args = {
            starts_at: starts_at,
            expires_at: expires_at
          }
          user_membership = membership_model.update_membership(user_membership, membership_args)
          user_membership = change_state(user_membership, MEMBERSHIP_EVENTS::PAYMENT_SUCCESSFUL)
          profile_service.update_user_savings(user.profile, user_membership.membership_payment.total_savings)
        else
          raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_PAYMENT_ID)
        end
        return user_membership
      end

      # 
      # Function to validate exisiting membership of User
      #  * if existing_user_membership is blank then we create a new membership
      #  * if existing_user_membership has a plan which is a part of request, we extend the
      #    membership expiry date by that duration days
      #  * if existing_user_membership has a plan apart from plan in request, we force_expire that
      #    plan & associate requested plan in new membership
      # 
      #  TO-DO: Money refund for the exisiting plan, which is not utilized
      #
      def validate_existing_membership(existing_membership, membership_args)
        membership_payment = PAYMENT_SERVICE.new
        membership_model = MEMBERSHIP.new
        payment_details = calculate_membership_payment(existing_membership, membership_args)
        if existing_membership.present?
          if existing_membership.plan == membership_args[:plan]
            membership_args[:starts_at] = existing_membership.starts_at
            membership_args[:expires_at] = existing_membership.expires_at + membership_args[:plan].duration.days
            membership_args[:quantity] = membership_args[:quantity] + existing_membership.quantity
          end
        end
        new_membership = membership_model.new_membership(membership_args)
        new_membership.membership_payment = membership_payment.new_payment(payment_details[:payment_args])
        new_membership.save_membership
        new_membership = move_user_membership_to_pending_state(membership_args[:plan], new_membership)
        return { membership: new_membership, payment_details: payment_details[:payment_mode_details] }
      end

      # 
      # Function to calculate cost of membership
      #
      def calculate_membership_payment(membership, membership_args)
        payment = PAYMENT_SERVICE.new
        membership_payment = payment.calculate_membership_payment(membership, membership_args[:plan])
        return membership_payment
      end

      # 
      # Function move membership to 
      #  * Pending if it is a 'trial' membership plan
      #  * payment_pending if it is not a 'trial' membership plan
      #
      def move_user_membership_to_pending_state(plan, membership)
        if plan.plan_key == PLAN_KEYS::TRIAL
          event = MEMBERSHIP_EVENTS::INITIATE_TRIAL  
        else
          event = MEMBERSHIP_EVENTS::INITIATE_PAYMENT
        end
        membership = change_state(membership, event)
        return membership
      end

      #
      # Function to activate Ample-Sample membership of User if it exists
      #
      def activate_ample_sample_membership(user)
        membership_dao = MEMBERSHIP_DAO.new(@params)
        membership_model = MEMBERSHIP.new
        pending_membership = membership_dao.get_only_pending_membership(user)
        if pending_membership.present?
          duration = MEMBERSHIP_HELPER.get_membership_duration(pending_membership)
          membership_model = MEMBERSHIP.new
          membership_args = {
            starts_at: Time.zone.now,
            expires_at: Time.zone.now + duration.days
          }
          pending_membership = membership_model.update_membership(pending_membership, membership_args)
          activated_membership = change_state(pending_membership, MEMBERSHIP_EVENTS::ACTIVATE)
        end
      end

      # 
      # Function to fetch the active membership of the User & expire payment pending membership
      #
      def get_active_user_membership(user)
        active_membership = nil
        user_memberships = user.memberships.where(workflow_state: [MEMBERSHIP_STATES::ACTIVE, MEMBERSHIP_STATES::PAYMENT_PENDING])
        user_memberships.each do |membership|
          if membership.payment_pending?
            change_state(membership, MEMBERSHIP_EVENTS::FORCE_EXPIRE)
          elsif membership.active?
            active_membership = membership
          end 
        end
        return active_membership
      end

      # 
      # Function to add one month extra free trial membership to the referred by user
      #
      def reward_referral_one_month_membership(referred_by)
        membership_dao = MEMBERSHIP_DAO.new(@params)
        membership_model = MEMBERSHIP.new
        user_service = @user_service.new(@params)
        referred_by = user_service.get_user_from_user_id(referred_by.to_i)
        if validate_membership_limit(referred_by)
          membership = membership_dao.get_ample_sample_membership(referred_by.memberships)
          if membership.present?
            membership_args = {
              expires_at: membership.expires_at + MEMBERSHIP_REWARD.days
            }
            membership = membership_model.update_membership(membership, membership_args)
            referred_by.benefit_limit -= 1
            referred_by.save_user
            ApplicationLogger.debug('One  month free membership Awarded to user:' + referred_by.id.to_s)
          end
        end
      end

      #
      # Function to get active or pending membershp of User
      #
      def get_pending_or_active_membership(user)
        membership_dao = MEMBERSHIP_DAO.new(@params)
        return membership_dao.get_pending_or_active_membership(user)
      end


      # 
      # Function to expire the PENDING & ACTIVE membership of user
      # When New membership payment has happened successfully
      #
      def expire_active_and_pending_membership(user)
        user_memberships = user.memberships.where(workflow_state: [MEMBERSHIP_STATES::ACTIVE, MEMBERSHIP_STATES::PENDING])
        user_memberships.each do |membership|
          if membership.active? || membership.pending?
            change_state(membership, MEMBERSHIP_EVENTS::FORCE_EXPIRE)
          end
        end
      end

      #
      # Function to check if user's benefit limit is not zero
      #
      def validate_membership_limit(user)
        if user.benefit_limit > 0
          return true
        else
          ApplicationLogger.debug('Benefit Limit of user: ' + user.id.to_s + ' expired')
          return false
        end
      end

      # 
      # change state function for membership
      # @param membership [Membership Model] 
      # @param event [Integer] MembershipEvent
      # 
      # @return [MembershipModel] updated membership object
      # 
      def change_state(membership, event)
        begin
          case event.to_i
            when MEMBERSHIP_EVENTS::INITIATE_TRIAL
              membership.trigger_event('initiate_trial')
            when MEMBERSHIP_EVENTS::INITIATE_PAYMENT
              membership.trigger_event('initiate_payment')
            when MEMBERSHIP_EVENTS::ACTIVATE
              membership.trigger_event('activate')
            when MEMBERSHIP_EVENTS::PAYMENT_SUCCESSFUL
              membership.trigger_event('payment_successful')
            when MEMBERSHIP_EVENTS::PAYMENT_FAILED
              membership.trigger_event('payment_failed')
            when MEMBERSHIP_EVENTS::EXPIRE
              membership.trigger_event('expire')
            when  MEMBERSHIP_EVENTS::FORCE_EXPIRE
              membership.trigger_event('force_expire')
            else
              raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
          end
         end
        return membership
      end
    end
  end
end

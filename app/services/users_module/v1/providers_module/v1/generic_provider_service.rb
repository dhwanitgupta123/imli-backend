module UsersModule
  module V1
    module ProvidersModule
      module V1
        class GenericProviderService < BaseModule::V1::BaseService

          def initialize
            @generic_model = UsersModule::V1::ProvidersModule::V1::Generic
            @custom_error_util = CommonModule::V1::CustomErrors
            @password_helper = UsersModule::V1::PasswordHelper
            @content_util = CommonModule::V1::Content
          end

          # 
          # to create new object of generic model
          # 
          # Parameters::
          #   * password [string] contains the password of user
          # 
          # @return [object] Generic model object
          def new_generic_provider(password)
            raise @custom_error_util::InsufficientDataError.new(@content_util::FIELD_MUST_PRESENT%{ field: 'Password' }) if password.blank?
            password = create_password_hash(password)
            new_generic(password)
          end

          # 
          # to match the password sent by user & stored in db
          # 
          # Parameters::
          #   * password [string] contains the password of user
          #   * generic [object] it contains the password stored in db
          # 
          # Raise Error::
          #   * WrongPasswordError if password dont' match
          # 
          # @return [boolean] if password matches else raises error
          def verify_password(password, generic)
            @password_helper.validate_password(password, generic.password) ? true : (
              raise @custom_error_util::WrongPasswordError.new(@content_util::WRONG_PASSWORD))
          end

          # 
          # Function to change the password of a user
          # 
          # Parameters::
          #   * password [string] contains the password of user
          # 
          # @return [boolean] if update was successfull
          def update_password(password)
            generic = @generic_model.new
            @generic.password = password
            @generic.save_generic
          end

          #
          # Set value to the reset_flag of generic
          #
          # @param generic [Object] [Generic model object]
          # @param value [Boolean] [Value to be set in the reset_flag]
          #
          # @return [Object] [updated generic model object]
          #
          def set_reset_flag(generic, value)
            generic.update_generic({
              reset_flag: value
              })
            return generic
          end

          #
          # Reset user's password, only if reset_flag is set
          # (User already initiated the resetting process and
          # completed authentication)
          # Also, set reset_flag to false once password is updated
          #
          # @param generic [Object] [Generic model object]
          # @param password [String] [plain password string to be set]
          # 
          # @return [Object] [Generic model object]
          #
          # @raise [InvalidArgumentsError] [if reset flag is not set, and tried to reset the password]
          #
          def reset_password(generic, password)
            if generic.present? && generic.reset_flag.present?
              password = create_password_hash(password)
              generic = generic.update_generic({
                password: password,
                reset_flag: false
                })
              return generic
            else
              raise @custom_error_util::InvalidArgumentsError.new(@content_util::PLEASE_REAUTHENTICATE)
            end
          end

          private
          
            # 
            # Function to create salt hash of the password sent by user
            # 
            # Parameters::
            #   * password [string] password sent by user
            # 
            # @return [string] Salt Hashed password
            def create_password_hash(password)
              return @password_helper.create_hash(password)
            end

            # 
            # Function to create new generic object
            # 
            # Parameters::
            #   * Password [string] it contains password gien by user
            # 
            # @return [object] Generic Object
            def new_generic(password)
              @generic_model.new(:password => password)
            end
        end
      end
    end
  end
end
module UsersModule
  module V1
    class RegisterUserService < BaseModule::V1::BaseService

      def initialize(params)
        @params = params
        @provider_service = UsersModule::V1::ProviderService
      end

      # 
      # It initiates the processing the register request of user
      # 
      # Paramaters::
      #     * user_params [Hash] Stores the strong parameter of User
      #     * request_type [enum] Stores the provider request type 
      # 
      # @return [object] Returns provider object if new provider is created
      def register_user(user_params, request_type)
        return new_provider(user_params, request_type)
      end 

      # 
      # Function to call new_provider of ProviderService
      # 
      # Paramaters::
      #     * user_params [Hash] Stores the strong parameter of User
      #     * request_type [enum] Stores the provider request type 
      # 
      # @return [objct] Returns provider object if new provider is created
      def new_provider(user_params, request_type)
        provider_service = @provider_service.new(@params)
        provider_service.new_provider(user_params, request_type)
      end   
    end
  end
end
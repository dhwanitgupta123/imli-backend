module UsersModule
  module V1
    class AccessControlService < BaseModule::V1::BaseService
      include ActionView::Helpers::DateHelper
      require 'application_logger'

      # 
      # Initializes required parameters
      # 
      # @param params [json] array of required parameters
      # 
      def initialize(params = {})
        @user_service = UsersModule::V1::UserService
        @user_response_decorator = UsersModule::V1::UserResponseDecorator
        @request_log_worker = LoggingModule::V1::RequestLogWorker
        @user_activity_analyzer_worker = AnalyzerModule::V1::UserAnalyzer::V1::UserActivityAnalyzerWorker
        @request_log_helper = LoggingModule::V1::RequestLogHelper
        @general_helper = CommonModule::V1::GeneralHelper
        @validation_map = ValidationsModule::V1::ValidationMap

        @cache_util = CommonModule::V1::Cache
        @content_util = CommonModule::V1::Content
        @response_util = CommonModule::V1::ResponseCodes
        @custom_error_util = CommonModule::V1::CustomErrors
        if params.present?
          @authentication_token = params[:session_token]
          @controller = params[:controller]
          @action = params[:action]
          @user = params[:user]
          @phone_number = params[:user][:phone_number] if @user.present?
        end
      end

      # 
      # Check for params validity and user's authorization
      # If asking for public api access, then return true
      # If authorized user, then return true
      # For all other cases, return false with specific error response codes
      # 
      # @return [Json] [Map containing attribute 'ok' as true or false
      # and response parameters if false]
      def has_access?
        begin
          initialize_resources
          if requested_for_public_access
            return {ok: true}
          end
          if is_authorized?
            return {ok: true, user_id: @user_id, phone_number: @phone_number}
          else
            create_response_unauthorized_access
            return {ok: false, data: @response}
          end
        rescue @custom_error_util::InsufficientDataError => e
          ApplicationLogger.debug('Insufficient data passed, ' + e.message)
          create_response_bad_request
          return {ok: false, data: @response}
        rescue @custom_error_util::UnAuthenticatedUserError => e
          ApplicationLogger.debug('Token: ' + @authentication_token + ' , User not authenticated, ' + e.message)
          create_response_unauthenticated_user
          return {ok: false, data: @response}
        rescue @custom_error_util::ResourceNotFoundError, @custom_error_util::UnAuthorizedUserError => e
          ApplicationLogger.debug('Token: ' + @authentication_token + ' , Access Denied, ' + e.message)
          create_response_unauthorized_access
          return {ok: false, data: @response}
        end
      end

      # 
      # This function checks if user is in blocked state or not
      # 
      # @return [Json] Map containing attribute 'ok' as true or false
      # and response parameters if false 
      # 
      def is_user_blocked?

        key = @general_helper.get_blocked_user_key(@phone_number)
        value = @cache_util.read(key)

        if value.present?
          time_remaining = get_remaining_time_in_words(value)
          blocked_response = @user_response_decorator.create_blocked_user_response(time_remaining)
          return {ok: true, data: blocked_response}
        else
          return {ok: false}
        end
      end

      # 
      # It will return the remaining time in human readable format
      # 
      # @param time_remaining [String] Time in String format
      # 
      # @return [String] Remaining time in human readable format
      # 
      def get_remaining_time_in_words(time_remaining)
        user_block_time_hours   = @cache_util.read_int('USER_BLOCK_TIME_HOUR') || 24
       
        distance_of_time_in_words(time_remaining.to_time + user_block_time_hours.hours, Time.zone.now)
      end

      # 
      # This will initiate async call to log the meta data of the 
      # request and its corresponding response
      # 
      # @param controller [Object] Controller object
      # 
      def log_request(args)
        args[:phone_number] = @phone_number
        log_params = @request_log_helper.create_log_params(args)
        
        @request_log_worker.perform_async(log_params)
      end

      # 
      # Getter for phone number
      # 
      # @return [String] returns the phone number
      # 
      def get_phone_number
        return @phone_number
      end

      # 
      # This will initiate the async call to activity alayzer worker 
      # 
      def analyze_user_activity
          @user_activity_analyzer_worker.perform_async(@phone_number)
      end


      #
      # Get all available permissions
      # It iterates over permissions.yml file stored in Cache, and create a array
      # of hashes (Hash consist of id, label and description)
      #
      # @return [Array] [Array of permissions hash]
      #
      def get_all_permissions
        begin
          permissions_lookup = get_permissions_lookup
          permissions_array = []
          permissions_lookup.each do |key,permission|
            permission_hash = {} 
            permission_hash['id'] = key
            permission_hash['label'] = permission['label'] || ''
            permission_hash['description'] = permission['description'] || ''
            # Push each permission hash into array
            permissions_array.push(permission_hash)
          end
          return permissions_array
        rescue
          return [];  # Return empty array if anything goes wrong
        end
      end

      #
      # apply validations corresponding to the particular API
      # these validations array fetched from permission.yml
      # corresponding to each validation enum there is a map of enum
      # to validator class in validatorMap class. validator class implements
      # validate function which return true or false according to the input
      # params
      # 
      # @return [Hash] return true hash if all validations passed else return false with response
      #
      def apply_validations

        return {ok: true} if @matched_api.blank?

        api_and_validation_hash = @matched_api[0]
        
        validations = []

        # These are pemission level validations
        if @permission_level_validations.present?
          validations = @permission_level_validations
        end

        # These are API level validation if present then adding api level
        # validation in overall validation
        if api_and_validation_hash.present? && api_and_validation_hash['validations'].present?
          validations = validations | api_and_validation_hash['validations']
        end

        if validations.blank?
          return {ok: true}
        end

        # apply all the validations if any one validation fails
        # then return false
        validations.each do |validation|
          data  = apply_validation(validation)

          next if data.blank? ||  data[:result] == true

          validation_failed_response = @user_response_decorator.create_validation_failed_response(data[:message])

          return  {ok: false, data: validation_failed_response}
        end

        return {ok: true}
      end

      #############################
      #    Private Functions      #   
      #############################
      private

      # 
      # Check presence of authentication token passed
      # 
      # @raise [@custom_error_util::UnAuthenticatedUserError] if authrntication token not present in params
      def authentication_token_present?
        raise @custom_error_util::UnAuthenticatedUserError.new('Session token not present') if @authentication_token.blank?
      end

      # 
      # Initialize resources required for authorization
      # 
      # @raise [@custom_error_util::InsufficientDataError] if params incorrect
      # @raise [@custom_error_util::ResourceNotFoundError] if permissions_lookup not found in cache
      def initialize_resources
        @response = {}  # Initializing empty response

        if !(@controller.present? && @action.present?)
          raise @custom_error_util::InsufficientDataError.new('Controller OR Action not present')
        end

        # Generating request in the format controller#action. Ex users#create
        @request_param = @controller.to_s + '#' + @action.to_s

        # Loading permissions file from cache
        permissions_hash = get_permissions_lookup
        if permissions_hash.blank?
          permissions_hash = fetch_and_store_permissions_lookup
          raise @custom_error_util::ResourceNotFoundError.new('Permissions Resource not found') if permissions_hash.blank?
        end
        @permissions_lookup = permissions_hash
      end

      #
      # Get permissions lookup table from cache
      #
      def get_permissions_lookup
        return @cache_util.read_json('PERMISSIONS_LOOKUP')
      end

      #
      # Check whether the request is for any public API
      #
      # @return [Boolean] [true if asked for public API access]
      #
      def requested_for_public_access
        public_api_access_index = @cache_util.read("PUBLIC_API_ACCESS_INDEX").to_i
        if has_permission_for_request?(public_api_access_index)
          return true
        else
          return false
        end
      end

      # 
      # Check whether user is authorized or not based on
      # initialized parameters
      # 
      # @return [boolean] True if authorized else false
      # @raise [@custom_error_util::UnAuthorizedUserError] if user has no associated roles
      def is_authorized?
        @user_roles = get_user_roles

        # Return false in case requested user has no roles assigned
        if (!@user_roles.present?)
          ApplicationLogger.debug('User id ' + @user_id.to_s + 'does not have any roles assigned') if @user_id.present?
          return false
        end
        @user_roles.each do |role|
          if has_required_permissions?(role)
            return true
          end
        end
        ApplicationLogger.debug('User id ' + @user_id.to_s + ' does not have valid permissions') if @user_id.present?
        return false
      end

      # 
      # Check whether given Role has required permissions
      # based on globaly configured permissions file
      # @param role [Role Object] Role to check
      # 
      # @return [boolean] True if role has required permissions else false
      def has_required_permissions? (role)
        role.permissions.each do |permission|
          if has_permission_for_request? (permission)
            return true
          end
        end
        return false
      end

      #
      # Get user roles based on authentication token
      #
      # @return [Roles] Roles associated with user (or authentication token)
      def get_user_roles
        user_service = @user_service.new(nil)
        user_id = user_service.get_user_id_from_session_token(@authentication_token)

        # store user-id in global scope. Needed for setting in session later on
        @user_id = user_id

        user = user_service.get_user_from_user_id(user_id)
        @current_user = user
        # store phone number in global scope. Needed for setting in session later on
        @phone_number = user.phone_number

        return user.roles
      end

      # 
      # Function to lookup into permissions file and check whether given
      # permission has requested access
      # @param permission [Integer] Permission to check access for request
      # 
      # @return [Boolean] True if given permission contains access to the request
      def has_permission_for_request? (permission)
        access_to_key = @cache_util.read("ACCESS_TO_REQUESTS_KEY")
        if @permissions_lookup[permission.to_s] &&
          @permissions_lookup[permission.to_s][access_to_key.to_s]

          apis_and_validations = @permissions_lookup[permission.to_s][access_to_key.to_s]
          @matched_api = apis_and_validations.select{|api_and_validation| api_and_validation['api'] == @request_param}

          @permission_level_validations = @permissions_lookup[permission.to_s]['validations']

          return true if @matched_api.present?
        end

        return false
      end

      # 
      # Return nil if no validator found else return validation result
      # 
      # @return [Hash] return hash of validator class
      #
      def apply_validation(validation)
        validator = @validation_map.get_validator_by_validation(validation)

        if validator.blank?
          ApplicationLogger.debug('Validator with  id ' + validation.to_s + ' not present')
          return nil
        end
        validator = validator.new
        return validator.validate(@current_user)
      end
      #
      # Fetch permissions hash from FILE and store it in CACHE
      #
      # @return [JSON] [Permissions hash]
      #
      def fetch_and_store_permissions_lookup
        # Load permissions file into cache
        permissions_file_path = Rails.root.join("db", "users_module/permissions.yml").to_s
        if File.file?(permissions_file_path)
          permissions_hash = JSON.generate( YAML.load_file(permissions_file_path) )
          # Store into Redis after converting into JSON string
          @cache_util.write({
            key: 'PERMISSIONS_LOOKUP',
            value: permissions_hash
          })
          return JSON.parse(permissions_hash)
        else
          # If permissions file does not exist
          # TO-DO: Need to restart server and raise an ALARM to devOps/developers
          return nil
        end
      end

      # 
      # Create forbidden response code back to user
      # @response [Json] [Map containing response and message]
      def create_response_unauthorized_access
        @response = {
          :error => {:message => @content_util::UNAUTHORIZED_USER },
          :response => @response_util::FORBIDDEN
        }
      end

      # 
      # Create HTTP-UnAuthorized (unauthenticated) response code back to user
      # @response [Json] [Map containing response and message]
      def create_response_unauthenticated_user
        @response = {
          :error => {:message => @content_util::UNAUTHENTIC_USER},
          :response => @response_util::UN_AUTHORIZED
        }
      end

      # 
      # Create Bad-Request response code back to user
      # @response [Json] [Map containing response and message]
      def create_response_bad_request
        @response = {
          :error => {:message => @content_util::INSUFFICIENT_DATA},
          :response => @response_util::BAD_REQUEST
        }
      end

    end
  end
end
require 'transaction_helper'
module UsersModule
  module V1
    class UserService < BaseModule::V1::BaseService

      USER_MODEL_EVENTS = UsersModule::V1::ModelStates::UserEvents
      USER_MODEL_STATES = UsersModule::V1::ModelStates::UserStates
      PAGINATION_UTIL = CommonModule::V1::Pagination
      TRIGGER_ENUM_UTIL = TriggersModule::V1::TriggerEnum

      def initialize(params)
        @params = params
        # Assigning all versioned classes to instance level variables
        @login_service = UsersModule::V1::LoginUserService
        @register_service = UsersModule::V1::RegisterUserService
        @user_model = UsersModule::V1::User
        @provider_service =UsersModule::V1::ProviderService
        @role_service = UsersModule::V1::RoleService
        @email_service = UsersModule::V1::EmailService
        @user_service_helper = UsersModule::V1::UserServiceHelper
        @general_helper = CommonModule::V1::GeneralHelper
        @user_response_decorator = UsersModule::V1::UserResponseDecorator
        @custom_error_util = CommonModule::V1::CustomErrors
        @content_util = CommonModule::V1::Content
        @sms_worker = CommunicationsModule::V1::Messengers::V1::SmsWorker
        @request_type = UsersModule::V1::ProviderType
        @sms_type = CommunicationsModule::V1::Messengers::V1::SmsType
        @address_service = AddressModule::V1::AddressService
        @address_response_decorator = AddressModule::V1::AddressResponseDecorator
        @profile_service = UsersModule::V1::ProfileService
        @cache_util = CommonModule::V1::Cache
        @plan_key = CommonModule::V1::PlanKeys
        @plan_service = CommonModule::V1::PlanService
        @membership_service = UsersModule::V1::MembershipService
        @benefit_service = CommonModule::V1::BenefitService
        @parse_notification_worker = CommunicationsModule::V1::Notifications::V1::UserNotificationWorker
        @notification_type = CommunicationsModule::V1::Notifications::V1::NotificationType
        @user_notification_service = UsersModule::V1::UserNotificationService
        @ample_credit_dao = CreditsModule::V1::AmpleCreditDao
      end

      # 
      # To decide if to register a new user or add new provider or update message provider
      # 
      # Parameters::
      #   * user_params [hash] phone_number is a must & can have other user related values like first_name etc.
      #   * request_type [enum] can have value 1,2,3,4 corresponding to SMS, Generic, FB, Google provider type
      #
      #
      # @return [response] 
      def decide_register_login(user_params, request_type)
        begin
          @user = decide_register_login_for_user({
            user_params: user_params,
            request_type: request_type
            })
          @user_response_decorator.create_ok_response(@user) if @user.present?
        rescue @custom_error_util::InsufficientDataError, @custom_error_util::InvalidDataError => e
          @user_response_decorator.create_response_bad_request(e.message)
        rescue @custom_error_util::InvalidDataError => e
          @user_response_decorator.create_response_invalid_data_passed(e.message)
        rescue @custom_error_util::RunTimeError => e
          @user_response_decorator.create_response_runtime_error(e.message)
        rescue @custom_error_util::ExistingUserError => e
          @user_response_decorator.create_exisiting_user_error(e.message)
        rescue @custom_error_util::ReachedMaximumLimitError => e
          @user_response_decorator.create_too_many_request_error(e.message)
        rescue @custom_error_util::ResourceNotFoundError => e
          return @user_response_decorator.create_not_found_error(e.message)
        rescue @custom_error_util::PreConditionRequiredError => e
          @user_response_decorator.create_response_pre_condition_required(e.message)
        end
      end

      # 
      # Logic for registering user (if new user) or signing in
      # (if user already registered with us)
      # @param args [Hash] Hash of user params, request type
      #
      # Description::
      #   if User_exists?
      #     call update_login_type
      #   else
      #     call register_ user
      #   end
      #
      # Raise Errors::
      #   * InsufficientDataError: if required parameters are missing or not proper
      #   * RunTimeError: if some error internal server error happens
      #   * ExistingUserError: If provider type is Generic & user already has generic provider type
      #   * ReachedMaximumLimitError: when user has crossed request limit to register for SMS type 
      #
      # @return [User object] User object created after successful operation
      def decide_register_login_for_user(args)
        transactional_function = Proc.new do |args|
          raise @custom_error_util::InsufficientDataError.new('Request type can not be blank') if args[:request_type].blank?
          initialize_user(args[:user_params])
          validate_phone_number(args[:user_params][:phone_number])
          user = user_exists
          if user.present?
            @user = update_login_type({
              user_params: args[:user_params],
              request_type: args[:request_type],
              user: @user
              })
          else
            @user = register_user(args[:user_params], args[:request_type])
          end
          if args[:request_type] == @request_type::SMS || args[:request_type] == @request_type::FORGOT_PASSWORD
            otp = @user.provider.message.otp
            send_otp(otp, @user.phone_number)
            # push_notification(@user.id, @notification_type::SIGN_UP[:type])
          end
          return @user
        end
        transaction_status = TransactionHelper.new({
          :function => transactional_function,
          :args => args
          })
        transaction_status.run();
      end

      # 
      # Transactionally applying benefits to user
      #
      def transactional_apply_refferal_benefits(args)
        
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return apply_refferal_benefits(args)
        end

        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      # 
      # This function get all benefits of user to be applied if referral code is correct
      # these are one time benefits while registering
      #
      # @param referral_code [String] referral code
      # 
      # @return [Model] user
      #
      def apply_refferal_benefits(referral_code)

        @user = get_user_from_user_id(USER_SESSION[:user_id])

        set_referral_for_user(referral_code)

        benefit_service = @benefit_service.new(@params)
        benefit_service.apply_benefits_to_user({ 
                        user: @user,
                        trigger: TRIGGER_ENUM_UTIL.get_trigger_id_by_trigger('ON_REFERRAL_ACTIVATION')
                    })

        return @user
      end

      # 
      # set referred_by for user by getting referrer from referral_code
      #
      # @param referral_code [String] Ample referral code
      #
      def set_referral_for_user(referral_code)

        referrer = validate_referral_code(referral_code)

        set_referred_by(referrer)
      end

      #
      # To login user with one time password
      # 
      # Parameters::
      #   * user_params [hash] phone_number & OTP are must 
      # 
      # Description::
      #   * If user_exists call login_user func else raise RunTimeError
      #   * If session_token is present & User state is INACTIVE? change it to pending 
      #   * Otherwise let user be in the same state
      # 
      #  
      # @return [response] 
      # 
      def login(user_params, request_type)
        begin
          session_token = login_for_user({
            user_params: user_params,
            request_type: request_type
            })
          if @user.active?
            @user_response_decorator.create_active_user_status_response(@user) 
          else
            @user_response_decorator.create_session_token_response(@user)
          end
        rescue @custom_error_util::InsufficientDataError, @custom_error_util::InvalidDataError => e
          @user_response_decorator.create_response_bad_request(e.message)
        rescue @custom_error_util::InvalidDataError => e
          @user_response_decorator.create_response_invalid_data_passed(e.message)
        rescue @custom_error_util::RunTimeError => e
          @user_response_decorator.create_response_runtime_error(e.message)
        rescue @custom_error_util::WrongOtpError => e
          @user_response_decorator.create_wrong_otp_request(e.message)
        rescue @custom_error_util::WrongPasswordError => e
          @user_response_decorator.create_wrong_password_request(e.message)
        rescue @custom_error_util::AuthenticationTimeoutError => e
          @user_response_decorator.create_request_timeout_error(e.message)
        rescue @custom_error_util::ResourceNotFoundError => e
          @user_response_decorator.create_not_found_error(e.message)
        rescue @custom_error_util::UnAuthenticatedUserError => e
          @user_response_decorator.create_response_unauthenticated_user(e.message)
        rescue @custom_error_util::PreConditionRequiredError => e
          @user_response_decorator.create_response_pre_condition_required(e.message)
        end
      end

      # 
      # Logic for signing in of user
      # @param args [Hash] Hash of user params
      # 
      # Raise Errors::
      #   * InsufficientDataError: if required parameters are missing or not proper
      #   * RunTimeError: if some error internal server error happens
      #   * WrongOtpError: if OTP doesn't matches with value in db
      #   * WrongPasswordError: if Password doesn't matches
      #   * AuthenticationTimeoutError: If OTP has expired
      #   * ResourceNotFoundError: If login provider type not found
      #   * UnAuthenticatedUserError: If user isn't found
      #
      # @return [String] Session token generated after successfull sign in
      def login_for_user(args)
        initialize_user(args[:user_params])
        validate_phone_number(args[:user_params][:phone_number])
        user =  user_exists
        validate_user_existence

        session_token = login_user({user_params: args[:user_params], user: @user, request_type: args[:request_type]}) if user.present?
        @user.change_user_state_to_pending if args[:user_params][:otp].present?
        if @general_helper.string_to_boolean(@cache_util.read('ACTIVATE_USER')) && args[:user_params][:otp].present? && @user.pending?
          user.trigger_event(USER_MODEL_EVENTS::ACTIVATE)
          send_welcome_sms(user.phone_number)
          push_notification(user.id, @notification_type::WELCOME[:type])
        end
        # Assigning him Mobile API roles
        @user = assign_mobile_api_role_to_user(@user)
        return session_token
      end

      # 
      # To resend OTP to user
      # 
      # Request::
      #   * user_params: [hash] phone_number is must 
      # 
      # Raise Errors::
      #   * InsufficientDataError: if phone number is  missing or not proper
      #   * RunTimeError: if otp not returned
      #   * UnAuthenticatedUserError: If user doesn't exists
      #   * ReachedMaximumLimitError: If user is doing spamming
      # 
      # @return [response] 
      # 
      def resend_otp(user_params)
        begin
          initialize_user(user_params)
          validate_phone_number(user_params[:phone_number])
          provider_service = @provider_service.new(@params)
          user = user_exists
          validate_user_existence
          validate_provider_existence
          otp = provider_service.resend_otp(user.provider)

          send_otp(otp, user.phone_number)

          @user_response_decorator.create_ok_response(@user) if otp.present?  
        rescue @custom_error_util::InsufficientDataError => e
          @user_response_decorator.create_response_bad_request(e.message)
        rescue @custom_error_util::InvalidDataError => e
          @user_response_decorator.create_response_invalid_data_passed(e.message)
        rescue @custom_error_util::RunTimeError => e
          @user_response_decorator.create_response_runtime_error(e.message)
        rescue @custom_error_util::UnAuthenticatedUserError => e
          @user_response_decorator.create_response_unauthenticated_user(e.message)
        rescue @custom_error_util::ReachedMaximumLimitError => e
          @user_response_decorator.create_too_many_request_error(e.message)
        end
      end

      # 
      # To update profile of a user
      # 
      # Parameters::
      #   * user_params [hash] first_name, email_id, session_token are must
      #   * session_token: a valid session_token is must
      # Description::
      #   * Service function to add user profile
      #    
      # @return [response] 
      # 
      def update_user_details(user_args)
        user = get_user_from_user_id(USER_SESSION[:user_id])
        if user_args[:emails].present?
          email = user_args[:emails].first
        end
        @user = update_user_info_details({
          user: user,
          first_name: user_args[:first_name],
          last_name: user_args[:last_name],
          email: email || {},
          account_type: UsersModule::V1::AccountType::CUSTOMER
          })
        @user_response_decorator.create_active_user_status_response(@user) if @user.present?
        rescue @custom_error_util::InsufficientDataError => e
          @user_response_decorator.create_response_bad_request(e.message)
        rescue @custom_error_util::RunTimeError => e
          @user_response_decorator.create_response_runtime_error(e.message)
        rescue @custom_error_util::InvalidDataError => e
          @user_response_decorator.create_response_invalid_data_passed(e.message)
        rescue @custom_error_util::UnAuthenticatedUserError => e
          @user_response_decorator.create_response_unauthenticated_user(e.message)
      end

      # 
      # Logic for adding user profile
      # @param args [Hash] Hash of user params
      # 
      # Description::
      #   * Fetches a valid user using session_token
      #   * Calls email service to add email_id to the user profile
      #   * Saves user Record
      # Raise Errors::
      #   * InsufficientDataError: if required parameters are missing or not proper
      #   * RunTimeError: if user record is invalid or email_id is (invalid/duplicated)
      #    
      # @return [User object] User object created after successful operation
      # 
      def update_user_info_details(args)
        user_notification_service = @user_notification_service.new(@params)
        transactional_function = Proc.new do |args|
          if args[:email].present?
            args[:email] = validate_emails_params(args[:email])
            user = add_email_id_for_user({
              user: args[:user],
              email: args[:email],
              account_type: args[:account_type]
            })
            args[:user] = user
            email_id = args[:email][:email_id]
            # trigger welcome verification mail for the newly added email for user
            user_notification_service.send_welcome_email(email_id)
          end
          @user = update_user(args)
          
          return @user
        end
        transaction_status = TransactionHelper.new({
          :function => transactional_function,
          :args => args
          })
        transaction_status.run();
      end

      # 
      # To get all addresses of a user
      # 
      # Parameters::
      #   * session_token: a valid session_token is must
      # Description::
      #   * Service function to get all addresses of a user
      #    
      # @return [response] 
      # 
      def get_all_addresses_of_user
        address = get_all_addresses
        @address_response_decorator.get_all_addresses_of_user_response(address) if address.present?
        rescue @custom_error_util::UnAuthenticatedUserError => e
          @user_response_decorator.create_response_unauthenticated_user(e.message)
      end

      # 
      # Logic for getting all user addresses
      # 
      # Description::
      #   * Fetches a valid user using session_token
      #   * Fetches all user addresses
      # @return [Array] Address object created after successful operation
      # 
      def get_all_addresses
        @user = get_user_from_user_id(USER_SESSION[:user_id])
        @user.addresses if @user.present?
      end

      # 
      # To add an address of a user
      # 
      # Parameters::
      #   * address: [hash] nickname, address_line1, address_line2, landmark, area_id
      #     * address_line1, address_line2, area_id is must
      #   * session_token: a valid session_token is must
      # 
      # Description::
      #   * Service function to add address of a user
      #    
      # @return [response] 
      # 
      def add_address(address_params)
        begin
          raise @custom_error_util::InsufficientDataError.new if address_params.nil?
          address = add_address_for_user(address_params)
        end
        @address_response_decorator.add_address_ok_response(address) if address.present?
        rescue @custom_error_util::InsufficientDataError => e
          @user_response_decorator.create_response_bad_request(e.message)
        rescue @custom_error_util::RunTimeError => e
          @user_response_decorator.create_response_runtime_error(e.message)
        rescue @custom_error_util::InvalidDataError => e
          @user_response_decorator.create_response_invalid_data_passed(e.message)
        rescue @custom_error_util::RecordNotFoundError => e
          @user_response_decorator.create_response_invalid_data_passed(e.message)
        rescue @custom_error_util::UnAuthenticatedUserError => e
          @user_response_decorator.create_response_unauthenticated_user(e.message)
        rescue @custom_error_util::InvalidArgumentsError => e
          @user_response_decorator.create_response_invalid_arguments_error(e.message)
      end

      # 
      # Logic for adding address of a user
      # @param args [Hash] Hash of address params
      # 
      # Description::
      #   * Fetches a valid user using session_token
      #   * Calls address service to add address of user
      # 
      # Raise Errors::
      #   * InsufficientDataError: if required parameters are missing or not proper
      #    
      # @return [Address object] Address object created after successful operation
      # 
      def add_address_for_user(address_params)
        address = address_params[:addresses].first
        raise @custom_error_util::InsufficientDataError.new if address[:area_id].blank?
        user = get_user_from_user_id(USER_SESSION[:user_id])
        if address[:recipient_name].blank?
          address[:recipient_name] = user.first_name + ' ' + (user.last_name || '')
        end
        if check_user_membership_benefits_for_address(user)
          address_service = @address_service.new
          address = address_service.add_address_for_user({
            user: user,
            address: address
          })
          return address if user.present?
        end
      end

      # 
      # To update an existing address of a user
      # 
      # Parameters::
      #   * address: [hash] nickname, address_line1, address_line2, landmark, area_id, address_id
      #     * area_id, address_id is must
      #   * session_token: a valid session_token is must
      # 
      # Description::
      #   * Service function to update n existing address of a user
      #    
      # @return [response] 
      #
      def update_address(address_params)
        address = update_address_for_user(address_params)
        @address_response_decorator.add_address_ok_response(address) if address.present?
        rescue @custom_error_util::RecordNotFoundError => e
          @user_response_decorator.create_response_invalid_data_passed(e.message)
        rescue @custom_error_util::InsufficientDataError => e
          @user_response_decorator.create_response_bad_request(e.message)
        rescue @custom_error_util::UnAuthenticatedUserError => e
          @user_response_decorator.create_response_unauthenticated_user(e.message)
        rescue @custom_error_util::UnAuthorizedUserError => e
          @user_response_decorator.create_response_unauthenticated_user(e.message)
      end

      # 
      # Logic for updating an existing address of a user
      # @param args [Hash] Hash of address params
      # 
      # Description::
      #   * Fetches a valid user using session_token
      #   * Calls address service to update address of user
      # 
      # Raise Errors::
      #   * InsufficientDataError: if required parameters are missing or not proper
      #    
      # @return [Address object] Address object created after successful operation
      # 
      def update_address_for_user(address_params)
        raise @custom_error_util::InsufficientDataError.new if address_params.blank?
        present_address_id = address_params[:id]
        address_updations = address_params[:addresses].first
        address_params = address_updations.merge({id: present_address_id})
        user = get_user_from_user_id(USER_SESSION[:user_id])
        address_service = @address_service.new
        address = address_service.update_address_for_user({
          user: user,
          address: address_params
        })
        return address
      end
      
      # 
      # To activate user when referral code is submitted
      # 
      # Request::
      #   * user_params: [hash] phone_number is must 
      # 
      # Raise Errors::
      #   * InsufficientDataError: if phone number is  missing or not proper
      #   * RunTimeError: if state not returned
      #   * UnAuthenticatedUserError: If user doesn't exists
      #   * WrongReferralCodeError: If Referral code is wrong
      #   * PreConditionRequiredError: if user is not in correct state
      # 
      # @return [response] 
      # 
      def activate_with_referral_code(user_params)
        begin
          initialize_user(user_params)
          return @user_response_decorator.create_response_bad_request if @referral_code.blank?
          @user = get_user_from_user_id(USER_SESSION[:user_id])
          if @user.active?
            return @user_response_decorator.create_active_user_status_response(@user)
          else
            if @user.pending?
              referred_by_user = get_referred_by_user
              state = activate_user if referred_by_user.present? 
              set_referred_by(referred_by_user)
            else
              return @user_response_decorator.create_response_pre_condition_required(@content_util::PENDING_USER)
            end
          end
          state.present? ? @user_response_decorator.create_active_user_status_response(@user) : (
            @user_response_decorator.create_response_runtime_error('State not found'))
        rescue @custom_error_util::InsufficientDataError => e
          @user_response_decorator.create_response_bad_request(e.message)
        rescue @custom_error_util::RunTimeError => e
          @user_response_decorator.create_response_runtime_error(e.message)
        rescue @custom_error_util::UnAuthenticatedUserError => e
          @user_response_decorator.create_response_unauthenticated_user(e.message)
        rescue @custom_error_util::WrongReferralCodeError => e
          @user_response_decorator.create_wrong_referral_code_request(e.message)
        rescue @custom_error_util::PreConditionRequiredError => e
          @user_response_decorator.create_response_pre_condition_required(e.message)
        rescue @custom_error_util::InvalidDataError => e
          @user_response_decorator.create_expired_referral_code_response(e.message)
        rescue @custom_error_util::InvalidArgumentsError => e
          @user_response_decorator.create_response_invalid_arguments_error(e.message)
        end
      end

      #
      # Change state of User based on passed event
      #
      # @param args [Hash] Hash containing User: {event} and user_id
      #
      # @return [Response] response to be sent to the user
      #
      # @raise [InsufficientDataError] [if parameter passed were insufficient]
      # @raise [InvalidArgumentsError] [if inner keys hash was invalid]
      # @raise [ResourceNotFoundError] [if benefits array contain invalid entries]
      #
      def change_state(user_params)
        begin
          response = change_user_state(user_params)
          return response
        rescue @custom_error_util::InsufficientDataError => e
          return @user_response_decorator.create_response_bad_request(e.message)
        rescue @custom_error_util::UnAuthenticatedUserError => e
          return @user_response_decorator.create_not_found_error(e.message)
        rescue @custom_error_util::InvalidArgumentsError => e
          return @user_response_decorator.create_response_invalid_data_passed(e.message)
        rescue @custom_error_util::PreConditionRequiredError => e
          return @user_response_decorator.create_response_pre_condition_required(e.message)
        end
      end

      #
      # Logic for changing state of User based on passed event
      #
      # @param args [Hash] Hash containing User: {event} and user_id
      #
      # @return [Response] response to be sent to the user
      #
      # @raise [InsufficientDataError] [if parameter passed were insufficient]
      # @raise [InvalidArgumentsError] [if inner keys hash was invalid]
      # @raise [UnAuthenticatedUserError] [if no User was found w.r.t given id]
      # @raise [PreConditionRequiredError] [if event passed was invalid corresponding to current state of User]
      #
      def change_user_state(user_params)
        if (user_params.blank? || user_params[:id].blank? )
          raise @custom_error_util::InsufficientDataError.new(@content_util::FIELD_MUST_PRESENT % {field: 'User ID'})
        end
        if (user_params[:user].blank? || user_params[:user][:event].blank?)
          raise @custom_error_util::InsufficientDataError.new(@content_util::FIELD_MUST_PRESENT % {field: 'User Event'})
        end
        user = get_user_from_user_id(user_params[:id])
        user.trigger_event(user_params[:user][:event])
        if user_params[:user][:event].to_i == USER_MODEL_EVENTS::ACTIVATE
          send_welcome_sms(user.phone_number)
          push_notification(user.id, @notification_type::WELCOME[:type])
        end
        # Assign mobile API role to the user
        user = assign_mobile_api_role_to_user(user)
        # Assign a membership to him and put it in pending state
        # TO-DO in a separate commit
        return @user_response_decorator.create_active_user_status_response(user)
      end

      # 
      # To return the current state of user
      # 
      # Request::
      #   * user_params: [hash] phone_number is must 
      # 
      # Raise Errors::
      #   * InsufficientDataError: if phone number is  missing or not proper
      #   * RunTimeError: if state not returned
      #   * UnAuthenticatedUserError: If user doesn't exists
      # 
      # @return [response] 
      # 
      def check_user_state
        begin
          @user = get_user_from_user_id(USER_SESSION[:user_id])
          state = @user.current_state.name
          return @user_response_decorator.create_response_runtime_error if state.blank?
          @user.active? ? @user_response_decorator.create_active_user_status_response(@user) : (
            @user_response_decorator.create_user_status_response(@user))
        rescue @custom_error_util::InsufficientDataError => e
          @user_response_decorator.create_response_bad_request(e.message)
        rescue @custom_error_util::RunTimeError => e
          @user_response_decorator.create_response_runtime_error(e.message)
        rescue @custom_error_util::UnAuthenticatedUserError => e
          @user_response_decorator.create_response_unauthenticated_user(e.message)
        end
      end

      #
      # Verify Email ID. Handles API request
      # @param args [Hash] Hash containing key :token
      #
      # @return [Response] ok_response, invalid_data_response, insufficient_data_response
      def verify_email(args)
        begin
          response = verify_email_for_user(args)
          return response
        rescue @custom_error_util::InsufficientDataError => e
          @user_response_decorator.create_response_bad_request(e.message)
        rescue @custom_error_util::InvalidDataError => e
          @user_response_decorator.create_response_invalid_data_passed(e.message)
        rescue @custom_error_util::EmailAlreadyVerifiedError => e
          @user_response_decorator.create_response_email_already_verified(e.message)
        rescue @custom_error_util::AuthenticationTimeoutError => e
          @user_response_decorator.create_request_timeout_error(e.message)
        rescue @custom_error_util::RunTimeError => e
          @user_response_decorator.create_response_runtime_error(e.message)
        end
      end

      #
      # Verify email id of user
      # @param args [Hash] Hash containing key :token
      #
      # @return [Response] Return ok response if email authenticated
      # @raise [InsufficientDataError] if passed arguments are not sufficient to perform operation
      # @raise [InvalidDataError] if passed parameters are invalid to perform operation
      def verify_email_for_user(args)
        raise @custom_error_util::InsufficientDataError.new(@content_util::TOKEN_NOT_FOUND) if args.blank? || args[:token].blank?

        # authenticate email token of user
        user = authenticate_email(args[:token])
        return  @user_response_decorator.create_ok_response(user)
      end

      #
      # Authenticate email id using email token
      #
      # @param email_token [String] Email token
      #
      # @return [User Object] User object associated with that email id
      def authenticate_email(email_token)
        email_service = @email_service.new
        user = email_service.authenticate_email(email_token)
        return user
      end

      #
      # Get email with accout type
      # @param email_id [String] [email id to be searched]
      # @param account_type [Integer] [account type to be matched]
      #
      # @return [Object] [Email Object matching above mentioned criteria]
      #
      def get_email_with_type(email_id, account_type)
        email_service = @email_service.new
        email = email_service.get_email_with_type(email_id, account_type)
        return email
      end

      # 
      # This function return users ample cerdit
      # 
      # @return [Model] ample credit
      #
      def get_current_ample_credit
        user = get_user_from_user_id(USER_SESSION[:user_id])
        ample_credit = user.ample_credit
        return ample_credit
      end

      # 
      # Resend email token to the user
      #
      # @param args [Hash] Hash containing email_id to be verified
      # 
      # @return [response] response to be sent to the user
      def resend_email_token(args)
        begin
          response = resend_email_token_for_user(args)
          return response
        rescue @custom_error_util::InsufficientDataError => e
          @user_response_decorator.create_response_bad_request(e.message)
        rescue @custom_error_util::InvalidDataError, @custom_error_util::EmailNotFoundError => e
          @user_response_decorator.create_response_invalid_data_passed(e.message)
        rescue @custom_error_util::RunTimeError => e
          @user_response_decorator.create_response_runtime_error(e.message)
        rescue @custom_error_util::UnAuthenticatedUserError => e
          @user_response_decorator.create_response_unauthenticated_user(e.message)
        end
      end

      #
      # Logic for generating and sending authentication token
      # to user corresponding to required email id
      #
      # @param args [Hash] Hash containing email id
      #
      # @return [Response] response to be sent to the user
      #
      # @raise [InsufficientDataError] if email id not present in hash passed
      # @raise [InvalidDataError] if no email associated with passed email-id or
      # email does not associate with requested user
      # @raise [EmailNotFoundError] when requested user does not have any email-id in DB
      # @raise [RunTimeError] if unable to save generate authentication token
      def resend_email_token_for_user(args)
        raise @custom_error_util::InsufficientDataError.new(@content_util::PARAMETER_MISSING) if args.blank?

        initialize_user(args)
        raise @custom_error_util::InsufficientDataError.new(@content_util::FIELD_MUST_PRESENT%{ field: 'Email ID' }) if @email_id.blank?

        email_service = @email_service.new
        email = email_service.get_email_from_email_id(@email_id)
        
        # get user from authenticated user id
        user = get_user_from_user_id(USER_SESSION[:user_id])

        emails = get_emails_for_user(user)

        #check whether email_id received belong to user
        if emails.include?(email)
          token = email_service.assign_new_authentication_token(email)
          #TO-DO: Send email-token mail to the given @email_id
          return @user_response_decorator.create_response_token_email_sent_successfully
        else
          raise @custom_error_util::InvalidDataError.new(@content_util::EMAIL_NOT_REGISTERED)
        end
      end

      #
      # Add roles to user
      # @param user_args [JSON] [JSON containing user_params and role_params]
      # user_args: {
      #   user_params: {
      #     user_id: 1234
      #   },
      #   role_params: {
      #     role_id: 6789
      #   }
      # }
      #
      # @return [Response] [response to be sent to the user]
      def add_role_to_user(user_args)
        begin
          response = add_role(user_args)
          return response
        rescue @custom_error_util::InsufficientDataError
          @user_response_decorator.create_response_bad_request
        rescue @custom_error_util::UserNotFoundError => e
          @user_response_decorator.create_response_invalid_arguments_error(e.message)
        rescue @custom_error_util::InvalidArgumentsError, @custom_error_util::InvalidDataError => e
          @user_response_decorator.create_response_invalid_arguments_error(e.message)
        end
      end

      #
      # Logic for adding role to the user
      # @param user_args [JSON] [JSON containing user_params and role_params]
      # user_args: {
      #   user_params: {
      #     user_id: 1234
      #   },
      #   role_params: {
      #     role_id: 6789
      #   }
      # }
      #
      # @return [Response] [Success response to be sent to the user]
      #
      # @raise [InsufficientDataError] if user_params or role_params are not present in hash
      # @raise [InvalidArgumentsError] if no role corresponds to passed role_id
      # @raise [UnAuthenticatedUserError] if no user is associated with passed user id
      def add_role(user_args)
        if user_args.blank? || user_args[:user_params].blank? || user_args[:role_params].blank?
          raise @custom_error_util::InsufficientDataError.new(@content_util::PARAMETER_MISSING)
        end
        validate_phone_number(user_args[:user_params][:phone_number])
        user = get_user_from_phone_number(user_args[:user_params][:phone_number])
        # Requester can not add role to himself
        validate_requester_different_from_user(user)
        role_service = @role_service.new(@params)
        role = role_service.get_role_from_role_id(user_args[:role_params][:role_id])
        # Add role to user
        user.add_role(role)
        return @user_response_decorator.create_role_added_response
      end

      #
      # Remove role from user
      # @param user_args [JSON] [JSON containing user_params and role_params]
      # user_args: {
      #   user_params: {
      #     user_id: 1234
      #   },
      #   role_params: {
      #     role_id: 6789
      #   }
      # }
      #
      # @return [Response] [response to be sent to the user]
      def remove_role_from_user(user_args)
        begin
          response = remove_role(user_args)
          return response
        rescue @custom_error_util::InsufficientDataError
          @user_response_decorator.create_response_bad_request
        rescue @custom_error_util::UnAuthenticatedUserError => e
          @user_response_decorator.create_response_invalid_arguments_error(e.message)
        rescue @custom_error_util::InvalidArgumentsError => e
          @user_response_decorator.create_response_invalid_arguments_error(e.message)
        end
      end

      # 
      # API service function to increase the referral limit of the user
      # refile args: {
      #     id: User id,
      #     referral_limit: integer, limit to be increase by   
      # }
      #
      def refill_referral_limit(refill_args)
        begin
          user = increase_user_referral_limit(refill_args)
          return @user_response_decorator.create_success_response(@content_util::REFERRAL_LIMIT_INCREASED)
        rescue @custom_error_util::InsufficientDataError => e
          @user_response_decorator.create_response_invalid_arguments_error(e.message)
        rescue @custom_error_util::UnAuthenticatedUserError => e
          @user_response_decorator.create_response_unauthenticated_user(e.message)
        rescue @custom_error_util::RunTimeError => e
          @user_response_decorator.create_response_runtime_error(e.message)
        rescue @custom_error_util::InvalidDataError => e
          @user_response_decorator.create_response_invalid_data_passed(e.message)
        end
      end

      # 
      # Service function to increase the referral limit of the user
      #
      def increase_user_referral_limit(refill_args)
        if refill_args.blank? || refill_args[:referral_limit].blank? || refill_args[:referral_limit] <= 0
          raise @custom_error_util::InsufficientDataError.new(
            @content_util::FIELD_ONLY_POSITIVE%{ field: 'Referral Limit'})
        end
        if refill_args[:id].blank?
          raise @custom_error_util::InsufficientDataError.new(
            @content_util::FIELD_MUST_PRESENT%{ field: 'User ID' })
        end
        user = get_user_from_user_id(refill_args[:id])
        user.referral_limit += refill_args[:referral_limit].to_i
        user.save_user
        return user
      end

      #
      # Logic for removing role from the user
      # @param user_args [JSON] [JSON containing user_params and role_params]
      # user_args: {
      #   user_params: {
      #     user_id: 1234
      #   },
      #   role_params: {
      #     role_id: 6789
      #   }
      # }
      #
      # @return [Response] [Success response to be sent to the user]
      #
      # @raise [InsufficientDataError] if user_params or role_params are not present in hash
      # @raise [InvalidArgumentsError] if no role corresponds to passed role_id
      # @raise [UnAuthenticatedUserError] if no user is associated with passed user id
      def remove_role(user_args)
        if user_args.blank? || user_args[:user_params].blank? || user_args[:role_params].blank?
          raise @custom_error_util::InsufficientDataError.new
        end
        user = get_user_from_user_id(user_args[:user_params][:user_id])
        role_service = @role_service.new(@params)
        role = role_service.get_role_from_role_id(user_args[:role_params][:role_id])
        # Remove role from user
        user.remove_role(role)
        return @user_response_decorator.create_role_removed_response
      end

      #
      # Create password for the user
      #
      # @param args [JSON] [Hash containing plain password]
      #
      # @return [Object] [User whose password is created]
      #
      def create_password(args)
        # get user from authenticated user id
        user = get_user_from_user_id(USER_SESSION[:user_id])
        provider_service = @provider_service.new(@params)
        user.provider = provider_service.create_password({
          provider: user.provider,
          password: args[:password]
        })
        return user
      end

      #
      # Check whether passed phone number belongs any of our registered
      # user or not
      #
      # @param args [JSON] [Hash containing phone_number]
      #
      # @return [JSON] [Hash containing is_user <Boolean> and user <UserObject>]
      #
      def check_user(args)
        phone_number = args[:phone_number]
        begin
          user = get_user_from_phone_number(phone_number)
          return {is_user: true, user: user}
        rescue @custom_error_util::UserNotFoundError => e
          return {is_user: false}
        end
      end

      #
      # Get all Users
      # It returns all Users according to the paginated params
      #
      # @return [Response] response to be sent to the user containing all Users
      #
      def get_all_users(paginate_params)
        users_hash = @user_model.get_all_users(paginate_params)
        return @user_response_decorator.create_all_users_paginate_response(users_hash)
      end

      #
      # get all pending users & trigger email to admin to activate them
      #
      def get_all_pending_users
        pagination_params = {
          state: [USER_MODEL_STATES::PENDING]
        }
        users_hash = @user_model.get_all_users(pagination_params)
      end

      #
      # Send mail to team regarding Pending Users
      #
      def send_pending_user_mail(users_hash)
        user_notification_service = @user_notification_service.new(@params)
        user_notification_service.send_all_pending_user_mail(users_hash)
      end

      #
      # Fetch user id from session token using provider service
      # @param token [String] session token associated with the user
      #
      # @return [Integer] unique id associated with the user
      def get_user_id_from_session_token(token)
        provider_service = @provider_service.new(@params)
        return provider_service.get_user_id_from_session_token(token)
      end

      #
      # Fetch user corresponding to passed user id
      # @param user_id [Integer] unique id of an user
      #
      # @return [Object] User object
      # @raise [@custom_error_util::UnAuthenticatedUserError] if invalid user id passed
      def get_user_from_user_id(user_id)
        begin
          user = @user_model.find_by_user_id(user_id)
        rescue ActiveRecord::RecordNotFound
          raise @custom_error_util::UnAuthenticatedUserError.new(@content_util::NO_USER)
        end
      end

      # 
      # Fetch user corresponding to passed phone user
      # @param phone_number [String] phone number of the uer
      # 
      # @return [User Object] if user exists with that phone number
      # @raise [@custom_error_util::UserNotFoundError] if user not found
      def get_user_from_phone_number(phone_number)
        @phone_number = phone_number
        user = user_exists

        if user.present?
          return user
        else
          raise @custom_error_util::UserNotFoundError.new(@content_util::NO_USER)
        end
      end

      # 
      # Associate email id with the user
      # @param args [Hash] Hash of email id, user object and account type
      # 
      # * Account type can be of 3 types: Customer, Employee and Vendor
      # @return [String] authentication token associated with the email
      def add_email_id_for_user(args)
        # Call Email service with params as email_id, user and user_type
        user = args[:user]
        email_service = @email_service.new
        email = email_service.add_email_for_user(args)
        user.emails << email
        user.save_user
        return user if email.present?
      end

      # 
      # Get employee corresponding to given user
      # @param user [User Object] User
      # 
      # @return [Employee Object] Employee associated with that user
      # @raise [@custom_error_util::EmployeeNotFoundError] if no employee is associated
      def get_employee_for_user(user)
        employee = user.employee
        if employee.present?
          return employee
        else
          raise @custom_error_util::EmployeeNotFoundError.new(@content_util::NOT_AN_EMPLOYEE)
        end
      end

      #
      # Get roles associated with that user
      #
      # @param user [User] [User for which role is requested]
      #
      # @return [Array] [Array of Role object. All the roles associated with that user]
      def get_roles_of_user(user)
        roles = user.roles
        if roles.present?
          return roles
        end
      end

      #
      # Get emails for the user
      #
      # @param user [User] [User for which email is requested]
      #
      # @return [Array] [Array of Email object. All the emails associated with that user]
      #
      # @raise [EmailNotFoundError] [When no email is associated with that user]
      def get_emails_for_user(user)
        emails = user.emails
        if emails.present?
          return emails
        else
          raise @custom_error_util::EmailNotFoundError.new(@content_util::EMAIL_NOT_REGISTERED)
        end
      end

      # 
      # To update user with the first_name and last_name if present
      # 
      def update_user(args)
        user = args[:user]
        user.first_name = args[:first_name] if args[:first_name].present?
        user.last_name = args[:last_name] if args[:last_name].present?

        # save changes. Raises exception if failed
        user.save_user
        return user
      end

      #
      # Assign mobile API role to user
      #
      # @param user [Object] [User to whol role is to be assigned]
      #
      # @return [Object] [User object after assigning role]
      #
      def assign_mobile_api_role_to_user(user)
        raise @custom_error_util::UserNotFoundError.new(@content_util::NO_USER) if user.blank?
        mobile_api_role_name = @cache_util.read('MOBILE_API_ROLE_NAME')
        mobile_api_permissions = @cache_util.read('MOBILE_API_PERMISSIONS')
        role_service = @role_service.new(@params)
        role = role_service.get_role_with_parameters({role_name: mobile_api_role_name, permissions: mobile_api_permissions})
        if role.blank?
          role = role_service.create_root_role_with_parameters({role_name: mobile_api_role_name, permissions: mobile_api_permissions})
        end
        user.add_role(role)
        return user
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # To initialize the class level paramaters from User_params
      # 
      def initialize_user(user_params)
        raise @custom_error_util::InsufficientDataError.new(@content_util::PARAMETER_MISSING)if user_params.blank?
        @first_name = user_params[:first_name] if user_params[:first_name].present?
        @last_name = user_params[:last_name] if user_params[:last_name].present?
        @referral_code = user_params[:referral_code] if user_params[:referral_code].present?
        @referred_by = user_params[:referred_by] if user_params[:referred_by].present?
        @referral_limit = user_params[:referral_limit] if user_params[:referral_limit].present?
        @email_id = user_params[:email_id] if user_params[:email_id].present?
      end

      # 
      # To initialize the class level phone_number
      # 
      # Parameters::
      #   * phone_number [string] [Stores the phone_number of User]
      #
      # Raise Error::
      #   * InsufficientDataError if required parameters are missing
      #   * InvalidDataError if given phone number is not valid
      def validate_phone_number(phone_number)
        raise @custom_error_util::InsufficientDataError.new(@content_util::FIELD_MUST_PRESENT%{ field: 'Phone Number' }) if phone_number.blank?
        if @general_helper.phone_number_valid?(phone_number)
          @phone_number = phone_number
        else
          raise @custom_error_util::InvalidDataError.new(@content_util::WRONG_PHONE_NUMBER)
        end
      end

      #
      # Validate Requester is different from user
      # Requester id will be fetched from USER_SESSION global variable, obtained
      # from session token (in access_control_service)
      #
      # @param user [User Object] [User which need to be checked]
      #
      # @raise [InvalidArgumentsError] [if no global session available OR requester is same as passed user]
      #
      def validate_requester_different_from_user(user)
        requester_id = USER_SESSION[:user_id]
        if requester_id.blank?
          raise @custom_error_util::InvalidArgumentsError.new(@content_util::NO_USER)
        end
        # Requester id should not equal to the subject user's id
        if requester_id == user.id
          raise @custom_error_util::InvalidArgumentsError.new(@content_util::ROLE_ASSIGN_NOT_ALLOWED)
        end
      end

      # 
      # To return the user who has referred the current user
      #  
      # @return [object] returns referred by user
      #  
      def get_referred_by_user
        user = @user_model.referred_by(@referral_code.upcase)
        # when user uses it's own referral code to activate himself
        raise @custom_error_util::InvalidArgumentsError.new(@content_util::BEING_SMART) if user == @user
        if user.present? && user.active?
          validate_referral_limit(user)
        else
          raise @custom_error_util::WrongReferralCodeError.new(@content_util::WRONG_REFERRAL_CODE)
        end
        return user
      end

      # 
      # validate if user already appliad referral code or not, if not then also
      # check for validity of referral code
      #
      # @param referral_code [String] referral code for user
      # @param user [Model] user model
      # 
      # @return [Model] referrer user
      #
      def validate_referral_code(referral_code)
        
        raise @custom_error_util::InvalidArgumentsError.new(@content_util::ALREADY_REFERRED) if @user.referred_by.present?

        referrer = @user_model.referred_by(referral_code.upcase)
        
        raise @custom_error_util::WrongReferralCodeError.new(@content_util::WRONG_REFERRAL_CODE) if referrer.blank?

        # when user uses it's own referral code to activate him
        raise @custom_error_util::InvalidArgumentsError.new(@content_util::BEING_SMART) if referrer == @user

        return referrer
      end

      # 
      # Function to chck validity of referral code
      #
      def validate_referral_limit(user)
        if user.referral_limit <= 0
          raise @custom_error_util::InvalidDataError.new(@content_util::REFERRAL_EXPIRED)
        else
          begin
            user.referral_limit -= 1
            user.save_user
          rescue @custom_error_util::RunTimeError
            # DO LOGGING OF THIS ERROR SINCE REDUCING LIMIT OF REFERRED USER FAILED TO SAVE
          end
          return true
        end
      end

      # 
      # Function to validate the params of email related args
      #   * email_id
      #   * is_primary
      #
      def validate_emails_params(email)
        if(email.present? && email[:email_id].present?)
          email[:email_id] = email[:email_id].delete(' ')
        end
        validate_email_id(email[:email_id])
        if @general_helper.string_to_boolean(email[:is_primary])
          email[:is_primary] = true 
        else
          email[:is_primary] = false
        end
        return email
      end

      # 
      # To validate class level email id
      # 
      # Parameters::
      #   * email_id [string] [Stores the email_id of User]
      #
      # Raise Error::
      #   * InvalidDataError if given email id is not valid
      def validate_email_id(email_id)
        unless @general_helper.email_id_valid?(email_id)
          raise @custom_error_util::InvalidDataError.new(@content_util::INVALID_EMAIL_ID)
        end
      end

      # 
      # Function to save user id of referrer in referred_by
      # 
      def set_referred_by(referred_user)
        @user.referred_by = referred_user.id
        @user.save
      end

      # 
      # Function to Activate the user when referral code verification is done
      # 
      # @return [string] return the current state of user
      def activate_user
        @user.trigger_event(USER_MODEL_EVENTS::ACTIVATE)
        send_welcome_sms(@user.phone_number)
        push_notification(@user.id, @notification_type::WELCOME[:type])
        return @user.current_state.name
      end

      # 
      # function to call login user function in LoginUserService
      # 
      # Parameters::
      #   * Parameters::
      #   * user_params [hash] of phone_number & password or otp
      #   * user [object] has user who wants to update provider type
      #   * request_type [enum] to know if provider is Generic or message
      # 
      # @return [string] returns session_token
      def login_user(args)
        login_service = @login_service.new(@params)
        login_service.login_user(args)
      end

      # 
      # function to call update_login_type function in LoginUserService
      # 
      # Parameters::
      #   * user_params [hash] of phone_number & password
      #   * user [object] has user who wants to update provider type
      #   * request_type [enum] to know if provider is Generic or message
      # 
      # @return [object] returns user object after update
      def update_login_type(args)
        user = args[:user]
        login_service = @login_service.new(@params)
        user.provider = login_service.update_login_type(args)
        return user
      end

      # 
      # function to call login user function in RegisterUserService
      # 
      # # Parameters::
      #   * user_params [hash] of phone_number & password
      #   * request_type [enum] to know if provider is Generic or message
      # 
      # @return [object] returns user object after new user object is created
      def register_user(user_params, request_type)
        register_service = @register_service.new(@params)
        profile_service = @profile_service.new(@params)
        benefit_service = @benefit_service.new(@params)
        ample_credit_dao = @ample_credit_dao.new(@params)
        user = create_user
        user.provider = register_service.register_user(user_params, request_type)
        user.profile = profile_service.new_profile
        user = create_trial_membership(user)
        user.save_user
        # to add on registration benefits to user
        benefit_service.add_initial_benefits_to_user(user)
        ample_credit_dao.create({ user_id: user.id })
        return user
      end

      # 
      # Function to associate trial membership with the User
      #
      def create_trial_membership(user)
        plan_service = @plan_service.new(@params)
        plan = plan_service.get_plan_by_key(@plan_key::TRIAL)
        args = { plan: { id: plan.id } }
        membership_service = @membership_service.new(@params)
        membership_args = membership_service.subscribe_user_to_membership(args, user)
        user.memberships << membership_args[:membership]
        return user
      end

      # 
      # create user
      # 
      def create_user
        referral_code = check_referral_code
        referral_limit = @cache_util.read('REFERRAL_LIMIT').to_i
        benefit_limit = @cache_util.read('BENEFIT_LIMIT').to_i
        @user_model.new(phone_number: @phone_number,
                        referral_code: referral_code.upcase,
                        referral_limit: referral_limit,
                        benefit_limit: benefit_limit)
      end

      # 
      # Function to generate unique referral code & keep on generating unless
      # we get the unique referral code
      # Putting a threshold of 100 retries, after that we add one more digit
      # to the referral code
      #
      def check_referral_code
        threshold = 1
        begin
          referral_code = @user_service_helper.create_referral_code
          user = @user_model.referred_by(referral_code.upcase)
          threshold += 1
        end while (user != nil && threshold < 100)

        if threshold == 100
          referral_code = referral_code + rand(0..9).to_s
        end

        return referral_code
      end

      # 
      # find user
      # 
      def user_exists
        @user = @user_model.exists?(@phone_number)
      end

      #
      # validate user existence
      # 
      def validate_user_existence
        raise @custom_error_util::UnAuthenticatedUserError.new(@content_util::NO_USER) if @user.blank?
      end

      #
      # validate presence of provider
      # 
      def validate_provider_existence
        raise @custom_error_util::RunTimeError.new(@content_util::NO_PROVIDER) if @user.provider.blank?
      end

      # 
      # function to send otp to the user
      #
      def send_otp(otp, phone_number)
        begin
          message_attributes = { otp: otp, phone_number: @user.phone_number }
          @sms_worker.perform_async(message_attributes, @sms_type::OTP)
        rescue => e
          ApplicationLogger.debug('Failed to send OTP SMS to user : ' + user.id.to_s + ' , ' + e.message)
        end
      end

      #
      # Function to send welcome to Ample club sms
      #
      def send_welcome_sms(phone_number)
        begin
          message_attributes = { phone_number: phone_number}
          @sms_worker.perform_async(message_attributes, @sms_type::WELCOME)
        rescue => e
          ApplicationLogger.debug('Failed to send Welcome SMS to user : ' + user.id.to_s + ' , ' + e.message)
        end
      end

      # 
      # Function to get benefits of membershit which are related to the address
      #
      def check_user_membership_benefits_for_address(user)
        address_benefit = nil
        membership_service = @membership_service.new(@params)
        membership = membership_service.get_pending_or_active_membership(user)
        raise @custom_error_util::InvalidArgumentsError.new(
          @content_util::ADDRESS_NOT_ALLOWED) if membership.blank?
        raise @custom_error_util::InvalidArgumentsError.new(
          @content_util::ADDRESS_NOT_ALLOWED) if !(membership.active? || membership.pending?)
        benefits = membership.plan.benefits
        if benefits.present?
          benefit_service = @benefit_service.new(@params)
          address_benefit = benefit_service.get_address_benefit(benefits)
        end
        address_service = @address_service.new(@params)
        active_addresses = address_service.get_active_addresses_of_user(user)
        addresses_count = active_addresses.count
        return is_new_address_allowed?(address_benefit, addresses_count)
      end

      # 
      # Function checks if User is allowed to add more addresses or not
      # based upon the membership benefit related to addresses
      #
      def is_new_address_allowed?(address_benefit, addresses_count)
        if address_benefit.blank?
          if addresses_count >= @cache_util.read('DEFAULT_ADDRESS_COUNT').to_i
            raise @custom_error_util::InvalidArgumentsError.new(
              @content_util::ADDRESS_NOT_ALLOWED)
          else
            return true
          end
        else
          if addresses_count <= address_benefit.reward_value.to_i
            return true
          else
            raise @custom_error_util::InvalidArgumentsError.new(
              @content_util::ADDRESS_NOT_ALLOWED)
          end
        end
      end

      #
      # function to send push notification to user
      #
      def push_notification(user_id, notification_type)
        begin
          params = {
            username: 'parse_user_' + user_id.to_s,
            notification_type: notification_type
          }
          @parse_notification_worker.perform_async(params)
        rescue => e
          ApplicationLogger.debug('Failed to push ' + notification_type  + 'notification to user : ' + user.id.to_s + ' , ' + e.message)
        end
      end

      # 
      # delete user
      #
      def delete_user
        #To-Do
      end
    end
  end
end

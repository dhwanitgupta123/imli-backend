#
# Module to handle user related functionality
#
module UsersModule
  module V1
    #
    # Module to handle membership related functionality
    #
    module MembershipModule
      module V1
        #
        # Membership Payment service class handles the membership payment related logic functionality
        #
        class MembershipPaymentService < BaseModule::V1::BaseService

          CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
          CONTENT_UTIL = CommonModule::V1::Content
          PAYMENT = UsersModule::V1::MembershipModule::V1::MembershipPayment
          PAYMENT_HELPER = MarketplaceOrdersModule::V1::PaymentHelper
          MEMBERSHIP_PAYMENT_HELPER = UsersModule::V1::MembershipModule::V1::MembershipPaymentHelper
          MEMBERSHIP_PAYMENT_MODES = UsersModule::V1::MembershipModule::V1::MembershipPaymentModes

          # 
          # Function returns new Payment object of membership payment
          #
          def new_payment(payment_args)
            payment = PAYMENT.new
            return payment.new_payment(payment_args)
          end

          # 
          # Function calculates the amount to be paid to for the membership subscrition
          #   * It handles if the user has one already active membership
          #
          def calculate_membership_payment(membership, plan)
            refund = 0.0
            if membership.present?
              if membership.plan != plan
                refund = get_membership_remaining_cost(membership)
              end
            end
            grand_total = get_new_membership_cost(plan)
            billing_amount = grand_total - refund
            net_total = billing_amount
            payment_args = {
              grand_total: grand_total,
              billing_amount: billing_amount,
              net_total: net_total,
              refund: refund,
              total_savings: BigDecimal.new('0.0')
            }
            details_array = MEMBERSHIP_PAYMENT_HELPER.calculate_payment_mode_details(payment_args)
            return { payment_args:  payment_args, payment_mode_details: details_array }
          end

          # 
          # Function calculates the final amount to be paid by the user
          # when payment is initiated.
          # It calcluates the discount based on Payment mode
          #
          def final_membership_payment(membership, payment_args)
            payment_mode_savings = get_payment_mode_savings(payment_args[:mode].to_s)
            payment = membership.membership_payment
            payment.payment_mode_savings = payment_mode_savings * payment.billing_amount / 100
            payment.total_savings = payment_mode_savings * payment.billing_amount / 100
            payment.net_total = payment.billing_amount - payment.payment_mode_savings
            return payment
          end

          # 
          # Function fetches the payment mode savings
          #
          def get_payment_mode_savings(mode)
            payment_details = PAYMENT_HELPER.fetch_payment_details_hash
            mode_details = payment_details[mode]
            if mode_details.blank? || !MEMBERSHIP_PAYMENT_MODES::ALLOWED_MODES.include?(mode.to_i)
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_PAYMENT_MODE)
            end
            payment_mode_savings = mode_details['payment_mode_savings'].to_d
          end

          # 
          # Function calculates cost of remaining user's active membership
          #   * This amount gets adjusted in the cost of new of membership subscribed
          #
          def get_membership_remaining_cost(membership)
            membership_used_in_days = (Time.zone.now.to_date - membership.starts_at.to_date).round
            membership_cost_paid = membership.plan.fee * membership.quantity
            plan_duration = membership.plan.duration
            cost_of_membership_used = membership_used_in_days * membership_cost_paid / plan_duration
            refund  = membership_cost_paid  - cost_of_membership_used
            return refund
          end

          # 
          # Function returns the fee of new plan
          #
          def get_new_membership_cost(plan)
            return plan.fee
          end

          # 
          # Function fetches payment based on it's ID
          #
          def get_payment_by_id(payment_args)
            payment = PAYMENT.new
            return payment.get_payment_by_id(payment_args[:id])
          end
        end
      end
    end
  end
end

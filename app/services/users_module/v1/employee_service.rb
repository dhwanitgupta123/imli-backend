require 'transaction_helper'
module UsersModule
  module V1
    class EmployeeService < BaseModule::V1::BaseService
      # Add versioned classed in global constants
      EMPLOYEE_MODEL = UsersModule::V1::Employee
      USER_SERVICE = UsersModule::V1::UserService
      EMAIL_SERVICE = UsersModule::V1::EmailService
      GENERAL_HELPER = CommonModule::V1::GeneralHelper
      EMPLOYEE_RESPONSE_DECORATOR = UsersModule::V1::EmployeeResponseDecorator
      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      PROVIDER_TYPE_UTIL = UsersModule::V1::ProviderType
      ACCOUNT_TYPE_UTIL = UsersModule::V1::AccountType
      CONTENT_UTIL = CommonModule::V1::Content


      # 
      # Initialize Employee service with required params
      #
      # @param params [Hash] Hash object of parameters
      #
      def initialize(params)
        @params = params
      end

      # 
      # Register employee
      # @param args [Hash] Hash object of employee params
      # 
      # @return [Json] Response to be sent to the user
      def register(args)
        begin
          response = register_employee(args)
          return response
        rescue CUSTOM_ERROR_UTIL::ExistingEmployeeError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_exisiting_employee_error(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERROR_UTIL::InvalidDataError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERROR_UTIL::RunTimeError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        rescue CUSTOM_ERROR_UTIL::ExistingUserError => e
          USER_RESPONSE_DECORATOR.create_exisiting_user_error(e.message)
        end
      end

      # 
      # Logic for registering employee.
      # Description:
      #  * Calls user service to register/login user
      #  * Add email-id to the above created/updated user
      #  * Create Employee object with relevant details and associate
      #    it with user
      #  * Send approval mail to the manager of given employee
      #
      # @param args [Hash] Hash object of employee params
      # 
      # @return [response] response to be sent to the user
      def register_employee(args)
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::PARAMETER_MISSING) if args.blank?
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::PARAMETER_MISSING) if args[:employee_params].blank? || args[:user_params].blank?
        employee_params = args[:employee_params].merge(args[:user_params])
        initialize_employee_params(employee_params)
        validate_employee_params
        validate_employee_existence

        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          # UserService will register user, if first time.
          # If not, then it will add its credentials as password with existing user
          user_service = new_user_service
          user = user_service.decide_register_login_for_user({
              user_params: args[:user_params],
              request_type: PROVIDER_TYPE_UTIL::GENERIC
              })
          
          # Add employee profile
          employee = add_employee_profile(user,{
            employee_params: employee_params,
            user_params: args[:user_params]
            })

          # Send employee approval mail to manager
          employee = send_approval_mail_to_manager(employee, @email_id)
          return EMPLOYEE_RESPONSE_DECORATOR.create_ok_response_for_employee(user, employee)
        end

        transaction_status = TransactionHelper.new({
          :function => transactional_function,
          :args => args
          })
        transaction_status.run();
      end

      # 
      # To add profile of an employee
      # 
      # Parameters::
      #   * employee_args [hash] consisting of user and manager params
      #   * session_token: a valid session_token is must
      # Description::
      #   * Service function to add employee profile
      #   * Fetches a valid user using session_token
      #    
      # @return [response] 
      # 
      def add_profile(employee_args)
        user = find_user_using_session
        @employee = add_employee_profile(user, employee_args)

        EMPLOYEE_RESPONSE_DECORATOR.create_ok_response_for_employee(user, @employee) if @employee.present?
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERROR_UTIL::RunTimeError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        rescue CUSTOM_ERROR_UTIL::InvalidDataError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
      end

      # 
      # Logic for adding emplyee profile
      # @param args [Hash] Hash of employee and user object
      # 
      # Description::
      #   
      #   * Calls user service to add email_id to the user profile for employee
      #   * Saves employee Record
      # Raise Errors::
      #   * InsufficientDataError: if required parameters are missing or not proper
      #   * RunTimeError: if user record is invalid or email_id is (invalid/duplicated)
      #    
      # @return [Employee object] User object created after successful operation
      # 
      def add_employee_profile(user,employee_args)
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::PARAMETER_MISSING) if employee_args.blank?
        initialize_employee_params(employee_args[:employee_params])
        validate_employee_profile_params
        # Fetch employee's manager before creating/updating user
        # to avoid ambiguity later on
        manager = get_manager

        args = {
          employee_params: employee_args[:employee_params],
          user: user,
          user_params: employee_args[:user_params],
          manager: manager
        }
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          user_service = new_user_service
          user = user_service.update_user_info_details({
            user: args[:user],
            first_name: args[:user_params][:first_name],
            last_name: args[:user_params][:last_name],
            email: { email_id: args[:user_params][:email_id], is_primary: false },
            account_type: ACCOUNT_TYPE_UTIL::EMPLOYEE
            })

          # Add employee details in DB
          employee = create_employee_with_details({
            user: user,
            manager: args[:manager]
           })
          return employee
        end

        transaction_status = TransactionHelper.new({
          :function => transactional_function,
          :args => args
          })
        transaction_status.run();
      end

      # 
      # Login employee
      # @param args [Hash] Hash of employee params
      # 
      # @return [Json] Json response to be returned to the user
      def login(args)
        begin
          response = login_employee(args)
          return response
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_response_still_pending_state(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERROR_UTIL::InvalidDataError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERROR_UTIL::RunTimeError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        rescue CUSTOM_ERROR_UTIL::WrongPasswordError => e
          USER_RESPONSE_DECORATOR.create_wrong_password_request(e.message)
        rescue CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_not_found_error(e.message)
        rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError, CUSTOM_ERROR_UTIL::EmployeeNotFoundError => e
          USER_RESPONSE_DECORATOR.create_response_unauthenticated_user(e.message)
        end
      end

      # 
      # Logic for signing up employee
      # Description
      #   * check employee existence from given phone number
      #   * If user is Active, then allow him to login. Else, throw
      #     PreConditionRequiredError
      # @param args [Hash] Hash of employee params including credentials
      # 
      # @return [String] session token
      def login_employee(args)
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::PARAMETER_MISSING) if args.blank?
        initialize_employee_params(args[:employee_credentials])
        validate_employee_credentials

        employee = get_employee_from_phone_number(@employee_phone_number)

        if employee.active?
          user_service = new_user_service
          session_token = user_service.login_for_user({
            user_params: args[:employee_credentials],
            request_type: PROVIDER_TYPE_UTIL::GENERIC
            })
          return EMPLOYEE_RESPONSE_DECORATOR.create_session_token_response(session_token, employee)
        else
          raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT_UTIL::EMPLOYEE_PENDING)
        end
      end

      #
      # Aproove employee (Activate its profile)
      # @param args [Hash] Hash containing employee email/authentication token
      #
      # @return [Json] Json response to be returned to the user
      def approve(args)
        begin
          response = approve_employee(args)
          return response
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERROR_UTIL::InvalidDataError, CUSTOM_ERROR_UTIL::EmployeeNotFoundError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          EMPLOYEE_RESPONSE_DECORATOR.create_response_still_inactive_state(e.message)
        end
      end

      #
      # Logic for approving employee and changing its state to active
      # @param token [String] authentication token of the employee's email
      #
      # @return [Json] Json response to be sent to the user
      def approve_employee(args)
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::TOKEN_NOT_FOUND) if args.blank? || args[:token].blank?

        # authenticate email token of employee
        user_service = new_user_service
        user = user_service.authenticate_email(args[:token])

        # set employee status to active
        employee = user_service.get_employee_for_user(user)
        raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT_UTIL::PRE_CONDITION_REQUIRED) if employee.inactive?

        employee.set_state_to_active

        return EMPLOYEE_RESPONSE_DECORATOR.create_employee_state_response(employee)
      end

      #
      # Create employee with the parameters passed 
      # @param args [Hash] Hash of params
      #
      # @return [Employee Object] created employee object
      # @raise [CUSTOM_ERROR_UTIL::RunTimeError] if creation failed (mainly due to validation)
      def create_employee_with_details(args)
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::PARAMETER_MISSING) if args.blank?

        # create employee with all the passed parameters
        # and save it to the DB
        # Raises RunTimeError is unable to save to DB
        employee = EMPLOYEE_MODEL.create_employee(
          :designation => @designation,
          :team => @team,
          :user => args[:user],
          :manager => args[:manager]
        )
        return employee
      end

      # 
      # Check whether employee already exists or not
      # @param phone_number [String] Phone number to be checked
      # 
      # @return [Boolean] true if employee already registered with
      # the provided phone_number or else false
      def employee_already_exists?(phone_number)
        begin
          employee = get_employee_from_phone_number(phone_number)
          return true
        rescue CUSTOM_ERROR_UTIL::EmployeeNotFoundError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
          return false
        end
      end

      # 
      # Get Employee corresponding to passed phone number
      # @param phone_number [String] phone number of user
      # 
      # @return [Object] Employee object corresponding phone number
      # @raise [CUSTOM_ERROR_UTIL::EmployeeNotFoundError] Employee not found corresponding
      # to passed phone number
      def get_employee_from_phone_number(phone_number)
        begin
          user_service = new_user_service
          user = user_service.get_user_from_phone_number(phone_number)
          employee = user_service.get_employee_for_user(user)
          return employee
        rescue CUSTOM_ERROR_UTIL::UserNotFoundError => e
          raise CUSTOM_ERROR_UTIL::UnAuthenticatedUserError.new(CONTENT_UTIL::WRONG_PHONE_NUMBER)
        rescue CUSTOM_ERROR_UTIL::EmployeeNotFoundError => e
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT_UTIL::NOT_AN_EMPLOYEE)
        end
      end

      #
      # Send approval mail to the corresponding manager
      # @param employee [Object] employee whose approval mail to be sent
      #
      def send_approval_mail_to_manager(employee, email_id)
        employee.set_state_to_pending
        # Temporarily, approving Employee
        # Employee can be approved using token associated with his official email_id
        user_service = new_user_service
        email = user_service.get_email_with_type(email_id, ACCOUNT_TYPE_UTIL::EMPLOYEE)
        approve_employee({
          token: email.authentication_token
          })
        #
        # TO-DO: Send mail to manager for approval
        # send_mail_to_employee({token: email.authentication_token})
        return employee
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # To initialize the class level paramaters from employee params
      #
      # @param [Hash] Hash of parameters: It might contain employee_phone_number,
      # team, password, designation, email_id, manager_phone_number
      #
      # @raise [InsufficientDataError] if sufficient params were not passed
      def initialize_employee_params(employee_params)
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::PARAMETER_MISSING) if employee_params.blank?
        @employee_phone_number = employee_params[:phone_number] if employee_params[:phone_number].present?
        @team = employee_params[:team] if employee_params[:team].present?
        @password = employee_params[:password] if employee_params[:password].present?       
        @designation = employee_params[:designation] if employee_params[:designation].present?
        @email_id = employee_params[:email_id] if employee_params[:email_id].present?
        @manager_phone_number = employee_params[:manager_phone_number] if employee_params[:manager_phone_number].present?
      end

      #
      # Validate employee credentials. Used during login to validate
      # phone_number and password
      #
      # @raise [InsufficientDataError] if sufficient params were not passed
      def validate_employee_credentials
        if(@employee_phone_number.blank? || @password.blank? )
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Phone Number & Password' })
        end
        # validate phone number
        if ! GENERAL_HELPER.phone_number_valid?(@employee_phone_number)
          raise CUSTOM_ERROR_UTIL::InvalidDataError.new(CONTENT_UTIL::WRONG_PHONE_NUMBER)
        end
      end

      #
      # Validate employee parameters. Used during register to validate
      # all kind of parameters required during registration
      #
      # @raise [InsufficientDataError] if sufficient params were not passed
      # @raise [InvalidDataError] if required params were not valid
      def validate_employee_params
        if ( @employee_phone_number.blank? || @email_id.blank? || @manager_phone_number.blank? || @designation.blank? || @team.blank? )
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Phone Number, Password, Email ID, Team, Designation, Manager Phone Number' })
        end
        # validate phone number
        unless GENERAL_HELPER.phone_number_valid?(@employee_phone_number)
          raise CUSTOM_ERROR_UTIL::InvalidDataError.new(CONTENT_UTIL::WRONG_PHONE_NUMBER)
        end
        unless GENERAL_HELPER.phone_number_valid?(@manager_phone_number)
          raise CUSTOM_ERROR_UTIL::InvalidDataError.new(CONTENT_UTIL::WRONG_MANAGER_PHONE_NUMBER)
        end
      end

      #
      # Validate employee profile parameters. Used during add employee profile
      # all kind of parameters required during registration
      #
      # @raise [InsufficientDataError] if sufficient params were not passed
      # @raise [InvalidDataError] if required params were not valid
      def validate_employee_profile_params
        if ( @manager_phone_number.blank? || @designation.blank? || @team.blank? )
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new
        end
        # validate phone number
        unless ( GENERAL_HELPER.phone_number_valid?(@manager_phone_number) )
          raise CUSTOM_ERROR_UTIL::InvalidDataError.new(CONTENT_UTIL::WRONG_MANAGER_PHONE_NUMBER)
        end
      end

      # 
      # Validate whether employee already exists or not
      #
      # @raise [CUSTOM_ERROR_UTIL::ExistingEmployeeError] if employee already exists
      def validate_employee_existence
        if employee_already_exists?(@employee_phone_number)
          raise CUSTOM_ERROR_UTIL::ExistingEmployeeError.new(CONTENT_UTIL::ALREADY_REGISTERED)
        end
      end

      # 
      # Get manager corresponding to globally set params
      # 
      # @return [Employee Object] Employee Object (manager)
      # @raise [CUSTOM_ERROR_UTIL::InvalidDataError] Manager details were invalid
      def get_manager
        begin
          manager = get_employee_from_phone_number(@manager_phone_number)
          return manager
        rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
          raise CUSTOM_ERROR_UTIL::InvalidDataError.new(CONTENT_UTIL::WRONG_MANAGER_PHONE_NUMBER)
        end
      end

      # 
      # Used to find a user from User Session
      # 
      # @return [User object] User object created after successful operation 
      def find_user_using_session
        user_service = new_user_service
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
        return user
      end

      # 
      # To instantiate user service
      # 
      def new_user_service
        return USER_SERVICE.new(@params)
      end

    end
  end
end
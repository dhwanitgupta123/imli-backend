module UsersModule
  module V1
    class UserNotificationService < BaseModule::V1::BaseService

    	USER_EMAIL_WORKER = CommunicationsModule::V1::Mailers::V1::UserEmailWorker
      PROMOTION_WORKER = CommunicationsModule::V1::Mailers::V1::PromotionEmailWorker
    	EMAIL_TYPE = CommunicationsModule::V1::Mailers::V1::EmailType
      EMAIL_SERVICE = UsersModule::V1::EmailService
      CACHE_UTIL = CommonModule::V1::Cache
      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper

    	def initialize(params)
        @params = params
      end

      #
      # Function to send welcome to Ample club email
      #
      def send_welcome_email(email_id)
        return if email_id.blank?
        email_service = EMAIL_SERVICE.new
        begin
          email = email_service.get_email_from_email_id(email_id)
          authentication_token = email.authentication_token
          current_host = CACHE_UTIL.read('HOST')
          api_url = current_host.to_s + '/api/users/verify_email?token=' + authentication_token.to_s

          email_subject = CACHE_UTIL.read('WELCOME_EMAIL_SUBJECT')
          mail_attributes = {
            email_type: EMAIL_TYPE::WELCOME[:type], 
            api_url: api_url,
            subject: email_subject.to_s,
            to_first_name: email.user.first_name.to_s,
            to_last_name: email.user.last_name.to_s,
            to_email_id: email.email_id.to_s,

          }
          USER_EMAIL_WORKER.perform_async(mail_attributes)
        rescue => e
          ApplicationLogger.debug('Failed to send Welcome Email to user id:' + email.user.id.to_s + 'for email id:' + email.id.to_s + ' , ' + e.message)
        end
      end

      #
      # Function to send email of list of Pending Users
      #
      def send_all_pending_user_mail(users_hash)
        if users_hash[:users].present?
          users = USER_SERVICE_HELPER.create_users_json(users_hash[:users])
          mail_attributes = {
            email_type: EMAIL_TYPE::PENDING_USERS[:type],
            to_name: CACHE_UTIL.read('IMLI_TEAM_NAME'),
            to_email_id: CACHE_UTIL.read('IMLI_TEAM_MAIL_IDS').to_s,
            subject: CACHE_UTIL.read('PENDING_USERS'),
            multiple: true,
            pdf_needed: true,
            users: users
          }
          USER_EMAIL_WORKER.perform_async(mail_attributes)
          return users
        end
      end

      #
      # Function to send invite email
      #
      def send_invite_mail(emails_array)
        unless emails_array.blank?
          emails_string = ""
          emails_array.each do |email|
            emails_string = emails_string + ',' + email[:email_id]
          end
          mail_attributes = {
            email_type: EMAIL_TYPE::INVITE[:type],
            from: CACHE_UTIL.read('INVITE_EMAIL_FROM'),
            to_email_id: emails_string.to_s,
            subject: CACHE_UTIL.read('INVITE_MAIL_SUBJECT'),
            multiple: true
          }
          PROMOTION_WORKER.perform_async(mail_attributes)
        end
      end

    end
  end
end

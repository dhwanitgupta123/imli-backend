require "route_constraints"
module UsersModule
  module V1
    class ProviderService < BaseModule::V1::BaseService

      def initialize(params)
        @params = params
        @provider_type_util = UsersModule::V1::ProviderType
        @custom_error_util = CommonModule::V1::CustomErrors
        @routing_predicates_util = CommonModule::V1::RoutingPredicates
        @general_helper = CommonModule::V1::GeneralHelper
        @content_util = CommonModule::V1::Content
        generic_provider = "UsersModule::V1::ProvidersModule::%{version}::GenericProviderService"
        message_provider = "UsersModule::V1::ProvidersModule::%{version}::MessageProviderService"
        @provider_model = UsersModule::V1::Provider
        if should_use_module_version({version: 'V1', default: true})
          version_to_use = 'V1'
        end
        @message_provider_service = @general_helper.string_to_class_with_version(message_provider, version_to_use)
        @generic_provider_service = @general_helper.string_to_class_with_version(generic_provider, version_to_use)
      end

      # 
      # Function to decide whether service calls should route
      # to specified version or not based on initialized params
      # @param version [String] [version to verify]
      # @param default [Boolean] [route to this version by default]
      # 
      # @return [Boolean] [true if decider/predicate passes for the
      # specified version else false]
      def should_use_module_version(args)

        # Predicate for routing constraint. Need to be a local procedural block
        match_from_yml = Proc.new do |version, args|
          @routing_predicates_util.match_from_yml(version, args)
        end
        # Initializing routing constraint
        module_routing_constraint = RouteConstraints.new({
          :version => args[:version],
          :predicate => match_from_yml,
          :default => args[:default]
          })

        module_routing_constraint.matches?(@params)
      end

      # 
      # function to create new object of provider type
      # 
      # Paramateres::
      #   * user_params [hash] stores the storng parameters of user
      #   * request type [enum] if provider type is SMS or GENERIC
      # 
      # Description::
      #   * Calls new_generic_provider() if request_type is GENERIC & password is not blank
      #   * Calls new_message_provider() if request_type is SMS
      # 
      # Raise Error::
      #   * InsufficientDataError if password is missing for request_type :: GENERIC
      # 
      # @return [object] provider object
      def new_provider(user_params, request_type)
        if request_type == @provider_type_util::GENERIC
          provider = create_provider_model
          provider.generic = new_generic_provider(user_params[:password])
        elsif request_type == @provider_type_util::SMS
          provider = create_provider_model
          provider.message = new_message_provider
        else
          raise @custom_error_util::InvalidDataError.new(@content_util::INVALID_PROVIDER_TYPE)
        end
        return provider
      end

      # 
      # function to update provider type
      # 
      # Paramateres::
      #   * user_params [hash] stores the storng parameters of user
      #   * request type [enum] if provider type is SMS or GENERIC
      #   * provider [object] provider object which needs to be updated
      # 
      # Description::
      #   * Calls new_generic_provider() if request_type is GENERIC & password is not blank 
      #     & generic provider doesnt exists
      #   * Calls create_update_message_provider() if request_type is SMS
      #   * args to create_update_message_provider depends on existence of provider.message
      # 
      # Raise Error::
      #   * ExistingUserError if reuest_type is GENERIC & provider.generic exists
      #   * InsufficientDataError if password is missing for request_type :: GENERIC & provider.generic doesn't exists
      # 
      # @return [object] provider object
      def update_provider_type(args)
        request_type = args[:request_type]
        provider = args[:provider]
        user_params = args[:user_params]
        if request_type == @provider_type_util::GENERIC
          raise @custom_error_util::ExistingUserError.new(@content_util::ALREADY_REGISTERED) if provider.generic.present?
          provider.generic = new_generic_provider(user_params[:password])
        elsif request_type == @provider_type_util::SMS
          message = provider.message.present? ? provider.message : nil
          provider.message = create_update_message_provider(message)
        elsif request_type == @provider_type_util::FORGOT_PASSWORD
          # FORGOT PASSWORD request will start authentication by OTP method for now.
          # It can be generalized later on from frontend side when other authentication
          # methods will come into picture
          message = provider.message.present? ? provider.message : nil
          provider.message = create_update_message_provider(message)
          provider = initiate_reset_password_process( { provider: provider } )
        else
          raise @custom_error_util::InvalidDataError.new(@content_util::INVALID_PROVIDER_TYPE)
        end
        return provider
      end

      # 
      # function to validate provider for the user requesting for log in
      # 
      # Paramateres::
      #   * user_params [hash] of phone_number & password or otp
      #   * user [object] has user who wants to update provider type
      #   * request_type [enum] to know if provider is Generic or messag
      # 
      # Description::
      #   * Calls verify_password() if password is present & generic provider exists
      #   * Calls verify_otp() if otp is present & message provider exists
      # 
      # Raise Error::
      #   * InsufficientDataError if password & both are missing for any request type
      # 
      # @return [string] returns session token if user is verified
      def check_provider(args)
        request_type = args[:request_type]
        user_params = args[:user_params]
        provider = args[:provider]
        raise @custom_error_util::InsufficientDataError.new(@content_util::FIELD_MUST_PRESENT%{ field: 'Provider type' }) if request_type.blank?
        if request_type == @provider_type_util::GENERIC 
          raise @custom_error_util::InsufficientDataError.new(@content_util::FIELD_MUST_PRESENT%{ field: 'Password' }) if user_params[:password].blank?
          session_token = provider.session_token if login_with_password(user_params[:password], provider)
        elsif request_type == @provider_type_util::SMS 
          raise @custom_error_util::InsufficientDataError.new(@content_util::FIELD_MUST_PRESENT%{ field: 'OTP' }) if user_params[:otp].blank?
          session_token = provider.session_token if login_with_otp(user_params[:otp], provider)
        else
          raise @custom_error_util::InvalidDataError.new(@content_util::INVALID_PROVIDER_TYPE)
        end
        return session_token.present? ? session_token : (raise @custom_error_util::RunTimeError.new(@content_util::SESSION_TOKEN_NOT_FOUND))
      end

      # 
      # Function to call message provider service to resend otp
      # 
      # Paramaters::
      #   * provider [object] for which we have to resend the OTP
      # 
      # @return [type] [description]
      def resend_otp(provider)
        raise @custom_error_util::RunTimeError.new(@content_util::MESSAGE_PROVIDER_MISSING) if provider.message.blank?
        message_provider_service = @message_provider_service.new
        otp = message_provider_service.resend_otp(provider.message)
      end

      # 
      # Fetch user id corresponding to passed session token
      # @param token [String] session token of user
      # 
      # @return [Integer] unique id of the user
      def get_user_id_from_session_token(token)
        return nil if token.blank?
        provider = @provider_model.get_provider_for_session_token(token)

        if provider.present? && provider.user.present?
          return provider.user.id
        else
          @custom_error_util::UnAuthenticatedUserError.new(@content_util::NO_USER)
        end
      end

      #
      # Initiate reset password process. Set the reset_flag to true
      # in password (generic) table
      #
      # @param args [JSON] [Hash containing provider]
      # 
      # @return [Object] [Provider which is updated]
      #
      def initiate_reset_password_process(args)
        provider = args[:provider]
        if provider.generic.present?
          generic_provider_service = @generic_provider_service.new
          provider.generic = generic_provider_service.set_reset_flag(provider.generic, true)
        else
          raise @custom_error_util::PreConditionRequiredError.new(@content_util::USER_SHOULD_HAVE_PASSWORD)
        end
        return provider
      end

      #
      # Create or Update password depending upon if its first time (virgin)
      # or not.
      #
      # @param args [JSON] [Hash containing provider and password]
      # 
      # @return [object] [Updated provider object]
      #
      def create_password(args)
        provider = args[:provider]
        if provider.present?
          if provider.generic.present?
            generic_provider_service = @generic_provider_service.new
            provider.generic = generic_provider_service.reset_password(provider.generic, args[:password])
          else
            provider.generic = new_generic_provider(args[:password])
          end
        else
          raise @custom_error_util::PreConditionRequiredError.new(@content_util::CREATE_PASSWORD_UNAUTHENTICATED)
        end
        return provider
      end


      ###############################
      #       Private Functions     #
      ###############################

      private
        # 
        # functon to new_generic_provider of GenericProviderService class to create new generic provider
        # 
        # Paramaters::
        #   * password [string] contains the password of user
        # 
        # @return [object] returns generic object
        def new_generic_provider(password)
          generic_provider_service = @generic_provider_service.new
          generic =  generic_provider_service.new_generic_provider(password)
        end

        # 
        # functon to call new_generic_provider of MessageProviderService class to create new generic provider
        #  
        # @return [object] returns message object
        def new_message_provider
          message_provider_service = @message_provider_service.new
          message =  message_provider_service.new_message_provider
        end

        # 
        # functon to call create_update_message_provider of MessageProviderService class to create new/update generic provider
        #  
        # @return [object] returns message object
        def create_update_message_provider(message)
          message_provider_service = @message_provider_service.new
          message =  message_provider_service.create_update_message_provider(message)
        end

        # 
        # function to login with password
        # 
        # Raise Error::
        #   * ResourceNotFoundError if Sign in req is by a user which is not present in our system
        #   * RunTimeError if session token not found
        # 
        # 
        # @return [string] session token
        def login_with_password(password, provider)
          if provider.generic.present?
            return verify_password(password, provider.generic)
          else
            raise @custom_error_util::ResourceNotFoundError.new(@content_util::NOT_REGISTERED_WITH_PASSWORD)
          end

        end

        # 
        # function to login with otp
        # 
        # Raise Error::
        #   * ResourceNotFoundError if Sign in req is by a user which is not present in our system
        #   * RunTimeError if session token not found
        # 
        # @return [string] session token
        def login_with_otp(otp, provider)
          if provider.message.present?
            return verify_otp(otp, provider.message)
           else 
            raise @custom_error_util::ResourceNotFoundError.new(@content_util::REQUEST_OTP)
          end
        end

        # 
        # functon to call verify_otp of MessageProviderService class to match OTP
        #  
        # @return [boolean] true if OTP matches
        def verify_otp(otp, message)
          generic_message_service = @message_provider_service.new
          generic_message_service.verify_otp(otp, message)
        end

        # 
        # functon to call verify_password of GenericProviderService class to to match password
        #  
        # @return [boolean] if password matches
        def verify_password(password, generic)
          generic_provider_service = @generic_provider_service.new
          generic_provider_service.verify_password(password, generic)
        end

        # 
        # creates new provider object
        # 
        # @return [object] of provider type
        def create_provider_model
          @provider_model.new
        end

        # 
        # Fuctions returns true if provider type is Google/Fb else false
        # 
        # @return [boolean] 
        def is_social_request_type?
          if @request_type == @provider_type_util::FB || @request_type == @provider_type_util::GOOGLE
            return true
          else 
            return false
          end    
        end
    end
  end
end
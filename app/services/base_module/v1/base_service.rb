module BaseModule
  module V1
    class BaseService

      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params = '')
        @custom_error_util = CommonModule::V1::CustomErrors
      end
    end
  end
end
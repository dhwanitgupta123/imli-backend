module CommonModule
  module V1
    #
    # EmailInviteService class to store email ID's for invite
    #
    class EmailInvitesService < BaseModule::V1::BaseService
      #
      # Defining global variables with versioning
      #
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      EMAIL_INVITE_MODEL = CommonModule::V1::EmailInvite
      GENERAL_HELPER = CommonModule::V1::GeneralHelper
      EMAIL_INVITES_RESPONSE_DECORATOR = CommonModule::V1::EmailInvitesResponseDecorator
      USER_EMAIL_WORKER = CommunicationsModule::V1::Mailers::V1::UserEmailWorker
      USER_NOTIFICATION_SERVICE = UsersModule::V1::UserNotificationService
      EMAIL_TYPE = CommunicationsModule::V1::Mailers::V1::EmailType
      CACHE_UTIL = CommonModule::V1::Cache

      #
      # initialize EmailInviteService Class
      #
      def initialize(params)
        @params = params
      end

      #
      # API function to take email ids for Invites
      #
      def email_invites(email_invites_param)
        begin
          response = add_email_invites(email_invites_param)
          return EMAIL_INVITES_RESPONSE_DECORATOR.create_success_response
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          return EMAIL_INVITES_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        end
      end

      #
      # Service Function to store email ids for invite
      #
      def add_email_invites(email_invites_param)
        if email_invites_param.blank? || email_invites_param[:email_id].blank?
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT::FIELD_MUST_PRESENT%{field: 'Email ID'}) 
        end
        email_invites_param[:email_id] = email_invites_param[:email_id].delete(' ')
        validate_email_id(email_invites_param)
        EMAIL_INVITE_MODEL.save_email(email_invites_param)
      end

      #
      # Function to send the list of all email ids for invites
      # requested by user of last 24 Hrs
      #
      def send_last_x_hours_email_invites_request_mail(x = '')
        user_notification_service = USER_NOTIFICATION_SERVICE.new(@params)
        emails = EMAIL_INVITE_MODEL.get_all_last_x_hours_email_invites_request(x)
        if emails.present?
          emails_array = []
          emails.each do |email|
            emails_array.push({
              email_id: email.email_id
              })
          end
          user_notification_service.send_invite_mail(emails_array)
          mail_attributes = {
              email_type: EMAIL_TYPE::EMAIL_INVITES[:type],
              to_name: CACHE_UTIL.read('IMLI_TEAM_NAME'),
              to_email_id: CACHE_UTIL.read('IMLI_TEAM_MAIL_IDS').to_s,
              subject: CACHE_UTIL.read('EMAIL_INVITES'),
              multiple: true,
              pdf_needed: true,
              emails: emails_array
            }
            USER_EMAIL_WORKER.perform_async(mail_attributes)
        end
        return emails_array
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      # 
      # To validate class level email id
      # 
      # Parameters::
      #   * email_id [string] [Stores the email_id of User]
      #
      # Raise Error::
      #   * InvalidDataError if given email id is not valid
      def validate_email_id(email_invites_param)
        unless GENERAL_HELPER.email_id_valid?(email_invites_param[:email_id])
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT::INVALID_EMAIL_ID)
        end
      end
    end
  end
end

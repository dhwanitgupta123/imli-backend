module CommonModule
  module V1
    #
    # Plan Service class to implement CRUD for PLAN model
    #
    class PlanService
      #
      # Defining global variables with versioning
      #
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      PLAN_MODEL = CommonModule::V1::Plan
      PLAN_RESPONSE_DECORATOR = CommonModule::V1::PlanResponseDecorator
      BENEFIT_SERVICE = CommonModule::V1::BenefitService
      IMAGE_SERVICE = ImageServiceModule::V1::ImageService

      #
      # initialize PlanService Class
      #
      def initialize(params)
        @params = params
        @image_service = IMAGE_SERVICE.new
      end

      #
      # Create PLAN with given params
      #
      # @param args [Hash] Hash containing plan: {name, duration, validity_label, details, fee, benefitsArray}
      #
      # @return [Response] response to be sent to the user
      def create_plan(plan_params)
        begin
          response = create_plan_with_params(plan_params)
          return PLAN_RESPONSE_DECORATOR.create_response_plan_hash(response)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return PLAN_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidArgumentsError => e
          return PLAN_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return PLAN_RESPONSE_DECORATOR::create_not_found_error(e.message)
        end
      end


      #
      # Logic for creating PLAN with given params
      #
      # @param args [Hash] Hash containing plan: {name, duration, validity_label, details, fee, benefitsArray}
      #
      # @return [Response] response to be sent to the user
      #
      # @raise [InsufficientDataError] [if parameter passed were insufficient]
      # @raise [InvalidArgumentsError] [if inner keys hash was invalid]
      # @raise [ResourceNotFoundError] [if benefits array contain invalid entries]
      #
      def create_plan_with_params(plan_params)
        initialize_params(plan_params)
        validate_params
        # Verify all the benefits and fetch array of Benefit Models
        benefit_service = BENEFIT_SERVICE.new(@params)
        benefits = benefit_service.get_benefits_from_id_array(@benefits)
        # Create new Plan model
        plan = PLAN_MODEL.new_plan({
          name: @name,
          details: @details,
          duration: @duration,
          validity_label: @validity_label,
          plan_key: @plan_key,
          fee: @fee,
          images: @images
          })
        plan.benefits = benefits
        plan.save_plan
        @image_service.upload_async(plan.images) if plan.images.present?
        return plan
      end

      #
      # Get all PLANs
      # It returns all non-deleted PLANs
      #
      # @return [Response] response to be sent to the user containing all plans
      #
      def get_all_plans
        plans = PLAN_MODEL.get_all_non_deleted_plans
        return PLAN_RESPONSE_DECORATOR.create_response_plans_array(plans)
      end

      # 
      # Function to get all Active plans with active benefits only
      #
      def get_all_active_plans
        plans = PLAN_MODEL.get_all_active_plans
        return PLAN_RESPONSE_DECORATOR.create_response_plans_array(plans)
      end

      #
      # Get PLAN with given id
      #
      # @param args [Hash] Hash containing plan_id
      #
      # @return [Response] response to be sent to the user
      #
      def get_plan(plan_params)
        begin
          response = get_plan_with_params(plan_params)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return PLAN_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return PLAN_RESPONSE_DECORATOR.create_not_found_error(e.message)
        end
      end

      #
      # Logic for fetching PLAN with given id
      #
      # @param args [Hash] Hash containing plan_id
      #
      # @return [Response] response to be sent to the user
      #
      # @raise [InsufficientDataError] [if parameter passed were insufficient]
      # @raise [ResourceNotFoundError] [if no plan exists with given id]
      #
      def get_plan_with_params(plan_params)
        if (plan_params.blank? || plan_params[:id].blank? )
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Plan ID'})
        end
        plan  = PLAN_MODEL.find_by_plan_id(plan_params[:id])
        return PLAN_RESPONSE_DECORATOR.create_response_plan_hash(plan)
      end

      #
      # Update PLAN with given params
      #
      # @param args [Hash] Hash containing plan: {name, duration, validity_label, details, fee, benefitsArray}
      #
      # @return [Response] response to be sent to the user
      #
      def update_plan(plan_params)
        begin
          response = update_plan_with_params(plan_params)
          return PLAN_RESPONSE_DECORATOR.create_response_plan_hash(response)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return PLAN_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidArgumentsError => e
          return PLAN_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return PLAN_RESPONSE_DECORATOR.create_not_found_error(e.message)
        end
      end

      #
      # Logic for updating PLAN with given params
      #
      # @param args [Hash] Hash containing plan: {name, duration, validity_label, details, fee, benefitsArray}
      #
      # @return [Response] response to be sent to the user
      #
      # @raise [InsufficientDataError] [if parameter passed were insufficient]
      # @raise [InvalidArgumentsError] [if inner keys hash was invalid]
      # @raise [ResourceNotFoundError] [if benefits array contain invalid entries]
      #
      def update_plan_with_params(plan_params)
        if (plan_params.blank? || plan_params[:id].blank? )
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Plan ID'})
        end
        initialize_params(plan_params)
        plan = PLAN_MODEL.find_by_plan_id(plan_params[:id])
        if @benefits.present?
          # Verify all the benefits and fetch array of Benefit Models
          benefit_service = BENEFIT_SERVICE.new(@params)
          benefits = benefit_service.get_benefits_from_id_array(@benefits)
          # Merge benefits array with other args
          plan_params[:plan] = plan_params[:plan].merge({benefits: benefits})
        end
        plan.update_plan(plan_params[:plan])
        @image_service.upload_async(plan.images) if plan.images.present?
        return plan
      end

      #
      # Change state of PLAN based on passed event
      #
      # @param args [Hash] Hash containing plan: {event} and plan_id
      #
      # @return [Response] response to be sent to the user
      #
      # @raise [InsufficientDataError] [if parameter passed were insufficient]
      # @raise [InvalidArgumentsError] [if inner keys hash was invalid]
      # @raise [ResourceNotFoundError] [if benefits array contain invalid entries]
      #
      def change_state(plan_params)
        begin
          response = change_plan_state(plan_params)
          return response
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return PLAN_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return PLAN_RESPONSE_DECORATOR.create_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidArgumentsError => e
          return PLAN_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          return PLAN_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        end
      end

      #
      # Logic for changing state of PLAN based on passed event
      #
      # @param args [Hash] Hash containing plan: {event} and plan_id
      #
      # @return [Response] response to be sent to the user
      #
      # @raise [InsufficientDataError] [if parameter passed were insufficient]
      # @raise [InvalidArgumentsError] [if inner keys hash was invalid]
      # @raise [ResourceNotFoundError] [if no plan was found w.r.t given id]
      # @raise [PreConditionRequiredError] [if event passed was invalid corresponding to current state of plan]
      #
      def change_plan_state(plan_params)
        if (plan_params.blank? || plan_params[:id].blank? )
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Plan ID'})
        end
        if (plan_params[:plan].blank? || plan_params[:plan][:event].blank?)
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Plan Event'})
        end
        plan = PLAN_MODEL.find_by_plan_id(plan_params[:id])
        plan.trigger_event(plan_params[:plan][:event])
        return PLAN_RESPONSE_DECORATOR.create_response_plan_hash(plan)
      end

      #
      # Logic for fetching PLAN with given id
      #
      # @param args [Hash] Hash containing plan_id
      #
      # @return [Plan] Plan model
      #
      # @raise [InsufficientDataError] [if parameter passed were insufficient]
      # @raise [ResourceNotFoundError] [if no plan exists with given id]
      #
      def get_plan_by_id(plan_id)
        if plan_id.blank?
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Plan ID'})
        end
        plan  = PLAN_MODEL.find_by_plan_id(plan_id)
      end

      #
      # Logic for fetching PLAN with given key
      #
      # @param args [string] containing plan_key
      #
      # @return [Plan] Plan model
      #
      # @raise [InsufficientDataError] [if parameter passed were insufficient]
      # @raise [ResourceNotFoundError] [if no plan exists with given id]
      #
      def get_plan_by_key(plan_key)
        if plan_key.blank?
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Plan Key'})
        end
        plan  = PLAN_MODEL.find_by_plan_key(plan_key)
        return plan
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # [Initialize parameters]
      # @param role_params [Hash] [Hash containing plan attributes]
      #
      # @raise [InsufficientDataError] [if Hash passed is blank]
      #
      def initialize_params(plan_params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::PLAN_HASH_MUST_PRESENT) if (plan_params.blank? || plan_params[:plan].blank? )
        @name = plan_params[:plan][:name] if plan_params[:plan][:name].present?
        @details = plan_params[:plan][:details] if plan_params[:plan][:details].present?
        @duration = plan_params[:plan][:duration] if plan_params[:plan][:duration].present?
        @validity_label = plan_params[:plan][:validity_label] if plan_params[:plan][:validity_label].present?
        @fee = plan_params[:plan][:fee] if plan_params[:plan][:fee].present?
        @benefits = plan_params[:plan][:benefits] if plan_params[:plan][:benefits].present?
        @plan_key = plan_params[:plan][:plan_key] if plan_params[:plan][:plan_key].present?
        @images = plan_params[:plan][:images] if plan_params[:plan][:images].present?
      end

      #
      # Validate PLAN parameters.
      #
      # @raise [InsufficientDataError] [if any of the hash fields are blank]
      def validate_params
        error_array = []
        error_array.push('Plan Name') if @name.blank?
        error_array.push('Plan Details') if @details.blank?
        error_array.push('Plan Duration') if @duration.blank?
        error_array.push('Plan Validity Label') if @validity_label.blank?
        error_array.push('Plan Fee') if @fee.blank?
        error_array.push('Plan Benefits (Array)') if @benefits.blank?
        error_array.push('Plan Key') if @plan_key.blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end

    end
  end
end
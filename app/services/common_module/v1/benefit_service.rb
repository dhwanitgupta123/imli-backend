module CommonModule
  module V1
    #
    # Plan Service class for BENEFIT model
    #
    class BenefitService
      #
      # Defining global variables with versioning
      #
      BENEFIT_MODEL_STATES = CommonModule::V1::ModelStates::V1::BenefitStates
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      BENEFIT_MODEL = CommonModule::V1::Benefit
      BENEFIT_RESPONSE_DECORATOR = CommonModule::V1::BenefitResponseDecorator
      BENEFIT_CONDITIONS = CommonModule::V1::BenefitConditions
      BENEFIT_HELPER = CommonModule::V1::BenefitHelper
      TRIGGER_ENUM_UTIL = TriggersModule::V1::TriggerEnum
      BENEFIT_ENUM_UTIL = CommonModule::V1::BenefitEnum
      TO_USER = 1
      TO_REFERRER = 2

      #
      # initialize PlanService Class
      #
      def initialize(params)
        @params = params
      end

      #
      # Get benefits from Array of benefit id
      #
      # @param benefits_array [Array] [Array of benefits IDs]
      # 
      # @return [Array] [Array of Benefit models corresponding to given IDs]
      # 
      # @raise [InsufficientDataError] [If parameter ID array is not present]
      # @raise [InvalidArgumentsError] [If passed array of ID contain some benefit IDs which are not ACTIVE]
      # @raise [ResourceNotFoundError] [If no benefit was present corresponding to passed ID]
      #
      def get_benefits_from_id_array(benefits_array)
        if benefits_array.blank?
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::BENEFITS_ARRAY_MUST_PRESENT)
        end
        benefits = []
        benefits_array.each do |benefit_id|
          benefit = BENEFIT_MODEL.find_by_benefit_id(benefit_id.to_i)
          if benefit.present? && benefit.active?
            benefits.push(benefit)
          else
            raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(
              CONTENT_UTIL::BENEFIT_MUST_BE_ACTIVE % {id: benefit.id.to_s, name: benefit.name.to_s}
            )
          end
        end
        return benefits
      end

      #
      # Get all active benefits
      #
      def get_all_active_benefits
        active_benefits = BENEFIT_MODEL.get_benefits_by_status(BENEFIT_MODEL_STATES::ACTIVE)
        return BENEFIT_RESPONSE_DECORATOR.create_response_benefits_array(active_benefits)
      end

      # 
      # Function to get the benefits related to address in Membership
      #
      def get_address_benefit(benefits)
        raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::BENEFITS_NOT_FOUND) if benefits.blank?
        address_benefit = nil
        benefits.each do |benefit|
          if benefit.condition.eql? BENEFIT_CONDITIONS::MAX_ADDRESS && benefit.active?
            address_benefit = benefit
            break
          end
        end
        return address_benefit
      end

      # 
      # Adding initial benefits for user
      #
      # @param user [Model] user
      #
      def add_initial_benefits_to_user(user)
        benefit = get_ample_credit_benefit
        user.benefits.push(benefit)
        user.save
      end

      # 
      # create ample credit benefit
      # 
      # @return [Model] AmpleCreditBenefit
      #
      def get_ample_credit_benefit
        ample_credit_benefit_args = BENEFIT_HELPER.get_ample_credit_benefit_args
        BENEFIT_MODEL.new(ample_credit_benefit_args)
      end

      # 
      # applying benefits to user
      #
      def apply_benefits_to_user(args)

        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return apply_benefits(args.merge(to: TO_USER))
        end

        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      # 
      # apply benefit to referrer
      #
      def apply_benefits_to_referrer(args)

        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return apply_benefits(args.merge(to: TO_REFERRER))
        end

        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      # 
      # This function takes user and trigger of the benefit
      # then it get all the benefits of user that has to be applied on that
      # trigger and apply accordingly
      #
      # @param args [Hash] has user and trigger as key
      #
      def apply_benefits(args)

        user = args[:user]
        trigger = args[:trigger]

        user_benefits = get_benefits_by_trigger(trigger, user, args[:to]) 

        return if user_benefits.blank?

        user_benefits.each do |benefit|

          benefit_class = get_benefit_instance(benefit, args[:to])

          benefit_class.apply({ user: user, benefit: benefit })
        end

        return true
      end

      # 
      # This function return the benefits by trigger
      #
      # @param trigger [String] trigger point of benefit
      # @param user [Model] user model
      # 
      # @return [Array] array of benefits
      #
      def get_benefits_by_trigger(trigger, user, to)
        benefits = user.benefits

        return benefits.where(trigger_point_of_user: trigger) if benefits.present? && to == TO_USER
        return benefits.where(trigger_point_of_referrer: trigger) if benefits.present? && to == TO_REFERRER
        return []
      end

      # 
      # This function returns the instance of benefit class
      #
      # @param benefit [Model] benefit model
      # @param to [Integer] to specify benifit is of user or referrer
      # 
      # @return [ClassInstance] instance of corresponding benefit class 
      #
      def get_benefit_instance(benefit, to)
        
        if to == TO_USER
          benefit_class = BENEFIT_ENUM_UTIL.get_benefit_class_by_benefit_id(benefit.benefit_to_user)
        else
          benefit_class = BENEFIT_ENUM_UTIL.get_benefit_class_by_benefit_id(benefit.benefit_to_referrer)
        end

        return benefit_class.new(@params)
      end
    end
  end
end

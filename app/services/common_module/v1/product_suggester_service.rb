module CommonModule
  module V1
    #
    # Product Suggester Service class to implement CRUD for PRODUCT SUGGESTER model
    #
    class ProductSuggesterService < BaseModule::V1::BaseService
      #
      # Defining global variables with versioning
      #
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      PRODUCT_SUGGESTER_MODEL = CommonModule::V1::ProductSuggester
      USER_SERVICE = UsersModule::V1::UserService
      PRODUCT_SUGGESTER_RESPONSE_DECORATOR = CommonModule::V1::ProductSuggesterResponseDecorator
      USER_EMAIL_WORKER = CommunicationsModule::V1::Mailers::V1::UserEmailWorker
      EMAIL_TYPE = CommunicationsModule::V1::Mailers::V1::EmailType
      CACHE_UTIL = CommonModule::V1::Cache

      #
      # initialize ProductSuggesterService Class
      #
      def initialize(params)
        @params = params
      end

      # 
      # update_product_suggestions function is API function
      # @param product_suggestion [Hash]
      # - contains [String] product_name  
      # 
      # @return [response]
      # 
      def update_product_suggestions(product_suggestion)
        begin
          response = update_suggestions_by_user(product_suggestion)
          return PRODUCT_SUGGESTER_RESPONSE_DECORATOR.create_product_suggestion_ok_response if response == true
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return PRODUCT_SUGGESTER_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          PRODUCT_SUGGESTER_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::UnAuthenticatedUserError => e
          PRODUCT_SUGGESTER_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        end
      end

      # 
      # [update_suggestions_by_user is logic function for update_product_suggestions]
      # @param product_suggestion [Hash]
      # - contains [String] product_name 
      # 
      # @return [Boolean] true if updated
      # 
      def update_suggestions_by_user(product_suggestion)
        validate_product_suggestion_params(product_suggestion)
        validate_user

        PRODUCT_SUGGESTER_MODEL.update_product_suggestion_for_user({
          user_id: @user.id,
          product_name: product_suggestion
          })
        return true
      end

      #
      # function to get all product suggestion details
      #
      # Response::
      #   * sends response with all the product suggestion details
      #
      # @return [response]
      #
      def get_all_product_suggestions(paginate_params)
        begin
          product_suggestions = get_all_suggestions(paginate_params)
          return PRODUCT_SUGGESTER_RESPONSE_DECORATOR.create_all_suggestions_response(product_suggestions)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          PRODUCT_SUGGESTER_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        end     
      end

      #
      # function to get all suggestions details
      #
      # Response::
      #   * sends response with all the suggestions details
      #
      # @return [response]
      #
      def get_all_suggestions(paginate_params)
        validate_user
  
        if paginate_params[:order].present? && paginate_params[:order] != 'ASC' && paginate_params[:order] != 'DESC'
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT::WRONG_ORDER)
        end
        if paginate_params[:sort_by].present? && !PRODUCT_SUGGESTER_MODEL.attribute_names.include?(paginate_params[:sort_by])
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT::INVALID_PARAMETER%{ field: paginate_params[:sort_by] })
        end
        all_suggestions = PRODUCT_SUGGESTER_MODEL.paginated_product_suggestions(paginate_params)
        return all_suggestions
      end

      #
      # Function to send email of all the product suggested by user in last 24 hrs
      #
      def send_last_x_hours_product_suggestion_mail(x = '')
        products = PRODUCT_SUGGESTER_MODEL.get_all_last_x_hours_suggestions(x)
        if products.present?
          products_array = []
          products.each do |suggestions|
            suggestions[:product_suggestions].each do |product|
              products_array.push({
                product_name: product['product_name']
                })
            end
          end
          mail_attributes = {
              email_type: EMAIL_TYPE::PRODUCT_SUGGESTION[:type],
              to_name: CACHE_UTIL.read('IMLI_TEAM_NAME'),
              to_email_id: CACHE_UTIL.read('IMLI_TEAM_MAIL_IDS').to_s,
              subject: CACHE_UTIL.read('PRODUCT_SUGGESTION'),
              multiple: true,
              pdf_needed: true,
              products: products_array
            }
            USER_EMAIL_WORKER.perform_async(mail_attributes)
        end
        return products_array
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      def validate_user
        user_service = USER_SERVICE.new(@params)
        @user = user_service.get_user_from_user_id(USER_SESSION[:user_id])     
      end

      #
      # Validate product suggestion parameters.
      #
      # @raise [InsufficientDataError] [if any of the hash fields are blank]
      def validate_product_suggestion_params(product_suggestion)
        if product_suggestion.blank? or product_suggestion[:product_name].blank?
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT::INSUFFICIENT_DATA)
        end
      end
    end
  end
end
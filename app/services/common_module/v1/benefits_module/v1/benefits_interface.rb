module CommonModule
  module V1
    module BenefitsModule
      module V1
        class BenefitsInterface

          def initialize(params = '')
          end

          def apply(params)
            raise 'Method not implemented'
          end

          # 
          # Helper method used by implementing classes
          #
          # @param user [Model] user model
          # @param benefit [Model] 
          # 
          # @return [Model] benefit model
          #
          def get_referrer_benefit(user, benefit)
            referrer = user.referrer
            benefits = referrer.benefits.where(benefit_type: benefit.benefit_type) if referrer.present?
            return benefits.first if benefits.present?
            return nil
          end
        end
      end
    end
  end
end

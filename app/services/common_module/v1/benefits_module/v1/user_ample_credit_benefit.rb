module CommonModule
  module V1
    module BenefitsModule
      module V1
        class UserAmpleCreditBenefit < CommonModule::V1::BenefitsModule::V1::BenefitsInterface

          CACHE = CommonModule::V1::Cache

          def initialize(params = '')
            @params = params
            @ample_credit_service = CreditsModule::V1::AmpleCreditService
          end

          # 
          # This function applies AmpleCreditBenefit the benefit to user
          #
          # @param params [Hash] containing user and benefit
          #
          def apply(params)

            user = params[:user]
            benefit = params[:benefit]

            return false unless is_valid?(user)

            amount = CACHE.read_int('AMPLE_CREDIT_BENEFIT_AMOUNT') || 200
            
            ample_credit_service = @ample_credit_service.new(@params)

            ample_credit_service.credit({
                ample_credit: user.ample_credit,
                amount: amount
              })

            update_referrer_benefit(user,benefit)

          end
          
          private

          # 
          # This function updates the refer_count of user
          #
          def update_referrer_benefit(user,benefit)
 
            referrer_benefit = get_referrer_benefit(user, benefit)

            referrer_benefit.refer_count += 1
            referrer_benefit.save
          end

          # 
          # checks user is valid or not
          #
          # @param user [Model] user
          # 
          # @return [Boolean] true if user.referrer_by is present else false
          #
          def is_valid?(user)

            return false  if user.referred_by.blank?

            return true
          end
        end
      end
    end
  end
end

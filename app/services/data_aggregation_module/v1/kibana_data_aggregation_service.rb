#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # This class is responsible to aggregate data to index that are used by kibana
    #
    class KibanaDataAggregationService < DataAggregationModule::V1::DataAggregationInterface

      CACHE_UTIL = CommonModule::V1::Cache
      AGGREGATED_WORKER_INTERVAL = CACHE_UTIL.read_int('AGGREGATOR_WORKER_INTERVAL') || 2

      def initialize(params = '')
        @params = params
      end

      # 
      # This function call aggregator worker asynchronously
      # which fetch required data and update index
      #
      def self.aggregate

        workers = get_workers
        aggregator_worker_interval_minues = AGGREGATED_WORKER_INTERVAL.minutes.to_i
        count = 0
        workers.each do |worker|
          worker.perform_in(aggregator_worker_interval_minues*count)
          count = count + 1
        end
      end

      def self.get_workers
        return [
          DataAggregationModule::V1::ProductAggregatorWorker,
          DataAggregationModule::V1::OrderAggregatorWorker,
          DataAggregationModule::V1::PaymentAggregatorWorker,
          DataAggregationModule::V1::UserAggregatorWorker,
          DataAggregationModule::V1::MasterProductAggregatorWorker,
          DataAggregationModule::V1::ApiLogAggregatorWorker
        ]
      end
    end
  end
end

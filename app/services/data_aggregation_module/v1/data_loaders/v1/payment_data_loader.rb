#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # Module responsible for loading data
    #
    module DataLoaders
      #
      # Version1 for data loaders 
      #
      module V1
        #
        # This class loads all payment data and return in payment_search_model 
        #
        class PaymentDataLoader


          def initialize
            @order_model = MarketplaceOrdersModule::V1::Order
            @order_product_service_instance = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService.new
            @order_state = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
            @cart_model = MarketplaceOrdersModule::V1::Cart
            @order_helper = MarketplaceOrdersModule::V1::OrderHelper
            @payment_search_model = DataAggregationModule::V1::PaymentSearchModel
            @payment_dao = MarketplaceOrdersModule::V1::PaymentDao.new
            @payment_helper = MarketplaceOrdersModule::V1::PaymentHelper
            @order_dao = MarketplaceOrdersModule::V1::OrderDao
          end

          # 
          # This will load payment data, if all = true then load all the data
          # else load data which has updated_at > x_days
          #
          # @param x_days [Integer] this specify we need data after (today - x_days) days
          # @param all = false [Boolean] if all is true then return all the data
          # 
          # @return [Array] Array of payment_search_module
          #
          def load_data(x_days, all = false)

            payment_search_models = []

            if all == false
              today_date = DateTime.now
              start_of_day_time = today_date.beginning_of_day
              from_time = start_of_day_time - x_days.day

              orders = get_updated_orders(from_time)
            else
              orders = @order_dao.get_completed_orders
            end

            orders = @order_dao.get_order_and_user_data(orders)

            orders.each do |order|

              cart = order.cart
              user = order.user
              order_date = @order_helper.get_order_date(order)
              order_date = Time.at(order_date.to_i)

              payment = @payment_dao.get_orders_payment_as_per_its_mode(order)
              payment_display_labels = @payment_helper.get_payment_display_labels(payment)

              order_area = order.address.area
              row = [order.id, order.order_id, order_date, user.id, user.phone_number, user.first_name, order_area.pincode, cart.total_mrp, 
                           cart.cart_savings, cart.cart_total, payment.delivery_charges, payment.billing_amount, payment.grand_total,
                           payment.discount, payment.net_total, payment.total_savings, payment_display_labels[:mode_display_name],
                           payment_display_labels[:status_display_name] ]

              payment_search_models.push(get_payment_search_model(row))

            end

            return payment_search_models
          end

          ##################################
          ############Private###############
          ##################################

          # 
          # This function returns all the orders which are updated or
          # there carts are updated
          #
          # @param from_time [Time] from time
          # 
          # @return orders [Array] array of order
          #
          def get_updated_orders(from_time)
            return @order_dao.get_completed_orders(@order_model.where('updated_at > ?', from_time))
          end

          #
          # map order to order model object
          # 
          # @param row [Hash] containing order related information
          # 
          # @return [Object] payment_search_model object
          # 
          def get_payment_search_model(row)
            @payment_search_model.new({
               id: row[0].to_i,
               order_id: row[1].to_s,
               order_time: DateTime.parse(row[2].to_s),
               user_id: row[3].to_i,
               phone_number: row[4].to_i,
               name: row[5].to_s,
               pincode: row[6].to_i,
               cart_mrp: row[7].to_i,
               cart_savings: row[8].to_i,
               cart_total: row[9].to_i,
               payment_delivery_charge: row[10].to_i,
               payment_billing_amount: row[11].to_i,
               payment_grant_total: row[12].to_i,
               payment_discount: row[13].to_i,
               payment_net_total: row[14].to_i,
               payment_total_savings: row[15].to_i,
               payment_mode: row[16].to_s,
               payment_status: row[17].to_s
              }) 
          end
        end
      end
    end
  end
end

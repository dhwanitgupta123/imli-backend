#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # Module responsible for loading data
    #
    module DataLoaders
      #
      # Version1 for data loaders 
      #
      module V1
        #
        # This class loads all order data and return in order_search_model 
        #
        class OrderDataLoader

          PLATFORM_TYPE_UTIL = CommonModule::V1::PlatformType

          def initialize
            @order_model = MarketplaceOrdersModule::V1::Order
            @order_product_service_instance = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService.new
            @order_state = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
            @cart_model = MarketplaceOrdersModule::V1::Cart
            @order_helper = MarketplaceOrdersModule::V1::OrderHelper
            @order_search_model = DataAggregationModule::V1::OrderSearchModel
            @order_products_dao = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsDao
            @order_context_dao = MarketplaceOrdersModule::V1::OrderContextDao.new
            @order_dao = MarketplaceOrdersModule::V1::OrderDao
          end

          # 
          # This will load order data, if all = true then load all the data
          # else load data which has updated_at > x_days
          #
          # @param x_days [Integer] this specify we need data after (today - x_days) days
          # @param all = false [Boolean] if all is true then return all the data
          # 
          # @return [Array] Array of order_search_module
          #
          def load_data(x_days, all = false)

            order_search_models = []

            if all == false
              today_date = DateTime.now
              start_of_day_time = today_date.beginning_of_day
              from_time = start_of_day_time - x_days.day

              orders = get_updated_orders(from_time)
            else
              orders = @order_model.all
            end

            orders = orders.eager_load(:user, :carts)

            return order_search_models if orders.blank?

            orders.each do |order|

              cart = order.cart
              user = order.user
              user_completed_order_count = @order_dao.get_completed_orders(user.orders).count
              order_date = @order_helper.get_order_date(order)
              order_date = Time.at(order_date.to_i)
              order_products = @order_product_service_instance.get_active_order_products_in_carts(cart)

              order_products = @order_products_dao.get_aggregated_order_products_and_mpsps_details(order_products)

              order_area = order.address.area
              begin
                order_context = @order_context_dao.get_context_by_order_id(order.id)
              rescue => e
                order_context = nil
              end

              platform_type = PLATFORM_TYPE_UTIL.get_display_platform_type_by_id(1)
              app_version = ''
              order_user_tag = ''

              if order_context.present?
                platform_type = PLATFORM_TYPE_UTIL.get_display_platform_type_by_id(order_context.platform_type)
                app_version  = order_context.app_version
                order_user_tag = order_context.order_user_tag
                order_user_tag = MarketplaceOrdersModule::V1::OrderUserTags.get_order_user_tag_label_by_id(order_user_tag) if order_user_tag.present?
              end

              order_products.each do |order_product|

                mpsp = order_product.marketplace_selling_pack
                mpsp_sub_category = mpsp.mpsp_sub_category
                mpsp_category = mpsp_sub_category.mpsp_parent_category
                mpsp_department = mpsp_category.mpsp_department
                mpbps = mpsp.marketplace_brand_packs
                mpsp_mpbps = mpsp.marketplace_selling_pack_marketplace_brand_packs

                bp_codes = ''
                company = ''
                brand = ''
                sub_brand = ''
                sku = ''
                mpbps.each do |mpbp|
                  brand_pack = mpbp.brand_pack
                  sku += brand_pack.sku + '#' if brand_pack.sku.present?
                  sub_brand += brand_pack.product.sub_brand.name + '#' if brand_pack.product.sub_brand.name.present?
                  brand += brand_pack.product.sub_brand.brand.name + '#' if brand_pack.product.sub_brand.brand.name.present?
                  company += brand_pack.product.sub_brand.brand.company.name + '#' if brand_pack.product.sub_brand.brand.company.name.present?

                  bp_codes += brand_pack.brand_pack_code.to_s + '#'

                end
                bp_codes = bp_codes[0..-2] if bp_codes.present?
                sku = sku[0..-2] if sku.present?
                sub_brand = sub_brand[0..-2] if sub_brand.present?
                brand = brand[0..-2] if brand.present?
                company = company[0..-2] if company.present?

                bp_quantity = ''
                mpsp_mpbps.each do |mpsp_mpbp|
                  bp_quantity = bp_quantity + mpsp_mpbp.quantity.to_s + '#'
                end
                bp_quantity = bp_quantity[0..-2]

                assortment = false

                assortment = true if mpbps.length > 1

                row = [ order.order_id, order_date, user.id, user.phone_number, user.first_name, cart.id, order_product.id, order_product.mrp, 
                       order_product.selling_price, order_product.savings, order_product.vat, order_product.service_tax, 
                       order_product.taxes, order_product.discount, order_product.quantity, mpsp.display_name, mpsp.display_pack_size, 
                       mpsp.id, mpsp_department.label, mpsp_category.label, mpsp_sub_category.label, bp_codes, bp_quantity,
                       order.status, order.updated_at, cart.cart_total, order_product.quantity, order_product.mrp, sku, sub_brand, brand, company,
                       assortment, user_completed_order_count, order_area.pincode, platform_type, app_version, order_user_tag]

                order_search_models.push(get_order_search_model(row))
                
              end
            end

            return order_search_models
          end

          ##################################
          ############Private###############
          ##################################

          # 
          # This function returns all the orders which are updated or
          # there carts are updated
          #
          # @param from_time [Time] from time
          # 
          # @return orders [Array] array of order
          #
          def get_updated_orders(from_time)
            return @order_model.where('updated_at > ?', from_time)
          end

          #
          # map order to order model object
          # 
          # @param row [Hash] containing order related information
          # 
          # @return [Object] order_search_model object
          # 
          def get_order_search_model(row)
            @order_search_model.new({
               order_id: row[0].to_s,
               order_time: DateTime.parse(row[1].to_s),
               user_id: row[2].to_i,
               phone_number: row[3].to_i,
               name: row[4].to_s,
               cart_id: row[5].to_i,
               id: row[6].to_i, # Id used to identify single index
               mrp: row[7].to_i,
               selling_price: row[8].to_i,
               savings: row[9].to_i,
               vat: row[10].to_i,
               service_tax: row[11].to_i,
               taxes: row[12].to_i,
               discount: row[13].to_i,
               quantity: row[14].to_i,
               display_name: row[15].to_s,
               display_pack_size: row[16].to_s,
               mpsp_id: row[17].to_i,
               department: row[18],
               category: row[19],
               sub_category: row[20],
               brand_codes: row[21].to_s,
               brand_pack_quantity: row[22].to_i,
               order_status: row[23].to_i,
               last_update_time: DateTime.parse(row[24].to_s),
               cart_total: row[25].to_i,
               order_product_quantity: row[26].to_i,
               order_product_mrp: row[27].to_i,
               sku: row[28].to_s,
               sub_brand: row[29].to_s,
               brand: row[30].to_s,
               company: row[31].to_s,
               assortment: row[32],
               user_completed_order_count: row[33].to_i,
               pincode: row[34].to_i,
               platform_type: row[35].to_s,
               app_version: row[36].to_s,
               order_user_tag: row[37].to_s
              }) 
          end
        end
      end
    end
  end
end

#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
     #
    # Module responsible for loading data
    #
    module DataLoaders
      #
      # Version1 for data loaders 
      #
      module V1
        #
        # This class loads all product data and return in product_search_model 
        #
        class ProductDataLoader

          def initialize
            @marketplace_selling_pack_model = MarketplaceProductModule::V1::MarketplaceSellingPack
            @product_search_model = DataAggregationModule::V1::ProductSearchModel
            @marketplace_selling_pack_dao = MarketplaceProductModule::V1::MarketplaceSellingPackDao
          end

          # 
          # This will load product data, if all = true then load all the data
          # else load data which has updated_at > x_days
          #
          # @param x_days [Integer] this specify we need data after (today - x_days) days
          # @param all = false [Boolean] if all is true then return all the data
          # 
          # @return [Array] Array of product_search_module
          #
          def load_data(x_days, all = false)

            if all == false
              today_date = DateTime.now
              start_of_day_time = today_date.beginning_of_day
              from_time = start_of_day_time - x_days.day

              mpsps = @marketplace_selling_pack_model.where('updated_at > ?', from_time)
            else
              mpsps = @marketplace_selling_pack_model.all
            end

            mpsps = @marketplace_selling_pack_dao.get_aggregated_mpsps_and_master_product_details(mpsps)

            product_search_models = []

            mpsps.each do |mpsp|

              pricing = mpsp.marketplace_selling_pack_pricing

              mpsp_sub_category = mpsp.mpsp_sub_category

              mpsp_category = mpsp_sub_category.mpsp_parent_category

              mpsp_department = mpsp_category.mpsp_department

              mpbps = mpsp.marketplace_brand_packs

              is_assortment = false

              is_assortment = true if mpbps.length > 1

              bp_codes = ''
              company = ''
              brand = ''
              sub_brand = ''
              sku = ''
              sku_size_and_units = ''
              article_codes = ''
              brand_pack_mrp = ''
              imli_pack_size = ''

              mpbps.each do |mpbp|
                bp_codes += mpbp.brand_pack.brand_pack_code.to_s + '#'
                brand_pack = mpbp.brand_pack
                sku += brand_pack.sku + '#' if brand_pack.sku.present?
                sub_brand +=  brand_pack.product.sub_brand.name + '#' if brand_pack.product.sub_brand.name.present?
                brand += brand_pack.product.sub_brand.brand.name + '#' if brand_pack.product.sub_brand.brand.name.present?
                company += brand_pack.product.sub_brand.brand.company.name + '#' if brand_pack.product.sub_brand.brand.company.name.present?
                sku_size_and_units += brand_pack.sku_size + ' ' + brand_pack.unit + '#' if brand_pack.sku_size.present? && brand_pack.unit.present?
                article_codes += brand_pack.article_code + '#' if brand_pack.article_code.present?
                brand_pack_mrp += brand_pack.mrp.to_s + '#' if brand_pack.mrp.present?
                imli_pack_size += mpbp.marketplace_selling_pack_marketplace_brand_packs[0].quantity.to_s + '#' if mpbp.marketplace_selling_pack_marketplace_brand_packs.present?
              end

              bp_codes = bp_codes[0..-2] if bp_codes.present?
              sku = sku[0..-2] if sku.present?
              sub_brand = sub_brand[0..-2] if sub_brand.present?
              brand = brand[0..-2] if brand.present?
              company = company[0..-2] if company.present?
              sku_size_and_units = sku_size_and_units[0..-2] if sku_size_and_units.present?
              article_codes = article_codes[0..-2] if article_codes.present?
              brand_pack_mrp = brand_pack_mrp[0..-2] if brand_pack_mrp.present?
              imli_pack_size = imli_pack_size[0..-2] if imli_pack_size.present?

              row = [mpsp.id, bp_codes, mpsp_department.label, mpsp_category.label, mpsp_sub_category.label,
                     mpsp.display_name, mpsp.display_pack_size, mpsp.primary_tags, mpsp.secondary_tags, pricing.mrp,
                     pricing.vat, pricing.base_selling_price, pricing.savings, mpsp.status, brand, sub_brand, sku, company,
                     is_assortment, sku_size_and_units, article_codes, brand_pack_mrp, imli_pack_size, mpsp.updated_at]

              product_search_models.push(get_product_search_model(row))
            end
            return product_search_models
          end

          ##################################
          ############Private###############
          ##################################

          #
          # map row to product model object
          # 
          # @param row [Hash] containing product related information
          # 
          # @return [ProductModel] product_search_model object
          # 
          def get_product_search_model(row)
            @product_search_model.new({
               id: row[0].to_i,
               brand_pack_code: row[1].to_s,
               department: row[2].to_s,
               category: row[3].to_s,
               sub_category: row[4].to_s,
               display_name: row[5].to_s,
               display_pack_size: row[6].to_s,
               primary_tags: row[7].to_s,
               secondary_tags: row[8].to_s,
               mrp: row[9].to_i,
               vat: row[10].to_i,
               base_selling_price: row[11].to_i,
               savings: row[12].to_i,
               status: row[13].to_i,
               brand: row[14].to_s,
               sub_brand: row[15].to_s,
               sku: row[16].to_s,
               company: row[17].to_s,
               is_assortment: row[18],
               sku_size_and_units: row[19].to_s,
               article_codes: row[20].to_s,
               brand_pack_mrp: row[21].to_s,
               imli_pack_size: row[22].to_i,
               updated_at: row[23]
              }) 
          end
        end
      end
    end
  end
end

#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # This class is responsible to aggregate data
    #
    class ProductIndexService < DataAggregationModule::V1::IndexInterface

      PRODUCT_DATA_LOADER = DataAggregationModule::V1::DataLoaders::V1::ProductDataLoader

      def initialize(params = '')
        super
        @params = params
        @repository = PRODUCT_REPOSITORY
        @product_data_loader = PRODUCT_DATA_LOADER.new
      end

      # 
      # This function update order-index with loaded data, it index
      # all data if order-index has 0 document else update the
      # order-repositroy
      #
      def update_index
         if index_exists?
          product_search_models = @product_data_loader.load_data(1)
          index_data(product_search_models)
          ApplicationLogger.debug('Successfully updated product-index with ' + product_search_models.length.to_s + ' models')
        else
          re_initialize_index
        end
      end

      # 
      # This function delete order-index and re-create
      # with all the order data
      #
      def re_initialize_index
        delete_previous_index
        product_search_models = @product_data_loader.load_data(0, true)
        index_data(product_search_models)
        ApplicationLogger.debug('Successfully re_initialize product-index with ' + product_search_models.length.to_s + ' models')
      end

    end
  end
end

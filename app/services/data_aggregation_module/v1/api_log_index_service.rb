#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # This class is responsible to aggregate data
    #
    class ApiLogIndexService < DataAggregationModule::V1::IndexInterface

      API_LOG_DATA_LOADER = DataAggregationModule::V1::DataLoaders::V1::ApiLogDataLoader

      def initialize(params = '')
        super
        @params = params
        @repository = API_LOG_REPOSITORY
        @api_log_data_loader = API_LOG_DATA_LOADER.new
      end

      # 
      # This function update api_log-index with loaded data, it index
      # all data if api_log-index has 0 document else update the
      # api_log-repositroy
      #
      def update_index
         if index_exists?
          api_log_search_models = @api_log_data_loader.load_data(1)
          index_data(api_log_search_models)
          ApplicationLogger.debug('Successfully updated api_log-index with ' + api_log_search_models.length.to_s + ' models')
        else
          re_initialize_index
        end
      end

      # 
      # This function delete api_log-index and re-create
      # with all the api_log data
      #
      def re_initialize_index
        delete_previous_index
        api_log_search_models = @api_log_data_loader.load_data(0, true)
        index_data(api_log_search_models)
        ApplicationLogger.debug('Successfully re_initialize api_log-index with ' + api_log_search_models.length.to_s + ' models')
      end

    end
  end
end

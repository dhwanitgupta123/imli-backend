#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # This is interface which all index service should implement
    # it also has basic functionalities of elasticsearch repository
    #
    class IndexInterface < BaseModule::V1::BaseService

      def initialize(params = '')

      end
      #
      # This function will be overrided by implementations of this interface 
      #
      def update_index
        raise 'method is not implemented'
      end

      #
      # This function will be overrided by implementations of this interface 
      #
      def re_initialize_index
        raise 'method is not implemented'
      end

      ##################################
      ############Private###############
      ##################################
      private

      # 
      # This function takes search_models as input and check if it is
      # already present in repository or not and accordingly save or update the
      # document
      #
      # @param search_models [Array] array of search model
      #
      def index_data(search_models)

        return false if search_models.blank?

        search_models.each do |search_model|
          if get_document_by_id(search_model.attributes[:id]) == false
            save(search_model)
          else
            update(search_model)
          end
        end
      end

      # 
      # Update document in repositroy
      #
      # @param search_model [Object] search model containing attributes to index
      #
      def update(search_model)
        @repository.update(search_model)
      end

      # 
      # Save document in repositroy
      #
      # @param search_model [Object] search model containing attributes to index
      #
      def save(search_model)
        @repository.save(search_model)
      end

      #
      # This function re-initialize index, delete the existing one
      #
      def delete_previous_index
        @repository.create_index! force: true
      end

      #
      # Return document by given id
      #
      # @param id [Integer] id of search model
      # 
      # @return [Document] this return document if it succeed to find
      # @return [False] if document not found in the repository
      #
      def get_document_by_id(id)
        begin
          @repository.find(id)
        rescue Elasticsearch::Persistence::Repository::DocumentNotFound => e
          return false
        end
      end

      #
      # This function check if index with index-name specified in  repository exists in
      # elastic search server of not
      #
      # @return [Boolean] true if index exists else false
      #
      def index_exists?
        begin
          return false unless @repository.client.indices.exists(index: @repository.index)
          return true if @repository.count > 0
        rescue Faraday::ConnectionFailed
          return false
        end
        return false
      end

    end
  end
end

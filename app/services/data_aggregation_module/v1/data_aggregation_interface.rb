#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # This is interface class each data aggregation service should implement this interface
    #
    class DataAggregationInterface < BaseModule::V1::BaseService

      #
      # This function will be overrided by implementations of this interface 
      #
      def self.aggregate
        raise 'method is not implemented'
      end

    end
  end
end

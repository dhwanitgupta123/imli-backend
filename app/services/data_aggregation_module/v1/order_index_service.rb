#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # This class is responsible to create and maniooulate order-index
    #
    class OrderIndexService < DataAggregationModule::V1::IndexInterface

      ORDER_DATA_LOADER = DataAggregationModule::V1::DataLoaders::V1::OrderDataLoader
      NUMBER_OF_DAYS_DATA = 1

      def initialize(params = '')
        @params = params
        @repository = ORDER_REPOSITORY
        @order_data_loader = ORDER_DATA_LOADER.new
      end

      # 
      # This function update order-index with loaded data, it index
      # all data if order-index has 0 document else update the
      # order-repositroy
      #
      def update_index
        if index_exists?
          order_search_models = @order_data_loader.load_data(NUMBER_OF_DAYS_DATA)
          index_data(order_search_models)
          ApplicationLogger.debug('Successfully updated order-index with ' + order_search_models.length.to_s + ' models')
        else
          re_initialize_index
        end
      end

      # 
      # This function delete order-index and re-create
      # with all the order data
      #
      def re_initialize_index
        delete_previous_index
        order_search_models = @order_data_loader.load_data(0, true)
        index_data(order_search_models)
        ApplicationLogger.debug('Successfully re_initialize order-index with ' + order_search_models.length.to_s + ' models')
      end

    end
  end
end

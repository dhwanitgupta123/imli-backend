#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # This class is responsible to aggregate data
    #
    class UserIndexService < DataAggregationModule::V1::IndexInterface

      USER_DATA_LOADER = DataAggregationModule::V1::DataLoaders::V1::UserDataLoader

      def initialize(params = '')
        super
        @params = params
        @repository = USER_REPOSITORY
        @user_data_loader = USER_DATA_LOADER.new
      end

      # 
      # This function update user-index with loaded data, it index
      # all data if user-index has 0 document else update the
      # user-repositroy
      #
      def update_index
         if index_exists?
          user_search_models = @user_data_loader.load_data(1)
          index_data(user_search_models)
          ApplicationLogger.debug('Successfully updated user-index with ' + user_search_models.length.to_s + ' models')
        else
          re_initialize_index
        end
      end

      # 
      # This function delete user-index and re-create
      # with all the user data
      #
      def re_initialize_index
        delete_previous_index
        user_search_models = @user_data_loader.load_data(0, true)
        index_data(user_search_models)
        ApplicationLogger.debug('Successfully re_initialize user-index with ' + user_search_models.length.to_s + ' models')
      end

    end
  end
end

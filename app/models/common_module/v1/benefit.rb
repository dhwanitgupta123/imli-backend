module CommonModule
  module V1
    class Benefit < ActiveRecord::Base
      #
      # Defining global variables with versioning
      #
      BENEFIT_MODEL_STATES = CommonModule::V1::ModelStates::V1::BenefitStates
      BENEFIT_MODEL_EVENTS = CommonModule::V1::ModelStates::V1::BenefitEvents
      BENEFIT_MODEL = CommonModule::V1::Benefit
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      # A benefit can have many plans associated with it
      has_and_belongs_to_many :plans

      belongs_to :benefit_of, polymorphic: true

      #
      # Workflow to define states of the BENEFIT
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, BENEFIT_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, BENEFIT_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, BENEFIT_MODEL_STATES::DELETED
      end

      #
      # function to create new Benefit object
      # This is just to create, object.save! needs to be called explicitly.
      #
      # @param args [Hash] [Hash containing benefit attributes]
      #
      # @return [Benefit] [Benefit object created]
      def self.new_benefit(args)
        benefit = BENEFIT_MODEL.new(args)
        return benefit
      end

      #
      # Update the Benefit in DB
      #
      # @param args [JSON] [Hash containing Benefit attributes]
      # 
      # @return [type] [description]
      # @raise [InvalidArgumentsError] [if validation fails while updating]
      def update_benefit(args)
        begin
          self.update_attributes!(args)
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(e.message)
        end
      end

      #
      # to save the Benefit in DB
      #
      def save_benefit
        begin
          self.save!
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(e.message)
        end
      end

      #
      # Find Plan by Benefit id
      #
      # @param plan_id [Integer] [Plan id for which Benefit to find]
      #
      # @return [Plan] [Plan object associated with that id]
      # 
      # @raise [ResourceNotFoundError] [if no Benefit found]
      def self.find_by_benefit_id(benefit_id)
        begin
          BENEFIT_MODEL.find(benefit_id.to_i)
        rescue ActiveRecord::RecordNotFound => e
          raise CUSTOM_ERRORS_UTIL::ResourceNotFoundError.new(CONTENT_UTIL::BENEFIT_NOT_FOUND + ' with id ' + benefit_id.to_s)
        end
      end

      #
      # Returns all the benefits
      #
      # @return [Array] array of Benefit model
      #
      def self.get_all_benefits
        BENEFIT_MODEL.all
      end

      #
      # Get all benefits which are not in DELETED state
      #
      def self.get_all_non_deleted_benefits
        BENEFIT_MODEL.where.not({ status: BENEFIT_MODEL_STATES::DELETED })
      end


      #
      # Get all Benefits by status
      # @param status [Integer] [Status of the Benefit]
      #
      # @return [Array] [Array of Benefit model]
      #
      def self.get_benefits_by_status(status)
        BENEFIT_MODEL.where({status: status})
      end

      #
      # Function to change state of Benefit based on the event
      #
      # Raise Error::
      #   * CustomError::PreConditionRequiredError if transition is not defined for the current status
      #
      def trigger_event(event)
        begin
          case event.to_i
            when BENEFIT_MODEL_EVENTS::ACTIVATE
              self.activate!
            when BENEFIT_MODEL_EVENTS::DEACTIVATE
              self.deactivate!
            when BENEFIT_MODEL_EVENTS::SOFT_DELETE
              self.soft_delete!
            else
              raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::EVENT_NOT_DEFINED % {event: "#{event.to_s}"})
          end
        rescue Workflow::NoTransitionAllowed => e
          raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(e.message)
        end
      end

    end
  end
end
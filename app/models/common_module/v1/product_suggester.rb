module CommonModule
  module V1
  	#
    # Mongoid document to save ProductSuggestions by User
    #
		class ProductSuggester

			#Making it mongoid document
		  include Mongoid::Document
		  #Saving timeStamp with each log
      include Mongoid::Timestamps

			PRODUCT_SUGGESTER_MODEL = CommonModule::V1::ProductSuggester
			PAGINATION_UTIL = CommonModule::V1::Pagination
			CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
			CONTENT = CommonModule::V1::Content
      CACHE_UTIL = CommonModule::V1::Cache

      field :user_id, type: Integer
		  field :product_suggestions, type: Array

		  validates_presence_of :user_id, :product_suggestions

		  # 
		  # update_product_suggestion_for_user function
		  # @param args [Hash]
		  # contains-
		  # 			* user_id [String]: user id of the user suggesting the product
		  # 			* product [String]: product name of the suggested product
		  # 
		  # Summary-
		  # * Function checks for existing user suggestions object in Mongo, 
		  #   and appends it
		  # * If no suggestion are  present before for user, creates a new
		  #   ProductSuggester Model
		  # 
		  def self.update_product_suggestion_for_user(args)
		  	initialize_suggestion_params(args)

		  	existing_suggestion = user_suggestion_exists?
		  	
		  	if existing_suggestion.blank?
		  		existing_suggestion = create_new_suggestion
		  	end

		  	existing_suggestion.product_suggestions.push(@product)

		  	begin
		  		existing_suggestion.save!
		  		rescue => e
			  		raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT::PROBLEM_SAVING_SUGGESTION)
		  	end
		  end

		  # 
		  # user_suggestion_exists? function
		  # 
		  # @return [ProductSuggester Model] existing Suggestion object for a user
		  def self.user_suggestion_exists?
		  	PRODUCT_SUGGESTER_MODEL.where(user_id: @user_id).entries.first
		  end

	 	  #
      # function to return array of all the suggestions paginated
      #
      # @return [array of product suggester object]
      def self.paginated_product_suggestions(paginate_param)
		  	if paginate_param[:user_id].present?
          return get_suggestions_by_user_id(paginate_param)
        else
          return get_all_suggestions(paginate_param)
        end
		  end

		  #
      # function to return paginated suggestions by user_id
      #
      def self.get_suggestions_by_user_id(paginate_param)
        set_pagination_properties(paginate_param)
        suggestions = PRODUCT_SUGGESTER_MODEL.where({ user_id: @user_id }).order_by(@sort_order)
        return get_paginated_suggestions(suggestions)
      end

      #
      # function to return paginated all suggestions
      #
      def self.get_all_suggestions(paginate_param)
        set_pagination_properties(paginate_param)
        suggestions = PRODUCT_SUGGESTER_MODEL.all.order_by(@sort_order)
        return get_paginated_suggestions(suggestions)
      end

      # 
		  # creates new ProductSuggester object in Mongoid document
		  # 
		  # @return [ProductSuggester Model] new ProductSuggester Model
		  def self.create_new_suggestion
		  	new_suggestion = PRODUCT_SUGGESTER_MODEL.new
		  	new_suggestion.product_suggestions = []
		  	new_suggestion.user_id = @user_id

		  	return new_suggestion
		  end

      #
      # Funciton to return list of products suggested by User in last 24 hrs
      # 
      def self.get_all_last_x_hours_suggestions(x = '')
        x =  CACHE_UTIL.read('DEFAULT_TIME_RANGE').to_i if x.blank?
        products = ProductSuggester.where(updated_at:(x.hours.ago..Time.zone.now))
        return products
      end
		  
		  ###############################
      #       Private Functions     #
      ###############################
      
		  private

		  # 
		  # initialize params for user and product suggestion
		  # @param args [Hash] 
		  # * contains user_id [Integer] user_id of the user who suggested
		  # * contains product [String] product suggestion by user
		  # 
		  def self.initialize_suggestion_params(args)
		  	@user_id = args[:user_id]
		  	@product = args[:product_name]
		  end

		  #
      # function to set pagination properties
      #
      def self.set_pagination_properties(paginate_param)
        @per_page = (paginate_param[:per_page] || PAGINATION_UTIL::PER_PAGE).to_f
        @page_no = (paginate_param[:page_no] || PAGINATION_UTIL::PAGE_NO).to_f
        sort_by = paginate_param[:sort_by] || PAGINATION_UTIL::SORT_BY
        order = paginate_param[:order] || PAGINATION_UTIL::ORDER
        @sort_order = sort_by + ' => ' + order
        @user_id = paginate_param[:user_id]
      end

      #
      # function to paginate the suggestions
      #
      def self.get_paginated_suggestions(suggestions)
        page_count = (suggestions.count / @per_page).ceil
        paginated_suggestion = suggestions.limit(@per_page).offset((@page_no - 1) * @per_page)
        return { suggestions: paginated_suggestion.entries, page_count: page_count }
      end

		end
	end
end
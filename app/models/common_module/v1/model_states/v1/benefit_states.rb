module CommonModule
  module V1
    # 
    # Module to keep information of model state
    # 
    module ModelStates
      module V1
        # 
        # UserStates has enums corresponding to the possible states of Benefit model
        # 
        class BenefitStates
          ACTIVE    = 1
          INACTIVE  = 2
          DELETED   = 3
        end
      end
    end
  end
end
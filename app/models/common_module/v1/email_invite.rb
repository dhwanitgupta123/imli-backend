module CommonModule
  module V1
    #
    # Mongoid document to save Emails of User for invite
    #
    class EmailInvite

      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      CACHE_UTIL = CommonModule::V1::Cache

      #Making it mongoid document
      include Mongoid::Document
      #Saving timeStamp with each log
      include Mongoid::Timestamps

      field :email_id, type: String

      VALID_EMAIL_REGEX = CommonModule::V1::RegexUtil::VALID_EMAIL_REGEX

      validates :email_id, presence: true, length: { maximum: 255 },
                        format: { with: VALID_EMAIL_REGEX },
                        uniqueness: { case_sensitive: false }

      #
      # Function to store new email id for invite
      #
      def self.save_email(email_invite_params)
        email = EmailInvite.new(email_invite_params)
        begin
          email.save!
          rescue Mongoid::Errors::Validations, Mongoid::Errors::InvalidValue => e
            raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::UNIQUE_EMAIL)
        end
      end

      #
      # Function to send a list of all email ids for invites
      # requested by user of last 24 Hrs
      #
      def self.get_all_last_x_hours_email_invites_request(x = '')
        x =  CACHE_UTIL.read('DEFAULT_TIME_RANGE').to_i if x.blank?
        emails = EmailInvite.where(created_at:(x.hours.ago..Time.zone.now))
        return emails
      end
    end
  end
end

module CategorizationModule
  module V1
    #
    # Departments model class to interact with department db table
    #
    class Department < ActiveRecord::Base
      #
      # Defining global variables with versioning
      #
      DEPARTMENT_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::DepartmentStates
      DEPARTMENT_MODEL = CategorizationModule::V1::Department
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      PAGINATION_UTIL = CommonModule::V1::Pagination
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper

      #
      # Department has many categories
      #
      has_many :categories, dependent: :destroy

      #
      # Department object can't exists without it's label
      #
      validates :label, presence: true, uniqueness: true
      validates :priority, uniqueness: true

      #
      # Workflow to define states of the Category
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, DEPARTMENT_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, DEPARTMENT_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, DEPARTMENT_MODEL_STATES::DELETED
      end

      #
      # Create department with passed arguments
      #
      # Parameters::
      #   * args [Hash] Hash of all required details
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError: if unable to save to DB
      #     (mostly occurs when validation checks failed)
      #
      # @return [Department Object] if created successfully
      #
      def self.create_department(department_params)
        if department_params[:images].present?
          image_id_array = IMAGE_SERVICE_HELPER.get_sorted_image_id_array_by_priority(department_params[:images])
          validate_images(image_id_array)
          department_params[:images] = image_id_array
        end

        department = DEPARTMENT_MODEL.new(
          label: department_params[:label],
          image_url: department_params[:image_url],
          description: department_params[:description],
          priority: department_params[:priority],
          images: department_params[:images]
        )
        begin
          department.save_department
          bind_image_with_resource(department.images, self.name) if department_params[:images].present?
          return department
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(e.message)
        end
      end

      #
      # to save the department in DB
      #
      def save_department
        self.save!
      end

      #
      # function to return the department object if it exists
      #
      # Parameters::
      #   * department_id [integer] id of department to be searched
      #
      # @return [ Department object]
      #
      def self.find_by_department_id(department_id)
        begin
          return DEPARTMENT_MODEL.find(department_id)
        rescue ActiveRecord::RecordNotFound => e
          return nil
        end
      end

      #
      # Update department with passed arguments
      #
      # Parameters::
      #   * args [Hash] Hash of all required details
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError: if unable to save to DB
      #     (mostly occurs when validation checks failed)
      #
      # @return [Department Object] if created successfully
      #
      def self.update_department(department_params, department)
        if department_params[:images].present?
          image_id_array = IMAGE_SERVICE_HELPER.get_sorted_image_id_array_by_priority(department_params[:images])
          validate_images(image_id_array)
          department.images = image_id_array
        end

        department.label = department_params[:label] if department_params[:label].present?
        department.image_url = department_params[:image_url] if department_params[:image_url].present?
        department.description = department_params[:description] if department_params[:description].present?
        department.priority = department_params[:priority] if department_params[:priority].present?
        begin
          department.save!
          bind_image_with_resource(department.images, self.name) if department_params[:images].present?
          return department
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(e.message)
        end
      end

      #
      # function to return array of all the departments paginated
      #
      # @return [array of department object]
      def self.paginated_departments(paginate_param)
        if paginate_param[:state].present?
          return get_departments_by_status(paginate_param)
        else
          return get_all_departments(paginate_param)
        end
      end

      #
      # function to return paginated departments based on the status
      #
      def self.get_departments_by_status(paginate_param)
        set_pagination_properties(paginate_param)
        departments = DEPARTMENT_MODEL.where({ status: @state }).order(@sort_order)
        return get_paginated_departments(departments)
      end

      #
      # function to return paginated all departments
      #
      def self.get_all_departments(paginate_param)
        set_pagination_properties(paginate_param)
        departments = DEPARTMENT_MODEL.where.not({ status: DEPARTMENT_MODEL_STATES::DELETED }).order(@sort_order)
        return get_paginated_departments(departments)
      end

      #
      # function to set pagination properties
      #
      def self.set_pagination_properties(paginate_param)
        @per_page = (paginate_param[:per_page] || PAGINATION_UTIL::PER_PAGE).to_f
        @page_no = (paginate_param[:page_no] || PAGINATION_UTIL::PAGE_NO).to_f
        sort_by = paginate_param[:sort_by] || PAGINATION_UTIL::CATEGORIZATION_SORT_BY
        order = paginate_param[:order] || PAGINATION_UTIL::ORDER
        @sort_order = sort_by + ' ' + order
        @state = paginate_param[:state]
      end

      #
      # function to paginate the departments
      #
      def self.get_paginated_departments(departments)
        page_count = (departments.count / @per_page).ceil
        paginated_department = departments.limit(@per_page).offset((@page_no - 1) * @per_page)
        return { departments: paginated_department, page_count: page_count }
      end

      #
      # Function to change state of department based on the event
      #
      # Raise Error::
      #   * CustomError::PreConditionRequiredError if trsnsition is not defined for the current status
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(e.message)
        end
      end

      #
      # Get current state of Department
      #
      # @return [String] Current state
      def get_current_state
        self.current_state.name
      end

      #
      # Returns True/false depending on it's active status
      #
      def department_is_active
        self.active? ? true : false
      end

      #
      # Return department corresponding to label
      #
      def self.get_department_by_label(label)
        DEPARTMENT_MODEL.find_by(label: label)
      end

      #
      # validate if all the image_id present in image_id_array is present or not
      #
      # @param image_id_array [Array] array containing all the image_ids
      #
      # @error [InvalidArgumentsError] if image_id not present in image table
      #
      def self.validate_images(image_id_array)
        if IMAGE_SERVICE_HELPER.validate_images(image_id_array) == false
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::IMAGE_LINKING_FAILED)
        end
      end

      # 
      # Bind images with resource
      #
      # @param image_ids [Array] Array of image ids
      # @param resource_name [String] Mode name
      #
      def self.bind_image_with_resource(image_ids, resource_name)
        IMAGE_SERVICE_HELPER.bind_image_with_resource(image_ids, resource_name)
      end
    end
  end
end

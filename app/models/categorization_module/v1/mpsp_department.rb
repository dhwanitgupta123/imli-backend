module CategorizationModule
  module V1
    #
    # Departments model class to interact with mpsp_department db table
    #
    class MpspDepartment < ActiveRecord::Base
      #
      # Defining global variables with versioning
      #
      DEPARTMENT_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::DepartmentStates
      MPSP_DEPARTMENT_MODEL = CategorizationModule::V1::MpspDepartment
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      PAGINATION_UTIL = CommonModule::V1::Pagination
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper

      #
      # Department has many categories
      #
      has_many :mpsp_categories, dependent: :destroy

      #
      # Department object can't exists without it's label
      #
      validates :label, presence: true, uniqueness: true

      #
      # Workflow to define states of the Category
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, DEPARTMENT_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, DEPARTMENT_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, DEPARTMENT_MODEL_STATES::DELETED
      end

      #
      # Create mpsp_department with passed arguments
      #
      # Parameters::
      #   * args [Hash] Hash of all required details
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError: if unable to save to DB
      #     (mostly occurs when validation checks failed)
      #
      # @return [Department Object] if created successfully
      #
      def self.create_mpsp_department(mpsp_department_params)
        if mpsp_department_params[:images].present?
          image_id_array = IMAGE_SERVICE_HELPER.get_sorted_image_id_array_by_priority(mpsp_department_params[:images])
          validate_images(image_id_array)
          mpsp_department_params[:images] = image_id_array
        end

        mpsp_department = MPSP_DEPARTMENT_MODEL.new(
          label: mpsp_department_params[:label],
          description: mpsp_department_params[:description],
          priority: mpsp_department_params[:priority],
          images: mpsp_department_params[:images]
        )
        begin
          mpsp_department.save_mpsp_department
          bind_image_with_resource(mpsp_department.images, self.name) if mpsp_department_params[:images].present?
          return mpsp_department
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(e.message)
        end
      end

      #
      # to save the mpsp_department in DB
      #
      def save_mpsp_department
        self.save!
      end

      #
      # function to return the mpsp_department object if it exists
      #
      # Parameters::
      #   * mpsp_department_id [integer] id of mpsp_department to be searched
      #
      # @return [ Department object]
      #
      def self.find_by_mpsp_department_id(mpsp_department_id)
        begin
          return MPSP_DEPARTMENT_MODEL.find(mpsp_department_id)
        rescue ActiveRecord::RecordNotFound => e
          return nil
        end
      end

      #
      # Update mpsp_department with passed arguments
      #
      # Parameters::
      #   * args [Hash] Hash of all required details
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError: if unable to save to DB
      #     (mostly occurs when validation checks failed)
      #
      # @return [Department Object] if created successfully
      #
      def self.update_mpsp_department(mpsp_department_params, mpsp_department)
        if mpsp_department_params[:images].present?
          image_id_array = IMAGE_SERVICE_HELPER.get_sorted_image_id_array_by_priority(mpsp_department_params[:images])
          validate_images(image_id_array)
          mpsp_department.images = image_id_array
        end

        mpsp_department.label = mpsp_department_params[:label] if mpsp_department_params[:label].present?
        mpsp_department.description = mpsp_department_params[:description] if mpsp_department_params[:description].present?
        mpsp_department.priority = mpsp_department_params[:priority] if mpsp_department_params[:priority].present?
        begin
          mpsp_department.save!
          bind_image_with_resource(mpsp_department.images, self.name) if mpsp_department_params[:images].present?
          return mpsp_department
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(e.message)
        end
      end

      #
      # function to return array of all the mpsp_departments paginated
      #
      # @return [array of mpsp_department object]
      def self.paginated_mpsp_departments(paginate_param)
        if paginate_param[:state].present?
          return get_mpsp_departments_by_status(paginate_param)
        else
          return get_all_mpsp_departments(paginate_param)
        end
      end

      #
      # function to return paginated mpsp_departments based on the status
      #
      def self.get_mpsp_departments_by_status(paginate_param)
        set_pagination_properties(paginate_param)
        mpsp_departments = MPSP_DEPARTMENT_MODEL.where({ status: @state }).order(@sort_order)
        return get_paginated_mpsp_departments(mpsp_departments)
      end

      #
      # function to return paginated all mpsp_departments
      #
      def self.get_all_mpsp_departments(paginate_param)
        set_pagination_properties(paginate_param)
        mpsp_departments = MPSP_DEPARTMENT_MODEL.where.not({ status: DEPARTMENT_MODEL_STATES::DELETED }).order(@sort_order)
        return get_paginated_mpsp_departments(mpsp_departments)
      end

      #
      # function to set pagination properties
      #
      def self.set_pagination_properties(paginate_param)
        @per_page = (paginate_param[:per_page] || PAGINATION_UTIL::PER_PAGE).to_f
        @page_no = (paginate_param[:page_no] || PAGINATION_UTIL::PAGE_NO).to_f
        sort_by = paginate_param[:sort_by] || PAGINATION_UTIL::CATEGORIZATION_SORT_BY
        order = paginate_param[:order] || PAGINATION_UTIL::ORDER
        @sort_order = sort_by + ' ' + order
        @state = paginate_param[:state]
      end

      #
      # function to paginate the mpsp_departments
      #
      def self.get_paginated_mpsp_departments(mpsp_departments)
        page_count = (mpsp_departments.count / @per_page).ceil
        paginated_mpsp_department = mpsp_departments.limit(@per_page).offset((@page_no - 1) * @per_page)
        return { mpsp_departments: paginated_mpsp_department, page_count: page_count }
      end

      #
      # Function to change state of mpsp_department based on the event
      #
      # Raise Error::
      #   * CustomError::PreConditionRequiredError if trsnsition is not defined for the current status
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(e.message)
        end
      end

      #
      # Get current state of Department
      #
      # @return [String] Current state
      def get_current_state
        self.current_state.name
      end

      #
      # Returns True/false depending on it's active status
      #
      def mpsp_department_is_active
        self.active? ? true : false
      end

      #
      # Return mpsp_department corresponding to label
      #
      def self.get_mpsp_department_by_label(label)
        MPSP_DEPARTMENT_MODEL.find_by(label: label)
      end

      #
      # validate if all the image_id present in image_id_array is present or not
      #
      # @param image_id_array [Array] array containing all the image_ids
      #
      # @error [InvalidArgumentsError] if image_id not present in image table
      #
      def self.validate_images(image_id_array)
        if IMAGE_SERVICE_HELPER.validate_images(image_id_array) == false
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::IMAGE_LINKING_FAILED)
        end
      end

      #
      # Bind images with resource
      #
      # @param image_ids [Array] Array of image ids
      # @param resource_name [String] Mode name
      #
      def self.bind_image_with_resource(image_ids, resource_name)
        IMAGE_SERVICE_HELPER.bind_image_with_resource(image_ids, resource_name)
      end
    end
  end
end

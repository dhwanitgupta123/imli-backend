module CategorizationModule
  module V1
    #
    # Category model class to interact with category db table
    #
    class Category < ActiveRecord::Base
      #
      # Defining global variables with versioning
      #
      CATEGORY_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::CategoryStates
      CATEGORY_MODEL = CategorizationModule::V1::Category
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      PAGINATION_UTIL = CommonModule::V1::Pagination

      #
      # Category belongs to department
      #
      belongs_to :department, dependent: :destroy

      #
      # Category has a self join to it's sub category
      #
      belongs_to :parent_category, class_name: 'Category', dependent: :destroy
      has_many :sub_categories, class_name: 'Category', foreign_key: 'parent_category_id', dependent: :destroy

      #
      # Sub Category has many brand packs
      #
      has_many :brand_packs, class_name: 'MasterProductModule::V1::BrandPack', foreign_key: 'sub_category_id', dependent: :destroy
      #
      # Category object can't exists without it's label
      #
      validates :label, presence: true

      #
      # Workflow to define states of the Category
      #
      # Initial State => Inactive
      #
      # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, CATEGORY_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, CATEGORY_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, CATEGORY_MODEL_STATES::DELETED
      end

      #
      # Create category with passed arguments
      #
      # Parameters::
      #   * args [Hash] Hash of all required details
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError: if unable to save to DB
      #     (mostly occurs when validation checks failed)
      #
      # @return [Category Object] if created successfully
      #
      def self.create_category(category_params)
        category = CATEGORY_MODEL.new(
          label: category_params[:label],
          image_url: category_params[:image_url],
          description: category_params[:description],
          priority: category_params[:priority]
        )
        begin
          category.save_category
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(e.message)
        end
        return category
      end

      #
      # Update department with passed arguments
      #
      # Parameters::
      #   * args [Hash] Hash of all required details
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError: if unable to save to DB
      #     (mostly occurs when validation checks failed)
      #
      # @return [Department Object] if created successfully
      #
      def update_category(category_params)
        self.label = category_params[:label] if category_params[:label].present?
        self.department_id = category_params[:department_id] if category_params[:department_id].present?
        self.parent_category_id = category_params[:category_id] if category_params[:category_id].present?
        self.image_url = category_params[:image_url] if category_params[:image_url].present?
        self.description = category_params[:description] if category_params[:description].present?
        self.priority = category_params[:priority] if category_params[:priority].present?
        begin
          self.save_category
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(e.message)
        end
      end

      #
      # to save the Category in DB
      #
      def save_category
        self.save!
      end

      #
      # function to return the category object if it exists
      #
      # Parameters::
      #   * category_id [integer] id of category to be searched
      #
      # @return [ Category object]
      #
      def self.find_by_category_id(category_id)
        begin
          return CATEGORY_MODEL.find(category_id)
        rescue ActiveRecord::RecordNotFound => e
          return nil
        end
      end

      #
      # function to return array of all the categories paginted
      #
      # @return [array of categories object]
      def self.paginated_categories(paginate_param)
        if paginate_param[:state].present?
          return get_categories_by_status(paginate_param)
        else
          return get_all_categories(paginate_param)
        end
      end

      #
      # function to return paginated categories based on the status
      #
      def self.get_categories_by_status(paginate_param)
        set_pagination_properties(paginate_param)
        categories = CATEGORY_MODEL.where({ status: @state, parent_category: nil}).order(@sort_order)
        return get_paginated_categories(categories)
      end

      #
      # function to return paginated all categories
      #
      def self.get_all_categories(paginate_param)
        set_pagination_properties(paginate_param)
        categories = CATEGORY_MODEL.where.not({ status: CATEGORY_MODEL_STATES::DELETED, department: nil }).order(@sort_order)
        return get_paginated_categories(categories)
      end

      #
      # function to return array of all the sub categories paginted
      #
      # @return [array of sub categories object]
      def self.paginated_sub_categories(paginate_param)
        if paginate_param[:state].present?
          return get_sub_categories_by_status(paginate_param)
        else
          return get_all_sub_categories(paginate_param)
        end
      end

      #
      # function to return paginated categories based on the status
      #
      def self.get_sub_categories_by_status(paginate_param)
        set_pagination_properties(paginate_param)
        sub_categories = CATEGORY_MODEL.where({ status: @state, department: nil}).order(@sort_order)
        return get_paginated_sub_categories(sub_categories)
      end

      #
      # function to return paginated all categories
      #
      def self.get_all_sub_categories(paginate_param)
        set_pagination_properties(paginate_param)
        sub_categories = CATEGORY_MODEL.where.not({ status: CATEGORY_MODEL_STATES::DELETED, parent_category: nil }).order(@sort_order)
        return get_paginated_sub_categories(sub_categories)
      end

      #
      # function to set pagination properties
      #
      def self.set_pagination_properties(paginate_param)
        @per_page = (paginate_param[:per_page] || PAGINATION_UTIL::PER_PAGE).to_f
        @page_no = (paginate_param[:page_no] || PAGINATION_UTIL::PAGE_NO).to_f
        sort_by = paginate_param[:sort_by] || PAGINATION_UTIL::CATEGORIZATION_SORT_BY
        order = paginate_param[:order] || PAGINATION_UTIL::ORDER
        @sort_order = sort_by + ' ' + order
        @state = paginate_param[:state]
      end

      #
      # function to paginate the categories
      #
      def self.get_paginated_categories(categories)
        page_count = (categories.count / @per_page).ceil
        paginated_categories = categories.limit(@per_page).offset((@page_no - 1) * @per_page)
        return { categories: paginated_categories, page_count: page_count }
      end

      #
      # function to paginate the categories
      #
      def self.get_paginated_sub_categories(sub_categories)
        page_count = (sub_categories.count / @per_page).ceil
        paginated_sub_categories = sub_categories.limit(@per_page).offset((@page_no - 1) * @per_page)
        return { sub_categories: paginated_sub_categories, page_count: page_count }
      end

      #
      # function to return array of all the active categories
      #
      # @return [array of categories object]
      def self.get_active_categories_by_department(department)
        set_pagination_properties({})
        categories = department.categories.
            where({ status: CATEGORY_MODEL_STATES::ACTIVE }).order(@sort_order)
        return categories
      end

      #
      # Function to change state of category based on the event
      #
      # Raise Error::
      #   * CustomError::PreConditionRequiredError if trsnsition is not defined for the current status
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(e.message)
        end
      end

      #
      # Get current state of Category
      #
      # @return [String] Current state
      def get_current_state
        self.current_state.name
      end

      #
      # Return category with label = label and department_id = department_id
      #
      def self.get_category_by_label_and_department(label, department_id)
        CATEGORY_MODEL.find_by(label: label, department_id: department_id)
      end

      #
      # Return sub category by label and category id
      #
      def self.get_sub_category_by_label_and_category_id(label, category_id)
        CATEGORY_MODEL.find_by(label: label, parent_category_id: category_id)
      end
    end
  end
end

module CategorizationModule
  module V1
    # 
    # Module to keep information of model state
    # 
    module ModelStates
      module V1
        # 
        # DepartmentStates has enums corresponding to the possible states of department model
        # 
        class DepartmentStates
          ACTIVE      = 1
          INACTIVE    = 2
          DELETED     = 3
        end
      end
    end
  end
end
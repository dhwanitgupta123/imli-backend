module CategorizationModule
  module V1
    # 
    # Module to keep information of model state
    # 
    module ModelStates
      module V1
        # 
        # CategoryStates has enums corresponding to the possible states of category model
        # 
        class CategoryStates
          ACTIVE      = 1
          INACTIVE    = 2
          DELETED     = 3
        end
      end
    end
  end
end
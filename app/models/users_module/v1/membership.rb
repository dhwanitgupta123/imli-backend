module UsersModule
  module V1
    class Membership < ActiveRecord::Base

      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      MEMBERSHIP_MODEL = UsersModule::V1::Membership
      MEMBERSHIP_STATES = UsersModule::V1::ModelStates::MembershipStates

      # Membership can belong to multiple users
      # Usecase: One Family (>1 member) can have one membership
      has_and_belongs_to_many :users

      has_one :membership_payment, dependent: :destroy, class_name: 'UsersModule::V1::MembershipModule::V1::MembershipPayment'

      # Membership can belong to only one plan
      belongs_to :plan, class_name: "CommonModule::V1::Plan"

      # Membership entity can not exist without presence of its
      # associated plan, starts and expiry date
      validates :starts_at, :expires_at, :plan, presence: true

      # Validates whether expiry date is after starts date
      validate :expires_must_be_after_starts_date_time

      #
      # Workflow to define states of the Membership Model
      #
      # Initial State => Inactive
      # State Diagram::
      #   Inactive --initial_trial--> Pending
      #   Inactive --initiate_payment--> payment_pending
      #   Pending --activate--> Active
      #   pending --force_expire--> expired
      #   payment_pending --payment_successful--> active
      #   payment_pending --payment_failed--> deleted
      #   payment_pending --force_expire--> expired
      #   active --expire--> expired
      #   active --force_expire--> expired
      #   Expired
      #   Deleted
      # * Inactive -> When Membership is initially assigned to user
      # * Pending -> When a trial plan is assigned to this membership\
      # * Payment Pending -> when payment is pending
      # * Active -> When user performed first purchase, we will assign a starts_at
      #     and expires_at time or payment is successful
      # * Expired -> When user switched on to different membership plan or his current
      #     membership expires_at date goes off.
      # * Deleted -> whene payment failes
      #
      # initiate_trial, initiate_payment, activate, force_expire, payment_successful,
      # payment_failed, force_expire, expire are the events which triggers the state transition
      #
      # Refer Workflow Gem git page for more reference
      #
      include Workflow
      workflow do
        state :inactive, MEMBERSHIP_STATES::INACTIVE do
          event :initiate_trial, transitions_to: :pending
          event :initiate_payment, transitions_to: :payment_pending
        end
        state :pending , MEMBERSHIP_STATES::PENDING do
          event :activate, transitions_to: :active
          event :force_expire, transitions_to: :expired
        end
        state :payment_pending, MEMBERSHIP_STATES::PAYMENT_PENDING do
          event :payment_successful, transitions_to: :active
          event :payment_failed, transitions_to: :deleted
          event :force_expire, transitions_to: :expired
        end
        state :active, MEMBERSHIP_STATES::ACTIVE do
          event :expire, transitions_to: :expired
          event :force_expire, transitions_to: :expired
        end  
        state :deleted, MEMBERSHIP_STATES::DELETED
        state :expired, MEMBERSHIP_STATES::EXPIRED
      end

      # 
      # Function to trigger transition from one state to another
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end

      # 
      # Function to return new object of membership
      #
      def new_membership(membership_args)
        membership = MEMBERSHIP_MODEL.new(membership_args)
        return membership
      end

      # 
      # Function to save membership
      #
      def save_membership
        begin
          self.save!
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.
            new(CONTENT::CREATION_FAILED % { model: 'Membership Model' })
        end
      end

      # 
      # Function to update membership
      #
      def update_membership(membership, args)
        args = ActionController::Parameters.new(args)
        args = args.permit(Membership.attribute_names)
        raise @custom_error_util::InsufficientDataError.new(
          CONTENT::INSUFFICIENT_DATA) if membership.nil? || args.nil?
        begin
          membership.update_attributes!(args)
          return membership
        rescue ActiveRecord::RecordInvalid => e
          raise @custom_error_util::InvalidArgumentsError.
            new(CONTENT::UPDATION_FAILED % { model: 'Membership Model' })
        end
      end

      #######################
      # Private Functions   #
      #######################
      private
      # 
      # Expires date must be after start date of membership plan
      # If condition does not satisfy, then it will add error into
      # errors stack and hence validation will fail
      #
      def expires_must_be_after_starts_date_time
        errors.add(:expires_at, "must be after starts_at time") unless
        expires_at >= starts_at
      end

    end
  end
end

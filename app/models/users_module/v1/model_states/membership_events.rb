module UsersModule
  module V1
    module ModelStates
      # 
      # Membership model can have following events
      # 
      class MembershipEvents
        INITIATE_TRIAL      = 1
        INITIATE_PAYMENT    = 2
        ACTIVATE            = 3
        PAYMENT_SUCCESSFUL  = 4
        PAYMENT_FAILED      = 5
        EXPIRE              = 6
        FORCE_EXPIRE        = 7
      end
    end
  end
end
module UsersModule
  module V1
    module ModelStates
      #
      # UserStates has enums corresponding to the possible
      # states of User model
      #
      class UserStates
        ACTIVE      = 1
        PENDING     = 2
        INACTIVE    = 3
        DELETED     = 4
        BLACKLISTED = 5
      end

    end
  end
end
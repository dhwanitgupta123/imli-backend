module UsersModule
  module V1
    # 
    # Module to keep information of model state
    # 
    module ModelStates
      # 
      # UserStates has enums corresponding to the possible events of category model
      # 
      class UserEvents
        OTP_VERIFIED    = 1
        ACTIVATE        = 2
        SOFT_DELETE     = 3
        PERMANENT_BLOCK = 4

        #
        # Get all allowed events for a particular User
        #
        # @param user [Object] [User]
        # 
        # @return [Array] [description]
        #
        def self.get_allowed_events(user)
          return [] if user.blank?
          allowed_actual_events = user.current_state.events.keys
          return convert_allowed_actual_event_to_event_ids(allowed_actual_events)
        end

        private

        #
        # Convert actual events to their respective IDs but
        # with filter over only ALLOWED Events
        # Ex: actual_events_map will be like [:activate, :soft_delete]
        # Final array: [{value: '2', lable: 'ACTIVATE'}, {value: '3', label: 'SOFT_DELETE'}]
        #
        # @param actual_events_map [Array] [Array of Symbols]
        # 
        # @return [Array] [Array of Hash]
        #
        def self.convert_allowed_actual_event_to_event_ids(actual_events_map)
          return [] if actual_events_map.blank?
          events_array = []
          event_map = event_to_id_map
          actual_events_map.each do |event|
            # Event is already a symbol. ex: ':placed'
            map = event_map[event]
            if map.present?
              events_array.push(event_map[event])
            end
          end
          return events_array
        end

        #
        # Fetch map of WorkflowEvents to its respective Hash (id and label)
        #
        def self.event_to_id_map
          event_map = {
            :activate => { id:  ACTIVATE, label: 'ACTIVATE' },
            :soft_delete => { id:  SOFT_DELETE, label: 'SOFT DELETE' },
            :permanent_block => { id: PERMANENT_BLOCK, label: 'PERMANENT BLOCK' }
          }
          return event_map
        end

      end
    end
  end
end
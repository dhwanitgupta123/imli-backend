module UsersModule
  module V1
    module ModelStates
      # 
      # Membership model can have following states.
      # The corresponding integer value is being stored in DB
      # column 'workflow_state'
      # 
      class MembershipStates
        ACTIVE          = 1
        PENDING         = 2
        PAYMENT_PENDING = 3
        DELETED         = 4
        EXPIRED         = 5
        INACTIVE        = 6
      end
    end
  end
end
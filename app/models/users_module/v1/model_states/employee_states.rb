module UsersModule
  module V1
    module ModelStates
      #
      # EmployeeStates has enums corresponding to the possible
      # states of Employee model
      #
      class EmployeeStates
        INACTIVE    = 1
        PENDING     = 2
        ACTIVE      = 3
        DELETED     = 4
        BLACKLISTED = 5
      end

    end
  end
end
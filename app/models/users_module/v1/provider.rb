module UsersModule
  module V1
    class Provider < ActiveRecord::Base
    
      #saves session token when new user is created
      before_validation :save_session_token

      #each user has one provider
      belongs_to :user

      #each user can login through one of the login medium
      has_one :generic ,class_name: "::UsersModule::V1::ProvidersModule::V1::Generic"
      has_one :message, class_name: "::UsersModule::V1::ProvidersModule::V1::Message"

      #validates the presence of foreign_key reference of user_id
      validates :user_id,  presence: true, uniqueness: true

      #validates the presence of session_token
      validates :session_token,  presence: true

      #validates the presence of either of the provider type
      validates :generic, presence: true, unless: ->(provider){provider.message.present?}
      validates :message, presence: true, unless: ->(provider){provider.generic.present?}

      # 
      # Fetch provider associated with passed session token from DB
      # @param token [String] session token of the user
      # 
      # @return [Object] Provider object
      def self.get_provider_for_session_token(token)
        UsersModule::V1::Provider.find_by(:session_token => token)
      end

      ###############################
      #       Private Functions     #
      ###############################

      private
        # 
        # to save the generated session token 
        # 
        def save_session_token
          self.session_token = generate_session_token
        end

        # 
        # generate session token using base64 
        # 
        # parameters::
        #   * 64 defines the length of token to be generated
        #   * false : without padding in the token generated 
        # 
        # logic::
        #   * Session token = urlsafe_base64(64, false) + User_ID to make it unique 
        # 
        # @return string 
        def generate_session_token
          session_token = UsersModule::V1::TokenHelper.generate_session_token(self.user_id)
          return session_token
        end
    end
  end
end
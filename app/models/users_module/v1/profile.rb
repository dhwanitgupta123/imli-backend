module UsersModule
  module V1
    #
    # Class to interact with profile table
    #
    class Profile < ActiveRecord::Base

      # user has a profile
      belongs_to :user

      # 
      # to save the profile in DB
      # 
      def save_profile
        begin
          self.save!
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(e.message)
        end
      end
    end
  end
end

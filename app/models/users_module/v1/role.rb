module UsersModule
  module V1
    class Role < ActiveRecord::Base
      # Define utils globally with versioning
      # Kept them as global constants, so that they are specific
      # to this class only
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      ROLE_MODEL = UsersModule::V1::Role

      # A single role can be assigned to many users
      has_many :user_roles
      has_many :users, through: :user_roles

      # Role Hierarchy (n-ary tree): A role can have many children roles under it
      # but can have only one parent role. The top-most role (root element) will
      # have its parent as NIL
      belongs_to :parent, class_name: "Role"
      has_many :children, class_name: "Role", foreign_key: "parent_id"

      # validates presence of Role title/label
      validates :role_name, presence: true

      # validates presence of permissions array. A Role can not be created without
      # assigning any permissions to it. Permissions are stored as an Array
      validates :permissions, presence: true

      #
      # function to create new role object
      # This is just to create, object.save! needs to be called explicitly.
      #
      # @param args [Hash] [Hash containing role_name and permissions array]
      #
      # @return [Role] [Role object created]
      def self.new_role(args)
        role = ROLE_MODEL.new({
          role_name: args[:role_name],
          permissions: args[:permissions],
          parent: args[:parent]
          })
        return role
      end

      #
      # function to update new role object
      # This is just to update model parameters, object.save! needs to be called explicitly.
      #
      # @param args [Hash] [Hash containing role_name and permissions array]
      #
      def update_role(args)
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new('Role details are blank') if args.blank?
        self.role_name = args[:role_name]
        self.permissions = args[:permissions]
      end

      #
      # Find role by role id
      #
      # @param role_id [Integer] [Role id for which role to find]
      #
      # @return [Role] [Role object associated with that id]
      # 
      # @raise [InvalidArgumentsError] [if no role found]
      def self.find_by_role_id(role_id)
        begin
          ROLE_MODEL.find(role_id.to_i)
        rescue ActiveRecord::RecordNotFound => e
          raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new('Role not found')
        end
      end

      #
      # Search role with passed parameters
      #
      # @param params [JSON] [Hash containing role name and permissions]
      # 
      # @return [Object] [Role object first in the filtered Array]
      def self.get_role_with_parameters(params)
        ROLE_MODEL.where(role_name: params[:role_name], permissions: params[:permissions]).first
      end

      #
      # Create Root Role with parameters
      #
      # @param params [JSON] [Hash containing role name and permissions]
      #
      # @return [Object] [created Role object]
      #
      def self.create_root_role_with_parameters(args)
        begin
          role = ROLE_MODEL.new({
            role_name: args[:role_name],
            permissions: args[:permissions]
            })
          role.save!
          return role
        rescue => e
          raise CUSTOM_ERRORS_UTIL::RunTimeError(e.message)
        end
      end

      #
      # to save the role in DB
      #
      def save_role
        begin
          self.save!
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(e.message)
        end
      end

      #
      # function to get users associated with that role
      #
      def get_users
        self.users
      end

      #
      # Set Role to Inactive/Deleted mode
      # will be mostly used during soft-delete (destroy) of role
      #
      def set_inactive
        # TO-DO Set isActive flag of Role to 0
      end
    end
  end
end
module UsersModule
  module V1
    class UserRole < ActiveRecord::Base
      # UserRole contains mapping of User(user_id) and Role(role_id)
      # Its a relational table between User and Role (user has many roles
      # through user_roles)
      belongs_to :user
      belongs_to :role

      # Validates uniqueness of composite key <user_id, role_id>
      # A user can't have multiple entries with the same role assigned
      validates :user_id, uniqueness: {scope: :role_id}
    end
  end
end
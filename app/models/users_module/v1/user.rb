module UsersModule
  module V1
    class User < ActiveRecord::Base

      # Define utils globally with versioning
      # Kept them as global constants, so that they are specific
      # to this class only
      AMPLE_CREDIT = CreditsModule::V1::AmpleCredit
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      PAGINATION_UTIL = CommonModule::V1::Pagination
      CONTENT_UTIL = CommonModule::V1::Content
      USER_MODEL_STATES = UsersModule::V1::ModelStates::UserStates
      USER_MODEL_EVENTS = UsersModule::V1::ModelStates::UserEvents
      USER_MODEL = UsersModule::V1::User

    	# validates the presence, uniqueness and max length 10 for the field Phone_number
      validates :phone_number, presence: true, length: { is: 10 }, uniqueness: true 

      #validates the presence of first_name field
    	validates :first_name, length: { maximum: 50 }

      #validates the presence of last_name field
    	validates :last_name,  length: { maximum: 50 }

      #user can have many emails based upon the account type viz: employee, customer, vendor
      has_many :emails, dependent: :destroy

      #user can have many devices from which it can login
      has_many :devices, dependent: :destroy

      #user can have many addresses for the delivery
      has_many :addresses, dependent: :destroy,class_name: 'AddressModule::V1::Address'

      #user must have one provider for login
      has_one :provider, dependent: :destroy

      # A user can have many roles associated with its profile
      has_many :user_roles

      has_many :roles, through: :user_roles

      # A single user can have many memberships
      # but only one ACTIVE membership at a time
      has_and_belongs_to_many :memberships

      # A user can also be an employee and hence user might be linked
      # to employee by one-to-one relationship
      has_one :employee, dependent: :destroy

      has_many :carts, class_name: 'MarketplaceOrdersModule::V1::Cart'

      has_many :orders, class_name: 'MarketplaceOrdersModule::V1::Order'
      
      has_many :inventory_orders, dependent: :destroy, class_name: 'InventoryOrdersModule::V1::InventoryOrder'

      # user has a profile
      has_one :profile, dependent: :destroy

      # user has AmpleCredit
      has_one :ample_credit, class_name: 'CreditsModule::V1::AmpleCredit', dependent: :destroy

      has_many :benefits, class_name: 'CommonModule::V1::Benefit', dependent: :destroy, as: :benefit_of

      #
      # Workflow to define states of the User Model
      # 
      # Initial State => Inactive
      # 
      # State Diagram::
      #   Inactive --otp_verified--> Pending
      #   Pending --activate--> Active
      #   Deleted
      #   Blacklisted
      # 
      # * otp_verified & activate are the events which triggers the state transition
      # 
      # * For Active to Deleted & Blacklisted events are not defined
      # 
      # * Refer Workflow Gem git page for more reference
      # 
      include Workflow
      workflow do
        state :inactive, USER_MODEL_STATES::INACTIVE do
          event :otp_verified, :transitions_to => :pending
        end         
        state :pending , USER_MODEL_STATES::PENDING do
          event :activate, :transitions_to => :active
        end
        state :active, USER_MODEL_STATES::ACTIVE do
          event :soft_delete, :transitions_to => :deleted
          event :permanent_block , :transitions_to => :blacklisted
        end
        state :deleted, USER_MODEL_STATES::DELETED
        state :blacklisted, USER_MODEL_STATES::BLACKLISTED
      end

      #
      # This function change the state of the model
      #
      # @param event [Integer] event to trigger 
      #
      # @raise [InvalidArgumentsError] if it fails to change state of the model
      # @raise [PreConditionRequiredError] [if invalid event for current state of user]
      #
      def trigger_event(event)
        begin
          case event.to_i
            when USER_MODEL_EVENTS::OTP_VERIFIED
              self.otp_verified!
            when USER_MODEL_EVENTS::ACTIVATE
              self.activate!
            when USER_MODEL_EVENTS::SOFT_DELETE
              self.soft_delete!
            when USER_MODEL_EVENTS::PERMANENT_BLOCK
              self.permanent_block!
            else
              raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::EVENT_MISSING)
          end
          return true
        rescue Workflow::NoTransitionAllowed => e
          raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(CONTENT_UTIL::INVALID_EVENT)
        end
      end

      #
      # Get all users as per the paginate params
      # Pagination params:
      #  - state: User state (ex: 1/2/3)
      #  - order: In which order want users list (ASC/DESC)
      #  - sort_by: key on which data is to be sorted (ex: id/name/status)
      #  - page_no: Page number offset
      #  - per_page: entries to fetch per page
      #
      # @param paginate_params [JSON] [Hash containing pagination attributes]
      #
      # @return [Array] [Array of all matched entries]
      #
      def self.get_all_users(paginate_params)
        validate_pagination_properties(paginate_params)
        set_pagination_properties(paginate_params)
        if @state.present?
          filtered_users = USER_MODEL.where({ workflow_state: @state })
        else
          filtered_users = USER_MODEL.where.not({ workflow_state: USER_MODEL_STATES::DELETED })
        end
        page_count = (filtered_users.count / @per_page).ceil
        paginated_users = filtered_users.order(@sort_order).
                                limit(@per_page).offset((@page_no - 1) * @per_page)
        return {users: paginated_users, page_count: page_count}
      end

      #
      # Set pagination properties
      # Pagination params:
      #  - state: User state (ex: 1/2/3)
      #  - order: In which order want users list (ASC/DESC)
      #  - sort_by: key on which data is to be sorted (ex: id/name/status)
      #  - page_no: Page number offset
      #  - per_page: entries to fetch per page
      #
      # @param paginate_params [JSON] [Hash containing pagination attributes]
      #
      def self.set_pagination_properties(paginate_params)
        # If none of the page number and per page is sent, then send all data (infinite) 
        if paginate_params[:per_page].blank? && paginate_params[:page_no].blank?
          @per_page = PAGINATION_UTIL::INFINITE_PER_PAGE.to_f
          @page_no = PAGINATION_UTIL::INFINITE_PAGE_NO.to_f
        else
          @per_page = (paginate_params[:per_page] || PAGINATION_UTIL::DEFAULT_PER_PAGE).to_f
          @page_no = (paginate_params[:page_no] || PAGINATION_UTIL::DEFAULT_PAGE_NO).to_f
        end
        @state = paginate_params[:state]

        sort_by = @sort_by || PAGINATION_UTIL::DEFAULT_SORT_BY
        order = @order || PAGINATION_UTIL::DEFAULT_ORDER
        @sort_order = sort_by + ' ' + order

        @per_page = PAGINATION_UTIL::DEFAULT_PER_PAGE.to_f if @per_page <= 0
        @page_no = PAGINATION_UTIL::DEFAULT_PAGE_NO.to_f if @page_no <= 0
      end

      def self.validate_pagination_properties(paginate_params)
        if paginate_params[:sort_by].present? && USER_MODEL.attribute_names.include?(paginate_params[:sort_by])
          @sort_by = paginate_params[:sort_by]
        end
        if paginate_params[:order].present? && PAGINATION_UTIL::ALLOWED_ORDERS.include?(paginate_params[:order])
          @order = paginate_params[:order]
        end
      end

      # 
      # function to find user based on phone number
      # 
      # Parameter::
      #   * Phone_number to find user with as it is our unique key
      # 
      # @return [Object] returns User if found else nil
      def self.exists?(phone_number)
        USER_MODEL.find_by(phone_number: phone_number)
      end

      # 
      # function to find user based on referral code
      # 
      # Parameter::
      #   * referral_code [string] value on base of which we have to find the user
      # 
      # @return [Object] returns User if found else nil
      def self.referred_by(referral_code)
        USER_MODEL.find_by(referral_code: referral_code)
      end

      # 
      # Fetch user corresponding to passed user id
      # @param user_id [String] unique id for user
      # 
      # @return [Object] User Object associated with passed id
      def self.find_by_user_id(user_id)
        USER_MODEL.find(user_id)
      end

      # 
      # to save the user in DB
      # 
      def save_user
        begin
          self.save!
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(e.message)
        end
      end

      # 
      #function to set user state pending
      # 
      def change_user_state_to_pending
        if self.inactive?
          self.otp_verified!
        end
      end

      # 
      #function to set user state active
      # 
      def change_user_state_to_active
        if self.pending?
          self.activate!
        end
      end

      #
      # function to add role to user model
      # Throws no error if role already associated with the user
      #
      # @param role [Role] [Role Object to be associate with the user]
      #
      def add_role(role)
        begin
          self.roles << role
        rescue ActiveRecord::RecordInvalid => e
          # Do Nothing, RecordInvalid is thrown when requested Role
          # is already assigned to the user
        end
      end

      #
      # function to remove role to user model
      # Throws no error if role to remove is not at all associated with the user
      #
      # @param role [Role] [Role Object to be removed from the user]
      #
      def remove_role(role)
        self.roles.delete(role)
      end

      def referrer
        USER_MODEL.find_by(id: self.referred_by)
      end
    end
  end
end

module UsersModule::V1
  #
  # Membership module to handle membership related functionalities
  #
  module MembershipModule::V1
    #
    # Class to interact with membership_payments db table
    #
    class MembershipPayment < ActiveRecord::Base
      CONTENT_UTIL = CommonModule::V1::Content
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      belongs_to :membership, class_name: 'UsersModule::V1::Membership'

      # 
      # Function returns the new object of membership_payment
      #
      def new_payment(payment_args)
        return MembershipPayment.new(payment_args)
      end

      # 
      # Function returns payment based on it's ID
      #
      def get_payment_by_id(payment_id)
        begin
          return MembershipPayment.find(payment_id)
        rescue ActiveRecord::RecordNotFound => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_PAYMENT_ID)
        end
      end
    end
  end
end

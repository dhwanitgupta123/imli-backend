module UsersModule
  module V1
    class Employee < ActiveRecord::Base

      # Define utils globally with versioning
      # Kept them as global constants, so that they are specific
      # to this class only
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      EMPLOYEE_MODEL_STATES = UsersModule::V1::ModelStates::EmployeeStates
      EMPLOYEE_MODEL = UsersModule::V1::Employee

      # An employee belongs to a user
      belongs_to :user

      # Employee Manager Hierarchy (n-ary tree): An empoyee can have many subordinates/employees
      # under it but can have only one manager. The top-most employee (root element) will
      # have its manager as NIL
      belongs_to :manager, class_name: "Employee"
      has_many :subordinates, class_name: "Employee", foreign_key: "manager_id"

      # Validates presence of user relation. An employee does not exist without user associated
      # with it.
      validates :user, presence: true

      #
      # Workflow to define states of the Employee Model
      # Initial State => Inactive
      # 
      # State Diagram::
      #   Inactive --created--> Pending
      #   Pending --approved--> Active
      #   Deleted
      #   Blacklisted
      # Refer Workflow Gem git page for more reference
      # 
      include Workflow
      workflow do
        state :inactive, EMPLOYEE_MODEL_STATES::INACTIVE do
          event :created, :transitions_to => :pending
        end         
        state :pending , EMPLOYEE_MODEL_STATES::PENDING do
          event :approved, :transitions_to => :active
        end
        state :active, EMPLOYEE_MODEL_STATES::ACTIVE
        state :deleted, EMPLOYEE_MODEL_STATES::DELETED
        state :blacklisted, EMPLOYEE_MODEL_STATES::BLACKLISTED
      end

      # 
      # Create employee with passed arguments
      # @param args [Hash] Hash of all required details
      # 
      # @return [Employee Object] if created successfully
      # @raise [CustomErrors::RunTimeError] if unable to save to DB
      # (mostly occurs when validation checks failed)
      def self.create_employee(args)
        employee = EMPLOYEE_MODEL.new(
          :designation => args[:designation],
          :team => args[:team],
          :user => args[:user],
          :manager => args[:manager]
        )

        employee.save_employee
        return employee
      end

      #
      # Set to Inactive/Deleted mode
      # will be mostly used during soft-delete (destroy)
      #
      def set_inactive
        # TO-DO Set isActive flag of Role to 0
      end

      # 
      # to save the employee in DB
      # 
      def save_employee
        begin
          self.save!
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(e.message)
        end
      end

      # 
      # Set Employee state to pending by triggering
      # required event
      # 
      def set_state_to_pending
        if self.inactive?
          self.created!
        end
      end

      # 
      # Set Employee state to active by triggering
      # required event
      # 
      def set_state_to_active
        if self.pending?
          self.approved!
        end
      end

      # 
      # Get current state of Employee
      # 
      # @return [String] Current state
      def get_current_state
        self.current_state.name
      end
    end
  end
end
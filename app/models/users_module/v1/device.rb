module UsersModule
  module V1
    class Device < ActiveRecord::Base
      
      #User to device has one to many relation
      belongs_to :user

      #validates the presence of foreign_key user_id
      validates :user_id, presence: true

      #validates the presence of timestamp last_login field
      validates :last_login, presence: true
    end
  end
end

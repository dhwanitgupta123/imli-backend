module UsersModule
  module V1
    class Email < ActiveRecord::Base

      # Define utils globally with versioning
      # Kept them as global constants, so that they are specific
      # to this class only
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CACHE_UTIL = CommonModule::V1::Cache
      TOKEN_HELPER = UsersModule::V1::TokenHelper
      EMAIL_MODEL = UsersModule::V1::Email
      CONTENT_UTIL = CommonModule::V1::Content

      # regular expression for the validation if email is in valid format or not
      VALID_EMAIL_REGEX = CommonModule::V1::RegexUtil::VALID_EMAIL_REGEX

      VALID_GMAIL_REGEX = CommonModule::V1::RegexUtil::VALID_GMAIL_REGEX

      VERIFY_GMAIL_ID_HELPER = EmailValidationModule::V1::VerifyGmailIdHelper

      # saves authentication token & verifiation expiry time 
      # only during CREATION
      before_create :set_authentication_token, :set_expires_at

      # after create it mark gmail_id used
      after_create :mark_gmail_id_used

      # validates the presence of account_type
      validates :account_type,  presence: true

      # validates the presence, format of email_id field.
      # User validation for no two Users having same email_id for same account type.
      # 
      # viz. Eg:
      # 
      # (user_id:1, account_type:1, email_id: "test@test.com") and
      # (user_id:2, account_type:1, email_id: "test@test.com") cannot exist
      # 
      # but,
      # 
      # (user_id:1, account_type:1, email_id: "test@test.com") and
      # (user_id:2, account_type:2, email_id: "test@test.com") can exist
      #  
      validates :email_id, presence: true, length: { maximum: 255 },
                        format: { with: VALID_EMAIL_REGEX },
                        uniqueness: { case_sensitive: false, scope: :account_type }

      validate :gmail_id_not_exists, on: :create

      # User to email has one to many relation
      belongs_to :user
      # validates the presence of foreign_key reference of user_id
      validates :user, presence: true



      # 
      # Find email by authentication token (Search Query)
      # @param authentication_token [String] [token associated with email]
      # 
      # @return [Email Object] email associated with the token
      # @raise [ActiveRecord::RecordNotFound] if no record found
      def self.find_by_authentication_token(authentication_token)
        EMAIL_MODEL.find_by(authentication_token: authentication_token)
      end

      # 
      # Find email by email id (Search Query)
      # @param email_id [String] [email_id associated with email]
      # 
      # @return [Email Object] email associated with the email_id
      # @raise [ActiveRecord::EmailNotFoundError] if no record found
      def self.find_by_email_id(email_id)
        email = EMAIL_MODEL.find_by(email_id: email_id)
        if email.nil?
          raise CUSTOM_ERRORS_UTIL::EmailNotFoundError.new(CONTENT_UTIL::EMAIL_NOT_REGISTERED)
        end
        return email
      end

      #
      # to set email verification to true
      #
      def set_verify_to_true
        self.verified = true
      end

      # 
      # to save the employee in DB
      # 
      def save_email
        begin
          self.save!
        rescue ActiveRecord::RecordInvalid
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT_UTIL::EMAIL_ALREADY_REGISTERED)
        end
      end

      # 
      # to save the generated authentication token in DB
      # 
      def set_authentication_token
        self.authentication_token = generate_authentication_token
      end

      # 
      # sets the expiry time of the Authentication URL for email verification
      # 
      # logic::
      #   * Expires_at = Time.now() + EMAIL_TIMEOUT in days (currently 7 Days) 
      # 
      def set_expires_at
        expires_at = Time.zone.now + CACHE_UTIL.read("EMAIL_TIMEOUT_DAYS").to_i.days
        self.expires_at = expires_at
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # get authentication token from helper token module
      #
      # @return string 
      def generate_authentication_token
        authentication_token = TOKEN_HELPER.generate_authentication_token
        return authentication_token
      end

      #
      # function to check whether entered gmail id already exists or not
      #
      def gmail_id_not_exists
        return 1 if errors[:email_id].present? || (email_id =~ VALID_GMAIL_REGEX).nil?
        
        if VERIFY_GMAIL_ID_HELPER.verify_if_already_in_use(email_id)
          errors.add(:email_id,"has already been taken")
          return 0
        end
      end

      #
      # This function adds gmail id in already used set of gmail id 
      #
      def mark_gmail_id_used
        return 1 if (email_id =~ VALID_GMAIL_REGEX).nil?

        VERIFY_GMAIL_ID_HELPER.mark_gmail_id_in_use(email_id)
      end
    end
  end
end
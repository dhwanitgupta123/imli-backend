module UsersModule
  module V1
    module ProvidersModule
      module V1
        class Message < ActiveRecord::Base
         
          #updates the expiry time for otp when requested again
          before_validation :update_expires_at 
          
          # validates the presence of otp
          validates :otp,  presence: true 

          #validates the presence of timestamp
          validates :expires_at,  presence: true

          # validates the presence of provider_id
          validates :provider_id,  presence: true, uniqueness: true

          #message has one on one relation with provider
          belongs_to :provider, dependent: :destroy

          # 
          # to save generic object in db
          # 
          def save_message
            begin
              self.save!
            rescue ActiveRecord::RecordInvalid => e
              raise CUSTOM_ERRORS_UTIL::RunTimeError.new(e.message)
            end
          end

          # 
          # function to update expiry time of OTP
          # 
          def update_expires_at
            expires_at = Time.zone.now + CommonModule::V1::Cache.read("OTP_TIMEOUT_MINS").to_i.minutes
            self.expires_at = expires_at
            self.resend_count = self.resend_count.to_i + 1
          end
        end
      end
    end
  end
end

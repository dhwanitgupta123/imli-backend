module UsersModule
  module V1
    module ProvidersModule
      module V1
        class Generic < ActiveRecord::Base

          CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
          CONTENT_UTIL = CommonModule::V1::Content

          #validates the presence & length of the password
          validates :password, presence: true, length: { is: 178 }

          # validates the presence of provider_id
          validates :provider_id,  presence: true, uniqueness: true

          #generic has one on one relation with provider
          belongs_to :provider, dependent: :destroy
          # 
          # to save message object in db
          # 
          def save_generic
            begin
              self.save!
            rescue ActiveRecord::RecordInvalid => e
              raise CUSTOM_ERROR_UTIL::RunTimeError.new(e.message)
            end
          end

          #
          # function to update generic provider object
          # args: {
          #   password: <String>
          #   reset_flag: <Boolean>
          # }
          #
          # @param args [Hash] [Hash containing password and reset_flag]
          #
          def update_generic(args)
            # Filtering the args before making DB call 
            args = filter_args(args)
            raise CUSTOM_ERROR_UTIL::InsufficientDataError.new('Generic details are blank') if args.blank?
            begin
              self.update_attributes!(args)
              return self
            rescue ActiveRecord::RecordInvalid => e
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.
                new(CONTENT_UTIL::UPDATION_FAILED % { model: 'Generic Provider' })
            end
          end

          private

          #
          # Permit only arguments which are model attributes
          #
          # @param args [JSON] [Hash of arguments]
          # 
          # @return [JSON] [filtered hash of arguments]
          #
          def filter_args(args)
            args = ActionController::Parameters.new(args)
            return args.permit(self.attribute_names)
          end

        end #End of class
      end
    end
  end
end


#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    #
    # Image model class to interact with category db table
    #
    class Image < ActiveRecord::Base

      has_attached_file :image_local
      validates_attachment_content_type :image_local, content_type: /\Aimage\/.*\Z/
      validates_attachment :image_local, content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }

      has_attached_file :image_s3, styles: S3_IMAGE_STYLES, :storage => :s3,
                                   :s3_credentials => {
                                    :access_key_id => APP_CONFIG['config']['AWS_ACCESS_KEY_ID'],
                                    :secret_access_key => APP_CONFIG['config']['AWS_SECRET_ACCESS_KEY']
                                   },
                                   :bucket => APP_CONFIG['config']['S3_BUCKET'],
                                   :url => ':s3_domain_url',
                                   :s3_host_name => APP_CONFIG['config']['S3_HOST_NAME'],
                                   :path => '/:attachment/:id_partition/:style_:filename',
                                   :convert_options => S3_CONVERT_OPTIONS

      validates_attachment_content_type :image_s3, content_type: /\Aimage\/.*\Z/
      validates_attachment :image_s3, content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }

      IMAGE_MODEL_STATES = ImageServiceModule::V1::ModelStates::ImageStates

      #
      # Workflow to define states of the Seller brand pack
      #
      # Initial State => Inactive
      #
      # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :active, IMAGE_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :inactive, IMAGE_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, IMAGE_MODEL_STATES::DELETED
      end
    end
  end
end

module CreditsModule
  module V1
    class AmpleCredit < ActiveRecord::Base

      # Credits belongs to user
      belongs_to :user, class_name: 'UsersModule::V1::User'

      validates :user, presence: true

      
    end
  end
end

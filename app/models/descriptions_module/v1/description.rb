module DescriptionsModule
  module V1
    class Description < ActiveRecord::Base

      CACHE_CLIENT = CacheModule::V1::CacheClient

      TTL = 7 * 24 * 60 * 60 # in seconds -> 7 days

      DESCRIPTION_HELPER = DescriptionsModule::V1::DescriptionHelper
      
      belongs_to :description_of, polymorphic: true

      #
      # Category has a self join to it's sub mpsp_category
      #
      belongs_to :parent, class_name: 'DescriptionsModule::V1::Description'
      has_many :childs, class_name: 'DescriptionsModule::V1::Description', foreign_key: 'parent_id', dependent: :destroy


      after_save :update_cache
      after_destroy :delete_from_cache


      # 
      # This function removes the description from cache on destroy
      #
      def delete_from_cache
        if self.parent_id.nil?
          CACHE_CLIENT.remove({key: self.id}, self.class.name)
        else
          self.parent.update_cache
        end
      end

      #
      # This function update chache of the root description
      #
      def update_cache
        if self.parent_id.nil?
          description_tree = DESCRIPTION_HELPER.get_description_tree(self)
          CACHE_CLIENT.set({key: self.id, value: description_tree}, self.class.name, TTL)
        else
          # if this is not root description then it call its parent description
          self.parent.update_cache
        end
      end

      # 
      # This function overrides model function if get_hash == true
      # else call corresponding super function
      #
      # @param get_hash = false [Boolean] To enforce loading data from cache
      # 
      # @return [ActiveModelArray] if get_hash == false
      # @return [Hash] if it is a root node and get_hash == true
      #
      def childs(get_hash = false)

        return super if get_hash == false

        return {} if self.parent.present?

        description_tree = CACHE_CLIENT.get({key: self.id}, self.class.name)

        if description_tree.nil?
          description_tree = DESCRIPTION_HELPER.get_description_tree(self)
          CACHE_CLIENT.set({key: self.id, value: description_tree}, self.class.name, TTL)
        end

        return description_tree
      end

      # 
      # It chekcs if description is root or child
      # 
      # @return [Boolean] true if parent_id is nil else false
      #
      def root?
        return true if self.parent_id.nil?
        return false
      end
    end
  end
end

module LoggingModule
  module V1
    class ActivityLog
      #Making it mongoid document
      include Mongoid::Document
      #Saving timeStamp with each log
      include Mongoid::Timestamps
      
      #concatination controller :: action name 
      field :api_name, type: String
      
      #unique property of user
      field :phone_number, type: String

      #ip of client
      field :request_ip, type: String

      #request url
      field :request_url, type: String

      #get parameters of request
      field :request_query_string, type: String

      #error in response for the corresponding request
      field :response_error, type: String

      #response status
      field :response_status, type: String

      #time to complete the request in seconds
      field :time, type: Float

      # device id of the app
      field :device_id, type: String

      # version of the app
      field :app_version, type: String

      # platform type. ex: Android, IOS, etc.
      field :platform_type, type: String

      field :api_spawn_time, type: DateTime

      #indexing created_at as we querying and ordering on this field only
      index "created_at" => 1

      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      def self.activities_in_last_x_time_with_api_name_by_order(options)
      
        raise  CUSTOM_ERROR_UTIL::InsufficientDataError.new if options.nil? || options[:time].nil? || options[:api_name].nil? || options[:order].nil?

        self.where({created_at: (Time.zone.now - options[:time].to_i)..Time.zone.now,
                  phone_number: options[:phone_number], api_name: options[:api_name]}).
                  order("created_at " + options[:order])
      end
    end
  end
end

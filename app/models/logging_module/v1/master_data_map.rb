module LoggingModule
  module V1
    class MasterDataMap
      #Making it mongoid document
      include Mongoid::Document
      #Saving timeStamp with each log
      include Mongoid::Timestamps

      field :distributor_id, type: String

      field :cash_and_carry_id, type: String

      field :inventory_id, type: String
      
      field :warehouse_id, type: String    

      field :seller_id, type: String

      field :company_id, type: String

      field :brand_id, type: String

      field :sub_brand_id, type: String

      field :product_id, type: String
      
      field :department_id, type: String
      
      field :category_id, type: String    

      field :sub_category_id, type: String

      field :brand_pack_id, type: String

      field :inventory_brand_pack_id, type: String

      field :warehouse_brand_pack_id, type: String

      field :seller_brand_pack_id, type: String

      field :marketplace_brand_pack_id, type: String

      field :marketplace_selling_pack_id, type: String

      #indexing created_at as we querying and ordering on this field only
      index "created_at" => 1
    end
  end
end

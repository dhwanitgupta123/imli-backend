module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # Custom repository class for order-index
    #
    class OrderRepository
      include Elasticsearch::Persistence::Repository

      def initialize(options = {})
        # throws Faraday::ConnectionFailed on timeout
        transport_options = options[:transport_options] || { request: { timeout: 5 } }
        index 'order-index' # index name, used by elasticsearch to hash index uniquely
        client Elasticsearch::Client.new url: options[:url], log: options[:log], transport_options: transport_options
      end

      klass DataAggregationModule::V1::OrderSearchModel # Json result transform to klass

      settings number_of_shards: 1 do
        mapping do
          indexes :display_name, analyzer: 'keyword'
          indexes :display_pack_size, analyzer: 'keyword'
          indexes :category, analyzer: 'keyword'
          indexes :sub_category, analyzer: 'keyword'
          indexes :name, analyzer: 'keyword'
          indexes :department, analyzer: 'keyword'
          indexes :sku, analyzer: 'keyword'
          indexes :brand, analyzer: 'keyword'
          indexes :sub_brand, analyzer: 'keyword'
          indexes :company, analyzer: 'keyword' 
          indexes :order_id, analyzer: 'keyword'
          indexes :brand_codes, analyzer: 'keyword'
          indexes :app_version, analyzer: 'keyword'
          indexes :platform_type, analyzer: 'keyword'
          indexes :order_user_tag, analyzer: 'keyword'
          indexes :brand_pack_quantity, analyzer: 'keyword'
        end
      end

      #
      # serializing document klass(order) to hash
      #
      # @param document [klass] klass object
      #
      # @return [hash] transformed hash
      #
      def serialize(document)
        hash = document.to_hash.clone
        hash.to_hash
      end

      #
      # deserialize hash to klass type
      #
      # @param document [hash]
      #
      # @return [klass] Object of type klass with attributes = hash
      #
      def deserialize(document)
        hash = document['_source']
        klass.new hash
      end
    end
  end
end

module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # Custom repository class for order-index
    #
    class PaymentRepository
      include Elasticsearch::Persistence::Repository

      def initialize(options = {})
        # throws Faraday::ConnectionFailed on timeout
        transport_options = options[:transport_options] || { request: { timeout: 5 } }
        index 'payment-index' # index name, used by elasticsearch to hash index uniquely
        client Elasticsearch::Client.new url: options[:url], log: options[:log], transport_options: transport_options
      end

      klass DataAggregationModule::V1::PaymentSearchModel # Json result transform to klass

      settings number_of_shards: 1 do
        mapping do
          indexes :payment_status, analyzer: 'keyword'
          indexes :payment_mode, analyzer: 'keyword'
          indexes :name, analyzer: 'keyword'
          indexes :order_id, analyzer: 'keyword' 
        end
      end

      #
      # serializing document klass(order) to hash
      #
      # @param document [klass] klass object
      #
      # @return [hash] transformed hash
      #
      def serialize(document)
        hash = document.to_hash.clone
        hash.to_hash
      end

      #
      # deserialize hash to klass type
      #
      # @param document [hash]
      #
      # @return [klass] Object of type klass with attributes = hash
      #
      def deserialize(document)
        hash = document['_source']
        klass.new hash
      end
    end
  end
end

module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # Custom repository class for order-index
    #
    class ProductRepository
      include Elasticsearch::Persistence::Repository

      def initialize(options = {})
        # throws Faraday::ConnectionFailed on timeout
        transport_options = options[:transport_options] || { request: { timeout: 5 } }
        index 'product-index' # index name, used by elasticsearch to hash index uniquely
        client Elasticsearch::Client.new url: options[:url], log: options[:log], transport_options: transport_options
      end

      klass DataAggregationModule::V1::ProductSearchModel # Json result transform to klass

      settings number_of_shards: 1 do
        mapping do
          indexes :display_name, analyzer: 'keyword'
          indexes :category, analyzer: 'keyword'
          indexes :sub_category, analyzer: 'keyword'
          indexes :department, analyzer: 'keyword'
          indexes :display_pack_size, analyzer: 'keyword'
          indexes :sku, analyzer: 'keyword'
          indexes :company, analyzer: 'keyword'
          indexes :sku_size_and_units, analyzer: 'keyword'
          indexes :article_codes, analyzer: 'keyword'
          indexes :brand, analyzer: 'keyword'
          indexes :sub_brand, analyzer: 'keyword'
          indexes :primary_tags, analyzer: 'keyword'
          indexes :secondary_tags, analyzer: 'keyword'
          indexes :brand_pack_mrp, analyzer: 'keyword'
          indexes :imli_pack_size, analyzer: 'keyword'
          indexes :brand_pack_code, analyzer: 'keyword'
        end
      end

      #
      # serializing document klass(order) to hash
      #
      # @param document [klass] klass object
      #
      # @return [hash] transformed hash
      #
      def serialize(document)
        hash = document.to_hash.clone
        hash.to_hash
      end

      #
      # deserialize hash to klass type
      #
      # @param document [hash]
      #
      # @return [klass] Object of type klass with attributes = hash
      #
      def deserialize(document)
        hash = document['_source']
        klass.new hash
      end
    end
  end
end

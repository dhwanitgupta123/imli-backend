module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # This class is responsible to aggregate data
    #
    class ProductSearchModel
      attr_reader :attributes

      def initialize(attributes = {})
        @attributes = attributes
      end

      def to_hash
        @attributes
      end
    end
  end
end

module TransactionsModule
  module V1
    class Transaction
      #Making it mongoid document
      include Mongoid::Document
      #Saving timeStamp with each log
      include Mongoid::Timestamps
      
      field :payment_id, type: Integer
      
      field :payment_type, type: Integer

      field :amount, type: Float

      field :transaction_data, type: Hash

      field :signature, type: String

      field :currency, type: String

      field :status, type: Integer

      field :user_id, type: Integer

      field :user_email_id, type: String

      field :gateway, type: Integer

    end
  end
end

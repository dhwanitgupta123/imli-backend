module UsersModule
  module V1
    #
    # Membership API controller class to route Membership related API calls
    # 
    # @author [kartik]
    #
    class MembershipsApiController < BaseModule::V1::ApplicationController
     
      MEMBERSHIP_SERVICE = UsersModule::V1::MembershipService

      # 
      # initialize required classes
      # 
      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
      end

      ##################################################################################
      #                           subscribe_to_membership API                          #
      ##################################################################################
      
      # 
      # To subscribe a user to a membership
      # 
      # Request::
      #   * plan: [hash] plan_id is required
      #   * session_token: a valid session_token is must
      # 
      # @return [response] sends response to user with address hash
      # 
      _LogActivity_
      def subscribe_to_membership
        deciding_params = @application_helper.get_deciding_params(params)
        membership_service = MEMBERSHIP_SERVICE.new(deciding_params)
        response = membership_service.subscribe_to_membership(subscribe_to_membership_params[:memberships])
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :memberships_api, 'UsersModule APIs'
      swagger_api :subscribe_to_membership do
        summary 'It subscribes a User to a plan via membership'
        notes 'It subscribes a User to a plan via membership'
        param :body, :subscribe_to_membership_request, :subscribe_to_membership_request, :required, 'subscribe to membership request'
        response :unauthorized, 'User not having correct Role to access the Page'
        response :bad_request, 'Parameters missing/invalid parameters'
        response :forbidden, 'When session token is not provided'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :subscribe_to_membership_request do
        description 'Request for subscribing user to a membership plan'
        property :user, :user_membership_hash, :required, 'membership hash with plan details'
        property :session_token, :string, :required, 'session token required for identifying a valid session'
      end
      swagger_model :user_membership_hash do
        description 'Subscribing plan details'
        property :memberships, :array, :required, 'hash of membership request', {'items' => { 'type' => :membership_hash } }
      end
      swagger_model :membership_hash do
        description 'hash of membership request'
        property :plan, :plan_hash, :required, 'Plan hash'
      end
      swagger_model :plan_hash do
        description 'hash of plan'
        property :id, :string, :required, 'ID of plan user wants to subscribe to'
      end

      ##################################################################################
      #                                payment_initiate API                            #
      ##################################################################################
      
      # 
      # To initiate payment for membership subscription
      # 
      # Request::
      #   * plan: [hash] plan_id is required
      #   * payment mode: is required
      #   * session_token: a valid session_token is must
      # 
      # @return [response] sends response to user with address hash
      # 
      _LogActivity_
      def payment_initiate
        deciding_params = @application_helper.get_deciding_params(params)
        membership_service = MEMBERSHIP_SERVICE.new(deciding_params)
        response = membership_service.payment_initiate(payment_initiate_params[:memberships])
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :memberships_api, 'UsersModule APIs'
      swagger_api :payment_initiate do
        summary 'It initiates membership payment'
        notes 'It initiates membership payment'
        param :body, :membership_payment_request, :membership_payment_request, :required, 'payment_initiate request'
        response :unauthorized, 'User not having correct Role to access the Page'
        response :bad_request, 'Parameters missing/invalid parameters'
        response :forbidden, 'When session token is not provided'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :membership_payment_request do
        description 'Request for subscribing user to a membership plan'
        property :user, :membership_payment_hash, :required, 'membership hash with plan details'
        property :session_token, :string, :required, 'session token required for identifying a valid session'
      end
      swagger_model :membership_payment_hash do
        description 'Subscribing plan details'
        property :memberships, :array, :required, 'hash of membership request', {'items' => { 'type' => :membership_details_hash } }
      end
      swagger_model :membership_details_hash do
        description 'hash of membership request'
        property :payment, :payment_hash, :required, 'payment details'
      end
      swagger_model :payment_hash do
        description 'contains the details of payment mode'
        property :mode, :integer, :required, 'Payment mode for membership payment'
      end


      ##################################################################################
      #                                 payment_success API                            #
      ##################################################################################
      
      # 
      # API call when payment is sucess & to activate the membership
      # 
      # Request::
      #   * payment id
      # 
      # @return [response] sends response to user with address hash
      # 
      _LogActivity_
      def payment_success
        deciding_params = @application_helper.get_deciding_params(params)
        membership_service = MEMBERSHIP_SERVICE.new(deciding_params)
        response = membership_service.payment_success(payment_success_params[:memberships])
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :memberships_api, 'UsersModule APIs'
      swagger_api :payment_success do
        summary 'To update membership when payment success API is called'
        notes 'It activates the payment pending membership of user & force expires the active membership
               if present.'
        param :body, :payment_success_request, :payment_success_request, :required, 'payment_success request'
        response :unauthorized, 'User not having correct Role to access the Page'
        response :bad_request, 'Parameters missing/invalid parameters'
        response :forbidden, 'When session token is not provided'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :payment_success_request do
        description 'Request for subscribing user to a membership plan'
        property :user, :membership_payment_success_hash, :required, 'membership hash with payment details'
        property :session_token, :string, :required, 'session token required for identifying a valid session'
      end
      swagger_model :membership_payment_success_hash do
        description 'Subscribing plan details'
        property :memberships, :array, :required, 'hash of membership request', {'items' => { 'type' => :payment_success_hash } }
      end
      swagger_model :payment_success_hash do
        description 'hash of payment'
        property :payment, :payment_id_hash, :required, 'payment id of successful payment'
      end
      swagger_model :payment_id_hash do
        description 'ID of payment of which '
        property :id, :integer, :required, 'Payment ID'
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      # Never trust parameters from the scary internet, only allow the white list through.
      def subscribe_to_membership_params
       params.require(:user).permit(memberships: [plan: [:id]])
      end

      def payment_initiate_params
        params.require(:user).permit(memberships: [payment: [:mode]])
      end

      def payment_success_params
        params.require(:user).permit(memberships: [payment: [:id]])
      end
    end
  end
end

module UsersModule
  module V1
    class UsersController < BaseModule::V1::ApplicationController

      # 
      # initialize UserService Class User Model Class
      # 
      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
        @user_service = UsersModule::V1::UserService
      end

      ##################################################################################
      #                           change_state API                                    #
      ##################################################################################
      # PUT users/state/:id
      #
      # API to change state of user
      #
      # Request::
      #    * session_token: required for authentication
      #    * user_id user whose state is to be changed
      #    * event StateEvent which is to be triggered
      #
      # Response::
      #    * 200 ok: if user state changed successfully
      #
      _LogActivity_
      def change_state
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.change_state({
          id: params[:id],
          user: get_event_from_user_params
          })
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :change_state do
        summary "It changes the state of user"
        notes "It accepts event inside user hash, and as per the event trigger an action over user requested
        It also validates whether user exists and is already in required state"
        param :path, :id, :integer, :required, "user id whose state is to be changed"
        param :body, :user_change_state_request, :user_change_state_request, :required, "user change state request"
        response :not_found, "user not found"
        response :forbidden, "invalid authentication token"
        response :bad_request, "insufficient or invalid arguments passed in request params"
        response :precondition_required, "if user is not at valid state for which transition is requested"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model 
      #       to create complex swagger_model
      swagger_model :user_change_state_request do
        description "User required params for the user_change_state request"
        property :user, :user_hash_for_change_state_request, :required, "user hash with event"
        property :session_token, :string, :required, "session_token to authorize user access"
      end
      swagger_model :user_hash_for_change_state_request do
        description "User hash for the event request"
        property :event, :integer, :required, "User state event to be triggered"
      end

      ##################################################################################
      #                                 add_role_to_user API                           #
      ##################################################################################
      #
      # API to add role to user
      #
      # Request::
      #    * user: hash containing user_id
      #    * role: hash containing role_id
      # Response::
      #    * 200 ok: if role successfully added
      #    * 400 : if invalid or insufficient
      #
      _LogActivity_
      def add_role_to_user
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.add_role_to_user({
          user_params: get_user_phone_number_from_post_params,
          role_params: get_role_id_from_post_params
          })
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, 'UsersModule APIs'
      swagger_api :add_role_to_user do
        summary 'It adds the specified role to the user'
        notes 'It takes role id in role params and user phone number inside user params. It validates
        and assigns the role to passed user'
        param :body, :add_role_to_user_request, :add_role_to_user_request, :required, 'add role to user request'
        response :bad_request, 'Parameters missing/wrong parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :add_role_to_user_request do
        description 'Request for assigning role to a user'
        property :user, :user_hash_for_add_role, :required, 'user hash with phone number as parameters'
        property :role, :role_hash_for_add_role, :required, 'role hash with role id as parameters'
        property :session_token, :string, :required, 'session token of the requester'
      end

      swagger_model :user_hash_for_add_role do
        description 'User hash with related details of user'
        property :phone_number, :string, :required, 'phone number in which role to be added'
      end

      swagger_model :role_hash_for_add_role do
        description 'Role hash with related details of role'
        property :role_id, :integer, :required, 'role id of the role which is to be assigned'
      end

      ##################################################################################
      #                              remove_role_to_user API                           #
      ##################################################################################
      #
      # API to remove role from user
      #
      # Request::
      #    * user: hash containing user_id
      #    * role: hash containing role_id
      # Response::
      #    * 200 ok: if role successfully added
      #    * 400 : if invalid or insufficient
      #
      _LogActivity_
      def remove_role_from_user
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.remove_role_from_user({
          user_params: get_user_id_from_post_params,
          role_params: get_role_id_from_post_params
          })
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, 'UsersModule APIs'
      swagger_api :remove_role_from_user do
        summary 'It removes the specified role from the user'
        notes 'It takes role id in role params and user id inside user params. It validates
        and removes the role from the passed user'
        param :body, :remove_role_from_user_request, :remove_role_from_user_request,
              :required, 'remove role from user request'
        response :bad_request, 'Parameters missing/wrong parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :remove_role_from_user_request do
        description 'Request for removing role from the user'
        property :user, :user_hash_for_remove_role, :required, 'user hash with user id as parameters'
        property :role, :role_hash_for_remove_role, :required, 'role hash with role id as parameters'
        property :session_token, :string, :required, 'session token of the requester'
      end

      swagger_model :user_hash_for_remove_role do
        description 'User hash with related details of user'
        property :user_id, :integer, :required, 'user id from which role to be removed'
      end

      swagger_model :role_hash_for_remove_role do
        description 'Role hash with related details of role'
        property :role_id, :integer, :required, 'role id of the role which is to be removed'
      end

      ##################################################################################
      #                                 get_all_users API                              #
      ##################################################################################
      #
      # API to get all USERs
      #
      # Request::
      #    * role: hash containing role_name(String) and permissions(Array of Integers)
      #
      # Response::
      #    * 200 ok: with users array. Empty if no users
      #
      _LogActivity_
      def get_all_users
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.get_all_users(pagination_params)
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, 'CommonModule APIs'
      swagger_api :get_all_users do
        summary 'It creates new user'
        notes 'It takes user session token and various pagination params. It validates and
        creates fetches all users. User should have specific permissions to access this API'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all users'
        param :query, :per_page, :integer, :optional, 'how many users to display in a page'
        param :query, :state, :array, :optional, 'to fetch users based on their current state', { 'items': { 'type': 'integer' } }
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'id' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'if request params are incorrect or api fails to create marketplace_brand_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                                 refill_limit API                               #
      ##################################################################################
      #
      # API to refill referral limit of User
      #
      # Request::
      #    * referral_limit: referral limit to be increased by
      #
      # Response::
      #    * 200 ok: success response
      #
      _LogActivity_
      def refill_referral_limit
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.refill_referral_limit(refill_limit_params)
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, 'CommonModule APIs'
      swagger_api :refill_referral_limit do
        summary 'It refills the user referral_limit'
        notes 'It takes referral_limit to be increased by and adds to the exisiting limit of the user'
        param :path, :id, :integer, :required, 'id of user whose referral_limit has to be increased'
        param :body, :refill_limit, :refill_limit, :required, 'request model for increasing referral limit'
        response :bad_request, 'if request params are incorrect or api fails to create marketplace_brand_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :refill_limit do
        description 'model has limit to be increased by of user referral '
        property :session_token, :string, :required, 'session token to authenticate user'
        property :referral_limit, :integer, :required, 'limit of referral code to be increased by'
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # white list parameters for resend_otp API.
      # 
      def get_user_phone_number_from_post_params
        params.require(:user).permit(:phone_number)
      end

      #
      # filter user_id from user hash
      #
      def get_user_id_from_post_params
        params.require(:user).permit(:user_id)
      end

      #
      # filter role_id from role hash
      #
      def get_role_id_from_post_params
        params.require(:role).permit(:role_id)
      end

      #
      # filter event from user hash
      #
      def get_event_from_user_params
        params.require(:user).permit(:event)
      end

      #
      # White list params for get_all_users API
      #
      def pagination_params
        params.permit(:page_no, :per_page, :sort_by, :order, :state)
      end

      def refill_limit_params
        params.permit(:referral_limit, :id)
      end
    end
  end
end

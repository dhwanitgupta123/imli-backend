module UsersModule
  module V1
    class AccessControlController < ActionController::Base
      extend AnnotationsModule::V1::ActivityLoggingAnnotations

      # 
      # Initializing access control context
      # 
      def initialize_access_control_controller(controller)
        self.extend AnnotationsModule::V1::ActivityLoggingAnnotations

        @access_control_service_class = UsersModule::V1::AccessControlService
        @access_control_helper = UsersModule::V1::AccessControlHelper
        @shield_service = SettingsModule::V1::ShieldService
        session_token = get_session_token();

        @access_control_service = @access_control_service_class.new({
            session_token: session_token,
            controller:    controller.controller_name,
            action:        controller.action_name,
            user:          params[:user]
          })

        pre_handle_acitivty_log_annotation(controller)
      end

      #
      #This function to validate the user corresponding to each request
      #
      def before_actions(controller)
        initialize_access_control_controller(controller)
  
        return false if !(authorized_request?)

        if @shield_service.get_feature_value('ALLOW_PERMISSION_VALIDATION') == '1'
         return false if !apply_validations
        end

        #Setting as global variable so that it can be used inside the controller
        # in other functions
        @phone_number = @access_control_service.get_phone_number

        return false if user_blocked?

        return true
      end

      #
      # This functions will perform after tasks like request logging
      #  user analyzing in async fashion
      # 
      # @param controller [Object] Controller Object
      # 
      def after_actions(controller)
        @access_control_service.analyze_user_activity if @phone_number.present?
        destroy_user_session
        post_handle_activity_log_annotation(controller)
      end

      # 
      # Return the error response if pre validation failed
      # else it will return true
      # 
      # @return [Boolean] true if all validations pass else false
      #
      def apply_validations

        validation_response = @access_control_service.apply_validations

        if validation_response[:ok] == false
          send_response(validation_response[:data])
          return false
        else
          return true
        end
      end

      # 
      # Return the error response if user is blocked
      # else it will return true
      # 
      # @return [Boolean] true if user not blocked else response
      # 
      def user_blocked?
        
        return false if @phone_number.blank?

        is_blocked = @access_control_service.is_user_blocked?

        if(is_blocked[:ok] == true)
          send_response(is_blocked[:data])
          return true
        else
          return false
        end
      end


      # 
      # Authorize user identity. Pass the general flow if authorized
      # else redirect or return appropriate http response codes
      # 
      def authorized_request?

        has_access = @access_control_service.has_access?

        if has_access[:ok]
          set_user_session(has_access)
          return true
        else
          send_response(has_access[:data])
        end

        return false
      end

      # 
      # Set user session 
      # 
      # @param data [hash] consisting of user information
      # 
      def set_user_session(data)
        USER_SESSION[:user_id] = data[:user_id]
        USER_SESSION[:phone_number] = data[:phone_number]
        store_app_details
      end

      #
      # Destroy values of user session
      #
      def destroy_user_session
        USER_SESSION[:user_id] = nil
        USER_SESSION[:phone_number] = nil
        USER_SESSION[:app_version] = nil
        USER_SESSION[:platform_type] = nil
      end

      #
      # Fetch session token from request
      # Preference:
      #  - body of request
      #  - cookies
      #  - header
      #
      # @return [String] [Session token came in request]
      #
      def get_session_token
        session_token = ""
        if ( params.present? && params[:session_token].present? )
          session_token = params[:session_token]
        elsif ( request.present? && request.cookies.present? && request.cookies['imli_session_token'].present? )
          session_token = request.cookies['imli_session_token']
        elsif ( request.present? && request.headers.present? && request.headers['Imli-Session-Token'].present? )
          session_token = request.headers['Imli-Session-Token']
        end
        return session_token    
      end

      #
      # Store App information in USER_SESSION (global context)
      #
      def store_app_details
        app_details = @access_control_helper.fetch_app_details_from_request_headers(request)

        USER_SESSION[:app_version] = app_details[:app_version]
        USER_SESSION[:platform_type] = app_details[:platform_type]
      end

    end
  end
end

module UsersModule
  module V1
    class AddressesApiController < BaseModule::V1::ApplicationController

      # 
      # initialize UserService Class User Model Class
      # 
      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
        @user_service = UsersModule::V1::UserService
      end

      ##################################################################################
      #                                get_all_addresses_of_user API                   #
      ##################################################################################
      
      # 
      # To get all addresses of a user
      # 
      # Request::
      #   * session_token: a valid session_token is must
      # 
      # @return [response] sends response to user with addresses hash
      #
      _LogActivity_ 
      def get_all_addresses_of_user
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.get_all_addresses_of_user
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, 'UsersModule APIs'
      swagger_api :get_all_addresses_of_user do
        param :query, :session_token, :string, :required, 'to authenticate user'
        summary 'It returns all the addresses of the user'
        notes 'API returns the addresses of current user'
        response :unauthorized, "user not authenticated"
      end

      ##################################################################################
      #                                add_address API                                 #
      ##################################################################################
      
      # 
      # To add address of a user
      # 
      # Request::
      #   * address: [hash] nickname, address_line1, address_line2, landmark, area_id
      #     * address_line1, address_line2, area_id is must
      #   * session_token: a valid session_token is must
      # 
      # @return [response] sends response to user with address hash
      #
      _LogActivity_
      def add_address
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.add_address(address_params)
        send_response(response)
      end


      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :add_address do
        summary "It adds User address"
        notes "add address API adds address of the User. address_line1, address_line2, area_id 
        are compulsory. This API calls the User Service function add_address to
        add address of the user."
        param :body, :add_address_request, :add_address_request, :required, "add address request"
        response :unauthorized, "User not having correct Role to access the Page"
        response :bad_request, "Parameters missing/invalid parameters"
        response :internal_server_error, "run time error happened"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :add_address_request do
        description "Request for adding address of a user"
        property :user, :add_address_params, :required, "user address params with address hash"
        property :session_token, :string, :required, "session token required for identifying a valid session"
      end
      swagger_model :add_address_params do
        description "params for adding address of a user"
        property :addresses, :array, :required, "address hash with address details", { 'items': { 'type': :address_hash } }
      end

      ##################################################################################
      #                                update_address API                                 #
      ##################################################################################
      
      # 
      # To update existing address of a user
      # 
      # Request::
      #   * address: [hash] nickname, address_line1, address_line2, landmark, area_id, address_id
      #     * area_id, address_id is must
      #   * session_token: a valid session_token is must
      # 
      # @return [response] sends response to user with address hash
      #
      _LogActivity_ 
      def update_address
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.update_address(address_params.merge({ id: params[:id] }))
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :update_address do
        summary "It updates User address"
        notes "update address API updates address of the User. area_id, address_id 
        are compulsory. This API calls the User Service function update_address to
        update address of the user. Area_is is not required, keeping it required 
        field for swagger to work"
        param :path, :id, :integer, :required, 'Which address to update'
        param :body, :update_address_request, :update_address_request, :required, "update address request"
        response :unauthorized, "User not having correct Role to access the Page"
        response :bad_request, "Parameters missing/invalid parameters"
        response :internal_server_error, "run time error happened"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :update_address_request do
        description "Request for updating address of a user"
        property :user, :update_address_params, :required, "user address params with address details"
        property :session_token, :string, :required, "session token required for identifying a valid session"
      end
      swagger_model :update_address_params do
        description "params for updating address of a user"
        property :addresses, :array, :required, "address hash with address details", { 'items': { 'type': :address_hash } }
      end
      swagger_model :address_hash do
        description "Address hash with address related details of user"
        property :recipient_name, :string, :required, 'Name of the order recipient'
        property :nickname, :string, :optional, "Address nickname"
        property :address_line1, :string, :optional, "User Flat & Building name"
        property :address_line2, :string, :optional, "User Locality Name"
        property :landmark, :string, :optional, "Address landmark"
        property :area_id, :string, :required, "Area id of the area, fetched from pincode"
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # white list parameters for address request.
      # Filter out: address_id, nickname, address_line1, address_line2, landmark, area_id
      #
      def address_params
        params.require(:user).permit(:addresses => [:nickname, :address_line1, 
          :address_line2, :landmark, :area_id, :recipient_name])
      end
    end
  end
end

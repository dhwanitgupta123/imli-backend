module UsersModule
  module V1
    #
    # Membership API controller class to route Profile related API calls
    #
    class ProfilesController < BaseModule::V1::ApplicationController
     
      PROFILE_SERVICE = UsersModule::V1::ProfileService

      # 
      # initialize required classes
      # 
      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
      end

      ##################################################################################
      #                                    add_pincode API                             #
      ##################################################################################
      
      # 
      # To add pincode to the user's profile
      # 
      # Request::
      #   {
      #     user: {
      #       profile: {
      #         pincode: 'pincode'
      #       }
      #     }
      #   }
      #   * user: [hash] user hash is required contains pincode to be added
      #   * session_token: a valid session_token is must
      # 
      # Response::
      #   {
      #     payload: {
      #       message: 'success'
      #     }
      #   }
      # 
      # @return [response] sends response to user with address hash
      # 
      _LogActivity_
      def add_pincode
        deciding_params = @application_helper.get_deciding_params(params)
        profile_service = PROFILE_SERVICE.new(deciding_params)
        response = profile_service.add_pincode(add_pincode_params)
        send_response(response)
      end

      swagger_controller :profiles_controller, 'UsersModule APIs'
      swagger_api :add_pincode do
        summary 'It pincodes of user area in profile'
        notes 'It takes pincode in request verify it & stores in profile'
        param :body, :add_pincode_request, :add_pincode_request, :required, 'add_pincode request'
        response :unauthorized, 'User not having correct Role to access the Page'
        response :bad_request, 'Parameters missing/invalid parameters'
        response :forbidden, 'When session token is not provided'
      end

      swagger_model :add_pincode_request do
        description 'Request for addign pinode to user profile'
        property :user, :user_profile_hash, :required, 'User hash with profile'
        property :session_token, :string, :required, 'session token required for identifying a valid session'
      end
      swagger_model :user_profile_hash do
        description 'profile hash'
        property :profile, :profile_hash, :required, 'hash of profile request'
      end
      swagger_model :profile_hash do
        description 'hash of profile request'
        property :pincode, :string, :required, 'Pincode of area'
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      # Never trust parameters from the scary internet, only allow the white list through.
      def add_pincode_params
       params.require(:user).permit(profile: [:pincode])
      end
    end
  end
end

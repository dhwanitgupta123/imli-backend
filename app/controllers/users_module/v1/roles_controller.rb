module UsersModule
  module V1
    # Controller to handle all the API calls related to user roles
    class RolesController < BaseModule::V1::ApplicationController
      # Storing versioned classes and helpers in class level constants
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      ROLE_SERVICE = UsersModule::V1::RoleService
      ROLE_MODEL = UsersModule::V1::Role

      ##################################################################################
      #                                 create_role API                                #
      ##################################################################################
      #
      # API to create a new role
      #
      # Request::
      #    * role: hash containing role_name(String) and permissions(Array of Integers)
      #
      # Response::
      #    * 200 ok: if role successfully created
      #    * 400 : if invalid or insufficient
      #
      def create_role
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        role_service = ROLE_SERVICE.new(deciding_params)
        response = role_service.create_role(role_params)
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :roles, 'UsersModule APIs'
      swagger_api :create_role do
        summary 'It creates new role'
        notes 'It takes role name and permissions as an integer array. It validates
        and creates the role. Permissions array [1] shoule be assigned to all the users
        for giving them access to basic mobile APIs'
        param :body, :create_role_request, :create_role_request,
              :required, 'create role request'
        response :bad_request, 'Parameters missing/wrong parameters'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :create_role_request do
        description 'Request for creating a role'
        property :role, :role_hash_for_create_role, :required, 'role hash with role details'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :role_hash_for_create_role do
        description 'Role hash with all details related to role'
        property :parent_id, :integer, :required, 'id of parent role'
        property :role_name, :string, :required, 'role name of the role which is to be created'
        property :permissions, :array, :required, 'array of permissions id (integer)', { 'items': { 'type': 'integer' } }
      end

      ##################################################################################
      #                                 update_role API                                #
      ##################################################################################
      #
      # API to update an existing role ( PUT /roles/:role_id )
      #
      # Request::
      #    * role_id: integer to be passed in path with PUT request
      #    * role: hash containing role_name(String) and permissions(Array of Integers)
      #
      # Response::
      #    * 200 ok: if role successfully created
      #    * 400 : if invalid or insufficient
      #
      def update_role
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        role_service = ROLE_SERVICE.new(deciding_params)
        response = role_service.update_role({
          role_id: get_role_id_from_params,
          role_params: role_params
          })
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :roles, 'UsersModule APIs'
      swagger_api :update_role do
        summary 'It updates an existing role'
        notes 'It takes role name and permissions as an integer array. It validates the role id
        passed and updates the role.'
        param :path, :role_id, :integer, :optional, 'id of the role which is to be updated'
        param :body, :update_role_request, :update_role_request,
              :required, 'update role request'
        response :bad_request, 'Parameters missing/wrong parameters'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :update_role_request do
        description 'Request for updating a role'
        property :role, :role_hash_for_update_role, :required, 'role hash with role details'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :role_hash_for_update_role do
        description 'Role hash with all details related to role'
        property :role_name, :string, :required, 'role name of the role which is to be created'
        property :permissions, :array, :required, 'array of permissions id (integer)', { 'items': { 'type': 'integer' } }
      end

      ##################################################################################
      #                               get_user_role API                                #
      ##################################################################################
      #
      # API to get Role of a particular user
      #
      # Request::
      #    * session_token of the user
      #
      # Response::
      #    * 200 ok: if role associated with the user is found and returned
      #    * 401/403 : if no user associated with passed session token
      #
      def get_user_role
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        role_service = ROLE_SERVICE.new(deciding_params)
        response = role_service.get_user_role
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :roles, 'UsersModule APIs'
      swagger_api :get_user_role do
        summary 'It returns all the roles assigned to the requested user'
        notes 'It takes session token of the requester. It validates
        and returns all the roles specific to the requester/user. A user can not ask for
        any other users role'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        response :unauthorized, 'no user associated with passed session token'
      end

      ##################################################################################
      #                               get_roles_tree API                               #
      ##################################################################################
      #
      # API to get complete Roles tree under the requested user role
      #
      # Request::
      #    * session_token of the user
      #
      # Response::
      #    * 200 ok: if role associated with the user is found and its sub-tree is returned
      #    * 401/403 : if no user associated with passed session token
      #
      def get_roles_tree
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        role_service = ROLE_SERVICE.new(deciding_params)
        response = role_service.get_roles_tree
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :roles, 'UsersModule APIs'
      swagger_api :get_roles_tree do
        summary 'It returns complete roles tree under the requested user'
        notes 'It takes session token of the requester. It validates
        and returns complete roles tree, with root as roles associated with the requested user.
        A user can not ask for any other users roles-tree'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        response :unauthorized, 'no user associated with passed session token'
      end

      ##################################################################################
      #                          get_users_for_role API                                #
      ##################################################################################
      #
      # API to get all users associated with a particular role
      #
      # Request::
      #    * session_token of the user (for authorization)
      #    * role: Hash containing role_id (Integer)
      #
      # Response::
      #    * 200 ok: if role exists corresponding to passed id and all users with that role is returned
      #    * 400 : if insufficient or invalid (no role associated with that role_id) arguments are passed
      #
      def get_users_for_role
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        role_service = ROLE_SERVICE.new(deciding_params)
        response = role_service.get_users_for_role(get_role_id_from_params)
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :roles, 'UsersModule APIs'
      swagger_api :get_users_for_role do
        summary 'It get all the users who have given role'
        notes 'It takes role id whose users are to be found. It validates
        and returns all the users who have specified role'
        param :path, :role_id, :integer, :required, 'id of the role which is to be created'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        response :bad_request, 'Parameters missing/wrong parameters'
      end

      ##################################################################################
      #                          get_all_permissions API                               #
      ##################################################################################
      #
      # API to get all permissions available
      #
      # Request::
      #    * session_token of the user (for authorization)
      #
      # Response::
      #    * 200 ok: if able to fetch all available permissions
      #    * 500 : if unable to fetch the permissions
      #
      def get_all_permissions
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        role_service = ROLE_SERVICE.new(deciding_params)
        response = role_service.get_all_permissions
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :roles, 'UsersModule APIs'
      swagger_api :get_all_permissions do
        summary 'It get all available permissions'
        notes 'It go through the Database/configuration file and return all available/active
        permissions'
        param :query, :session_token, :string, :required, 'session token to authenticate user/requester'
        response :internal_server_error, "run time error when unable to fetch permissions"
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      # Never trust parameters from the scary internet, only allow the white list through.
      def role_params
       params.require(:role).permit(:role_name, :parent_id, :permissions => [])
      end

      #
      # Extract out role id from request params
      #
      def get_role_id_from_params
        params.require(:role_id)
      end
    end
  end
end

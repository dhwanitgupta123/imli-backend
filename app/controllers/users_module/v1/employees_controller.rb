module UsersModule
  module V1
    class EmployeesController < BaseModule::V1::ApplicationController

      # 
      # initialize Employee Controller Class
      # 
      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
        @employee_service = UsersModule::V1::EmployeeService
        @employee_model = UsersModule::V1::Employee
      end

      ##################################################################################
      #                                   register API                                 #
      ##################################################################################
      #
      # POST /employees/register
      #
      def register
        deciding_params = @application_helper.get_deciding_params(params)
        employee_service = @employee_service.new(deciding_params)
        response = employee_service.register({
          employee_params: employee_params,
          user_params: user_params
          })
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :employees, "UsersModule APIs"
      swagger_api :register do
        summary "It registers new Employee"
        notes "register API gets all the required information of the employee, calls the underlying
          User decide_register_login API to create a user for the employee, then it adds the email for 
          the employee and sets it to pending. It also sends a mail to Manager to approve the employee"
        param :body, :register_request, :register_request, :required, "register request"
        response :unauthorized, "Existing Employee/Customer, redirect to login page"
        response :bad_request, "Parameters missing/wrong parameters"
        response :internal_server_error, "run time error happened"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model 
      #       to create complex swagger_model
      swagger_model :register_request do
        description "Request for registering an employee"
        property :user, :user_hash, :required, "user hash with user details", { "items" => { "$ref" => "user_hash" } }
        property :employee, :employee_hash, :required, "user hash with user details", { "items" => { "$ref" => "employee_hash" } }
      end
      swagger_model :user_hash do
        description "User hash with user related details of employee"
        property :phone_number, :string, :required, "Employee phone_number"
        property :first_name, :string, :required, "Employee first_name"
        property :last_name, :string, :required, "Employee last_name"
        property :email_id, :string, :required, "Employee Email ID"
        property :password, :string, :required, "Employee password"
      end
      swagger_model :employee_hash do
        description "Employee hash with employee related details"
        property :designation, :string, :required, "Employee's designation"
        property :team, :string, :required, "Employee's team"
        property :manager_phone_number, :string, :required, "Employee's manager_phone_number"
      end

      ##################################################################################
      #                                     login API                                  #
      ##################################################################################
      #
      # POST /employees/login
      #
      def login
        deciding_params = @application_helper.get_deciding_params(params)
        employee_service = @employee_service.new(deciding_params)
        response = employee_service.login({
          employee_credentials: employee_credentials
          })
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :employees, "UsersModule APIs"
      swagger_api :login do
        summary "Employee Sign in API"
        notes "login API is used to allow employee to sign in into our system, underlying API called
               is Users Login API"
        param :body, :login_request, :login_request, :required, "login employee request"
        response :unauthorized, "user not found"
        response :bad_request, "wrong phone_number/password, or parameters missing"
        response :internal_server_error, "run time error happened"
        response :precondition_required, "you are still in pending state"
        response :not_found, "when provider type is not found for employee"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model 
      #       to create complex swagger_model
      swagger_model :login_request do
        description "Employee required params for the login request"
        property :user, :user_login_hash, :required, "user hash with phone_number & password", { "items" => { "$ref" => "user_login_hash" } }
      end
      swagger_model :user_login_hash do
        description "User hash for the login request"
        property :phone_number, :string, :required, "User Phone Number"
        property :password, :string, :optional, "password to verify"
      end

      ##################################################################################
      #                                add_profile API                                 #
      ##################################################################################
      
      # 
      # To add first_name, last_name and email_id to the employee profile
      # 
      # Request::
      #   * user_params: [hash] first_name, email_id is must
      #   * session_token: a valid session_token is must
      # 
      # @return [response] sends response to user with phone_number
      # 
      def add_profile
        deciding_params = @application_helper.get_deciding_params(params)
        employee_service = @employee_service.new(deciding_params)
        response = employee_service.add_profile({
          employee_params: employee_params,
          user_params: user_params
          })
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :employees, "UsersModule APIs"
      swagger_api :add_profile do
        summary "It adds Employee profile"
        notes "add profile API adds all the required information of the employee, and the information
        of his manager by searching if the manager exists as an employee. This is done using the
        provided Manager phone number. This API calls the UserService function add_user_profile to 
        add user details"
        param :body, :add_employee_profile_request, :add_employee_profile_request, :required, "add profile request"
        response :unauthorized, "User not having correct Role to access the Page"
        response :bad_request, "Parameters missing/invalid parameters"
        response :internal_server_error, "run time error happened"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model 
      #       to create complex swagger_model
      swagger_model :add_employee_profile_request do
        description "Request for adding profile of an employee"
        property :user, :add_user_hash, :required, "user hash with user details", { "items" => { "$ref" => "add_user_hash" } }
        property :employee, :add_employee_hash, :required, "user hash with user details", { "items" => { "$ref" => "employee_hash" } }
        property :session_token, :string, :required, "session token required for identifying a valid session"
      end
      swagger_model :add_user_hash do
        description "User hash with user related details of employee"
        property :first_name, :string, :required, "Employee first_name"
        property :last_name, :string, "Employee last_name"
        property :email_id, :string, :required, "Employee Email ID"
      end
      swagger_model :add_employee_hash do
        description "Employee hash with employee related details"
        property :designation, :string, :required, "Employee's designation"
        property :team, :string, :required, "Employee's team"
        property :manager_phone_number, :string, :required, "Employee's manager_phone_number"
      end

      ##################################################################################
      #                                      approve API                               #
      ##################################################################################
      #
      # GET /employees/approve
      #
      def approve
        deciding_params = @application_helper.get_deciding_params(params)
        employee_service = @employee_service.new(deciding_params)
        response = employee_service.approve({
          token: params[:token]
          })
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :employees, "UsersModule APIs"
      swagger_api :approve do
        summary "It approves the new employee in the company"
        notes "approve API to approve new employee in the company by activating the employee"
        param :query, :token, :string, :required, "employee verification token"
        response :bad_request, "not valid parameters"
        response :internal_server_error, "run time error happened"
        response :precondition_required, "you are still in inactive state"
      end

      #
      # POST /employees/logout
      #
      def logout
        # Call user service to delete session token
        
        # Return success response
      end

      
      ###############################
      #       Private Functions     #
      ###############################

      private

      # Never trust parameters from the scary internet, only allow the white list through.
      def employee_params
        params.require(:employee).permit(:designation, :team, :manager_phone_number)
      end

      def user_params
        params.require(:user).permit(:phone_number, :email_id, :first_name, :last_name, :password)
      end

      def employee_credentials
        params.require(:user).permit(:phone_number, :password)
      end

    end
  end
end
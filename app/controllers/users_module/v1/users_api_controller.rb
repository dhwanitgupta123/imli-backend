module UsersModule
  module V1
    class UsersApiController < BaseModule::V1::ApplicationController

      CHECK_USER_API = UsersModule::V1::CheckUserApi
      CREATE_PASSWORD_API = UsersModule::V1::CreatePasswordApi
      APPLY_REFERRAL_BENEFITS_API = UsersModule::V1::ApplyReferralBenefitsApi
      GET_AMPLE_CREDIT_OF_USER_API = UsersModule::V1::GetAmpleCreditOfUserApi

      #
      # initialize UserService Class User Model Class
      #
      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
        @user_service = UsersModule::V1::UserService
      end

      ##################################################################################
      #                             check_user API                                     #
      ##################################################################################
      
      # 
      # To check if given user is registered with us or not
      # 
      # Request::
      #   * user_params: [hash] phone_number is a must.
      # 
      # @return [response] sends response to user with boolean value, and status code
      #
      _LogActivity_
      def check_user
        deciding_params = @application_helper.get_deciding_params(params)
        check_user_api = CHECK_USER_API.new(deciding_params)
        check_user_request = {
          user: check_user_request_params
        }
        check_user_response = check_user_api.enact(check_user_request)
        send_response(check_user_response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :check_user do
        summary "It checks whether user is registered with us or not"
        notes "It checks whether the given phone number belongs to our registered user or not"
        param :body, :check_user_request, :check_user_request, :optional, "check_user request"
        response :bad_request, "not valid parameters"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model 
      #       to create complex swagger_model
      swagger_model :check_user_request do
        description "request schema for /check_user API"
        property :user, :user_hash_for_check_user, :required, "user details for check user request"
      end

      swagger_model :user_hash_for_check_user do
        description "user related params"
        property :phone_number, :string, :required, "User hash with Phone Number"
      end

      ##################################################################################
      #                          decide_register_login API                             #
      ##################################################################################
      
      # 
      # To decide if to register a new user or add new provider or update message provider
      # 
      # Request::
      #   * user_params: [hash] phone_number is a must & can have other user related values like first_name etc.
      #   * request_type: [integer] can have value 1,2,3,4 corresponding to SMS, Generic, FB, Google provider type
      # 
      # @return [response] sends response to user with phone_number, and status code
      #
      _LogActivity_
      def decide_register_login
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.decide_register_login(decide_register_login_params, request_type_param)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :decide_register_login do
        summary "It registers a new user or create a new provider type for user"
        notes "decide_register_login API. It first checks for the existence of user, if user
               exists request is redirected to login service otherwise it goes through
               register service. Based on request_type (SMS, GENERIC) provider service decides
               which provider type to create. If Request type is GENERIC & user is existing, 
               it throws existing user error"
        param :body, :user_request, :user_request, :optional, "decide_register_login request"
        param :body, :user_password_request, :user_password_request, :optional, "decide_register_login request with password"
        param :body, :user_register_forgot_password_request, :user_register_forgot_password_request, :optional, "decide_register_login request for forgot password"
        response :unauthorized, "you are already an existing user in generic provider"
        response :bad_request, "not valid parameters"
        response :internal_server_error, "run time error happened"
        response :too_many_requests, "You have reached the maximum limit"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model 
      #       to create complex swagger_model
      swagger_model :user_request do
        description "request schema for /take_my_phone_number API"
        property :user, :user_register_hash, :required, "decide_register_login request",{ "items" => { "$ref" => "user_register_hash" } }
        property :request_type, :integer, :required, "request_type: SMS: 1, Generic: 2"
      end

      swagger_model :user_password_request do
        description "request schema for /take_my_phone_number API"
        property :user, :user_password_hash, :required, "decide_register_login request",{ "items" => { "$ref" => "user_password_hash" } }
        property :request_type, :integer, :required, "request_type: SMS: 1, Generic: 2"
      end

      swagger_model :user_register_forgot_password_request do
        description "request schema for request_type FORGOT_PASSWORD"
        property :user, :user_register_hash, :required, "decide_register_login request"
        property :request_type, :integer, :required, "request_type: FORGOT_PASSWORD: 3", { 'default_value': 3 }
      end

      swagger_model :user_register_hash do
        description "user related params"
        property :phone_number, :string, :required, "User hash with Phone Number"
      end

      swagger_model :user_password_hash do
        description "user related params"
        property :phone_number, :string, :required, "User hash with Phone Number"
        property :password, :string, :required, "User password"
      end

      ##################################################################################
      #                                     login API                                  #
      ##################################################################################
      
      # 
      # To login user with one time password
      # 
      # Request::
      #   * user_params: [hash] phone_number & OTP are must 
      # 
      # @return [response] sends response to user with phone_number, and session_token
      #
      _LogActivity_ 
      def login
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.login(login_params, request_type_param)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :login do
        summary "sign in API"
        notes "login API is used to allow user into our system, provider_service based on 
               existence of password or otp directs request to message or generic service 
               to validate password or otp"
        param :body, :user_login_otp_request, :user_login_otp_request, :optional, 
              "login with OTP API request.         
               NOTE: use only one body at a time, otherwise first request will be overwritten by second"
        param :body, :user_login_password_request, :user_login_password_request, :optional, "login with password API request"
        response :unauthorized, "User not found"
        response :bad_request, "wrong password or OTP"
        response :internal_server_error, "run time error happened"
        response :authentication_timeout, "your OTP has expired"
        response :not_found, "when provider type is not found through which user is trying to login"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise optional
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model 
      #       to create complex swagger_model
      swagger_model :user_login_otp_request do
        description "User required params for the login request"
        property :user, :user_login_otp_hash, :required, "user hash with phone_number & OTP/Password", { "items" => { "$ref" => "user_login_otp_hash" } }
        property :request_type, :integer, :required, "request_type: SMS: 1, Generic: 2"
      end
      swagger_model :user_login_otp_hash do
        description "User hash for the login request"
        property :phone_number, :string, :required, "User Phone Number"
        property :otp, :integer, :optional, "OTP to verify"
      end
      swagger_model :user_login_password_request do
        description "User required params for the login request"
        property :user, :user_login_password_hash, :required, "user hash with phone_number & OTP/Password", { "items" => { "$ref" => "user_login_password_hash" } }
        property :request_type, :integer, :required, "request_type: SMS: 1, Generic: 2"
      end
      swagger_model :user_login_password_hash do
        description "User hash for the login request"
        property :phone_number, :string, :required, "User Phone Number"
        property :password, :string, :optional, "password to verify"
      end
     
      ##################################################################################
      #                                 resend_otp API                                 #
      ##################################################################################
      
      # 
      # To resend OTP to user
      # 
      # Request::
      #   * user_params: [hash] phone_number is must 
      # 
      # @return [response] sends response to user with phone_number
      #
      _LogActivity_
      def resend_otp
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.resend_otp(get_user_phone_number_from_post_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :resend_otp do
        summary "It resends_otp to User"
        notes "When user re-sends request for OTP, resend_otp API checks the presence of user,
               if found it checks for the expiry of OTP, if expired generates new OTP & sends
               it to the user otherwise it increases the expiry and sends the same OTP to user."
        param :body, :user_resend_otp_request, :user_resend_otp_request, :required, "resend_otp request"
        response :unauthorized, "when user is not found"
        response :bad_request, "not valid parameters"
        response :internal_server_error, "run time error happened"
        response :too_many_requests, "You have reached the maximum limit"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model 
      #       to create complex swagger_model
      swagger_model :user_resend_otp_request do
        description "User required params for the resend_otp request"
        property :user, :user_resend_otp_hash, :required, "user hash with phone_number", { "items" => { "$ref" => "user_resend_otp_hash" } }
      end
      swagger_model :user_resend_otp_hash do
        description "User hash for the resend_otp request"
        property :phone_number, :string, :required, "User Phone Number"
      end

      ##################################################################################
      #                       activate_with_referral_code API                          #
      ##################################################################################
      
      # 
      # To activate user when referral code is submitted
      # 
      # Request::
      #   * user_params: [hash] phone_number is must 
      # 
      # @return [response] sends response to user with user state: active
      #
      _LogActivity_ 
      def activate_with_referral_code
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.activate_with_referral_code(activate_with_referral_code_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :activate_with_referral_code do
        summary "It activates the user state"
        notes "activate_with_referral_code API activates the inactive User if in request we get correct
        referral code otherwise throws precondition_required error"
        param :body, :user_activate_request, :user_activate_request, :required, "activate user request"
        response :unauthorized, "user not found"
        response :bad_request, "not valid parameters"
        response :internal_server_error, "run time error happened"
        response :precondition_required, "you are still in inactive state"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model 
      #       to create complex swagger_model
      swagger_model :user_activate_request do
        description "User required params for the user_activate request"
        property :user, :user_activate_hash, :required, "user hash with referral_code", { "items" => { "$ref" => "user_activate_hash" } }
        property :session_token, :string, :required, "session_token to authorize user access"
      end
      swagger_model :user_activate_hash do
        description "User hash for the resend_otp request"
        property :referral_code, :string, :required, "User referral code to activate"
      end


      ##################################################################################
      #                       apply_referral_benefits API                              #
      ##################################################################################
      # 
      # To apply referral benefits to user when referral code is submitted
      # 
      # Request::
      #   * user_params: [hash] phone_number is must 
      # 
      # @return [response] sends response to user
      # 
      def apply_referral_benefits
        deciding_params = @application_helper.get_deciding_params(params)
        apply_referral_benefits_api = APPLY_REFERRAL_BENEFITS_API.new(deciding_params)

        response = apply_referral_benefits_api.enact(referral_code_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :apply_referral_benefits do
        summary "It activates the user state"
        notes "activate_with_referral_code API activates the inactive User if in request we get correct
        referral code otherwise throws precondition_required error"
        param :body, :referral_request, :referral_request, :required, "activate user request"
        response :unauthorized, "user not found"
        response :bad_request, "not valid parameters"
        response :internal_server_error, "run time error happened"
        response :precondition_required, "you are still in inactive state"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model 
      #       to create complex swagger_model
      swagger_model :referral_request do
        description "User required params for the user_activate request"
        property :user, :referral_code_hash, :required, "user hash with referral_code", { "items" => { "$ref" => "referral_code_hash" } }
        property :session_token, :string, :required, "session_token to authorize user access"
      end
      swagger_model :referral_code_hash do
        description "User hash for the resend_otp request"
        property :referral_code, :string, :required, "User referral code to activate"
      end

      ##################################################################################
      #                             check_user_state API                               #
      ##################################################################################
      
      # 
      # To return the current state of user
      # 
      # Request::
      #   * user_params: [hash] phone_number is must 
      # 
      # @return [response] sends response to user with current user state
      #
      _LogActivity_ 
      def check_user_state
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.check_user_state
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :check_user_state do
        summary "It returns the current state of user"
        notes "check_user_state API returns the current state of User"
        param :query, :session_token, :string, :required, "session_token to authorize user access & retrieve it's information"
        response :unauthorized, "user not found"
        response :bad_request, "session_token not found"
        response :internal_server_error, "run time error happened"
      end
    
      ##################################################################################
      #                          update_user_details API                               #
      ##################################################################################

      # 
      # To update first_name, last_name and email_id of the user
      # 
      # Request::
      #   * user_params: [hash] first_name, email_id is must
      #   * session_token: a valid session_token is must
      # 
      # @return [response] sends response to user with phone_number
      #
      _LogActivity_ 
      def update_user_details
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.update_user_details(update_user_details_params)
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :update_user_details do
        summary "It adds User profile"
        notes "add profile API adds first name, last name and email id of the User. Email id and
        First name are compulsory. This API calls the Email Service function add_email_for_user to
        add email id of the user."
        param :body, :add_user_profile_request, :add_user_profile_request, :required, "add profile request"
        response :unauthorized, "User not having correct Role to access the Page"
        response :bad_request, "Parameters missing/invalid parameters"
        response :internal_server_error, "run time error happened"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :add_user_profile_request do
        description "Request for adding profile of an employee."
        property :user, :add_user_hash, :required, "user hash with user details", { "items" => { "$ref" => "user_hash" } }
        property :session_token, :string, :required, "session token required for identifying a valid session"
      end
      swagger_model :add_user_hash do
        description "User hash with user related details of user"
        property :first_name, :string, :required, "User first_name"
        property :last_name, :string, "User last_name"
        property :emails, :array, :required, "User Emails hash", {'items' => { 'type' => :email_hash } }
      end
      swagger_model :email_hash do
        description 'Email model hash having user emails related details'
        property :email_id, :string, :required, 'users email id'
        property :is_primary, :boolean, :optional, 't/f is email id is primary or not'
      end

      ##################################################################################
      #                           create_password API                                  #
      ##################################################################################
      
      # 
      # To create password for user
      # 
      # Request::
      #   * user_params: [hash] password is must
      #   * session_token: to authenticate user request
      # 
      # @return [response] sends response to user with appropriate message
      #
      _LogActivity_ 
      def create_password
        deciding_params = @application_helper.get_deciding_params(params)
        create_password_api = CREATE_PASSWORD_API.new(deciding_params)
        create_password_request = {
          user: create_password_user_params
        }
        create_password_response = create_password_api.enact(create_password_request)
        send_response(create_password_response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :create_password do
        summary "create password API"
        notes "create password API is called to generate/replace user's password. The replacement
              will be governed by reset flag set in the password table and creation will be 
              governed by generic provider presence"
        param :body, :create_password_request, :create_password_request, :optional, 
              "create password API request."
        response :unauthorized, "User not found"
        response :bad_request, "wrong password params"
        response :precondition_required, "reset process not initiated and password creation called"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise optional
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model 
      #       to create complex swagger_model
      swagger_model :create_password_request do
        description "User required params for the create password request"
        property :user, :create_password_user_hash, :required, "user hash with Password"
        property :session_token, :string, :required, "session token required for identifying a valid session"
      end
      swagger_model :create_password_user_hash do
        description "User hash for the create password request"
        property :password, :string, :required, "password to create"
      end

      # 
      # To get ample credits of user
      # 
      # Request::
      #   * session_token: to authenticate user request
      # 
      # @return [response] sends response to user with appropriate message
      #
      def get_current_ample_credits
        deciding_params = @application_helper.get_deciding_params(params)
        get_ample_credit_of_user_api = GET_AMPLE_CREDIT_OF_USER_API.new(deciding_params)
       
        response = get_ample_credit_of_user_api.enact

        send_response(response) 
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :get_current_ample_credits do
        summary "get current ample credit of users"
        notes "extract user from session and return the ample credit for his/her"
        param :query, :session_token, :string, :required, "session_token to authorize user access & retrieve it's information"
        response :unauthorized, "User not found"
        response :bad_request, "wrong password params"
        response :precondition_required, "reset process not initiated and password creation called"
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # white list parameters for decide_register_login API.
      #
      def decide_register_login_params
        params.require(:user).permit(:phone_number, :password)
      end

      #
      # white list parameters for login API.
      # 
      def login_params
        params.require(:user).permit(:phone_number, :password, :otp)
      end

      #
      # white list parameters for resend_otp API.
      # 
      def get_user_phone_number_from_post_params
        params.require(:user).permit(:phone_number)
      end

      #
      # white list parameters for activate_with_referral_code API.
      # 
      def activate_with_referral_code_params
        params.require(:user).permit(:referral_code)
      end

      def referral_code_params
        params.require(:user).permit(:referral_code)
      end

      #
      # white list parameter for request type
      # 
      def request_type_param
        params.require(:request_type)
      end

      #
      # filter user_id from user hash
      #
      def get_user_id_from_post_params
        params.require(:user).permit(:user_id)
      end

      #
      # white list parameters for add profile request.
      # Filter out: first name, last name, email id, session token
      #
      def update_user_details_params
        params.require(:user).permit(:first_name, :last_name, emails: [:email_id, :is_primary])
      end

      #
      # white list parameters for create_password API.
      # 
      def create_password_user_params
        params.require(:user).permit(:password)
      end

      #
      # white list parameters for check_user API.
      # 
      def check_user_request_params
        params.require(:user).permit(:phone_number)
      end

    end
  end
end

#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    #
    # Prodives API to upload image
    #
    class ImagesController < BaseModule::V1::ApplicationController

      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      IMAGE_SERVICE = ImageServiceModule::V1::ImageService
      CHANGE_IMAGE_STATE_API = ImageServiceModule::V1::ChangeImageStateApi
      IMAGE_MODEL_EVENTS = ImageServiceModule::V1::ModelStates::ImageEvents
      # Response::
      #    * 200 ok: if role successfully created
      #    * 400 : if invalid or insufficient
      #
      def upload_image
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        image_service = IMAGE_SERVICE.new(deciding_params)
        response = image_service.upload_image(image_params)
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :images, 'ImageServiceModule APIs'
      swagger_api :upload_image do
        summary 'It upload image locally and returns the url'
        notes 'It takes image upload it locally and return the image_id and relative url'
        param :query, :file, :required, 'file to upload'
        response :bad_request, 'Parameters missing/wrong parameters'
      end

      # 
      # This api change state of image to soft delete
      # 
      # Response::
      #    * 200 ok: if role successfully created
      #    * 400 : if invalid or insufficient
      #
      def delete_image
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_image_state_api = CHANGE_IMAGE_STATE_API.new(deciding_params)

        response = change_image_state_api.enact( {
            id: get_id_from_params,
            event: IMAGE_MODEL_EVENTS::SOFT_DELETE
          })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :images, 'ImageServiceModule APIs'
      swagger_api :delete_image do
        summary 'It delete the image'
        notes 'delete_image API change the status of image to soft delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :path, :id, :integer, :required, 'image_id to be deleted'
        response :bad_request, 'wrong parameters or image not found or it fails to delete'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private
      # Incoming params will be a json with key 'file' and value as a json like:
      # :params => {'file'=>
      #   {'0'=>#<ActionDispatch::Http::UploadedFile:0x007f9a7440bf30 
      #          @tempfile=#<Tempfile:/var/folders/kj/8tnfbzns49zbd36njrdmkwm40000gn/T/RackMultipart20151029-93065-fakisz.jpg>, 
      #          @original_filename='role_icon.jpg', @content_type='image/jpeg', 
      #          @headers='Content-Disposition: form-data; name=\'file[0]\'; filename=\'role_icon.jpg\'\r\nContent-Type: image/jpeg\r\n'>,
      #    '1'=>#<ActionDispatch::Http::UploadedFile:0x007f9a757a1090
      #         @tempfile=#<Tempfile:/var/folders/kj/8tnfbzns49zbd36njrdmkwm40000gn/T/RackMultipart20151029-93065-8zuvfj.png>,
      #         @original_filename='role_icon.png', @content_type='image/png',
      #         @headers='Content-Disposition: form-data; name=\'file[1]\'; filename=\'role_icon.png\'\r\nContent-Type: image/png\r\n'>
      #   },
      #   'controller' => ..., 'action'=> ...
      # }
      def image_params
        params.require(:file)
      end

      def get_id_from_params
        return params.require(:id)
      end
    end
  end
end

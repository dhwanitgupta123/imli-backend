module CategorizationModule
  module V1
    #
    # Category controller service class to route panel API calls to the Category service
    #
    class MpspCategoriesController < BaseModule::V1::ApplicationController
      #
      # initialize CategoryService Class
      #
      def initialize
        @mpsp_category_service = CategorizationModule::V1::MpspCategoryService
        @application_helper = CommonModule::V1::ApplicationHelper
      end

      ##################################################################################
      #                              create_mpsp_category API                               #
      ##################################################################################

      #
      # function to create new mpsp_category
      #
      # POST /categories/new
      #
      # Request::
      #   * mpsp_category_params [hash] it contains label & image url of the mpsp_category
      #
      # Response::
      #   * sends ok response to the panel
      #
      def create_mpsp_category
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_category_service = @mpsp_category_service.new(deciding_params)
        response = mpsp_category_service.create_mpsp_category(create_mpsp_category_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :categories, 'CategorizationModule APIs'
      swagger_api :create_mpsp_category do
        summary 'It creates a new mpsp_category'
        notes 'create_mpsp_category API creates a new mpsp_category under an existing mpsp_department, if mpsp_department
               doesnt exists it throws an error'
        param :body, :mpsp_category_request, :mpsp_category_request, :required, 'create_mpsp_category request'
        response :bad_request, 'if label of mpsp_category is missing or mpsp_department not found'
        response :internal_server_error, 'run time error happened'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if is a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :mpsp_category_request do
        description 'request schema for /create_mpsp_category API'
        property :mpsp_category, :mpsp_category_hash, :required, 'mpsp_category model hash',
                 { 'items' => { '$ref' => 'mpsp_category_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :mpsp_category_hash do
        description 'details of mpsp_category'
        property :label, :string, :required, 'name of mpsp_category'
        property :mpsp_department_id, :integer, :required, 'mpsp_department id to which mpsp_category belongs'
        property :image_url, :string, :optional, 'image url for the mpsp_department image'
        property :description, :string, :optional, 'description of mpsp_department'
        property :priority, :integer, :required, 'priority of categories'
      end

      ##################################################################################
      #                             create_mpsp_sub_category API                             #
      ##################################################################################

      #
      # function to create new sub category
      #
      # POST /mpsp_sub_categories/new
      #
      # Request::
      #   * category_params [hash] it contains label & image url of the sub category
      #
      # Response::
      #   * sends ok response to the panel
      #
      def create_mpsp_sub_category
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_category_service = @mpsp_category_service.new(deciding_params)
        response = mpsp_category_service.create_mpsp_sub_category(create_mpsp_sub_category_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_categories, 'CategorizationModule APIs'
      swagger_api :create_mpsp_sub_category do
        summary 'It creates a new sub category'
        notes 'create_mpsp_sub_category API creates a new sub category under an existing category, if category
               doesnt exists it throws an error'
        param :body, :mpsp_sub_category_request, :mpsp_sub_category_request, :required, 'create_mpsp_sub_category request'
        response :bad_request, 'if label of sub category is missing or category not found'
        response :internal_server_error, 'run time error happened'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if its a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :mpsp_sub_category_request do
        description 'request schema for /create_category API'
        property :mpsp_category, :mpsp_sub_category_hash, :required, 'category model hash',
                 { 'items' => { '$ref' => 'mpsp_sub_category_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :mpsp_sub_category_hash do
        description 'details of sub category'
        property :label, :string, :required, 'name of sub category'
        property :mpsp_category_id, :integer, :required, 'mpsp_department id to which category belongs'
        property :image_url, :string, :optional, 'image url for the mpsp_department image'
        property :description, :string, :optional, 'description of mpsp_department'
        property :priority, :integer, :required, 'priority of sub categories'
      end

      ##################################################################################
      #                               update_category API                              #
      ##################################################################################

      #
      # function to update a mpsp_category
      #
      # PUT /mpsp_categories/update/:id
      #
      # Request::
      #   * mpsp_category_params [hash] it contains updated label & image url of the mpsp_category
      #
      # Response::
      #   * sends ok response to the panel
      #
      def update_mpsp_category
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_category_service = @mpsp_category_service.new(deciding_params)
        response = mpsp_category_service.update_mpsp_category(update_mpsp_category_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_categories, 'CategorizationModule APIs'
      swagger_api :update_mpsp_category do
        summary 'It updates a mpsp_category'
        notes 'update_mpsp_category API updates a Category'
        param :path, :id, :integer, :optional, 'mpsp_category ID'
        param :body, :update_mpsp_category_request, :update_mpsp_category_request, :required, 'update_mpsp_category request'
        response :bad_request, 'if id of mpsp_category is missing'
        response :internal_server_error, 'run time error happened'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :update_mpsp_category_request do
        description 'request schema for /update_mpsp_category API'
        property :mpsp_category, :update_mpsp_category_hash, :required, 'mpsp_category model hash',
                 { 'items' => { '$ref' => 'update_mpsp_category_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :update_mpsp_category_hash do
        description 'details of mpsp_category to be updated'
        property :mpsp_department_id, :integer, :required, 'id of mpsp_department'
        property :label, :string, :optional, 'name of mpsp_category'
        property :image_url, :string, :optional, 'image url for the mpsp_category image'
        property :description, :string, :optional, 'description of mpsp_category'
        property :priority, :integer, :required, 'priority of mpsp_department'
      end

      ##################################################################################
      #                                    change_state API                            #
      ##################################################################################

      #
      # function to change the state of a mpsp_category
      #
      # PUT /mpsp_categories/state/:id
      #
      # Request::
      #   * id [integer] id of mpsp_category whose status is to be changed based on the event
      #   * params[:event] [integer] event to trigger
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_category_service = @mpsp_category_service.new(deciding_params)
        response = mpsp_category_service.change_state(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_api :change_state do
        summary 'It changes the status of mpsp_category/sub category'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the mpsp_category/sub-category & chnges the status if everyhting is correct'
        param :path, :id, :integer, :required, 'mpsp_category id to be deactivated'
        param :body, :change_mpsp_category_state_request, :change_mpsp_category_state_request, :required, 'change state request'
        response :bad_request, 'wrong parameters or mpsp_category not found'
        response :precondition_required, 'event cant be triggered based on the current status of mpsp_category'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_mpsp_category_state_request do
        description 'request schema for /change_state API'
        property :mpsp_category, :change_mpsp_category_state_hash, :required, 'hash of event to trigger state change',
                 { 'items' => { '$ref' => 'change_mpsp_category_state_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_mpsp_category_state_hash do
        description 'event to be triggered'
        property :event, :integer, :required, 'event to trigger status change'
      end

      ##################################################################################
      #                            update_mpsp_sub_category API                             #
      ##################################################################################

      #
      # function to update a sub category
      #
      # PUT /mpsp_sub_categories/update/:id
      #
      # Request::
      #   * mpsp_sub_category_params [hash] it contains updated label & image url of the sub category
      #
      # Response::
      #   * sends ok response to the panel
      #
      def update_mpsp_sub_category
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_category_service = @mpsp_category_service.new(deciding_params)
        response = mpsp_category_service.update_mpsp_sub_category(update_mpsp_sub_category_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_categories, 'CategorizationModule APIs'
      swagger_api :update_mpsp_sub_category do
        summary 'It updates a sub category'
        notes 'update_mpsp_sub_category API updates a Sub Category'
        param :path, :id, :integer, :optional, 'Sub category ID'
        param :body, :update_mpsp_sub_category_request, :update_mpsp_sub_category_request, :required,
              'update_mpsp_sub_category request'
        response :bad_request, 'if Id of sub category is missing'
        response :internal_server_error, 'run time error happened'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :update_mpsp_sub_category_request do
        description 'request schema for /update_category API'
        property :mpsp_category, :update_mpsp_sub_category_hash, :required, 'sub category model hash',
                 { 'items' => { '$ref' => 'update_mpsp_sub_category_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :update_mpsp_sub_category_hash do
        description 'details of sub category to be updated'
        property :mpsp_category_id, :integer, :required, 'id of parent category'
        property :label, :string, :optional, 'name of sub category'
        property :image_url, :string, :optional, 'image url for the sub category image'
        property :description, :string, :optional, 'description of sub category'
        property :priority, :integer, :required, 'priority of sub categories'
      end


      ##################################################################################
      #                                 get_category API                               #
      ##################################################################################

      #
      # function to get data of a category
      #
      # GET /mpsp_categories/:id
      #
      # Request::
      #   * category_params [hash] it contains label of category of which information is requested
      #
      # Response::
      #   * sends response with all the sub categories which are the part of requested category
      #
      def get_mpsp_category
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_category_service = @mpsp_category_service.new(deciding_params)
        response = mpsp_category_service.get_mpsp_category(params[:id])
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_categories, 'CategorizationModule APIs'
      swagger_api :get_mpsp_category do
        summary 'It returns details of a mpsp_category'
        notes 'get_mpsp_category API returns the details of a mpsp_category which includes its mpsp_sub_category
               and mpsp_department. If category id is of a sub-category then it returns details of sub category
               with category label'
        param :path, :id, :integer, :required, 'mpsp_category_id of which details are requested'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or mpsp_category not found'
        response :internal_server_error, 'run time error happened'
      end

      ##################################################################################
      #                             get_all_mpsp_categories API                             #
      ##################################################################################

      #
      # function to get data of a all mpsp_categories
      #
      # GET /mpsp_categories
      #
      # Response::
      #   * sends response with all the mpsp_categories labels
      #
      def get_all_mpsp_categories
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_category_service = @mpsp_category_service.new(deciding_params)
        response = mpsp_category_service.get_all_mpsp_categories(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if its a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_categories, 'CategorizationModule APIs'
      swagger_api :get_all_mpsp_categories do
        summary 'It returns details of all the mpsp_categories'
        notes 'get_all_mpsp_categories API returns the details of all mpsp_categories and its mpsp_department.'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all mpsp_categories'
        param :query, :per_page, :integer, :optional, 'how many mpsp_categories to display in a page'
        param :query, :state, :integer, :optional, 'to fetch mpsp_categories based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
      end

      ##################################################################################
      #                             get_all_mpsp_sub_categories AP                          #
      ##################################################################################

      #
      # function to get data of a all sub categories
      #
      # GET /mpsp_sub_categories
      #
      # Response::
      #   * sends response with all the sub categories labels
      #
      def get_all_mpsp_sub_categories
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_category_service = @mpsp_category_service.new(deciding_params)
        response = mpsp_category_service.get_all_mpsp_sub_categories(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if its a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_categories, 'CategorizationModule APIs'
      swagger_api :get_all_mpsp_sub_categories do
        summary 'It returns details of all the sub categories'
        notes 'get_all_mpsp_sub_categories API returns the details of all sub categories and its parent mpsp_category.'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all sub categories'
        param :query, :per_page, :integer, :optional, 'how many sub categories to display in a page'
        param :query, :state, :integer, :optional, 'to fetch sub categories based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
      end


      private

      #
      # white list params for create_mpsp_category
      #
      def create_mpsp_category_params
        params.require(:mpsp_category).permit(:label, :mpsp_department_id, :description, :priority)
      end

      #
      # white list params for create_mpsp_sub_category
      #
      def create_mpsp_sub_category_params
        params.require(:mpsp_category).permit(:label, :mpsp_category_id, :description, :priority)
      end

      #
      # White list params for update dept API
      #
      def update_mpsp_category_params
        params.require(:mpsp_category).permit(:id, :label, :mpsp_department_id, :description, :priority).merge({ id: params[:id] })
      end

      #
      # White list params for update dept API
      #
      def update_mpsp_sub_category_params
        params.require(:mpsp_category).permit(:id, :label, :mpsp_category_id, :description, :priority).merge({ id: params[:id] })
      end

      #
      # White list params for change state API
      #
      def change_state_params
        params.require(:mpsp_category).permit(:event).merge({ id: params[:id] })
      end

      #
      # White list params for get_all_mpsp_departments API
      #
      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end

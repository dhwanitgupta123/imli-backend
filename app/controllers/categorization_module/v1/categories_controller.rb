module CategorizationModule
  module V1
    #
    # Category controller service class to route panel API calls to the Category service
    #
    class CategoriesController < BaseModule::V1::ApplicationController
      #
      # initialize CategoryService Class
      #
      def initialize
        @category_service = CategorizationModule::V1::CategoryService
        @application_helper = CommonModule::V1::ApplicationHelper
      end

      ##################################################################################
      #                              create_category API                               #
      ##################################################################################

      #
      # function to create new category
      #
      # POST /categories/new
      #
      # Request::
      #   * category_params [hash] it contains label & image url of the category
      #
      # Response::
      #   * sends ok response to the panel
      #
      def create_category
        deciding_params = @application_helper.get_deciding_params(params)
        category_service = @category_service.new(deciding_params)
        response = category_service.create_category(create_category_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :categories, 'CategorizationModule APIs'
      swagger_api :create_category do
        summary 'It creates a new category'
        notes 'create_category API creates a new category under an existing department, if department
               doesnt exists it throws an error'
        param :body, :category_request, :category_request, :required, 'create_category request'
        response :bad_request, 'if label of category is missing or department not found'
        response :internal_server_error, 'run time error happened'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if is a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :category_request do
        description 'request schema for /create_category API'
        property :category, :category_hash, :required, 'category model hash',
                 { 'items' => { '$ref' => 'category_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :category_hash do
        description 'details of category'
        property :label, :string, :required, 'name of category'
        property :department_id, :integer, :required, 'department id to which category belongs'
        property :image_url, :string, :optional, 'image url for the department image'
        property :description, :string, :optional, 'description of department'
        property :priority, :integer, :required, 'priority of categories'
      end

      ##################################################################################
      #                             create_sub_category API                             #
      ##################################################################################

      #
      # function to create new sub category
      #
      # POST /sub_categories/new
      #
      # Request::
      #   * category_params [hash] it contains label & image url of the sub category
      #
      # Response::
      #   * sends ok response to the panel
      #
      def create_sub_category
        deciding_params = @application_helper.get_deciding_params(params)
        category_service = @category_service.new(deciding_params)
        response = category_service.create_sub_category(create_sub_category_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :categories, 'CategorizationModule APIs'
      swagger_api :create_sub_category do
        summary 'It creates a new sub category'
        notes 'create_sub_category API creates a new sub category under an existing category, if category
               doesnt exists it throws an error'
        param :body, :sub_category_request, :sub_category_request, :required, 'create_sub_category request'
        response :bad_request, 'if label of sub category is missing or category not found'
        response :internal_server_error, 'run time error happened'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if its a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :sub_category_request do
        description 'request schema for /create_category API'
        property :category, :sub_category_hash, :required, 'category model hash',
                 { 'items' => { '$ref' => 'sub_category_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :sub_category_hash do
        description 'details of sub category'
        property :label, :string, :required, 'name of sub category'
        property :category_id, :integer, :required, 'department id to which category belongs'
        property :image_url, :string, :optional, 'image url for the department image'
        property :description, :string, :optional, 'description of department'
        property :priority, :integer, :required, 'priority of sub categories'
      end

      ##################################################################################
      #                               update_category API                              #
      ##################################################################################

      #
      # function to update a category
      #
      # PUT /categories/update/:id
      #
      # Request::
      #   * category_params [hash] it contains updated label & image url of the category
      #
      # Response::
      #   * sends ok response to the panel
      #
      def update_category
        deciding_params = @application_helper.get_deciding_params(params)
        category_service = @category_service.new(deciding_params)
        response = category_service.update_category(update_category_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :categories, 'CategorizationModule APIs'
      swagger_api :update_category do
        summary 'It updates a category'
        notes 'update_category API updates a Category'
        param :path, :id, :integer, :optional, 'category ID'
        param :body, :update_category_request, :update_category_request, :required, 'update_category request'
        response :bad_request, 'if id of category is missing'
        response :internal_server_error, 'run time error happened'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :update_category_request do
        description 'request schema for /update_category API'
        property :category, :update_category_hash, :required, 'category model hash',
                 { 'items' => { '$ref' => 'update_category_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :update_category_hash do
        description 'details of category to be updated'
        property :department_id, :integer, :required, 'id of department'
        property :label, :string, :optional, 'name of category'
        property :image_url, :string, :optional, 'image url for the category image'
        property :description, :string, :optional, 'description of category'
        property :priority, :integer, :required, 'priority of department'
      end

      ##################################################################################
      #                                    change_state API                            #
      ##################################################################################

      #
      # function to change the state of a category
      #
      # PUT /categories/state/:id
      #
      # Request::
      #   * id [integer] id of category whose status is to be changed based on the event
      #   * params[:event] [integer] event to trigger
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = @application_helper.get_deciding_params(params)
        category_service = @category_service.new(deciding_params)
        response = category_service.change_state(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_api :change_state do
        summary 'It changes the status of category/sub category'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the category/sub-category & chnges the status if everyhting is correct'
        param :path, :id, :integer, :required, 'category id to be deactivated'
        param :body, :change_category_state_request, :change_category_state_request, :required, 'change state request'
        response :bad_request, 'wrong parameters or category not found'
        response :precondition_required, 'event cant be triggered based on the current status of category'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_category_state_request do
        description 'request schema for /change_state API'
        property :category, :change_category_state_hash, :required, 'hash of event to trigger state change',
                 { 'items' => { '$ref' => 'change_category_state_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_category_state_hash do
        description 'event to be triggered'
        property :event, :integer, :required, 'event to trigger status change'
      end

      ##################################################################################
      #                            update_sub_category API                             #
      ##################################################################################

      #
      # function to update a sub category
      #
      # PUT /sub_categories/update/:id
      #
      # Request::
      #   * sub_category_params [hash] it contains updated label & image url of the sub category
      #
      # Response::
      #   * sends ok response to the panel
      #
      def update_sub_category
        deciding_params = @application_helper.get_deciding_params(params)
        category_service = @category_service.new(deciding_params)
        response = category_service.update_sub_category(update_sub_category_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :categories, 'CategorizationModule APIs'
      swagger_api :update_sub_category do
        summary 'It updates a sub category'
        notes 'update_sub_category API updates a Sub Category'
        param :path, :id, :integer, :optional, 'Sub category ID'
        param :body, :update_sub_category_request, :update_sub_category_request, :required,
              'update_sub_category request'
        response :bad_request, 'if Id of sub category is missing'
        response :internal_server_error, 'run time error happened'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :update_sub_category_request do
        description 'request schema for /update_category API'
        property :category, :update_sub_category_hash, :required, 'sub category model hash',
                 { 'items' => { '$ref' => 'update_sub_category_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :update_sub_category_hash do
        description 'details of sub category to be updated'
        property :category_id, :integer, :required, 'id of parent category'
        property :label, :string, :optional, 'name of sub category'
        property :image_url, :string, :optional, 'image url for the sub category image'
        property :description, :string, :optional, 'description of sub category'
        property :priority, :integer, :required, 'priority of sub categories'
      end


      ##################################################################################
      #                                 get_category API                               #
      ##################################################################################

      #
      # function to get data of a category
      #
      # GET /categories/:id
      #
      # Request::
      #   * category_params [hash] it contains label of category of which information is requested
      #
      # Response::
      #   * sends response with all the sub categories which are the part of requested category
      #
      def get_category
        deciding_params = @application_helper.get_deciding_params(params)
        category_service = @category_service.new(deciding_params)
        response = category_service.get_category(params[:id])
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :categories, 'CategorizationModule APIs'
      swagger_api :get_category do
        summary 'It returns details of a category'
        notes 'get_category API returns the details of a category which includes its sub_category
               and department. If category id is of a sub-category then it returns details of sub category
               with category label'
        param :path, :id, :integer, :required, 'category_id of which details are requested'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or category not found'
        response :internal_server_error, 'run time error happened'
      end

      ##################################################################################
      #                             get_all_categories API                             #
      ##################################################################################

      #
      # function to get data of a all categories
      #
      # GET /categories
      #
      # Response::
      #   * sends response with all the categories labels
      #
      def get_all_categories
        deciding_params = @application_helper.get_deciding_params(params)
        category_service = @category_service.new(deciding_params)
        response = category_service.get_all_categories(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if its a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :categories, 'CategorizationModule APIs'
      swagger_api :get_all_categories do
        summary 'It returns details of all the categories'
        notes 'get_all_categories API returns the details of all categories and its department.'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all categories'
        param :query, :per_page, :integer, :optional, 'how many categories to display in a page'
        param :query, :state, :integer, :optional, 'to fetch categories based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
      end

      ##################################################################################
      #                             get_all_sub_categories AP                          #
      ##################################################################################

      #
      # function to get data of a all sub categories
      #
      # GET /sub_categories
      #
      # Response::
      #   * sends response with all the sub categories labels
      #
      def get_all_sub_categories
        deciding_params = @application_helper.get_deciding_params(params)
        category_service = @category_service.new(deciding_params)
        response = category_service.get_all_sub_categories(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if its a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :categories, 'CategorizationModule APIs'
      swagger_api :get_all_sub_categories do
        summary 'It returns details of all the sub categories'
        notes 'get_all_sub_categories API returns the details of all sub categories and its parent category.'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all sub categories'
        param :query, :per_page, :integer, :optional, 'how many sub categories to display in a page'
        param :query, :state, :integer, :optional, 'to fetch sub categories based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
      end


      private

      #
      # white list params for create_category
      #
      def create_category_params
        params.require(:category).permit(:label, :image_url, :department_id, :description, :priority)
      end

      #
      # white list params for create_sub_category
      #
      def create_sub_category_params
        params.require(:category).permit(:label, :image_url, :category_id, :description, :priority)
      end

      #
      # White list params for update dept API
      #
      def update_category_params
        params.require(:category).permit(:id, :label, :image_url, :department_id, :description, :priority).merge({ id: params[:id] })
      end

      #
      # White list params for update dept API
      #
      def update_sub_category_params
        params.require(:category).permit(:id, :label, :image_url, :category_id, :description, :priority).merge({ id: params[:id] })
      end

      #
      # White list params for change state API
      #
      def change_state_params
        params.require(:category).permit(:event).merge({ id: params[:id] })
      end

      #
      # White list params for get_all_departments API
      #
      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end

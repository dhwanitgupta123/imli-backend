module CategorizationModule
  module V1
    #
    # MpspDepartments controller service class to route panel API calls to the MpspDepartment service
    #
    class MpspDepartmentsController < BaseModule::V1::ApplicationController
      #
      # initialize MpspDepartmentService Class
      #
      def initialize
        @mpsp_department_service = CategorizationModule::V1::MpspDepartmentService
        @application_helper = CommonModule::V1::ApplicationHelper
      end

      ##################################################################################
      #                             create_mpsp_department API                              #
      ##################################################################################

      #
      # function to create new mpsp_department
      #
      # POST /mpsp_departments/new
      #
      # Request::
      #   * mpsp_department_params [hash] it contains label & image url of the mpsp_department
      #
      # Response::
      #   * sends ok response to the panel
      #
      def create_mpsp_department
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_department_service = @mpsp_department_service.new(deciding_params)
        response = mpsp_department_service.create_mpsp_department(mpsp_department_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_departments, 'CategorizationModule APIs'
      swagger_api :create_mpsp_department do
        summary 'It creates a new mpsp_department'
        notes 'create_mpsp_department API creates a new mpsp_department'
        param :body, :mpsp_department_request, :mpsp_department_request, :required, 'create_mpsp_department request'
        response :bad_request, 'if label of mpsp_department is missing'
        response :internal_server_error, 'run time error happened'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :mpsp_department_request do
        description 'request schema for /create_mpsp_department API'
        property :mpsp_department, :mpsp_department_hash, :required, 'mpsp_department model hash',
                 { 'items' => { '$ref' => 'mpsp_department_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :mpsp_department_hash do
        description 'details of mpsp_department to be created'
        property :label, :string, :required, 'name of mpsp_department'
        property :description, :string, :optional, 'description of mpsp_department'
        property :priority, :integer, :required, 'priority of mpsp_department'
        property :images, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :image_hash_array } }
      end

      ##################################################################################
      #                             update_mpsp_department API                              #
      ##################################################################################

      #
      # function to update a mpsp_department
      #
      # PUT /mpsp_departments/update/:id
      #
      # Request::
      #   * mpsp_department_params [hash] it contains updated label & image url of the mpsp_department
      #
      # Response::
      #   * sends ok response to the panel
      #
      def update_mpsp_department
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_department_service = @mpsp_department_service.new(deciding_params)
        response = mpsp_department_service.update_mpsp_department(
          mpsp_department_params.merge({ id: get_id_from_params }))
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_departments, 'CategorizationModule APIs'
      swagger_api :update_mpsp_department do
        summary 'It updates a mpsp_department'
        notes 'update_mpsp_department API updates a mpsp_department'
        param :path, :id, :integer, :optional, 'mpsp_department_id of which details are requested'
        param :body, :update_mpsp_department_request, :update_mpsp_department_request, :required, 'update_mpsp_department request'
        response :bad_request, 'if id of mpsp_department is missing'
        response :internal_server_error, 'run time error happened'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :update_mpsp_department_request do
        description 'request schema for /update_mpsp_department API'
        property :mpsp_department, :update_mpsp_department_hash, :required, 'mpsp_department model hash',
                 { 'items' => { '$ref' => 'update_mpsp_department_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :update_mpsp_department_hash do
        description 'details of mpsp_department to be updated'
        property :label, :string, :required, 'name of mpsp_department'
        property :description, :string, :optional, 'description of mpsp_department'
        property :priority, :integer, :required, 'priority of mpsp_department'
        property :images, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :image_hash_array } }
      end

      swagger_model :image_hash_array do
        description 'details of image id and corresponding priority'
        property :image_id, :integer, :required, 'reference to image'
        property :priority, :integer, :required, 'priority of image'
      end

      ##################################################################################
      #                                    change_state API                            #
      ##################################################################################

      #
      # function to change the status of mpsp_department
      #
      # PUT /mpsp_departments/state/:id
      #
      # Request::
      #   * mpsp_department_id [integer] id of mpsp_department of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_department_service = @mpsp_department_service.new(deciding_params)
        response = mpsp_department_service.change_state(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_departments, 'CategorizationModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of mpsp_department'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the mpsp_department & chnges the status if everyhting is correct'
        param :path, :id, :integer, :required, 'mpsp_department_id to be deactivated'
        param :body, :change_state_request, :change_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or mpsp_department not found'
        response :internal_server_error, 'run time error happened'
        response :precondition_required, 'event cant be triggered based on the current status of mpsp_department'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_state_request do
        description 'request schema for /change_state API'
        property :mpsp_department, :change_state_hash, :required, 'hash of event to trigger state change',
                 { 'items' => { '$ref' => 'change_state_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      ##################################################################################
      #                                get_mpsp_department API                              #
      ##################################################################################

      #
      # function to get data of a mpsp_department
      #
      # GET /mpsp_departments/:id
      #
      # Request::
      #   * mpsp_department_id [integer] id of the mpsp_department whose detail is requested
      #
      # Response::
      #   * sends response with all the categories which are the part of requested mpsp_department
      #
      def get_mpsp_department
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_department_service = @mpsp_department_service.new(deciding_params)
        response = mpsp_department_service.get_mpsp_department(params[:id])
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_departments, 'CategorizationModule APIs'
      swagger_api :get_mpsp_department do
        summary 'It returns details of a mpsp_department'
        notes 'get_mpsp_department API returns the details of a mpsp_department which includes its categories'
        param :path, :id, :integer, :required, 'mpsp_department_id of which details are requested'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        response :bad_request, 'wrong parameters or mpsp_department not found'
        response :internal_server_error, 'run time error happened'
      end

      ##################################################################################
      #                            get_all_mpsp_departments API                             #
      ##################################################################################

      #
      # function to get label of all the mpsp_departments
      #
      # GET /mpsp_departments
      #
      # Response::
      #   * sends response with all the mpsp_department labels
      #
      def get_all_mpsp_departments
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_department_service = @mpsp_department_service.new(deciding_params)
        response = mpsp_department_service.get_all_mpsp_departments(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_departments, 'UsersModule APIs'
      swagger_api :get_all_mpsp_departments do
        summary 'It returns details of all the mpsp_departments'
        notes 'get_all_mpsp_departments API returns the details of all the mpsp_departments'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all mpsp_departments'
        param :query, :per_page, :integer, :optional, 'how many mpsp_departments to display in a page'
        param :query, :state, :integer, :optional, 'to fetch mpsp_departments based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
      end

      #
      # function to get label of all the mpsp_departments, categories and sub_categories
      #
      # GET /mpsp_departments
      #
      # Response::
      #   * sends response with all the mpsp_department labels
      #
      def get_all_mpsp_departments_categories_and_sub_categories
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_department_service = @mpsp_department_service.new(deciding_params)
        response = mpsp_department_service.get_all_mpsp_departments_categories_and_sub_categories
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_departments, 'UsersModule APIs'
      swagger_api :get_all_mpsp_departments_categories_and_sub_categories do
        summary 'It returns details of all the mpsp_departments'
        notes 'get_all_mpsp_departments API returns the details of all the mpsp_departments'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        response :bad_request, 'wrong pagination parameters'
      end

      private

      #
      # White list params for update dept API
      #
      def mpsp_department_params
        if params[:mpsp_department].present? && params[:mpsp_department][:images].present? &&
          params[:mpsp_department][:images].is_a?(String)
          params[:mpsp_department][:images] = JSON.parse(params[:mpsp_department][:images])
        end
        params.require(:mpsp_department).permit(:id, :label, :description, :priority, images: [:image_id, :priority])
      end

      #
      # Get Id from parameters
      #
      def get_id_from_params
        return params.require(:id)
      end

      #
      # White list params for change state API
      #
      def change_state_params
        params.require(:mpsp_department).permit(:event).merge({ id: params[:id] })
      end

      #
      # White list params for get_all_mpsp_departments API
      #
      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end

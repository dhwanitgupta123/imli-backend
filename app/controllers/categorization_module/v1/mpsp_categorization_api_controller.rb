module CategorizationModule
  module V1
    #
    # MpspCategorization API controller class to route Categories related API calls
    # 
    # @author [kartik]
    #
    class MpspCategorizationApiController < BaseModule::V1::ApplicationController
      #
      # initialize required classes
      #
      def initialize
        @mpsp_categorization_api_service = CategorizationModule::V1::MpspCategorizationApiService
        @application_helper = CommonModule::V1::ApplicationHelper
      end

      ##################################################################################
      #                         get_all_mpsp_departments_details API                        #
      ##################################################################################
      #
      # 
      # GET /api/mpsp_departments
      # 
      # To get all active mpsp_departments with all active categories under it
      # 
      # Response::
      #   * sends response with all the active mpsp_departments with all active categories
      # 
      _LogActivity_
      def get_all_mpsp_departments_details
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_categorization_api_service = @mpsp_categorization_api_service.new(deciding_params)
        response = mpsp_categorization_api_service.get_all_mpsp_departments_details(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if its a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_categorization_api, 'MpspCategorizationModule APIs'
      swagger_api :get_all_mpsp_departments_details do
        summary 'It returns details of all the active mpsp_departments and its corresponding active categories'
        notes 'get_all_mpsp_departments_details API returns the details of all active mpsp_departments and its active
          categories.'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all categories'
        param :query, :per_page, :integer, :optional, 'how many categories to display in a page'
        param :query, :state, :integer, :optional, 'to fetch categories based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        param :query, :testing, :boolean, :optional, 'If testing products also to be displayed on APP'
        response :bad_request, 'wrong pagination parameters'
      end

      ##################################################################################
      #                         get_products API                                       #
      ##################################################################################
      #
      # 
      # GET /api/mpsp_departments
      # 
      # To get all active mpsp_departments with all active categories under it
      # 
      # Response::
      #   * sends response with all the active mpsp_departments with all active categories
      # 
      _LogActivity_
      def get_products_by_filters
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_categorization_api_service = @mpsp_categorization_api_service.new(deciding_params)
        response = mpsp_categorization_api_service.get_products_by_filter({
          id: params[:id],
          product_params: get_products_params
          })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if its a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_categorization_api, 'MpspCategorizationModule APIs'
      swagger_api :get_products_by_filters do
        summary 'return products for corresponding mpsp_category id and filters'
        notes 'get all active products for a given mpsp_category id and filters passed'
        param :path, :id, :integer, :requied, 'mpsp_category id for which products are to be fetched'
        param :body, :get_products_request, :get_products_request, :required, 'get_products_by_filters request'
        response :bad_request, 'if filters params or id is missing'
        response :pre_condition_required, 'if all sub categories are not active'
        response :not_found, 'if mpsp_category or corresponding sub categories not found'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :get_products_request do
        description 'request schema for /get_products_request API'
        property :filters, :filters_hash, :required, 'filters hash hash'
        property :session_token, :string, :required, 'session token to authenticate user'
        property :page_no, :integer, :optional, 'page no from which to get all categories'
        property :per_page, :integer, :optional, 'how many categories to display in a page'
        property :state, :integer, :optional, 'to fetch categories based on their current state'
        property :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        property :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
      end

      swagger_model :filters_hash do
        description 'details of filters hash'
        property :mpsp_sub_categories, :array, :required, 'array of sub categories id (integer)', { 'items': { 'type': 'integer' } }
      end

      ##################################################################################
      #                         get_product_by_id API                        #
      ##################################################################################
      #
      # 
      # GET /api/products/mpsp_category/:id
      # 
      # To get all active products with all active categories under it
      # 
      # Response::
      #   * sends response with all the active mpsp_departments with all active categories
      #
      _LogActivity_
      def get_products_by_mpsp_category
        deciding_params = @application_helper.get_deciding_params(params)
        mpsp_categorization_api_service = @mpsp_categorization_api_service.new(deciding_params)
        response = mpsp_categorization_api_service.get_products_by_mpsp_category({
          id: params[:id],
          pagination_params: pagination_params,
          testing: params[:testing]
          })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if its a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :mpsp_categorization_api, 'MpspCategorizationModule APIs'
      swagger_api :get_products_by_mpsp_category do
        summary 'It returns details of all the active products corresponding to passed mpsp_category id'
        notes 'get all active products for a given mpsp_category id'
        param :path, :id, :integer, :requied, 'mpsp_category id for which products are to be fetched'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all categories'
        param :query, :per_page, :integer, :optional, 'how many categories to display in a page'
        param :query, :state, :integer, :optional, 'to fetch categories based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        param :query, :testing, :boolean, :optional, 'If testing products also to be displayed on APP'
        response :bad_request, 'if filters params or id is missing'
        response :pre_condition_required, 'if all sub categories are not active'
        response :not_found, 'if mpsp_category or corresponding sub categories not found'
      end

      private

      #
      # White list params for common pagination params
      #
      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order, :testing)
      end

      #
      # White list params for common pagination params
      #
      def get_products_params
        params.permit(:page_no, :per_page, :sort_by, :order, :filters => [:mpsp_sub_categories => []])
      end

    end
  end
end
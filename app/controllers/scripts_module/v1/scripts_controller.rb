#
# This module handle all functionalities of scripts 
#
module ScriptsModule
  #
  # Version1 fror scripts module 
  #
  module V1
    class ScriptsController < BaseModule::V1::ApplicationController
      # Storing versioned classes and helpers in class level constants
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMPARE_PRICE_API = ScriptsModule::V1::ComparePriceApi

      before_action do |controller|
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end

      def compare_prices
        compare_price_api = COMPARE_PRICE_API.new(@deciding_params)
        response = compare_price_api.enact({ file_path: params[:file].path })
        send_response(response)
      end
    end
  end
end

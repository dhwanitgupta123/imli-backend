module CommonModule
  module V1
    # Controller to handle all the API calls related to user roles
    class PlansController < BaseModule::V1::ApplicationController
      # Storing versioned classes and helpers in class level constants
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      PLAN_SERVICE = CommonModule::V1::PlanService
      PLAN_MODEL = CommonModule::V1::Plan
      PLAN_EVENTS = CommonModule::V1::ModelStates::V1::PlanEvents

      before_action do |controller|
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end

      ##################################################################################
      #                                 create_plan API                                #
      ##################################################################################
      #
      # API to create a new PLAN
      #
      # Response::
      #    * 200 ok: if role successfully created
      #    * 400 : if invalid or insufficient
      #
      def create_plan
        plan_service = PLAN_SERVICE.new(@deciding_params)
        response = plan_service.create_plan({
          plan: plan_params
          })
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :plans, 'CommonModule APIs'
      swagger_api :create_plan do
        summary 'It creates new plan'
        notes 'It takes plan name and its various details. It validates and
        creates a new plan. User should have specific permissions to access this API'
        param :body, :create_plan_request, :create_plan_request,
              :required, 'create plan request'
        response :bad_request, 'Parameters missing/wrong parameters'
        response :not_found, 'Benefits not found w.r.t. passed parameters'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :create_plan_request do
        description 'Request for creating a plan'
        property :plan, :plan_hash_for_create_plan, :required, 'plan hash with plan details'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :plan_hash_for_create_plan do
        description 'PLAN hash with all details related to plan'
        property :name, :string, :required, 'name of the plan'
        property :details, :string, :required, 'details of the plan which is to be created'
        property :duration, :integer, :required, 'duration till which plan will be valid'
        property :validity_label, :string, :required, 'validity label to be displayed'
        property :fee, :integer, :required, 'fee of the plan in INR'
        property :plan_key, :integer, :required, 'unique key for plan'
        property :benefits, :array, :required, 'array of benefits id (integer)', { 'items': { 'type': 'integer' } }
        property :images, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :plan_image_hash_array } }
      end

      swagger_model :plan_image_hash_array do
        description 'details of image id and corresponding priority'
        property :image_id, :integer, :required, 'reference to image'
        property :priority, :integer, :required, 'priority of image'
      end
      ##################################################################################
      #                                 get_all_plans API                              #
      ##################################################################################
      #
      # API to get all plans
      #
      # Request::
      #    * role: hash containing role_name(String) and permissions(Array of Integers)
      #
      # Response::
      #    * 200 ok: with plans array. Empty if no plans
      #
      def get_all_plans
        plan_service = PLAN_SERVICE.new(@deciding_params)
        response = plan_service.get_all_plans
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :plans, 'CommonModule APIs'
      swagger_api :get_all_plans do
        summary 'It creates new plan'
        notes 'It takes plan name and its various details. It validates and
        creates a new plan. User should have specific permissions to access this API'
        param :query, :session_token, :integer, :required, 'session token needed for user authorization'
      end

      ##################################################################################
      #                                 get_plan API                                   #
      ##################################################################################
      #
      # API to get a plan
      #
      # Request::
      #    * role: hash containing role_name(String) and permissions(Array of Integers)
      #
      # Response::
      #    * 200 ok: with valid plan details
      #    * 400 : if invalid or insufficient
      #
      def get_plan
        plan_service = PLAN_SERVICE.new(@deciding_params)
        response = plan_service.get_plan({id: params[:id]})
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :plans, 'CommonModule APIs'
      swagger_api :get_plan do
        summary 'It creates new plan'
        notes 'It takes plan name and its various details. It validates and
        creates a new plan. User should have specific permissions to access this API'
        param :path, :id, :integer, :required, 'id of the plan to fetch'
        param :query, :session_token, :integer, :required, 'session token needed for user authorization'
        response :bad_request, 'Parameters missing/wrong parameters'
        response :not_found, 'If no plan exists for passed id'
      end

      ##################################################################################
      #                                 update_plan API                                #
      ##################################################################################
      #
      # API to update a plan
      #
      # Request::
      #    * role: hash containing role_name(String) and permissions(Array of Integers)
      #
      # Response::
      #    * 200 ok: if role successfully created
      #    * 400 : if invalid or insufficient
      #
      def update_plan
        plan_service = PLAN_SERVICE.new(@deciding_params)
        response = plan_service.update_plan({
          id: params[:id],
          plan: plan_params
          })
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :plans, 'CommonModule APIs'
      swagger_api :update_plan do
        summary 'It creates new plan'
        notes 'It takes plan name and its various details. It validates and
        creates a new plan. User should have specific permissions to access this API'
        param :path, :id, :integer, :required, 'id of the plan to update'
        param :body, :update_plan_request, :update_plan_request,
              :required, 'create plan request'
        response :bad_request, 'Parameters missing/wrong parameters'
        response :not_found, 'if no benefits present w.r.t. passed benefits id array'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :update_plan_request do
        description 'Request for creating a plan'
        property :plan, :plan_hash_for_update_plan, :required, 'plan hash with plan details'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :plan_hash_for_update_plan do
        description 'PLAN hash with all details related to plan'
        property :name, :string, :required, 'name of the plan'
        property :details, :string, :required, 'details of the plan which is to be created'
        property :duration, :integer, :required, 'duration till which plan will be valid'
        property :validity_label, :string, :required, 'validity label to be displayed'
        property :fee, :integer, :required, 'fee of the plan in INR'
        property :plan_key, :integer, :required, 'unique key for plan'
        property :benefits, :array, :required, 'array of benefits id (integer)', { 'items': { 'type': 'integer' } }
        property :images, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :plan_image_hash_array } }
      end

      ##################################################################################
      #                                 change_state API                               #
      ##################################################################################
      #
      # API to change state of a plan
      #
      # Request::
      #    * role: hash containing role_name(String) and permissions(Array of Integers)
      #
      # Response::
      #    * 200 ok: if role successfully created
      #    * 400 : if invalid or insufficient
      #
      def change_state
        plan_service = PLAN_SERVICE.new(@deciding_params)
        response = plan_service.change_state({
          id: params[:id],
          plan: get_event_param
          })
        send_response(response)
      end
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :plans, 'CommonModule APIs'
      swagger_api :change_state do
        summary 'It creates new plan'
        notes 'It takes plan name and its various details. It validates and
        creates a new plan. User should have specific permissions to access this API'
        param :path, :id, :integer, :required, 'id of the plan to update'
        param :body, :change_state_plan_request, :change_state_plan_request,
              :required, 'create plan request'
        response :bad_request, 'Parameters missing/wrong parameters'
        response :not_found, 'if no plan present for requested id'
        response :precondition_required, 'if invalid event for current plan state is passed'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_state_plan_request do
        description 'Request for changing state of the plan'
        property :plan, :plan_hash_for_change_state, :required, 'plan hash with plan details'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :plan_hash_for_change_state do
        description 'PLAN hash with event of plan'
        property :event, :string, :required, 'event to which plan is to be changed to'
      end


      ##################################################################################
      #                                 delete_plan API                               #
      ##################################################################################
      #
      # API to delete a new PLAN
      #
      # Request::
      #    * role: hash containing role_name(String) and permissions(Array of Integers)
      #
      # Response::
      #    * 200 ok: if role successfully created
      #    * 400 : if invalid or insufficient
      #
      def delete_plan
        plan_service = PLAN_SERVICE.new(@deciding_params)
        response = plan_service.change_state({
          id: params[:id],
          plan: { event: PLAN_EVENTS::SOFT_DELETE }
          })
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :plans, 'CommonModule APIs'
      swagger_api :delete_plan do
        summary 'It creates new plan'
        notes 'It takes plan name and its various details. It validates and
        creates a new plan. User should have specific permissions to access this API'
        param :path, :id, :integer, :required, 'id of the plan to update'
        param :query, :session_token, :integer, :required, 'session token needed for user authorization'
        response :bad_request, 'Parameters missing/wrong parameters'
        response :not_found, 'if no plan present for requested id'
        response :precondition_required, 'if delete event is not valid for current plan state'
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      # Never trust parameters from the scary internet, only allow the white list through.
      def plan_params
       params.require(:plan).permit(:name, :details, :duration, :validity_label, :fee, :plan_key, :benefits => [], images: [:image_id, :priority])
      end

      def get_event_param
        params.require(:plan).permit(:event)
      end

    end
  end
end

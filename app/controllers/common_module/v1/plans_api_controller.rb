#
# Common module to handle common functionality of User
#
module CommonModule
  #
  # version of Pans API controller
  #
  module V1
    #
    # Controller to handle all the API calls related to plans
    #
    class PlansApiController < BaseModule::V1::ApplicationController
      # Storing versioned classes and helpers in class level constants
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      PLAN_SERVICE = CommonModule::V1::PlanService

      before_action do |controller|
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end

      ##################################################################################
      #                                 get_all_plans API                              #
      ##################################################################################
      #
      # API to get all active plans
      #
      # Response::
      #    * 200 ok: with plans array. Empty if no plans
      #
      def get_all_plans
        plan_service = PLAN_SERVICE.new(@deciding_params)
        response = plan_service.get_all_active_plans
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :plans_api, 'CommonModule APIs'
      swagger_api :get_all_plans do
        summary 'It creates new plan'
        notes 'It takes plan name and its various details. It validates and
        creates a new plan. User should have specific permissions to access this API'
        param :query, :session_token, :integer, :required, 'session token needed for user authorization'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end
    end
  end
end

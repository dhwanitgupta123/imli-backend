module CommonModule
  module V1
    # Controller to handle all the API calls related to Product suggestions
    class ProductsSuggesterController < BaseModule::V1::ApplicationController
      # Storing versioned classes and helpers in class level constants
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      PRODUCT_SUGGESTER_SERVICE = CommonModule::V1::ProductSuggesterService

      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end

      ##################################################################################
      #                        update_product_suggestions API                          #
      ##################################################################################
      #
      # API to give a product suggestion
      # POST products/suggestions/update
      #
      # Request::
      #    * product_suggestion: hash containing product(String)
      #
      # Response::
      #    * 200 : if successfully created
      #    * 400 : if invalid or insufficient
      #
      def update_product_suggestions
      	product_suggester_service = PRODUCT_SUGGESTER_SERVICE.new(@deciding_params)
      	response = product_suggester_service.update_product_suggestions(update_product_suggestions_params)
      	send_response(response)
      end
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :products_suggester, 'CommonModule APIs'
      swagger_api :update_product_suggestions do
        summary 'It updates users product suggestion'
        notes 'It takes product suggestions from user and saves them in Mongodb'
        param :body, :update_product_suggestions_request, :update_product_suggestions_request,
              :required, 'update product suggestions request'
        response :bad_request, 'Parameters missing/wrong parameters'
        response :forbidden, 'User is not allowed to view the page'
        response :unauthorized, 'User is not having correct rights'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :update_product_suggestions_request do
        description 'Request for suggesting product'
        property :product_suggestion, :product_suggestion_hash, :required, 'suggestion hash with product details'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :product_suggestion_hash do
        description 'HASH having product suggested'
        property :product_name, :string, :required, 'product which user wanted but did not find'
      end

      ##################################################################################
      #                        get_all_product_suggestions API                         #
      ##################################################################################
      #
      # API to get all product suggestions
      # GET products/suggestions
      #
      # Request::
      #    * pagination_params
      #
      # Response::
      #    * 200 : if successfully rendered
      #    * 400 : if invalid or insufficient
      #    * sends response with all the product suggestions
      #
      def get_all_product_suggestions
        product_suggester_service = PRODUCT_SUGGESTER_SERVICE.new(@deciding_params)
        response = product_suggester_service.get_all_product_suggestions(pagination_params)
        send_response(response) 
      end
      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :products_suggester, 'CommonModule APIs'
      swagger_api :get_all_product_suggestions do
        summary 'It returns details of all the suggestions'
        notes 'get_all_product_suggestions API returns the details of all the suggestions'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all product suggestions'
        param :query, :per_page, :integer, :optional, 'how many users to display in a page'
        param :query, :user_id, :integer, :optional, 'to fetch product_suggestions by user_id'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'updated_at' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User is not allowed to view the page'
        response :unauthorized, 'User is not having correct rights'
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # White list params for get_product_suggestions params
      #
      def update_product_suggestions_params
        params.require(:product_suggestion).permit(:product_name)
      end

      #
      # White list params for get_all_product_suggestions API
      #
      def pagination_params
        params.permit(:page_no, :user_id, :per_page, :sort_by, :order)
      end

    end
  end
end

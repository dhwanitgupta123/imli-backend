module CommonModule
  module V1
    # Controller to handle all the API calls related to carousel
    class CarouselsController < BaseModule::V1::ApplicationController
      # Storing versioned classes and helpers in class level constants
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      CAROUSEL_SERVICE = CommonModule::V1::CarouselService

      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end

      ##################################################################################
      #                               update_carousel API                              #
      ##################################################################################
      #
      # API to update a carousel
      # POST carousels/update/:type
      #
      # Request::
      #    * carousel: hash containing type(String), images(Array), priority(Array)
      #
      # Response::
      #    * 200 : if successfully created
      #    * 400 : if invalid or insufficient
      #
      def update_carousel
      	carousel_service = CAROUSEL_SERVICE.new(@deciding_params)
      	response = carousel_service.update_carousel(carousel_params)
      	send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :carousels, "CommonModule APIs"
      swagger_api :update_carousel do
        summary "It updates the carousel"
        notes "It updates the carousel an existent type carousel or creates it if does not exist
        for an existing carousel type"
        param :path, :type, :integer, :optional, "carousel ID"
        param :body, :update_carousel_request, :update_carousel_request, :required, "update carousel request"
        response :unauthorized, "User not having correct Role to access the Page"
        response :bad_request, "Parameters missing/invalid parameters"
        response :forbidden, "User is not allowed to view the page"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :update_carousel_request do
        description "Request for updating the carousel"
        property :carousel, :update_carousel_params, :required, "carousel params with carousel details"
        property :session_token, :string, :required, "session token required for identifying a valid session"
      end
      swagger_model :update_carousel_params do
        description "params for updating carousel"
        property :images, :array, :required, "images array with image hashes", { 'items': { 'type': :image_hash } }
      end
      swagger_model :image_hash do
        description "image hash with image details"
        property :priority, :string, :required, "priority at which image is to be inserted"
        property :image_id, :string, :required, "valid image_id"
        property :action, :string, :required, "action to be taken when image is clicked"
      end


      ##################################################################################
      #                                 get_carousel API                               #
      ##################################################################################
      #
      # API to get a carousel
      # POST carousels/:type
      #
      # Request::
      #    * carousel: hash containing type(String)
      #
      # Response::
      #    * 200 : if successfully rendered
      #    * 400 : if invalid or insufficient
      #
      def get_carousel
        carousel_service = CAROUSEL_SERVICE.new(@deciding_params)
        response = carousel_service.get_carousel(get_carousel_params)
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :carousels, 'CommonModule APIs'
      swagger_api :get_carousel do
        summary 'It returns the requested carousel'
        notes 'It returns the requested carousel with the image details inside it. User should have specific permissions to access this API'
        param :path, :type, :string, :required, 'carousel id to be fetched'
        param :query, :session_token, :string, :required, 'session token needed for user authorization'
        response :bad_request, 'Parameters missing/wrong parameters'
        response :unauthorized, "User not having correct Role to access the Page"
        response :not_found, 'if carousel type does not exist'
        response :forbidden, "User is not allowed to view the page"
      end

      ##################################################################################
      #                  get_orphan_carousels_types_with_details API                   #
      ##################################################################################
      #
      # API to get a carousel
      # GET carousels/types
      #
      # Response::
      #    * 200 : if successfully rendered
      #    * 401 : if insufficient permissions
      #
      def get_orphan_carousels_types_with_details
        carousel_service = CAROUSEL_SERVICE.new(@deciding_params)
        response = carousel_service.get_orphan_carousels_types_with_details
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :carousels, 'CommonModule APIs'
      swagger_api :get_orphan_carousels_types_with_details do
        summary 'It returns all the orphan carousel types with carousel type details'
        notes 'It returns all the orphan carousel types with carousel type details. User should have specific permissions to access this API'
        param :query, :session_token, :string, :required, 'session token needed for user authorization'
        response :unauthorized, "User not having correct Role to access the Page"
        response :forbidden, "User is not allowed to view the page"
      end

      ##################################################################################
      #                             get_all_carousels API                              #
      ##################################################################################
      #
      # API to get all carousels
      # GET carousels
      #
      # Response::
      #    * 200 : if successfully rendered
      #    * 401 : if insufficient permissions
      #
      def get_all_carousels
        carousel_service = CAROUSEL_SERVICE.new(@deciding_params)
        response = carousel_service.get_all_carousels
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :carousels, 'CommonModule APIs'
      swagger_api :get_all_carousels do
        summary 'It returns all the carousel types with carousel type details'
        notes 'It returns all the carousel types with carousel type details. User should have specific permissions to access this API'
        param :query, :session_token, :string, :required, 'session token needed for user authorization'
        response :unauthorized, "User not having correct Role to access the Page"
        response :forbidden, "User is not allowed to view the page"
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # White list params for carousel params
      #
      def carousel_params
        if params[:carousel].present? &&
             params[:carousel][:images].present? &&
               params[:carousel][:images].is_a?(String)

          params[:carousel][:images] = JSON.parse(params[:carousel][:images])

        end
        params.require(:carousel).permit(:type, images: [:image_id, :priority, :action]).merge({ type: params[:type] })
      end

      #
      # White list params for get_carousel params
      #
      def get_carousel_params
        params.permit(:type).merge({ type: params[:type] })
      end

    end
  end
end
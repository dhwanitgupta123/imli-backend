module CommonModule
  module V1
    # Controller to handle all the API calls related to user roles
    class BenefitsController < BaseModule::V1::ApplicationController
      # Storing versioned classes and helpers in class level constants
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      BENEFIT_SERVICE = CommonModule::V1::BenefitService

      before_action do |controller|
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end

      ##################################################################################
      #                       get_all_active_benefits API                              #
      ##################################################################################
      #
      # API to create a new PLAN
      #
      # Request::
      #    * role: hash containing role_name(String) and permissions(Array of Integers)
      #
      # Response::
      #    * 200 ok: with plans array. Empty if no plans
      #
      def get_all_active_benefits
        benefit_service = BENEFIT_SERVICE.new(@deciding_params)
        response = benefit_service.get_all_active_benefits
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :benefits, 'CommonModule APIs'
      swagger_api :get_all_active_benefits do
        summary 'It returns all active benefits'
        notes 'It returns all active benefits present currently. User should have specific permissions to access this API'
        param :query, :session_token, :integer, :required, 'session token needed for user authorization'
      end

    end
  end
end

module CommonModule
  class CommonController < ActionController::Base
    # Define utils globally with versioning
    # Kept them as global constants, so that they are specific
    # to this class only
    STATUS_CODES_UTIL = CommonModule::V1::StatusCodes
    CONTENT_UTIL = CommonModule::V1::Content

    #
    # Action:: Not Found
    # This action is hit when no route found
    # --- 404 ---
    #
    def not_found
      response = {
        payload: {},
        error: { message: CONTENT_UTIL::PAGE_NOT_FOUND },
        meta: {}
      }
      status_code = STATUS_CODES_UTIL::NOT_FOUND
      
      render json: response, status: status_code
    end

  end
end
#
# Module to handle functionalities related to description in any resource
#
module DescriptionsModule
  #
  # Version1 for description module
  #
  module V1
    #
    #
    class DescriptionsController < BaseModule::V1::ApplicationController

      UPDATE_DESCRIPTION_API = DescriptionsModule::V1::UpdateDescriptionApi
      GET_DESCRIPTION_API = DescriptionsModule::V1::GetDescriptionApi
      ADD_DESCRIPTION_API = DescriptionsModule::V1::AddDescriptionApi

      #
      # Initialize helper classes
      #
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper

      #
      # before actions to be executed
      #
      before_action do
        sanitize_params
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end
      #
      # initialize DescriptionsController Class
      #
      def initialize
      end

      #
      # function to update description
      #
      # post /update_description
      #
      # Request:: request object contain
      #   * description_params [hash] it contains heading, data, child descriptions
      #
      # Response::
      #   * If action succeed then returns Success Status code with create description
      #    else returns BadRequest response
      #
      def update_description
        update_description_api = UPDATE_DESCRIPTION_API.new(@deciding_params)
        response = update_description_api.enact(params[:description].merge(id: get_id_from_params))
        send_response(response)
      end


      swagger_controller :descriptions, 'DescriptionsModule APIs'
      swagger_api :update_description do
        summary 'It creates the description with given input'
        notes 'update description'
        param :path, :id, :integer, :required, 'description to update'
        param :body, :description_request, :description_request, :required, 'update_description request'
        response :bad_request, 'if request params are incorrect or api fails to create description'
      end

      #
      # function to get a description
      #
      # GET /descriptions/:id
      #
      # Request::
      #   * description_id [integer] id of description of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_description
        get_description_api = GET_DESCRIPTION_API.new(@deciding_params)
        response = get_description_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :descriptions, 'DescriptionsModule APIs'
      swagger_api :get_description do
        summary 'It return a description tree'
        notes 'get_description API return a description tree corresponding to the id'
        param :path, :id, :integer, :required, 'description id'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or description not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to add description
      #
      # post /add_description
      #
      # Request:: request object contain
      #   * description_params [hash] it contains heading, data, child descriptions
      #
      # Response::
      #   * If action succeed then returns Success Status code with create description
      #    else returns BadRequest response
      #
      def add_description
        add_description_api = ADD_DESCRIPTION_API.new(@deciding_params)
        response = add_description_api.enact(params[:description])
        send_response(response)
      end

      swagger_controller :descriptions, 'DescriptionsModule APIs'
      swagger_api :add_description do
        summary 'It creates the description with given input'
        notes 'create description'
        param :body, :description_request, :description_request, :required, 'create_description request'
        response :bad_request, 'if request params are incorrect or api fails to create description'
      end

      swagger_model :description_request do
        description 'request schema for /create_category API'
        property :description, :description_hash, :required, 'description model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :description_hash do
        description 'details of description'       
        property :resource, :string, :optional, 'resource which description belongs'
        property :resource_id, :integer, :required, 'id of the resource'
        property :heading, :string, :required, 'heading/title of description'
        property :data, :array, :optional, 'array of description',
                    { 'items': { 'type': 'string' } }
        property :childs, :array, :optional, 'array of childs',
                    { 'items': { 'type': 'child_hash' } }
      end

      swagger_model :child_hash do
        description 'details of child description'
        property :id, :integer, :optional, 'id of child in update request'
        property :heading, :string, :required, 'heading/title of description'
        property :data, :array, :optional, 'array of description',
                    { 'items': { 'type': 'string' } }
        property :childs, :array, :optional, 'array of childs',
                    { 'items': { 'type': 'child_hash' } }
      end

      private

      def sanitize_params
        if params[:description].present? && params[:description][:childs].present? &&
          params[:description][:childs].is_a?(String)
          params[:description][:childs] = JSON.parse(params[:description][:childs])
        end
        description_level_keys = [:resource, :resource_id, :heading, :data, :id, :childs]

        description = params[:description]

        return if description.blank?

        description.keys.each do |key|
          if !description_level_keys.include?(key.to_sym)
            description.delete(key)
          end
          if key == :childs
            description[key] = sanitize_childs(description[key])            
          end
        end
        params[:description] = description
      end

      def sanitize_childs(childs)
        
        return childs if childs.blank?

        child_level_keys = [:id, :heading, :data, :childs]
        childs.each do |child|
          child.keys.each do |key|
            if !child_level_keys.include?(key.to_sym)
              child.delete(key)
            end
            if key == :childs
              childs[key] = sanitize_childs(childs[key])
            end
          end
        end
        return childs
      end

      def get_id_from_params
        return params.require(:id)
      end
    end
  end
end

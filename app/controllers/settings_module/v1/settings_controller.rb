module SettingsModule
  module V1
    class SettingsController < BaseModule::V1::ApplicationController
      # Storing versioned classes and helpers in class level constants
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      SETTING_SERVICE = SettingsModule::V1::SettingService

      GET_STATIC_SETTINGS_API = SettingsModule::V1::GetStaticSettingsApi
      GET_DYNAMIC_SETTINGS_API = SettingsModule::V1::GetDynamicSettingsApi

      ##################################################################################
      #                            get_static_settings API                                #
      ##################################################################################
      #
      # API to get Settings required based on certain parameters
      # Settings should be based on following parameters:
      #   * Environment (Development/Testing/Production)
      #   * Platform (App/Web/Panel)
      #   * Type (Android/IOS/BrowserTypes)
      #   * Marketplace (MUM/HYD/DLI)
      #
      # Request::
      #    * session_token of the user
      #
      # Response::
      #    * 200 ok: if role associated with the user is found and returned
      #    * 401/403 : if no user associated with passed session token
      #
      def get_static_settings
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_static_settings_api = GET_STATIC_SETTINGS_API.new(deciding_params)
        # TO-DO: Need to pass parameters specific to Request
        # Ex: Platform(Web/Panel/App), Type(Android/IOS), Marketplace(MUM/HYD)
        response = get_static_settings_api.enact
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :settings, 'SettingsModule APIs'
      swagger_api :get_static_settings do
        summary 'It returns all the settings based on certain criteria'
        notes 'It is just static settings which is same for group of users. It validates
        and returns all the settings based on certain criteria like Environment, Platform, Marketplace
        Currently, sending all DEFAULT settings. IF settings not found, then return empty hash'
      end

      ##################################################################################
      #                            get_dynamic_settings API                            #
      ##################################################################################
      #
      # API to get Settings required based on certain parameters
      # Settings should be based on following parameters:
      #   * Environment (Development/Testing/Production)
      #   * Platform (App/Web/Panel)
      #   * Type (Android/IOS/BrowserTypes)
      #   * Marketplace (MUM/HYD/DLI)
      #
      # Request::
      #    * session_token of the user
      #
      # Response::
      #    * 200 ok: if role associated with the user is found and returned
      #    * 401/403 : if no user associated with passed session token
      #
      def get_dynamic_settings
        # Storing in-coming request in global variable to be accesses by
        # SHIELD later on for fetching experiment override values and other
        # relevant fields. Ex: request.parameters, request.user_agent, request.ip, etc.
        SHIELD_PARAMS[:request] = request
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_dynamic_settings_api = GET_DYNAMIC_SETTINGS_API.new(deciding_params)
        # TO-DO: Need to pass parameters specific to Request
        # Ex: Platform(Web/Panel/App), Type(Android/IOS), Marketplace(MUM/HYD)
        response = get_dynamic_settings_api.enact
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :settings, 'SettingsModule APIs'
      swagger_api :get_dynamic_settings do
        summary 'It returns all the settings based on certain criteria'
        notes 'It takes session token of the requester. It validates
        and returns all the settings specific to requester. If a certain setting is already assigned to a user,
        then he/she will get the same response always. In short, user specific settings are persisted.
        IF settings not found, then return empty hash'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        response :unauthorized, 'no user associated with passed session token'
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      # Never trust parameters from the scary internet, only allow the white list through.
      def get_all_settings_params
        #params.require(:api).permit(:platform, :type, :marketplace)
      end
    end
  end
end
module PaymentModule
  module V1
    class CitrusPaymentsController < BaseModule::V1::ApplicationController 

      CITRUS_PAYMENT_SERVICE = PaymentModule::V1::CitrusPaymentService

      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
      end

      _LogActivity_
      def generate_url

        deciding_params = @application_helper.get_deciding_params(params)
        citrus_payment_service_class = CITRUS_PAYMENT_SERVICE.new(deciding_params)
        response = citrus_payment_service_class.generate_url(generate_url_params)
        send_response(response)
      end

      _LogActivity_
      def transaction_callback

        deciding_params = @application_helper.get_deciding_params(params)
        citrus_payment_service_class = CITRUS_PAYMENT_SERVICE.new(deciding_params)
        response = citrus_payment_service_class.get_transaction_info(transaction_callback_params)
        render json: response
      end

      _LogActivity_
      def web_view
        deciding_params = @application_helper.get_deciding_params(params)
        citrus_payment_service_class = CITRUS_PAYMENT_SERVICE.new(deciding_params)
        response = citrus_payment_service_class.get_web_view(web_view_params)
        @citrus_url = response[:citrus_url]
        @transaction = response[:transaction]
        @return_url = response[:return_url] 
        render 'payment_module/v1/citrus_payments/web_view'
      end

      private
      
      def generate_url_params
        params.require(:transaction).permit(:payment_id, :payment_type)
      end

      def web_view_params
        params.permit(:transaction_id, :email_id)
      end

      def transaction_callback_params
        params.permit(:TxId, :TxStatus, :amount, :pgTxnNo, :issuerRefNo,
                      :authIdCode, :firstName, :lastName, :pgRespCode, :addressZip, :signature)
      end
    end
  end
end

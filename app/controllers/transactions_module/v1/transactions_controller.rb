#
# Module to handle functionalities related to transactions
#
module TransactionsModule
  #
  # Version1 for transactions module
  #
  module V1
    #
    # transactions controller
    #
    class TransactionsController < BaseModule::V1::ApplicationController 

      TRANSACTIONS_SERVICE = TransactionsModule::V1::TransactionsService

      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
      end

      _LogActivity_
      def get_transaction_status

        deciding_params = @application_helper.get_deciding_params(params)
        transactions_service_class = TRANSACTIONS_SERVICE.new(deciding_params)
        response = transactions_service_class.get_transaction_status({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :transactions, 'TransactionsModule APIs'
      swagger_api :get_transaction_status do
        summary 'It return a status of transaction'
        notes 'get_transaction_status API return a status of transaction'
        param :path, :id, :string, :required, 'transaction id'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or transaction not found'
        response :forbidden, 'User does not have permissions to access this page'
      end

      private

      def get_id_from_params
        return params.require(:id)
      end
    end
  end
end

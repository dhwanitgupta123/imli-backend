module TransactionsModule
  module V1
    class PaymentTransactionsController < BaseModule::V1::ApplicationController 
      PAYMENT_TRANSACTIONS_SERVICE = TransactionsModule::V1::PaymentTransactionsService
      RECORD_TRANSACTION_API = TransactionsModule::V1::RecordTransactionApi

      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
      end

      #
      # API function to initiate transaction for payment
      #
      _LogActivity_
      def initiate_transaction
        deciding_params = @application_helper.get_deciding_params(params)
        payment_transactions_service = PAYMENT_TRANSACTIONS_SERVICE.new(deciding_params)
        response = payment_transactions_service.initiate_transaction(initiate_transaction_params)
        send_response(response)
      end

      swagger_controller :payment_transactions, "TransactionsModule APIs"
      swagger_api :initiate_transaction do
        summary "It initiates the transaction for the user"
        notes "It takes Payment ID & payment type as req and gateway type based on which it routes the API call 
               to the corresponding payment service"
        param :body, :initiate_transaction_req, :initiate_transaction_req, :required, "initiate_transaction request"
        response :forbidden, "invalid session_token"
        response :bad_request, "insufficient or invalid arguments passed in request params"
        response :unauthorized, "user not authenticated"
      end

      swagger_model :initiate_transaction_req do
        description "Transaction creation requests"
        property :transaction, :transaction_hash, 'Transaction details hash'
        property :session_token, :string, :required, "session_token to authorize user access"
      end

      swagger_model :transaction_hash do
        description "hash of transaction"
        property :payment_id, :string, :required, 'ID of payment which is to be processed'
        property :payment_type, :string, :required, 'Type of payment, membership/orders'
        property :gateway, :string, :required, 'Which gateway is used for transaction'
      end


      #
      # API to update transaction based on the response from the gateway
      #
      _LogActivity_
      def update_transaction
        deciding_params = @application_helper.get_deciding_params(params)
        payment_transactions_service = PAYMENT_TRANSACTIONS_SERVICE.new(deciding_params)
        response = payment_transactions_service.update_transaction(update_transaction_params)
        send_response(response)
      end

      swagger_api :update_transaction do
        summary "It updates the transaction for the user"
        notes "It takes the transaction id, status, gateway & gateway dependent params as request and performs
               action based on the payment type & gateway"
        param :body, :update_transaction_req, :update_transaction_req, :required, "update_transaction request"
        response :forbidden, "invalid session_token"
        response :bad_request, "insufficient or invalid arguments passed in request params"
        response :unauthorized, "user not authenticated"
      end

      swagger_model :update_transaction_req do
        description "Transaction updation requests"
        property :transaction, :transaction_update_hash, 'Transaction update details hash'
        property :session_token, :string, :required, "session_token to authorize user access"
      end

      swagger_model :transaction_update_hash do
        description "Uhash of transaction"
        property :id, :string, :required, 'ID of transaction which is to be processed'
        property :status, :string, :required, 'status of transaction'
        property :gateway, :string, :required, 'Which gateway is used for transaction'
        property :razorpay, :razorpay_hash, :optional, 'If gateway is razorpay need razorpay payment ID'
      end

      swagger_model :razorpay_hash do
        description 'razorpay hash to get details of response of razorpay response'
        property :payment_id, :string, :required, 'Razorpay transaction id to be captured to complete the payment process'
      end


      #
      # API function to initiate transaction for payment
      #
      _LogActivity_
      def record_transaction
        deciding_params = @application_helper.get_deciding_params(params)
        record_transaction_api = RECORD_TRANSACTION_API.new(deciding_params)
        response = record_transaction_api.enact(record_transaction_params)
        send_response(response)
      end

      swagger_controller :payment_transactions, "TransactionsModule APIs"
      swagger_api :record_transaction do
        summary "It records the transaction for the user"
        notes "It takes Payment ID, payment type, transaction status and gateway type based on which it routes the API call 
               to the corresponding payment service"
        param :body, :record_transaction_req, :record_transaction_req, :required, "record_transaction request"
        response :forbidden, "invalid session_token"
        response :bad_request, "insufficient or invalid arguments passed in request params"
        response :unauthorized, "user not authenticated"
      end

      swagger_model :record_transaction_req do
        description "Transaction recording requests"
        property :transaction, :record_transaction_hash, 'Transaction details hash'
        property :session_token, :string, :required, "session_token to authorize user access"
      end

      swagger_model :record_transaction_hash do
        description "hash of record transaction"
        property :payment_id, :string, :required, 'ID of payment which is to be processed'
        property :payment_type, :string, :required, 'Type of payment, membership/orders'
        property :status, :string, :required, 'status of transaction'
        property :gateway, :string, :required, 'Which gateway is used for transaction'
        property :post_order, :post_order_hash, :optional, 'If gateway is post_order need post_order payment ID'
      end

      swagger_model :post_order_hash do
        description 'post_order hash to get details of response of post_order response'
        property :reference_id, :string, :required, 'COD transaction id to be captured to complete the payment process'
        property :payment_mode, :integer, :optional, 'payment mode of transaction'
      end

      private
      
      def initiate_transaction_params
        params.require(:transaction).permit(:payment_id, :payment_type, :gateway)
      end

      def update_transaction_params
        params.require(:transaction).permit(:id, :status, :gateway, razorpay: [:payment_id])
      end

      def record_transaction_params
        params.require(:transaction).permit(:payment_id, :payment_type, :status, :gateway, post_order: [:reference_id, :payment_mode])
      end

    end
  end
end

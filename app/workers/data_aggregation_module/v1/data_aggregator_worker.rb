#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    class DataAggregatorWorker
      include Sidekiq::Worker
      sidekiq_options :queue => :default, :backtrace => true, :retry => 2
    end
  end
end

#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    class OrderAggregatorWorker < DataAggregatorWorker

      def perform
        order_index_service = DataAggregationModule::V1::OrderIndexService.new
        order_index_service.re_initialize_index
      end
    end
  end
end

#
# Module to handle functionalities related to file upload to S3
#
module FileServiceModule
  #
  # Version1 for file service module
  #
  module V1
    #
    # This class is responsible to upload file to S3
    #
    class UploadFileWorker < FileServiceWorker

      #
      # Upload the file to S3
      #
      # @param [Hash] args contains
      #  :data [String] file_path
      #  :key [key] key of the object
      #  :options [Hash] hash contains other options
      #
      def perform(args)
        file_service_util = FILE_SERVICE_UTIL.new
        file_service_util.upload_file(args)
      end
    end
  end
end

#
# Module to handle functionalities related to file upload to S3
#
module FileServiceModule
  #
  # Version1 for file service module
  #
  module V1
    #
    # This class is responsible to upload data to S3
    #
    class UploadDataWorker < FileServiceWorker

      #
      # Upload the data to S3
      #
      # @param [Hash] args contains
      #   :data [String] String to write
      #   :key [key] key of the object
      #   :options [Hash] hash contains other options
      #
      def perform(args)
        file_service_util = FILE_SERVICE_UTIL.new
        file_service_util.upload_data(args)
      end
    end
  end
end

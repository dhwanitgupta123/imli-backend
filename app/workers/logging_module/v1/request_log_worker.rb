module LoggingModule
  module V1
    class RequestLogWorker < LogWorker
 
      ACTIVITY_LOGGER_MODULE = LoggingModule::V1::ActivityLogger
      CUSTOM_ERRORS = CommonModule::V1::CustomErrors
      # 
      # function to save the log in ActivityLog table
      # 
      # @param logs [flat_hash] containing information to log
      # 
      def perform(logs) 
        activity_logger = ACTIVITY_LOGGER_MODULE.new
        begin 
          activity_logger.log_activity(logs)
        rescue CUSTOM_ERRORS::InsufficientDataError => e
          return
        end
      end

    end
  end
end

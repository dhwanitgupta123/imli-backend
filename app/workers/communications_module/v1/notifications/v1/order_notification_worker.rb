module CommunicationsModule
  module V1
    module Notifications
      module V1
        class OrderNotificationWorker < NotificationWorker

          NOTIFICATION_TYPE = CommunicationsModule::V1::Notifications::V1::NotificationType
          PARSE_SERVICE = CommunicationsModule::V1::Notifications::V1::ParseNotificationService

          # 
          # function to retrieve perform job for the worker and 
          # delegate it to corresponding instance function
          # 
          # Paramaters::
          #   * notification_attributes [flat hash] contains data points for the
          #     notification templates
          # 
          def perform(notification_attributes)
            case notification_attributes['notification_type']
            when NOTIFICATION_TYPE::PLACED_ORDER[:type]
              template = NOTIFICATION_TYPE::PLACED_ORDER[:template]
              notification_attributes = notification_attributes.merge({
                title: NOTIFICATION_TYPE::PLACED_ORDER[:title],
                action: NOTIFICATION_TYPE::PLACED_ORDER[:action],
                data: NOTIFICATION_TYPE::PLACED_ORDER[:data]
              })
            when NOTIFICATION_TYPE::PAYMENT_FAILURE[:type]
              template = NOTIFICATION_TYPE::PAYMENT_FAILURE[:template]
              notification_attributes = notification_attributes.merge({
                title: NOTIFICATION_TYPE::PAYMENT_FAILURE[:title],
                action: NOTIFICATION_TYPE::PAYMENT_FAILURE[:action],
                data: NOTIFICATION_TYPE::PAYMENT_FAILURE[:data]
              })
            when NOTIFICATION_TYPE::OUT_FOR_DELIVERY[:type]
              template = NOTIFICATION_TYPE::OUT_FOR_DELIVERY[:template]
              notification_attributes = notification_attributes.merge({
                title: NOTIFICATION_TYPE::OUT_FOR_DELIVERY[:title],
                action: NOTIFICATION_TYPE::OUT_FOR_DELIVERY[:action],
                data: NOTIFICATION_TYPE::OUT_FOR_DELIVERY[:data]
              })
            when NOTIFICATION_TYPE::DELIVERY_CANCELLED[:type]
              template = NOTIFICATION_TYPE::DELIVERY_CANCELLED[:template]
              notification_attributes = notification_attributes.merge({
                title: NOTIFICATION_TYPE::DELIVERY_CANCELLED[:title],
                action: NOTIFICATION_TYPE::DELIVERY_CANCELLED[:action],
                data: NOTIFICATION_TYPE::DELIVERY_CANCELLED[:data]
              })
            end
            deliver_push_notification(notification_attributes, template)
          end

          # 
          # function to push notification to the user asynchronously
          # 
          # Paramaters::
          #   * notification_attributes [flat hash] contains data points for the
          #     notification templates
          # 
          def deliver_push_notification(notification_attributes, template)
            PARSE_SERVICE.send(notification_attributes, template)
          end
        end
      end
    end
  end
end

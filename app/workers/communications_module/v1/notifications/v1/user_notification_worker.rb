module CommunicationsModule
  module V1
    module Notifications
      module V1
        class UserNotificationWorker < NotificationWorker

          NOTIFICATION_TYPE = CommunicationsModule::V1::Notifications::V1::NotificationType
          PARSE_SERVICE = CommunicationsModule::V1::Notifications::V1::ParseNotificationService

          # 
          # function to retrieve perform job for the worker and 
          # delegate it to corresponding instance function
          # 
          # Paramaters::
          #   * notification_attributes [flat hash] contains data points for the
          #     notification templates
          # 
          def perform(notification_attributes)
            case notification_attributes['notification_type']
            when NOTIFICATION_TYPE::SIGN_UP[:type]
              template = NOTIFICATION_TYPE::SIGN_UP[:template]
              notification_attributes = notification_attributes.merge({
                title: NOTIFICATION_TYPE::SIGN_UP[:title],
                action: NOTIFICATION_TYPE::SIGN_UP[:action],
                data: NOTIFICATION_TYPE::SIGN_UP[:data]
              })
            when NOTIFICATION_TYPE::WELCOME[:type]
              template = NOTIFICATION_TYPE::WELCOME[:template]
               notification_attributes = notification_attributes.merge({
                title: NOTIFICATION_TYPE::WELCOME[:title],
                action: NOTIFICATION_TYPE::WELCOME[:action],
                data: NOTIFICATION_TYPE::WELCOME[:data]
              })
            end
            deliver_push_notification(notification_attributes, template)
          end

          # 
          # function to push notification to the user asynchronously
          # 
          # Paramaters::
          #   * notification_attributes [flat hash] contains data points for the
          #     notification templates
          # 
          def deliver_push_notification(notification_attributes, template)
            PARSE_SERVICE.send(notification_attributes, template)
          end
        end
      end
    end
  end
end

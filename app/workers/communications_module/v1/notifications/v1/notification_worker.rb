module CommunicationsModule
  module V1
    module Notifications
      module V1
        class NotificationWorker
          include Sidekiq::Worker
          RETRY_COUNT = 2
          sidekiq_options :queue => :notification_queue, :backtrace => true, :retry => RETRY_COUNT
        end
      end
    end
  end
end

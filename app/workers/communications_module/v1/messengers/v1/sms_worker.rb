module CommunicationsModule
  module V1
    module Messengers
      module V1
        class SmsWorker
          include Sidekiq::Worker

          RETRY_COUNT = 5

          sidekiq_options :queue => :sms_queue, :backtrace => true, :retry => RETRY_COUNT

          # 
          # function to retrieve perform job for the worker and 
          # call sms service send message 
          # 
          # Paramaters::
          #   * sms_attributes [flat hash] contains data points for the sms to be send
          #
          def perform(sms_attributes, message_type)
            if Rails.env == 'production' || Rails.env == 'feature' || Rails.env == 'pre_prod'
              CommunicationsModule::V1::Messengers::V1::SmsService.send_sms(sms_attributes, message_type)
            end
            return true
          end
        end
      end
    end
  end
end

module CommunicationsModule
  module V1
    module Mailers
      module V1
        class PromotionEmailWorker < EmailWorker
        	CACHE_UTIL = CommonModule::V1::Cache
          MAILER_SERVICE = CommunicationsModule::V1::Mailers::V1::MailerService
          EMAIL_TYPE = CommunicationsModule::V1::Mailers::V1::EmailType
          # 
          # function to retrieve perform job for the worker and 
          # delegate it to corresponding instance function
          # 
          # Paramaters::
          #   * mail_attributes [flat hash] contains data points for the email templates
          # 
          def perform(mail_attributes)
            case mail_attributes['email_type']
            when EMAIL_TYPE::INVITE[:type]
              template_name = EMAIL_TYPE::INVITE[:template]
            else
              ApplicationLogger.debug('Failed to send mail')
            end
            deliver_mail_to_user(mail_attributes.merge(template_name: template_name))
          end

          # 
          # function to send welcome email to the user asynchronously
          # 
          # Paramaters::
          #   * mail_attributes [flat hash] contains data points for the email templates
          # 
          def deliver_mail_to_user(mail_attributes)
            MAILER_SERVICE.send_customize_email(mail_attributes).deliver_now
          end
        end
      end
    end
  end
end
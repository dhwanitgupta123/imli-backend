module SweeperModule
  module V1
    class SweeperWorker
      include Sidekiq::Worker
      sidekiq_options :queue => :job_queue, :retry => false
    end
  end
end

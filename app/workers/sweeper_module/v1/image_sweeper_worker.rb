module SweeperModule
  module V1
    class ImageSweeperWorker < SweeperWorker
  
      IMAGE_STATES = ImageServiceModule::V1::ModelStates::ImageStates

      # 
      # function to clean unused image ids from image table
      # 
      def perform
        clear_unused_images
      end

      private

      # 
      # This function clear the unused images from s3
      # unused images are identified by image status and image resource
      # 
      # it assumes that if image status is DELETED or image resource is blank
      # then this image is unused and need to be deleted
      #
      def clear_unused_images

        all_images = ImageServiceModule::V1::Image.ids
        all_images.each do |image_id|
          image = ImageServiceModule::V1::Image.find_by(id: image_id)
          if image.present?
            if image.status == IMAGE_STATES::DELETED || image.resource.blank?
              image.destroy
            end
          end
        end
      end
    end
  end
end

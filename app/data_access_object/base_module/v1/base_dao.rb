module BaseModule
  module V1
    class BaseDao

      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      def initialize
        @custom_error_util = CommonModule::V1::CustomErrors
      end

      #
      # this function only permit attributes required for the table
      #
      # @return [Hash] filterd params
      #
      def model_params(args, model)
        args = ActionController::Parameters.new(args)
        args.permit(model.attribute_names)
      end
    end
  end
end

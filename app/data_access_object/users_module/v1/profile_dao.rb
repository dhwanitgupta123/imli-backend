module UsersModule
  module V1
    #
    # Dao class to handle all the profile related logics
    #
    class ProfileDao < BaseModule::V1::BaseDao

      AREA_DAO = AddressModule::V1::AreaDao
      def initialize(params='')
        @params = params
      end

      #
      # Function to get area corresponding to the pincode
      #
      def get_area_from_pincode(pincode)
        area_dao = AREA_DAO.new
        return area_dao.get_area_by_pincode(pincode)
      end
    end
  end
end

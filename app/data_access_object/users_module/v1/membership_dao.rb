module UsersModule
  module V1
    #
    # Memberhsip service to handle the membership related logic 
    #
    class MembershipDao < BaseModule::V1::BaseDao

      PLAN_KEYS = CommonModule::V1::PlanKeys
      PLAN_STATES = CommonModule::V1::ModelStates::V1::PlanStates
      MEMBERSHIP_STATES = UsersModule::V1::ModelStates::MembershipStates
      MEMBERSHIP_EVENTS = UsersModule::V1::ModelStates::MembershipEvents
      MEMBERSHIP = UsersModule::V1::Membership
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      # 
      # initialize required classes
      # 
      def initialize(params)
        @params = params
      end

      # 
      # Function to get membership with Ample Sample plan
      #
      def get_ample_sample_membership(memberships)
        memberships.each do |membership|
          if membership.plan.plan_key == PLAN_KEYS::TRIAL
            return membership
          end
        end
        return nil
      end

      # 
      # Function to get the PAYMENT_PENDING membership of user
      #
      def get_payment_pending_membership(user)
        membership = user.memberships.where(workflow_state: MEMBERSHIP_STATES::PAYMENT_PENDING).first
        if membership.blank?
          raise CUSTOM_ERRORS_UTIL::ResourceNotFoundError.new(CONTENT::NO_PAYMENT_PENDING_MEMBERSHIP)
        end
        return membership
      end

      # 
      # Function to get the active membership of user
      #
      def get_only_active_membership(user)
        active_membership = user.memberships.where(workflow_state: MEMBERSHIP_STATES::ACTIVE).first
        return active_membership
      end

      # 
      # Function to get the pending membership of user
      #
      def get_only_pending_membership(user)
        pending_membership = user.memberships.where(workflow_state: MEMBERSHIP_STATES::PENDING).first
        return pending_membership
      end

      #
      # Function to get active or pending membershp of User
      #
      def get_pending_or_active_membership(user)
        user_memberships = user.memberships.where(workflow_state: [MEMBERSHIP_STATES::ACTIVE, MEMBERSHIP_STATES::PENDING])
        return user_memberships.first
      end
    end
  end
end

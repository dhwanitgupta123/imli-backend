#
# Module to handle functionalities related to transactions
#
module TransactionsModule
  #
  # Version1 for transactions module
  #
  module V1
    #
    # This class is responsible to intract with Transactions model
    #
    class TransactionsDao < BaseModule::V1::BaseDao

      TRANSACTIONS_MODEL = TransactionsModule::V1::Transaction
      TRANSACTION_STATUS = TransactionsModule::V1::TransactionStatus
      CONTENT = CommonModule::V1::Content
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      # 
      # initialize required classes
      # 
      def initialize(params = {})
        @params = params
      end

      def create_transaction(args)
        transaction = TRANSACTIONS_MODEL.new(args)
        begin
          transaction.save!
          return transaction
        rescue Mongoid::Errors::Validations => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create transaction ' + e.message)
        end
      end

      def update_transaction(args, transaction)
        begin
          transaction.update_attributes!(args)
          return transaction
        rescue Mongoid::Errors::Validations => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update transaction ' + e.message)
        end
      end

      def get_transaction_by_id(transaction_id)
        begin
          TRANSACTIONS_MODEL.find_by(id: transaction_id)
        rescue Mongoid::Errors::DocumentNotFound => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_TRANSACTION_ID)
        end
      end

      def get_transaction_for_payment_id(payment_id)
        begin
          TRANSACTIONS_MODEL.find_by(payment_id: payment_id)
        rescue Mongoid::Errors::DocumentNotFound => e
          return {}
        end
      end

      #
      # Return successfull transaction for given payment id
      #
      def get_successfull_transaction_for_payment_id(payment_id)
        begin
          TRANSACTIONS_MODEL.find_by(payment_id: payment_id, status: TRANSACTION_STATUS::SUCCESS)
        rescue Mongoid::Errors::DocumentNotFound => e
          return {}
        end
      end

    end #End of class
  end
end

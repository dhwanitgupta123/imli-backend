module CategorizationModule
  module V1
    class MpspDepartmentDao

      MPSP_DEPARTMENT_MODEL = CategorizationModule::V1::MpspDepartment
      MPSP_CATEGORY_DAO = CategorizationModule::V1::MpspCategoryDao

      # 
      # This function returns label of mpsp_department of given mpsp_department id
      #
      # @param id [Integer] id of mpsp_department
      # 
      # @return [String] return label if mpsp_department present else blank
      #
      def self.get_label_by_id(id)
        mpsp_department = get_by_id(id)
        return '' if mpsp_department.blank?

        return mpsp_department.label
      end

      # 
      # Return mpsp_department by id
      # 
      # @param id [Integer] id of mpsp_department
      # 
      # @return [Model] mpsp_department if present else nil
      #
      def self.get_by_id(id)
        return MPSP_DEPARTMENT_MODEL.find_by(id: id)
      end

      # 
      # This function return true if department is active else false
      #
      # @param mpsp_department [Object] mpsp_department
      # 
      # @return [Boolean] true if active else false
      #
      def self.is_active?(mpsp_department)
        return mpsp_department.active?
      end

      # 
      # This function set is_parent_active to true for all products within the department
      #
      # @param mpsp_department [Object] mpsp department
      # 
      def self.activate_products_within_department(mpsp_department)
        mpsp_categories = mpsp_department.mpsp_category_ids
        if mpsp_categories.present?
          MPSP_CATEGORY_DAO.activate_products_within_categories(mpsp_categories)
        end
      end

      # 
      # This function set is_parent_active to false for all products within the department
      #
      # @param mpsp_department [Object] mpsp department
      # 
      def self.deactivate_products_within_department(mpsp_department)
        mpsp_categories = mpsp_department.mpsp_category_ids
        if mpsp_categories.present?
          MPSP_CATEGORY_DAO.deactivate_products_within_categories(mpsp_categories)
        end
      end
    end
  end
end

module CategorizationModule
  module V1
    class MpspCategoryDao

      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates
      MPSP_CATEGORY_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::CategoryStates
      MPSP_CATEGORY_MODEL = CategorizationModule::V1::MpspCategory
      GENERAL_HELPER = CommonModule::V1::GeneralHelper
      MPSP_DEPARTMENT_DAO = CategorizationModule::V1::MpspDepartmentDao
      MARKETPLACE_SELLING_PACK_INDEX_SERVICE = SearchModule::V1::MarketplaceSellingPackIndexService
      MARKETPLACE_SELLING_PACK_DAO = MarketplaceProductModule::V1::MarketplaceSellingPackDao

      def self.get_aggregated_mpsp_sub_categories(mpsp_sub_categories, testing)
        #
        # Joining sub_category, brand_packs, mpbps and mpsps, this is done to reduce number of queries
        # What if this is not done : If we not join these tables then 
        # Number of querie to db = NumberOf Active SubCategories * Number of Active BrandPacks in Subcategories
        #  * Number of Active MPBPs in BrandPacks * Number of Active MPSPs in MPBPs which is huge
        # so to avoid multiple query we join these table which is obviously take more space but 
        # it reduce the db queries and hecne reduce the time to complete the request
        # 
        # After join Number of DB Queries = 4
        # 
        #
        if GENERAL_HELPER.string_to_boolean(testing)
          mpsp_sub_categories = MPSP_CATEGORY_MODEL.includes(:marketplace_selling_packs).
                                  where(id: mpsp_sub_categories, status: MPSP_CATEGORY_MODEL_STATES::ACTIVE)
          return mpsp_sub_categories
        end
        mpsp_sub_categories = MPSP_CATEGORY_MODEL.includes(:marketplace_selling_packs).
                                  where(id: mpsp_sub_categories, status: MPSP_CATEGORY_MODEL_STATES::ACTIVE,
                                        marketplace_selling_packs: {status: COMMON_MODEL_STATES::ACTIVE})
        return mpsp_sub_categories
      end

      #
      # This function returns all the categories corresponding to the department id
      #
      # @param department_id [Integer] department id
      #
      # @return [Array] array of categories model
      #
      def get_mpsp_categories_by_mpsp_department(mpsp_department_id)
        return MPSP_CATEGORY_MODEL.where(mpsp_department_id: mpsp_department_id)
      end

      # 
      # This function returns all the mpsp_sub_categories corresponding to the categories array
      #
      # @param categories [Array] array of categories
      # 
      # @return [Array] array of mpsp_sub_categories
      #
      def get_mpsp_sub_categories_by_mpsp_categories(mpsp_categories)
        return MPSP_CATEGORY_MODEL.where(mpsp_parent_category_id: mpsp_categories)
      end

      # 
      # This function returns label of mpsp_category of given mpsp_category id
      #
      # @param id [Integer] id of mpsp_category
      # 
      # @return [String] return label if mpsp_category present else blank
      #
      def self.get_label_by_id(id)
        mpsp_category = get_by_id(id)
        return '' if mpsp_category.blank?

        return mpsp_category.label
      end

      # 
      # Return mpsp_category by id
      # 
      # @param id [Integer] id of mpsp_category
      # 
      # @return [Model] mpsp_category if present else nil
      #
      def self.get_by_id(id)
        return MPSP_CATEGORY_MODEL.find_by(id: id)
      end

      # 
      # This function will check if input category is category or sub_category 
      # if category return true if category itself and its department is active else false
      # 
      # if sub_category return true if sub_category itself and its category_node is active else false
      #
      # @param mpsp_category [Object] mpsp category
      # 
      # @return [Boolean] true if current node and its parents are active else false
      #
      def self.is_node_active?(mpsp_category)
        if mpsp_category.mpsp_department_id.blank?
          return mpsp_category.active? && is_node_active?(mpsp_category.mpsp_parent_category) 
        else
          return mpsp_category.active? && MPSP_DEPARTMENT_DAO.is_active?(mpsp_category.mpsp_department) 
        end
      end

      # 
      # 
      # This function set is_parent_active to true for all products within the categories
      #
      # @param mpsp_categories [Object] mpsp categories
      #
      def self.activate_products_within_categories(mpsp_categories)
        mpsp_sub_categories = get_sub_categories_by_categories(mpsp_categories)
        if mpsp_sub_categories.present?
          activate_products_within_sub_categories(mpsp_sub_categories)
        end
      end

      # 
      # 
      # This function set is_parent_active to false for all products within the categories
      #
      # @param mpsp_categories [Object] mpsp categories
      #
      def self.deactivate_products_within_categories(mpsp_categories)
        mpsp_sub_categories = get_sub_categories_by_categories(mpsp_categories)
        if mpsp_sub_categories.present?
          deactivate_products_within_sub_categories(mpsp_sub_categories)
        end
      end

      # 
      # This function return mpsp_sub_categories by mpsp_categories
      #
      # @param mpsp_categories [Object] mpsp categories
      # 
      # @return [Array] Array of mpsp_sub_categories
      #
      def self.get_sub_categories_by_categories(mpsp_categories)
        MPSP_CATEGORY_MODEL.where(mpsp_parent_category_id: mpsp_categories)
      end

      # 
      # 
      # This function set is_parent_active to true for all products within the sub_categories
      #
      # @param mpsp_sub_categories [Object] mpsp sub_categories
      #
      def self.activate_products_within_sub_categories(mpsp_sub_categories)
        marketplace_selling_pack_dao = MARKETPLACE_SELLING_PACK_DAO.new
        marketplace_selling_packs = marketplace_selling_pack_dao.get_mpsps_by_mpsp_sub_categories(mpsp_sub_categories)

        return if marketplace_selling_packs.blank?

        marketplace_selling_pack_index_service = MARKETPLACE_SELLING_PACK_INDEX_SERVICE.new
        marketplace_selling_pack_index_service.activate_mpsps_parent_status(marketplace_selling_packs)
      end

      # 
      # 
      # This function set is_parent_active to false for all products within the sub_categories
      #
      # @param mpsp_sub_categories [Object] mpsp sub_categories
      #
      def self.deactivate_products_within_sub_categories(mpsp_sub_categories)
        marketplace_selling_pack_dao = MARKETPLACE_SELLING_PACK_DAO.new
        marketplace_selling_packs = marketplace_selling_pack_dao.get_mpsps_by_mpsp_sub_categories(mpsp_sub_categories)

        return if marketplace_selling_packs.blank?

        marketplace_selling_pack_index_service = MARKETPLACE_SELLING_PACK_INDEX_SERVICE.new
        marketplace_selling_pack_index_service.deactivate_mpsps_parent_status(marketplace_selling_packs)
      end
    end
  end
end

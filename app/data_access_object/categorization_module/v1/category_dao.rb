module CategorizationModule
  module V1
    class CategoryDao

      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates
      CATEGORY_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::CategoryStates
      CATEGORY_MODEL = CategorizationModule::V1::Category

      def self.get_aggregated_sub_categories(sub_categories)
        #
        # Joining sub_category, brand_packs, mpbps and mpsps, this is done to reduce number of queries
        # What if this is not done : If we not join these tables then 
        # Number of querie to db = NumberOf Active SubCategories * Number of Active BrandPacks in Subcategories
        #  * Number of Active MPBPs in BrandPacks * Number of Active MPSPs in MPBPs which is huge
        # so to avoid multiple query we join these table which is obviously take more space but 
        # it reduce the db queries and hecne reduce the time to complete the request
        # 
        # After join Number of DB Queries = 4
        # 
        #
        sub_categories = CategorizationModule::V1::Category.includes(brand_packs: [marketplace_brand_packs: :marketplace_selling_packs]).
                                  where(id: sub_categories, status: CATEGORY_MODEL_STATES::ACTIVE, 
                                        marketplace_selling_packs: {status: COMMON_MODEL_STATES::ACTIVE})

      end

      #
      # This function returns all the categories corresponding to the department id
      #
      # @param department_id [Integer] department id
      #
      # @return [Array] array of categories model
      #
      def get_categories_by_department(department_id)
        return CATEGORY_MODEL.where(department_id: department_id)
      end

      # 
      # This function returns all the sub_categories corresponding to the categories array
      #
      # @param categories [Array] array of categories
      # 
      # @return [Array] array of sub_categories
      #
      def get_sub_categories_by_categories(categories)
        return CATEGORY_MODEL.where(parent_category_id: categories)
      end
    end
  end
end
module CreditsModule
  module V1
    module AmpleCreditMapper
      # 
      # map ample_credit model to hash
      #
      # @param ample_credit [Model] ample_credit
      # 
      # @return [Hash] mapped hash from ampe_credit model
      #
      def self.map_ample_credit_to_hash(ample_credit)
        
        return {} if ample_credit.blank?

        {
          balance: ample_credit.balance,
          currency: ample_credit.currency,
          user: {
              id: ample_credit.user_id,
            },
          max_limit: ample_credit.max_limit,
          min_limit: ample_credit.min_limit
        }
      end
    end
  end
end

module CategorizationModule
  module V1
    #
    # Module to generate responses & other functionalities
    #
    module MpspCategorizationApiHelper
      #
      # defining global variable with versioning
      #
      MPSP_CATEGORY_SERVICE_HELPER = CategorizationModule::V1::MpspCategoryServiceHelper
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper

      # 
      # Create response of a mpsp_department
      #
      def self.get_mpsp_departments_hash(mpsp_departments)
        mpsp_departments_hash = []
        mpsp_departments.each do |mpsp_department|
          mpsp_departments_hash.push(mpsp_department)
        end
        return mpsp_departments_hash
      end

      # 
      # Create mapping of mpsp_department with mpsp_categories
      # 
      def self.map_mpsp_department_to_hash(mpsp_department, active_mpsp_categories_hash)
          response = {
            id: mpsp_department.id,
            label: mpsp_department.label,
            images: IMAGE_SERVICE_HELPER.get_images_hash_by_ids(mpsp_department.images),
            status: mpsp_department.status,
            description: mpsp_department.description,
            priority: mpsp_department.priority,
            categories: active_mpsp_categories_hash
          }
        return response
      end
    end
  end
end
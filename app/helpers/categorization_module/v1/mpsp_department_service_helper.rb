module CategorizationModule
  module V1
    #
    # Module to generate DepartmentService responses & other functionalities
    #
    # @author [anuj]
    #
    module MpspDepartmentServiceHelper
      #
      # defining global variable with versioning
      #
      MPSP_CATEGORY_RESPONSE_DECORATOR = CategorizationModule::V1::MpspCategoryResponseDecorator
      MPSP_DEPARTMENT_RESPONSE_DECORATOR = CategorizationModule::V1::MpspDepartmentResponseDecorator
      CATEGORY_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::CategoryStates
      IMAGE_SERVIC_HELPER = ImageServiceModule::V1::ImageServiceHelper

      #
      # Create response with a list of all the mpsp_department
      #
      def self.get_mpsp_departments_hash(mpsp_departments)
        mpsp_departments_hash = []
        mpsp_departments.each do |mpsp_department|
          mpsp_department = create_mpsp_department_hash(mpsp_department)
          mpsp_departments_hash.push(mpsp_department)
        end
        return mpsp_departments_hash
      end

      #
      # create hash of deparment object attributes
      # 
      def self.create_mpsp_department_hash(mpsp_department)
        response = {
          id: mpsp_department.id,
          label: mpsp_department.label,
          description: mpsp_department.description,
          priority: mpsp_department.priority,
          status: mpsp_department.status,
          images: IMAGE_SERVIC_HELPER.get_images_hash_by_ids(mpsp_department.images)
        }
        return response
      end

      # 
      # This will create mpsp_departments to mpsp_categories to mpsp_sub_categories tree
      # it also include one dummy deparment, mpsp_category and mpsp_sub_category which
      # is ALL which is required in panel
      #
      # @param aggregated_data [Model] Joined mpsp_department, mpsp_categories and mpsp_sub_categories
      # 
      # @return [Array] Aggregated array where mpsp_department is root node
      #
      def self.get_aggregated_data_array(aggregated_data)
        mpsp_departments = []
        return mpsp_departments if aggregated_data.blank?

        aggregated_data.each do |data|
          mpsp_departments.push({
              id: data.id,
              label: data.label,
              categories: get_mpsp_categories_array(data.mpsp_categories)
            })
        end
        return mpsp_departments
      end

      #
      # Creates mpsp_categories to mpsp_sub_categories tree with dummy mpsp_category and mpsp_sub_category
      #
      def self.get_mpsp_categories_array(mpsp_categories)
        mpsp_categories_array = []
        
        return mpsp_categories_array if mpsp_categories.blank?

        mpsp_categories.each do |mpsp_category|
          mpsp_categories_array.push({
              id: mpsp_category.id,
              label: mpsp_category.label,
              sub_categories: get_mpsp_sub_categories_array(mpsp_category.mpsp_sub_categories)
            })
        end
        return mpsp_categories_array
      end

      #
      # Creates mpsp_sub_categories array with dummy mpsp_sub_category
      #
      def self.get_mpsp_sub_categories_array(mpsp_sub_categories)
        mpsp_sub_categories_array = []

        return mpsp_sub_categories_array if mpsp_sub_categories.blank?
        
        mpsp_sub_categories.each do |mpsp_sub_category|
          next if mpsp_sub_category.status == CATEGORY_MODEL_STATES::DELETED
          mpsp_sub_categories_array.push({
              id: mpsp_sub_category.id,
              label: mpsp_sub_category.label,
            })
        end
        return mpsp_sub_categories_array
      end
    end
  end
end

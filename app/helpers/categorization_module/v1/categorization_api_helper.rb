module CategorizationModule
  module V1
    #
    # Module to generate responses & other functionalities
    #
    module CategorizationApiHelper
      #
      # defining global variable with versioning
      #
      CATEGORY_SERVICE_HELPER = CategorizationModule::V1::CategoryServiceHelper
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper

      # 
      # Create response of a department
      #
      def self.get_departments_hash(departments)
        departments_hash = []
        departments.each do |department|
          departments_hash.push(department)
        end
        return departments_hash
      end

      # 
      # Create mapping of department with categories
      # 
      def self.map_department_to_hash(department, active_categories_hash)
          response = {
            id: department.id,
            label: department.label,
            images: IMAGE_SERVICE_HELPER.get_images_hash_by_ids(department.images),
            status: department.status,
            description: department.description,
            priority: department.priority,
            categories: active_categories_hash
          }
        return response
      end
    end
  end
end
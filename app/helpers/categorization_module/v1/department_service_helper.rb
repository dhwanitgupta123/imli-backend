module CategorizationModule
  module V1
    #
    # Module to generate DepartmentService responses & other functionalities
    #
    # @author [anuj]
    #
    module DepartmentServiceHelper
      #
      # defining global variable with versioning
      #
      CATEGORY_RESPONSE_DECORATOR = CategorizationModule::V1::CategoryResponseDecorator
      DEPARTMENT_RESPONSE_DECORATOR = CategorizationModule::V1::DepartmentResponseDecorator
      CATEGORY_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::CategoryStates
      #
      # Create response with a list of all the department
      #
      def self.get_departments_hash(departments)
        departments_hash = []
        departments.each do |department|
          department = DEPARTMENT_RESPONSE_DECORATOR.create_department_hash(department)
          departments_hash.push(department)
        end
        return departments_hash
      end

      # 
      # This will create departments to categories to sub_categories tree
      # it also include one dummy deparment, category and sub_category which
      # is ALL which is required in panel
      #
      # @param aggregated_data [Model] Joined department, categories and sub_categories
      # 
      # @return [Array] Aggregated array where department is root node
      #
      def self.get_aggregated_data_array(aggregated_data)
        departments = []

        return departments if aggregated_data.blank?

        aggregated_data.each do |data|
          departments.push({
              id: data.id,
              label: data.label,
              categories: get_categories_array(data.categories)
            })
        end

        return departments
      end

      #
      # Creates categories to sub_categories tree with dummy category and sub_category
      #
      def self.get_categories_array(categories)
        categories_array = []
        
        return categories_array if categories.blank?

        categories.each do |category|
          categories_array.push({
              id: category.id,
              label: category.label,
              sub_categories: get_sub_categories_array(category.sub_categories)
            })
        end
        return categories_array
      end

      #
      # Creates sub_categories array with dummy sub_category
      #
      def self.get_sub_categories_array(sub_categories)
        sub_categories_array = []

        return sub_categories_array if sub_categories.blank?
        
        sub_categories.each do |sub_category|
          next if sub_category.status == CATEGORY_MODEL_STATES::DELETED
          sub_categories_array.push({
              id: sub_category.id,
              label: sub_category.label,
            })
        end
        return sub_categories_array
      end
    end
  end
end

#
# Module handles all the functionality related to Categorization of products
# 
module CategorizationModule
  #
  # Categorization module Version 1
  # 
  module V1 
    #
    # Module to generate CategoryService responses & other functionalities
    # 
    # @author [anuj]
    #
    module CategoryServiceHelper

      #
      #defining global variable with versioning
      # 
      CATEGORY_RESPONSE_DECORATOR = CategorizationModule::V1::CategoryResponseDecorator

      # 
      # Create response of a department with all the categories
      # 
      def self.get_categories_hash(categories)
        if categories.blank?
          return []
        end
        categories_hash = []
        categories.each do |category|
          category = CATEGORY_RESPONSE_DECORATOR.create_category_hash(category)
          categories_hash.push(category)
        end
        return categories_hash
      end

      # 
      # Create response of a category with all the sub categories
      # 
      def self.get_sub_categories_hash(sub_categories)
        if sub_categories.blank?
          return [];
        end
        sub_categories_hash = []
        sub_categories.each do |sub_category|
          sub_category = CATEGORY_RESPONSE_DECORATOR.create_sub_category_hash(sub_category)
          sub_categories_hash.push(sub_category)
        end
        return sub_categories_hash
      end
    end
  end
end
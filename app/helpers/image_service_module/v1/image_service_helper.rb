#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    #
    # It contain commmon image service helper functions
    #
    module ImageServiceHelper

      IMAGE_MODEL = ImageServiceModule::V1::Image
      CACHE_UTIL = CommonModule::V1::Cache
      IMAGE_NAMESPACE = 'IMAGES'
      IMAGE_URLS_TTL  = 70 * 24 * 60 * 60 # in seconds
      CDN_URL = APP_CONFIG['config']['CDN_URL']

      #
      # This function return the image_id_array sorted by priority attribute
      # of image_hash_array
      # example : image_hash_array = [ {priority: 2, image_id: im_id1} {priority: 1, image_id: im_id2}]
      # in this case return image_id_array will be [im_id2, im_id1]
      #
      # @param image_hash_array [Array] array of hash where each hash contain image_id and priority
      #
      # @return image_id_array [Array] list of image_id sorted by priority
      #
      def self.get_sorted_image_id_array_by_priority(image_hash_array)
        return [] if image_hash_array.blank?
        image_hash_array_sorted = image_hash_array.sort_by{ |image_hash| image_hash['priority'].to_i }
        image_id_array = []

        image_hash_array_sorted.each do |image_hash|
          image_id_array.push(image_hash['image_id'])
        end
        return image_id_array
      end

      #
      # Return image if image corresponding to image_id is present else return nil
      #
      # @param [String] image id
      #
      def self.get_image_by_id(image_id)
        IMAGE_MODEL.find_by(id: image_id)
      end

      #
      # Return the hash containg key as image_id which point to hash
      # of image_style_hash
      # example :
      # image_hash[:image_id] => {
      #  {
      #   original: 'url/original/image.jpg'
      #   medium: 'url/medium/image.jpg'
      #   thumb:   'url/thumb/image.jpg'
      #  }
      # }
      #
      # @param image_id_array [Array] array of image_ids
      #
      # @return [Array] Returns the image hash
      #
      def self.get_images_hash_by_ids(image_id_array, style_filter = {})
        image_hash_array = []
        return image_hash_array if image_id_array.blank?
        priority = 1
        image_id_array.each do |image_id|
          image_hash = get_cached_image_hash(image_id, priority.to_s)
          image_hash = get_image_from_db(image_id, priority.to_s) if image_hash.blank?
          
          next if image_hash.blank?
          image_hash[:urls] =  filter_styles(image_hash[:urls], style_filter) if style_filter.present?
          image_hash_array.push(image_hash)
          priority += 1
        end

        return image_hash_array
      end

      #
      # This function only return the required styles 
      #
      def self.filter_styles(image_urls, style_filter)
        return image_urls if image_urls.size <= 1
        image_urls.symbolize_keys!

        filtered_hash = {}
        style_keys = style_filter.keys

        style_keys.each do |key|
          filtered_hash[key] = image_urls[key]
        end
        return filtered_hash
      end

      #
      # This function call Image Model to get the image urls
      # and also put them in cache
      #
      # @param image_id [Integer] Image id
      # @param priority [String] priority of image
      # 
      # @return [Hash] containg styles and corresponding urls
      #
      def self.get_image_from_db(image_id, priority)
        image = get_image_by_id(image_id)
        return {} if image.nil?

        image_hash = {
                priority: priority, 
                image_id: image_id
              }

        if image.image_local.present?

          image_urls = {
                        orignal: image.image_local.url
                      }
          image_hash[:urls] = image_urls

        else

          image_urls = get_all_styles(image)
          image_hash[:urls] = image_urls
        end

        cache_image_urls(image_id, image_urls)

        return image_hash
      end

      # 
      # This function cache the image_urls
      #
      # @param image_id [Integer] Image id
      # @param image_urls [Hash] containg styles and corresponding urls
      #
      def self.cache_image_urls(image_id, image_urls)
        CACHE_UTIL.write({ namespace: IMAGE_NAMESPACE,
                           key: image_id.to_s,
                           value: image_urls.to_json,
                           expire: IMAGE_URLS_TTL
                          })
      end

      # 
      # This functin return checks if image_urls present in cache if present then return 
      # image_hash from cache
      # 
      # @param image_id [Integer] Image Id
      # @param priority [String] Priority of image
      # 
      # @return [Hash] containg styles and corresponding urls
      #
      def self.get_cached_image_hash(image_id, priority)

        image_urls = CACHE_UTIL.read_json(image_id.to_s, IMAGE_NAMESPACE)

        return {} if image_urls.blank?
        
        return {
                  priority: priority,
                  image_id: image_id,
                  urls: image_urls
                }
      end

      #
      # This function update the image_url in cache if it is present
      #
      # @param image [Model] Image model object
      #
      def self.update_cached_urls(image)
        
        if CACHE_UTIL.read_json(image.id.to_s, IMAGE_NAMESPACE).present?
          CACHE_UTIL.write({ namespace: IMAGE_NAMESPACE,
                             key: image.id.to_s, 
                             value: get_all_styles(image).to_json,
                             expire: IMAGE_URLS_TTL
                          })
        end
      end

      #
      # This function return the hash consist of url corresponding to each image style
      # example {
      #   original: 'url/original/image.jpg'
      #   medium: 'url/medium/image.jpg'
      #   thumb:   'url/thumb/image.jpg'
      # }
      #
      # @param image [ModelObject] image object
      #
      # @return [Hash] containing url corresponding to style
      #
      def self.get_all_styles(image)
        image_style_hash = {}
        image_style_hash[:original] = get_cdn_url(image.image_s3)

        S3_IMAGE_STYLES.keys.each do |style|
          image_style_hash[style] = get_cdn_url(image.image_s3, style)
        end
        return image_style_hash
      end

      # 
      # This function returns CDN URL corresponding to the S3 bucket
      # if CDN url is present else it return s3 url
      #
      # @param image_s3 [Hash] image_s3 containing s3 image urls
      # @param style [String] style of image
      # 
      # @return [String] iamge url
      #
      def self.get_cdn_url(image_s3, style = 'original')
        if CDN_URL.present?
          return CDN_URL + image_s3.path(style)
        end

        return image_s3.url(style)
      end

      #
      # Return false if any of the image_id not present in image table
      #
      # @param image_id_array [Array] array containing all the image_id
      #
      # @return [Boolean] true if all image_ids are present else false
      #
      def self.validate_images(image_id_array)
        return true if image_id_array.blank?

        image_id_array = image_id_array.map(&:to_i)

        valid_image_ids = IMAGE_MODEL.ids
        remaining_image_ids = image_id_array - valid_image_ids
  
        return false if remaining_image_ids.present?
        return true
      end

      # 
      # This function bind the image with model
      #
      # @param image_id_array [Array] Array of image ids
      # @param resource_name [String] Mode name
      #
      def self.bind_image_with_resource(image_id_array, resource_name)

        return nil if image_id_array.blank?

        image_id_array.each do |image_id|
          image = get_image_by_id(image_id)
          if image.present?
            image.resource = resource_name
            image.save
          end
        end
      end
    end
  end
end

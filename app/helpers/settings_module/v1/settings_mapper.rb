module SettingsModule
  module V1
    module SettingsMapper

      def self.get_static_settings_response_hash(settings)
        return {
          'MAP': settings['MAP'],
          'FEATURES': settings['FEATURES'],
          'CONSTANTS': settings['CONSTANTS']
        }
      end

      def self.get_dynamic_settings_response_hash(settings)
        return settings
      end

    end
  end
end
#
# Module to handle functionalities related to transactions
#
module TransactionsModule
  #
  # Version1 for transactions module
  #
  module V1

    module TransactionHelper

      TRANSACTIONS_DAO = TransactionsModule::V1::TransactionsDao      

      #
      # Get transaction details for payment
      #
      # @param payment [Object] [Payment model object]
      #
      # @return [JSON] [Hash of transaction data]
      #
      def self.get_transaction_details_for_payment(payment)
        return {} if payment.blank?
        transaction_dao = TRANSACTIONS_DAO.new
        transaction = transaction_dao.get_successfull_transaction_for_payment_id(payment.id)
        return {} if transaction.blank?
        return get_transaction_hash(transaction)
      end

      #
      # Create transaction hash from Transaction Object
      #
      # @param transaction [Object] [Transaction model object]
      #
      # @return [JSON] [Hash of transaction]
      #
      def self.get_transaction_hash(transaction)
        return {} if transaction.blank?
        return {
          payment_id: transaction.payment_id,
          amount: transaction.amount,
          transaction_data: transaction.transaction_data,
          status: transaction.status,
          user_id: transaction.user_id,
          user_email_id: transaction.user_email_id,
          gateway: transaction.gateway,
          display_gateway: TransactionsModule::V1::GatewayType.get_display_gateway_by_type(transaction.gateway),
          reference_id: get_reference_id(transaction)
        }
      end

      # 
      # This function returns reference id according to the gateway
      # @param transaction [type] [description]
      # 
      # @return [String] reference_id
      #
      def self.get_reference_id(transaction)
        if transaction.transaction_data.present? && transaction.transaction_data['post_order'].present?
          return transaction.transaction_data['post_order']['reference_id']
        elsif transaction.gateway == TransactionsModule::V1::GatewayType::CITRUS_WEB && transaction.transaction_data.present?
          return transaction.transaction_data['TxId']
        elsif transaction.gateway == TransactionsModule::V1::GatewayType::RAZORPAY && transaction.transaction_data.present?
          razorpay_response = transaction.transaction_data['razorpay']
          if razorpay_response.present?
            return JSON.parse(razorpay_response)['id']
          end
        end
        return ''
      end

    end #End of class
  end
end

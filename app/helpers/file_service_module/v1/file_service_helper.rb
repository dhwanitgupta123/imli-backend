#
# Module to handle functionalities related to image upload to S3
#
module FileServiceModule
  #
  # Version1 for file service module
  #
  module V1
    #
    # It contain commmon file service helper functions
    #
    module FileServiceHelper

      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      FILE_SERVICE_UTIL = FileServiceModule::V1::FileServiceUtil
      FILE_SERVICE = FileServiceModule::V1::FileService
      CONTENT_UTIL= CommonModule::V1::Content


      #
      # This function does the following in succession:
      # - Upload the data privately to S3 synchronously, this function gets the object with given key
      # from the S3_BUCKET and create or override the content
      # - Next, it creates a signed URL for the uploaded file
      #
      # @param data [String] file_path
      # @param key [key] key of the object
      # @param expires [ActiveSupport::Duration] duration for which link should be active, example (5.minutes, 10.hours)
      #
      #
      # @return [String] signed url of the file uploaded
      # Raises Exception:
      # -[FileNotFound] if file_path passed is non-existent
      # -[InsufficientDataError] if expires param is blank or isn't an Object of
      #                           ActiveSupport::Duration class
      #
      def self.upload_file_to_s3_and_get_signed_url(file_path, key, expires)
        if expires.blank? || expires.class != ActiveSupport::Duration
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::INVALID_DURATION)
        end
        raise CUSTOM_ERROR_UTIL::FileNotFound.new('file does not exist, file_path: ' + file_path.to_s) unless File.file?(file_path)
        file_service_util = FILE_SERVICE_UTIL.new
        file_service = FILE_SERVICE.new({})
        s3_key = file_service_util.get_s3_key(key)
        s3_key = S3_PRIVATE_FOLDER + '/' + s3_key
        s3_upload_options = {
          acl: 'private'
        }
        s3_url = file_service.upload_file_to_s3(file_path, s3_key, s3_upload_options)
        signed_s3_url = file_service.get_signed_s3_url(s3_url[:path], expires)

        return signed_s3_url
      end
    end
  end
end

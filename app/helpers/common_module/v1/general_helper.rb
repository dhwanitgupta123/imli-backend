# General Helper containing common functions which are used
# by different service objects
# @author Sumit <sumitchhuttani007@gmail.com>
module CommonModule
  module V1
    module GeneralHelper

      # regular expression for the validation if phone number is in valid format or not
      VALID_PHONE_REGEX = CommonModule::V1::RegexUtil::VALID_PHONE_NUMBER_REGEX
      # regular expression for the validation if email id is in valid format or not
      VALID_EMAIL_REGEX = CommonModule::V1::RegexUtil::VALID_EMAIL_REGEX
      # regular expression for the validation of pincode
      VALID_PINCODE_REGEX = CommonModule::V1::RegexUtil::PINCODE_REGEX

      # 
      # Converts string to class after replacing named group version
      # @param string_to_replace [String] [string which need to convert to class]
      # @param version [String] [version name which is to be put in provided string]
      # 
      # @return [Class Object] [Class object corresponding to passed string]
      def self.string_to_class_with_version(string_to_replace, version)
        class_string = string_to_replace % {:version => version}
        class_string.constantize
      end

      def self.string_to_class(class_string)
        begin
          class_string.constantize
        rescue
          return nil
        end
      end

      #
      # Converts string to class
      # @param class_string [String] [string which need to convert to class]
      #
      # @return [Class Object] [Class object corresponding to passed string]
      def self.string_to_class(class_string)
        class_string.constantize
      end

      #
      # funtion to convert string to boolean
      #
      def self.string_to_boolean(string)
        string =  string.to_s.downcase
        if string == 'true'
          return true
        else
          return false
        end
      end
      #
      # Check whether given phone number is valid or not
      # @param phone_number [String] phone number to be validated
      # Description::
      #   * VALID_PHONE_REGEX = /\A\[1..9]|\d{9}\z/ 
      #       * Regex makes sure that phone_number parameter is of exact 10 digits 
      #       * It also ensures that first digit isn't zero
      # 
      # @return [Boolean] true if valid phone number is passed else false
      def self.phone_number_valid?(phone_number)
        match_result = (phone_number =~ VALID_PHONE_REGEX)
        if match_result == 0
          return true 
        else
          return false
        end
      end

      # Add prefix "BLOCKED::" and return the key
      # 
      # @param phone_number [String] phone_number
      # 
      # @return [String] appended phone_number
      # 
      def self.get_blocked_user_key(phone_number)
        blocked_prefix = Cache.read('BLOCKED_PREFIX') || "BLOCKED::" 
        return blocked_prefix + phone_number
      end

      #
      # Check whether email_id is valid or not
      # @param email_id [String] email id to be validated
      # Description::
      #   * VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
      #       * Regex makes sure that email id parameter is in correct format
      # 
      # @return [Boolean] true if valid email id is passed else false
      def self.email_id_valid?(email_id)
        match_result = VALID_EMAIL_REGEX.match(email_id)
        if match_result.blank?
          return false 
        else
          return true
        end
      end
    end
  end
end

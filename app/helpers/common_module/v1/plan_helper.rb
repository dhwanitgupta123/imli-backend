module CommonModule 
  module V1
    # 
    # Helper for serializing PLAN Object model
    #
    module PlanHelper
      BENEFIT_HELPER = CommonModule::V1::BenefitHelper
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper
      IMAGE_STYLES_UTIL = ImageServiceModule::V1::ImageStyles

      # 
      # Get PLAN details
      # @param plan [PLAN Object] [Model Plan object whose details are needed]
      # 
      # @return [JSON] [Hash of relevant Plan fields]
      def self.get_plan_details(plan)
        return {} if plan.blank?
        return {
          id: plan.id,
          name: plan.name,
          details: plan.details,
          duration: plan.duration,
          validity_label: plan.validity_label,
          fee: plan.fee,
          plan_key: plan.plan_key,
          status: plan.status,
          benefits: BENEFIT_HELPER.get_active_benefits_array(plan.benefits),
          images: IMAGE_SERVICE_HELPER.get_images_hash_by_ids(plan.images, IMAGE_STYLES_UTIL::PLAN_IMAGE_STYLE_FILTERS)
        }
      end

      # 
      # Get Array of PLANs
      # @param plans [Array] [Array of PLAN Object]
      # 
      # @return [Array] [Array containing hashes of relevant attributes of PLAN model]
      def self.get_plans_array(plans)
        plans_array = []
        return plans_array if plans.blank?
        plans.each do |plan|
          plans_array.push(get_plan_details(plan))
        end
        return plans_array
      end
    end
  end
end

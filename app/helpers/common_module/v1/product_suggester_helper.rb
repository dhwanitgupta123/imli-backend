module CommonModule 
  module V1
    # 
    # Helper for serializing ProductSuggester Object model
    #
    module ProductSuggesterHelper

      #
      # Create response with a list of all the suggestions
      #
      def self.get_product_suggestions_hash(suggestions)
        suggestions_hash = []
        return suggestions_hash if suggestions.blank?
        suggestions.each do |suggestion|
          suggestion = create_suggestion_hash(suggestion)
          suggestions_hash.push(suggestion)
        end
        return suggestions_hash
      end

      #
      # create hash of product suggestion object attributes
      # 
      def self.create_suggestion_hash(suggestion)
        response = {
          user_id: suggestion[:user_id],
          products: suggestion[:product_suggestions]
        }
        return response
      end
   	end
  end
end
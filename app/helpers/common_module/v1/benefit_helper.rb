module CommonModule 
  module V1
    # 
    # Helper for serializing PLAN Object model
    #
    module BenefitHelper

      BENEFIT_MODEL_STATES = CommonModule::V1::ModelStates::V1::BenefitStates
      BENEFIT_ENUMS_UTIL = CommonModule::V1::BenefitEnum
      TRIGGER_ENUMS_UTIL = TriggersModule::V1::TriggerEnum
      CACHE = CommonModule::V1::Cache

      #
      # Get Benefits Details
      # @param benefit [Benefit model] [benefit whose details are needed]
      #
      # @return [JSON] [JSON containing benefit details]
      #
      def self.get_benefit_details(benefit)
        if benefit.blank?
          return {}
        end
        return {
          id: benefit.id,
          name: benefit.name,
          details: benefit.details,
          condition: benefit.condition,
          reward_value: benefit.reward_value,
          reward_type: benefit.reward_type,
          status: benefit.status
        }
      end

      #
      # Get benefits array (ALL Benefits)
      #
      def self.get_benefits_array(benefits)
        if benefits.blank?
          return []
        end
        benefits_array = []
        benefits.each do |benefit|
          benefits_array.push(get_benefit_details(benefit))
        end
        return benefits_array
      end

      #
      # Get ONLY ACTIVE benefits
      # It returns an array of all active benefits hash
      #
      def self.get_active_benefits_array(benefits)
        if benefits.blank?
          return []
        end
        benefits_array = []
        benefits.each do |benefit|
          if benefit.active?
            benefits_array.push(get_benefit_details(benefit))
          end
        end
        return benefits_array
      end

      # 
      # params for ample credit benefit
      # 
      # @return [Hash] benefit args
      #
      def self.get_ample_credit_benefit_args
        
        benefit_limit = CACHE.read_int('AMPLE_CREDIT_BENEFIT_LIMIT') || 5
        refer_limit = CACHE.read_int('AMPLE_CREDIT_REFER_LIMIT') || 2**31 - 1

        {
          benefit_type: BENEFIT_ENUMS_UTIL.get_benefit_type_by_label('AMPLE_CREDITS'),
          status: BENEFIT_MODEL_STATES::ACTIVE,
          refer_limit:  refer_limit,
          benefit_limit: benefit_limit,
          benefit_to_user: BENEFIT_ENUMS_UTIL.get_benefit_id_by_benefit('ADD_AMPLE_CREDIT_TO_USER'),
          benefit_to_referrer: BENEFIT_ENUMS_UTIL.get_benefit_id_by_benefit('ADD_AMPLE_CREDIT_TO_REFERRER'),
          trigger_point_of_user: TRIGGER_ENUMS_UTIL.get_trigger_id_by_trigger('ON_REFERRAL_ACTIVATION'),
          trigger_point_of_referrer: TRIGGER_ENUMS_UTIL.get_trigger_id_by_trigger('AFTER_FIRST_ORDER'),
          is_global: false
        }
      end

    end
  end
end
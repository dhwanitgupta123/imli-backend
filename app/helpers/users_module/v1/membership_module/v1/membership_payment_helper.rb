module UsersModule::V1
  #
  # Membership module to handle membership related functionalities
  #
  module MembershipModule::V1
    #
    # modoule to create JSON object of membership payment
    #
    module MembershipPaymentHelper

      PAYMENT_HELPER = MarketplaceOrdersModule::V1::PaymentHelper
      MEMBERSHIP_PAYMENT_MODES = UsersModule::V1::MembershipModule::V1::MembershipPaymentModes

      def self.get_payment_details(payment)
        response = {
          id: payment.id,
          mode: payment.mode || '',
          grand_total: payment.grand_total.round(2) || 0.0,
          billing_amount: payment.billing_amount.round(2) || 0.0,
          refund: payment.refund.round(2) || 0.0,
          total_vat_tax: payment.total_vat_tax.round(2) || 0.0,
          total_service_tax: payment.total_service_tax.round(2) || 0.0,
          payment_mode_savings: payment.payment_mode_savings.round(2) || 0.0,
          discount: payment.discount.round(2) || 0.0,
          gift_card: payment.gift_card.round(2) || 0.0,
          total_savings: payment.total_savings.round(2) || 0.0,
          net_total: payment.net_total.round(2) || 0.0,
          status: payment.status || ''
        }
      end


      #
      # Function to calculate payment mode related details for all modes
      #
      def self.calculate_payment_mode_details(payment_args)
        payment_details = PAYMENT_HELPER.fetch_payment_details_hash
        details_array = []
        payment_details.keys.each do |key|
          if MEMBERSHIP_PAYMENT_MODES::ALLOWED_MODES.include?(key.to_i)
            details_hash = payment_details[key]
            payment_mode_details = compute_payment_mode_details(details_hash, payment_args[:billing_amount])

            details_hash['mode'] = key.to_s
            details_hash['net_total'] = payment_mode_details[:net_total]
            details_hash['total_savings'] = payment_mode_details[:total_savings]

            details_array.push(details_hash)
          end
        end
        return details_array
      end

      # 
      # Function to make hash of mode basis payment details
      #
      def self.compute_payment_mode_details(payment_mode_details, billing_amount)
        # Fetch payment mode percentage discount
        # Convert string to BigDecimal, since DB values are all BigDecimal
        payment_mode_savings = payment_mode_details['payment_mode_savings'].to_d  # to decimal
        payment_mode_savings = BigDecimal.new('0.0') if payment_mode_savings.blank?
        # Compute additional discount
        additional_discount = billing_amount * (payment_mode_savings / 100)

        net_total = (billing_amount - additional_discount).round(2)

        return { net_total: net_total, total_savings: additional_discount }
      end
    end
  end
end

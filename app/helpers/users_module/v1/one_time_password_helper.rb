# 
# Helper to facilitate one time password generation
# and validation
# 
# @author [anuj]
#
module UsersModule
  module V1
    module OneTimePasswordHelper
      # Store util class with versioning in global variable
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      # 
      # static function to verify OTP if it matches with our system & is not expired
      # 
      # Parameters::
      #   *otp => contains OTP sent by the user
      #   *message => message object which contains OTP & expiry time from our system
      # 
      # Raise Error::
      #   *AuthenticationTimeoutError if OTP sent by user is expired
      #   *WrongOtpError if OTP sent by user doesn't matches 
      # 
      # @return [boolean] true if succefull else raises error
      def self.verify_otp(otp, message)
        raise CUSTOM_ERROR_UTIL::AuthenticationTimeoutError.new(CONTENT_UTIL::OTP_EXPIRED) if otp_expired(message)
        if message.otp == otp.to_s
          return true
        else
          raise CUSTOM_ERROR_UTIL::WrongOtpError.new(CONTENT_UTIL::WRONG_OTP)
        end
      end
      
      # 
      # function to generate random 4 digit number OTP
      # 
      # @return [int] One time password
      def self.generate_otp
        rand(1000..9999)
      end

      # 
      # funciton to check if otp has expired
      # 
      # @return [boolean] if otp expired return true else false
      def self.otp_expired(message)
        expiry_time =  message.expires_at
        Time.zone.now > expiry_time ? true : false
      end

    end
  end
end

# 
# Helper to facilitate membership related functionalities
#
module UsersModule
  module V1
    module MembershipHelper

      #
      # Function to get exact duration of Ample Sample membership 
      # due to additional referral bonus membership days
      #
      def self.get_membership_duration(membership)
        duration = (membership.expires_at.to_date - membership.starts_at.to_date).round
        return duration
      end
    end
  end
end

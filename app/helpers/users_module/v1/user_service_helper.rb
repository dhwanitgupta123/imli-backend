#
# Module handles all the functionality related to User
#
module UsersModule
  #
  # UsersModule module Version 1
  #
  module V1 
    #
    # Helper to generate User service responses
    # and other functionalities
    #
    module UserServiceHelper

      #
      # defining global variable with versioning
      #
      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator
      ACCOUNT_TYPE = UsersModule::V1::AccountType
      CACHE_UTIL = CommonModule::V1::Cache

      ADDRESS_HELPER = AddressModule::V1::AddressHelper
      PROFILE_DECORATOR = UsersModule::V1::ProfileResponseDecorator
      EMAIL_DECORATOR = UsersModule::V1::EmailDecorator
      MEMBERSHIP_DECORATOR = UsersModule::V1::MembershipResponseDecorator

      #
      # Create users array containing list of all relevant inforamtion about that User
      #
      def self.get_users_array(users)
        users_array = []
        if users.blank?
          return users_array
        end
        users.each do |user|
          user = USER_RESPONSE_DECORATOR.get_user_details(user)
          users_array.push(user)
        end
        return users_array
      end

      #
      # Create users array containing list of all relevant inforamtion about that User
      # with allowed events
      #
      def self.get_all_users_details_array(users)
        users_array = []
        if users.blank?
          return users_array
        end
        users.each do |user|
          user = USER_RESPONSE_DECORATOR.get_all_user_details(user)
          users_array.push(user)
        end
        return users_array
      end

      #
      # Get complete details of user
      #
      def self.get_user_details(user)
        return {} if user.blank?

        addresses = ADDRESS_HELPER.get_active_addresses_hash(user.addresses)
        emails = EMAIL_DECORATOR.get_customer_type_email_hash(user.emails)
        membership = MEMBERSHIP_DECORATOR.get_user_membership_hash(user.memberships)
        profile = PROFILE_DECORATOR.get_profile_hash(user.profile)
        
        response = {
          id: user.id,
          first_name: user.first_name || '',
          last_name: user.last_name || '',
          phone_number: user.phone_number || '',
          referral_code: user.referral_code || '',
          referral_limit: user.referral_limit || '',
          status: user.workflow_state || '',
          addresses: addresses || [],
          emails: emails || [],
          membership: membership || [],
          profile: profile || ''
        }
        return response
      end

      # 
      # Function to generate referral code for the user
      # Referral code  = random referral code from REDIS using key 'REFERRAL_CODES'
      # appended with randomly generated three digits.
      #
      def self.create_referral_code
        referral_codes = CACHE_UTIL.read_json('REFERRAL_CODES')
        referral_codes_length = referral_codes['referral_codes'].count
        # Convert it to string because accessing key of JSON need to be string
        referral_code_key = rand(1..referral_codes_length).to_s
        referral_code = (referral_codes['referral_codes'][referral_code_key] + rand(0..9).to_s + rand(0..9).to_s + rand(0..9).to_s).upcase
        return referral_code
      end

      #
      # Get primary e-mail of user
      # Return first email id, if primary tag is not set to any of the mail ids
      #
      # @param user [Object] [User for which primary email is to be fetched]
      #
      # @return [Object] [Email object belongs to user]
      def self.get_primary_mail(user)
        return nil if user.blank? || user.emails.blank?
        user_emails = user.emails
        primary_email = user_emails.where(account_type: ACCOUNT_TYPE::CUSTOMER, is_primary: true).first
        if primary_email.present?
          return primary_email
        end
        return user_emails.first
      end

      #
      # Function to create JSON of all pending users
      #
      def self.create_users_json(users)
        users_array = []
        users.each do |user|
          users_array.push({
            id: user.id,
            phone_number: user.phone_number
            })
        end
        return users_array
      end

      def self.is_password_set?(user)
        return false if user.blank? || user.provider.blank? || user.provider.generic.blank?
        generic = user.provider.generic
        # If user already initiated the process of reseting password,
        # then return its password set as FALSE
        return false if generic.reset_flag.present?
        # Else return true by default
        return true
      end

      def self.is_first_time_password?(user)
        return true if user.blank? || user.provider.blank? || user.provider.generic.blank?
        # Else return false
        return false
      end

    end
  end
end

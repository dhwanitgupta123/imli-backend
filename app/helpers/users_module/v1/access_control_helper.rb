module UsersModule
  module V1
    #
    # Helper to perform certain common functions
    # for ACL layer
    #
    module AccessControlHelper

      #
      # Fetch App details from request headers
      #
      # @param request [Object] [Request Object for ]
      # 
      # @return [type] [description]
      def self.fetch_app_details_from_request_headers(request)
        app_version = ''
        platform_type = ''
        device_type = ''
        if ( request.present? && request.headers.present? )
          app_version = request.headers['Imli-App-Version']
          platform_type = request.headers['Imli-Platform-Type']
          device_type = request.headers['Imli-Device-Id']
        end
        return { app_version: app_version, platform_type: platform_type, device_type: device_type }
      end

    end
  end
end
# 
# Helper to generate Authentication & Session token
# 
# @author [anuj]
#
module UsersModule
  module V1
    module TokenHelper
    
      # 
      # 
      # generate authentication token of length 64 using base64 without padding
      # 
      # parameters::
      #   * 64 defines the length of token to be generated
      #   * false : without padding in the token generated 
      # 
      # logic::
      #   * Authentication token = urlsafe_base64(64, false) + Time in microseconds to make it unique 
      # 
      # @return string 
      def self.generate_authentication_token
        authentication_token = SecureRandom.urlsafe_base64(64,false)
        return [authentication_token, Time.new.usec.to_s].join
      end

      # 
      # generate session token using base64 
      # 
      # parameters::
      #   * 64 defines the length of token to be generated
      #   * false : without padding in the token generated 
      # 
      # logic::
      #   * Session token = urlsafe_base64(64, false) + User_ID to make it unique 
      # 
      # @return string 
      def self.generate_session_token(user_id)
        session_token = SecureRandom.urlsafe_base64(64,false)
        return [session_token,user_id.to_s].join
      end
    end
  end
end
#
# Module handles all the functionality related to User
# 
module UsersModule
  #
  # UsersModule module Version 1
  # 
  module V1 
    #
    # Module to generate RoleService responses & other functionalities
    #
    module RoleServiceHelper

      #
      # defining global variable with versioning
      #
      ROLE_RESPONSE_DECORATOR = UsersModule::V1::RoleResponseDecorator

      #
      # Create array of roles
      #
      def self.get_roles_array(roles)
        roles_hash = []
        if roles.blank?
          return roles_hash
        end
        roles.each do |role|
          role = ROLE_RESPONSE_DECORATOR.get_role_details(role)
          roles_hash.push(role)
        end
        return roles_hash
      end

      #
      # Create array of roles with successive tree containing
      # roles under it
      #
      def self.get_roles_tree_hash(roles)
        roles_array = []
        if roles.blank?
          return roles_array
        end
        roles.each do |role|
          role_hash = ROLE_RESPONSE_DECORATOR.get_role_details(role)
          role_hash['children'] = get_roles_tree_hash(role.children)
          roles_array.push(role_hash)
        end
        return roles_array
      end
    end
  end
end

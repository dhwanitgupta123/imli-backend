  #
  # @author Sumit <sumitchhuttani007@gmail.com>
  # Helper to perform various cryptographic algorithms
  # 
module UsersModule
  module V1
    module PasswordHelper

      # The following constants can be changed without breaking existing hashes.
      #
      # Constants used are stored in cache, and also have fallback to local variables:
      # PBKDF2_ITERATIONS [work factor used to slow down encryption]
      # SALT_BYTE_SIZE [bytes of salt used]
      # SALT_INDEX [end index of salt in final hash, Need to segregate salt and hash]
      #
      # Presently, hash[178 bytes] => salt[88 bytes] | H(salt,digest)[90 bytes]
      PBKDF2_ITERATIONS= 20000
      SALT_BYTE_SIZE= 64
      SALT_INDEX= 87

      # Create Hash of the password passed as parameter
      # hash = salt || H(salt, password)
      #
      # @param password [String] string to hashed
      #
      # @return [String] final hash of password including salt
      #  OR nil if provided password string is blank
      def self.create_hash(password)
        return nil if password.blank?
        salt = generate_salt
        pbkdf2_generated_hash = generate_pbkdf2_hash(salt, password)
        encoded_hash = base64_encode(pbkdf2_generated_hash)

        # Prepend salt with hashed string. Need salt during validation
        hash_including_salt = [salt, encoded_hash].join
        return hash_including_salt
      end

      #
      # Checks if the password is correct given hash of the
      # correct one
      # @param password [String] password to verify
      # @param expected_hash [String] hash to be expected
      #
      # @return [Boolean] true if hash of password matches expected one
      # else false otherwise
      def self.validate_password(password, expected_hash)
        return false if (password.blank? || expected_hash.blank?)

        salt_index = $redis.get("SALT_INDEX")
        # If not present in cache, then use default value
        salt_index = salt_index.blank? ? SALT_INDEX : salt_index.to_i
        salt = expected_hash.slice(0..salt_index)
        hash_without_salt = expected_hash.slice(salt_index + 1..-1)
        # verify both salt and hash are present
        return false if (salt.blank? || hash_without_salt.blank?)

        decoded_hash = base64_decode(hash_without_salt)

        pbkdf2_generated_hash = generate_pbkdf2_hash(salt, password)
        value =  equal_time_comparision(decoded_hash, pbkdf2_generated_hash)
        return value
      end

      private

      # 
      # Generates random salt number
      # Presently, using SecureRandom to generate base64 random number
      # of size SALT_BYTE_SIZE
      # 
      # @return [String] generated random number
      def self.generate_salt
        salt_byte_size = $redis.get("SALT_BYTE_SIZE")
        return SecureRandom.base64( salt_byte_size.blank? ? SALT_BYTE_SIZE : salt_byte_size.to_i)
      end

      # 
      # Digest algorithm to be used during hashing
      # Presently, using SHA-512 as underlying digest algorithm
      # 
      # @return [Object] Instance of Digest Algorithm
      def self.digest_function
        OpenSSL::Digest::SHA512.new
      end

      # 
      # Generate "Password-Based Key Derivation Function 2" hash
      # wiki: 'https://en.wikipedia.org/wiki/PBKDF2'
      # 
      # @param salt [String] random string used for salting
      # @param password [type] password to be hashed
      # 
      # @return [String] Hashed string
      def self.generate_pbkdf2_hash(salt, password)
        digest = digest_function
        length = digest.digest_length
        iterations = $redis.get("PBKDF2_ITERATIONS")
        pbkdf2_generated_hash = OpenSSL::PKCS5::pbkdf2_hmac(
          password,
          salt,
          iterations.blank? ? PBKDF2_ITERATIONS : iterations.to_i,
          length,
          digest
        )
        return pbkdf2_generated_hash
      end

      # 
      # Encode string using base64 encoding
      # @param value [String] value to encode
      # 
      # @return [String] encoded string
      def self.base64_encode(value)
        Base64.encode64(value)
      end


      # 
      # Decode string using base64 decoding
      # @param value [String] value to decode
      # 
      # @return [String] decoded string
      def self.base64_decode(value)
        Base64.decode64(value)
      end

      # 
      # Compares whether a and b are equal or not
      # 
      # Typically, string comparision in cryptography using "==" short-circuits on evaluation,
      # and is therefore vulnerable to timing attacks.
      # The proper way is to use a method that always takes the same amount of time
      # when comparing two values, thus not leaking any information to potential attackers.
      # 
      # @param a [String] string 1 to match
      # @param b [String] string 2 to match
      # 
      # @return [Boolean] if a matches b, then true else false
      def self.equal_time_comparision(a, b)
        unless a.length == b.length
          return false
        end
        cmp = b.bytes.to_a
        result = 0
        a.bytes.each_with_index {|c,i|
          result |= c ^ cmp[i]
        }
        result == 0
      end
    end
  end
end
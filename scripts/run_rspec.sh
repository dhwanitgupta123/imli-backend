#!/bin/bash -xe

echo_rspec_block_name()
{
	echo "
	##########################################################################################
	## Running Rspecs for $1
	##########################################################################################
	"
}

script_directory="$( cd "$( dirname "$0" )" && pwd )"
# rails application root directory

application_root_dir=`dirname $script_directory`

# engines directory of rails application
engines_dir="$application_root_dir/engines"
# array of full path of engines
engines=`ls -d $engines_dir/*`

echo_rspec_block_name $application_root_dir

# bundle install for application
cd $application_root_dir; bundle exec rspec

for engine in $engines; do
	echo_rspec_block_name $engine
  # chaning directory to target engine and running bundle install
  cd $engine ; bundle exec rspec
done
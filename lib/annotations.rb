# Annotations module, enable in base class with “extend Annotations”
module Annotations

  #
  # Example to explaing this workflow
  # Suppose there are method m1 and m2 in class c1 
  # 
  # 
  # class c1
  #   extends Anotations
  #   extends MyAnnotations
  #   _A_
  #   def m1 end;
  #   
  #   def m2 end;
  # end
  # 
  # module MyAnnotations
  #  def _A_
  #   annotate_method(:annotation, :value)
  #  end
  # end
  # 
  # suppose if we initialize c1, c1.new
  # then it will parse _A_ first which call MyAnnotations._A_
  # which again call Annotations.annotate_method this function sets instance variable
  # @_annotated_next_method with give value [this is indicating that method which is going to be added after this
  # will have annotation ]
  # Not def m1 will be executed now in ruby it call method_added(method_name) but we overides this function
  # and check is @_annotated_next_method is nil or not if it is present then add method name in
  # @_annotated_methods hash and again reset @_annotated_next_method variable
  #

  #
  # Annotate the next method defined
  # This function sets the instance variable @_annotated_next_method so that
  # when method_added function get called if it finds @_annotated_next_method
  # then it sets the annotation for current method
  #
  def annotate_method(annotation_name, value, jump_point = nil)
    _annotation_storage(:@_annotated_next_method)[annotation_name] = {
                                            value: value,
                                            jump_point: jump_point
                                          }

  end

  # 
  # This function return annotations corresponding to the method
  # 
  def annotation_get(method_name, annotation_name)
    m = self.instance_variable_get(:@_annotated_methods)
    if m.present? && m[method_name].present? && m[method_name][annotation_name].present?
      return m[method_name][annotation_name][:value]
    end
    return nil
  end

  # 
  # This function execute all the jump points and after that it
  # executes original method
  #1
  def self.proceed(jp_context)
    if jp_context[:current_jp] >= jp_context[:jump_points].length
      jp_context[:method].call(*jp_context[:args], &jp_context[:block])
    else
      jp_context[:current_jp] += 1
      jp_context[:jump_points][jp_context[:current_jp] - 1].call(jp_context)
    end
  end

  private
  
  # 
  # Magic method to make this work
  # 
  # This method is called when class gets initialize, it adds all the methods
  # in the class
  # 
  def method_added(method_name)
    a = self.instance_variable_get(:@_annotated_next_method)
    
    if a != nil
      _annotation_storage(:@_annotated_methods,{})[method_name] = a
      self.instance_variable_set(:@_annotated_next_method, nil)

      hooks = get_hooks(a)

      jump_points = hooks[:jump_points]
      
      original_method = instance_method(method_name)

      #
      # Re-definig method with all its pre and post hooks
      # 
      define_method(method_name) do |*args, &block|

        Annotations.proceed({ 
                              args: args, method_name: method_name, 
                              method: original_method.bind(self), 
                              block: block, current_jp: 0, 
                              jump_points: jump_points
                          })
      end
    end
    super
  end
  
  # 
  # Return function hooks
  #
  def get_hooks(a)

    jump_points = []

    a.keys.each do |key|
      value = a[key]
      jump_points.push(value[:jump_point]) if value[:jump_point].present?
    end

    { jump_points: jump_points }
  end

  #
  # Store data in the class variable
  #
  def _annotation_storage(name, default = nil)
    a = self.instance_variable_get(name)
    if a == nil
      a = Hash.new(default)
      self.instance_variable_set(name, a)
    end
    return a
  end
end

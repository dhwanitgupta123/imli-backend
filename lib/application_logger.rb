#
# FILE logger to log application (developer code logs) into a separate file
# Log levels need to be set in specific environments' *.rb
# Ex:
# # Set log level
# config.log_level = :ERROR
#
# Severity levels: DEBUG < INFO < WARN < ERROR < FATAL < UNKNOWN
#
class ApplicationLogger

  # Logfile will be environment specific. Forex: 'development_application.log'
  LOGFILE = Rails.root.join("log/", "#{Rails.env}_application.log").to_s

  def self.create_logger
    file_logger = Logger.new(LOGFILE)
    # Log statements shoule be prepended with date in format "2015-08-18 22:54:26 +05:30"
    file_logger.datetime_format = '%Y-%m-%d %H:%M:%S %z '
    return file_logger
  end

  #
  # Print DEBUG level logs into specified logger
  #
  def self.debug(message = nil)
    @file_log ||= create_logger
    @file_log.debug(message) unless message.nil?
  end

  #
  # Print INFO level logs into specified logger
  #
  def self.info(message = nil)
    @file_log ||= create_logger
    @file_log.info(message) unless message.nil?
  end

  #
  # Print WARN level logs into specified logger
  #
  def self.warn(message = nil)
    @file_log ||= create_logger
    @file_log.warn(message) unless message.nil?
  end

  #
  # Print ERROR level logs into specified logger
  #
  def self.error(message = nil)
    @file_log ||= create_logger
    @file_log.error(message) unless message.nil?
  end

end
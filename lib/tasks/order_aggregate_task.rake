namespace :aggregate do

  module OrderAggregation

    def self.init(from_time, to_time)
      puts 'Order Aggregation started at:' + Time.zone.now.to_s
      from_time = from_time.to_time
      to_time = to_time.to_time
      from_time = Time.zone.now.change(hour: from_time.hour, min: from_time.min, sec: from_time.sec)
      to_time = Time.zone.now.change(hour: to_time.hour, min: to_time.min, sec: to_time.sec)

      # Taking care of edge case where:
      # from time hour is greater than to time hour
      # In this case, a day is decreased from From-Time
      if (from_time.hour > to_time.hour)
        puts "From-Time Hour provided is greater than To-Time Hour"
        puts "Making From-Time to previous day"
        from_time = from_time - 1.days
      end
      initialize_services
      from_epoch = @date_util.convert_time_to_epoch(from_time)
      to_epoch = @date_util.convert_time_to_epoch(to_time)
      puts "Aggregation for #{from_time} to #{to_time}"
      send_aggregation_mail(from_epoch, to_epoch)
      puts "Generated Aggregation at #{Time.zone.now} - Success!"
    end

    def self.initialize_services
      @order_panel_service = MarketplaceOrdersModule::V1::OrderPanelService.new({})
      @date_util = CommonModule::V1::DateTimeUtil
    end

    def self.send_aggregation_mail(from_epoch, to_epoch)
      # to_time = Time.zone.now
      # from_time = to_time - @time_diff.hours
      params = {
        from_time: from_epoch,
        to_time: to_epoch
      }
      response = @order_panel_service.get_aggregated_products_in_interval(params)
      puts response
    end
  end

  desc 'Rake task to get aggregate details of order within a spcific time period'
  task :orders, [:arg1, :arg2] => :environment do |t, args|
    OrderAggregation.init(args[:arg1],args[:arg2])
  end
end

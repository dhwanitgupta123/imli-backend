namespace :pending do

  module PendingUsers

    def self.init

      puts 'List of Pending users task started at:' + Time.zone.now.to_s
      initialize_services
      send_pending_user_mail
    end

    def self.initialize_services
      @user_service = UsersModule::V1::UserService.new({})
    end

    def self.send_pending_user_mail
      pending_users = @user_service.get_all_pending_users
      response = @user_service.send_pending_user_mail(pending_users)
      puts response
    end
  end

  desc 'Rake task to send mail of all pending users to Admin for activation'
  task :users => :environment do
    PendingUsers.init
    puts "#{Time.zone.now} - Success!"
  end
end

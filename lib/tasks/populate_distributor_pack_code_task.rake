namespace :populate do
  module UpdateDpCode
    require 'csv'
    def self.init
      puts 'please enter the Vendor code'
      vendor_code = STDIN.gets.strip
      puts 'please enter full path of csv of distributor pack code'
      csv_file_path = STDIN.gets.strip
      count = 0
      @field_map = {}

      self.initialize_classes

      puts 'Loading table....................................................'

      CSV.foreach(csv_file_path) do |row|
        if count == 0
          self.map_brand_pack_fields(row)
          count = 1
          next
        end 
        next if row[@field_map[:bp_code]].blank?
        begin
          brand_pack = @brand_pack_dao.get_brand_pack_by_code(row[@field_map[:bp_code]])
        rescue @custom_error_util::ResourceNotFoundError
          next
        end
        if row[@field_map[:dp_code]].present?
          inventory = @inventory_service.get_inventory_by_vendor_code(vendor_code)
          update_ibp(brand_pack, inventory, row[@field_map[:dp_code]])
        end
      end

      puts 'Success............................................'
    end

    #
    # Function to update IBP
    #
    def self.update_ibp(brand_pack, inventory, dp_code)
      ibp = @inventory_brand_pack_dao.ibp_of_brand_pack_for_a_vendor(inventory, brand_pack)
      if ibp.present?
        @inventory_brand_pack_dao.update({distributor_pack_code: dp_code}, ibp)
      end
    end

    #
    # Initializing classes
    #
    def self.initialize_classes
      params = {version: 1}
      @brand_pack_dao = MasterProductModule::V1::BrandPackDao.new
      @inventory_service = SupplyChainModule::V1::InventoryService.new
      @inventory_brand_pack_dao = InventoryProductModule::V1::InventoryBrandPackDao.new
      @custom_error_util = CommonModule::V1::CustomErrors
    end

    #
    # This function map master data fields from the CSV
    #
    def self.map_brand_pack_fields(row)
      index = 0
      row.each do |col|
        case col.downcase
        when 'bp_code'
          @field_map[:bp_code] = index
        when 'dp_code'
          @field_map[:dp_code] = index
        end
        index += 1
      end
    end
  end
  desc 'This task populates the distributor pack code for each brand pack'
  task distributor_pack_code: :environment do
      UpdateDpCode.init
  end
end

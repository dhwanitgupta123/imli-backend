namespace :initialize do
  desc 'Initialize the marketplace selling pack index this will ovrrite the index'
  task mpsp_index: :environment do
    mpsp_index_service = SearchModule::V1::MarketplaceSellingPackIndexService.new
    mpsp_index_service.initialize_index('true')
  end
end
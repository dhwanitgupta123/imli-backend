namespace :populate do
  
  module PopulatePlans

    PLANS_KEY = %W[name details duration validity_label fee status plan_key benefits] 

    def self.init
      STDOUT.puts 'please enter full path of csv'
      csv_file_path = STDIN.gets.strip

      initialize_services
      initialize_models

      @field_map = {}
      count = 0

      CSV.foreach(csv_file_path) do |row|

        if count == 0
          map_fields(row)
          count = 1
          next
        end
        plan = create_or_update_plans(row)
        plan.status = row[@field_map[:status]]
        plan.save
      end
    end

    def self.initialize_models
      @benefits_model = CommonModule::V1::Benefit
    end

    def self.initialize_services
      @plan_service = CommonModule::V1::PlanService.new({})
    end

    def self.get_benefit_ids(benefits_keys)
      benefit_ids = []
      benefits_keys.each do |key|
        benefit_ids.push(@benefits_model.find_by(benefit_key: key).id)
      end
      return benefit_ids
    end

    def self.create_or_update_plans(row)
      plan_args = get_args(PLANS_KEY, row)
      plan_args[:benefits] = get_benefit_ids(plan_args[:benefits].split(','))
      begin
        plan = @plan_service.get_plan_by_key(plan_args[:plan_key])
        return @plan_service.update_plan_with_params({plan: plan_args, id: plan.id})
      rescue
        return @plan_service.create_plan_with_params({plan: plan_args})
      end
    end

    def self.get_args(keys, row)
      args = {}
      keys.each do |key|
        args[key.to_sym] = row[@field_map[key.to_sym]]
      end
      return args
    end

    def self.map_fields(row)
      index = 0
      
      row.each do |col|
        case col.downcase
        when 'name'
          @field_map[:name] = index
        when 'details'
          @field_map[:details] = index
        when 'duration'
          @field_map[:duration] = index
        when 'validity_label'
          @field_map[:validity_label] = index
        when 'fee'
          @field_map[:fee] = index
        when 'status'
          @field_map[:status] = index
        when 'plan_key'
          @field_map[:plan_key] = index
        when 'benefits'
          @field_map[:benefits] = index
        end
        index += 1
      end
    end
  end

  desc 'To populate plans'
  task plans: :environment do
    PopulatePlans.init
  end
end

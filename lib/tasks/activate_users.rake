namespace :activate do

  module ActivateUsers

    def self.init
      puts 'Activation of Pending users task started at:' + Time.zone.now.to_s
      initialize_services
      activate_users
    end

    def self.initialize_services
      @user_service = UsersModule::V1::UserService.new({})
      @user_event = UsersModule::V1::ModelStates::UserEvents
    end

    def self.activate_users
      pending_users = @user_service.get_all_pending_users
      if pending_users[:users].present?
        pending_users[:users].each do |user|
          args = { 
            id: user.id,
            user: {
              event: @user_event::ACTIVATE
            }
          }
          puts @user_service.change_state(args)
        end
      else
        puts 'No Pending Users'
      end
    end
  end

  desc 'Rake task to activate all the pending users'
  task :users => :environment do
    ActivateUsers.init
    puts "#{Time.zone.now} - Success!"
  end
end

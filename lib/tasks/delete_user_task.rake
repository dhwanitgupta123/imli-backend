namespace :delete do
  desc 'This task take input phone number and delete user'
  task :user, [:phone_number] => :environment do |t, args|

    if Rails.env != 'development' && Rails.env != 'feature'
      abort('Not Allower to run in ' + Rails.env + ' environment')
    end

    user = UsersModule::V1::User.find_by(phone_number: args.phone_number)
    email_validator = EmailValidationModule::V1::VerifyGmailIdHelper

    if user.nil?
      abort('User not found')
    end
    
    STDOUT.puts 'Are you sure to delete user with phone_number ' + args.phone_number.to_s + ' [Y/N] '
    answer = STDIN.gets.strip

    if answer.upcase != 'Y'
      abort('Aborted by user')
    end

    addresses = user.addresses

    if addresses.present?
      addresses.each do |address|
        orders = MarketplaceOrdersModule::V1::Order.where(address_id: address.id)
        if orders.present?
          orders.each do |order|
            cart = order.cart
            order_products = cart.order_products if cart.present?
            order_products.delete_all if order_products.present?
            cart.delete if cart.present?

            payments = order.payments
            payments.delete_all if payments.present?
            order.delete
          end
        end
        address.delete
      end
    end

    carts = user.carts

    if carts.present?
      carts.each do |cart|
        order_products = cart.order_products
        order_products.delete_all if order_products.present?
        cart.delete if cart.present?
      end
    end

    orders = user.orders
    orders.delete_all if orders.present?


    emails = user.emails
    if emails.present?
      emails.each do |email|
        email_id = email.email_id
        if email_validator.verify_if_already_in_use(email_id)
          email_validator.delete(email_id)
        end
        email.delete
      end
    end

    employee = user.employee
    employee.delete if employee.present?

    profile = user.profile
    profile.delete if profile.present?

    provider = user.provider
    if provider.present?
      generic = provider.generic
      generic.delete if generic.present?

      message = provider.message
      message.delete if message.present?
      provider.delete
    end

    memberships = user.memberships
    memberships.delete_all if memberships.present?

    ample_credit = user.ample_credit
    ample_credit.delete if ample_credit

    user.delete

    puts 'Successfully Deleted User'
  end
end

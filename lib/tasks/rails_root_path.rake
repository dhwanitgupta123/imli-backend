namespace :rails do
  desc "Return the root path of rails application"
  task :root_path => :environment do
    puts File.join(Rails.root)
  end
end

#
#  COMMENTING THIS TASK AS THIS IS NOT IN USE
# 

# namespace :update do
#   module Update
#     require 'csv'
#     def self.init
#       STDOUT.puts 'please enter full path of csv'
#       csv_file_path = STDIN.gets.strip
#       STDOUT.puts 'please enter base path for images'
#       @base_image_path = STDIN.gets.strip

#       script_log_file_path = File.join(Rails.root,'log','script_log.txt')
#       @script_file = File.open(script_log_file_path, 'w')

#       unique_id_file_path = File.join(Rails.root, 'updated_unique_ids')
#       unique_id_file_writer = CSV.open(unique_id_file_path, 'w')
      
#       count = 0
#       @initials = 0
#       @field_map = {}

#       @added_row_array = []

#       self.initialize_dao
#       self.initialize_service
#       self.initialize_models

#       puts 'Loading table....................................................'

#       self.initialize_default_data

#       CSV.foreach(csv_file_path) do |row|
#         ActiveRecord::Base.transaction do

#           if count == 0
#             self.map_master_data_fields(row)
#             count = 1
#             next
#           end
#           break if row[@field_map[:company_name]].blank?

#           self.create_model_arguments(row)

#           if row[@field_map[:unique_ids]].blank?
#             unique_id_file_writer << [self.create_record(row)]
#           else
#             unique_id_file_writer << [row[@field_map[:unique_ids]]]
#             self.update_record(row, row[@field_map[:unique_ids]])
#           end
#         end
#       end

#       puts 'Success............................................'
#       puts 'PLEASE CHECK log/script_log.txt TO SEE THE INCONSISTENCY IN DATA'
#       @script_file.close
#       unique_id_file_writer.close
#     end

#     #
#     # 1. Department, Categories and Sub Categories are populate via update:priority task
#     # 2. We are querying Department, Category, Sub Category, Company, Brand, Sub Brand by name and they need to be 
#     #    in same case in the sheet
#     # 3. If anyone change any of the field defined in 2 points, then this script change the name of the entity not changing
#     #    the id of model and if user wants to move brand_pack from one [Department, Category, Sub Category, Company, Brand, Sub Brand] 
#     #    to other then in excel sheet first deactivate the existing brand_pack by updating pass field to 2 and create
#     #    a new row with pass = 1
#     # 4. Here we are assuming unique ids as
#     #   i. For company name is unique
#     #   ii. For Brand brand name and company_id is composite unique key
#     #   iii. For SubBrand sub brand name and brand_id is composite unique key
#     #
#     def self.create_record(row)

#       @error_text = ''

#       mapper_row = {}
#       mapper_row[:distributor_id] = @distributor.id
#       mapper_row[:cash_and_carry_id] = @cash_and_carry.id
#       mapper_row[:inventory_id] = @inventory.id
#       mapper_row[:warehouse_id] = @warehouse.id
#       mapper_row[:seller_id] = @seller.id

#       company = create_company(@company_args)
#       mapper_row[:company_id] = company.id

#       brand = create_brand(@brand_args.merge(company_id: company.id))
#       mapper_row[:brand_id] = brand.id

#       sub_brand = create_sub_brand(@sub_brand_args.merge(brand_id: brand.id))
#       mapper_row[:sub_brand_id] = sub_brand.id

#       product = create_product(@product_args.merge(sub_brand_id: sub_brand.id))
#       mapper_row[:product_id] = product.id

#       department = create_department(@department_args)
#       mapper_row[:department_id] = department.id

#       category = create_category(@category_args.merge(department_id: department.id))
#       mapper_row[:category_id] = category.id

#       sub_category = create_sub_category(@sub_category_args.merge(category_id: category.id))
#       mapper_row[:sub_category_id] = sub_category.id

#       brand_pack = create_brand_pack(@brand_pack_args.merge(product_id: product.id, sub_category_id: sub_category.id, status: 1))
#       mapper_row[:brand_pack_id] = brand_pack.id

#       inventory_brand_pack = create_inventory_brand_pack({brand_pack_id: brand_pack.id, inventory_id: @inventory.id, status: 1})
#       mapper_row[:inventory_brand_pack_id] = inventory_brand_pack.id

#       warehouse_brand_pack = create_warehouse_brand_pack({warehouse_id: @warehouse.id, brand_pack_id: brand_pack.id, inventory_brand_pack_id: inventory_brand_pack.id, status: 1})
#       mapper_row[:warehouse_brand_pack_id] = warehouse_brand_pack.id

#       seller_brand_pack = create_seller_brand_pack({seller_id: @seller.id, warehouse_brand_pack_id: warehouse_brand_pack.id, brand_pack_id: brand_pack.id, status: 1})
#       mapper_row[:seller_brand_pack_id] = seller_brand_pack.id

#       marketplace_brand_pack = create_marketplace_brand_pack({brand_pack_id: brand_pack.id, seller_brand_pack_id: seller_brand_pack.id, status: 1})
#       mapper_row[:marketplace_brand_pack_id] = marketplace_brand_pack.id

#       marketplace_selling_pack = create_marketplace_selling_pack(marketplace_brand_pack.id)
#       mapper_row[:marketplace_selling_pack_id] = marketplace_selling_pack.id

#       master_data_row = LoggingModule::V1::MasterDataMap.new(mapper_row)
#       master_data_row.save

#       @script_file.puts(@error_text) if @error_text.present?
#       return master_data_row.id
#     end

#     #
#     # This assumes that the brand_pack is already present in db and it is used to update 
#     # properties of bp like mrp, sp, savings, ladder pricing, product name, sku
#     # 
#     # It assumes that if [Department, Category, Sub Category, Company, Brand, Sub Brand] any of these
#     # fields changes then only name of these entity will be changed it won't create new entity for the 
#     # above once
#     # 
#     def self.update_record(row, unique_id)
#       @master_data_row = LoggingModule::V1::MasterDataMap.find(unique_id)
      
#       update_company(@company_args)
#       update_brand(@brand_args)
#       update_sub_brand(@sub_brand_args)
#       update_product(@product_args)
#       update_department(@department_args)
#       update_category(@category_args)
#       update_sub_category(@sub_category_args)
#       update_brand_pack(@brand_pack_args)
#       update_marketplace_selling_pack
#     end

#     def self.initialize_default_data
#       master_data_row = LoggingModule::V1::MasterDataMap.all[0]
#       @distributor = @distributor_service.get_distributor_by_id(master_data_row.distributor_id)
#       @inventory = @inventory_service.get_inventory_by_id(master_data_row.inventory_id)
#       @cash_and_carry = @cash_and_carry_service.get_cash_and_carry(master_data_row.cash_and_carry_id)
#       @warehouse = @warehouse_service.get_warehouse(master_data_row.warehouse_id)
#       @seller = @seller_service.get_seller(master_data_row.seller_id)
#     end

#     #
#     # Initializing services
#     #
#     def self.initialize_service
#       params = {version: 1}
#       @category_service = CategorizationModule::V1::CategoryService.new(params)
#       @department_service = CategorizationModule::V1::DepartmentService.new(params)
#       @product_service = MasterProductModule::V1::ProductService.new
#       @brand_pack_service = MasterProductModule::V1::BrandPackService.new
#       @distributor_service = SupplyChainModule::V1::DistributorService.new
#       @inventory_service = SupplyChainModule::V1::InventoryService.new
#       @inventory_brand_pack_service = InventoryProductModule::V1::InventoryBrandPackService.new
#       @cash_and_carry_service = SupplyChainModule::V1::CashAndCarryService.new
#       @warehouse_service = SupplyChainModule::V1::WarehouseService.new
#       @warehouse_brand_pack_service = WarehouseProductModule::V1::WarehouseBrandPackService.new
#       @seller_service = SupplyChainModule::V1::SellerService.new
#       @seller_brand_pack_service = SellerProductModule::V1::SellerBrandPackService.new
#       @marketplace_brand_pack_service = MarketplaceProductModule::V1::MarketplaceBrandPackService.new
#       @marketplace_selling_pack_service = MarketplaceProductModule::V1::MarketplaceSellingPackService.new 
      
#     end

#     #
#     # Initialize models 
#     #
#     def self.initialize_models
#       @department_model = CategorizationModule::V1::Department
#       @category_model = CategorizationModule::V1::Category
#     end

#     #
#     # initialize all the required dao's
#     #
#     def self.initialize_dao
#       @brand_pack_dao = MasterProductModule::V1::BrandPackDao.new
#       @product_dao = MasterProductModule::V1::ProductDao.new
#       @sub_brand_dao = MasterProductModule::V1::SubBrandDao.new
#       @brand_dao = MasterProductModule::V1::BrandDao.new
#       @company_dao = SupplyChainModule::V1::CompanyDao.new
#     end

#     def self.create_marketplace_selling_pack(mpbp_id)
#       @marketplace_selling_pack_args[:marketplace_brand_packs][0][:marketplace_brand_pack_id] = mpbp_id
#       @marketplace_selling_pack_service.create_marketplace_selling_pack(@marketplace_selling_pack_args)
#     end

#     def self.update_marketplace_selling_pack
#       @marketplace_selling_pack_args[:id] = @master_data_row.marketplace_selling_pack_id
#       @marketplace_selling_pack_service.update_marketplace_selling_pack(@marketplace_selling_pack_args)
#     end

#     def self.create_marketplace_brand_pack(marketplace_brand_pack_args)
#       @marketplace_brand_pack_service.create_marketplace_brand_pack(marketplace_brand_pack_args)
#     end

#     def self.update_marketplace_brand_pack(marketplace_brand_pack_args)
#       marketplace_brand_pack_args[:id] = @master_data_row.marketplace_brand_pack_id
#       @marketplace_brand_pack_service.update_marketplace_brand_pack(marketplace_brand_pack_args)
#     end

#     def self.create_seller_brand_pack(seller_brand_pack_args)
#       @seller_brand_pack_service.create_seller_brand_pack(seller_brand_pack_args)
#     end

#     def self.create_warehouse_brand_pack(warehouse_brand_pack_args)
#       @warehouse_brand_pack_service.create(warehouse_brand_pack_args)
#     end

#     def self.create_inventory_brand_pack(inventory_brand_pack_args)
#       @inventory_brand_pack_service.create_inventory_brand_pack(inventory_brand_pack_args)
#     end

#     def self.create_department(department_args)

#       department = @department_model.get_department_by_label(department_args[:label])
#       return department unless department.nil?
#       department = @department_service.create_department_with_details(department_args)
#       department.activate! if department_args[:status] == '1'
#       return department
#     end

#     def self.update_department(department_args)
#       department_args[:id] = @master_data_row.department_id
#       @department_service.update_department_details(department_args)
#     end

#     def self.create_category(category_args)
#       category = @category_model.get_category_by_label_and_department(category_args[:label], category_args[:department_id])
#       return category unless category.nil?
#       category = @category_service.create_category_with_details(category_args)
#       category.activate! if category_args[:status] == '1'
#       return category
#     end

#     def self.update_category(category_args)
#       category_args[:id] = @master_data_row.category_id
#       @category_service.update_category_details(category_args)
#     end

#     def self.create_sub_category(sub_category_args)
#       sub_category = @category_model.get_sub_category_by_label_and_category_id(sub_category_args[:label], sub_category_args[:category_id])
#       return sub_category unless sub_category.nil?
#       sub_category = @category_service.create_sub_category_with_details(sub_category_args)
#       sub_category.activate! if sub_category_args[:status] == '1'
#       return sub_category
#     end

#     def self.update_sub_category(sub_category_args)
#       sub_category_args[:id] = @master_data_row.sub_category_id
#       @category_service.update_sub_category_details(sub_category_args)
#     end

#     #
#     # This function creates the brand_pack
#     #
#     def self.create_brand_pack(brand_pack_args)
#       @brand_pack_service.create_brand_pack(brand_pack_args)
#     end

#     def self.update_brand_pack(brand_pack_args)
#       @brand_pack_service.update_brand_pack(brand_pack_args, @master_data_row.brand_pack_id)
#     end

#     #
#     # This function first validate if product with same name and same sub_brand_id
#     # exist if yes then return product else create new product
#     #
#     def self.create_product(product_args)
#       product_args[:initials] = MasterProductModule::V1::Product.last.id + 1
#       product = @product_dao.get_product_by_name_and_sub_brand(product_args[:name], product_args[:sub_brand_id])
#       return product unless product.nil?
#       return @product_service.create_product(product_args)
#     end

#     def self.update_product(product_args)
#       product_id = @master_data_row.product_id
#       product = @product_dao.get_product_by_id(product_id)
#       return @product_dao.update(product_args, product)
#     end

#     #
#     # This function first validate if sub_bran with same name and same brand_id
#     # exist if yes then return sub_brand else create new sub_brand
#     # 
#     def self.create_sub_brand(sub_brand_args)
#       sub_brand_args[:initials] = MasterProductModule::V1::SubBrand.last.id + 1
#       sub_brand = @sub_brand_dao.get_sub_brand_by_name_and_brand(sub_brand_args[:name], sub_brand_args[:brand_id])
#       return sub_brand unless sub_brand.nil?
#       return @sub_brand_dao.create(sub_brand_args)
#     end

#     def self.update_sub_brand(sub_brand_args)
#       sub_brand_id = @master_data_row.sub_brand_id
#       sub_brand = @sub_brand_dao.get_sub_brand_by_id(sub_brand_id)
#       return @sub_brand_dao.update(sub_brand_args, sub_brand)
#     end

#     #
#     # This function first validate if brand with brand_name and same company_id
#     # exist if yes then return brand else create new brand
#     #
#     def self.create_brand(brand_args)
#       brand_args[:initials] = MasterProductModule::V1::Brand.last.id + 1
#       brand = @brand_dao.get_brand_by_name_and_company(brand_args[:name], brand_args[:company_id])
#       return brand unless brand.nil?
#       return @brand_dao.create(brand_args)
#     end

#     def self.update_brand(brand_args)
#       brand_id = @master_data_row.brand_id
#       brand = @brand_dao.get_brand_by_id(brand_id)
#       return @brand_dao.update(brand_args, brand)
#     end

#     #
#     # This function first validate if company with company_name
#     # exist if yes then return company else create new company
#     #
#     def self.create_company(company_args)
#       company_args[:initials] = SupplyChainModule::V1::Company.last.id + 1
#       company = @company_dao.get_company_by_name(company_args[:name])
#       return company unless company.nil?
#       return @company_dao.create(company_args)
#     end

#     def self.update_company(company_args)
#       company_id = @master_data_row.company_id
#       company = @company_dao.get_company_by_id(company_id)
#       @company_dao.update(company_args, company)
#     end

#     #
#     # This function map master data fields from the CSV
#     #
#     def self.map_master_data_fields(row)
#       index = 0
#       row.each do |col|
#         case col.downcase
#         when 'company'
#           @field_map[:company_name] = index
#         when 'department'
#           @field_map[:department_name] = index
#         when 'category'
#           @field_map[:category_name] = index
#         when 'sub_category'
#           @field_map[:sub_category_name] = index
#         when 'brand'
#           @field_map[:brand_name] = index
#         when 'sub_brand'
#           @field_map[:sub_brand_name] = index
#         when 'sub-brand'
#           @field_map[:sub_brand_name] = index
#         when 'product'
#           @field_map[:product_name] = index
#         when 'sku'
#           @field_map[:sku] = index
#         when 'sku_size'
#           @field_map[:sku_size] = index
#         when 'retailer_pack'
#           @field_map[:retailer_pack] = index
#         when 'mrp'
#           @field_map[:mrp] = index
#         when 'unit'
#           @field_map[:unit] = index
#         when 'article_code'
#           @field_map[:article_code] = index
#         when 'display_name'
#           @field_map[:display_name] = index
#         when 'display_pack_size'
#           @field_map[:display_pack_size] = index
#         when 'contextual'
#           @field_map[:context] = index
#         when 'imli_packsize'
#           @field_map[:imli_packsize] = index
#         when 'total_mrp'
#           @field_map[:total_mrp] = index
#         when 'pass'
#           @field_map[:status] = index
#         when 'primary_tags'
#           @field_map[:primary_tags] = index
#         when 'secondary_tags'
#           @field_map[:secondary_tags] = index
#         when 'drop1'
#           @field_map[:drop1_sp] = index
#         when 'drop2'
#           @field_map[:drop2_sp] = index
#         when 'drop3'
#           @field_map[:drop3_sp] = index
#         when 'saving1'
#           @field_map[:drop1_savings] = index
#         when 'saving2'
#           @field_map[:drop2_savings] = index
#         when 'saving3'
#           @field_map[:drop3_savings] = index
#         when 'vat'
#           @field_map[:vat] = index
#         when 'image_id'
#           @field_map[:image_id] = index
#         when 'unique_ids'
#           @field_map[:unique_ids] = index
#         when 'bp_code'
#           @field_map[:brand_pack_code] = index
#         end
#         index += 1
#       end
#     end

#     def self.sanetize_all_price_fields(row)
#       price_keys = %W[vat drop3_savings drop2_savings drop1_savings drop1_sp drop2_sp drop3_sp total_mrp]
#       price_keys.each do |key|
#         row[@field_map[key.to_sym]] = row[@field_map[key.to_sym]].to_s.delete(',')
#       end
#       return row
#     end

#     def self.create_model_arguments(row)

#       row = sanetize_all_price_fields(row)

#       @company_args = {}
#       @company_args[:name] = row[@field_map[:company_name]]
#       @company_args[:status] = row[@field_map[:status]]

#       @brand_args = {}
#       @brand_args[:name] = row[@field_map[:brand_name]]
#       @brand_args[:status] = row[@field_map[:status]]

#       @sub_brand_args = {}
#       @sub_brand_args[:name] = row[@field_map[:sub_brand_name]]
#       @sub_brand_args[:status] = row[@field_map[:status]]

#       @product_args = {}
#       @product_args[:name] = row[@field_map[:product_name]]
#       @product_args[:status] = row[@field_map[:status]] || 1

#       @department_args = {}
#       @department_args[:label] = row[@field_map[:department_name]] || 'department'
#       @department_args[:status] = row[@field_map[:status]] || 1

#       @category_args = {}
#       @category_args[:label] = row[@field_map[:category_name]] || 'category'
#       @category_args[:status] = row[@field_map[:status]] || 1

#       @sub_category_args = {}
#       @sub_category_args[:label] = row[@field_map[:sub_category_name]] || 'subc'
#       @sub_category_args[:status] = row[@field_map[:status]] || 1

#       @brand_pack_args = {}
#       @brand_pack_args[:sku] = row[@field_map[:sku]] || 'temp'
#       @brand_pack_args[:sku_size] = row[@field_map[:sku_size]] || '0'
#       @brand_pack_args[:unit] = row[@field_map[:unit]] || 'unit'
#       @brand_pack_args[:mrp] = row[@field_map[:mrp]] || '0'
#       @brand_pack_args[:retailer_pack] = row[@field_map[:retailer_pack]] || '0'
#       @brand_pack_args[:brand_pack_code] = row[@field_map[:brand_pack_code]]
#       #@brand_pack_args[:article_code] = row[@field_map[:article_code]] || '12'
#       @brand_pack_args[:status] = row[@field_map[:status]] || 1
#       image_array = upload_image_locally(row[@field_map[:image_id]])
#       @brand_pack_args[:images] = image_array if image_array.present?

#       @marketplace_selling_pack_args = {}
#       mpbp_args = []
#       mpbp_args.push({marketplace_brand_pack_id: nil, quantity: row[@field_map[:imli_packsize]] || 1})

#       @marketplace_selling_pack_args[:max_quantity] = 10
#       @marketplace_selling_pack_args[:marketplace_brand_packs] = mpbp_args
#       @marketplace_selling_pack_args[:status] = row[@field_map[:status]]
#       @marketplace_selling_pack_args[:display_name] = row[@field_map[:display_name]]
#       @marketplace_selling_pack_args[:display_pack_size] = row[@field_map[:display_pack_size]]
#       @marketplace_selling_pack_args[:primary_tags] = row[@field_map[:primary_tags]]
#       @marketplace_selling_pack_args[:secondary_tags] = row[@field_map[:secondary_tags]]
#       @marketplace_selling_pack_args[:is_ladder_pricing_active] = true
#       @marketplace_selling_pack_args[:description] = 'Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events.'

#       pricing_args = {}
#       pricing_args[:mrp] = row[@field_map[:total_mrp]]
#       pricing_args[:base_selling_price] = row[@field_map[:drop1_sp]]
#       pricing_args[:savings] = row[@field_map[:drop1_savings]]
#       pricing_args[:vat] = row[@field_map[:vat]]

#       validate_pricing(pricing_args, @marketplace_selling_pack_args[:display_name])

#       ladder_args = [
#         {
#           quantity: 1,
#           selling_price: row[@field_map[:drop1_sp]],
#           additional_savings: (row[@field_map[:drop1_savings]].to_f - pricing_args[:savings].to_f).round(2).to_s
#           # For now this will be zero as both value points to same coloumn
#           # LOGIC
#         },
#         {
#           quantity: 2,
#           selling_price: row[@field_map[:drop2_sp]],
#           additional_savings: (row[@field_map[:drop2_savings]].to_f - pricing_args[:savings].to_f).round(2).to_s
#           # LOGIC
#         },
#         {
#           quantity: 3,
#           selling_price: row[@field_map[:drop3_sp]],
#           additional_savings: (row[@field_map[:drop3_savings]].to_f - pricing_args[:savings].to_f).round(2).to_s
#           # LOGIC
#         }
#       ]

#       pricing_args[:ladder] = ladder_args
#       @marketplace_selling_pack_args[:pricing] = pricing_args
#       @initials += 1
#     end

#     def self.upload_image_locally(image_id)
      
#       if image_id.blank?
#         @error_text = ('Error in Row Number ### ' + (@initials + 1).to_s)  if @error_text.blank?
#         @error_text = @error_text + ' IMAGE ID is missing '
#         return nil
#       end
#       absolute_path = @base_image_path + '/' + image_id

#       unless File.directory?(absolute_path)
#         @error_text = ('Error in Row Number ###' + (@initials + 1).to_s)  if @error_text.blank?
#         @error_text = @error_text + ' IMAGE folder corresponding to image id = ' + image_id + ' not present, Image Folder missing ' + absolute_path
#         return nil
#       end
#       image_service = ImageServiceModule::V1::ImageService.new
#       return image_service.upload_images_from_directory(absolute_path)
#     end

#     def self.validate_pricing(pricing_args, display_name)
#       mrp = pricing_args[:savings].to_f + pricing_args[:base_selling_price].to_f

#       if mrp != pricing_args[:mrp].to_f
#         @error_text = ('Error in Row Number ###' + (@initials + 1).to_s) if @error_text.blank?
#         @error_text = @error_text + ' Inconsistency in Pricing Data ::::  MRP - BASE_SELLING_PRICE NOT EQUAL TO SAVINGS for Row number ' + (@initials + 1).to_s + ' with Display Name ' + display_name
#       end
#     end
#   end
#   desc 'This task update master data it checks if unique id is present or not which indicates that if 
#         record is already present or not, it accordingly create or update the records'
#   task master_data: :environment do
#       Update.init
#   end
# end

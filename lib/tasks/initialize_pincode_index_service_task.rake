namespace :initialize do
  desc 'Initialize the pincode index this will ovrrite the index'
  task pincode_index: :environment do
    pincode_index_service = SearchModule::V1::PincodeIndexService.new
    pincode_index_service.initialize_index('true')
  end
end

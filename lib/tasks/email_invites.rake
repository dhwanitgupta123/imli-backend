namespace :request do

  module EmailInvites

    def self.init

      puts 'List of all the Users, who have requested email invite, started at:' + Time.zone.now.to_s
      initialize_services
      send_pending_user_mail
    end

    def self.initialize_services
      @email_invites_service = CommonModule::V1::EmailInvitesService.new({})
      @cache_util = CommonModule::V1::Cache
    end

    def self.send_pending_user_mail
      response = @email_invites_service.send_last_x_hours_email_invites_request_mail(@cache_util.read('DEFAULT_TIME_RANGE').to_i)
      puts response
    end
  end

  desc 'Rake task to send mail of list of email invite request'
  task :email_invites => :environment do
    EmailInvites.init
    puts "#{Time.zone.now} - Success!"
  end
end

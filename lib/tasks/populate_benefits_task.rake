namespace :populate do
  
  module PopulateBenefits

    BENEFITS_KEY = %W[name details condition reward_value reward_type status benefit_key] 

    def self.init
      STDOUT.puts 'please enter full path of csv'
      csv_file_path = STDIN.gets.strip

      initialize_models

      @field_map = {}
      count = 0

      CSV.foreach(csv_file_path) do |row|

        if count == 0
          map_fields(row)
          count = 1
          next
        end
        create_or_update_benifits(row)
      end
    end

    def self.initialize_models
      @benefits_model = CommonModule::V1::Benefit
    end

    def self.create_or_update_benifits(row)
      benefit_args = get_args(BENEFITS_KEY, row)
      benefit = @benefits_model.find_by(benefit_key: benefit_args[:benefit_key])
      return benefit.update_attributes!(benefit_args) if benefit.present?

      benefit = @benefits_model.new(benefit_args)
      benefit.save!
      return benefit
    end

    def self.get_args(keys, row)
      args = {}
      keys.each do |key|
        args[key.to_sym] = row[@field_map[key.to_sym]]
      end
      return args
    end

    def self.map_fields(row)
      index = 0
      
      row.each do |col|
        case col.downcase
        when 'name'
          @field_map[:name] = index
        when 'details'
          @field_map[:details] = index
        when 'condition'
          @field_map[:condition] = index
        when 'reward_value'
          @field_map[:reward_value] = index
        when 'reward_type'
          @field_map[:reward_type] = index
        when 'status'
          @field_map[:status] = index
        when 'benefit_key'
          @field_map[:benefit_key] = index
        end
        index += 1
      end
    end
  end

  desc 'To populate benifits'
  task benefits: :environment do
    PopulateBenefits.init
  end
end

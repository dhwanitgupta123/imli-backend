namespace :extend do

  module ExtendMembership

    def self.init
      initialize_services
      extend_user_membership
    end

    def self.initialize_services
      @membership = UsersModule::V1::Membership
      @membership_state = UsersModule::V1::ModelStates::MembershipStates
    end

    def self.extend_user_membership
      output_file = CSV.open('membership_extended.csv','w')
      ##Header##
      header = ['user_id', 'phone_number', 'name', 'email_id', 'expiry_date']
      output_file << header
      from_time = Time.zone.now - 30.days
      to_time = Time.zone.now + 30.days
      memberships = @membership.where(expires_at: [from_time..to_time], workflow_state: @membership_state::ACTIVE)
      memberships.each do |membership|
        user = membership.users.first
        user_id = user.id
        phone_number = user.phone_number
        name = user.first_name
        email_id = user.emails.first.email_id if user.emails.present?
        expiry_date = membership.expires_at.strftime("%d.%m.%Y")
        row = [user_id, phone_number, name, email_id, expiry_date]
        output_file << row
        membership.expires_at = membership.expires_at + 90.days
        membership.save!
      end
      output_file.close
    end
  end

  desc 'Rake task to extend membership of Users which are expired or going to expired in next month by 3 months'
  task :membership => :environment do
    ExtendMembership.init
    puts "#{Time.zone.now} - Success!"
  end
end

namespace :fetch do
  require 'csv'
  desc 'This task populate order_details (its product details, its user details, etc) for all the orders till date'
  task order_details: :environment do
    output_file = CSV.open('order_details.csv','w')
    order_stats_file = CSV.open('revenue_details.csv', 'w')

    @order_model = MarketplaceOrdersModule::V1::Order
    @order_product_service_instance = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService.new
    @order_state = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
    @order_helper = MarketplaceOrdersModule::V1::OrderHelper
    @payment_dao = MarketplaceOrdersModule::V1::PaymentDao
    @payment_helper = MarketplaceOrdersModule::V1::PaymentHelper

    total_sp = BigDecimal('0')
    total_mrp = BigDecimal('0')
    total_savings = BigDecimal('0')

    #### HEADERS ###
    header = ['order_id', 'order_date', 'user_id', 'phone_number', 'name', 'cart_id', 'order_product_id', 'mrp', 'selling_price', 'savings', 'vat', 'service_tax', 'taxes', 'discount',
              'quantity', 'product_name', 'product_size', 'mpsp_id', 'Department', 'Category', 'Sub_Category', 'Brand_Codes', 'Brand_Pack_Quantity', 'Brand_Pack_Company']
    output_file << header

    stats_header = ['order_id', 'order_date', 'user_id', 'phone_number', 'name', 'pincode', 'total_mrp', 'cart_savings', 'cart_total',  'delivery_charges', 'billing_amount', 'grand_total',
      'discount', 'net_total', 'total_savings', 'payment_mode', 'payment_status']
    order_stats_file << stats_header

    puts 'Start fetching Order details..........'

    orders = @order_model.all.where.not(status: [@order_state::INITIATED[:value], @order_state::ORDER_CANCELLED[:value]])

    orders.each do |order|
      puts 'Processing Order: ' + order.id.to_s
      cart = order.cart
      user = order.user
      order_date = @order_helper.get_order_date(order)
      order_date = Time.at(order_date.to_i)
      order_products = @order_product_service_instance.get_active_order_products_in_carts(cart)
      order_products.each do |op|
        mpsp = op.marketplace_selling_pack
        mpsp_sub_category = mpsp.mpsp_sub_category
        mpsp_category = mpsp_sub_category.mpsp_parent_category
        mpsp_department = mpsp_category.mpsp_department
        mpbps = mpsp.marketplace_brand_packs
        mpsp_mpbps = mpsp.marketplace_selling_pack_marketplace_brand_packs

        bp_codes = ''
        bp_company = ''
        mpbps.each do |mpbp|
          bp_codes = bp_codes + mpbp.brand_pack.brand_pack_code.to_s + '/'
          bp_company = bp_company + mpbp.brand_pack.product.sub_brand.brand.company.name.to_s + '/'
        end
        bp_codes = bp_codes[0..-2]
        bp_company = bp_company[0..-2]


        bp_quantity = ''
        mpsp_mpbps.each do |mpsp_mpbp|
          bp_quantity = bp_quantity + mpsp_mpbp.quantity.to_s + '/'
        end
        bp_quantity = bp_quantity[0..-2]

        row = [order.id, order_date, user.id, user.phone_number, user.first_name, cart.id, op.id, op.mrp, op.selling_price, op.savings, op.vat, op.service_tax, op.taxes, op.discount, op.quantity,
          mpsp.display_name, mpsp.display_pack_size, mpsp.id, mpsp_department.label, mpsp_category.label, mpsp_sub_category.label, bp_codes, bp_quantity, bp_company]
        output_file << row
      end

      payment = @payment_dao.new.get_orders_payment_as_per_its_mode(order)
      payment_display_labels = @payment_helper.get_payment_display_labels(payment)

      order_area = order.address.area
      stats_row = [order.id, order_date, user.id, user.phone_number, user.first_name, order_area.pincode, cart.total_mrp, cart.cart_savings, cart.cart_total, payment.delivery_charges, payment.billing_amount, payment.grand_total,
        payment.discount, payment.net_total, payment.total_savings, payment_display_labels[:mode_display_name], payment_display_labels[:status_display_name] ]

      order_stats_file << stats_row
    end
    puts 'Total orders count: ' + orders.count.to_s
    puts 'Details fetched successfully.........'
    output_file.close
    order_stats_file.close
  end
end

# PdfGenerator Library to convert into pdf from a template and given params:
#
# - pdf_file_name: file name to be give to the generated pdf file (without the .pdf suffix)
# - pdf_file_params: params to be needed to be rendered in the liquid template provided
# - relative_template_path: path of liquid template, relative from PATH: {Rails.root}/app/views
#
class PdfGenerator

  def generate_pdf(pdf_file_name, pdf_file_params, relative_template_path)
    file = File.join(Rails.root,
                     'app',
                     'views',
                      relative_template_path
                    )
    file = File.read(file)
    template = Liquid::Template.parse(file)
    template = template.render(ActiveSupport::HashWithIndifferentAccess.new(pdf_file_params))
    wicked_pdf_instance = WickedPdf.new
    pdf = wicked_pdf_instance.pdf_from_string(template)
    file_path = "#{Rails.root}/public/"
    filename = "#{pdf_file_name}.pdf"
    pdf_path = file_path + filename
    File.open(pdf_path, 'wb') do |file|
      file << pdf
    end
    return { file_path: pdf_path, filename: filename }
  end

end

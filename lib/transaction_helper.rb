# Transactions Helper Library to run transactions over any code block.
# 
# Usage:
# Use following code where you want to use transaction:
# 
# #####
# transactional_function = Proc.new do |args|
# 	
# <put your code to be executed inside transaction in this block>
# 
# end
# transaction_status = TransactionHelper.new({
#           :function => transactional_function,
#           :args => args
#           })
#         transaction_status.run();
# #####
# 
class TransactionHelper

	def initialize(parameters)
		@transactional_function = parameters[:function]
		@args = parameters[:args]
	end

	def run
 		ActiveRecord::Base.transaction do
 			@transactional_function.call(@args)
 		end
	end

end
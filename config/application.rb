require File.expand_path('../boot', __FILE__)

require 'rails/all'
require 'json'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ImliBackend
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Mumbai'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Filter sensitive parameters
    config.filter_parameters << :password

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # Auto load files in app/services
    config.autoload_paths += %W(
        #{config.root}/app/services
        #{config.root}/app/utils
        #{config.root}/app/models/users_module/v1/model_states
        #{config.root}/app/data_access_object
    )

    # Change default headers of response to allow cross origin calls
    config.middleware.insert_before 0, 'Rack::Cors' do
      allow do
        # allow all the domains to access our (cross origin) resources and
        # can hit our APIs successfully
        origins '*'
        # allow following configurations for all the resources
        resource '*',
          headers: :any,
          methods: [:get, :post, :delete, :put, :patch, :options, :head]
      end
    end

    #RSpec & Factory Girl configuration
    config.generators do |g|
      g.test_framework :rspec,
        :fixtures => true,
        :view_specs => false,
        :helper_specs => false,
        :routing_specs => false,
        :controller_specs => true,
        :request_specs => true
      g.fixture_replacement :factory_girl, :dir => "spec/factories"
      g.orm :active_record
    end

    config.before_initialize do
      # Anything which needs to be initialized or fetched before all other initializers/*.rb
      # should be invoked here in this block
      puts '--> Initializing Redis Cache (before initializing other things)'
      $redis = Redis.new(
        :host => APP_CONFIG["config"]["redis_host"],
        :port => APP_CONFIG["config"]["redis_port"]
      )
    end


    config.after_initialize do
      # Loading configuration for mongoDB
      Mongoid.load!("config/mongoid.yml")
    end


  end
end

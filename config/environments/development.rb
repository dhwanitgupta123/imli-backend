Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = true
  
  # Use Redis store as caching data store.
  # Please note:Keys will be stored in redis with specified namespacing
  # Ex:
  # Rails.cache.write("key", "value")   ~ OK
  # Rails.cache.read("key")             ~ "value"
  # $redis.get("key")                   ~ nil
  # $redis.get("<namespace>:key")       ~ ActiveSupport::Cache::Entry object
  config.cache_store = :redis_store, { :host => APP_CONFIG['config']['redis_host'],
                                     :port => APP_CONFIG['config']['redis_port'],
                                     :namespace => APP_CONFIG['config']['redis_namespace'] }

  # Configurations for Action mailer setup.
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default_url_options = { :host => APP_CONFIG['config']['action_mailer_host'] }
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    :address              => APP_CONFIG['config']['action_mailer_address'],
    :port                 => APP_CONFIG['config']['action_mailer_port'],
    :domain               => APP_CONFIG['config']['action_mailer_domain'],
    :user_name            => APP_CONFIG['config']['action_mailer_username'],
    :password             => APP_CONFIG['config']['action_mailer_password'],
    :authentication       => :plain,
    :enable_starttls_auto => true
  }

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

end

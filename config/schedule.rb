# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
require File.expand_path(File.dirname(__FILE__) + '/../config/environment')

set :environment, Rails.env
set :output, { error: 'log/cron_error_log.log', standard: 'log/cron_log.log' }


##################################################################################
#                                 Misc. Crons                                    #
##################################################################################

# Runs Pending Users, Email Invites, Product Suggestions Cron everyday at 10:00 hrs
every 1.day, at: Time.zone.now.change(hour: 10).in_time_zone('UTC') do
  rake 'pending:users'
  rake 'request:email_invites'
  rake 'product:suggestions'
end

#### Misc. Crons Ends ####


##################################################################################
#                           Order Aggregation Crons                              #
##################################################################################

# # Runs Order Aggregation for interval "00:00" to "08:00" hrs everyday at 08:00 hrs
# every 1.day, at: Time.zone.now.change(hour: 8, min: 00).in_time_zone('UTC') do
#   rake 'aggregate:orders["00:00:01","08:00:00"]'
# end
#
# # Runs Order Aggregation for interval "08:00:01" to "13:00:00" hrs everyday at 13:00 hrs
# every 1.day, at: Time.zone.now.change(hour: 13, min: 00).in_time_zone('UTC') do
#   rake 'aggregate:orders["08:00:01","13:00:00"]'
# end
#
# # Runs Order Aggregation for interval "13:00:01" to "18:00:00" hrs everyday at 18:00 hrs
# every 1.day, at: Time.zone.now.change(hour: 18, min: 00).in_time_zone('UTC') do
#   rake 'aggregate:orders["13:00:01","18:00:00"]'
# end
#
# # Runs Order Aggregation for interval "18:00:01" to "23:59:59" hrs everyday at 23:59 hrs
# every 1.day, at: Time.zone.now.change(hour: 00, min: 00).in_time_zone('UTC') do
#   rake 'aggregate:orders["18:00:01","00:00:00"]'
# end
#

# Runs Order Aggregation for interval "07:00:01" to "15:00:00" hrs everyday at 15:00 hrs
every 1.day, at: Time.zone.now.change(hour: 15, min: 00).in_time_zone('UTC') do
  rake 'aggregate:orders["07:00:01","15:00:00"]'
end

# Runs Order Aggregation for interval "15:00:01" to "07:00:00" hrs everyday at 07:00 hrs
every 1.day, at: Time.zone.now.change(hour: 07, min: 00).in_time_zone('UTC') do
  rake 'aggregate:orders["15:00:01","07:00:00"]'
end

#### Order Aggregation Crons Ends ####


##################################################################################
#                          Kibana Data Aggregation Cron                          #
##################################################################################
#
# This will run every day at 01:00 hours and update data that is used
# by kibana
#
every 1.day, at: Time.zone.now.change(hour: 01, min: 00).in_time_zone('UTC') do
  runner 'DataAggregationModule::V1::KibanaDataAggregationService.aggregate'
end

#### Kibana Data Aggregation Cron Ends ####

#
# Commenting this cron as we are not using it currently
#
# every 1.day, at: Time.zone.now.change(hour: 01, min: 00).in_time_zone('UTC') do
#   runner 'SweeperModule::V1::ImageSweeperWorker.perform_async'
# end

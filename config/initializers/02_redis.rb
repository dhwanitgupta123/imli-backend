# global variable to access Redis cache
# Requirement: Redis server should be up and running
# at below specified host and port
# puts 'Initializing Redis from initializers/redis.rb '
# $redis = Redis.new(
#   :host => APP_CONFIG["config"]["redis_host"],
#   :port => APP_CONFIG["config"]["redis_port"]
# )
#
# Initializing product repository
#
elastic_search_url = APP_CONFIG['config']['ELASTIC_SEARCH_URL']

PRODUCT_REPOSITORY = DataAggregationModule::V1::ProductRepository.new url: elastic_search_url, log: true

ORDER_REPOSITORY = DataAggregationModule::V1::OrderRepository.new url: elastic_search_url, log: true

PAYMENT_REPOSITORY = DataAggregationModule::V1::PaymentRepository.new url: elastic_search_url, log: true

USER_REPOSITORY = DataAggregationModule::V1::UserRepository.new url: elastic_search_url, log: true

MASTER_PRODUCT_REPOSITORY = DataAggregationModule::V1::MasterProductRepository.new url: elastic_search_url, log: true

API_LOG_REPOSITORY = DataAggregationModule::V1::ApiLogRepository.new url: elastic_search_url, log: true

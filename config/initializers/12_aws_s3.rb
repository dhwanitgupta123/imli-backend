# S3 configurations for Image Uploading functionality
puts '--> Initializing S3 bucket configurations'

S3_PRIVATE_FOLDER = APP_CONFIG['config']['S3_PRIVATE_FOLDER']

S3  = AWS::S3.new(
  :access_key_id => APP_CONFIG['config']['AWS_ACCESS_KEY_ID'],
  :secret_access_key => APP_CONFIG['config']['AWS_SECRET_ACCESS_KEY'],
  :s3_endpoint => APP_CONFIG['config']['S3_HOST_NAME']
)

S3_BUCKET = S3.buckets[APP_CONFIG['config']['S3_BUCKET']]

begin
S3_BUCKET.exists?
rescue => e
puts '[ERROR]: S3 Bucket failed to initialize due to the following error ' + e.message
end

if Rails.env == 'production'

  S3_IMAGE_STYLES = {
    xxxxhdpi_large: '1720x1720>',
    # xxxxhdpi_medium: -> use hdpi_large
    # xxxxhdpi_small:  -> use xhdpi_medium
    xxxhdpi_large: '1376x1376>',
    xxxhdpi_medium: '460x460>',
    xxxhdpi_small: '192x192>',
    xxhdpi_large: '1032x1032>',
    xxhdpi_medium: '345x345>',
    xxhdpi_small: '144x144>',
    xhdpi_large: '688x688>',
    xhdpi_medium: '230x230>',
    xhdpi_small: '96x96>',
    hdpi_large: '516x516>',
    hdpi_medium: '173x173>',
    hdpi_small: '72x72>',
    mdpi_large: '344x344>',
    mdpi_medium: '115x115>',
    mdpi_small: '48x48>'
  }
  S3_CONVERT_OPTIONS = {
    xxxxhdpi_large: '-quality 80',
    xxxhdpi_large: '-quality 80',
    xxxhdpi_medium: '-quality 80',
    xxxhdpi_small: '-quality 90',
    xxhdpi_large: '-quality 80',
    xxhdpi_medium: '-quality 90',
    xxhdpi_small: '-quality 90',
    xhdpi_large: '-quality 80',
    xhdpi_medium: '-quality 80',
    xhdpi_small: '-quality 90',
    hdpi_large: '-quality 80',
    hdpi_medium: '-quality 90',
    hdpi_small: '-quality 90',
    mdpi_large: '-quality 90',
    mdpi_medium: '-quality 90',
    mdpi_small: '-quality 90'
  }
else
  S3_IMAGE_STYLES = {
    xxxxhdpi_large: '1720x1720>',
    xxxhdpi_large: '1376x1376>',
    xxhdpi_large: '1032x1032>',
    xhdpi_large: '688x688>',
    hdpi_large: '516x516>',
    xhdpi_medium: '230x230>'
  }
  S3_CONVERT_OPTIONS = {
    xxxxhdpi_large: '-quality 70',
    xxxhdpi_large: '-quality 70',
    xxhdpi_large: '-quality 70',
    xhdpi_large: '-quality 70',
    mdpi_large: '-quality 70',
    xhdpi_medium: '-quality 70'
  }
end


puts '--> Initializing SHIELD AB testing configuration'
puts ''

#
# Fetch experiments details from CONSUL with fallback on local file
#
def self.get_experiments
  experiments_relative_store_url = '/default/default/default/default/EXPERIMENTS'
  experiments_local_path = 'config/initializers/settings_module/experiments.yml'
  distributed_store_util = DistributedStoreModule::V1::DistributedStoreUtil

  experiments = distributed_store_util.fetch_environment_specific_config_from_distributed_store(experiments_relative_store_url)
  if experiments.blank?
    experiments_file_path = Rails.root.join(experiments_local_path).to_s
    if File.file?(experiments_file_path)
      experiments = YAML.load_file(experiments_file_path)[Rails.env]
      puts '[SUCCESS]: YML - Experiments fetched from local file path: ' + experiments_file_path.to_s
    end
  end
  return experiments
end


###############################################################
#                  Split gem configuration                    #
###############################################################

# Add Split configuration
SHIELD_PARAMS = {}
Split.configure do |config|
  
  redis_host = APP_CONFIG['config']['redis_host']
  redis_port = APP_CONFIG['config']['redis_port']
  redis_url = redis_host + ':' + redis_port

  config.redis_url = redis_url
  config.allow_multiple_experiments = true
  config.start_manually = true

  config.experiments = get_experiments

  # Our own custom methods to support functionality of split gem from other places also
  # apart from controller and view only.
  config.custom_override = Proc.new { |experiment_name| SettingsModule::V1::Split.shield_experiment_override(experiment_name) }
  config.custom_request = Proc.new { SettingsModule::V1::Split.shield_custom_request }

  # Need to persist user behaviour in Redis, and hence need a unique identifier
  config.persistence = Split::Persistence::RedisAdapter.with_config(lookup_by: :shield_unique_identifier)
  
end


###############################################################
#       Redis Namespace for persisting Shield values          #
###############################################################

Split.redis.namespace = "imli:shield:split"

###############################################################
#             CREDENTIALS for Split Dashboard                 #
###############################################################

# Assigning username and password for route '../shield'
# Do not apply Authentication for local environment and test environment
if Rails.env != 'development' && Rails.env != 'test'
  shield_username = APP_CONFIG['config']['SHIELD_DASHBOARD_USERNAME']
  shield_password = APP_CONFIG['config']['SHIELD_DASHBOARD_PASSWORD']
  Split::Dashboard.use Rack::Auth::Basic do |username, password|
    username == shield_username && password == shield_password
  end
end

puts '********************************************'
puts ''
# Add Raven configuration
puts '--> Initializing Raven-Sentry'
Raven.configure do |config|
  # Sensitive information should not get logged in sentry.
  # Specify sensitive information in Rails configuration
  config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)

  # Add sentry DSN configuration (sentry url)
  config.dsn = APP_CONFIG["config"]['SENTRY_DSN']
  # sentry should work in following environments
  config.environments = ['feature', 'production', 'pre_prod']

  # Sending data to sentry asynchronously
  config.async = lambda { |event|
    Thread.new { Raven.send_event(event) }
  }

  # Stop printing messages on console while starting Raven
  # Original message be like INFO -- : ** [Raven] Raven 0.9.4 ready to catch errors"
  config.silence_ready = true
end
require 'json'

puts ''
puts '********************************************'
puts '********** LOADING ALL RESOURCES ***********'
puts '********************************************'

puts 'Loading all required Resources from Consul'
puts 'with a fallback on local stored yml files'
puts ''

###############################################################
#         LOAD ALL APPLICATION RESOURCES HERE                 #
###############################################################

#
# Write Key Value pair to Redis
#
# @param key [String] [Key for which value is to be stored]
# @param value [Object] [Value to be stored]
#
def write_key_value_to_redis(key, value)
  CommonModule::V1::Cache.write({
    key: key.to_s,
    value: value.to_s
    })
end

# 
# Function to write Environment specific constants to the Redis Cache
# 
# Parameters::
#   * file [string] path to the constant file 
# 
def write_file_entries_to_redis(file)
  if File.file?(file)
    YAML.load_file(file)[Rails.env].each do |key, value|
      write_key_value_to_redis(key, value)
    end # end YAML.load_file
  end # end if File.exists?
end

def write_complete_file_as_a_variable_to_redis(variable_name, complete_file_path)
  if File.file?(complete_file_path)
    file = YAML.load_file(complete_file_path)
    # Store into Redis after converting into JSON string
    write_key_value_to_redis(variable_name, JSON.generate(file) )
  end 
end

def write_individual_values_to_redis(hash)
  return {} if hash.blank?
  hash.each do |key, value|
    write_key_value_to_redis(key, value)
  end
end


########################################################################
#        STORE COMPLETE FILE AS A VARIABLE INTO REDIS CACHE            #
########################################################################

# Load permissions file into cache
permissions_file_path = Rails.root.join("db", "users_module/permissions.yml").to_s
# Referal Codes File
referral_codes_file_path = Rails.root.join("config", "initializers/users_module/referral_codes.yml").to_s

write_complete_file_as_a_variable_to_redis("PERMISSIONS_LOOKUP", permissions_file_path)
write_complete_file_as_a_variable_to_redis("REFERRAL_CODES", referral_codes_file_path)


########################################################################
#        TRAVERSE FILE AND STORE EACH VARIABLE INTO REDIS CACHE        #
########################################################################


# Put ALL common constants in redis cache, irrespective of current environment
# common constants can be find in config/initializers/constants.yml file
# under key 'all'
static_files = [
  { consul_path: '/backend/imli-backend/default/default/CONSTANTS', file_path: "config/initializers/users_module/constants.yml" },
  { consul_path: '/backend/imli-backend/default/default/EMAIL_CONSTANTS' , file_path: "config/initializers/communications_module/email_constants.yml" }
]
static_files.each do |static_file|
  puts '--------------------------------------------'
  file_hash = DistributedStoreModule::V1::DistributedStoreUtil.fetch_environment_specific_config_from_distributed_store(static_file[:consul_path])
  if file_hash.blank?
    file_path = static_file[:file_path]
    if File.file?(file_path)
      file_hash = YAML.load_file(file_path)[Rails.env]
      puts '[SUCCESS]: YML - Config fetched from local YML file successfuly'
    end
  end
  write_individual_values_to_redis(file_hash)
end

########################################################################
#                    STORE INTO GLOBAL VARIABLE                        #
########################################################################

# Load all routing specific application config into a global variable
route_file = Rails.root.join("config", "initializers", "common_module/app_versioning_routes.yml").to_s
$APP_ROUTING_CONFIG = YAML.load_file(route_file)


########################################################################
#                        OTHER TYPES OF STORING                        #
########################################################################

# Store APP CONFIG into Cache too. Convert HASH into JSON String before saving it to redis
write_key_value_to_redis('APP_CONFIG', JSON.generate(APP_CONFIG['config']) )

# Fetch FEATURES value from Consul and store it in redis
SettingsModule::V1::SettingsUtil.get_and_locally_store_complete_settings

puts ''
puts 'Resources Loaded successfuly'
puts '********************************************'
puts ''


require "route_constraints"
Rails.application.routes.draw do
  require 'sidekiq/web'

  mount SupplyChainEngine::Engine => '/'
  
  mount LocationEngine::Engine => '/'

  mount OrderEngine::Engine => '/'

  mount Split::Dashboard, at: 'shield' # ex: http://localhost:3000/shield/ --> SHIELD Dashboard

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  
  # Predicate for versioning based on app version
  match_app_version_predicate = Proc.new do |version, request|
    CommonModule::V1::RoutingPredicates.match_app_version(version, request)
  end

  # Route for UsersModule with versioning and constraint matching
  scope module: :users_module, defaults: {format: 'json'} do
    scope module: :v1,
      constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      # User related APIs
      post    'users/add_role'        =>  'users#add_role_to_user'
      post    'users/remove_role'     =>  'users#remove_role_from_user'
      put     'users/state/:id'       =>  'users#change_state'
      get     'users'                 =>  'users#get_all_users'
      put     'users/referral_limit/:id'  =>  'users#refill_referral_limit'

      # Employee related APIs
      post    'employee/register'     => 'employees#register'
      post    'employee/login'        => 'employees#login'
      get     'employee/approve'      => 'employees#approve'
      post    'employee/add_profile'  => 'employees#add_profile'
      # Roles related APIs
      post    'roles/create'          =>  'roles#create_role'
      put     'roles/:role_id'        =>  'roles#update_role'
      get     'roles/user_role'       =>  'roles#get_user_role'
      get     'roles/role_tree'       =>  'roles#get_roles_tree'
      get     'roles/:role_id/users'  =>  'roles#get_users_for_role'
      get     'roles/permissions'     =>  'roles#get_all_permissions'
      # User API related routes
      post    'take_my_phone_number'  => 'users_api#decide_register_login'
      post    'login'                 => 'users_api#login'
      post    'resend_otp'            => 'users_api#resend_otp'
      post    'user/check'            => 'users_api#check_user'
      put     'api/users/referred' => 'users_api#activate_with_referral_code'
      get     'api/users/state'      => 'users_api#check_user_state'
      put     'api/users/details'      => 'users_api#update_user_details'
      put     'users/referral/benefits' => 'users_api#apply_referral_benefits'
      get     'api/users/ample_credits' =>  'users_api#get_current_ample_credits'
      # Users password related routes
      post    'api/users/password/create'   => 'users_api#create_password'
      # Users email related routes
      get     'api/users/verify_email'    => 'emails_api#verify_email', defaults: {format: 'html'}
      post    'api/users/resend_email_token' => 'emails_api#resend_email_token'
      # Users address related routes
      get     'api/users/addresses' => 'addresses_api#get_all_addresses_of_user'
      post    'api/users/addresses/new'      => 'addresses_api#add_address'
      put     'api/users/addresses/:id'   => 'addresses_api#update_address'
      # Users membership related APIs
      post    'api/membership/subscribe'  => 'memberships_api#subscribe_to_membership'
      post    'api/membership/payment/initiate' => 'memberships_api#payment_initiate'
      post    'api/membership/payment/success' => 'memberships_api#payment_success'
      # Users profile related APIs
      post    'api/profile/pincode' => 'profiles#add_pincode'
    end
    # CAN BE EXTENDED TO 'V2' AS BELOW:
    # scope module: :v2,
    #   constraints: RouteConstraints.new(version: '2', default: true, predicate: match_app_version_predicate) do
    #   get "users", to: "users#index"
    # end
  end

  # Route for CategorizationModule with versioning and constraint matching
  scope module: :categorization_module, defaults: {format: 'json'} do
    scope module: :v1,
      constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      post    'departments/new' => 'departments#create_department'
      put     'departments/update/:id' => 'departments#update_department'
      get     'departments/:id' => 'departments#get_department'
      get     'departments' => 'departments#get_all_departments'
      put     'departments/state/:id' => 'departments#change_state'
      get     'get_all_departments_categories_and_sub_categories' => 'departments#get_all_departments_categories_and_sub_categories'

      post    'mpsp/departments/new' => 'mpsp_departments#create_mpsp_department'
      put     'mpsp/departments/update/:id' => 'mpsp_departments#update_mpsp_department'
      get     'mpsp/departments/:id' => 'mpsp_departments#get_mpsp_department'
      get     'mpsp/departments' => 'mpsp_departments#get_all_mpsp_departments'
      put     'mpsp/departments/state/:id' => 'mpsp_departments#change_state'
      get     'mpsp/get_all_departments_categories_and_sub_categories' => 'mpsp_departments#get_all_mpsp_departments_categories_and_sub_categories'

      post    'categories/new' => 'categories#create_category'
      put     'categories/update/:id' => 'categories#update_category'
      get     'categories/:id' => 'categories#get_category'
      get     'categories' => 'categories#get_all_categories'
      put     'categories/state/:id' => 'categories#change_state'

      post    'sub_categories/new' => 'categories#create_sub_category'
      put     'sub_categories/update/:id' => 'categories#update_sub_category'
      get     'sub_categories/:id' => 'categories#get_category'
      get     'sub_categories' => 'categories#get_all_sub_categories'

      post    'mpsp/categories/new' => 'mpsp_categories#create_mpsp_category'
      put     'mpsp/categories/update/:id' => 'mpsp_categories#update_mpsp_category'
      get     'mpsp/categories/:id' => 'mpsp_categories#get_mpsp_category'
      get     'mpsp/categories' => 'mpsp_categories#get_all_mpsp_categories'
      put     'mpsp/categories/state/:id' => 'mpsp_categories#change_state'

      post    'mpsp/sub_categories/new' => 'mpsp_categories#create_mpsp_sub_category'
      put     'mpsp/sub_categories/update/:id' => 'mpsp_categories#update_mpsp_sub_category'
      get     'mpsp/sub_categories/:id' => 'mpsp_categories#get_mpsp_category'
      get     'mpsp/sub_categories' => 'mpsp_categories#get_all_mpsp_sub_categories'

      get     'api/departments' => 'mpsp_categorization_api#get_all_mpsp_departments_details'
      get     'api/products/categories/:id'    => 'mpsp_categorization_api#get_products_by_mpsp_category'
      post    'api/products/categories/:id/filter'    =>  'mpsp_categorization_api#get_products_by_filters'

      get     'api/mpsp/departments' => 'mpsp_categorization_api#get_all_mpsp_departments_details'
      get     'api/mpsp/products/categories/:id'    => 'mpsp_categorization_api#get_products_by_mpsp_category'
      post    'api/mpsp/products/categories/:id/filter'    =>  'mpsp_categorization_api#get_products_by_filters'
    end
  end

  #
  # Routes for CommonModule
  scope module: :common_module, defaults: {format: 'json'} do
    scope module: :v1,
      constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      # plans controller
      post      'plans/new'        =>      'plans#create_plan'
      get       'plans'            =>      'plans#get_all_plans'
      get       'plans/:id'        =>      'plans#get_plan'
      put       'plans/:id'        =>      'plans#update_plan'
      put       'plans/state/:id'  =>      'plans#change_state'
      delete    'plans/:id'        =>      'plans#delete_plan'
      #plansAPI controller
      get       'api/plans'        =>      'plans_api#get_all_plans'
      # Benefits Controller
      get       'benefits/active'  =>      'benefits#get_all_active_benefits'
      # product suggester controller
      post      'products/suggestions/update' =>    'products_suggester#update_product_suggestions'
      get       'products/suggestions'        =>    'products_suggester#get_all_product_suggestions'

      # carousels controller
      post      'carousels/update/:type'      =>    'carousels#update_carousel'
      get       'carousels/types'             =>    'carousels#get_orphan_carousels_types_with_details'
      get       'carousels/:type'             =>    'carousels#get_carousel'
      get       'carousels'                   =>    'carousels#get_all_carousels'
      # email invites controller
      post      'email/invites'    =>       'email_invites#email_invites' 

    end
  end

  # Route for ImageServiceModule with versioning and constraint matching
  scope module: :image_service_module, defaults: {format: 'json'} do
    scope module: :v1,
       constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
        post     'upload/image' => 'images#upload_image'
        delete    'images/:id'        =>      'images#delete_image'
    end
  end

  # Route for SettingsModule with versioning and constraint matching
  scope module: :settings_module, defaults: {format: 'json'} do
    scope module: :v1,
      constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      get     'settings'          =>    'settings#get_static_settings'
      get     'settings/dynamic'  =>    'settings#get_dynamic_settings'
    end
  end

  # Route for PaymentModule with versioning and constraint matching with return format json
  scope module: :payment_module, defaults: {format: 'json'} do
    scope module: :v1,
      constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      post 'citrus/transaction/callback' => 'citrus_payments#transaction_callback'
      post 'citrus/url/generate' => 'citrus_payments#generate_url'
    end
  end

  # Route for PaymentModule with versioning and contraint matching with return format html
  scope module: :payment_module do
    scope module: :v1,
      constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      get 'citrus/web_view' => 'citrus_payments#web_view'
    end
  end

  # Route for TransactionsModule with versioning and constraint matching
  scope module: :transactions_module, defaults: {format: 'json'} do
    scope module: :v1,
      constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      get 'transaction/status/:id' => 'transactions#get_transaction_status'
      post 'transaction/initiate' => 'payment_transactions#initiate_transaction'
      post 'transaction/update' => 'payment_transactions#update_transaction'
      post 'transaction/record' => 'payment_transactions#record_transaction'
    end
  end

  scope module: :scripts_module, defaults: {format: 'json'} do
    scope module: :v1,
      constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      post 'compare/prices' => 'scripts#compare_prices'
    end
  end

  scope module: :descriptions_module, defaults: {format: 'json'} do
    scope module: :v1,
      constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      post 'descriptions/new' => 'descriptions#add_description'
      get 'descriptions/:id' => 'descriptions#get_description'
      put 'descriptions/:id' => 'descriptions#update_description'
    end
  end

  #
  # Routes for CommonModule
  # 
  scope module: :common_module, defaults: {format: 'json'} do
    scope module: :v1,
      constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      get 'constants/cluster_constants' => 'constants#get_cluster_constants'
    end
  end

  #
  # route to mount sidekiz datapanel 
  # 
  mount Sidekiq::Web => '/sidekiq'

  #
  # route to use Swagger_ui RESTful APIs doc
  # 
  if Rails.env != 'production'
    get '/apidocs' => redirect('/swagger/dist/index.html?url=/api/v1/api-docs.json')
  end

  #
  # Routes for CommonModule
  scope module: :common_module, defaults: {format: 'json'} do
    # THIS should be the last matcher for Routes
    # If nothing matches above mentioned route, then render 404
    match '*unmatched_route', to: 'common#not_found', via: :all
  end
# End of Routes
end

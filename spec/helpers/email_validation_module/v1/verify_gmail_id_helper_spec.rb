module EmailValidationModule
  module V1
    require 'rails_helper'
    RSpec.describe EmailValidationModule::V1::VerifyGmailIdHelper do

      let(:valid_gmail_id_with_dots) {"xy.zq.sd.a@gmail.com"}
      let(:invalid_gmail_id) {"xyzasds@yahoo.com"}
      let(:verify_gmail_id_helper) {EmailValidationModule::V1::VerifyGmailIdHelper}
      let(:cache) {CommonModule::V1::Cache}

      it "with invalid gmail id" do
        response = verify_gmail_id_helper.verify_if_already_in_use(invalid_gmail_id)
        expect(response).to eq(true)
      end

      context "With valid gmail id" do
        let(:user_name) {verify_gmail_id_helper.get_user_name(valid_gmail_id_with_dots)}
        let(:key) {verify_gmail_id_helper.create_key(user_name)}
        it "not present before" do          
          expect(cache).to receive(:exists?).and_return(false)
          response = verify_gmail_id_helper.verify_if_already_in_use(valid_gmail_id_with_dots)
          expect(response).to eq(false)
        end

        it "already exists" do
          expect(cache).to receive(:exists?).and_return(true)
          response = verify_gmail_id_helper.verify_if_already_in_use(valid_gmail_id_with_dots)
          expect(response).to eq(true)
        end
      end
    end
  end
end

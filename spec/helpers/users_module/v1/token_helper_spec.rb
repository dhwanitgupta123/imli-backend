module UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::TokenHelper do
      let(:token_helper) { UsersModule::V1::TokenHelper }

      it " generates authentication_token  " do
        token_helper.generate_authentication_token.should_not be_nil
      end

      it " generates session_token  " do
        user_id = 1
        token_helper.generate_session_token(user_id).should_not be_nil
      end
    end
  end
end
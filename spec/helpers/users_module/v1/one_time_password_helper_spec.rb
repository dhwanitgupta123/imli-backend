module UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::OneTimePasswordHelper do
      let(:otp_verification) { UsersModule::V1::OneTimePasswordHelper }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }

      it "verifies otp successfully" do
        otp = 5674
        message  = UsersModule::V1::ProvidersModule::V1::Message.new(:otp => 5674, :expires_at => Time.zone.now + 5.minutes)
        otp_verification.verify_otp(otp, message).should be_truthy
      end

      it "otp doesn't matches" do
        otp = 5673
        message  = UsersModule::V1::ProvidersModule::V1::Message.new(:otp => 5674, :expires_at => Time.zone.now + 5.minutes)
        expect{ otp_verification.verify_otp(otp, message) }.to raise_error(
          custom_error_util::WrongOtpError)
      end

      it "otp has expired " do
        otp = 5674
        message  = UsersModule::V1::ProvidersModule::V1::Message.new(:otp => 5674, :expires_at => Time.zone.now - 5.minutes)
        expect{ otp_verification.verify_otp(otp, message) }.to raise_error(
          custom_error_util::AuthenticationTimeoutError)
      end

      it " generates OTP " do
        otp_verification.generate_otp.should_not be_nil
      end
    end
  end
end
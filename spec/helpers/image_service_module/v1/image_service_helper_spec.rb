#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    require 'rails_helper'
    RSpec.describe ImageServiceModule::V1::ImageServiceHelper do

      let(:image_service_helper) { ImageServiceModule::V1::ImageServiceHelper }
      let(:image_model) { ImageServiceModule::V1::Image }
      let(:image) { FactoryGirl.build_stubbed(:image) }
      let(:cache_util) { CommonModule::V1::Cache }

      let(:image_hash_array) {
        [
          {
            'priority' => 2,
            'image_id' => 1
          },
          {
            'priority' => 3,
            'image_id' => 2
          },
          {
            'priority' => 1,
            'image_id' => 3
          }
        ]
      }

      context 'get sorted image_id_array by priority' do
        it 'with valid hash array' do
          image_id_array = image_service_helper.get_sorted_image_id_array_by_priority(image_hash_array)
          expect(image_id_array).to eq([3,1,2])
        end

        it 'with empty array' do
          image_id_array = image_service_helper.get_sorted_image_id_array_by_priority([])
          expect(image_id_array).to eq([])
        end
      end

      context 'get images_hash by ids' do

        it 'image_hash pressent in cache' do
          expect(cache_util).to receive(:read_json).and_return({original: Faker::Internet.url }.to_json)
          image_hash_array = image_service_helper.get_images_hash_by_ids([image.id])
          expect(image_hash_array[0][:image_id]).to eq(image.id)
        end

        it 'with valid image ids not in cache' do
          expect(image_model).to receive(:find_by).at_least(:once).and_return(image)
          expect(cache_util).to receive(:read_json).and_return(nil)
          expect(cache_util).to receive(:write).and_return(true)
          image_hash_array = image_service_helper.get_images_hash_by_ids([image.id])
          expect(image_hash_array[0][:image_id]).to eq(image.id)
        end

        it 'with empty array' do
          image_hash_array = image_service_helper.get_images_hash_by_ids([])
          expect(image_hash_array).to eq([])
        end
      end

      context 'validate images' do
        it 'with image id present' do
          expect(image_model).to receive(:ids).and_return([image.id])
          data = image_service_helper.validate_images([image.id])
          expect(data).to eq(true)
        end

        it 'with image id not present' do
          expect(image_model).to receive(:ids).and_return([-1*image.id])
          data = image_service_helper.validate_images([Faker::Number.number(1)])
          expect(data).to eq(false)
        end

        it 'with blank array' do
          data = image_service_helper.validate_images([])
          expect(data).to eq(true)
        end
      end
    end
  end
end

module UsersModule
  module V1
    require 'rails_helper'
    require 'json'

    RSpec.describe UsersModule::V1::UsersApiController, type: :controller do

      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:provider_type_util) { UsersModule::V1::ProviderType }
      let(:user_service) { UsersModule::V1::UserService }
      let(:user_response_decorator) { UsersModule::V1::UserResponseDecorator }
      let(:create_password_api) { UsersModule::V1::CreatePasswordApi }
      let(:check_user_api) { UsersModule::V1::CheckUserApi }
      let(:get_ample_credit_of_user_api) { UsersModule::V1::GetAmpleCreditOfUserApi }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) {access_control_controller.any_instance.stub(:after_actions).and_return(true)}
      let(:access_control_controller){UsersModule::V1::AccessControlController}
      let(:user_params) { {:phone_number => Faker::Number.number(10).to_s, :first_name => Faker::Lorem.characters(7)} }
      let(:request_type_sms) { provider_type_util::SMS }
      let(:request_type_generic) { provider_type_util::GENERIC }
      let(:session_token) {Faker::Lorem.characters(64)}

      before(:each) do
        before_actions
        after_actions
      end

      describe "check user is registered with us via api /check_user" do
        let(:phone_number_hash) {
          {
            phone_number: Faker::Number.number(10).to_s
          }
        }
        it "phone number present in our DB" do
          stub_ok_response = {
            "payload": {
              "is_user": true
            },
            "response": response_codes_util::SUCCESS
          }
          check_user_api.any_instance.should_receive(:enact).and_return(stub_ok_response)
          post :check_user, :user => phone_number_hash, :session_token => session_token
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it "when required params not present" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes_util::BAD_REQUEST
          }
          check_user_api.any_instance.should_receive(:enact).and_return(stub_bad_request_response)
          post :check_user, :user => phone_number_hash, :session_token => session_token
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end

      describe "Decide register login #decide_register_login" do
        it "register/login successfully" do
          stub_ok_response = {
            "payload": {
              "user": {
                "phone_number": Faker::Number.number(10).to_s
              }
            },
            "response": response_codes_util::SUCCESS
          }
          user_service.any_instance.stub(:decide_register_login).and_return(stub_ok_response)
          post :decide_register_login, :user => user_params, :request_type => request_type_sms
          expect(response).to have_http_status(status_codes_util::SUCCESS)
          expect(JSON.parse(response.body)["payload"].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it "when required params are not present" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes_util::BAD_REQUEST
          }
          user_service.any_instance.stub(:decide_register_login).and_return(stub_bad_request_response)
          post :decide_register_login, :user => user_params, :request_type => request_type_sms
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it "when generic is present in db" do
          stub_existing_user_response = {
            "error": { :message => content_util::UNAUTHENTIC_USER },
            "response": response_codes_util::UN_AUTHORIZED
          }
          user_service.any_instance.stub(:decide_register_login).and_return(stub_existing_user_response)
          post :decide_register_login, :user => user_params, :request_type => request_type_sms
          expect(response).to have_http_status(status_codes_util::UN_AUTHORIZED)
        end

        it "when internal server error happens" do
          stub_run_time_error_response = {
            "error": { :message => content_util::RUN_TIME_ERROR },
            "response": response_codes_util::INTERNAL_SERVER_ERROR
          }
          user_service.any_instance.stub(:decide_register_login).and_return(stub_run_time_error_response)
          post :decide_register_login, :user => user_params, :request_type => request_type_sms
          expect(response).to have_http_status(status_codes_util::INTERNAL_SERVER_ERROR)
        end

        it "when too many request" do
          stub_too_many_req_response = {
            "error": { :message => content_util::TOO_MANY_REQUESTS },
            "response": response_codes_util::TOO_MANY_REQUESTS
          }
          user_service.any_instance.stub(:decide_register_login).and_return(stub_too_many_req_response)
          post :decide_register_login, :user => user_params, :request_type => request_type_sms
          expect(response).to have_http_status(status_codes_util::TOO_MANY_REQUESTS)
        end
      end
      
      describe "login with OTP #login_with_otp" do
        it "verifies OTP successfully" do
          stub_ok_response = {
            "payload": {
              "user": {
                "phone_number": Faker::Number.number(10).to_s
              }
            },
            "response": response_codes_util::SUCCESS
          }
          user_service.any_instance.stub(:login).and_return(stub_ok_response)
          post :login, :user => user_params, :request_type => request_type_sms
          expect(response).to have_http_status(status_codes_util::SUCCESS)
          expect(JSON.parse(response.body)["payload"].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it "when required params not present" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes_util::BAD_REQUEST
          }
          user_service.any_instance.stub(:login).and_return(stub_bad_request_response)
          post :login, :user => user_params, :request_type => request_type_sms
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it "when otp doesn't matches" do
          stub_wrong_otp_response = {
            "error": { :message => content_util::WRONG_OTP },
            "response": response_codes_util::BAD_REQUEST
          }
          user_service.any_instance.stub(:login).and_return(stub_wrong_otp_response)
          post :login, :user => user_params,:request_type => request_type_sms
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it "when otp expired" do
          stub_wrong_otp_response = {
            "error": { :message => content_util::AUTHENTICATION_TIMEOUT },
            "response": response_codes_util::AUTHENTICATION_TIMEOUT
          }
          user_service.any_instance.stub(:login).and_return(stub_wrong_otp_response)
          post :login, :user => user_params, :request_type => request_type_sms
          expect(response).to have_http_status(status_codes_util::AUTHENTICATION_TIMEOUT)
        end

        it "when internal server error happens" do
          stub_run_time_error_response = {
            "error": { :message => content_util::RUN_TIME_ERROR },
            "response": response_codes_util::INTERNAL_SERVER_ERROR
          }
          user_service.any_instance.stub(:login).and_return(stub_run_time_error_response)
          post :login, :user => user_params, :request_type => request_type_sms
          expect(response).to have_http_status(status_codes_util::INTERNAL_SERVER_ERROR)
        end

        it "when provider type not found" do
          stub_resource_not_found_response = {
            "error": { :message => content_util::NOT_FOUND },
            "response": response_codes_util::NOT_FOUND
          }
          user_service.any_instance.stub(:login).and_return(stub_resource_not_found_response)
          post :login, :user => user_params, :request_type => request_type_sms
          expect(response).to have_http_status(status_codes_util::NOT_FOUND)
        end

        it "when user isn't found" do
          stub_unauthorized_response = {
            "error": { :message => content_util::UNAUTHENTIC_USER },
            "response": response_codes_util::UN_AUTHORIZED
          }
          user_service.any_instance.stub(:login).and_return(stub_unauthorized_response)
          post :login, :user => user_params, :request_type => request_type_sms
          expect(response).to have_http_status(status_codes_util::UN_AUTHORIZED)
        end
      end

      describe "login with Password #login_with_password" do
        it "verifies password successfully" do
          stub_ok_response = {
            "payload": {
              "user": {
                "phone_number": Faker::Number.number(10).to_s
              }
            },
            "response": response_codes_util::SUCCESS
          }
          user_service.any_instance.stub(:login).and_return(stub_ok_response)
          post :login, :user => user_params, :request_type => request_type_generic
          expect(response).to have_http_status(status_codes_util::SUCCESS)
          expect(JSON.parse(response.body)["payload"].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it "when required params not present" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes_util::BAD_REQUEST
          }
          user_service.any_instance.stub(:login).and_return(stub_bad_request_response)
          post :login, :user => user_params, :request_type => request_type_generic
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it "when password doesn't matches" do
          stub_wrong_otp_response = {
            "error": { :message => content_util::WRONG_OTP },
            "response": response_codes_util::BAD_REQUEST
          }
          user_service.any_instance.stub(:login).and_return(stub_wrong_otp_response)
          post :login, :user => user_params, :request_type => request_type_generic
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it "when internal server error happens" do
          stub_run_time_error_response = {
            "error": { :message => content_util::RUN_TIME_ERROR },
            "response": response_codes_util::INTERNAL_SERVER_ERROR
          }
          user_service.any_instance.stub(:login).and_return(stub_run_time_error_response)
          post :login, :user => user_params, :request_type => request_type_generic
          expect(response).to have_http_status(status_codes_util::INTERNAL_SERVER_ERROR)
        end

        it "when provider type not found" do
          stub_resource_not_found_response = {
            "error": { :message => content_util::NOT_FOUND },
            "response": response_codes_util::NOT_FOUND
          }
          user_service.any_instance.stub(:login).and_return(stub_resource_not_found_response)
          post :login, :user => user_params, :request_type => request_type_generic
          expect(response).to have_http_status(status_codes_util::NOT_FOUND)
        end

        it "when user isn't found" do
          stub_unauthorized_response = {
            "error": { :message => content_util::UNAUTHENTIC_USER },
            "response": response_codes_util::UN_AUTHORIZED
          }
          user_service.any_instance.stub(:login).and_return(stub_unauthorized_response)
          post :login, :user => user_params, :request_type => request_type_generic
          expect(response).to have_http_status(status_codes_util::UN_AUTHORIZED)
        end
      end

      describe "resend otp resquest" do
        it "when no otp returned" do
          stub_run_time_error_response = {
            "error": { :message => content_util::RUN_TIME_ERROR },
            "response": response_codes_util::INTERNAL_SERVER_ERROR
          }
          user_service.any_instance.stub(:resend_otp).and_return(stub_run_time_error_response)
          post :resend_otp, :user => user_params
          expect(response).to have_http_status(status_codes_util::INTERNAL_SERVER_ERROR)
        end
        it "when required params are not in request" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes_util::BAD_REQUEST
          }
          user_service.any_instance.stub(:resend_otp).and_return(stub_bad_request_response)
          post :resend_otp, :user => user_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
        it "when otp returned" do
          stub_ok_response = {
            "payload": {
              "user": {
                "phone_number": Faker::Number.number(10).to_s
              }
            },
            "response": response_codes_util::SUCCESS
          }
          user_service.any_instance.stub(:resend_otp).and_return(stub_ok_response)
          post :resend_otp, :user => user_params
          expect(response).to have_http_status(status_codes_util::SUCCESS)
          expect(JSON.parse(response.body)["payload"].to_json).to eq(stub_ok_response[:payload].to_json)
        end
        it "when user doesn't exists" do
          stub_unauthorized_response = {
            "error": { :message => content_util::UNAUTHENTIC_USER },
            "response": response_codes_util::UN_AUTHORIZED
          }
          user_service.any_instance.stub(:resend_otp).and_return(stub_unauthorized_response)
          post :resend_otp, :user => user_params
          expect(response).to have_http_status(status_codes_util::UN_AUTHORIZED)
        end
        it "when user is doing spamming" do
          stub_spam_response = {
            "error": { :message => content_util::TOO_MANY_REQUESTS },
            "response": response_codes_util::TOO_MANY_REQUESTS
          }
          user_service.any_instance.stub(:resend_otp).and_return(stub_spam_response)
          post :resend_otp, :user => user_params
          expect(response).to have_http_status(status_codes_util::TOO_MANY_REQUESTS)
        end
      end

      describe "activate with referral code request" do
        it "when no status returned" do
          stub_run_time_error_response = {
            "error": { :message => content_util::RUN_TIME_ERROR },
            "response": response_codes_util::INTERNAL_SERVER_ERROR
          }
          user_service.any_instance.stub(:activate_with_referral_code).and_return(stub_run_time_error_response)
          put :activate_with_referral_code, :user => user_params
          expect(response).to have_http_status(status_codes_util::INTERNAL_SERVER_ERROR)
        end
        it "when required params are not in request" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes_util::BAD_REQUEST
          }
          user_service.any_instance.stub(:activate_with_referral_code).and_return(stub_bad_request_response)
          put :activate_with_referral_code, :user => user_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
        it "when status returned" do
          stub_ok_response = {
            "payload": {
              "user": {
                "phone_number": Faker::Number.number(10).to_s
              }
            },
            "response": response_codes_util::SUCCESS
          }
          user_service.any_instance.stub(:activate_with_referral_code).and_return(stub_ok_response)
          put :activate_with_referral_code, :user => user_params
          expect(response).to have_http_status(status_codes_util::SUCCESS)
          expect(JSON.parse(response.body)["payload"].to_json).to eq(stub_ok_response[:payload].to_json)
        end
        it "when user doesn't exists" do
          stub_unauthorized_response = {
            "error": { :message => content_util::UNAUTHENTIC_USER },
            "response": response_codes_util::UN_AUTHORIZED
          }
          user_service.any_instance.stub(:activate_with_referral_code).and_return(stub_unauthorized_response)
          put :activate_with_referral_code, :user => user_params
          expect(response).to have_http_status(status_codes_util::UN_AUTHORIZED)
        end
        it "when referral code is wrong" do
          stub_wrong_referral_response = {
            "error": { :message => content_util::WRONG_REFERRAL_CODE },
            "response": response_codes_util::BAD_REQUEST
          }
          user_service.any_instance.stub(:activate_with_referral_code).and_return(stub_wrong_referral_response)
          put :activate_with_referral_code, :user => user_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
        it "when user is pending" do
          stub_pending_user_response = {
            "error": { :message => content_util::INACTIVE_STATE },
            "response": response_codes_util::PRE_CONDITION_REQUIRED
          }
          user_service.any_instance.stub(:activate_with_referral_code).and_return(stub_pending_user_response)
          put :activate_with_referral_code, :user => user_params
          expect(response).to have_http_status(status_codes_util::PRE_CONDITION_REQUIRED)
        end
      end

      describe "check user status" do
        it "when no status returned" do
          stub_run_time_error_response = {
            "error": { :message => content_util::RUN_TIME_ERROR },
            "response": response_codes_util::INTERNAL_SERVER_ERROR
          }
          user_service.any_instance.stub(:check_user_state).and_return(stub_run_time_error_response)
          get :check_user_state, :user => user_params
          expect(response).to have_http_status(status_codes_util::INTERNAL_SERVER_ERROR)
        end
        it "when required params are not in request" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes_util::BAD_REQUEST
          }
          user_service.any_instance.stub(:check_user_state).and_return(stub_bad_request_response)
          get :check_user_state, :user => user_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
        it "when status returned" do
          stub_ok_response = {
            "payload": {
              "user": {
                "phone_number": Faker::Number.number(10).to_s
              }
            },
            "response": response_codes_util::SUCCESS
          }
          user_service.any_instance.stub(:check_user_state).and_return(stub_ok_response)
          get :check_user_state, :user => user_params
          expect(response).to have_http_status(status_codes_util::SUCCESS)
          expect(JSON.parse(response.body)["payload"].to_json).to eq(stub_ok_response[:payload].to_json)
        end
        it "when user doesn't exists" do
          stub_unauthorized_response = {
            "error": { :message => content_util::UNAUTHENTIC_USER },
            "response": response_codes_util::UN_AUTHORIZED
          }
          user_service.any_instance.stub(:check_user_state).and_return(stub_unauthorized_response)
          get :check_user_state, :user => user_params
          expect(response).to have_http_status(status_codes_util::UN_AUTHORIZED)
        end
      end

      describe "add user profile #add_profile" do
        it "adds user profile successfully" do
          stub_ok_response = {
            "payload": {
              "user": {
                "phone_number": Faker::Number.number(10).to_s
              }
            },
            "response": response_codes_util::SUCCESS
          }
          user_service.any_instance.stub(:update_user_details).and_return(stub_ok_response)
          put :update_user_details, :user => user_params, :session_token => session_token
          expect(response).to have_http_status(status_codes_util::SUCCESS)
          expect(JSON.parse(response.body)["payload"].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it "when required params not present" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes_util::UN_AUTHORIZED
          }
          user_service.any_instance.stub(:update_user_details).and_return(stub_bad_request_response)
          put :update_user_details, :user => user_params, :session_token => session_token
          expect(response).to have_http_status(status_codes_util::UN_AUTHORIZED)
        end
      end

      describe "create user password /create_password" do
        let(:password_hash) {
          {
            password: Faker::Lorem.characters(6)
          }
        }
        it "create user password successfully" do
          stub_ok_response = {
            "payload": {
              "message": "success message"
            },
            "response": response_codes_util::SUCCESS
          }
          create_password_api.any_instance.should_receive(:enact).and_return(stub_ok_response)
          post :create_password, :user => password_hash, :session_token => session_token
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it "when required params not present" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes_util::BAD_REQUEST
          }
          create_password_api.any_instance.should_receive(:enact).and_return(stub_bad_request_response)
          post :create_password, :user => password_hash, :session_token => session_token
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it "when reset process was not started earlier" do
          stub_pre_condition_response = {
            "error": { :message => content_util::CREATE_PASSWORD_UNAUTHENTICATED },
            "response": response_codes_util::PRE_CONDITION_REQUIRED
          }
          create_password_api.any_instance.should_receive(:enact).and_return(stub_pre_condition_response)
          post :create_password, :user => password_hash, :session_token => session_token
          expect(response).to have_http_status(status_codes_util::PRE_CONDITION_REQUIRED)
        end
      end

      describe 'get ample credits of user' do
        it 'with valid user' do
          stub_ok_response = {
              "payload": {
                "message": "success message"
              },
              "response": response_codes_util::SUCCESS
          }
          expect_any_instance_of(get_ample_credit_of_user_api).to receive(:enact).and_return(stub_ok_response)
          get :get_current_ample_credits
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end
      end

    end #End of class
  end
end

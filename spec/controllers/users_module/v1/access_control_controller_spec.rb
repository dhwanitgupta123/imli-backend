module UsersModule
  module V1
    require 'rails_helper'

    RSpec.describe AccessControlController, type: :controller do

      # Anonymous controller inheriting ApplicationController for testing dummy action
      # Note: While calling controller#index, AccessControlController#authorize is being called
      controller(BaseModule::V1::ApplicationController) do
        def index
          render text: 'Accessed successfully'
        end
      end

      let(:access_control_service){UsersModule::V1::AccessControlService }
      let(:user_response_decorator) {UsersModule::V1::UserResponseDecorator}
      let(:request_log_helper){LoggingModule::V1::RequestLogHelper}
      let(:shield_service) { SettingsModule::V1::ShieldService }
      let(:phone_number) {Faker::Number.number(10).to_s}
      let(:log) {FactoryGirl.build(:activity_log)}

      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:dummy_feature_value) { '1' }

      # Test cases for AccessControlController
      context "with invalid request" do
        it "and unauthenticated user" do
          stub_response = {ok: false, data: {response: response_codes_util::UN_AUTHORIZED}}
          access_control_service.any_instance.stub(:has_access?).and_return(stub_response)
          get :index, {:user => {:phone_number => phone_number} }
  
          expect(response).to have_http_status(status_codes_util::UN_AUTHORIZED)
        end

        it "and insufficient data" do
          stub_response = {ok: false, data: {response: response_codes_util::BAD_REQUEST}}
          access_control_service.any_instance.stub(:has_access?).and_return(stub_response)
          get :index, {:user => {:phone_number => phone_number} }

          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end

      context "with valid request" do
        let(:valid_params) { {:user_id => "1", :controller => "users", :action => "index"} }

        it "and authorized user" do
          stub_response = {ok: true}
          request_log_helper.stub(:create_log_params).and_return(log)
          access_control_service.any_instance.stub(:has_access?).and_return(stub_response)
          expect(shield_service).to receive(:get_feature_value).and_return(dummy_feature_value)
          access_control_service.any_instance.stub(:apply_validations).and_return({ok: true})
          get :index, {:user => {:phone_number => phone_number} }

          expect(response).to have_http_status(status_codes_util::SUCCESS)
          expect(response.body).to match(/Accessed successfully/)
        end

        it "and un-authorized user" do
          stub_response = {ok: false, data: {response: response_codes_util::FORBIDDEN}}
          access_control_service.any_instance.stub(:has_access?).and_return(stub_response)
          get :index, {:user => {:phone_number => phone_number} }

          expect(response).to have_http_status(status_codes_util::FORBIDDEN)
        end
      end

      context "with valid request " do
        let(:valid_params) { {:user_id => "1", :controller => "users", :action => "index"} }
        let(:stub_response) {{ok: true}}
        let(:valid_response) {user_response_decorator.create_blocked_user_response(Faker::Number.number(5).to_s)}
        before(:each) do
          expect(shield_service).to receive(:get_feature_value).and_return(dummy_feature_value)
        end
        it "with blocked phone number" do
          request_log_helper.stub(:create_log_params).and_return(log)
          access_control_service.any_instance.stub(:has_access?).and_return(stub_response)
          access_control_service.any_instance.stub(:apply_validations).and_return({ok: true})
          access_control_service.any_instance.stub(:is_user_blocked?).and_return({ok: true, data:valid_response})
          get :index, {:user => {:phone_number => phone_number} }
          expect(response.body).to match(valid_response[:error][:message])
        end

        it "with not blocked phone number" do
          request_log_helper.stub(:create_log_params).and_return(log)
          access_control_service.any_instance.stub(:apply_validations).and_return({ok: true})
          access_control_service.any_instance.stub(:has_access?).and_return(stub_response)
          access_control_service.any_instance.stub(:is_user_blocked?).and_return({ok: false})
          get :index, {:user => {:phone_number => phone_number} }
          expect(response.body).to match(/Accessed successfully/)          
        end
      end

      context 'apply validation' do

        it 'with failed validations' do
          validation_failed_response = user_response_decorator.create_validation_failed_response(Faker::Lorem.word)
          request_log_helper.stub(:create_log_params).and_return(log)
          expect(shield_service).to receive(:get_feature_value).and_return(dummy_feature_value)
          expect_any_instance_of(access_control_service).to receive(:has_access?).and_return({ok: true})
          expect_any_instance_of(access_control_service).to receive(:apply_validations).and_return({ok: false, data: validation_failed_response})
          get :index, {:user => {:phone_number => phone_number} }
          expect(response.body).to match(validation_failed_response[:error][:message])
        end

        it 'with valid validations' do
          request_log_helper.stub(:create_log_params).and_return(log)
          expect(shield_service).to receive(:get_feature_value).and_return(dummy_feature_value)
          expect_any_instance_of(access_control_service).to receive(:has_access?).and_return({ok: true})
          expect_any_instance_of(access_control_service).to receive(:is_user_blocked?).and_return({ok: false})
          expect_any_instance_of(access_control_service).to receive(:apply_validations).and_return({ok: true})
          get :index, {:user => {:phone_number => phone_number} }
          expect(response.body).to match(/Accessed successfully/)
        end

        it 'with false setting' do
          request_log_helper.stub(:create_log_params).and_return(log)
          expect(shield_service).to receive(:get_feature_value).and_return(nil)
          expect_any_instance_of(access_control_service).to receive(:has_access?).and_return({ok: true})
          expect_any_instance_of(access_control_service).to receive(:is_user_blocked?).and_return({ok: false})
          get :index, {:user => {:phone_number => phone_number} }
          expect(response.body).to match(/Accessed successfully/)
        end
      end
    end
  end
end
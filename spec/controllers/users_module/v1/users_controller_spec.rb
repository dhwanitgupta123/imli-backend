module UsersModule
  module V1
    require 'rails_helper'
    require 'json'

    RSpec.describe UsersModule::V1::UsersController, type: :controller do

      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:user_service) { UsersModule::V1::UserService }
      let(:user_response_decorator) { UsersModule::V1::UserResponseDecorator }
      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) {access_control_controller.any_instance.stub(:after_actions).and_return(true)}
      let(:access_control_controller){UsersModule::V1::AccessControlController}
      let(:session_token) {Faker::Lorem.characters(64)}
      let(:user) { FactoryGirl.build_stubbed(:user) }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add role to user' do
        let(:user_params) {
          {
            phone_number: Faker::Number.number(10)
          }
        }
        let(:role_params) {
          {
            role_id: Faker::Number.number(1)
          }
        }

        it 'with all valid parameters' do
          stub_ok_response = user_response_decorator.create_role_added_response
          user_service.any_instance.stub(:add_role_to_user).and_return(stub_ok_response)
          post :add_role_to_user, user: user_params, role: role_params
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with insufficient arguments' do
          stub_insufficient_response = user_response_decorator.create_response_bad_request
          user_service.any_instance.stub(:add_role_to_user).and_return(stub_insufficient_response)
          post :add_role_to_user, user: user_params, role: role_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'with invalid arguments' do
          stub_invalid_response = user_response_decorator.create_response_invalid_data_passed
          user_service.any_instance.stub(:add_role_to_user).and_return(stub_invalid_response)
          post :add_role_to_user, user: user_params, role: role_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end

      describe 'remove role from user' do
        let(:user_params) {
          {
            user_id: Faker::Number.number(1)
          }
        }
        let(:role_params) {
          {
            role_id: Faker::Number.number(1)
          }
        }

        it 'with all valid parameters' do
          stub_ok_response = user_response_decorator.create_role_removed_response
          user_service.any_instance.stub(:remove_role_from_user).and_return(stub_ok_response)
          post :remove_role_from_user, user: user_params, role: role_params
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with insufficient arguments' do
          stub_insufficient_response = user_response_decorator.create_response_bad_request
          user_service.any_instance.stub(:remove_role_from_user).and_return(stub_insufficient_response)
          post :remove_role_from_user, user: user_params, role: role_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'with invalid arguments' do
          stub_invalid_response = user_response_decorator.create_response_invalid_data_passed
          user_service.any_instance.stub(:remove_role_from_user).and_return(stub_invalid_response)
          post :remove_role_from_user, user: user_params, role: role_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end

      describe 'get all users' do
        it 'when users are present in DB' do
          stub_ok_response = user_response_decorator.create_all_users_paginate_response({users: [ user ]})
          user_service.any_instance.stub(:get_all_users).and_return(stub_ok_response)
          get :get_all_users
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'when no users present in DB' do
          stub_ok_response = user_response_decorator.create_all_users_paginate_response({users: []})
          user_service.any_instance.stub(:get_all_users).and_return(stub_ok_response)
          get :get_all_users
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end
      end

      describe 'change state of user' do
        let(:user_id) {
          Faker::Number.number(2).to_s
        }
        let(:user_params) {
          {
            event: Faker::Number.number(1).to_s
          }
        }

        it 'with all valid parameters' do
          stub_ok_response = user_response_decorator.create_ok_response(user)
          user_service.any_instance.stub(:change_state).and_return(stub_ok_response)
          put :change_state, id: user_id, user: user_params
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with insufficient arguments' do
          stub_bad_response = user_response_decorator.create_response_bad_request
          user_service.any_instance.stub(:change_state).and_return(stub_bad_response)
          put :change_state, id: user_id, user: user_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'with invalid arguments' do
          stub_invalid_data_response = user_response_decorator.create_response_invalid_data_passed
          user_service.any_instance.stub(:change_state).and_return(stub_invalid_data_response)
          put :change_state, id: user_id, user: user_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'when no user exist with respect to passed id' do
          stub_not_found_response = user_response_decorator.create_not_found_error
          user_service.any_instance.stub(:change_state).and_return(stub_not_found_response)
          put :change_state, id: user_id, user: user_params
          expect(response).to have_http_status(status_codes_util::NOT_FOUND)
        end

        it 'when event passed is not valid for current user state' do
          stub_not_found_response = user_response_decorator.create_response_pre_condition_required
          user_service.any_instance.stub(:change_state).and_return(stub_not_found_response)
          put :change_state, id: user_id, user: user_params
          expect(response).to have_http_status(status_codes_util::PRE_CONDITION_REQUIRED)
        end
      end

      describe 'refill_referral_limit' do
        it 'with all valid parameters' do
          stub_ok_response = user_response_decorator.create_success_response(content_util::REFERRAL_LIMIT_INCREASED)
          user_service.any_instance.stub(:refill_referral_limit).and_return(stub_ok_response)
          put :refill_referral_limit, id: Faker::Number.number(1), referral_limit: Faker::Number.number(1)
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with wrong params' do
          stub_bad_response = user_response_decorator.create_response_bad_request
          user_service.any_instance.stub(:refill_referral_limit).and_return(stub_bad_response)
          put :refill_referral_limit, id: Faker::Number.number(1), referral_limit: Faker::Number.number(1)
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end

    end # End of Rspec Class
  end
end
module UsersModule
  module V1
    require 'rails_helper'
    require 'json'

    RSpec.describe UsersModule::V1::MembershipsApiController, type: :controller do

      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:membership_service) { UsersModule::V1::MembershipService }
      let(:user_response_decorator) { UsersModule::V1::UserResponseDecorator }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) {access_control_controller.any_instance.stub(:after_actions).and_return(true)}
      let(:access_control_controller){UsersModule::V1::AccessControlController}
      let(:benefit) { FactoryGirl.build_stubbed(:benefit) }
      let(:plan) { FactoryGirl.build_stubbed(:plan, benefits: [benefit]) }
      let(:user) { FactoryGirl.build_stubbed(:user) }
      let(:membership) { FactoryGirl.build_stubbed(:membership, plan: plan) }
      before(:each) do
        before_actions
        after_actions
      end

      describe 'subscribe_to_membership' do
        let(:user) {
          {
            memberships: [
              {
                plan_id: Faker::Number.number(1)
              }
            ]          
          }
        }

        it 'with all valid parameters' do
          stub_ok_response = {
            user: {
              memberships: [
                {
                  id: 1,
                  status: 1,
                  plan: {
                    name: 'test',
                    benefits: [
                      {
                        name: 'test'
                      }
                    ]
                  }
                }
              ]
            }
          }
          membership_service.any_instance.should_receive(:subscribe_to_membership).and_return(stub_ok_response)
          post :subscribe_to_membership, user: user
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with insufficient arguments' do
          stub_insufficient_response = user_response_decorator.create_response_bad_request
          membership_service.any_instance.should_receive(:subscribe_to_membership).and_return(stub_insufficient_response)
          post :subscribe_to_membership, user: user
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'with invalid arguments' do
          stub_invalid_response = user_response_decorator.create_response_invalid_data_passed
          membership_service.any_instance.should_receive(:subscribe_to_membership).and_return(stub_invalid_response)
          post :subscribe_to_membership, user: user
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end

      describe 'payment_initiate' do
        let(:user) {
          {
            memberships: [
              {
                payment: {
                  mode: 1
                }
              }
            ]       
          }
        }

        it 'with all valid parameters' do
          stub_ok_response = {
            user: {
              memberships: [
                {
                  id: 1,
                  status: 1,
                  plan: {
                    name: 'test',
                    benefits: [
                      {
                        name: 'test'
                      }
                    ]
                  }
                }
              ]
            }
          }
          membership_service.any_instance.should_receive(:payment_initiate).and_return(stub_ok_response)
          post :payment_initiate, user: user
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with insufficient arguments' do
          stub_insufficient_response = user_response_decorator.create_response_bad_request
          membership_service.any_instance.should_receive(:payment_initiate).and_return(stub_insufficient_response)
          post :payment_initiate, user: user
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'with invalid arguments' do
          stub_invalid_response = user_response_decorator.create_response_invalid_data_passed
          membership_service.any_instance.should_receive(:payment_initiate).and_return(stub_invalid_response)
          post :payment_initiate, user: user
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end

      describe 'payment_success' do
        let(:user) {
          {
            memberships: [
              {
                payment: {
                  id: 1
                }
              }
            ]       
          }
        }

        it 'with all valid parameters' do
          stub_ok_response = {
            user: {
              memberships: [
                {
                  id: 1,
                  status: 1,
                  plan: {
                    name: 'test',
                    benefits: [
                      {
                        name: 'test'
                      }
                    ]
                  }
                }
              ]
            }
          }
          membership_service.any_instance.should_receive(:payment_success).and_return(stub_ok_response)
          post :payment_success, user: user
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with insufficient arguments' do
          stub_insufficient_response = user_response_decorator.create_response_bad_request
          membership_service.any_instance.should_receive(:payment_success).and_return(stub_insufficient_response)
          post :payment_success, user: user
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'with invalid arguments' do
          stub_invalid_response = user_response_decorator.create_response_invalid_data_passed
          membership_service.any_instance.should_receive(:payment_success).and_return(stub_invalid_response)
          post :payment_success, user: user
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end
    end # End of Rspec Class
  end
end

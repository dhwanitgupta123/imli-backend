module CommonModule
  # Version 1 module of CommonModule
  module V1
    require 'rails_helper'

    RSpec.describe CommonModule::V1::ConstantsController, type: :controller do
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:constants_service) { CommonModule::V1::ConstantsService }
      let(:constants_response_decorator) { CommonModule::V1::ConstantsResponseDecorator }

      # This should return the minimal set of values that should be in the session
      # in order to pass any filters (e.g. authentication) defined in
      # UsersController. Be sure to keep this updated too.
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      before(:each) do
        expect_any_instance_of(access_control_controller).to receive(:before_actions).and_return(true)
        expect_any_instance_of(access_control_controller).to receive(:after_actions).and_return(true)
      end

      let(:cluster_constants) {
        {
          level_constants: {
            level: 1,
            display_level: 'Department'
          }
        }
      }

      describe 'get cluster constants' do

        it 'with all valid parameters' do
          stub_ok_response = constants_response_decorator.create_cluster_constant_response(cluster_constants)
          expect_any_instance_of(constants_service).to receive(:get_cluster_constants)
                .and_return(stub_ok_response)
          get :get_cluster_constants
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end
      end
    end
  end
end

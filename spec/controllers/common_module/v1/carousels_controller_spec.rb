module CommonModule
  # Version 1 module of CommonModule
  module V1
    require 'rails_helper'

    RSpec.describe CommonModule::V1::CarouselsController, type: :controller do
      let(:version) { 1 }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:carousel_service) { CommonModule::V1::CarouselService }
      let(:carousel_response_decorator) { CommonModule::V1::CarouselResponseDecorator }

      # This should return the minimal set of values that should be in the session
      # in order to pass any filters (e.g. authentication) defined in
      # UsersController. Be sure to keep this updated too.
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      before(:each) do
        expect_any_instance_of(access_control_controller).to receive(:before_actions).and_return(true)
        expect_any_instance_of(access_control_controller).to receive(:after_actions).and_return(true)
      end

      let(:label) { Faker::Name::name }
      let(:description) { Faker::Name::name }
      let(:priority) { Faker::Number.number(1) }

      # Fetching type here as this is to be used from the available types ones present in CarouselTypes class
      let(:carousel_service_instance) { CommonModule::V1::CarouselService.new(version) }
      let(:valid_carousel_types) { carousel_service_instance.get_valid_carousel_types }
      let(:type) { valid_carousel_types[0] }
      
      let(:image) {
        {
          image_id: Faker::Number.number(1),
          priority: priority,
          action: Faker::Internet.url
        }
      }

      let(:images_array) {[image]}

      let(:carousel) {
        {
          type: type,
          label: label,
          images: images_array
        }
      }

      describe 'get carousel' do

        it 'with all valid parameters' do
          stub_ok_response = carousel_response_decorator.create_carousel_ok_response(carousel)
          expect_any_instance_of(carousel_service).to receive(:get_carousel)
                .and_return(stub_ok_response)
          get :get_carousel, type: type
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with insufficient arguments' do
          stub_bad_response = carousel_response_decorator.create_response_bad_request
          expect_any_instance_of(carousel_service).to receive(:get_carousel)
                .and_return(stub_bad_response)
          get :get_carousel, type: type
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'with invalid arguments' do
          stub_bad_response = carousel_response_decorator.create_response_invalid_data_passed
          expect_any_instance_of(carousel_service).to receive(:get_carousel)
                .and_return(stub_bad_response)
          get :get_carousel, type: type
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'when no carousel exist with passed type' do
          stub_not_found_response = carousel_response_decorator.create_not_found_error
          expect_any_instance_of(carousel_service).to receive(:get_carousel)
                .and_return(stub_not_found_response)
          get :get_carousel, type: type
          expect(response).to have_http_status(status_codes_util::NOT_FOUND)
        end
      end

      describe 'update carousel' do

        it 'with all valid parameters' do
          stub_ok_response = carousel_response_decorator.create_carousel_ok_response(carousel)
          expect_any_instance_of(carousel_service).to receive(:update_carousel)
                .and_return(stub_ok_response)
          post :update_carousel, carousel: carousel , type: type
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with insufficient arguments' do
          stub_bad_response = carousel_response_decorator.create_response_bad_request
          expect_any_instance_of(carousel_service).to receive(:update_carousel)
                .and_return(stub_bad_response)
          post :update_carousel, carousel: carousel, type: type
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'with invalid arguments' do
          stub_bad_response = carousel_response_decorator.create_response_invalid_data_passed
          expect_any_instance_of(carousel_service).to receive(:update_carousel)
                  .and_return(stub_bad_response)
          post :update_carousel, carousel: carousel, type: type
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'when no carousel exist with passed type' do
          stub_not_found_response = carousel_response_decorator.create_not_found_error
          expect_any_instance_of(carousel_service).to receive(:update_carousel)
                  .and_return(stub_not_found_response)
          post :update_carousel, carousel: carousel, type: type
          expect(response).to have_http_status(status_codes_util::NOT_FOUND)
        end
      end
    # End of Rspec class
    end
  end
end
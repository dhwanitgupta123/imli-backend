module SettingsModule
  module V1
    require 'rails_helper'

    RSpec.describe SettingsModule::V1::SettingsController, type: :controller do

      # This should return the minimal set of values that should be in the session
      # in order to pass any filters (e.g. authentication) defined in
      # UsersController. Be sure to keep this updated too.
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:stub_before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:stub_after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      before(:each) do
        stub_before_actions
        stub_after_actions
      end

      let(:get_static_settings_api) { SettingsModule::V1::GetStaticSettingsApi }
      let(:get_dynamic_settings_api) { SettingsModule::V1::GetDynamicSettingsApi }

      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }

      let(:stub_ok_response) {
          {
            payload: {
              settings: {}
            },
            response: response_codes_util::SUCCESS
          }
        }

      describe 'get static settings' do
        let(:stub_ok_response) {
          {
            payload: {
              settings: {}
            },
            response: response_codes_util::SUCCESS
          }
        }
        it 'returns static settings successfully' do
          get_static_settings_api.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_static_settings
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end
      end

      describe 'get dynamic settings' do
        let(:stub_ok_response) {
          {
            payload: {
              dynamic_settings: {}
            },
            response: response_codes_util::SUCCESS
          }
        }
        it 'returns dynamic settings successfully' do
          get_dynamic_settings_api.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_dynamic_settings
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end
      end

    end # End of Rspec
  end
end

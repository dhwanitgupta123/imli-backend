module CategorizationModule
  module V1
    require 'rails_helper'

    RSpec.describe MpspCategorizationApiController, type: :controller do

      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:stub_before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:stub_after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:mpsp_categorization_api_service) { CategorizationModule::V1::MpspCategorizationApiService }

      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:content_util) { CommonModule::V1::Content }

      let(:department) { FactoryGirl.create(:mpsp_department) }
      let(:category) {{ categories:[ FactoryGirl.create(:mpsp_category) ] }}


      before(:each) do
        stub_before_actions
        stub_after_actions
      end

      describe "get_all_departments_details" do
        it "returns all departments successfully" do
          stub_ok_response = {
            "payload": {
              "departments": [
                {
                  id: department.id,
                  label: department.label,
                  status: department.status,
                  description: department.description,
                  categories: category
                }
              ] 
            },
            "response": response_codes::SUCCESS
          }   
          mpsp_categorization_api_service.any_instance.stub(:get_all_mpsp_departments_details).and_return(stub_ok_response)
          get :get_all_mpsp_departments_details
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)["payload"].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'with wrong request' do
          stub_bad_request_response = {
            'error': { message: content_util::INVALID_DATA_PASSED },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_categorization_api_service.any_instance.stub(:get_all_mpsp_departments_details).and_return(stub_bad_request_response)
          get :get_all_mpsp_departments_details
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end
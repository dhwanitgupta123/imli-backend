#
# Module to handle all the functionalities related to description
#
module UsersModule
  #
  # Version1 for description module
  #
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::GetAmpleCreditOfUserApi do
    
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:user_service) { UsersModule::V1::UserService }
      let(:ample_credit) { FactoryGirl.build(:ample_credit) }
      let(:get_ample_credit_of_user_api) { UsersModule::V1::GetAmpleCreditOfUserApi.new(version) }

      context 'enact ' do

        it 'with valid args' do
          expect_any_instance_of(user_service).to receive(:get_current_ample_credit).
            and_return(ample_credit)
          data = get_ample_credit_of_user_api.enact
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end

      end
    end
  end
end

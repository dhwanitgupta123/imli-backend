#
# Module to handle all the functionalities related to description
#
module UsersModule
  #
  # Version1 for description module
  #
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::ApplyReferralBenefitsApi do
    
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:user_service) { UsersModule::V1::UserService }
      let(:apply_referral_benefit_api) { UsersModule::V1::ApplyReferralBenefitsApi.new(version) }
      let(:user) { FactoryGirl.build(:user) }

      let(:request) {
        {
          referral_code: Faker::Lorem.word
        }
      }
      context 'enact ' do
        it 'with valid args' do
          expect_any_instance_of(user_service).to receive(:transactional_apply_refferal_benefits).
            and_return(user)
          data = apply_referral_benefit_api.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end

        it 'with invalid args' do
          data = apply_referral_benefit_api.enact({}) 
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong referral code' do
          expect_any_instance_of(user_service).to receive(:transactional_apply_refferal_benefits).
            and_raise(custom_errors_util::WrongReferralCodeError.new)
          data = apply_referral_benefit_api.enact(request) 
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end
    end
  end
end

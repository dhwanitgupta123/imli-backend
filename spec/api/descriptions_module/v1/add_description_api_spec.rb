#
# Module to handle all the functionalities related to description
#
module DescriptionsModule
  #
  # Version1 for description module
  #
  module V1
        require 'rails_helper'
    RSpec.describe DescriptionsModule::V1::AddDescriptionApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:description_service) { DescriptionsModule::V1::DescriptionService }
      let(:description) { FactoryGirl.build(:description) }
      let(:add_description_api) { DescriptionsModule::V1::AddDescriptionApi.new(version) }

      let(:description_request) {
        {
          heading: Faker::Company.name,
          resource:  Faker::Lorem.characters(6),
          resource_id: Faker::Number.number(1),
          data: [
             Faker::Lorem.characters(6)
          ]
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_description_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil resource' do
          data = add_description_api.enact(description_request.merge(resource: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(description_service).to receive(:transactional_create_descriptions).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_description_api.enact(description_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(description_service).to receive(:transactional_create_descriptions).
            and_return(description)
          data = add_description_api.enact(description_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to settings
#
module SettingsModule
  #
  # Version1 for description module
  #
  module V1
        require 'rails_helper'
    RSpec.describe SettingsModule::V1::GetDynamicSettingsApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:settings_service) { SettingsModule::V1::SettingService }
      let(:settings_mapper) { SettingsModule::V1::SettingsMapper }

      let(:get_dynamic_settings_api) { SettingsModule::V1::GetDynamicSettingsApi.new(version) }

      let(:settings) {
        {
          'DUMMY_SETTING_111': Faker::Number.between(0,1),
          'DUMMY_SETTING_222': Faker::Number.between(0,1),
        }
      }

      context 'enact ' do

        it 'with valid settings present' do 
          expect_any_instance_of(settings_service).to receive(:get_all_dynamic_settings).
            and_return(settings)
          expect(settings_mapper).to receive(:get_dynamic_settings_response_hash).
            and_return(settings)
          data = get_dynamic_settings_api.enact
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end

        it 'with empty settings present' do 
          expect_any_instance_of(settings_service).to receive(:get_all_dynamic_settings).
            and_return({})
          expect(settings_mapper).to receive(:get_dynamic_settings_response_hash).
            and_return({})
          data = get_dynamic_settings_api.enact
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end

      end
    end
  end
end

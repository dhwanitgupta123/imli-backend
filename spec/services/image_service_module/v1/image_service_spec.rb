#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    require 'rails_helper'
    RSpec.describe ImageServiceModule::V1::ImageService do

      let(:image_service) { ImageServiceModule::V1::ImageService.new }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:common_image_worker) { ImageServiceModule::V1::CommonImageWorker }
      let(:image_service_helper) { ImageServiceModule::V1::ImageServiceHelper }
      let(:image_model) { ImageServiceModule::V1::Image }
      let(:image) { FactoryGirl.build_stubbed(:image) }
      let(:image_events) { ImageServiceModule::V1::ModelStates::ImageEvents }
      let(:image_states) { ImageServiceModule::V1::ModelStates::ImageStates }
      let(:valid_image_url) { File.join(Rails.root, 'spec/factories/image_service_module/v1/dummy_image.jpg')}
      let(:file) { File }
      let(:max_priority) { 2**32 }
      let(:valid_args) {
        {
          '0' => image.image_local
        }
      }
      let(:invalid_args) {
        {
          '1' => Faker::Number.number(100)
        }
      }

      context 'upload image locally' do
        it 'with blank request' do
          data = image_service.upload_image({})
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(image_model).to receive(:save!).and_return(true)
          expect(image_service_helper).to receive(:get_cdn_url).and_return(valid_image_url)
          data = image_service.upload_image(valid_args)
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'with invalid image' do
          data = image_service.upload_image(invalid_args)

          expect(data[:response]).to eq(response_codes_util::INTERNAL_SERVER_ERROR)
        end
      end

      context 'upload image async' do
        it 'calls common image worker' do
          expect(common_image_worker).to receive(:perform_async).and_return(true)
          data = image_service.upload_async(valid_args)
          expect(data).to eq(true)
        end
      end

      context 'sync_image_with_remote_storage' do
        it 'with blank request' do
          expect{image_service.sync_image_with_remote_storage([])}.
            to raise_error(custom_errors_util::InsufficientDataError)
        end

        it 'with valid args' do
          expect(image_service_helper).to receive(:update_cached_urls).and_return(true)
          expect_any_instance_of(image_model).to receive(:save!).twice.and_return(true)
          expect(image_model).to receive(:find_by).and_return(image)
          expect(file).to receive(:open).at_least(:once).and_return(File.open(valid_image_url))
          expect(file).to receive(:file?).and_return(true)
          expect(file).to receive(:delete).and_return(true)
          data = image_service.sync_image_with_remote_storage([image.id])
          expect(data).to eq(true)
        end

        it 'with invalid image url' do
          expect(image_model).to receive(:find_by).and_return(image)
          expect{image_service.sync_image_with_remote_storage([image.id])}.
            to raise_error(custom_errors_util::RunTimeError)
        end
      end

      context 'get priority by image name' do
        it 'return valid priority' do
          folder_name = Faker::Number.number(3).to_s
          image_postfix = Faker::Number.number(1).to_s
          image_path = folder_name + '/' + folder_name + '_' + image_postfix + '.jpg'
          priority = image_service.get_priority_from_file_path(image_path)
          expect(priority).to eq(image_postfix)
        end

        it 'return invalid prioriy with invalid url' do
          url = Faker::Internet.url
          priority = image_service.get_priority_from_file_path(url)
          expect(priority).to eq(max_priority)
        end
      end

      context 'change image state' do
        it 'from inactive to active and active to inactive' do
          image = FactoryGirl.create(:image)
          image = image_service.change_image_state({ id: image.id, event: image_events::DEACTIVATE })
          expect(image.status).to eq(image_states::INACTIVE)
          image = image_service.change_image_state({ id: image.id, event: image_events::ACTIVATE })
          expect(image.status).to eq(image_states::ACTIVE)
        end

        it 'from active to soft_delete' do
          image = FactoryGirl.create(:image)
          image = image_service.change_image_state({ id: image.id, event:image_events::SOFT_DELETE })
          expect(image.status).to eq(image_states::DELETED)
        end

        it 'from inactive to soft_delete' do
          image = FactoryGirl.create(:image)
          image = image_service.change_image_state({ id: image.id, event: image_events::DEACTIVATE })
          image = image_service.change_image_state({ id: image.id, event: image_events::SOFT_DELETE })
          expect(image.status).to eq(image_states::DELETED)
        end

        it 'raise error change state from S1 to S1' do
          image = FactoryGirl.create(:image)
          expect{ image_service.change_image_state({ id: image.id, event: image_events::ACTIVATE }) }.
          to raise_error(custom_errors_util::PreConditionRequiredError)
        end

        it 'raise error if image not present' do
          expect{ image_service.change_image_state({ id: Faker::Number.number(1), event: image_events::ACTIVATE }) }.
            to raise_error(custom_errors_util::ResourceNotFoundError)
        end

        it 'raise error if with invalid event' do
          image = FactoryGirl.create(:image)
          expect{ image_service.change_image_state({ id: image.id, event: -1*image_events::ACTIVATE }) }.
            to raise_error(custom_errors_util::InvalidArgumentsError)
        end
      end
    end
  end
end

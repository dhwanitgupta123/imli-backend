#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # This is interface class each data aggregation service should implement this interface
    #
    require 'rails_helper'

    #
    # This is rspec for the IndexInterface 
    # These rspecs ensure if any class extends this interface
    # then it should implement the function exposed by the
    # interface
    # 
    RSpec.describe DataAggregationModule::V1::IndexInterface do
      before :all do
        #
        # Loading classes of config.autoload_paths
        # 
        Rails.application.eager_load!
        #
        # Get all the classes which include this interface
        # 
        @implementations = ObjectSpace.each_object(Class).select { |klass| klass < DataAggregationModule::V1::IndexInterface }
      end

      #
      # This enforce that all the classes which include the interface
      # implement update_index method
      # 
      it 'should implement update_index method' do
        @implementations.each do |klass|
          klass_methods = klass.instance_methods(false)
          expect(klass_methods).to include(:update_index)
        end
      end

      #
      # This enforce that all the classes which include the interface
      # implement re_initialize_index method
      # 
      it 'should implement re_initialize_index method' do
        @implementations.each do |klass|
          klass_methods = klass.instance_methods(false)
          expect(klass_methods).to include(:re_initialize_index)
        end
      end
    end
  end
end

#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # This class is responsible to aggregate data to index that are used by kibana
    #
    require 'rails_helper'
    RSpec.describe DataAggregationModule::V1::KibanaDataAggregationService do

      let(:kibana_data_aggregation_service) { DataAggregationModule::V1::KibanaDataAggregationService }
      let(:product_aggregator_worker) { DataAggregationModule::V1::ProductAggregatorWorker }
      let(:order_aggregator_worker) { DataAggregationModule::V1::OrderAggregatorWorker }
      let(:user_aggregator_worker) { DataAggregationModule::V1::UserAggregatorWorker }
      let(:payment_aggregator_worker) { DataAggregationModule::V1::PaymentAggregatorWorker }
      let(:master_producy_aggregator_worker) { DataAggregationModule::V1::MasterProductAggregatorWorker }
      let(:api_log_aggregator_worker) { DataAggregationModule::V1::ApiLogAggregatorWorker }

      it 'aggregate function should call workers' do
        expect(order_aggregator_worker).to receive(:perform_in).and_return(true)
        expect(product_aggregator_worker).to receive(:perform_in).and_return(true)
        expect(user_aggregator_worker).to receive(:perform_in).and_return(true)
        expect(payment_aggregator_worker).to receive(:perform_in).and_return(true)
        expect(master_producy_aggregator_worker).to receive(:perform_in).and_return(true)
        expect(api_log_aggregator_worker).to receive(:perform_in).and_return(true)

        kibana_data_aggregation_service.aggregate
      end
    end
  end
end
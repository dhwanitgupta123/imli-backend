module CommonModule
  # Module V1 of UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe CommonModule::V1::PlanService do
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:content_util) { CommonModule::V1::Content }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }

      let(:plan_service) { CommonModule::V1::PlanService.new({}) }
      let(:plan_response_decorator) { CommonModule::V1::PlanResponseDecorator }
      let(:plan_model_class) { CommonModule::V1::Plan }
      let(:benefit_service_class) { CommonModule::V1::BenefitService }

      let(:plan) { FactoryGirl.build(:plan) }
      let(:benefit) { FactoryGirl.build(:benefit) }

      describe 'create plan' do
        let(:plan_params) {
          {
            name: Faker::Lorem.word,
            details: Faker::Lorem.sentence,
            duration: Faker::Number.number(2).to_s,
            validity_label: Faker::Lorem.word,
            fee: Faker::Number.number(2).to_s,
            benefits: [ Faker::Number.number(1) ],
            plan_key: Faker::Number.number(1)
          }
        }

        it 'with nil arguments' do
          data = plan_service.create_plan(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with insufficient parameters' do
          plan_params[:name] = nil
          data = plan_service.create_plan({plan: plan_params})
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with no benefit corresponding to passed benefit id' do
          benefit_service_class.any_instance.should_receive(:get_benefits_from_id_array).with(plan_params[:benefits]) {
            raise custom_error_util::ResourceNotFoundError.new
          }
          data = plan_service.create_plan({plan: plan_params})
          expect(data[:response]).to eq(response_codes_util::NOT_FOUND)
        end


        it 'with benefit present but not active corresponding to passed benefit id' do
          benefit_service_class.any_instance.should_receive(:get_benefits_from_id_array).with(plan_params[:benefits]) {
            raise custom_error_util::InvalidArgumentsError.new
          }
          data = plan_service.create_plan({plan: plan_params})
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        context 'with valid benefits' do
          before(:example) {
            benefit_service_class.any_instance.should_receive(:get_benefits_from_id_array).with(plan_params[:benefits]).and_return([benefit])
          }

          it 'and invalid parameters and failed to save plan' do
            plan_model_class.any_instance.should_receive(:save_plan) {
              raise custom_error_util::InvalidArgumentsError.new
            }
            data = plan_service.create_plan({plan: plan_params})
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it 'and all valid parameters' do
            plan_model_class.any_instance.should_receive(:save_plan).and_return(true)
            data = plan_service.create_plan({plan: plan_params})
            expect(data[:response]).to eq(response_codes_util::SUCCESS)
          end
        end
      end

      describe 'get all plans' do

        it 'when DB has no plans or all deleted plans' do
          plan_model_class.should_receive(:get_all_non_deleted_plans).and_return([])
          data = plan_service.get_all_plans
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'when DB has non-deleted plans' do
          plan_model_class.should_receive(:get_all_non_deleted_plans).and_return([plan])
          data = plan_service.get_all_plans
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end
      end

      describe 'get all active plans' do

        it 'when DB has no plans or non active plans' do
          expect(plan_model_class).to receive(:get_all_active_plans).and_return([])
          data = plan_service.get_all_active_plans
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'when DB has active plans' do
          expect(plan_model_class).to receive(:get_all_active_plans).and_return([plan])
          data = plan_service.get_all_active_plans
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end
      end

      describe 'get plan' do
        let(:plan_id) {
          Faker::Number.number(2).to_s
        }

        it 'with nil arguments' do
          data = plan_service.get_plan(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with insufficient parameters' do
          data = plan_service.get_plan({id: nil})
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with no plan corresponding to passed plan id' do
          plan_model_class.should_receive(:find_by_plan_id).with(plan_id) {
            raise custom_error_util::ResourceNotFoundError.new
          }
          data = plan_service.get_plan({id: plan_id})
          expect(data[:response]).to eq(response_codes_util::NOT_FOUND)
        end

        it 'with valid plan id' do
          plan_model_class.should_receive(:find_by_plan_id).with(plan_id).and_return(plan)
          data = plan_service.get_plan({id: plan_id})
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end
      end

      describe 'update plan' do
        let(:plan_id) {
          Faker::Number.number(2).to_s
        }
        let(:plan_params) {
          {
            name: Faker::Lorem.word,
            details: Faker::Lorem.sentence,
            duration: Faker::Number.number(2).to_s,
            validity_label: Faker::Lorem.word,
            fee: Faker::Number.number(2).to_s,
            benefits: [ Faker::Number.number(1) ]
          }
        }

        it 'with nil arguments' do
          data = plan_service.update_plan(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with insufficient parameters' do
          data = plan_service.update_plan({id: nil, plan: plan_params})
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with no plan corresponding to passed plan id' do
          plan_model_class.should_receive(:find_by_plan_id).with(plan_id) {
            raise custom_error_util::ResourceNotFoundError.new
          }
          data = plan_service.update_plan({id: plan_id, plan: plan_params})
          expect(data[:response]).to eq(response_codes_util::NOT_FOUND)
        end

        context 'with valid plan id' do
          before(:example) {
            plan_model_class.should_receive(:find_by_plan_id).with(plan_id).and_return(plan)
          }
          it 'with no benefit corresponding to passed benefit id' do
            benefit_service_class.any_instance.should_receive(:get_benefits_from_id_array).with(plan_params[:benefits]) {
              raise custom_error_util::ResourceNotFoundError.new
            }
            data = plan_service.update_plan({id: plan_id, plan: plan_params})
            expect(data[:response]).to eq(response_codes_util::NOT_FOUND)
          end


          it 'with benefit present but not active corresponding to passed benefit id' do
            benefit_service_class.any_instance.should_receive(:get_benefits_from_id_array).with(plan_params[:benefits]) {
              raise custom_error_util::InvalidArgumentsError.new
            }
            data = plan_service.update_plan({id: plan_id, plan: plan_params})
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          context 'with valid benefits' do
            before(:example) {
              benefit_service_class.any_instance.should_receive(:get_benefits_from_id_array).with(plan_params[:benefits]).and_return([benefit])
            }

            it 'and invalid parameters and failed to save plan' do
              plan.should_receive(:update_plan) {
                raise custom_error_util::InvalidArgumentsError.new
              }
              data = plan_service.update_plan({id: plan_id, plan: plan_params})
              expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
            end

            it 'and all valid parameters' do
              plan.should_receive(:update_plan).and_return(true)
              data = plan_service.update_plan({id: plan_id, plan: plan_params})
              expect(data[:response]).to eq(response_codes_util::SUCCESS)
            end
          end
        end
      end

      describe 'change state of plan' do
        let(:plan_id) {
          Faker::Number.number(2).to_s
        }
        let(:plan_params) {
          {
            event: Faker::Number.number(1)
          }
        }

        it 'with nil arguments' do
          data = plan_service.change_state(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with insufficient parameters' do
          data = plan_service.change_state({id: nil, plan: plan_params})
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with no event passed' do
          plan_params[:event] = nil
          data = plan_service.change_state({id: plan_id, plan: plan_params})
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with no plan corresponding to passed plan id' do
          plan_model_class.should_receive(:find_by_plan_id).with(plan_id) {
            raise custom_error_util::ResourceNotFoundError.new
          }
          data = plan_service.change_state({id: plan_id, plan: plan_params})
          expect(data[:response]).to eq(response_codes_util::NOT_FOUND)
        end

        context 'with valid plan id' do
          before(:example) {
            plan_model_class.should_receive(:find_by_plan_id).with(plan_id).and_return(plan)
          }
          it 'with invalid event' do
            plan.should_receive(:trigger_event).with(plan_params[:event]) {
              raise custom_error_util::PreConditionRequiredError.new
            }
            data = plan_service.change_state({id: plan_id, plan: plan_params})
            expect(data[:response]).to eq(response_codes_util::PRE_CONDITION_REQUIRED)
          end

          it 'with valid event' do
            plan.should_receive(:trigger_event).with(plan_params[:event]).and_return(true)
            data = plan_service.change_state({id: plan_id, plan: plan_params})
            expect(data[:response]).to eq(response_codes_util::SUCCESS)
          end
        end
      end

    # End of Rspec class
    end
  end
end

module CommonModule
  module V1
    module BenefitsModule
      module V1
        require 'rails_helper'
        RSpec.describe CommonModule::V1::BenefitsModule::V1::BenefitsInterface do
         
          before :all do
            #
            # Loading classes of config.autoload_paths
            # 
            Rails.application.eager_load!
            #
            # Get all the classes which include this interface
            # 
            @implementations = ObjectSpace.each_object(Class).select { |klass| klass < CommonModule::V1::BenefitsModule::V1::BenefitsInterface }
          end

          #
          # This enforce that all the classes which include the interface
          # implement apply method
          # 
          it 'should implement apply method' do
            @implementations.each do |klass|
              klass_methods = klass.instance_methods(false)
              expect(klass_methods).to include(:apply)
            end
          end
        end
      end
    end
  end
end

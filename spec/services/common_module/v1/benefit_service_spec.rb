module CommonModule
  # Module V1 of UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe CommonModule::V1::BenefitService do
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:content_util) { CommonModule::V1::Content }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:benfit_conditions) { CommonModule::V1::BenefitConditions }
      let(:benefit_service) { CommonModule::V1::BenefitService.new({}) }
      let(:benefit_model_class) { CommonModule::V1::Benefit }
      let(:benfit_states) { CommonModule::V1::ModelStates::V1::BenefitStates }
      let(:benefit_enum) { CommonModule::V1::BenefitEnum }
      let(:user_ample_credit_benefit) { CommonModule::V1::BenefitsModule::V1::UserAmpleCreditBenefit }
      let(:referrer_ample_credit_benefit) { CommonModule::V1::BenefitsModule::V1::ReferrerAmpleCreditBenefit }
      let(:plan) { FactoryGirl.build(:plan) }
      let(:benefit) { FactoryGirl.build(:benefit) }

      describe 'get benefits from ids array ' do
        let(:benefit_id) { Faker::Number.number(1) }
        let(:benefits_array) { [ benefit_id ]  }

        it 'with nil arguments' do
          expect{ benefit_service.get_benefits_from_id_array(nil) }.to raise_error(
            custom_error_util::InsufficientDataError
          )
        end

        it 'with invalid benefit id' do
          benefit_model_class.should_receive(:find_by_benefit_id).with(benefit_id.to_i) {
            raise custom_error_util::ResourceNotFoundError.new
          }
          expect{ benefit_service.get_benefits_from_id_array(benefits_array) }.to raise_error(
            custom_error_util::ResourceNotFoundError
          )
        end

        it 'with valid but inactive id' do
          benefit_model_class.should_receive(:find_by_benefit_id).with(benefit_id.to_i).and_return(benefit)
          benefit.should_receive(:active?).and_return(false)
          expect{ benefit_service.get_benefits_from_id_array(benefits_array) }.to raise_error(
            custom_error_util::InvalidArgumentsError
          )
        end

        it 'with valid and active id' do
          benefit_model_class.should_receive(:find_by_benefit_id).with(benefit_id.to_i).and_return(benefit)
          benefit.should_receive(:active?).and_return(true)
          expect( benefit_service.get_benefits_from_id_array(benefits_array) ).to eq([benefit])
        end
      end

      describe 'get_address_benefit' do
        it 'with valid params' do
          benefits = [FactoryGirl.build_stubbed(:benefit, condition: benfit_conditions::MAX_ADDRESS, status: benfit_states::ACTIVE), FactoryGirl.build_stubbed(:benefit)]
          data = benefit_service.get_address_benefit(benefits)
          expect(benefits.first.condition).to eq(benfit_conditions::MAX_ADDRESS)
        end

        it 'with invalid params' do
          expect{ benefit_service.get_address_benefit(nil) }.to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      describe 'get_benefit_instance' do
        it 'with valid args' do
          benefit_class = benefit_service.get_benefit_instance(benefit, Faker::Number.between(1,2))
          expect(benefit_class.class.class == Class).to eq(true)
        end
      end

      describe 'apply benefits' do
        it 'with valid args with benefit to user' do
          user = FactoryGirl.build(:user)
          user.benefits.push(benefit)
          user.save
          expect_any_instance_of(benefit_service.class).to receive(:get_benefit_instance).and_return(user_ample_credit_benefit.new({}))
          expect_any_instance_of(benefit_service.class).to receive(:get_benefits_by_trigger).and_return([benefit])
          expect_any_instance_of(user_ample_credit_benefit).to receive(:apply).and_return(true)

          expect(benefit_service.apply_benefits({user: user, trigger: Faker::Number.between(1,2), to: Faker::Number.between(1,2)})).to eq(true)
        end

        it 'with benefit to referrer' do
          user = FactoryGirl.build(:user)
          user.benefits.push(benefit)
          user.save
          expect_any_instance_of(benefit_service.class).to receive(:get_benefit_instance).and_return(referrer_ample_credit_benefit.new({}))
          expect_any_instance_of(benefit_service.class).to receive(:get_benefits_by_trigger).and_return([benefit])
          expect_any_instance_of(referrer_ample_credit_benefit).to receive(:apply).and_return(true)

          expect(benefit_service.apply_benefits({user: user, trigger: Faker::Number.between(1,2), to: Faker::Number.between(1,2)})).to eq(true)
        end
      end
    end
  end
end

module CommonModule
  # Module V1 of CommonModule
  module V1
    require 'rails_helper'
    RSpec.describe CommonModule::V1::CarouselService do
      let(:version) { 1 }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:content_util) { CommonModule::V1::Content }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }

      let(:carousel_service) { CommonModule::V1::CarouselService.new(version) }
      let(:carousel_model_class) { CommonModule::V1::Carousel }
      let(:image_service_helper) { ImageServiceModule::V1::ImageServiceHelper }

      # Fetching type here as this is to be used from the available types ones present in CarouselTypes class
      let(:valid_carousel_types) { carousel_service.get_valid_carousel_types }
      let(:type) { valid_carousel_types[0] }

      let(:label) { Faker::Name::name }
      let(:image) {
            {
              priority: Faker::Number.number(1),
              image_id: Faker::Number.number(1),
              action: Faker::Internet.url
            }
          }

      let(:carousel) {
        {
          type: type,
          images: [image]
        }
      }

      describe 'update carousel' do

        it 'with insufficient parameters' do
          carousel[:type] = nil
          data = carousel_service.update_carousel(carousel)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with no carousel type corresponding to passed type' do
          expect(carousel_service).to receive(:validate_update_carousel_params)
              .with(carousel).and_raise(custom_error_util::InsufficientDataError.new)

          data = carousel_service.update_carousel(carousel)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        context 'with valid carousel' do

          it 'with invalid image passed' do
            expect(carousel_service).to receive(:validate_update_carousel_params)
              .with(carousel).and_raise(custom_error_util::ResourceNotFoundError.new)
            
            data = carousel_service.update_carousel(carousel)
            expect(data[:response]).to eq(response_codes_util::NOT_FOUND)
          end

          it 'and all valid parameters' do
            expect(carousel_service).to receive(:update_the_carousel).and_return(carousel)  
            data = carousel_service.update_carousel(carousel)
            expect(data[:response]).to eq(response_codes_util::SUCCESS)
          end
        end
      end

      describe 'get carousel' do

        it 'with insufficient parameters' do
          data = carousel_service.get_carousel({type: nil})
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with invalid carousel type passed' do
          carousel_model_class.should_receive(:get_the_carousel).with({type: carousel[:type]}) {
            raise custom_error_util::ResourceNotFoundError.new
          }
          expect{ carousel_service.get_the_carousel({type: carousel[:type]}) }.to raise_error(
            custom_error_util::ResourceNotFoundError
          )
        end

        it 'with valid carousel type and carousel present with the given type' do
          expect(carousel_service).to receive(:get_the_carousel)
              .with({type: carousel[:type]}).and_return(carousel)
          
          data = carousel_service.get_carousel({type: carousel[:type]})
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end
      end
    # End of Rspec class
    end
  end
end
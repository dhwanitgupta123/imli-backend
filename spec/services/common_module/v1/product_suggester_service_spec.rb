module CommonModule
  # Module V1 of CommonModule
  module V1
    require 'rails_helper'
    RSpec.describe CommonModule::V1::ProductSuggesterService do
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:content_util) { CommonModule::V1::Content }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }

      let(:product_suggester_service) { CommonModule::V1::ProductSuggesterService.new({}) }
      let(:product_suggester_response_decorator) { CommonModule::V1::ProductSuggesterResponseDecorator }
      let(:product_suggester_model_class) { CommonModule::V1::ProductSuggester }

      let(:product_suggestion_params) {
          {
            product_name: Faker::Lorem.word
          }
        }

      let(:product_suggestions_array) { [product_suggestion_params] }

      describe 'update product suggestions' do

        it 'with nil arguments' do
          expect(product_suggester_service).to receive(:validate_product_suggestion_params)
                .with(product_suggestion_params).and_raise(custom_error_util::InsufficientDataError.new)
          data = product_suggester_service.update_product_suggestions(product_suggestion_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with insufficient parameters' do
          expect(product_suggester_service).to receive(:validate_product_suggestion_params)
                .with(nil).and_raise(custom_error_util::InsufficientDataError.new)
          data = product_suggester_service.update_product_suggestions(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with invalid user' do
          expect(product_suggester_service).to receive(:update_suggestions_by_user)
                .and_raise(custom_error_util::UnAuthenticatedUserError)
          data = product_suggester_service.update_product_suggestions(product_suggestion_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'and all valid parameters' do
          expect(product_suggester_service).to receive(:update_suggestions_by_user)
                .and_return(true)
          data = product_suggester_service.update_product_suggestions(product_suggestion_params)
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end
      end

      describe 'get product suggestions' do
        let(:get_product_suggestions_params) {
          {
            suggestions: [
              {
                user_id: Faker::Number.number(1),
                product_suggestions: product_suggestions_array
              }
            ]
          }
        }

        request = { sort_by: 'user_id' }
        it 'with wrong sort_by or order' do
          expect(product_suggester_service).to receive(:get_all_suggestions)
                .and_raise(custom_error_util::InvalidDataError.new)
          data = product_suggester_service.get_all_product_suggestions(request)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)     
        end

        it 'with correct params' do
          expect(product_suggester_service).to receive(:get_all_suggestions)
                .and_return(get_product_suggestions_params)
          expect(product_suggester_service.get_all_product_suggestions(request)).to_not be_nil
        end
      end
    # End of Rspec class
    end
  end
end
module PaymentModule
  module V1
    require 'rails_helper'
    RSpec.describe PaymentModule::V1::CitrusPaymentService do

      let(:citrus_payment_service){ PaymentModule::V1::CitrusPaymentService.new({}) }
      let(:user_service){  UsersModule::V1::UserService }
      let(:transaction_dao) { TransactionsModule::V1::TransactionsDao }
      let(:payment_dao){ MarketplaceOrdersModule::V1::PaymentDao }
      let(:payment) { FactoryGirl.build_stubbed(:payment) }
      let(:user) { FactoryGirl.create(:user) }
      let(:payment_types){ TransactionsModule::V1::PaymentTypes }
      let(:email){ FactoryGirl.create(:email, user: user) }
      let(:transaction) { FactoryGirl.build_stubbed(:transaction, payment_type: payment_types::ORDER_PAYMENT) }
      let(:response_code_util) { CommonModule::V1::ResponseCodes }
      let(:transaction_status) { TransactionsModule::V1::TransactionStatus }
      let(:payment_service) { MarketplaceOrdersModule::V1::PaymentService }
      let(:citrus_payment_utils) { PaymentModule::V1::CitrusPaymentUtils }

      let(:request) {
        {
          payment_id: Faker::Number.number(1),
          payment_type: payment_types::ORDER_PAYMENT
        }
      }
      let(:transaction_info_request) {
        {
          TxId: Faker::Number.number(1),
          amount: Faker::Commerce.price,
          pgTxnNo: Faker::Number.number(1),
          issuerRefNo: Faker::Number.number(1),
          signature: Faker::Lorem.characters(10)

        }
      }
      context 'generate url ' do
        it 'with valid request params' do
  
          user.emails = [email]
          expect_any_instance_of(payment_dao).to receive(:get_payment_from_id).and_return(payment)
          expect_any_instance_of(user_service).to receive(:get_user_from_user_id).and_return(user)
          expect_any_instance_of(transaction_dao).to receive(:create_transaction).and_return(transaction)
          expect_any_instance_of(transaction_dao).to receive(:update_transaction).and_return(transaction)

          response = citrus_payment_service.generate_url(request)
          expect(response[:response]).to eq(response_code_util::SUCCESS)
        end

        it 'with invalid args' do
          response = citrus_payment_service.generate_url({})
          expect(response[:response]).to eq(response_code_util::BAD_REQUEST)
        end
      end

      context 'get web view' do
        it 'with valid params' do
          expect_any_instance_of(transaction_dao).to receive(:get_transaction_by_id).and_return(transaction)
          response = citrus_payment_service.get_web_view({})
          expect(response[:transaction]).to eq(transaction)
        end
      end

      context 'get transaction info' do
        it 'with valid args' do
          expect_any_instance_of(transaction_dao).to receive(:get_transaction_by_id).and_return(transaction)
          expect_any_instance_of(payment_service).to receive(:handle_payment).and_return(true)
          expect_any_instance_of(transaction_dao).to receive(:update_transaction).and_return(transaction)
          expect_any_instance_of(citrus_payment_utils).to receive(:hmac_sha1).and_return(transaction_info_request[:signature])
          response = citrus_payment_service.get_transaction_info(transaction_info_request.merge(TxStatus: 'success'))
          expect(response[:TxStatus]).to eq('success')
        end

        it 'with invalid signature' do
          expect_any_instance_of(transaction_dao).to receive(:get_transaction_by_id).and_return(transaction)
          expect_any_instance_of(payment_service).to receive(:handle_payment).and_return(true)
          expect_any_instance_of(transaction_dao).to receive(:update_transaction).and_return(transaction)
          expect_any_instance_of(citrus_payment_utils).to receive(:hmac_sha1).and_return(transaction_info_request[:signature] +  Faker::Lorem.characters(10))
          response = citrus_payment_service.get_transaction_info(transaction_info_request.merge(TxStatus: 'success'))
          expect(response["Error"]).not_to be_nil
        end
      end

      context 'update transaction' do
        it 'with status not cancelled' do
          expect_any_instance_of(transaction_dao).to receive(:get_transaction_by_id).and_return(transaction)
          response = citrus_payment_service.update_transaction({status: transaction_status::SUCCESS})
          expect(response[:response]).to eq(response_code_util::BAD_REQUEST)
        end

        it 'with status cancelled' do
          expect_any_instance_of(transaction_dao).to receive(:get_transaction_by_id).and_return(transaction)
          expect_any_instance_of(payment_service).to receive(:handle_payment).and_return(true)
          expect_any_instance_of(transaction_dao).to receive(:update_transaction).and_return(transaction)
          response = citrus_payment_service.update_transaction({status: transaction_status::CANCELLED})
          expect(response[:response]).to eq(response_code_util::SUCCESS)
        end
      end
    end
  end
end

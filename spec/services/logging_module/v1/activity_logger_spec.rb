module LoggingModule
  module V1
    require 'rails_helper'
    RSpec.describe LoggingModule::V1::ActivityLogger do
      let(:activity_logger) { LoggingModule::V1::ActivityLogger.new }
      let(:log) { FactoryGirl.build_stubbed(:activity_log) }
      let(:custom_errors) {CommonModule::V1::CustomErrors}

      it "with valid params" do
        response = activity_logger.log_activity(log)
        expect(response).to eq(true)
      end

      it "with nil params" do
        expect{ activity_logger.log_activity(nil) }.to raise_error(custom_errors::InsufficientDataError)
      end
    end
  end
end
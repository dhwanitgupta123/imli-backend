# Commenting as Sendgrid doesn gives any object to action mailer in response, thus can't test this data

# module CommunicationsModule
#   module V1
#     module Mailers
#       module V1
#         require "rails_helper"
#         RSpec.describe MailerService, type: :mailer do
#           let(:cache_util) { CommonModule::V1::Cache }
#           let(:authentication_token) { Faker::Lorem.characters(65) }
#           let(:api_url) { cache_util.read('HOST').to_s + '/api/users/verify_email?token=' + authentication_token.to_s }
#           let(:mail_attributes) {{"first_name" => Faker::Name.first_name,
#                                  "to_last_name" => Faker::Name.last_name,
#                                  "to_email_id" => Faker::Internet.email,
#                                  "to_first_name" => Faker::Name.first_name,
#                                  "api_url" => api_url,
#                                  "subject" => Faker::Lorem.word
#                                 }}
#           let(:mailer_service) { MailerService}
#           let(:mail) {MailerService.send_customize_email(mail_attributes.merge(template_name: 'welcome'))}
#           let(:from_email_id) { CommonModule::V1::Cache.read('EMAIL_FROM') }

#           it 'should send an email' do
#             byebug
#             mail.deliver_now
#             ActionMailer::Base.deliveries.count.should == 1
#           end
#           it 'should set the subject to the correct subject' do
#             expect(mail.subject).to eql(mail_attributes["subject"])
#           end

#           it 'renders the receiver email' do
#             expect(mail.to).to eql([mail_attributes["to_email_id"]])
#           end
         
#           it 'renders the sender email' do
#             expect(mail.from).to eql([from_email_id])
#           end

#           it 'assigns first_name' do
#             expect(mail.body.encoded).to match(mail_attributes["to_first_name"])
#           end

#           it 'assigns last_name' do
#             expect(mail.body.encoded).to match(mail_attributes["to_last_name"])
#           end

#           it 'assigns api_url' do
#             expect(mail.body.encoded).to include(mail_attributes["api_url"])
#           end
#         end
#       end
#     end
#   end
# end
#
# Module to handle all the functionalities related to description
#
module SettingsModule
  #
  # Version1 for description module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SettingsModule::V1::SettingService do
      let(:settings_service) { SettingsModule::V1::SettingService.new({}) }

      let(:map_settings_class) { SettingsModule::V1::StaticSettings::MapSettings }
      let(:feature_settings_class) { SettingsModule::V1::StaticSettings::FeatureSettings }
      let(:constant_settings_class) { SettingsModule::V1::StaticSettings::ConstantSettings }
      let(:dynamic_settings_class) { SettingsModule::V1::DynamicSettings::DynamicSettings }
      let(:shield_service_class) { SettingsModule::V1::ShieldService }

      context 'get static settings' do
        let(:map_settings) {
          {
            'DUMMY_STATES': Faker::Lorem.word
          }
        }
        let(:feature_settings) {
          {
            'DUMMY_FEATURE': Faker::Number.number(2)
          }
        }
        let(:constant_settings) {
          {
            'DUMMY_CONSTANT': Faker::Number.number(2)
          }
        }
        it 'with valid settings present' do
          expect_any_instance_of(map_settings_class).to receive(:fetch).
            and_return(map_settings)
          expect_any_instance_of(feature_settings_class).to receive(:fetch).
            and_return(feature_settings)
          expect_any_instance_of(constant_settings_class).to receive(:fetch).
            and_return(constant_settings)
          response = settings_service.get_all_static_settings

          expect(response['MAP']).to eq(map_settings)
          expect(response['FEATURES']).to eq(feature_settings)
          expect(response['CONSTANTS']).to eq(constant_settings)
        end

        it 'with NO settings present' do
          expect_any_instance_of(map_settings_class).to receive(:fetch).
            and_return({})
          expect_any_instance_of(feature_settings_class).to receive(:fetch).
            and_return({})
          expect_any_instance_of(constant_settings_class).to receive(:fetch).
            and_return({})
          response = settings_service.get_all_static_settings

          expect(response['MAP']).to eq({})
          expect(response['FEATURES']).to eq({})
          expect(response['CONSTANTS']).to eq({})
        end
      end

      context 'get dynamic settings' do
        let(:treatment_value) { Faker::Number.number(2) }
        let(:dynamic_settings) {
          {
            'DUMMY_KEY': 'DUMMY_EXPERIMENT_NAME'
          }
        }

        it 'with valid settings present' do
          expect_any_instance_of(dynamic_settings_class).to receive(:fetch).
            and_return(dynamic_settings)
          expect(shield_service_class).to receive(:get_treatment_value).
            with(dynamic_settings[:DUMMY_KEY]).and_return(treatment_value)

          response = settings_service.get_all_dynamic_settings

          expect(response['DUMMY_KEY']).to eq(treatment_value)
        end

        it 'with invalid treatment' do
          expect_any_instance_of(dynamic_settings_class).to receive(:fetch).
            and_return(dynamic_settings)
          expect(shield_service_class).to receive(:get_treatment_value).
            with(dynamic_settings[:DUMMY_KEY]).and_return(nil)

          response = settings_service.get_all_dynamic_settings

          expect(response['DUMMY_KEY']).to eq('0')
        end
      end

    end
  end
end

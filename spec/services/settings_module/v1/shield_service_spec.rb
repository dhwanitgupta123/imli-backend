#
# Module to handle all the functionalities related to description
#
module SettingsModule
  #
  # Version1 for description module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SettingsModule::V1::ShieldService do
      let(:shield_service) { SettingsModule::V1::ShieldService }
      let(:shield_util) { SettingsModule::V1::Shield }

      let(:dummy_key) { Faker::Lorem.word }
      let(:dummy_experiment_name) { Faker::Lorem.word }
      let(:dummy_feature_value) { Faker::Number.number(2) }
      let(:dummy_treatment_value) { Faker::Number.number(2) }

      context 'get treatment value ' do

        it 'with any key' do
          expect(shield_util).to receive(:get_treatment_value).
            and_return(dummy_treatment_value)
          
          response = shield_service.get_treatment_value(dummy_key)

          expect(response).to eq(dummy_treatment_value)
        end
      end

      context 'start experiment' do

        it 'with any experiment name' do
          expect(shield_util).to receive(:start_experiment).
            and_return(dummy_treatment_value)
          
          response = shield_service.start_experiment(dummy_experiment_name)

          expect(response).to eq(dummy_treatment_value)
        end
      end

      context 'finish experiment' do

        it 'with any experiment name' do
          expect(shield_util).to receive(:finish_experiment).
            and_return(dummy_treatment_value)
          
          response = shield_service.finish_experiment(dummy_experiment_name)

          expect(response).to eq(dummy_treatment_value)
        end
      end

      context 'get feature value ' do

        it 'with any key' do
          expect(shield_util).to receive(:get_feature_value).
            and_return(dummy_feature_value)

          response = shield_service.get_feature_value(dummy_key)

          expect(response).to eq(dummy_feature_value)
        end
      end

    end
  end
end

module TransactionsModule
  module V1
    require 'rails_helper'
    RSpec.describe TransactionsModule::V1::TransactionsService do
      let(:transactions_dao) { TransactionsModule::V1::TransactionsDao }
      let(:transactions_response_decorator) { TransactionsModule::V1::TransactionsResponseDecorator }
      let(:transactions_service) { TransactionsModule::V1::TransactionsService.new({}) }
      let(:response_code_util) { CommonModule::V1::ResponseCodes }
      let(:transaction) { FactoryGirl.build(:transaction) }
      let(:custom_error_utils) { CommonModule::V1::CustomErrors }
      context 'get transaction status ' do
        it 'with nil request ' do
          response = transactions_service.get_transaction_status(nil)
          expect(response[:response]).to eq(response_code_util::BAD_REQUEST)
        end

        it 'with valid request ' do
          expect_any_instance_of(transactions_dao).to receive(:get_transaction_by_id).and_return(transaction)
          response = transactions_service.get_transaction_status({ id: Faker::Number.number(2) })
          expect(response[:response]).to eq(response_code_util::SUCCESS)
        end

        it 'with invalid transaction id' do
          expect_any_instance_of(transactions_dao).to receive(:get_transaction_by_id).and_raise(custom_error_utils::InvalidArgumentsError)
          response = transactions_service.get_transaction_status({ id: Faker::Number.number(2) })
          expect(response[:response]).to eq(response_code_util::BAD_REQUEST)
        end
      end
    end
  end
end
module TransactionsModule
  module V1
    require 'rails_helper'
    RSpec.describe TransactionsModule::V1::PaymentTransactionsService do

      let(:payment_transactions_service) { TransactionsModule::V1::PaymentTransactionsService.new({}) }
      let(:custom_error_utils) { CommonModule::V1::CustomErrors }
      let(:response_code_util) { CommonModule::V1::ResponseCodes }
      let(:gateway) { TransactionsModule::V1::GatewayType }
      let(:citrus_payment_service) { PaymentModule::V1::CitrusPaymentService }
      let(:razor_payment_service) { PaymentModule::V1::RazorpayPaymentService }
      let(:citrus_response) {
          {
            payload: {
              transaction: {
                id: Faker::Number.number(1),
                payment_status: Faker::Number.number(2),
                url: Faker::Internet.url
              }
            }
          }
        }
      let(:initiate_transaction_request) {
        {
          payment_id: Faker::Number.number(1),
          payment_type: Faker::Number.number(1),
          gateway: gateway::CITRUS_WEB
        }
      }
      let(:citrus_update_transaction_request) {
        {
          id: Faker::Number.number(1),
          status: Faker::Number.number(2),
          gateway: gateway::CITRUS_WEB,

        }
      }
      let(:razor_update_transaction_request) {
        {
          id: Faker::Number.number(1),
          status: Faker::Number.number(2),
          gateway: gateway::RAZORPAY,
          razorpay: {
            payment_id: Faker::Number.number(1)
          }
        }
      }
      let(:razor_payment_response){
        {
          payload: {
            transaction:
            {
              id: Faker::Number.number(1),
              gateway: gateway::RAZORPAY
            }
          },
          response: response_code_util::SUCCESS
        }
      }
      let(:transaction_status_response) {
         {
          payload: {
            transaction:
            {
              status: Faker::Number.number(1).to_s
            }
          },
          response: response_code_util::SUCCESS
        }
      }
      context 'initiate transaction ' do
        it ' with invalid request' do
          response = payment_transactions_service.initiate_transaction({payment_id: Faker::Number.number(2)})
          expect(response[:response]).to eq(response_code_util::BAD_REQUEST)
        end

        it 'with valid request and citrus gateway' do
          expect_any_instance_of(citrus_payment_service).to receive(:generate_url).and_return(citrus_response)
          response = payment_transactions_service.initiate_transaction(initiate_transaction_request)
          expect(response[:response]).to eq(response_code_util::SUCCESS)
        end

        it 'with valid request and razor pay gateway' do
          expect_any_instance_of(razor_payment_service).to receive(:initiate_transaction).and_return(razor_payment_response)
          razor_payment_request = initiate_transaction_request
          razor_payment_request[:gateway] = gateway::RAZORPAY
          response = payment_transactions_service.initiate_transaction(razor_payment_request)
          expect(response[:response]).to eq(response_code_util::SUCCESS)
        end

        it 'with invalid gateway' do
          invalid_gateway_request = initiate_transaction_request
          invalid_gateway_request[:gateway] = -1
          response = payment_transactions_service.initiate_transaction(invalid_gateway_request)
          expect(response[:response]).to eq(response_code_util::BAD_REQUEST)
        end
      end

      context 'update transaction' do
        it ' with invalid request' do
          response = payment_transactions_service.update_transaction({payment_id: Faker::Number.number(2)})
          expect(response[:response]).to eq(response_code_util::BAD_REQUEST)
        end

        it 'with invalid gateway' do
          invalid_gateway_request = initiate_transaction_request
          invalid_gateway_request[:gateway] = -1
          response = payment_transactions_service.update_transaction(invalid_gateway_request)
          expect(response[:response]).to eq(response_code_util::BAD_REQUEST)
        end

        it 'with valid request and citrus gateway' do
          expect_any_instance_of(citrus_payment_service).to receive(:update_transaction).and_return(transaction_status_response)
          response = payment_transactions_service.update_transaction(citrus_update_transaction_request)
          expect(response[:response]).to eq(response_code_util::SUCCESS)
        end

        it 'with valid request and razorpay gateway' do
          expect_any_instance_of(razor_payment_service).to receive(:update_transaction).and_return(transaction_status_response)
          response = payment_transactions_service.update_transaction(razor_update_transaction_request)
          expect(response[:response]).to eq(response_code_util::SUCCESS)
        end
      end
    end
  end
end

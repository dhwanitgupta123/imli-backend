module UsersModule
  # Module V1 of UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::RoleService do
      let(:version_params) { { version: 1 } }
      let(:user_service_class) { UsersModule::V1::UserService }
      let(:access_control_service_class) { UsersModule::V1::AccessControlService }
      let(:role_service_class) { UsersModule::V1::RoleService }
      let(:role_model_class) { UsersModule::V1::Role }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:content_util) { CommonModule::V1::Content }

      let(:role_service) { UsersModule::V1::RoleService.new(version_params) }
      let(:user) { FactoryGirl.build(:user) }
      let(:role) { FactoryGirl.build(:role) }

      describe 'create role' do
        let(:role_params) {
          {
            role_name: Faker::Lorem.word,
            permissions: [Faker::Number.number(1)],
            parent_id: Faker::Number.number(1)
          }
        }
        let(:parent_role) { FactoryGirl.build(:role) }

        it 'with nil arguments' do
          data = role_service.create_role(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with nil or no parent id' do
          role_params[:parent_id] = nil
          data = role_service.create_role(role_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with blank permissions array' do
          role_params[:permissions] = []
          data = role_service.create_role(role_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with no role corresponding to passed parent id' do
          role_model_class.should_receive(:find_by_role_id).with(role_params[:parent_id]) {
            raise custom_error_util::InvalidArgumentsError.new
          }
          data = role_service.create_role(role_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        context 'with valid parent role' do
          before(:example) {
            role_model_class.should_receive(:find_by_role_id).with(role_params[:parent_id]).and_return(parent_role)
          }

          it 'with invalid parameters and failed to save role' do
            role_model_class.any_instance.should_receive(:save_role) {
              raise custom_error_util::InvalidArgumentsError.new
            }
            data = role_service.create_role(role_params)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it 'with all valid parameters' do
            role_model_class.any_instance.should_receive(:save_role).and_return(true)
            data = role_service.create_role(role_params)
            expect(data[:response]).to eq(response_codes_util::SUCCESS)
          end
        end
      end

      describe 'update role' do
        let(:role_id) { Faker::Number.number(1) }
        let(:role_params) {
          {
            role_name: Faker::Lorem.word,
            permissions: [Faker::Number.number(1)]
          }
        }
        let(:valid_params) {
          {
            role_id: role_id,
            role_params: role_params
          }
        }

        it 'with nil arguments' do
          data = role_service.update_role(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with no role id to update' do
          valid_params[:role_id] = nil
          data = role_service.update_role(valid_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with blank permissions array' do
          valid_params[:role_params][:permissions] = []
          data = role_service.update_role(valid_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'when no role associated with role id' do
          role_model_class.should_receive(:find_by_role_id).with(valid_params[:role_id]) {
            raise custom_error_util::InvalidArgumentsError.new
          }
          data = role_service.update_role(valid_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        context 'when role id passed was correct' do
          before(:example) do
            role_model_class.should_receive(:find_by_role_id).with(valid_params[:role_id]).and_return(role)
          end
          it 'but invalid parameters' do
            role.should_receive(:update_role).with(role_params).and_return(true)
            role.should_receive(:save_role) {
              raise custom_error_util::InvalidArgumentsError.new
            }
            data = role_service.update_role(valid_params)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it 'and valid params' do
            role.should_receive(:update_role).with(role_params).and_return(true)
            role.should_receive(:save_role).and_return(true)
            data = role_service.update_role(valid_params)
            expect(data[:response]).to eq(response_codes_util::SUCCESS)
          end
        end
      end

      describe 'get user role' do
        let(:role_params) {
          {
            role_name: Faker::Lorem.word,
            permissions: [Faker::Number.number(1)]
          }
        }

        it 'requested by invalid user' do
          user_service_class.any_instance.should_receive(:get_user_from_user_id) {
            raise custom_error_util::UnAuthenticatedUserError.new
          }
          data = role_service.get_user_role
          expect(data[:response]).to eq(response_codes_util::UN_AUTHORIZED)
        end

        it 'with valid user and no roles' do
          user.roles = []
          user_service_class.any_instance.should_receive(:get_user_from_user_id).and_return(user)
          data = role_service.get_user_role
          expect(data[:payload][:roles]).to eq([])
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'with valid user and having some role' do
          user.roles << role
          user_service_class.any_instance.should_receive(:get_user_from_user_id).and_return(user)
          data = role_service.get_user_role
          expect(data[:payload][:roles]).not_to be_empty
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end
      end

      describe 'get roles tree' do
        let(:role_params) {
          {
            role_name: Faker::Lorem.word,
            permissions: [Faker::Number.number(1)]
          }
        }

        it 'requested by invalid user' do
          user_service_class.any_instance.should_receive(:get_user_from_user_id) {
            raise custom_error_util::UnAuthenticatedUserError.new
          }
          data = role_service.get_roles_tree
          expect(data[:response]).to eq(response_codes_util::UN_AUTHORIZED)
        end

        it 'with valid user and no roles' do
          user.roles = []
          user_service_class.any_instance.should_receive(:get_user_from_user_id).and_return(user)
          data = role_service.get_roles_tree
          expect(data[:payload][:roles]).to eq([])
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'with valid user and having some role' do
          user.roles << role
          user_service_class.any_instance.should_receive(:get_user_from_user_id).and_return(user)
          data = role_service.get_roles_tree
          expect(data[:payload][:roles]).not_to be_empty
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end
      end

      describe 'get users for role' do
        let(:role_id) { Faker::Number.number(1) }

        it 'with insufficient arguments' do
          data = role_service.get_users_for_role(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'when no role associated with role id' do
          role_model_class.should_receive(:find_by_role_id).with(role_id) {
            raise custom_error_util::InvalidArgumentsError.new
          }
          data = role_service.get_users_for_role(role_id)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'when role has various users associated' do
          role.users << user
          role_model_class.should_receive(:find_by_role_id).with(role_id).and_return(role)
          data = role_service.get_users_for_role(role_id)
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end
      end

      describe 'get all permissions' do
        let(:permissions) {
          [
            {
              id: Faker::Number.number(1),
              label: Faker::Lorem.word,
              description: Faker::Lorem.sentence
            }
          ]
        }

        it 'when unable to fetch permissions' do
          expect_any_instance_of(access_control_service_class).to receive(:get_all_permissions) {
            raise custom_error_util::InvalidArgumentsError.new
          }
          data = role_service.get_all_permissions
          expect(data[:response]).to eq(response_codes_util::INTERNAL_SERVER_ERROR)
        end

        it 'when able to fetch all permissions' do
          expect_any_instance_of(access_control_service_class).to receive(:get_all_permissions).and_return(permissions)
          data = role_service.get_all_permissions
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end
      end
    # End of Rspec class
    end
  end
end

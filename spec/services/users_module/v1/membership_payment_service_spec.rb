module UsersModule
  module V1
    module MembershipModule
      module V1
        require 'rails_helper'

        RSpec.describe UsersModule::V1::MembershipModule::V1::MembershipPaymentService do

          let(:membership_payment_service) { UsersModule::V1::MembershipModule::V1::MembershipPaymentService.new }
          let(:payment_model) { UsersModule::V1::MembershipModule::V1::MembershipPayment }
          let(:payment) { FactoryGirl.build(:membership_payment) }
          let(:custom_error_util) { CommonModule::V1::CustomErrors }
          describe 'new_payment' do
            it 'with valid params' do
              payment_args = {
                grand_total: Faker::Commerce.price,
                billing_amount: Faker::Commerce.price,
                refund: Faker::Commerce.price,
                net_total: Faker::Commerce.price,
              }
              expect(membership_payment_service.new_payment(payment_args)).to be_valid
            end
          end

          describe 'get_payment_by_id' do
            it' with valid args' do
              payment_model.any_instance.should_receive(:get_payment_by_id).and_return(payment)
              expect(membership_payment_service.get_payment_by_id({ id: 1 })).to be_valid
            end

            it' with invalid args' do
              payment_model.any_instance.should_receive(:get_payment_by_id).and_raise(custom_error_util::
                InvalidArgumentsError)
              expect{ membership_payment_service.get_payment_by_id({ id: 1 }) }.to raise_error(
                custom_error_util::InvalidArgumentsError)
            end
          end

          describe 'calculate_membership_payment' do
            it 'with plan = membership.plan' do
              plan = FactoryGirl.build(:plan)
              membership = FactoryGirl.build(:membership, membership_payment: payment, plan: plan)
              args = membership_payment_service.calculate_membership_payment(membership, plan)
              expect(args[:payment_args][:refund]).to eql(0.0)
            end

            it 'with plan != membership.plan' do
              plan = FactoryGirl.build(:plan)
              another_plan = FactoryGirl.build(:plan)
              membership = FactoryGirl.build(:membership, membership_payment: payment, plan: another_plan)
              args = membership_payment_service.calculate_membership_payment(membership, plan)
              expect(args[:payment_args][:refund]).to eql(another_plan.fee)
            end
          end

          describe 'final_membership_payment' do
            let(:plan) { FactoryGirl.build(:plan) }
            let(:membership) { FactoryGirl.build(:membership, membership_payment: payment, plan: plan) }
            let(:payment_args) { { mode: 1 } }
            it 'with valid params' do
              expect(membership_payment_service.final_membership_payment(membership, payment_args)).to be_valid
            end
            it 'with invalid params' do
              expect{ membership_payment_service.final_membership_payment(membership, payment_args.merge(mode: 10)) }.to raise_error(
                custom_error_util::InvalidArgumentsError)
            end
          end
        end
      end
    end
  end
end

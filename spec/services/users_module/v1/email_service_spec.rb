module UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::EmailService do
      let(:version) { 1 }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:provider_type_util) { UsersModule::V1::ProviderType }
      let(:email_service) { UsersModule::V1::EmailService.new }
      let(:email_model) { UsersModule::V1::Email }

      let(:request_type_sms) { provider_type_util::SMS }
      let(:request_type_generic) { provider_type_util::GENERIC }
      let(:message) { FactoryGirl.build_stubbed(:message) }
      let(:provider) { FactoryGirl.build_stubbed(:provider, message: message) }
      let(:valid_user) { FactoryGirl.build(:user) }

      describe "add email for user" do
        let(:valid_params) {
          {
            email: {
              email_id: Faker::Internet.free_email,
              is_primary: true
            },
            account_type: Faker::Number.number(1),
            user: valid_user
          }
        }
        it "with invalid arguments" do
          valid_params[:email_id] = nil
          email_model.any_instance.should_receive(:save_email){
            raise custom_error_util::RunTimeError
          }
          expect{ email_service.add_email_for_user(valid_params) }.to raise_error(
          custom_error_util::RunTimeError)
        end

        it "with valid arguments" do
          email_model.any_instance.should_receive(:save_email).and_return(true)
          expect{ email_service.add_email_for_user(valid_params) }.to_not raise_error
        end
      end

      describe "authenticate email" do
        let(:user) { FactoryGirl.create(:user) }
        let(:email) { FactoryGirl.create(:email, :user => user) }
        let(:token) { Faker::Lorem.characters(20) }
        
        it "with no email associated with token" do
          email_model.should_receive(:find_by_authentication_token).with(token){
            raise ActiveRecord::RecordNotFound.new
          }

          expect{ email_service.authenticate_email(token) }.to raise_error(
            custom_error_util::InvalidDataError )
        end

        it "with email token with no user associated" do
          email.user = nil
          email_model.should_receive(:find_by_authentication_token).and_return(email)
          expect{ email_service.authenticate_email(token) }.to raise_error(
            custom_error_util::InvalidDataError )
        end

        context "and valid email token" do
          # before(:example) {
          #   email_model.should_receive(:find_by_authentication_token).and_return(email)
          # }

          it "but already verified" do
            email.verified = true
            email_model.should_receive(:find_by_authentication_token).and_return(email)

            expect{ email_service.authenticate_email(token) }.to raise_error(
              custom_error_util::EmailAlreadyVerifiedError )
          end

          it "and also not verified earlier" do
            email.verified = false
            email_model.should_receive(:find_by_authentication_token).and_return(email)
            email_model.any_instance.should_receive(:set_verify_to_true).and_return(true)
            email_model.any_instance.should_receive(:save_email).and_return(true)

            expect{ email_service.authenticate_email(token) }.to_not raise_error
          end
        end
      end


      describe "get email from email id" do
        let(:email_id) { Faker::Internet.free_email }
        let(:email) { FactoryGirl.build_stubbed(:email, :email_id => email_id) }

        it "with invalid mail id" do
          email_model.should_receive(:find_by_email_id).with(email_id){
            raise custom_error_util::EmailNotFoundError.new
          }

          expect{ email_service.get_email_from_email_id(email_id) }.to raise_error(
            custom_error_util::EmailNotFoundError )
        end

        it "with valid mail id" do
          email_model.should_receive(:find_by_email_id).with(email_id).and_return(email)

          expect(email_service.get_email_from_email_id(email_id)).to eq(email)
        end
      end

      describe "assign new authentication token" do
        let(:email) { FactoryGirl.build_stubbed(:email) }

        it "when token expired" do
          email.expires_at = Time.zone.now - 1.day
          email_model.any_instance.should_receive(:set_authentication_token).and_return(true)
          email_model.any_instance.should_receive(:set_expires_at).and_return(true)
          email_model.any_instance.should_receive(:save_email).and_return(true)

          email_service.assign_new_authentication_token(email)
        end

        it "when token did not expired" do
          email.expires_at = Time.zone.now + 2.days
          expect { email_service.assign_new_authentication_token(email) }.to_not raise_error
        end
      end

    end
  end
end
module UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::LoginUserService do
      let(:version) { 1 }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:provider_type_util) { UsersModule::V1::ProviderType }
      let(:login_user_service) { UsersModule::V1::LoginUserService.new(version) }

      let(:user_params_password) { {:phone_number => Faker::Number.number(10).to_s, :password => Faker::Lorem.characters(7)} }
      let(:user_params_otp) { {:phone_number => Faker::Number.number(10).to_s, :otp => Faker::Number.number(4) } } 
      let(:user_params_without_otp_password) { {:phone_number => Faker::Number.number(10).to_s} }
      let(:user_params_without_phone_number) { {:first_name => Faker::Lorem.characters(7)} }
      let(:request_type_sms) { provider_type_util::SMS }
      let(:request_type_generic) { provider_type_util::GENERIC }
      let(:message) { FactoryGirl.build_stubbed(:message) }
      let(:provider) { FactoryGirl.build_stubbed(:provider, message: message) }
      let(:user) { FactoryGirl.build(:user, provider: provider) }

      describe "update_login_type" do
        it "update log in type successfully" do
          UsersModule::V1::ProviderService.any_instance.stub(:update_provider_type).and_return(provider)
          args = { :user_params => user_params_otp, :request_type => request_type_sms, :user => user }
          expect(login_user_service.update_login_type(args)).to_not be_nil
        end

        it "if password is missing" do
          UsersModule::V1::ProviderService.any_instance.stub(:update_provider_type).and_raise(custom_error_util::InsufficientDataError)
          args = { :user_params => user_params_without_otp_password, :request_type => request_type_generic, :user => user }
          expect{ login_user_service.update_login_type(args) }.to raise_error(
            custom_error_util::InsufficientDataError)
        end

        it "if user doesn't exists" do
          UsersModule::V1::ProviderService.any_instance.stub(:update_provider_type).and_raise(custom_error_util::ResourceNotFoundError)
          args = { :user_params => user_params_otp, :request_type => request_type_sms, :user => user }
          expect{ login_user_service.update_login_type(args) }.to raise_error(
            custom_error_util::ResourceNotFoundError)
        end

        it "if user updates generic type" do
          UsersModule::V1::ProviderService.any_instance.stub(:update_provider_type).and_raise(custom_error_util::ExistingUserError)
          args = { :user_params => user_params_password, :request_type => request_type_generic, :user => user }
          expect{ login_user_service.update_login_type(args) }.to raise_error(
            custom_error_util::ExistingUserError)
        end
      end  
      describe "login_user" do
        it "log in with OTP successfully" do
          UsersModule::V1::ProviderService.any_instance.stub(:check_provider).and_return(provider)
          args = { :user_params => user_params_otp, :user => user, :request_type => request_type_sms }
          expect(login_user_service.login_user(args)).to_not be_nil
        end

        it "log in with password successfully" do
          UsersModule::V1::ProviderService.any_instance.stub(:check_provider).and_return(provider)
          args = { :user_params => user_params_password, :user => user, :request_type => request_type_generic }
          expect(login_user_service.login_user(args)).to_not be_nil
        end

        it "if password is missing" do
          UsersModule::V1::ProviderService.any_instance.stub(:check_provider).and_raise(custom_error_util::InsufficientDataError)
          args = { :user_params => user_params_without_otp_password, :user => user, :request_type => request_type_generic }
          expect{ login_user_service.login_user(args) }.to raise_error(
            custom_error_util::InsufficientDataError)
        end

        it "if user doesn't exists" do
          UsersModule::V1::ProviderService.any_instance.stub(:check_provider).and_raise(custom_error_util::ResourceNotFoundError)
          args = { :user_params => user_params_without_otp_password, :user => user, :request_type => request_type_sms }
          expect{ login_user_service.login_user(args) }.to raise_error(
            custom_error_util::ResourceNotFoundError)
        end

        it "if otp is missing" do
          UsersModule::V1::ProviderService.any_instance.stub(:check_provider).and_raise(custom_error_util::InsufficientDataError)
          args = { :user_params => user_params_without_otp_password, :user => user, :request_type => request_type_sms }
          expect{ login_user_service.login_user(args) }.to raise_error(
            custom_error_util::InsufficientDataError)
        end

        it "if proper request_type is not present" do
          UsersModule::V1::ProviderService.any_instance.stub(:check_provider).and_raise(custom_error_util::InsufficientDataError)
          args = { :user_params => user_params_otp, :user => user, :request_type => 3 }
          expect{ login_user_service.login_user(args) }.to raise_error(
            custom_error_util::InsufficientDataError)
        end
      end 
    end
  end
end
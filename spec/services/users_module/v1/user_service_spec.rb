module UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::UserService do
      let(:version_params) { { version: 1 } }
      let(:user_service_class) { UsersModule::V1::UserService }
      let(:email_service_class) { UsersModule::V1::EmailService }
      let(:role_service_class) { UsersModule::V1::RoleService }
      let(:address_service_class) { AddressModule::V1::AddressService }
      let(:user_model_class) { UsersModule::V1::User }

      let(:user_service) { UsersModule::V1::UserService.new(version_params) }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:content_util) { CommonModule::V1::Content }
      let(:provider_type_util) { UsersModule::V1::ProviderType }
      let(:plan_service) { CommonModule::V1::PlanService }
      let(:membership_service) { UsersModule::V1::MembershipService }
      let(:benefit_service) { CommonModule::V1::BenefitService }
      let(:membership_state) { UsersModule::V1::ModelStates::MembershipStates }
      let(:benefit_state) { CommonModule::V1::ModelStates::V1::BenefitStates }
      let(:user_params) {
        {
          :phone_number => Faker::Number.number(10).to_s,
          :first_name => Faker::Lorem.characters(7),
          :referral_code => Faker::Lorem.characters(4)
          }
        }
      let(:user_params_password) { {:phone_number => Faker::Number.number(10).to_s, :password => Faker::Lorem.characters(7)} }
      let(:user_params_otp) { {:phone_number => Faker::Number.number(10).to_s, :otp => Faker::Number.number(4) } } 
      let(:user_params_without_otp_password) { {:phone_number => Faker::Number.number(10).to_s} }
      let(:user_params_without_phone_number) { {:first_name => Faker::Lorem.characters(7)} }
      let(:user_profile_params) { { first_name: Faker::Lorem.characters(7), last_name: Faker::Lorem.characters(7), emails: [{ email_id: 'a@b.com', is_primary: true }] } }
      let(:request_type_sms) { provider_type_util::SMS }
      let(:request_type_generic) { provider_type_util::GENERIC }
      let(:message) { FactoryGirl.build_stubbed(:message) }
      let(:provider) { FactoryGirl.build_stubbed(:provider, message: message) }
      # Using FactoryGirl.build_stubbed because user.id is needed in test cases
      # which can only be provided by FG.create OR FG.build_stubbed
      let(:user) { FactoryGirl.build_stubbed(:user, provider: provider) }
      let(:role) { FactoryGirl.build_stubbed(:role) }
      let(:email) { {:email_id => Faker::Internet.free_email} }

      #Building factories of Address module
      let(:country) { FactoryGirl.build_stubbed(:country) }
      let(:state) { FactoryGirl.build_stubbed(:state, country: country) }
      let(:city) { FactoryGirl.build_stubbed(:city, state: state) }
      let(:area) { FactoryGirl.build_stubbed(:area, city: city) }
      let(:address) { FactoryGirl.build_stubbed(:address, user: user, area: area) }
      let(:benefit) { FactoryGirl.build_stubbed(:benefit, status: benefit_state::ACTIVE, condition: 'max_address') }
      let(:plan) { FactoryGirl.build_stubbed(:plan) }
      let(:membership) { FactoryGirl.build_stubbed(:membership, plan: plan, workflow_state: membership_state::ACTIVE) }
      # Using FactoryGirl.build_stubbed because address.id and area.id is needed in test cases
      # which can only be provided by FG.create OR FG.build_stubbed
      let(:address_params) {
          {
            id: address.id,
            nickname: address.nickname,
            address_line1: address.address_line1,
            address_line2: address.address_line2,
            landmark: address.landmark,
            area_id: area.id
          }
        }
      let(:address_params_array) { [address_params] }
      let(:membership_args) { { membership: membership } }
      describe 'decide register login ' do
        it 'with no request_type' do
          request_type = ''
          response = user_service.decide_register_login(user_params, request_type)
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end
        it 'with correct request' do
          plan_service.any_instance.should_receive(:get_plan_by_key).and_return(plan)
          expect_any_instance_of(membership_service).to receive(:subscribe_user_to_membership).and_return(membership_args)
          response = user_service.decide_register_login(user_params, request_type_sms)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'with exisiting user' do
          UsersModule::V1::UserService.any_instance.stub(:user_exists).and_return(user)
          UsersModule::V1::UserService.any_instance.stub(:update_login_type).and_return(user)
          response = user_service.decide_register_login(user_params, request_type_sms)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'with correct request for new generic type' do
          UsersModule::V1::UserService.any_instance.stub(:register_user).and_return(user)
          response = user_service.decide_register_login(user_params_password, request_type_generic)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'when new generic provider succeeds' do
          plan_service.any_instance.stub(:get_plan_by_key).and_return(plan)
          membership_service.any_instance.stub(:subscribe_user_to_membership).and_return(membership_args)
          user_service.decide_register_login(user_params, request_type_sms)
          UsersModule::V1::UserService.any_instance.stub(:update_login_type).and_return(user)
          UsersModule::V1::UserService.any_instance.stub(:update_login_type).and_return(user)
          response =  user_service.decide_register_login(user_params_password, request_type_generic)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'when referral code reaches threshold of 100 retries' do
          plan_service.any_instance.should_receive(:get_plan_by_key).and_return(plan)
          expect_any_instance_of(membership_service).to receive(:subscribe_user_to_membership).and_return(membership_args)
          UsersModule::V1::User.stub(:referred_by).and_return(user)
          UsersModule::V1::UserService.any_instance.stub(:user_exists).and_return(nil)
          response = user_service.decide_register_login(user_params, request_type_sms)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end
      end

      describe 'login with OTP ' do
        it 'when user not exists' do
          UsersModule::V1::UserService.any_instance.stub(:validate_user_existence).and_raise(custom_error_util::UnAuthenticatedUserError)
          response = user_service.login(user_params,request_type_sms)
          expect(response[:response]).to eq(response_codes_util::UN_AUTHORIZED)
        end
        it 'when OTP is verified successfully' do
          session_token = 'xyz'
          UsersModule::V1::User.stub(:exists?).and_return(user)
          UsersModule::V1::LoginUserService.any_instance.stub(:login_user).and_return(session_token)
          UsersModule::V1::User.any_instance.stub(:change_user_state_to_pending)
          UsersModule::V1::User.any_instance.stub(:trigger_event)
          response = user_service.login(user_params_otp,request_type_sms)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end
        it 'when session token is missing' do
          UsersModule::V1::User.stub(:exists?).and_return(user)
          UsersModule::V1::LoginUserService.any_instance.stub(:login_user).and_raise(custom_error_util::RunTimeError)
          UsersModule::V1::User.any_instance.stub(:change_user_state_to_pending)
          response = user_service.login(user_params_otp, request_type_sms)
          expect(response[:response]).to eq(response_codes_util::INTERNAL_SERVER_ERROR)
        end
      end

      describe 'login with Password' do
        it 'when user not exists' do
          UsersModule::V1::UserService.any_instance.stub(:validate_user_existence).and_raise(custom_error_util::UnAuthenticatedUserError)
          response = user_service.login(user_params, request_type_generic)
          expect(response[:response]).to eq(response_codes_util::UN_AUTHORIZED)
        end
        it 'when Password is verified successfully' do
          session_token = 'xyz'
          UsersModule::V1::User.stub(:exists?).and_return(user)
          UsersModule::V1::LoginUserService.any_instance.stub(:login_user).and_return(session_token)
          response = user_service.login(user_params_password, request_type_generic)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end
        it 'with no phone_number for generic type' do
          response = user_service.login(user_params_without_phone_number, request_type_generic)
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end
      end

      describe 'resend otp resquest' do
        it 'when no otp returned' do
          UsersModule::V1::User.stub(:exists?).and_return(user)
          UsersModule::V1::ProviderService.any_instance.stub(:resend_otp).and_raise(custom_error_util::RunTimeError)
          response = user_service.resend_otp(user_params) 
          expect(response[:response]).to eq(response_codes_util::INTERNAL_SERVER_ERROR)
        end
        it 'when no phone_number in request' do
          response = user_service.resend_otp(user_params_without_phone_number)
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end
        it 'when otp returned' do
          otp = 4567
          UsersModule::V1::User.stub(:exists?).and_return(user)
          UsersModule::V1::ProviderService.any_instance.stub(:resend_otp).and_return(otp)
          response = user_service.resend_otp(user_params)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end
        it 'when user not exists' do
          UsersModule::V1::UserService.any_instance.stub(:validate_user_existence).and_raise(custom_error_util::UnAuthenticatedUserError)
          response = user_service.resend_otp(user_params)
          expect(response[:response]).to eq(response_codes_util::UN_AUTHORIZED)
        end
        it 'when provider not exists' do
          user = FactoryGirl.build_stubbed(:user, provider: nil)
          UsersModule::V1::User.stub(:exists?).and_return(user)
          response = user_service.resend_otp(user_params)
          expect(response[:response]).to eq(response_codes_util::INTERNAL_SERVER_ERROR)
        end
      end

      describe 'activate with referral code request' do
        it 'when user uses his own referral code' do
          UsersModule::V1::UserService.any_instance.stub(:get_user_from_user_id).and_return(user)
          UsersModule::V1::User.stub(:referred_by).and_return(user)
          UsersModule::V1::User.any_instance.stub(:pending?).and_return(true)
          UsersModule::V1::UserService.any_instance.stub(:activate_user).and_return(nil)
          UsersModule::V1::UserService.any_instance.stub(:set_referred_by)
          response = user_service.activate_with_referral_code(user_params)
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end
        it 'when referred user is pending' do
          referred_user = FactoryGirl.build(:user, referral_limit: 5 )
          UsersModule::V1::UserService.any_instance.stub(:get_user_from_user_id).and_return(user)
          UsersModule::V1::User.stub(:referred_by).and_return(referred_user)
          expect(user).to receive(:pending?).and_return(true)
          expect(referred_user).to receive(:active?).and_return(false)
          UsersModule::V1::UserService.any_instance.stub(:activate_user).and_return(nil)
          UsersModule::V1::UserService.any_instance.stub(:set_referred_by)
          response = user_service.activate_with_referral_code(user_params)
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end
        it 'when no referral_code in request' do
          response = user_service.activate_with_referral_code(user_params_otp)
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end
        it 'when user is Inactive' do
          UsersModule::V1::UserService.any_instance.stub(:get_user_from_user_id).and_return(user)
          UsersModule::V1::User.stub(:referred_by).and_return(user)
          UsersModule::V1::User.any_instance.stub(:pending?).and_return(false)
          response = user_service.activate_with_referral_code(user_params)
          expect(response[:response]).to eq(response_codes_util::PRE_CONDITION_REQUIRED)
        end
        it 'when user not exists' do
          UsersModule::V1::UserService.any_instance.stub(:validate_user_existence).and_raise(custom_error_util::UnAuthenticatedUserError)
          response = user_service.activate_with_referral_code(user_params)
          expect(response[:response]).to eq(response_codes_util::UN_AUTHORIZED)
        end
        it 'when referral code is wrong' do
          UsersModule::V1::UserService.any_instance.stub(:get_user_from_user_id).and_return(user)
          UsersModule::V1::UserService.any_instance.stub(:get_referred_by_user).and_raise(custom_error_util::WrongReferralCodeError)
          UsersModule::V1::User.any_instance.stub(:pending?).and_return(true)
          response = user_service.activate_with_referral_code(user_params)
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end
        it 'when status is returned' do
          referred_user = FactoryGirl.build(:user, referral_limit: 5)
          UsersModule::V1::UserService.any_instance.stub(:get_user_from_user_id).and_return(user)
          UsersModule::V1::User.stub(:referred_by).and_return(referred_user)
          expect(user).to receive(:pending?).and_return(true)
          expect(referred_user).to receive(:active?).and_return(true)
          UsersModule::V1::UserService.any_instance.stub(:activate_user).and_return('active')
          UsersModule::V1::UserService.any_instance.stub(:set_referred_by)
          response = user_service.activate_with_referral_code(user_params)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end
      end

      describe 'check user status' do
        it 'when user not exists' do
          UsersModule::V1::UserService.any_instance.stub(:get_user_from_user_id).and_raise(custom_error_util::UnAuthenticatedUserError)
          response = user_service.check_user_state
          expect(response[:response]).to eq(response_codes_util::UN_AUTHORIZED)
        end
      end

      describe 'verify email' do
        let(:email_token) { Faker::Lorem.characters(20).to_s}
        let(:valid_token_request) {
          {
            token: email_token
          }
        }
        it 'with nil arguments' do
          data = user_service.verify_email(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        context 'with valid arguments' do
          it 'but invalid data' do
            email_service_class.any_instance.should_receive(:authenticate_email).with(email_token){
              raise custom_error_util::InvalidDataError.new
            }
            data = user_service.verify_email(valid_token_request)
            expect(data[:error][:message]).to eq(content_util::INVALID_DATA_PASSED)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST) 
          end

          it 'and email already verified' do
            email_service_class.any_instance.should_receive(:authenticate_email).with(email_token){
              raise custom_error_util::EmailAlreadyVerifiedError.new
            }
            data = user_service.verify_email(valid_token_request)
            expect(data[:payload][:message]).to eq(content_util::EMAIL_ALREADY_VERIFIED)
            expect(data[:response]).to eq(response_codes_util::SUCCESS) 
          end

          it 'but token expired' do
            email_service_class.any_instance.should_receive(:authenticate_email).with(email_token){
              raise custom_error_util::AuthenticationTimeoutError.new
            }
            data = user_service.verify_email(valid_token_request)
            expect(data[:error][:message]).to eq(content_util::AUTHENTICATION_TIMEOUT)
            expect(data[:response]).to eq(response_codes_util::AUTHENTICATION_TIMEOUT) 
          end
        end
      end

      describe 'resend email token' do
        # Building a dummy user again using FactoryGirl.build because interaction
        # with DB is needed
        let(:user) { FactoryGirl.build(:user, provider: provider) }
        let(:email_id) { Faker::Internet.free_email }
        let(:email) { FactoryGirl.build_stubbed(:email) }
        let(:valid_email_request) {
          {
            email_id: email_id
          }
        }

        it 'with nil arguments' do
          data = user_service.resend_email_token(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with blank email id in parameters' do
          valid_email_request[:email_id] = ''
          data = user_service.resend_email_token(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with email id for which no email is associated' do
          email_service_class.any_instance.should_receive(:get_email_from_email_id).with(email_id){
            raise custom_error_util::EmailNotFoundError.new
          }
          data = user_service.resend_email_token(valid_email_request)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'but request is not by authenticated user' do
          email_service_class.any_instance.stub(:get_email_from_email_id).and_return(email)
          user_model_class.stub(:find_by_user_id){
            raise custom_error_util::UnAuthenticatedUserError.new
          }
          data = user_service.resend_email_token(valid_email_request)
          expect(data[:response]).to eq(response_codes_util::UN_AUTHORIZED)
        end

        context 'with proper email and user associated with parameters' do
          let(:email_one) { FactoryGirl.build(:email, :user => user) }
          let(:email_two) { FactoryGirl.build(:email, :user => user) }
          let(:email_token) { Faker::Lorem.characters(20).to_s }

          before(:example){
            email_service_class.any_instance.stub(:get_email_from_email_id).and_return(email)
          }

          it 'and user has no emails' do
            user.emails = []
            user_model_class.stub(:find_by_user_id).and_return(user)
            data = user_service.resend_email_token(valid_email_request)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it 'and user with emails but not the requested email_id' do
            user_model_class.stub(:find_by_user_id).and_return(user)
            data = user_service.resend_email_token(valid_email_request)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it 'and the requested mail id belongs to requester/user' do
            user.emails << email
            user_model_class.stub(:find_by_user_id).and_return(user)

            # email service should receive call for setting authentication token
            email_service_class.any_instance.should_receive(:assign_new_authentication_token).with(email).and_return(email_token)
            data = user_service.resend_email_token(valid_email_request)
            expect(data[:response]).to eq(response_codes_util::SUCCESS)
          end
        end
      end

      describe 'add role to user' do
        let(:user_params){
          {
            phone_number: Faker::Number.number(10)
          }
        }
        let(:role_params){
          {
            role_id: Faker::Number.number(2)
          }
        }
        let(:request_params){
          {
            user_params: user_params,
            role_params: role_params
          }
        }

        it 'with nil arguments' do
          data = user_service.add_role_to_user(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with insufficient params' do
          request_params[:user_params] = nil
          data = user_service.add_role_to_user(request_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with invalid user id' do
          user_model_class.should_receive(:exists?).with(user_params[:phone_number]).and_return(nil)
          data = user_service.add_role_to_user(request_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with requester same as user' do
          USER_SESSION['user_id'] = user.id
          user_model_class.should_receive(:exists?).with(user_params[:phone_number]).and_return(user)
          data = user_service.add_role_to_user(request_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        context 'with requester different from user' do
          before(:example) {
            # Modify session user_id to be different from user's id
            USER_SESSION[:user_id] = user.id + 1
            user_model_class.should_receive(:exists?).with(user_params[:phone_number]).and_return(user)
          }
          it 'and valid user but invalid role id passed' do
            role_service_class.any_instance.should_receive(:get_role_from_role_id).with(role_params[:role_id]){
              raise custom_error_util::InvalidArgumentsError.new
            }
            data = user_service.add_role_to_user(request_params)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it 'and valid user and valid role' do
            role_service_class.any_instance.should_receive(:get_role_from_role_id).
                with(role_params[:role_id]).and_return(role)
            data = user_service.add_role_to_user(request_params)
            expect(data[:payload][:message]).to eq(content_util::ROLE_ADDED)
            expect(data[:response]).to eq(response_codes_util::SUCCESS)
          end
        end
      end

      describe 'remove role from user' do
        let(:user_params){
          {
            user_id: Faker::Number.number(2)
          }
        }
        let(:role_params){
          {
            role_id: Faker::Number.number(2)
          }
        }
        let(:request_params){
          {
            user_params: user_params,
            role_params: role_params
          }
        }

        it 'with nil arguments' do
          data = user_service.remove_role_from_user(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with insufficient params' do
          request_params[:user_params] = nil
          data = user_service.remove_role_from_user(request_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with invalid user id' do
          user_model_class.should_receive(:find_by_user_id).with(user_params[:user_id]){
            raise ActiveRecord::RecordNotFound.new
          }
          data = user_service.remove_role_from_user(request_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with valid user but invalid role id passed' do
          user_model_class.should_receive(:find_by_user_id).with(user_params[:user_id]).and_return(user)
          role_service_class.any_instance.should_receive(:get_role_from_role_id).with(role_params[:role_id]){
            raise custom_error_util::InvalidArgumentsError.new
          }
          data = user_service.remove_role_from_user(request_params)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with valid user and valid role' do
          user_model_class.should_receive(:find_by_user_id).
              with(user_params[:user_id]).and_return(user)
          role_service_class.any_instance.should_receive(:get_role_from_role_id).
              with(role_params[:role_id]).and_return(role)
          data = user_service.remove_role_from_user(request_params)
          expect(data[:payload][:message]).to eq(content_util::ROLE_REMOVED)
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end
      end

      describe "update user details" do

        context "with invalid parameters" do
          it "if no data passed" do
            data = user_service.update_user_details(nil)
            expect(data[:response]).to eq(response_codes_util::UN_AUTHORIZED)
          end
        end

        context "with valid details" do
          context "and valid user registration" do
            let(:email_token) { Faker::Lorem.characters(20).to_s}

            it "with invalid email registration" do
              user_service.should_receive(:get_user_from_user_id).and_return(user)
              user_profile_params[:emails].first[:email_id] = "@test.com"
              data = user_service.update_user_details(user_profile_params)
              expect(data[:error][:message]).to eq(content_util::INVALID_EMAIL_ID)
              expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
            end

            it "with valid user registration" do
              user_service.should_receive(:get_user_from_user_id).and_return(user)
              user_service.should_receive(:update_user_info_details).and_return(user)
              data = user_service.update_user_details(user_profile_params)
              expect(data[:response]).to eq(response_codes_util::SUCCESS)
            end
          end
        end
      end

      describe 'get all users' do
        let(:paginate_params) { { state: 1, per_page: 20, page_no: 1, sort_by: 'id' } }
        it 'when users are present in DB' do
          user_model_class.should_receive(:get_all_users).with(paginate_params).and_return({users: [user]})
          response = user_service.get_all_users(paginate_params)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'when no users present in DB' do
          user_model_class.should_receive(:get_all_users).with(paginate_params).and_return({users: []})
          response = user_service.get_all_users(paginate_params)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end
      end

      describe 'change state of user' do
        let(:user_id) {
          Faker::Number.number(2).to_s
        }
        let(:user_params) {
          {
            event: Faker::Number.number(1)
          }
        }

        it 'with nil arguments' do
          data = user_service.change_state(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with insufficient parameters' do
          data = user_service.change_state({id: nil, user: user_params})
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with no event passed' do
          user_params[:event] = nil
          data = user_service.change_state({id: user_id, user: user_params})
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with no user corresponding to passed user id' do
          user_model_class.should_receive(:find_by_user_id).with(user_id) {
            raise ActiveRecord::RecordNotFound.new
          }
          data = user_service.change_state({id: user_id, user: user_params})
          expect(data[:response]).to eq(response_codes_util::NOT_FOUND)
        end

        context 'with valid user id' do
          before(:example) {
            user_model_class.should_receive(:find_by_user_id).with(user_id).and_return(user)
          }
          it 'with invalid event' do
            user.should_receive(:trigger_event).with(user_params[:event]) {
              raise custom_error_util::PreConditionRequiredError.new
            }
            data = user_service.change_state({id: user_id, user: user_params})
            expect(data[:response]).to eq(response_codes_util::PRE_CONDITION_REQUIRED)
          end

          it 'with valid event and valid user role' do
            user.should_receive(:trigger_event).with(user_params[:event]).and_return(true)
            role_service_class.any_instance.should_receive(:get_role_with_parameters).and_return(role)
            data = user_service.change_state({id: user_id, user: user_params})
            expect(data[:response]).to eq(response_codes_util::SUCCESS)
          end
        end
      end

      describe "add user address" do
        let(:addresses) {{ addresses: address_params_array }}
        let(:add_address_request) {
          {
            user: user,
            addresses: address_params_array
          }
        }
        context "with invalid params" do
          it 'with nil arguments' do
            data = user_service.add_address(nil)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it 'with address line1 blank' do
            plan.benefits << benefit
            address_params[:address_line1] = nil
            user_service.should_receive(:get_user_from_user_id).and_return(user)
            membership_service.any_instance.should_receive(:get_pending_or_active_membership).and_return(membership)
            benefit_service.any_instance.should_receive(:get_address_benefit).and_return(benefit)
            data = user_service.add_address(addresses)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it 'with area_id blank' do
            address_params[:area_id] = nil
            data = user_service.add_address(addresses)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it 'with invalid user id' do
          user_service.should_receive(:get_user_from_user_id).and_raise(custom_error_util::UnAuthenticatedUserError)
          data = user_service.add_address(addresses)
          expect(data[:response]).to eq(response_codes_util::UN_AUTHORIZED)
          end

          it 'when membership does not allow to add new address' do
            benefit = FactoryGirl.build_stubbed(:benefit)
            plan.benefits << benefit
            user_service.should_receive(:get_user_from_user_id).and_return(user)
            membership_service.any_instance.should_receive(:get_pending_or_active_membership).and_return(membership)
            benefit_service.any_instance.should_receive(:get_address_benefit).and_return(benefit)
            data = user_service.add_address(addresses)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it 'when membership is not active' do
            membership = FactoryGirl.build_stubbed(:membership, plan: plan, workflow_state: membership_state::INACTIVE)
            plan.benefits << benefit
            user_service.should_receive(:get_user_from_user_id).and_return(user)
            membership_service.any_instance.should_receive(:get_pending_or_active_membership).and_return(membership)
            data = user_service.add_address(addresses)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it 'when address benefit is blank' do
            user_service.should_receive(:get_user_from_user_id).and_return(user)
            membership_service.any_instance.should_receive(:get_pending_or_active_membership).and_return(membership)
            data = user_service.add_address(addresses)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end
        end

        context "with valid params" do
          it 'with valid user and valid args' do
            plan.benefits << benefit
            membership_service.any_instance.should_receive(:get_pending_or_active_membership).and_return(membership)
            benefit_service.any_instance.should_receive(:get_address_benefit).and_return(benefit)
            user_service.should_receive(:get_user_from_user_id).and_return(user)
            address_service_class.any_instance.should_receive(:add_address_for_user).
              with({user: user, address: address_params}).and_return(address)
            data = user_service.add_address(addresses)
            expect(data[:response]).to eq(response_codes_util::SUCCESS)
          end
        end
      end

      describe "get all address of user" do
        it 'with invalid user' do
          user_service.should_receive(:get_user_from_user_id).and_raise(custom_error_util::UnAuthenticatedUserError)
          data = user_service.get_all_addresses_of_user
          expect(data[:response]).to eq(response_codes_util::UN_AUTHORIZED)
        end

        it 'with valid user' do
          user_service.should_receive(:get_user_from_user_id).and_raise(custom_error_util::UnAuthenticatedUserError)
          data = user_service.get_all_addresses_of_user
          expect(data[:response]).to eq(response_codes_util::UN_AUTHORIZED)
        end
      end

      describe "update address of user" do
        let(:update_address_request) {
          {
            addresses: address_params_array,
            id: address.id
          }
        }
        context "with invalid params" do
          it 'with nil arguments' do
            data = user_service.update_address(nil)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it 'with invalid user id' do
          user_service.should_receive(:get_user_from_user_id).and_raise(custom_error_util::UnAuthenticatedUserError)
          data = user_service.update_address(update_address_request)
          expect(data[:response]).to eq(response_codes_util::UN_AUTHORIZED)
          end

          it 'with valid user but not his owned address' do
            USER_SESSION[:user_id] = user.id + 1
            user_service.should_receive(:get_user_from_user_id).and_return(user)
            address_service_class.any_instance.should_receive(:update_address_for_user).
              with({user: user, address: address_params}).and_raise(custom_error_util::UnAuthenticatedUserError)
            data = user_service.update_address(update_address_request)
            expect(data[:response]).to eq(response_codes_util::UN_AUTHORIZED)
          end
        end

        context "with valid params" do
          it 'with valid user and valid args and his owned address' do
            user_service.should_receive(:get_user_from_user_id).and_return(user)
            address_service_class.any_instance.should_receive(:update_address_for_user).
              with({user: user, address: address_params}).and_return(address)
            data = user_service.update_address(update_address_request)
            expect(data[:response]).to eq(response_codes_util::SUCCESS)
          end
        end
      end

      describe 'refill_referral_limit' do
        it 'with blank params' do
          response = user_service.refill_referral_limit({})
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'wrong referral_limit' do
          response = user_service.refill_referral_limit({ id: 1, referral_limit: 0 })
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'without user id' do
          response = user_service.refill_referral_limit({ referral_limit: 0 })
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with valid params' do
          UsersModule::V1::UserService.any_instance.stub(:get_user_from_user_id).and_return(user)
          response = user_service.refill_referral_limit({ referral_limit: 0 })
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end
      end

    # End of Rspec class
    end
  end
end
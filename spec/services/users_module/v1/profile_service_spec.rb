module UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::ProfileService do
      let(:version_params) { { version: 1 } }

      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_util) { CommonModule::V1::ResponseCodes }
      let(:user_service) { UsersModule::V1::UserService }
      let(:profile_service) { UsersModule::V1::ProfileService.new }
      let(:profile_dao) { UsersModule::V1::ProfileDao }
      let(:area_states) { AddressModule::V1::ModelStates::V1::AreaStates }
      let(:profile) { FactoryGirl.create(:profile) }
      let(:user) { FactoryGirl.create(:user, profile: profile) }
      let(:area) { FactoryGirl.create(:area, status: area_states::ACTIVE) }
      let(:params) {
        {
          profile: {
            pincode: Faker::Number.number(6).to_s
          }
        }
      }
      let(:invalid_params) {
        {
          profile: {
            pincode: Faker::Number.number(5).to_s
          }
        }
      }
      let(:stub_ok_response) {
        {
          message: content_util::SUCCESS
        }
      }

      describe 'add_profile' do
        it 'with valid params' do
          expect_any_instance_of(profile_dao).to receive(:get_area_from_pincode).and_return(area)
          expect_any_instance_of(user_service).to receive(:get_user_from_user_id).and_return(user)
          response = profile_service.add_pincode(params)
          expect(response[:payload][:message]).to eql(content_util::OK_RESPONSE)
        end

        it 'with unauthenticated user' do
          expect_any_instance_of(profile_dao).to receive(:get_area_from_pincode).and_return(area)
          expect_any_instance_of(user_service).to receive(:get_user_from_user_id).and_raise(
            custom_error_util::UnAuthenticatedUserError)
          response = profile_service.add_pincode(params)
          expect(response[:response]).to eql(response_util::UN_AUTHORIZED)
        end

        it 'with inactive area' do
          area = FactoryGirl.create(:area, status: area_states::INACTIVE)
          expect_any_instance_of(profile_dao).to receive(:get_area_from_pincode).and_return(area)
          response = profile_service.add_pincode(params)
          expect(response[:response]).to eql(response_util::BAD_REQUEST)
        end

        it 'with invalid pincode' do
          response = profile_service.add_pincode(invalid_params)
          expect(response[:response]).to eql(response_util::BAD_REQUEST)
        end
      end

    end
  end
end

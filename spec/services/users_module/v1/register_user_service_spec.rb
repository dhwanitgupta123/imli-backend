module UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::RegisterUserService do
      let(:version) { 1 }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:provider_type_util) { UsersModule::V1::ProviderType }
      let(:register_service) { UsersModule::V1::RegisterUserService.new(version) }

      let(:user_params) { {:phone_number => Faker::Number.number(10).to_s, :first_name => Faker::Lorem.characters(7)} }
      let(:user_params_password) { {:phone_number => Faker::Number.number(10).to_s, :password => Faker::Lorem.characters(7)} }
      let(:user_params_otp) { {:phone_number => Faker::Number.number(10).to_s, :otp => Faker::Number.number(4) } } 
      let(:user_params_without_otp_password) { {:phone_number => Faker::Number.number(10).to_s} }
      let(:user_params_without_phone_number) { {:first_name => Faker::Lorem.characters(7)} }
      let(:request_type_sms) { provider_type_util::SMS }
      let(:request_type_generic) { provider_type_util::GENERIC }
      let(:message) { FactoryGirl.build_stubbed(:message) }
      let(:provider) { FactoryGirl.build_stubbed(:provider, message: message) }
      let(:user) { FactoryGirl.build(:user, provider: provider) }


      it "with blank request from user" do
        user_params = {}
        expect{register_service.register_user(user_params, request_type_generic)}.to raise_error(
            custom_error_util::InsufficientDataError)
      end

      it "with no phone_number" do 
        expect{register_service.register_user(user_params_without_phone_number, request_type_generic)}.to raise_error(
            custom_error_util::InsufficientDataError)
      end

      it "with correct request" do
        expect{register_service.register_user(user_params, request_type_sms)}.to_not raise_error
      end

      it "with correct request for generic type" do
        expect{register_service.register_user(user_params_password, request_type_generic)}.to_not raise_error
      end

      it "with no password for generic type" do
        expect{register_service.register_user(user_params_without_otp_password, request_type_generic)}.to raise_error(
          custom_error_util::InsufficientDataError)
      end

      it "with no phone_number for generic type" do
        expect{register_service.register_user(user_params_without_phone_number, request_type_generic)}.to raise_error(
          custom_error_util::InsufficientDataError)
      end
      
      it "when update expires_at succeeds" do
        register_service.register_user(user_params, request_type_sms)
        expect{ register_service.register_user(user_params, request_type_sms) }.to_not raise_error
      end

      it "when update sms provider succeeds" do
        register_service.register_user(user_params_password, request_type_generic)
        expect{ register_service.register_user(user_params, request_type_sms) }.to_not raise_error
      end

      it "when new generic provider succeeds" do
        register_service.register_user(user_params, request_type_sms)
        expect{ register_service.register_user(user_params_password, request_type_generic) }.to_not raise_error
      end
    end
  end
end
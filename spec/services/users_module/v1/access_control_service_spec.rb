module UsersModule
  module V1
    require 'rails_helper'

    RSpec.describe UsersModule::V1::AccessControlService do
      let(:access_control_service_class){UsersModule::V1::AccessControlService }
      let(:user_service_class) { UsersModule::V1::UserService }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:validation_map) { ValidationsModule::V1::ValidationMap }
      let(:active_user_validator) { ValidationsModule::V1::ActiveUserValidator }

      let(:general_helper) {CommonModule::V1::GeneralHelper}
      let(:cache) {CommonModule::V1::Cache}

      let(:unauthenticated_user_response){response_codes_util::UN_AUTHORIZED}
      let(:unauthorized_user_response){response_codes_util::FORBIDDEN}

      let(:dummy_session_token) { Faker::Number.number(10).to_s }
      let(:dummy_controller) { "users" }
      let(:dummy_action) { "index" }
      let(:public_api_action) { "public-api" }

      context "request received with invalid" do
        let(:bad_request_response){response_codes_util::BAD_REQUEST}


        it "action" do
          params_with_invalid_action = {:session_token => dummy_session_token, :controller => dummy_controller, :action => ""}
          access_control_service = access_control_service_class.new(params_with_invalid_action)
          result = access_control_service.has_access?
          expect(result[:ok] ).to be false
          expect( result[:data][:response]).to eq(bad_request_response)
        end

        it "controller" do
          params_with_invalid_controller = {:session_token => dummy_session_token, :controller => "", :action => dummy_action}
          access_control_service = access_control_service_class.new(params_with_invalid_controller)
          result = access_control_service.has_access?
          expect(result[:ok] ).to be false
          expect( result[:data][:response]).to eq(bad_request_response)
        end

        it "authentication token" do
          params_with_invalid_userid = {:session_token => "", :controller => dummy_controller, :action => dummy_action}
          access_control_service = access_control_service_class.new(params_with_invalid_userid)
          result = access_control_service.has_access?
          expect(result[:ok] ).to be false
          expect( result[:data][:response]).to eq(unauthenticated_user_response)
        end
      end

      context "request received" do
        let(:valid_params) { {:session_token => dummy_session_token, :controller => dummy_controller, :action => dummy_action} }
        let(:access_control_service) { access_control_service_class.new(valid_params) }
        let(:dummy_user_id) { 123 }
        let(:valid_user_id) { 12345 }
        # create dummy valid authorized user
        let(:valid_role) { FactoryGirl.build(:role, :permissions => [1]) }
        let(:valid_user) { FactoryGirl.build(:user, :roles => [valid_role]) }
        # create dummy invalid user with not sufficient permissions/roles
        let(:invalid_role) { FactoryGirl.build(:role, :permissions => []) }
        let(:invalid_user) { FactoryGirl.build(:user, :roles => [invalid_role]) }
        # create dummy user with no roles assigned to him
        let(:user_with_no_roles) { FactoryGirl.build(:user)}
        let(:phone_number) {Faker::Number.number(10).to_s}
        let(:ttl) {Faker::Number.number(5).to_s}


        # Write temporary cache for testing
        let(:dummy_permission_lookup) {
          {
          "0"=>{"label"=>"public-api", "access_to"=>[ {"api" => dummy_controller + "#" + public_api_action, "validations" => [] }]},
          "1"=>{"label"=>"dummy-permission", "access_to"=>[ {"api" => dummy_controller + "#" + dummy_action, "validations" => []}]}
          }
        }
        #Write dummy_permission_lookup in Rails Cache
        # Function called everytime before each example/'it' block
        before(:example) {
          cache.stub(:read_json).with('PERMISSIONS_LOOKUP').and_return(dummy_permission_lookup)
        }

        it "with public api access" do
          params_with_public_access = {:session_token => "", :controller => dummy_controller, :action => public_api_action}
          access_control_service = access_control_service_class.new(params_with_public_access)
          result = access_control_service.has_access?
          expect(result[:ok] ).to be true
        end

        it "when database has no provider associated with provided token" do
          user_service_class.any_instance.should_receive(:get_user_id_from_session_token).with(valid_params[:session_token]) { raise custom_error_util::UnAuthenticatedUserError}

          result = access_control_service.has_access?
          expect(result[:ok] ).to be false
          expect( result[:data][:response]).to eq(unauthenticated_user_response)
        end

        it "when database has no user associated with provided token" do
          user_service_class.any_instance.should_receive(:get_user_id_from_session_token).with(valid_params[:session_token]).and_return(dummy_user_id)
          user_service_class.any_instance.should_receive(:get_user_from_user_id).with(dummy_user_id) { raise custom_error_util::UnAuthenticatedUserError}

          result = access_control_service.has_access?
          expect(result[:ok] ).to be false
          expect( result[:data][:response]).to eq(unauthenticated_user_response)
        end

        it "when user has no associated roles" do
          user_service_class.any_instance.should_receive(:get_user_id_from_session_token).with(valid_params[:session_token]).and_return(valid_user_id)
          user_service_class.any_instance.should_receive(:get_user_from_user_id).with(valid_user_id).and_return(user_with_no_roles)

          result = access_control_service.has_access?
          expect(result[:ok] ).to be false
          expect( result[:data][:response]).to eq(unauthorized_user_response)
        end

        it "when user has invalid access role or permissions" do
          user_service_class.any_instance.should_receive(:get_user_id_from_session_token).with(valid_params[:session_token]).and_return(valid_user_id)
          user_service_class.any_instance.should_receive(:get_user_from_user_id).with(valid_user_id).and_return(invalid_user)

          result = access_control_service.has_access?
          expect(result[:ok] ).to be false
          expect( result[:data][:response]).to eq(unauthorized_user_response)
        end

        it "when user has valid access role or authorized permissions" do
          user_service_class.any_instance.should_receive(:get_user_id_from_session_token).with(valid_params[:session_token]).and_return(valid_user_id)
          user_service_class.any_instance.should_receive(:get_user_from_user_id).with(valid_user_id).and_return(valid_user)

          result = access_control_service.has_access?
          expect(result[:ok] ).to be true
        end

        it "when user phone number is blocked" do
          general_helper.stub(:get_blocked_user_key).and_return(phone_number)
          cache.stub(:read).and_return(ttl)
          response = access_control_service.is_user_blocked?
          expect(response[:ok]).to eq(true)
        end

        it "when user phone number is not blocked" do
          general_helper.stub(:get_blocked_user_key).and_return(phone_number)
          cache.stub(:read).and_return(nil)
          response = access_control_service.is_user_blocked?
          expect(response[:ok]).to eq(false)
        end

        it 'with no validations' do
          params_with_public_access = {:session_token => "", :controller => dummy_controller, :action => public_api_action}
          access_control_service = access_control_service_class.new(params_with_public_access)
          access_control_service.has_access?
          response = access_control_service.apply_validations
          expect(response[:ok]).to eq(true)
        end

        it 'with failed validations' do
          permission_lookup = 
          {
            "0"=>{"label"=>"public-api", "access_to"=>[ {"api" => dummy_controller + "#" + public_api_action, "validations" => [1] }]}
          }
          cache.stub(:read_json).with('PERMISSIONS_LOOKUP').and_return(permission_lookup)
          expect(validation_map).to receive(:get_validator_by_validation).and_return(active_user_validator)
          expect_any_instance_of(active_user_validator).to receive(:validate).and_return({result: false, message: 'Inactive User'})
          params_with_public_access = {:session_token => "", :controller => dummy_controller, :action => public_api_action}
          access_control_service = access_control_service_class.new(params_with_public_access)
          access_control_service.has_access?
          response = access_control_service.apply_validations
          expect(response[:ok]).to eq(false)
        end

        it 'with passed validations' do
          permission_lookup = 
          {
            "0"=>{"label"=>"public-api", "access_to"=>[ {"api" => dummy_controller + "#" + public_api_action, "validations" => [1] }]}
          }
          cache.stub(:read_json).with('PERMISSIONS_LOOKUP').and_return(permission_lookup)
          expect(validation_map).to receive(:get_validator_by_validation).and_return(active_user_validator)
          expect_any_instance_of(active_user_validator).to receive(:validate).and_return({result: true})
          params_with_public_access = {:session_token => "", :controller => dummy_controller, :action => public_api_action}
          access_control_service = access_control_service_class.new(params_with_public_access)
          access_control_service.has_access?
          response = access_control_service.apply_validations
          expect(response[:ok]).to eq(true)
        end

      end
    end
  end
end

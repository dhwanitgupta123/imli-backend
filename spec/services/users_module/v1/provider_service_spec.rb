module UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::ProviderService do
      let(:version) { 1 }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:provider_type_util) { UsersModule::V1::ProviderType }
      let(:otp_password_helper) { UsersModule::V1::OneTimePasswordHelper }
      let(:provider_service) { UsersModule::V1::ProviderService.new(version) }
      let(:generic_provider_service) { UsersModule::V1::ProvidersModule::V1::GenericProviderService }

      let(:user_params) { {:phone_number => Faker::Number.number(10).to_s, :first_name => Faker::Lorem.characters(7)} }
      let(:user_params_password) { {:phone_number => Faker::Number.number(10).to_s, :password => Faker::Lorem.characters(7)} }
      let(:user_params_otp) { {:phone_number => Faker::Number.number(10).to_s, :otp => Faker::Number.number(4) } } 
      let(:user_params_without_otp_password) { {:phone_number => Faker::Number.number(10).to_s} }
      let(:request_type_sms) { provider_type_util::SMS }
      let(:request_type_generic) { provider_type_util::GENERIC }
      let(:message) { FactoryGirl.build_stubbed(:message) }
      let(:generic) { FactoryGirl.build_stubbed(:generic) }
      let(:provider) { FactoryGirl.build_stubbed(:provider, message: message) }
      let(:user) { FactoryGirl.build(:user, provider: provider) }

      describe "new_provider " do
        it "creates new generic provider successfully" do
          expect{ provider_service.new_provider(user_params_password, request_type_generic)}.to_not raise_error
        end
        it "if password not present" do
          expect{ provider_service.new_provider(user_params_without_otp_password, request_type_generic)}.to raise_error(custom_error_util::InsufficientDataError)
        end
        it "creates new message provider successfully" do
          expect{ provider_service.new_provider(user_params, request_type_sms)}.to_not raise_error
        end
      end 

      describe "update provider type" do
        it "when request type is not in range" do
          args = { :user_params => user_params_otp, :request_type => 6, :provider => provider }
          expect{ provider_service.update_provider_type(args) }.to raise_error(
          custom_error_util::InvalidDataError)
        end
        it "when again signs up with generic" do
          provider = FactoryGirl.build_stubbed(:provider, generic: FactoryGirl.build_stubbed(:generic))
          args = { :user_params => user_params_password, :request_type => request_type_generic, :provider => provider }
          expect{ provider_service.update_provider_type(args) }.to raise_error(
          custom_error_util::ExistingUserError)
        end 
      end

      describe "check provider" do
        it "when otp/password is missing" do
          args = { :user_params => user_params_without_otp_password, :request_type => request_type_generic, :provider => provider }
          expect{ provider_service.check_provider(args) }.to raise_error(
          custom_error_util::InsufficientDataError)
        end
        it "when request type is not in range" do
          args = { :user_params => user_params_otp, :request_type => 6, :provider => provider }
          expect{ provider_service.check_provider(args) }.to raise_error(
          custom_error_util::InvalidDataError)
        end
        it "user hasn't register for requested provider" do
          provider = FactoryGirl.build_stubbed(:provider, message: FactoryGirl.build_stubbed(:message), generic: nil)
          args = { :user_params => user_params_password, :request_type => request_type_generic, :provider => provider }
          expect{ provider_service.check_provider(args) }.to raise_error(
          custom_error_util::ResourceNotFoundError)
        end
        it "user has given wrong password" do
          provider = FactoryGirl.build_stubbed(:provider, generic: FactoryGirl.build_stubbed(:generic))
          args = { :user_params => user_params_password, :request_type => request_type_generic, :provider => provider }
          expect{ provider_service.check_provider(args) }.to raise_error(
          custom_error_util::WrongPasswordError)
        end
        it "user has given wrong otp" do
          otp_password_helper.stub(:verify_otp).and_raise(custom_error_util::WrongOtpError)
          args = { :user_params => user_params_otp, :request_type => request_type_sms, :provider => provider }
          expect{ provider_service.check_provider(args) }.to raise_error(
          custom_error_util::WrongOtpError)
        end
        it "user otp has expired" do
          otp_password_helper.stub(:verify_otp).and_raise(custom_error_util::AuthenticationTimeoutError)
          args = { :user_params => user_params_otp, :request_type => request_type_sms, :provider => provider }
          expect{ provider_service.check_provider(args) }.to raise_error(
          custom_error_util::AuthenticationTimeoutError)
        end
        it "session token is not present otp login" do
          provider = FactoryGirl.build_stubbed(:provider, message: message, session_token: nil)
          UsersModule::V1::ProviderService.any_instance.stub(:login_with_otp).and_return(true)
          args = { :user_params => user_params_otp, :request_type => request_type_sms, :provider => provider }
          expect{ provider_service.check_provider(args) }.to raise_error(
            custom_error_util::RunTimeError)
        end
        it "session token is not present password login" do
          provider = FactoryGirl.build_stubbed(:provider, generic: generic, session_token: nil)
          UsersModule::V1::ProviderService.any_instance.stub(:login_with_password).and_return(true)
          args = { :user_params => user_params_password, :request_type => request_type_generic, :provider => provider }
          expect{ provider_service.check_provider(args) }.to raise_error(
            custom_error_util::RunTimeError)
        end
      end

      describe "resend_otp" do
        it "provider doesn't exists" do
          provider = FactoryGirl.build_stubbed(:provider, message: nil)
          expect{ provider_service.resend_otp(provider) }.to raise_error(custom_error_util::RunTimeError)
        end
      end

      describe "initiate reset password process" do
        it "when password was never created before (generic provider missing)" do
          provider = FactoryGirl.build_stubbed(:provider, message: FactoryGirl.build_stubbed(:message), generic: nil)
          args = { :provider => provider }
          expect{ provider_service.initiate_reset_password_process(args) }.to raise_error(
          custom_error_util::PreConditionRequiredError )
        end
        it "when generic provider present" do
          generic = FactoryGirl.build_stubbed(:generic)
          provider = FactoryGirl.build(:provider, generic: generic)
          args = { :provider => provider }
          generic_provider_service.any_instance.should_receive(:set_reset_flag).with(generic, true).and_return(generic)
          expect{ provider_service.initiate_reset_password_process(args) }.to_not raise_error
        end
      end

      describe "create password" do
        let(:password) { Faker::Lorem.characters(6) }

        it "when provider not present" do
          args = { :provider => nil, :password => password }
          expect{ provider_service.create_password(args) }.to raise_error(
          custom_error_util::PreConditionRequiredError )
        end

        it "when provider does not have generic beforehand" do
          provider = FactoryGirl.build(:provider, message: FactoryGirl.build_stubbed(:message), generic: nil)
          args = { :provider => provider, :password => password }
          generic_provider_service.any_instance.should_receive(:new_generic_provider).with(password).and_return(generic)
          expect{ provider_service.create_password(args) }.to_not raise_error
        end

        it "when provider have generic (password already set)" do
          provider = FactoryGirl.build(:provider, generic: generic)
          args = { :provider => provider, :password => password }
          generic_provider_service.any_instance.should_receive(:reset_password).with(generic, password).and_return(generic)
          expect{ provider_service.create_password(args) }.to_not raise_error
        end
      end

    end #End of class
  end
end

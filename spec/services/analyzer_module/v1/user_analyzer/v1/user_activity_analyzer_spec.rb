module AnalyzerModule
  module V1
    module UserAnalyzer
      require 'rails_helper'
      RSpec.describe AnalyzerModule::V1::UserAnalyzer::V1::UserActivityAnalyzer do

        let(:user_activity_analyzer) { AnalyzerModule::V1::UserAnalyzer::V1::UserActivityAnalyzer.new }
        let(:log) { FactoryGirl.build_stubbed(:activity_log) }
        let(:cache_util) {CommonModule::V1::Cache}
        let(:content_util) {CommonModule::V1::Content}
        let(:custom_errors_util) {CommonModule::V1::CustomErrors}
        let(:general_helper) {CommonModule::V1::GeneralHelper}
        let(:valid_error) {content_util::AUTHENTICATION_TIMEOUT}
        let(:invalid_error) {content_util::OK_RESPONSE}
        let(:activity_log) {LoggingModule::V1::ActivityLog}
        let(:key) {cache_util.read('BLOCKED_PREFIX') + Faker::Number.number(10).to_s}
        let(:phone_number) {Faker::Number.number(10).to_s}
        
        context "Error" do
          it "in valid error array" do
            response = user_activity_analyzer.check_for_valid_errors(valid_error)
            expect(response).to eq(true)
          end

          it "not in valid error array" do
            response = user_activity_analyzer.check_for_valid_errors(invalid_error)
            expect(response).to eq(false)
          end
        end

        context "User with " do
          it "consicutive last three invalid activities" do
            activities = (1..5).collect {FactoryGirl.build(:activity_log, phone_number: phone_number)}
            response = user_activity_analyzer.has_previous_invalid_actvities?(activities)
            expect(response).to eq(true)
          end

          it "valid last actvities" do
            activities = (1..5).collect {FactoryGirl.build(:activity_log, response_error: invalid_error)}
            response = user_activity_analyzer.has_previous_invalid_actvities?(activities)
            expect(response).to eq(false)
          end
        end

        it "with nil params to model" do
          activity_log.stub(:activities_in_last_x_time_with_api_name_by_order).and_raise(custom_errors_util::InsufficientDataError)
          expect(user_activity_analyzer.analyze(phone_number)).to eq(false)
        end
      end
    end
  end
end
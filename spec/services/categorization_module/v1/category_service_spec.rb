module CategorizationModule
  module V1
    require "rails_helper"
    RSpec.describe CategoryService do
      let(:version) { 1 }
      let(:category_service) { CategorizationModule::V1::CategoryService.new(version) }
      let(:department_service) { CategorizationModule::V1::DepartmentService }
      let(:category_model) {CategorizationModule::V1::Category}
      let(:department_model) {CategorizationModule::V1::Department}
      let(:category_params) { { "id": Faker::Number.number(1), "label": Faker::Name.first_name, "department_id": Faker::Number.number(1), 'priority': 1 } }
      let(:sub_category_params){ { "id": Faker::Number.number(1), "label": Faker::Name.first_name, "category_id": Faker::Number.number(1), 'priority': 1 } }
      let(:department) {FactoryGirl.create(:department)}
      let(:category) { FactoryGirl.create(:category, department: department)}
      let(:sub_category) { FactoryGirl.create(:category, parent_category: category)}
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      
      describe "create_category" do
        it "with correct resquest" do          
          department_model.stub(:find_by_department_id).and_return(department)
          response = category_service.create_category(category_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it "with wrong request" do
          department_model.stub(:find_by_department_id).and_return(department)
          response = category_service.create_category(category_params.merge(label: nil))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it "with wrong request" do
          department_model.stub(:find_by_department_id).and_return(department)
          response = category_service.create_category(category_params.merge(priority: nil))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it "when department doesn't exists" do
          response = category_service.create_category(category_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe "create_sub_category" do
        it "with correct resquest" do          
          category_model.stub(:find_by_category_id).and_return(category)
          response = category_service.create_sub_category(sub_category_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it "with wrong request" do
          sub_category_params = {"label": "","category_id": Faker::Number.number(1) }
          category_model.stub(:find_by_category_id).and_return(category)
          response = category_service.create_sub_category(sub_category_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it "when category doesn't exists" do
          response = category_service.create_category(sub_category_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe "get_category" do
        it "with correct resquest" do
          id = Faker::Number.number(1)
          category_model.stub(:find_by_category_id).and_return(category)
          response = category_service.get_category(id)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it "when category doesn't exists" do
          id = Faker::Number.number(1)
          response = category_service.get_category(id)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe "get_all_categories" do
        let(:paginated_params) { { state: 1, per_page: 20, page_no: 1, sort_by: 'label', order: 'ASC' } }
        it "with correct request" do
          response = category_service.get_all_categories(paginated_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end

        it "with wrong request" do
          response = category_service.get_all_categories(paginated_params.merge({ order: 'xyz' }))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe 'update_category' do
        it 'with correct request' do
          category_model.stub(:find_by_category_id).and_return(category)
          department_model.stub(:find_by_department_id).and_return(department)
          category_model.any_instance.stub(:update_category).and_return(category)
          response = category_service.update_category(category_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it 'when department doesnt exists' do
          category_model.stub(:find_by_category_id).and_return(category)
          expect_any_instance_of(department_service).to receive(:get_department_by_id).
            and_raise(custom_error_util::DepartmentNotFoundError.new)
          response = category_service.update_category(category_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it 'when category doesnt exists' do
          category_model.stub(:find_by_category_id).and_raise(custom_error_util::CategoryNotFoundError)
          response = category_service.update_category(category_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe 'change_state' do
        let(:params) { { "id": Faker::Number.number(1), "event": 1 } }
        let(:inavlid_params) { { "id": Faker::Number.number(1), "event": 10 } }
        it 'with correct request' do
          category_model.stub(:find_by_category_id).and_return(category)
          response = category_service.change_state(params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it 'when category doesnt exists' do
          category_model.stub(:find_by_category_id).and_raise(custom_error_util::CategoryNotFoundError)
          response = category_service.change_state(params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it 'when event doesnt exists' do
          category_model.stub(:find_by_category_id).and_return(category)
          response = category_service.change_state(inavlid_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it 'when transition not allowed' do
          category_model.stub(:find_by_category_id).and_return(category)
          category_model.any_instance.stub(:trigger_event).and_raise(custom_error_util::PreConditionRequiredError)
          response = category_service.change_state(params)
          expect(response[:response]).to eq(response_codes::PRE_CONDITION_REQUIRED)
        end
      end

      describe "get_all_sub_categories" do
        let(:paginated_params) { { state: 1, per_page: 20, page_no: 1, sort_by: 'label', order: 'ASC' } }
        it "with correct request" do
          response = category_service.get_all_sub_categories(paginated_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end

        it "with wrong request" do
          response = category_service.get_all_sub_categories(paginated_params.merge({ order: 'xyz' }))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end
    end
  end
end

module CategorizationModule
  module V1
    require 'rails_helper'
    RSpec.describe DepartmentService do
      let(:version) { 1 }
      let(:department_service) { CategorizationModule::V1::DepartmentService.new(version) }
      let(:department_model) {CategorizationModule::V1::Department}
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:department_params) { {'id': Faker::Number.number(1), 'label': Faker::Name.first_name , 'priority': 1 } }
      let(:department) { FactoryGirl.create(:department)}
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:image_service) { ImageServiceModule::V1::ImageService }
      
      describe 'create_department' do
        it 'with correct resquest' do
          expect(department_model).to receive(:create_department).and_return(department)
          response = department_service.create_department(department_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it 'with wrong request' do
          response = department_service.create_department(department_params.merge(label: nil))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it 'with wrong request' do
          response = department_service.create_department(department_params.merge(priority: nil))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe 'get_department' do
        it 'with correct resquest' do
          id = Faker::Number.number(1)
          department_model.stub(:find_by_department_id).and_return(department)
          response = department_service.get_department(id)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it 'when department doesnt exists' do
          id = Faker::Number.number(1)
          response = department_service.get_department(id)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe 'get_all_departments' do
        let(:paginated_params) { { state: 1, per_page: 20, page_no: 1, sort_by: 'label' } }
        it 'with correct request' do
          response = department_service.get_all_departments(paginated_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end

        it 'with wrong request' do
          response = department_service.get_all_departments(paginated_params.merge({ order: 'xyz' }))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe 'update_department' do
        it 'with correct resquest' do
          department_model.stub(:find_by_department_id).and_return(department)
          expect(department_model).to receive(:update_department).and_return(department)
          response = department_service.update_department(department_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it 'when department doesnt exists' do
          department_model.stub(:find_by_department_id).and_raise(custom_error_util::DepartmentNotFoundError)
          response = department_service.update_department(department_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe 'change_state' do
        let(:params) { { "id": Faker::Number.number(1), "event": 1 } }
        let(:inavlid_params) { { "id": Faker::Number.number(1), "event": 10 } }
        it 'with correct request' do
          department_model.stub(:find_by_department_id).and_return(department)
          response = department_service.change_state(params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it 'when department doesnt exists' do
          department_model.stub(:find_by_department_id).and_raise(custom_error_util::DepartmentNotFoundError)
          response = department_service.change_state(params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it 'when event doesnt exists' do
          department_model.stub(:find_by_department_id).and_return(department)
          response = department_service.change_state(inavlid_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it 'when transition not allowed' do
          department_model.stub(:find_by_department_id).and_return(department)
          department_model.any_instance.stub(:trigger_event).and_raise(custom_error_util::PreConditionRequiredError)
          response = department_service.change_state(params)
          expect(response[:response]).to eq(response_codes::PRE_CONDITION_REQUIRED)
        end
      end

      describe 'get_all_departments_categories_and_sub_categories' do
        it 'with correct request' do
          department = FactoryGirl.create(:department)
          category = FactoryGirl.create(:category, department: department)
          sub_category = FactoryGirl.create(:category, department: nil, parent_category: category)
          response = department_service.get_all_departments_categories_and_sub_categories
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

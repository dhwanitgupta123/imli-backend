module CategorizationModule
  module V1
    require 'rails_helper'
    RSpec.describe CategorizationModule::V1::MpspCategorizationApiService do
      let(:version) { 1 }
      let(:mpsp_categorization_api_service) { CategorizationModule::V1::MpspCategorizationApiService.new(version) }
      let(:response_codes) { CommonModule::V1::ResponseCodes }

      describe 'get_all_mpsp_departments_details which are active' do
        let(:paginated_params) { { sort_by: 'label' } }
        it 'with correct request' do
          response = mpsp_categorization_api_service.get_all_mpsp_departments_details(paginated_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end

        it 'with wrong request' do
          response = mpsp_categorization_api_service.get_all_mpsp_departments_details(paginated_params.merge({ order: 'xyz' }))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe 'get_products_by_mpsp_category' do
        it 'with active entities ' do 
          mpsp_category = FactoryGirl.create(:mpsp_category, status: 1)
          mpsp_sub_category = FactoryGirl.create(:mpsp_category, status: 1, mpsp_department: nil, mpsp_parent_category: mpsp_category)
          marketplace_selling_pack = FactoryGirl.create(:marketplace_selling_pack, status: 1, mpsp_sub_category: mpsp_sub_category)
          response = mpsp_categorization_api_service.get_products_by_mpsp_category({ id: mpsp_category.id })
          expect(response[:payload][:products].blank?).to eq(false) 
        end
        
        it 'with inactive entities ' do 
          mpsp_category = FactoryGirl.create(:mpsp_category, status: 1)
          mpsp_sub_category = FactoryGirl.create(:mpsp_category, mpsp_department: nil, mpsp_parent_category: mpsp_category)
          marketplace_selling_pack = FactoryGirl.create(:marketplace_selling_pack, status: 1, mpsp_sub_category: mpsp_sub_category)
          response = mpsp_categorization_api_service.get_products_by_mpsp_category({ id: mpsp_category.id })
          expect(response[:payload][:products].blank?).to eq(true) 
        end
      end
    end
  end
end

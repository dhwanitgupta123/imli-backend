module CategorizationModule
  module V1
    require 'rails_helper'
    RSpec.describe CategorizationModule::V1::MpspDepartmentService do
      let(:version) { 1 }
      let(:mpsp_department_service) { CategorizationModule::V1::MpspDepartmentService.new(version) }
      let(:mpsp_department_dao) { CategorizationModule::V1::MpspDepartmentDao }
      let(:mpsp_department_model) {CategorizationModule::V1::MpspDepartment}
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:mpsp_department_params) { {'id': Faker::Number.number(1), 'label': Faker::Name.first_name , 'priority': 1 } }
      let(:mpsp_department) { FactoryGirl.create(:mpsp_department)}
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:image_service) { ImageServiceModule::V1::ImageService }
      
      describe 'create_mpsp_department' do
        it 'with correct resquest' do
          expect(mpsp_department_model).to receive(:create_mpsp_department).and_return(mpsp_department)
          response = mpsp_department_service.create_mpsp_department(mpsp_department_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it 'with wrong request' do
          response = mpsp_department_service.create_mpsp_department(mpsp_department_params.merge(label: nil))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it 'with wrong request' do
          response = mpsp_department_service.create_mpsp_department(mpsp_department_params.merge(priority: nil))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe 'get_mpsp_department' do
        it 'with correct resquest' do
          id = Faker::Number.number(1)
          mpsp_department_model.stub(:find_by_mpsp_department_id).and_return(mpsp_department)
          response = mpsp_department_service.get_mpsp_department(id)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it 'when mpsp_department doesnt exists' do
          id = Faker::Number.number(1)
          response = mpsp_department_service.get_mpsp_department(id)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe 'get_all_mpsp_departments' do
        let(:paginated_params) { { state: 1, per_page: 20, page_no: 1, sort_by: 'label' } }
        it 'with correct request' do
          response = mpsp_department_service.get_all_mpsp_departments(paginated_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end

        it 'with wrong request' do
          response = mpsp_department_service.get_all_mpsp_departments(paginated_params.merge({ order: 'xyz' }))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe 'update_mpsp_department' do
        it 'with correct resquest' do
          mpsp_department_model.stub(:find_by_mpsp_department_id).and_return(mpsp_department)
          expect(mpsp_department_model).to receive(:update_mpsp_department).and_return(mpsp_department)
          response = mpsp_department_service.update_mpsp_department(mpsp_department_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it 'when mpsp_department doesnt exists' do
          mpsp_department_model.stub(:find_by_mpsp_department_id).and_raise(custom_error_util::DepartmentNotFoundError)
          response = mpsp_department_service.update_mpsp_department(mpsp_department_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe 'change_state' do
        let(:params) { { "id": Faker::Number.number(1), "event": 1 } }
        let(:inavlid_params) { { "id": Faker::Number.number(1), "event": 10 } }
        it 'with correct request' do
          expect(mpsp_department_dao).to receive(:activate_products_within_department).and_return(true)
          mpsp_department_model.stub(:find_by_mpsp_department_id).and_return(mpsp_department)
          response = mpsp_department_service.change_state(params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it 'when mpsp_department doesnt exists' do
          mpsp_department_model.stub(:find_by_mpsp_department_id).and_raise(custom_error_util::DepartmentNotFoundError)
          response = mpsp_department_service.change_state(params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it 'when event doesnt exists' do
          mpsp_department_model.stub(:find_by_mpsp_department_id).and_return(mpsp_department)
          response = mpsp_department_service.change_state(inavlid_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it 'when transition not allowed' do
          mpsp_department_model.stub(:find_by_mpsp_department_id).and_return(mpsp_department)
          mpsp_department_model.any_instance.stub(:trigger_event).and_raise(custom_error_util::PreConditionRequiredError)
          response = mpsp_department_service.change_state(params)
          expect(response[:response]).to eq(response_codes::PRE_CONDITION_REQUIRED)
        end
      end

      describe 'get_all_mpsp_departments_categories_and_sub_categories' do
        it 'with correct request' do
          mpsp_department = FactoryGirl.create(:mpsp_department)
          mpsp_category = FactoryGirl.create(:mpsp_category, mpsp_department: mpsp_department)
          mpsp_sub_category = FactoryGirl.create(:mpsp_category, mpsp_department: nil, mpsp_parent_category: mpsp_category)
          response = mpsp_department_service.get_all_mpsp_departments_categories_and_sub_categories
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

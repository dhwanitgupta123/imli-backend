module CategorizationModule
  module V1
    require 'rails_helper'

    RSpec.describe DepartmentsController, type: :routing do
      describe 'routing' do
        it 'routes to #create_department' do
          expect(:post => 'departments/new').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/departments', 
             'action'=>'create_department'})
        end

        it 'routes to #get_department' do
          expect(:get => 'departments/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/departments', 
             'action'=>'get_department',
             'id'=>'1'})
        end

        it 'routes to #get_all_departments' do
          expect(:get => 'departments').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/departments', 
             'action'=>'get_all_departments'})
        end

        it 'routes to #update_department' do
          expect(:put => 'departments/update/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/departments', 
             'action'=>'update_department',
             'id'=>'1'})
        end

        it 'routes to #change_state' do
          expect(:put => 'departments/state/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/departments', 
             'action'=>'change_state',
             'id'=>'1'})
        end
      end
    end
  end
end

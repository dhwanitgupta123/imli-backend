module CategorizationModule
  module V1
    require 'rails_helper'

    RSpec.describe CategorizationModule::V1::MpspDepartmentsController, type: :routing do
      describe 'routing' do
        it 'routes to #create_mpsp_department' do
          expect(:post => 'mpsp/departments/new').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_departments', 
             'action'=>'create_mpsp_department'})
        end

        it 'routes to #get_mpsp_department' do
          expect(:get => 'mpsp/departments/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_departments', 
             'action'=>'get_mpsp_department',
             'id'=>'1'})
        end

        it 'routes to #get_all_mpsp_departments' do
          expect(:get => 'mpsp/departments').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_departments', 
             'action'=>'get_all_mpsp_departments'})
        end

        it 'routes to #update_mpsp_department' do
          expect(:put => 'mpsp/departments/update/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_departments', 
             'action'=>'update_mpsp_department',
             'id'=>'1'})
        end

        it 'routes to #change_state' do
          expect(:put => 'mpsp/departments/state/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_departments', 
             'action'=>'change_state',
             'id'=>'1'})
        end

        it 'routes to #get_all_mpsp_departments_categories_and_sub_categories' do
          expect(:get => 'mpsp/get_all_departments_categories_and_sub_categories').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_departments', 
             'action'=>'get_all_mpsp_departments_categories_and_sub_categories'})
        end
      end
    end
  end
end

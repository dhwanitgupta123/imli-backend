module UsersModule
  module V1
    require "rails_helper"
    # Routing specs for Membership Controller
    RSpec.describe UsersModule::V1::MembershipsApiController, type: :routing do
      describe "routing" do

        it "routes to #subscribe_to_membership" do
          expect(:post => "api/membership/subscribe").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/memberships_api", 
             "action"=>"subscribe_to_membership"
             } )
        end

        it "routes to #payment_initiate" do
          expect(:post => "api/membership/payment/initiate").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/memberships_api", 
             "action"=>"payment_initiate"
             } )
        end

        it "routes to #payment_success" do
          expect(:post => "api/membership/payment/success").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/memberships_api", 
             "action"=>"payment_success"
             } )
        end
      end
    end
  end
end

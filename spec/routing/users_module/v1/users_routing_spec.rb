module UsersModule
  module V1
    require "rails_helper"
    # Routing specs for Users Controller
    RSpec.describe UsersModule::V1::UsersController, type: :routing do
      describe "routing" do

        it "routes to #add_role_to_user" do
          expect(:post => "users/add_role").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/users", 
             "action"=>"add_role_to_user"
             } )
        end

        it "routes to #remove_role_from_user" do
          expect(:post => "users/remove_role").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/users", 
             "action"=>"remove_role_from_user"
             } )
        end

        it "routes to users#get_all_users" do
          expect(:get => "/users").to route_to(
            {"format"=>"json", 
             "controller"=>"users_module/v1/users", 
             "action"=>"get_all_users"})
        end

        it "routes to users#change_state" do
          expect(:put => "/users/state/1").to route_to(
            {"format"=>"json", 
             "controller"=>"users_module/v1/users", 
             "action"=>"change_state",
             "id" => "1" })
        end

        it "routes to users#refill_referral_limit" do
          expect(:put => "/users/referral_limit/1").to route_to(
            {"format"=>"json", 
             "controller"=>"users_module/v1/users", 
             "action"=>"refill_referral_limit",
             "id" => "1" })
        end

      end
    end
  end
end
module UsersModule
  module V1
    require 'rails_helper'
    # Routing specs for Users api Controller
    RSpec.describe UsersModule::V1::UsersApiController, type: :routing do
      describe 'routing' do

        it 'routes to #decide_register_login' do
        expect(:post => 'take_my_phone_number').to route_to(
        {
          'format'=>'json', 
          'controller'=>'users_module/v1/users_api', 
          'action'=>'decide_register_login'
        })
        end

        it 'routes to #login' do
        expect(:post => 'login').to route_to(
        {
          'format'=>'json', 
          'controller'=>'users_module/v1/users_api', 
          'action'=>'login'
        })
        end

        it 'routes to #resend_otp' do
        expect(:post => 'resend_otp').to route_to(
        {
          'format'=>'json', 
          'controller'=>'users_module/v1/users_api', 
          'action'=>'resend_otp'
        })
        end

        it 'routes to #activate_with_referral_code' do
        expect(:put => 'api/users/referred').to route_to(
        {
          'format'=>'json', 
          'controller'=>'users_module/v1/users_api', 
          'action'=>'activate_with_referral_code'
        })
        end

        it 'routes to #check_user_state' do
        expect(:get => 'api/users/state').to route_to(
        {
          'format'=>'json', 
          'controller'=>'users_module/v1/users_api', 
          'action'=>'check_user_state'
        })
        end

        it 'routes to users#update_user_details' do
          expect(:put => 'api/users/details').to route_to(
          {
            'format'=>'json', 
            'controller'=>'users_module/v1/users_api', 
            'action'=>'update_user_details'
          })
        end

        it 'routes to users_api#get_current_ample_credits' do
          expect(:get => 'api/users/ample_credits').to route_to(
          {
            'format'=>'json', 
            'controller'=>'users_module/v1/users_api', 
            'action'=>'get_current_ample_credits'
          })
        end
      end
    end
  end
end
module SettingsModule
  module V1
    require 'rails_helper'
    # Routing specs for Address Api Controller
    RSpec.describe SettingsModule::V1::SettingsController, type: :routing do

      describe 'routing' do

        it 'routes to settings#get_static_settings' do
          expect(:get => 'settings').to route_to(
            {'format'=>'json', 
             'controller'=>'settings_module/v1/settings', 
             'action'=>'get_static_settings'})
        end

        it 'routes to settings#get_dynamic_settings' do
          expect(:get => 'settings/dynamic').to route_to(
            {'format'=>'json', 
             'controller'=>'settings_module/v1/settings', 
             'action'=>'get_dynamic_settings'})
        end
      end

    end
  end
end

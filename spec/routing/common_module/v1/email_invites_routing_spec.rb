module CommonModule
  module V1
    require 'rails_helper'
    # Routing specs for email invites Controller
    RSpec.describe CommonModule::V1::EmailInvitesController, type: :routing do
      describe 'routing' do

        it 'routes to #email_invites' do
          expect(:post => '/email/invites').to route_to(
            {'format'=>'json', 
             'controller'=>'common_module/v1/email_invites', 
             'action'=>'email_invites'})
        end
      end
    end
  end
end

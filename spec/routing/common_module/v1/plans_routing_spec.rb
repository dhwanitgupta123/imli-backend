module CommonModule
  module V1
    require "rails_helper"
    # Routing specs for Users Controller
    RSpec.describe CommonModule::V1::PlansController, type: :routing do
      describe "routing" do

        it "routes to #create_plan" do
          expect(:post => "/plans/new").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/plans", 
             "action"=>"create_plan"})
        end

        it "routes to #get_all_plans" do
          expect(:get => "/plans").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/plans", 
             "action"=>"get_all_plans"})
        end

        it "routes to #create_plan" do
          expect(:get => "/plans/1").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/plans", 
             "action"=>"get_plan",
             "id"=>"1" })
        end

        it "routes to #update_plan" do
          expect(:put => "/plans/1").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/plans", 
             "action"=>"update_plan",
             "id"=>"1" })
        end

        it "routes to #change_state" do
          expect(:put => "/plans/state/1").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/plans", 
             "action"=>"change_state",
             "id"=>"1" })
        end

        it "routes to #create_plan" do
          expect(:delete => "/plans/1").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/plans", 
             "action"=>"delete_plan",
             "id"=>"1" })
        end

      end
    end
  end
end
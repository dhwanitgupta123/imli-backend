module TransactionsModule
  module V1
    require "rails_helper"
    # Routing specs for Address Api Controller
    RSpec.describe TransactionsModule::V1::TransactionsController, type: :routing do
      describe "routing" do

        it "routes to transactions#get_transaction_status" do
          expect(:get => "transaction/status/:id").to route_to(
            {"format"=>"json", 
             "controller"=>"transactions_module/v1/transactions", 
             "action"=>"get_transaction_status",
             "id"=>":id"})
        end

      end
    end
  end
end
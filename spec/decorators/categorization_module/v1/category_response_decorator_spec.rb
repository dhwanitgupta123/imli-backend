module CategorizationModule
  module V1
    require 'rails_helper'
    RSpec.describe CategoryResponseDecorator do
      let(:category_response_decorator) { CategorizationModule::V1::CategoryResponseDecorator}
      let(:department) { FactoryGirl.build_stubbed(:department) }
      let(:category) { FactoryGirl.build_stubbed(:category, department: department) }
      let(:sub_category) {FactoryGirl.build_stubbed(:category, parent_category: category)}
      let(:response_codes) {CommonModule::V1::ResponseCodes}

      it "with create_ok_category_response" do
        response = category_response_decorator.create_ok_category_response(category)
        expect(response[:response]).to eq(response_codes::SUCCESS)
      end

      it "with create_ok_sub_category_response" do
        response = category_response_decorator.create_ok_sub_category_response(sub_category)
        expect(response[:response]).to eq(response_codes::SUCCESS)
      end

      it "with create_category_details_response" do
        response = category_response_decorator.create_category_details_response(category)
        expect(response[:response]).to eq(response_codes::SUCCESS)
      end

      it "with create_response_category_not_found_error" do
        response = category_response_decorator.create_response_category_not_found_error('error message')
        expect(response[:response]).to eq(response_codes::BAD_REQUEST)
      end

      it "with create_all_categories_response" do
        args = { 
                  categories: [FactoryGirl.build_stubbed(:category),FactoryGirl.build_stubbed(:category)],
                  page_count: Faker::Number.number(2)
                }
        response = category_response_decorator.create_all_categories_response(args)
        expect(response[:response]).to eq(response_codes::SUCCESS)
      end

      it "with create_all_sub_categories_response" do
        args = { 
                  sub_categories: [FactoryGirl.build_stubbed(:category),FactoryGirl.build_stubbed(:category)],
                  page_count: Faker::Number.number(2)
                }
        response = category_response_decorator.create_all_sub_categories_response(args)
        expect(response[:response]).to eq(response_codes::SUCCESS)
      end
    end
  end
end
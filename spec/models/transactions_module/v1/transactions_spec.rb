module TransactionsModule
  module V1
    require 'rails_helper'

    RSpec.describe TransactionsModule::V1::Transaction, type: :model do
      
      let(:transaction) { TransactionsModule::V1::Transaction }

      it "is valid" do
        FactoryGirl.build(:transaction).should be_valid
      end
    end
  end
end
module CategorizationModule
  module V1
    require 'rails_helper'

    RSpec.describe CategorizationModule::V1::Category, type: :model do
      
      it "is a valid factory" do
        FactoryGirl.create(:category).should be_valid
      end

      it "is invalid without label" do
        FactoryGirl.build(:category, label: nil).should_not be_valid
      end
    end
  end
end

module UsersModule
  module V1
    require 'rails_helper'

    RSpec.describe UsersModule::V1::Profile, type: :model do

      it "is valid" do
        user = FactoryGirl.build(:user)
        profile = FactoryGirl.build(:profile, user: user)
        expect(profile).to be_valid
        profile.delete
        user.delete
      end
    end
  end
end

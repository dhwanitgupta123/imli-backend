module UsersModule
  module V1
    require 'rails_helper'

    RSpec.describe UsersModule::V1::Role, type: :model do
      it "is a valid factory" do
        FactoryGirl.create(:role).should be_valid
      end

      it "is invalid without role name" do
        FactoryGirl.build(:role, role_name: nil).should_not be_valid
      end

      it "is invalid without permissions" do
        FactoryGirl.build(:role, permissions: nil).should_not be_valid
        FactoryGirl.build(:role, permissions: "").should_not be_valid
      end

      it "is valid with proper permissions" do
        FactoryGirl.build(:role).permissions.should be_a_kind_of(Array)
      end
    end
  end
end
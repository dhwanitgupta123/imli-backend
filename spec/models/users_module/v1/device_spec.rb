module UsersModule
  module V1
    require 'rails_helper'

    RSpec.describe UsersModule::V1::Device, type: :model do
      
      it "is valid" do
        FactoryGirl.create(:device).should be_valid
      end

      it "is invalid without user_id" do
        FactoryGirl.build(:device, user: nil).should_not be_valid
      end

      it "is invalid without last_login" do
        FactoryGirl.build(:device, last_login: nil).should_not be_valid
      end
    end
  end
end

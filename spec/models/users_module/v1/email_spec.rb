module UsersModule
  module V1
    require 'rails_helper'

    RSpec.describe UsersModule::V1::Email, type: :model do

    	it "is valid with valid format" do
    	  FactoryGirl.create(:email).should be_valid
    	end

      it "is invalid without user_id" do
        FactoryGirl.build(:email, user: nil).should_not be_valid
      end
    	
      it "is invalid without email_id" do
        FactoryGirl.build(:email, email_id: nil).should_not be_valid
      end

      it "is invalid without account_type" do
        FactoryGirl.build(:email, account_type: nil).should_not be_valid
      end

      it "is a valid with invalid format" do
        FactoryGirl.build(:email, email_id: "xyz").should_not be_valid
      end

      it "is valid with valid format" do
        FactoryGirl.build(:email, email_id: "user@example.com").should be_valid
      end

      it "is valid with valid format" do
        FactoryGirl.build(:email, email_id: "USER@foo.COM").should be_valid
      end

      it "is valid with valid format" do
        FactoryGirl.build(:email, email_id: "A_US-ER@foo.bar.org").should be_valid
      end

      it "is valid with valid format" do
        FactoryGirl.build(:email, email_id: "first.last@foo.jp").should be_valid
      end

      it "is valid with valid format" do
        FactoryGirl.build(:email, email_id: "alice+bob@baz.cn").should be_valid
      end

      context "Email Id " do
        let(:user) {FactoryGirl.create(:user, :phone_number => Faker::Number.number(10).to_s)}
        let(:user_email) {Faker::Internet.email}
        let(:gmail_id) {"adbcdefgh@gmail.com"}
        let(:verify_gmail_id_helper) {EmailValidationModule::V1::VerifyGmailIdHelper}

        it " already present" do
          FactoryGirl.create(:email, :email_id => user_email)
          FactoryGirl.build(:email, :email_id => user_email, :user => user).should_not be_valid
          
        end

        it "already present gmail id" do
          verify_gmail_id_helper.stub(:verify_if_already_in_use).and_return(true)
          FactoryGirl.build(:email, :user => user, :email_id => gmail_id).should_not be_valid
        end

      end
    end
  end
end
module UsersModule
  module V1
  	require 'rails_helper'

  	RSpec.describe UsersModule::V1::User, type: :model do
  	  
  		it "is a valid factory" do
        FactoryGirl.create(:user).should be_valid
  		end
  		  
  		it "is invalid without phone_number" do
        FactoryGirl.build(:user, phone_number: nil).should_not be_valid
  		end

  		it "is invalid without phone_number" do
        FactoryGirl.build(:user, phone_number: '').should_not be_valid
  		end	

  		it 'is not valid if its phone_number is same as another user account' do
        _existing_user = FactoryGirl.create(:user)
        user = FactoryGirl.build(:user, phone_number: _existing_user[:phone_number])
        expect(user).not_to be_valid
  		end
  		
  		it "phone number is of 10 digit" do
        FactoryGirl.build(:user, phone_number: "a"*11).should_not be_valid
  		end

  		it "phone number is of 10 digit" do
        FactoryGirl.build(:user, phone_number: "a"*9).should_not be_valid
  		end

  		describe 'states' do
        let(:user) {FactoryGirl.create(:user)}
        it 'should be an initial state' do
          user.should be_inactive
          end

        it 'when otp_verified!' do
          user.otp_verified!
          user.should be_pending
          end

        it 'when otp_verified! & activate!' do
          user.otp_verified!
          user.activate!
          user.should be_active
        end

        it "when activate!" do
          lambda {user.send('activate')}.should raise_error( NoMethodError )
        end
      end	
  	end
  end
end
module UsersModule
  module V1
    require 'rails_helper'

    RSpec.describe UsersModule::V1::Membership, type: :model do

      it "with valid params" do
        FactoryGirl.create(:membership).should be_valid
      end

      it "with expiry date less than start date" do
        start_date = DateTime.now
        expiry_date = start_date - 1
        FactoryGirl.build(:membership, starts_at: start_date, expires_at: expiry_date).should_not be_valid
      end

      it "with no plan associated" do
        FactoryGirl.build(:membership, :plan => nil).should_not be_valid
      end

    end
  end
end

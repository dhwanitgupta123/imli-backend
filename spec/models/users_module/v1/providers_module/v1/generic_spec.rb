module UsersModule
  module V1
    module ProvidersModule
      module V1
        require 'rails_helper'

        RSpec.describe UsersModule::V1::ProvidersModule::V1::Generic, type: :model do
          it "is valid " do
            FactoryGirl.build_stubbed(:generic, provider: FactoryGirl.build_stubbed(:provider) ).should be_valid
          end

          it "is invalid without password" do
            FactoryGirl.build(:generic, password: nil).should_not be_valid
          end

          it "is invalid with shorter password" do
            FactoryGirl.build(:generic, password: "test", provider: FactoryGirl.build_stubbed(:provider)).should_not be_valid
          end

          it "is valid with good password length" do
            FactoryGirl.build(:generic, password: Faker::Lorem.characters(178), provider: FactoryGirl.build_stubbed(:provider)).should be_valid
          end

          it "is invalid without provider_id" do
            FactoryGirl.build(:generic, provider_id: nil).should_not be_valid
          end

          it 'is not valid if duplicate provider_id exists' do
            class_path = UsersModule::V1::ProvidersModule::V1
            user = UsersModule::V1::User.new(:phone_number => Faker::Number.number(10).to_s)
            user.provider = UsersModule::V1::Provider.new
            user.provider.generic = class_path::Generic.new(:password => Faker::Lorem.characters(178))
            user.provider.generic.save

            user.provider.generic = class_path::Generic.new(:password => Faker::Lorem.characters(178))
            should_not user.provider.generic.save
          end
        end
      end
    end
  end
end
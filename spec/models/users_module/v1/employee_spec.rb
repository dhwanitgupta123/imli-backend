module UsersModule
  module V1
    require 'rails_helper'

    RSpec.describe UsersModule::V1::Employee, type: :model do

      it "is a valid factory" do
        FactoryGirl.create(:employee).should be_valid
      end

      it "is invalid without user association" do
        FactoryGirl.build(:employee, :user => nil).should_not be_valid
      end

      context "has state" do
        let(:employee) { FactoryGirl.create(:employee)}

        it "inactive after creation" do
          employee.should be_inactive
        end

        it "inactive and set_state_to_pending method is called" do
          employee.set_state_to_pending
          employee.should be_pending
        end

        it "inactive and set_state_to_active method is called" do
          employee.set_state_to_active
          employee.should be_inactive
          employee.should_not be_active
        end

        context "pending" do
          before(:example) { employee.created! }

          it "and set_state_to_active method is called" do
            employee.set_state_to_active
            employee.should be_active
          end

          it "and set_state_to_pending method is called" do
            employee.set_state_to_pending
            employee.should be_pending
          end
        end

      end
    end
  end
end

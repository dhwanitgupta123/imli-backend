module UsersModule
  module V1
    module MembershipModule
      module V1
        require 'rails_helper'

        RSpec.describe UsersModule::V1::MembershipModule::V1::MembershipPayment, type: :model do

          it "with valid params" do
            FactoryGirl.create(:membership_payment).should be_valid
          end
        end
      end
    end
  end
end

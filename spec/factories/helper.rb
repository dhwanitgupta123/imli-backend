#FactoryHelper class contains all common factory helper functions
# that might be re-used in test-cases and avoid redundancy
class FactoryHelper

	#Factory Helper function to create user
	#
	#returns user
	def create_user
      message = build_message
      provider =  build_stubbed_provider(message)
      user = FactoryGirl.create(:user, provider: provider)
      return user
	end

	#Factory Helper function to build user
	#
	#returns user
	def build_user
	  message = build_stubbed_message
      provider =  build_stubbed_provider(message)
      user = FactoryGirl.build(:user, provider: provider)
      return user
	end

	#Factory Helper function to build stubbed provider
	#args: message object
	#
	#returns provider
	def build_stubbed_provider(message)
	  provider =  FactoryGirl.build_stubbed(:provider, message: message)
	  return provider
	end

	#Factory Helper function to build provider
	#args: message object
	#
	#returns provider
	def build_provider(message)
	  provider = FactoryGirl.build(:provider, message: message)
	  return provider
	end

	#Factory Helper function to build stubbed message
	#
	#returns message
	def build_stubbed_message
	  message =  FactoryGirl.build_stubbed(:message)
	  return message
	end

	#Factory Helper function to build message
	#
	#returns message
	def build_message
	  message =  FactoryGirl.build(:message)
	  return message
	end
	
end
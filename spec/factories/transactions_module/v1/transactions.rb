module TransactionsModule
  module V1
    FactoryGirl.define do
      factory :transaction, :class => TransactionsModule::V1::Transaction do
        #Slug(str1 str2, dilim) joins str1 + dilim + str2
        payment_type Faker::Number.number(2)
        payment_id Faker::Number.number(2)
      end
    end
  end
end

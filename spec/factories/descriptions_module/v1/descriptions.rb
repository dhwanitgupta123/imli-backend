module DescriptionsModule
  module V1
    FactoryGirl.define do
      factory :description,:class => DescriptionsModule::V1::Description do 
        heading Faker::Name.first_name
        data [Faker::Lorem.characters(6)]
      end
    end
  end
end

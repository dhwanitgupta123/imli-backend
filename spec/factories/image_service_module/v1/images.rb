#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    FactoryGirl.define do
      factory :image, :class => ImageServiceModule::V1::Image do
        image_local File.open(File.join(File.dirname(__FILE__), 'dummy_image.jpg'))
      end
    end
  end
end

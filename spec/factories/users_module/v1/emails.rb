module UsersModule
  module V1
    FactoryGirl.define do
      factory :email, :class => UsersModule::V1::Email do
        association :user, factory: :user
        email_id Faker::Internet.email
        account_type Faker::Number.number(1)
        authentication_token Faker::Lorem.characters(64)
      end
    end
  end
end

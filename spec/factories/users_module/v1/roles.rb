module UsersModule
  module V1
    FactoryGirl.define do
      factory :role, :class => UsersModule::V1::Role do
        role_name Faker::Lorem.word
        permissions [1,2]
      end

    end
  end
end
module UsersModule
  module V1
    FactoryGirl.define do
      factory :membership, :class => UsersModule::V1::Membership do
        starts_at DateTime.now
        expires_at DateTime.now + 1
        quantity 1
        association :plan, factory: :plan
        association :membership_payment, factory: :membership_payment
      end
    end
  end
end

module UsersModule
  module V1
    module ProvidersModule
      module V1
        FactoryGirl.define do
          factory :generic, :class => UsersModule::V1::ProvidersModule::V1::Generic do
            password Faker::Lorem.characters(178).to_s
          end
        end
      end
    end
  end
end

module UsersModule
  module V1
    module MembershipModule
      module V1
        FactoryGirl.define do
          factory :membership_payment, :class => UsersModule::V1::MembershipModule::V1::MembershipPayment do
            mode Faker::Number.number(1)
            grand_total Faker::Commerce.price
            billing_amount Faker::Commerce.price
            refund Faker::Commerce.price
            total_vat_tax Faker::Number.decimal(2)
            total_service_tax Faker::Number.decimal(2)
            payment_mode_savings Faker::Number.decimal(2)
            discount Faker::Commerce.price
            gift_card Faker::Commerce.price
            total_savings Faker::Commerce.price
            net_total Faker::Commerce.price
          end
        end
      end
    end
  end
end

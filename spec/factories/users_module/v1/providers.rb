module UsersModule
  module V1
    FactoryGirl.define do
      factory :provider, :class => UsersModule::V1::Provider do
        session_token Faker::Lorem.characters(10)
      end
    end
  end
end

module UsersModule
  module V1
    FactoryGirl.define do
      factory :device, :class => UsersModule::V1::Device do
        association :user, factory: :user
        device_id Faker::Lorem.words(4)
        platform Faker::Lorem.characters(4)
        app_version Faker::App.version
        last_login Faker::Time.between(DateTime.now - 1, DateTime.now)
      end
    end
  end
end

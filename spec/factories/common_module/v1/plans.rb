module CommonModule
  module V1
    FactoryGirl.define do
      factory :plan, :class => CommonModule::V1::Plan do
        name Faker::Lorem.word
        details Faker::Lorem.sentence
        duration Faker::Number.between(1,99).to_s
        validity_label Faker::Lorem.word
        fee Faker::Number.number(2).to_s
        plan_key Faker::Number.number(1)
      end

    end
  end
end
module LoggingModule
  module V1
    FactoryGirl.define do
      factory :activity_log, :class => LoggingModule::V1::ActivityLog do
        #Slug(str1 str2, dilim) joins str1 + dilim + str2
        api_name Faker::Internet.slug('user login', '::')
        phone_number Faker::Number.number(10).to_s
        request_ip Faker::Internet.ip_v6_address
        request_url Faker::Internet.url
        request_query_string Faker::Lorem.word
        response_error CommonModule::V1::Content::AUTHENTICATION_TIMEOUT
        response_status CommonModule::V1::StatusCodes::SUCCESS
      end
    end
  end
end

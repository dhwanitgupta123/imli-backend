module CategorizationModule
  module V1
    FactoryGirl.define do
      factory :category,:class => CategorizationModule::V1::Category do 
        label Faker::Name.first_name
        image_url Faker::Internet.url
        priority Faker::Number.number(1)
      end
    end
  end
end

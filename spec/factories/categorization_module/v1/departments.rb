module CategorizationModule
  module V1    
    FactoryGirl.define do
      factory :department,:class => CategorizationModule::V1::Department do 
        label Faker::Name.first_name
        image_url Faker::Internet.url
        images [Faker::Number.number(1)]
        priority Faker::Number.number(1)
      end
    end
  end
end

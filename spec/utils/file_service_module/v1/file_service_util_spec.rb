#
# Module to handle functionalities related to file upload to S3
#
module FileServiceModule
  #
  # Version1 for file service module
  #
  module V1
    require 'rails_helper'
    RSpec.describe FileServiceModule::V1::FileServiceUtil do

      let(:version) { 1 }
      let(:file_service_util) { FileServiceModule::V1::FileServiceUtil.new }
      let(:s3_object) { AWS::S3::S3Object }
      let(:key) { Faker::Lorem.characters(7) }
      let(:random_url) { Faker::Internet.url }
      let(:file_url) { Faker::Internet.slug(random_url, key) }

      let(:current_dir) { File.dirname(__FILE__) }
      let(:files_in_current_dir) { Dir.glob( current_dir + '/*') }
      let(:valid_file_path) { files_in_current_dir[0] }
      let(:invalid_file_path) { Faker::Lorem.characters(70) }

      let(:custom_errors_util) { CommonModule::V1::CustomErrors } 

      let(:data_args) {
        {
          data: Faker::Lorem.characters(70),
          key: Faker::Lorem.characters(7),
          options: {
            acl: 'bucket-owner-full-control'
          }
        }
      }

      let(:file_args) {
        {
          file_path: valid_file_path,
          key: Faker::Lorem.characters(7),
          options: {
            acl: 'bucket-owner-full-control'
          }
        }
      }

      context 'upload data to s3' do
        it 'return url' do
          expect_any_instance_of(s3_object).to receive(:write).and_return(file_url)
          data = file_service_util.upload_data(data_args)
          expect(data).to include(data_args[:key])
        end

        it 'return error' do
          expect_any_instance_of(s3_object).to receive(:write).and_raise(StandardError.new)
          expect{ file_service_util.upload_data(data_args) }.to raise_error(custom_errors_util::S3FailedToUpload)
        end
      end

      context 'upload file to s3' do
        it 'return url with success upload' do
          expect_any_instance_of(s3_object).to receive(:write).and_return(file_url)
          data = file_service_util.upload_file(file_args)
          expect(data).to include(file_args[:key])
        end

        it 'return error' do
          expect_any_instance_of(s3_object).to receive(:write).and_raise(StandardError.new)
          expect{ file_service_util.upload_data(file_args) }.to raise_error(custom_errors_util::S3FailedToUpload)
        end
      end

      context 'get s3 unique key' do
        it 'return different s3_key for same input key' do
          s3_key_one = file_service_util.get_s3_key(key)
          sleep(1)
          s3_key_two = file_service_util.get_s3_key(key)
          expect(s3_key_one).not_to eq(s3_key_two)
        end
      end
    end
  end
end

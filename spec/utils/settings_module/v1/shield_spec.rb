#
# Module to handle all the functionalities related to description
#
module SettingsModule
  #
  # Version1 for description module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SettingsModule::V1::Shield do

      let(:shield_util) { SettingsModule::V1::Shield }
      let(:split) { SettingsModule::V1::Split }
      let(:feature_settings_class) { SettingsModule::V1::StaticSettings::FeatureSettings }

      let(:dummy_key) { Faker::Lorem.word }
      let(:dummy_experiment_name) { Faker::Lorem.word }
      let(:dummy_feature_value) { Faker::Number.number(2) }
      let(:dummy_treatment_value) { Faker::Number.number(2) }

      context 'get treatment value ' do

        it 'with valid key' do
          expect_any_instance_of(split).to receive(:start_split_experiment).
            and_return(dummy_treatment_value)
          
          response = shield_util.get_treatment_value(dummy_key)

          expect(response).to eq(dummy_treatment_value)
        end

        it 'with invalid key' do
          expect_any_instance_of(split).to receive(:start_split_experiment).
            and_return(nil)
          
          response = shield_util.get_treatment_value(dummy_key)

          expect(response).to eq(nil)
        end
      end

      context 'start experiment' do

        it 'with valid experiment name' do
          expect_any_instance_of(split).to receive(:start_split_experiment).
            and_return(dummy_treatment_value)
          
          response = shield_util.start_experiment(dummy_experiment_name)

          expect(response).to eq(dummy_treatment_value)
        end

        it 'with invalid experiment name' do
          expect_any_instance_of(split).to receive(:start_split_experiment).
            and_return(nil)
          
          response = shield_util.start_experiment(dummy_experiment_name)

          expect(response).to eq(nil)
        end
      end

      context 'finish experiment' do

        it 'with valid experiment name' do
          expect_any_instance_of(split).to receive(:finish_split_experiment).
            and_return(dummy_treatment_value)
          
          response = shield_util.finish_experiment(dummy_experiment_name)
          # finish_experiment returns Shield::Experiment Object
          expect(response).to_not eq(nil)
        end

        it 'with invalid experiment name' do
          expect_any_instance_of(split).to receive(:finish_split_experiment).
            and_return(nil)

          response = shield_util.finish_experiment(nil)

          expect(response).to eq(nil)
        end
      end

      context 'get feature value ' do

        let(:feature_settings) {
          {
            'DUMMY_FEATURE': Faker::Number.number(2)
          }
        }

        it 'with valid feature' do
          json_of_feature_settings = JSON.parse(JSON.generate(feature_settings))
          expect_any_instance_of(feature_settings_class).to receive(:fetch).
            and_return(json_of_feature_settings)

          response = shield_util.get_feature_value('DUMMY_FEATURE')

          expect(response).to eq(feature_settings[:DUMMY_FEATURE])
        end

        it 'with invalid feature key' do
          expect_any_instance_of(feature_settings_class).to receive(:fetch).
            and_return(feature_settings)

          response = shield_util.get_feature_value('ANY_RANDOM_INVALID_KEY')

          expect(response).to eq(nil)
        end
      end

    end
  end
end

#
# This module is responsible to handle all the pre-request validations
# like user state, user membership state etc 
#
module ValidationsModule
  #
  # Version1 for validation module 
  #
  module V1
    require 'rails_helper'
    RSpec.describe ValidationsModule::V1::UserMembershipValidator do

      let(:user_membership_validator) { ValidationsModule::V1::UserMembershipValidator.new }
      let(:membership_dao) { UsersModule::V1::MembershipDao }
      let(:user) { FactoryGirl.build(:user) }
      let(:membership) { FactoryGirl.build(:membership) }

      context 'validate' do
        it 'with zero active/pending user membership' do
          expect_any_instance_of(membership_dao).to receive(:get_pending_or_active_membership).and_return(nil)
          response = user_membership_validator.validate(user)
          expect(response[:result]).to eq(false)
        end
        it 'with active/pending user membership' do
          expect_any_instance_of(membership_dao).to receive(:get_pending_or_active_membership).and_return(membership)
          response = user_membership_validator.validate(user)
          expect(response[:result]).to eq(true)
        end
      end
    end
  end
end

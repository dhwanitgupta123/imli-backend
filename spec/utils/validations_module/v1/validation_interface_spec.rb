#
# This module is responsible to handle all the pre-request validations
# like user state, user membership state etc 
#
module ValidationsModule
  #
  # Version1 for validation module 
  #
  module V1
    require 'rails_helper'

    #
    # This is rspec for the ValidationInterface 
    # These rspecs ensure if any class extends this interface
    # then it should implement the function exposed by the
    # interface
    # 
    RSpec.describe ValidationsModule::V1::ValidationInterface do
      before :all do
        #
        # Loading classes of config.autoload_paths
        # 
        Rails.application.eager_load!
        #
        # Get all the classes which include this interface
        # 
        @implementations = ObjectSpace.each_object(Class).select { |klass| klass < ValidationsModule::V1::ValidationInterface }
      end

      #
      # This enforce that all the classes which include the interface
      # implement validate method
      # 
      it 'should implement validate method' do
        @implementations.each do |klass|
          klass_methods = klass.instance_methods(false)
          expect(klass_methods).to include(:validate)
        end
      end
    end
  end
end

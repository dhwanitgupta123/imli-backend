#
# This module is responsible to handle all the pre-request validations
# like user state, user membership state etc 
#
module ValidationsModule
  #
  # Version1 for validation module 
  #
  module V1
    require 'rails_helper'
    RSpec.describe ValidationsModule::V1::ActiveUserValidator do

      let(:active_user_validator) { ValidationsModule::V1::ActiveUserValidator.new }

      context 'validate' do
        it 'with active user' do
          user = FactoryGirl.build(:user)
          user.workflow_state = 1
          user.save
          response = active_user_validator.validate(user)
          expect(response[:result]).to eq(true)
          user.destroy
        end

        it 'with inactive user' do
          user = FactoryGirl.build(:user)
          response = active_user_validator.validate(user)
          expect(response[:result]).to eq(false)
          user.destroy
        end
      end
    end
  end
end

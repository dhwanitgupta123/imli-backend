module SweeperModule
  module V1
    require "rails_helper"
    RSpec.describe ImageSweeperWorker, type: :sweeper do

      let(:image_sweeper_worker) { SweeperModule::V1::ImageSweeperWorker}

      it "enqueues a UserActivityAnalyzer worker" do
        image_sweeper_worker.perform_async
        expect(image_sweeper_worker).to have_enqueued_job
      end
          
      #to check the queue name
      it { is_expected.to be_processed_in :job_queue }
    end
  end
end
#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    require "rails_helper"
    RSpec.describe DataAggregationModule::V1::ApiLogAggregatorWorker, type: :analyzer do

      let(:api_log_aggregator_worker) { DataAggregationModule::V1::ApiLogAggregatorWorker }

      it "enqueues a ApiLogAggregatorWorker worker" do
        api_log_aggregator_worker.perform_async
        expect(api_log_aggregator_worker).to have_enqueued_job
      end
          
      #to check the queue name
      it { is_expected.to be_processed_in :default }

      #to check how many retries can be done
      it { is_expected.to be_retryable 2 }

      #to check if a job should save the error backtrace when there is a failure in it's execution
      it { is_expected.to save_backtrace }
    end
  end
end

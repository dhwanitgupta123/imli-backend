module CommunicationsModule
  module V1
    module Messengers
      module V1
        require "rails_helper"
        RSpec.describe SmsWorker do
          let(:sms_attribue) {
            {
              "message" => Faker::Lorem.characters(10),
              "phone_number" => Faker::Number.number(10).to_s
            }
          }

          let(:sms_worker) { CommunicationsModule::V1::Messengers::V1::SmsWorker }
          let(:sms_type) { CommunicationsModule::V1::Messengers::V1::SmsType }

          it "enqueues a sms worker" do
            sms_worker.perform_async(sms_attribue, sms_type::WELCOME)
            expect(sms_worker).to have_enqueued_job(sms_attribue, sms_type::WELCOME)
          end
            
          #to check the queue name
          it { is_expected.to be_processed_in :sms_queue }

          #to check how many retries can be done
          it { is_expected.to be_retryable 5 }

          #to check if a job should save the error backtrace when there is a failure in it's execution
          it { is_expected.to save_backtrace }
        end
      end
    end
  end
end
module UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::MembershipDao do
      let(:version_params) { { version: 1 } }

      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:content_util) { CommonModule::V1::Content }
      let(:plan_service) { CommonModule::V1::PlanService }
      let(:membership_state) { UsersModule::V1::ModelStates::MembershipStates }
      let(:plan_state) { CommonModule::V1::ModelStates::V1::PlanStates}
      let(:membership_model) { UsersModule::V1::Membership }
      let(:plan_keys) { CommonModule::V1::PlanKeys }
      let(:membership_dao) { UsersModule::V1::MembershipDao.new({}) }
      let(:membership_state) { UsersModule::V1::ModelStates::MembershipStates }
      # Using FactoryGirl.build_stubbed because user.id is needed in test cases
      # which can only be provided by FG.create OR FG.build_stubbed
      let(:profile) { FactoryGirl.build_stubbed(:profile) }
      let(:user) { FactoryGirl.build_stubbed(:user, profile: profile) }
      let(:benefit) { FactoryGirl.build_stubbed(:benefit, status: benefit_state::ACTIVE, condition: 'max_address') }
      let(:plan) { FactoryGirl.build_stubbed(:plan, plan_key: plan_keys::TRIAL) }
      let(:ample_go) { FactoryGirl.build_stubbed(:plan, plan_key: plan_keys::AMPLE_GO) }
      let(:membership) { FactoryGirl.build_stubbed(:membership, plan: plan, workflow_state: membership_state::ACTIVE) }
      let(:payment) { FactoryGirl.build_stubbed(:membership_payment) }
      let(:memberships) { [FactoryGirl.build_stubbed(:membership, plan: ample_go), FactoryGirl.build_stubbed(:membership, plan: ample_go)] }

      describe 'get_ample_sample_membership' do
        it 'returns nil if no trial membership' do
          expect(membership_dao.get_ample_sample_membership(memberships)).to be_nil
        end

        it 'returns trial membership' do
          expect(membership_dao.get_ample_sample_membership([membership])).to_not be_nil
        end
      end
    end
  end
end

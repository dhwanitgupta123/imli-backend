#
# Module to handle all the functionalities related to ample_credit
#
module CreditsModule
  #
  # Version1 for ample_credit module
  #
  module V1
    require 'rails_helper'

    RSpec.describe CreditsModule::V1::AmpleCreditDao, type: :dao do
      let(:ample_credit_dao) { CreditsModule::V1::AmpleCreditDao.new }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }

      let(:ample_credit) { FactoryGirl.create(:ample_credit) }
      let(:user) { FactoryGirl.create(:user) }

      let(:ample_credit_request) {
        {
          user_id: user.id,
          balance: Faker::Number.decimal(2).to_f
        }
      }

      context 'create ample_credit' do
        it 'with valid params' do
          ample_credit = ample_credit_dao.create(ample_credit_request)
          expect(ample_credit.balance.to_s).to eq(ample_credit_request[:balance].to_s)
        end
      end

      context 'update ample_credit' do
        it 'with valid arguments' do
          ample_credit_before = FactoryGirl.create(:ample_credit)
          new_balance = Faker::Number.decimal(2).to_f
          ample_credit = ample_credit_dao.update({ balance: new_balance }, ample_credit_before)
          expect(ample_credit.balance.to_s).to eq(new_balance.to_s)
        end
      end
    end
  end
end

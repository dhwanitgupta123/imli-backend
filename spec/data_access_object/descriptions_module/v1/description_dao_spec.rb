#
# Module to handle all the functionalities related to description
#
module DescriptionsModule
  #
  # Version1 for description module
  #
  module V1
    require 'rails_helper'

    RSpec.describe DescriptionsModule::V1::DescriptionDao, type: :dao do
      let(:description_dao) { DescriptionsModule::V1::DescriptionDao.new }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:common_states) { SupplyChainCommonModule::V1::CommonStates }
      let(:description) { FactoryGirl.create(:description) }

      let(:description_request) {
        {
          heading: Faker::Company.name,
          resource:  Faker::Lorem.characters(6),
          resource_id: Faker::Number.number(1),
          data: [
             Faker::Lorem.characters(6)
          ]
        }
      }

      context 'create description' do
        it 'with valid params' do
          description = description_dao.create(description_request)
          expect(description.heading).to eq(description_request[:heading])
        end
      end

      context 'update description' do
        it 'with valid arguments' do
          description_before = FactoryGirl.create(:description)
          new_heading = Faker::Company.name
          description = description_dao.update({ heading: new_heading }, description_before)
          expect(description.heading).to eq(new_heading)
        end
      end

      context 'get description by id' do
        it 'with valid args' do 
          found_description = description_dao.get_by_id(description.id)
          expect(found_description.id).to eq(description.id)
        end

        it 'with invalid args' do
          found_description = description_dao.get_by_id(description.id)
          expect{ description_dao.get_by_id(description.id + 1) }.
          to raise_error(custom_error_util::ResourceNotFoundError)
        end
      end
    end
  end
end

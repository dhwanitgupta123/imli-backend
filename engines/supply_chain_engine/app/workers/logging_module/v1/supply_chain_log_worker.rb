#
# This module is responsible for logging meta data
#
module LoggingModule
  #
  # Version1 for logging module
  #
  module V1
    #
    # Common log worker for supply chain engine
    #
    class SupplyChainLogWorker
      include Sidekiq::Worker
      sidekiq_options queue: :default
    end
  end
end

#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    #
    # Model for seller buying brand pack table
    #
    class SellerBrandPack < ActiveRecord::Base
      #
      # Defining global variables with versioning
      #
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors


      has_one :seller_pricing, dependent: :destroy, class_name: 'SellerPricingModule::V1::SellerPricing'

      belongs_to :seller, class_name: 'SupplyChainModule::V1::Seller'

      validates :seller, presence: true

      belongs_to :brand_pack, class_name: 'MasterProductModule::V1::BrandPack'

      validates :brand_pack, presence: true

      validates :seller_pricing, presence: true

      # WBP has many SBPs
      has_and_belongs_to_many :warehouse_brand_packs, dependent: :destroy, class_name: 'WarehouseProductModule::V1::WarehouseBrandPack'

      validates :warehouse_brand_packs, presence: true

      # putting uniqueness constraint on the tuple of seller & brand pack
      validates_uniqueness_of :brand_pack_id, scope: :seller_id

      # A MPBP can have many SBP
      has_many :marketplace_brand_pack_seller_brand_packs, dependent: :destroy, class_name:  'MarketplaceProductModule::V1::MarketplaceBrandPackSellerBrandPack'
      has_many :marketplace_brand_packs, through: :marketplace_brand_pack_seller_brand_packs, dependent: :destroy, class_name: 'MarketplaceProductModule::V1::MarketplaceBrandPack'

      #
      # Workflow to define states of the Seller brand pack
      #
      # Initial State => Inactive
      #
      # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, COMMON_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, COMMON_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_MODEL_STATES::DELETED
      end

      #
      # Function to change state of seller brand pack based on the event
      #
      # Raise Error::
      #   * CustomError::PreConditionRequiredError if trsnsition is not defined for the current status
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end

    end
  end
end
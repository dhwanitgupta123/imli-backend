#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # MarketplaceSellingPack class with instance valriable attributes hash
    #
    class MarketplaceSellingPackSearchModel
      attr_reader :attributes

      def initialize(attributes = {})
        @attributes = attributes
      end

      def to_hash
        @attributes
      end
    end
  end
end

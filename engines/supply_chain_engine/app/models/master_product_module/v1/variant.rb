#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Model for variants table
    #
    class Variant < ActiveRecord::Base
      has_many :products, dependent: :destroy

      validates :name, presence: true, uniqueness: true

      validates :initials, presence: true, uniqueness: true
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Model for brands table
    #
    class Brand < ActiveRecord::Base
      
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      has_many :sub_brands, dependent: :destroy

      belongs_to :company, class_name: 'SupplyChainModule::V1::Company'

      validates :company_id, presence: true

      validates :name, presence: true

      validates :initials, presence: true, uniqueness: true

      #
      # Workflow to define states of the Brand
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :active, COMMON_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :inactive, COMMON_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_MODEL_STATES::DELETED
      end
    end
  end
end

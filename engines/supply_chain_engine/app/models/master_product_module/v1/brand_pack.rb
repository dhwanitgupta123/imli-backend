#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Model for brandpacks table
    #
    class BrandPack < ActiveRecord::Base

      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      belongs_to :product

      has_many :seller_brand_packs, dependent: :destroy, class_name: 'SellerProductModule::V1::SellerBrandPack'

      validates :product, presence: true

      belongs_to :sub_category, class_name: 'CategorizationModule::V1::Category', dependent: :destroy

      validates :sub_category_id, presence: true
      after_save :reload, on: :create
      validates :brand_pack_code, uniqueness: true
      has_many :inventory_brand_packs, dependent: :destroy, class_name: 'InventoryProductModule::V1::InventoryBrandPack'
      has_many :warehouse_brand_packs, dependent: :destroy, class_name: 'WarehouseProductModule::V1::WarehouseBrandPack'
      has_many :marketplace_brand_packs, dependent: :destroy, class_name: 'MarketplaceProductModule::V1::MarketplaceBrandPack'

      #
      # Workflow to define states of the brand pack
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :active, COMMON_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :inactive, COMMON_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_MODEL_STATES::DELETED
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Model for products table
    #
    class Product < ActiveRecord::Base
      
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      has_many :brand_packs, dependent: :destroy

      belongs_to :sub_brand

      belongs_to :variant

      validates :name, presence: true

      validates :sub_brand_id, presence: true

      validates :initials, presence: true, uniqueness: true

      #
      # Workflow to define states of the Product
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :active, COMMON_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :inactive, COMMON_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_MODEL_STATES::DELETED
      end
    end
  end
end

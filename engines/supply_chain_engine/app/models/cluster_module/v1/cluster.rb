#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # Model for clusters table
    #
    class Cluster < ActiveRecord::Base

      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      has_and_belongs_to_many :marketplace_selling_packs, dependent: :destroy, class_name: 'MarketplaceProductModule::V1::MarketplaceSellingPack'

      has_one :cluster_context, dependent: :destroy


      #
      # Workflow to define states of the MarketplaceBrandPack
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, COMMON_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, COMMON_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_MODEL_STATES::DELETED
      end
    end
  end
end

#
# This module is responsible for logging meta data
#
module LoggingModule
  #
  # Version1 for logging module
  #
  module V1
    #
    # Mongoid document to save query string and result hash
    #
    class SearchQueryLog
      #Making it mongoid document
      include Mongoid::Document
      #Saving timeStamp with each log
      include Mongoid::Timestamps

      field :query, type: String
      field :results, type: Hash
    end
  end
end

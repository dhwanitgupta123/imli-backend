#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Model for MarketplaceBrandPackSellerBrandPack table
    #
    class MarketplaceBrandPackSellerBrandPack < ActiveRecord::Base
      # MarketplaceBrandPackSellerBrandPack contains mapping of SBP and MPBP
      # Its a relational table between MPBP and SBP (MPBP has many SBPs
      # through MarketplaceBrandPackSellerBrandPack)
      belongs_to :seller_brand_pack, class_name: 'SellerProductModule::V1::SellerBrandPack'
      belongs_to :marketplace_brand_pack
    end
  end
end

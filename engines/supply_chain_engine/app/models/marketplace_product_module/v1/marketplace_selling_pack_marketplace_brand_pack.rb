#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Model for MarketplaceSellingPackMarketplaceBrandPack table
    #
    class MarketplaceSellingPackMarketplaceBrandPack < ActiveRecord::Base
      # MarketplaceSellingPackMarketplaceBrandPack contains mapping of MPBP and MPSP
      # Its a relational table between MPSP and MPBP (MPSP has many MPBP
      # through MarketplaceSellingPackMarketplaceBrandPack)
      belongs_to :marketplace_selling_pack
      belongs_to :marketplace_brand_pack
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Model for MarketplaceSellingPack table
    #
    class MarketplaceSellingPack < ActiveRecord::Base
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      has_one :marketplace_selling_pack_pricing, dependent: :destroy, class_name: 'MarketplacePricingModule::V1::MarketplaceSellingPackPricing'

      # A MPSP can have many MPBP associated with quantity
      has_many :marketplace_selling_pack_marketplace_brand_packs, dependent: :destroy
      has_many :marketplace_brand_packs, through: :marketplace_selling_pack_marketplace_brand_packs, dependent: :destroy

      # A MPBP can have many MPSP associated with order_products
      has_many :order_products, dependent: :destroy, class_name: 'MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProduct'

      validates :max_quantity, presence: true

      validates :marketplace_selling_pack_pricing, presence: true


      belongs_to :mpsp_sub_category, class_name: 'CategorizationModule::V1::MpspCategory', dependent: :destroy

      validates :mpsp_sub_category_id, presence: true

      has_and_belongs_to_many :clusters, class_name: 'ClusterModule::V1::Cluster', dependent: :destroy

      has_one :description, dependent: :destroy, class_name: 'DescriptionsModule::V1::Description', as: :description_of
      #
      # Workflow to define states of the MarketplaceSellingPack
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, COMMON_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, COMMON_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_MODEL_STATES::DELETED
      end

      # 
      # Overriding this function to get get description tree
      # 
      # @return [Tree] where description is root node
      #
      def description
        description = super
        return description.childs(true) if description.present?
        return description
      end
    end
  end
end

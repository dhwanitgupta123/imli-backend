#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Model for MarketplaceBrandPack table
    #
    class MarketplaceBrandPack < ActiveRecord::Base
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      has_one :marketplace_brand_pack_pricing, dependent: :destroy, class_name: 'MarketplacePricingModule::V1::MarketplaceBrandPackPricing'

      # A MPBP can have many MPSP associated with quantity
      has_many :marketplace_selling_pack_marketplace_brand_packs, dependent: :destroy
      has_many :marketplace_selling_packs, through: :marketplace_selling_pack_marketplace_brand_packs, dependent: :destroy

      # A MPBP can have many SBP
      has_many :marketplace_brand_pack_seller_brand_packs, dependent: :destroy
      has_many :seller_brand_packs, through: :marketplace_brand_pack_seller_brand_packs, dependent: :destroy, class_name: 'SellerProductModule::V1::SellerBrandPack'

      belongs_to :brand_pack, class_name: 'MasterProductModule::V1::BrandPack'
      validates :brand_pack, presence: true, uniqueness: true

      validates :marketplace_brand_pack_pricing, presence: true
      #
      # Workflow to define states of the MarketplaceBrandPack
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, COMMON_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, COMMON_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_MODEL_STATES::DELETED
      end
    end
  end
end

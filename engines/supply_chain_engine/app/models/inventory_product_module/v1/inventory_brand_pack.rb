#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Model for InventoryBrandPack table
    #
    class InventoryBrandPack < ActiveRecord::Base
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      has_one :inventory_pricing, dependent: :destroy, class_name: 'InventoryPricingModule::V1::InventoryPricing'

      belongs_to :inventory, class_name: 'SupplyChainModule::V1::Inventory'

      validates :inventory, presence: true

      belongs_to :brand_pack, class_name: 'MasterProductModule::V1::BrandPack'

      has_many :inventory_order_products, dependent: :destroy, class_name: 'InventoryOrdersModule::V1::InventoryOrderProduct'

      validates :brand_pack, presence: true

      validates :inventory_pricing,  presence: true

      # IBP has many WBPs
      has_and_belongs_to_many :warehouse_brand_packs, class_name: 'WarehouseProductModule::V1::WarehouseBrandPack'

      #
      # Workflow to define states of the InventoryBrandPack
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, COMMON_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, COMMON_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_MODEL_STATES::DELETED
      end
    end
  end
end

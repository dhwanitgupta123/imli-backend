#
# Module to handle all the functionalities related to warehouse product
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    # 
    # Module to keep information of model state
    # 
    module ModelStates
      module V1
        class WarehouseStockStates
          INWARD    = 1
          DAMAGED   = 2
          EXPIRED   = 3
          OUTWARD   = 4
          AUDITED   = 5
          CANCELLED_INWARD = 6
        end
      end
    end
  end
end

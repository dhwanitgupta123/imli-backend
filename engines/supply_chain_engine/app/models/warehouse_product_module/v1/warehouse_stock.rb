#
# Module to handle all the functionalities related to warehouse brand pack
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Model for warehouse stock table
    #
    class WarehouseStock < ActiveRecord::Base

      WAREHOUSE_STOCK_STATES = WarehouseProductModule::V1::ModelStates::V1::WarehouseStockStates
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      belongs_to :warehouse_pricing, dependent: :destroy, class_name: 'WarehousePricingModule::V1::WarehousePricing'


      validates :quantity, presence: true

      #
      # Workflow to define states of the warehouse stock
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inward, WAREHOUSE_STOCK_STATES::INWARD
        state :damaged, WAREHOUSE_STOCK_STATES::DAMAGED
        state :expired, WAREHOUSE_STOCK_STATES::EXPIRED
        state :outward, WAREHOUSE_STOCK_STATES::OUTWARD
        state :audited, WAREHOUSE_STOCK_STATES::AUDITED
        state :cancelled_inward, WAREHOUSE_STOCK_STATES::CANCELLED_INWARD
      end

    end
  end
end

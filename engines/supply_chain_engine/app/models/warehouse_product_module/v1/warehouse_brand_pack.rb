#
# Module to handle all the functionalities related to warehouse brand pack
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Model for warehouse brand packs table
    #
    class WarehouseBrandPack < ActiveRecord::Base

      COMMON_STATES = SupplyChainCommonModule::V1::CommonStates
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      has_many :warehouse_pricings, dependent: :destroy, class_name: 'WarehousePricingModule::V1::WarehousePricing'

      # warehouse has many Warehouse Brand packs
      belongs_to :warehouse, class_name: 'SupplyChainModule::V1::Warehouse'

      # IBP has many WBPs
      has_and_belongs_to_many :inventory_brand_packs, dependent: :destroy, class_name: 'InventoryProductModule::V1::InventoryBrandPack'

      # WBP has many SBPs
      has_and_belongs_to_many :seller_brand_packs, dependent: :destroy, class_name: 'SellerProductModule::V1::SellerBrandPack'

      # MBP has many WBPs
      belongs_to :brand_pack, class_name: 'MasterProductModule::V1::BrandPack'

      validates :warehouse, presence: true
      validates :brand_pack, presence: true
      validates :inventory_brand_packs, presence: true

      # putting uniqueness constraint on the tuple of warehouse & brand pack
      validates_uniqueness_of :brand_pack_id, scope: :warehouse_id

      validates :warehouse_pricings, presence: true

      # TO-DO
      # belongs_to :seller_brand_pack

      #
      # Workflow to define states of the warehouse
      #
      # Initial State => Inactive
      #
      # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, COMMON_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, COMMON_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_STATES::DELETED
      end

      #
      # Function to change state of warehouse based on the event
      #
      # Raise Error::
      #   * CustomError::PreConditionRequiredError if transition is not defined for the current status
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end

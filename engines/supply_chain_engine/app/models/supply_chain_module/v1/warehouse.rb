#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Model for warehouse table
    #
    class Warehouse < ActiveRecord::Base

      COMMON_STATES = SupplyChainCommonModule::V1::CommonStates
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      validates :name, presence: true, uniqueness: { case_sensitive: false }

      validates :cash_and_carry, presence: true

      # warehouse has many Warehouse Brand packs
      has_many :warehouse_brand_packs, dependent: :destroy, class_name: 'WarehouseProductModule::V1::WarehouseBrandPack'
      # warehouse has many addresses
      has_many :warehouse_addresses, dependent: :destroy, class_name: 'SupplyChainModule::V1::WarehouseModule::V1::WarehouseAddress'

      # cash_and_carry has many warehouses
      belongs_to :cash_and_carry

      has_many :inventory_orders, dependent: :destroy, class_name: 'InventoryOrdersModule::V1::InventoryOrder'

      #
      # Workflow to define states of the warehouse
      #
      # Initial State => Inactive
      #
      # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, COMMON_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, COMMON_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_STATES::DELETED
      end

      #
      # Function to change state of warehouse based on the event
      #
      # Raise Error::
      #   * CustomError::PreConditionRequiredError if transition is not defined for the current status
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Model for seller table
    #
    class Seller < ActiveRecord::Base
      #
      # Defining global variables with versioning
      #
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors


      has_many :seller_brand_packs, dependent: :destroy, class_name: 'SellerProductModule::V1::SellerBrandPack'

      validates :name, presence: true, uniqueness: { case_sensitive: false }

      #
      # Workflow to define states of the Seller
      #
      # Initial State => Inactive
      #
      # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, COMMON_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, COMMON_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_MODEL_STATES::DELETED
      end

      #
      # Function to change state of seller based on the event
      #
      # Raise Error::
      #   * CustomError::PreConditionRequiredError if trsnsition is not defined for the current status
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end
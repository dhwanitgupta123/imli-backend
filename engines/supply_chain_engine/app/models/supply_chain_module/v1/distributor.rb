#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Model for Distributors table
    #
    class Distributor < ActiveRecord::Base
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      has_one :inventory, dependent: :destroy

      belongs_to :company
      after_save :reload, on: :create
      validates :name, presence: true, uniqueness: { case_sensitive: false }
      validates :vendor_code, uniqueness: true

      #
      # Workflow to define states of the Distributor
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, COMMON_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, COMMON_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_MODEL_STATES::DELETED
      end
    end
  end
end

module SupplyChainModule
  module V1
    #
    # Module to handle all the functionalities related to inventory
    #
    module InventoryModule
      module V1
        #
        # Model for inventory_addresses table
        #
        class InventoryAddress < ActiveRecord::Base

          belongs_to :area, class_name: 'AddressModule::V1::Area'

          belongs_to :inventory, class_name: 'SupplyChainModule::V1::Inventory'

          validates :address_line1, presence: true

          validates :area_id, presence: true
        end
      end
    end
  end
end

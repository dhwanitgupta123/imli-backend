#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Model for companies table
    #
    class Company < ActiveRecord::Base

      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      has_many :brands, dependent: :destroy, class_name: 'MasterProductModule::V1::Brand'

      validates :name, presence: true, uniqueness: { case_sensitive: false }

      validates :initials, presence: true, uniqueness: true

      has_many :distributors, dependent: :destroy

      #
      # Workflow to define states of the Company
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :active, COMMON_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :inactive, COMMON_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_MODEL_STATES::DELETED
      end
    end
  end
end

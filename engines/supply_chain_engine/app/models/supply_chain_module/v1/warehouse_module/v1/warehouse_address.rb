module SupplyChainModule
  module V1
    #
    # Module to handle all the functionalities related to warehouse
    #
    module WarehouseModule
      module V1
        #
        # Model for warehouse_addresses table
        #
        class WarehouseAddress < ActiveRecord::Base

          belongs_to :area, class_name: 'AddressModule::V1::Area'

          belongs_to :warehouse, class_name: 'SupplyChainModule::V1::Warehouse'

          has_many :inventory_orders, dependent: :destroy, class_name: 'InventoryOrdersModule::V1::InventoryOrder'

          validates :address_line1, presence: true

          validates :area_id, presence: true
        end
      end
    end
  end
end

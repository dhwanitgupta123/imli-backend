#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Model for Inventory table
    #
    class Inventory < ActiveRecord::Base
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      has_many :inventory_brand_packs, dependent: :destroy, class_name: 'InventoryProductModule::V1::InventoryBrandPack'

      has_many :inventory_orders, dependent: :destroy, class_name: 'InventoryOrdersModule::V1::InventoryOrder'

      belongs_to :distributor

      validates :distributor, presence: true

      # inventory has one address
      has_one :inventory_address, dependent: :destroy, class_name: 'SupplyChainModule::V1::InventoryModule::V1::InventoryAddress'

      #
      # Workflow to define states of the Inventory
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, COMMON_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, COMMON_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, COMMON_MODEL_STATES::DELETED
      end
    end
  end
end

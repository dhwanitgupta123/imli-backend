#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # Reader class for marketplace_selling_pack model
    #
    class MarketplaceSellingPackModelReader
      def initialize(results)
        @results = results
      end

      #
      # Map marketplace_selling_pack result to array of attributes hash
      #
      # @return [Array] array of marketplace_selling_pack attributes
      #
      def get_attribute_array
        return [] if @results.blank?
        attributes = []
        @results.each do |result|
          attributes.push(result.attributes['product'])
        end
        attributes
      end
    end
  end
end

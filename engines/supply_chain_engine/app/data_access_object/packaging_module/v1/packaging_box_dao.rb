#
# Module to handle all the functionalities related to Packaging
#
module PackagingModule
  #
  # Version1 for Packaging module
  #
  module V1
    #
    # Data Access Object to interact with PackagingBoxModel
    #
    class PackagingBoxDao < SupplyChainBaseModule::V1::BaseDao

      PACKAGING_BOX_MODEL = PackagingModule::V1::PackagingBox

      def initialize(params='')
        super
        @params = params
      end

      def create(args)
        packaging_box_model = PACKAGING_BOX_MODEL
        create_model(args, packaging_box_model, 'PackagingBox')
      end

      def update(args, packaging_box)
        update_model(args, packaging_box, 'PackagingBox')
      end

      def get_by_id(id)
        get_model_by_id(PACKAGING_BOX_MODEL, id)
      end

      def get_all(pagination_params = {})
        get_all_element(PACKAGING_BOX_MODEL, pagination_params)
      end
    end
  end
end

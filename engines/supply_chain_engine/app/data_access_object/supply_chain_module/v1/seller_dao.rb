#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Data Access Object to interact with SellerModel
    #
    class SellerDao < SupplyChainBaseModule::V1::BaseDao
      SELLER_MODEL = SupplyChainModule::V1::Seller
      PAGINATION_UTIL = CommonModule::V1::Pagination
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      def initialize
        super
      end

      #
      # Create the seller
      #
      # @param args [name] seller name
      #
      # @return [SellerModel]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        new_seller = SELLER_MODEL.new(args)

        begin
          new_seller.save!
          return new_seller
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create Seller, ' + e.message)
        end
      end

      #
      # Find the seller by id
      #
      # @param name [Integer] seller id
      #
      # @return [SellerModel] return seller model else nil
      #
      def get_seller_by_id(seller_id)
        begin
          SELLER_MODEL.find(seller_id)
          rescue ActiveRecord::RecordNotFound => e
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::SELLER_NOT_FOUND)
        end
      end

      #
      # function to return array of all the sellers paginated
      #
      # @return [array of sellers object]
      def paginated_seller(paginate_param)
        if paginate_param[:state].present?
          return get_seller_by_status(paginate_param)
        else
          return get_all_sellers(paginate_param)
        end
      end

      #
      # function to return paginated sellers based on the status
      #
      def get_seller_by_status(paginate_param)
        set_pagination_properties(paginate_param)
        seller = SELLER_MODEL.where({ status: @state }).order(@sort_order)
        return get_paginated_seller(seller)
      end

      #
      # Returns all the sellers
      #
      # @return [Array] array of seller model
      #
      def get_all_sellers(paginate_param)
        valid_paginate_params(paginate_param)
        set_pagination_properties(paginate_param)
        seller = SELLER_MODEL.where.not({ status: COMMON_MODEL_STATES::DELETED }).order(@sort_order)
        return get_paginated_seller(seller)
      end

      # 
      # function to validate the paginated params
      #
      def valid_paginate_params(paginate_params)    
        if paginate_params[:order].present? && paginate_params[:order] != 'ASC' && paginate_params[:order] != 'DESC'
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WRONG_ORDER)
        end
        if paginate_params[:sort_by].present? && !SELLER_MODEL.attribute_names.include?(paginate_params[:sort_by])
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::VALID_SORT_BY)
        end
      end

      #
      # function to set pagination properties
      #
      def set_pagination_properties(paginate_param)
        @per_page = (paginate_param[:per_page] || PAGINATION_UTIL::PER_PAGE).to_f
        @page_no = (paginate_param[:page_no] || PAGINATION_UTIL::PAGE_NO).to_f
        sort_by = paginate_param[:sort_by] || PAGINATION_UTIL::SORT_BY
        order = paginate_param[:order] || PAGINATION_UTIL::ORDER
        @sort_order = sort_by + ' ' + order
        @state = paginate_param[:state]
      end

      #
      # function to paginate the seller
      #
      def get_paginated_seller(seller)
        page_count = (seller.count / @per_page).ceil
        paginated_seller = seller.limit(@per_page).offset((@page_no - 1) * @per_page)
        return { seller: paginated_seller, page_count: page_count }
      end

      #
      # Find the seller by name
      #
      # @param name [String] seller name
      #
      # @return [SellerModel] return seller model else nil
      #
      def get_seller_by_name(name)
        SELLER_MODEL.find_by(name: name)
      end

      # 
      # function to change the state of the seller based on the event to trigger
      # @param args [args] [hash] contains the event to trigger the status change
      # 
      # @return [Seller object]
      def change_state(args)
        seller = get_seller_by_id(args[:id])
        case args[:event].to_i
        when COMMON_EVENTS::ACTIVATE
          seller.trigger_event('activate')
        when COMMON_EVENTS::DEACTIVATE
          seller.trigger_event('deactivate')
        when COMMON_EVENTS::SOFT_DELETE
          seller.trigger_event('soft_delete')
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
        end
        return seller
      end

      #
      # Update the given seller
      #
      # @param args [hash] seller update details
      # @param sbp [Model]  seller
      #
      # @return [ seller model] updated  seller
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, seller)
        begin
          seller.update_attributes!(args)
          return seller
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update Seller, ' + e.message)
        end
      end
    end
  end
end
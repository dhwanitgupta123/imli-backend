#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Data Access Object to interact with InventoryModel
    #
    class InventoryDao < SupplyChainBaseModule::V1::BaseDao

      INVENTORY_MODEL = SupplyChainModule::V1::Inventory

      def initialize
        super
      end

      def create(args)
        inventory_model = INVENTORY_MODEL
        create_model(args, inventory_model, 'Inventory')
      end

      def update(args, inventory)
        update_model(args, inventory, 'Inventory')
      end

      def get_by_id(id)
        get_model_by_id(INVENTORY_MODEL, id)
      end

      def get_all(pagination_params = {})
        get_all_element(INVENTORY_MODEL, pagination_params)
      end
    end
  end
end

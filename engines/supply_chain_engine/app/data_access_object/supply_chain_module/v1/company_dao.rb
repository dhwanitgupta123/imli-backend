#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Data Access Object to interact with CompanyModel
    #
    class CompanyDao < SupplyChainBaseModule::V1::BaseDao
      COMPANY_MODEL = SupplyChainModule::V1::Company
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates
      CONTENT = CommonModule::V1::Content
      def initialize(params={})
        @params = params
        super
      end

      #
      # Create the company
      #
      # @param args [name] company name
      #
      # @return [companyModel]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        new_company = COMPANY_MODEL.new(args)

        begin
          new_company.save!
          return new_company
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create Company, ' + e.message)
        end
      end

      #
      # Update the given company with name
      #
      # @param name [String] new company name
      # @param company [Model] companyModel
      #
      # @return [companyModel] updated company
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, company)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::COMPANY_MISSING) if company.nil? || args.nil?
        begin
          company.update_attributes!(args)
          return company
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update Company, ' + e.message)
        end
      end

      #
      # Find the company by id
      #
      # @param name [Integer] company id
      #
      # @return [companyModel] return company model else nil
      #
      def get_company_by_id(company_id)
        COMPANY_MODEL.find_by(id: company_id)
      end

      #
      # Returns all the companies
      #
      # @return [Array] array of company model
      #
      def get_all(pagination_params = {})
        get_all_element(COMPANY_MODEL, pagination_params)
      end

      #
      # Find the company by name
      #
      # @param name [String] company name
      #
      # @return [companyModel] return company model else nil
      #
      def get_company_by_name(name)
        COMPANY_MODEL.find_by(name: name)
      end

      # validate if company exists or not if exists then call dao to change the state of company
      # else return error
      #
      # @param company_id [String] company id of which status is to be changed
      # @param event [String] event to trigger to change the status 
      #
      # @error [InvalidArgumentsError] if company with give company_id doesn't exists or if it fails to update state
      #
      def change_company_state(company_id, event)
        company = COMPANY_MODEL.find_by(id: company_id)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::COMPANY_NOT_FOUND) if company.nil?
        change_state(company, event)
        return company
      end

      #
      # This function gets the label of all the companies
      # which have non-deleted brands and sub_brands under it
      # 
      # @return [Json Response]
      #
      def get_companies_tree
        aggregated_data = COMPANY_MODEL.includes(brands: :sub_brands).
        where.not(status: COMMON_MODEL_STATES::DELETED).
        where.not(brands: { status: COMMON_MODEL_STATES::DELETED }).
        where.not(sub_brands: { status: COMMON_MODEL_STATES::DELETED })

        return aggregated_data
      end
    end
  end
end

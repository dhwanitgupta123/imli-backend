#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Data Access Object to interact with Warehouse Model
    #
    class WarehouseDao < SupplyChainBaseModule::V1::BaseDao
      WAREHOUSE_MODEL = SupplyChainModule::V1::Warehouse
      WBP_DAO = WarehouseProductModule::V1::WarehouseBrandPackDao
      PAGINATION_UTIL = CommonModule::V1::Pagination
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      COMMON_STATES = SupplyChainCommonModule::V1::CommonStates
      CATEGORY_UTIL = CategorizationModule::V1::CategoryUtil
      SUB_BRAND_UTIL = MasterProductModule::V1::SubBrandUtil
      PRODUCT_UTIL = MasterProductModule::V1::ProductUtil
      WBP_MODEL = WarehouseProductModule::V1::WarehouseBrandPack
      def initialize(params = {})
        @params = params
        super
      end

      #
      # Create the warehouse
      #
      # @param args [name] warehouse name
      #
      # @return [warehouse model]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        new_warehouse = WAREHOUSE_MODEL.new(args)

        begin
          new_warehouse.save!
          return new_warehouse
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create Warehouse, ' + e.message)
        end
      end

      #
      # Update the given warehouse
      #
      # @param args [hash] warehouse update details
      # @param wbp [Model] Warehouse
      #
      # @return [Warehouse model] updated warehouse
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, warehouse)
        begin
          warehouse.update_attributes!(args)
          return warehouse
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update Warehouse, ' + e.message)
        end
      end

      #
      # Find the warehouse by id
      #
      # @param name [Integer] warehouse id
      #
      # @return [companyModel] return warehouse model else nil
      #
      def get_warehouse_by_id(warehouse_id)
        begin
          WAREHOUSE_MODEL.find(warehouse_id)
        rescue ActiveRecord::RecordNotFound => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_NOT_FOUND)
        end
      end

      #
      # function to return array of all the warehouses paginated
      #
      # @return [array of warehouse object]
      def paginated_warehouse(paginate_param)
        paginated_warehouses = get_all_element(WAREHOUSE_MODEL, paginate_param)
        return { warehouse: paginated_warehouses[:elements], page_count: paginated_warehouses[:page_count] }
      end

      # 
      # function to change the state of thewarehouse based on the event to trigger
      # @param args [args] [hash] contains the event to trigger the status change
      # 
      # @return [warehouse object]
      def change_state(args)
        warehouse = get_warehouse_by_id(args[:id])
        case args[:event].to_i
        when COMMON_EVENTS::ACTIVATE
          warehouse.trigger_event('activate')
        when COMMON_EVENTS::DEACTIVATE
          warehouse.trigger_event('deactivate')
        when COMMON_EVENTS::SOFT_DELETE
          warehouse.trigger_event('soft_delete')
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
        end
        return warehouse
      end

      #
      # Function to fetch WBPS based on filters & pagination params
      #
      def get_filtered_wbps_by_pagination_params(args)
        sub_categories = CATEGORY_UTIL.get_sub_categories({
          department_id: args[:department_id],
          category_id: args[:category_id],
          sub_category_id: args[:sub_category_id]
        })
        sub_brands = SUB_BRAND_UTIL.get_sub_brands({
          company_id: args[:company_id],
          brand_id: args[:brand_id],
          sub_brand_id: args[:sub_brand_id]
        })
        warehouse = get_warehouse_by_id(args[:id])
        if sub_categories.present? || sub_brands.present?
          if sub_categories.present?
            wbps = get_wbp_by_sub_categories(sub_categories, warehouse)
          else
            wbps = get_wbp_joined_with_filter_table(warehouse)
          end
          if sub_brands.present?
            products = PRODUCT_UTIL.filter_products({ sub_brands: sub_brands })
            wbps = get_wbp_by_products(wbps, products)
          end
        else
          wbps = get_wbp_joined_with_filter_table(warehouse)
        end
        paginated_wbps = get_paginated_element(WBP_MODEL, wbps, args)
        return {
          warehouse_brand_packs: paginated_wbps[:elements],
          page_count: paginated_wbps[:page_count]
        }
      end

      #
      # Function to get WBPS based on sub categories to which they belong
      # 
      def get_wbp_by_sub_categories(sub_categories, warehouse)
        wbp_dao = WBP_DAO.new(@params)
        return wbp_dao.get_wbp_by_sub_categories(sub_categories, warehouse)
      end

      #
      # Function to get WBPS based on products to which they belong
      # 
      def get_wbp_by_products(wbps, products)
        wbp_dao = WBP_DAO.new(@params)
        return wbp_dao.get_wbp_by_products(wbps, products)
      end

      #
      # Function to get WBP wcich include Products table
      #
      def get_wbp_joined_with_filter_table(warehouse)
        wbp_dao = WBP_DAO.new(@params)
        return wbp_dao.get_wbp_joined_with_filter_table(warehouse)
      end
    end
  end
end

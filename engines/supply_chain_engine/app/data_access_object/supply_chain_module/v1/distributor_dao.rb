#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Data Access Object to interact with DistributorModel
    #
    class DistributorDao < SupplyChainBaseModule::V1::BaseDao

      DISTRIBUTOR_MODEL = SupplyChainModule::V1::Distributor
      COMPANY_DAO = SupplyChainModule::V1::CompanyDao

      def initialize
        super
      end

      def create(args)
        verify_company_args(args)
        distributor_model = DISTRIBUTOR_MODEL
        distributor = create_model(args, distributor_model, 'Distributor')
        if args[:email_ids].present?
          distributor.email_ids = args[:email_ids]
        end
        distributor.save!
        return distributor
      end

      def update(args, distributor)
        verify_company_args(args)
        update_model(args, distributor, 'Distributor')
        if args[:email_ids].present?
          distributor.email_ids = args[:email_ids]
        end
        distributor.save!
        return distributor
      end

      def get_by_id(id)
        get_model_by_id(DISTRIBUTOR_MODEL, id)
      end

      def get_all(pagination_params = {})
        get_all_element(DISTRIBUTOR_MODEL, pagination_params)
      end

      def verify_company_args(args)
        if args.present? && (args[:company_id]).present?
          company_dao = COMPANY_DAO.new
          company = company_dao.get_company_by_id(args[:company_id])
          if company.blank?
            raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT::COMPANY_NOT_FOUND)
          end
        end
      end

      #
      # Function to fetch distributor by it's vendor code
      #
      def get_distributor_by_vendor_code(vendor_code)
        distributor = DISTRIBUTOR_MODEL.where(vendor_code: vendor_code).first
        if distributor.blank?
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT::VENDOR_CODE_NOT_PRESENT%{code: vendor_code})
        end
        return distributor
      end
    end
  end
end

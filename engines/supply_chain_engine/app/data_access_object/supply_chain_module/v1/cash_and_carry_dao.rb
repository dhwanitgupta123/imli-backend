#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Data Access Object to interact with CashAndCarry model
    #
    class CashAndCarryDao < SupplyChainBaseModule::V1::BaseDao
      CASH_AND_CARRY_MODEL = SupplyChainModule::V1::CashAndCarry
      PAGINATION_UTIL = CommonModule::V1::Pagination
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      COMMON_STATES = SupplyChainCommonModule::V1::CommonStates
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      def initialize
        super
      end

      #
      # Create the C&C
      #
      # @param args [name] C&C name
      #
      # @return [C&C model]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        new_c_and_c = CASH_AND_CARRY_MODEL.new(args)

        begin
          new_c_and_c.save!
          return new_c_and_c
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create Cash & Carry, ' + e.message)
        end
      end

      #
      # Update the given c&c
      #
      # @param args [hash] c&c update details
      # @param wbp [Model] c&c
      #
      # @return [c&c model] updated c&c
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, c_and_c)
        begin
          c_and_c.update_attributes!(args)
          return c_and_c
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update Cash & Carry, ' + e.message)
        end
      end

      #
      # Find the c&c by id
      #
      # @param name [Integer] c&c id
      #
      # @return [c&c model] return c&c model else nil
      #
      def get_cash_and_carry_by_id(c_and_c_id)
        begin
          CASH_AND_CARRY_MODEL.find(c_and_c_id)
        rescue ActiveRecord::RecordNotFound => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::CASH_AND_CARRY_NOT_FOUND)
        end
      end

      #
      # function to return array of all the cash and carriers paginated
      #
      # @return [array of cash & carry object]
      def paginated_cash_and_carry(paginate_param)
        valid_paginate_params(paginate_param)
        if paginate_param[:state].present?
          return get_cash_and_carry_by_status(paginate_param)
        else
          return get_all_cash_and_carry(paginate_param)
        end
      end

      # 
      # function to validate the paginated params
      #
      def valid_paginate_params(paginate_params)
        if paginate_params[:order].present? && paginate_params[:order] != 'ASC' && paginate_params[:order] != 'DESC'
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WRONG_ORDER)
        end
        if paginate_params[:sort_by].present? && !CASH_AND_CARRY_MODEL.attribute_names.include?(
          paginate_params[:sort_by])
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::VALID_SORT_BY)
        end
      end

      #
      # function to return paginated cash_and_carriers based on the status
      #
      def get_cash_and_carry_by_status(paginate_param)
        set_pagination_properties(paginate_param)
        cash_and_carry = CASH_AND_CARRY_MODEL.where({ status: @state }).order(@sort_order)
        return get_paginated_cash_and_carry(cash_and_carry)
      end

      #
      # function to return paginated all cash and carriers
      #
      def get_all_cash_and_carry(paginate_param)
        set_pagination_properties(paginate_param)
        cash_and_carry = CASH_AND_CARRY_MODEL.where.not({ status: COMMON_STATES::DELETED }).order(@sort_order)
        return get_paginated_cash_and_carry(cash_and_carry)
      end

      #
      # function to set pagination properties
      #
      def set_pagination_properties(paginate_param)
        @per_page = (paginate_param[:per_page] || PAGINATION_UTIL::PER_PAGE).to_f
        @page_no = (paginate_param[:page_no] || PAGINATION_UTIL::PAGE_NO).to_f
        sort_by = paginate_param[:sort_by] || PAGINATION_UTIL::SORT_BY
        order = paginate_param[:order] || PAGINATION_UTIL::ORDER
        @sort_order = sort_by + ' ' + order
        @state = paginate_param[:state]
      end

      #
      # function to paginate the cash and carry
      #
      def get_paginated_cash_and_carry(cash_and_carry)
        page_count = (cash_and_carry.count / @per_page).ceil
        paginated_cash_and_carry = cash_and_carry.limit(@per_page).offset((@page_no - 1) * @per_page)
        return { cash_and_carry: paginated_cash_and_carry, page_count: page_count }
      end

      # 
      # function to change the state of the c&c based on the event to trigger
      # @param args [args] [hash] contains the event to trigger the status change
      # 
      # @return [c&c object]
      def change_state(args)
        c_and_c = get_cash_and_carry_by_id(args[:id])
        case args[:event].to_i
        when COMMON_EVENTS::ACTIVATE
          c_and_c.trigger_event('activate')
        when COMMON_EVENTS::DEACTIVATE
          c_and_c.trigger_event('deactivate')
        when COMMON_EVENTS::SOFT_DELETE
          c_and_c.trigger_event('soft_delete')
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
        end
        return c_and_c
      end
    end
  end
end

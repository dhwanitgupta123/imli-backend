#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Data Access Object to interact with MarketplaceSellingPackModel
    #
    class MarketplaceSellingPackDao < SupplyChainBaseModule::V1::BaseDao

      MARKETPLACE_SELLING_PACK_MODEL = MarketplaceProductModule::V1::MarketplaceSellingPack
      MARKETPLACE_SELLING_PACK_PRICING_DAO = MarketplacePricingModule::V1::MarketplaceSellingPackPricingDao
      MARKETPLACE_SELLING_PACK_STATES = SupplyChainCommonModule::V1::CommonStates
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper
      MPSP_CATEGORY_DAO = CategorizationModule::V1::MpspCategoryDao

      def initialize
        super
      end

      def new_marketplace_selling_pack(args)

        if args[:mpsp][:images].present?
          image_id_array = IMAGE_SERVICE_HELPER.get_sorted_image_id_array_by_priority(args[:mpsp][:images])
          validate_images(image_id_array)
          args[:mpsp][:images] = image_id_array
        end

        mpsp_quantity_array = []
        args[:mpsp] = args[:mpsp].merge(dirty_bit: true)
        marketplace_selling_pack = MARKETPLACE_SELLING_PACK_MODEL.new(
          model_params(args[:mpsp], MARKETPLACE_SELLING_PACK_MODEL))

        marketplace_selling_pack.images = args[:mpsp][:images] if args[:mpsp][:images].present?

        begin          
          args[:marketplace_brand_packs].each do |mpbp|
            mpsp_quantity = MarketplaceSellingPackMarketplaceBrandPack.new(
              marketplace_brand_pack: mpbp[:marketplace_brand_pack],
              marketplace_selling_pack: marketplace_selling_pack,
              quantity: mpbp[:quantity])
            mpsp_quantity.save!
            mpsp_quantity_array.push(mpsp_quantity)
          end
          marketplace_selling_pack.marketplace_selling_pack_marketplace_brand_packs = mpsp_quantity_array
          bind_image_with_resource(marketplace_selling_pack.images, MARKETPLACE_SELLING_PACK_MODEL.name) if args[:mpsp][:images].present?
          return marketplace_selling_pack
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            'Not able to create Marketplace Brand Pack, ' + e.message)
        end
      end

      def update(args, marketplace_selling_pack)
        
        if args[:images].present?
          image_id_array = IMAGE_SERVICE_HELPER.get_sorted_image_id_array_by_priority(args[:images])
          validate_images(image_id_array)
          args[:images] = image_id_array
        end
        marketplace_selling_pack.images = args[:images] if args[:images].present?
        marketplace_selling_pack = update_model(args, marketplace_selling_pack, 'Marketplace Selling Pack')
        bind_image_with_resource(marketplace_selling_pack.images, MARKETPLACE_SELLING_PACK_MODEL.name) if args[:images].present?
        return marketplace_selling_pack
      end

      # 
      # Bind images with resource
      #
      # @param image_ids [Array] Array of image ids
      # @param resource_name [String] Mode name
      #
      def bind_image_with_resource(image_ids, resource_name)
        IMAGE_SERVICE_HELPER.bind_image_with_resource(image_ids, resource_name)
      end

      def get_by_id(id)
        marketplace_selling_pack = get_model_by_id(MARKETPLACE_SELLING_PACK_MODEL, id)
      end

      def get_all(pagination_params = {})
        get_all_element(MARKETPLACE_SELLING_PACK_MODEL, pagination_params)
      end

      #
      # Get savings and total for mpsp product
      #
      # @param quantity [Integer] [quantity of Order product]
      # @param marketplace_selling_pack [Object] [MPSP corresponding to Order product]
      #
      # @return [JSON] [hash containing savings and selling_price]
      #
      def get_savings_and_pricing_for_mpsp(quantity, marketplace_selling_pack)
        is_ladder_pricing_active = marketplace_selling_pack.is_ladder_pricing_active
        # Fetching pricing details from MPSP_pricing according to the ladder_pricing (if active)
        mpsp_pricing_dao = MARKETPLACE_SELLING_PACK_PRICING_DAO.new
        pricing_details = mpsp_pricing_dao.get_mpsp_pricing_details(quantity, marketplace_selling_pack.marketplace_selling_pack_pricing, is_ladder_pricing_active)
        return pricing_details
      end

      def save_marketplace_selling_pack(marketplace_selling_pack)
        begin
          marketplace_selling_pack.save!
          return marketplace_selling_pack
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            'Not able to save Marketplace Selling pack, ' + e.message)
        end
      end

      #
      # Joining tables MPSP, MPSP_Pricing, MPSP_LadderPricing, MPBP, MPBP_Pricing, BrandPack,
      # and SellingBrandPack all the object are used to build the response.
      # 
      # So to avoid making call to db for each mpsp first joining the table in one query
      # and then accessing the results
      # 
      def self.get_aggregated_mpsps(marketplace_selling_packs)
        marketplace_selling_packs = MARKETPLACE_SELLING_PACK_MODEL.eager_load({marketplace_selling_pack_pricing: :marketplace_selling_pack_ladder_pricings},
            marketplace_brand_packs: [:marketplace_brand_pack_pricing, :brand_pack, :seller_brand_packs]).
            where(id: marketplace_selling_packs)
      end

      # 
      # Joining MPSP, MPSP_Pricing, MPSP_LadderPricing, MPBPs and BrandPacks, this is to reduce number of queries
      # to db, without join
      # Number of Queries to db = Number of MPSPs * Number of MPBP
      # 
      # After join Number of db queries = 5
      #
      def self.get_aggregated_products(products_array)
        products_array = MARKETPLACE_SELLING_PACK_MODEL.includes({marketplace_selling_pack_pricing: :marketplace_selling_pack_ladder_pricings},
            marketplace_brand_packs: :brand_pack).where(id: products_array)
      end

      # 
      # This function returns joined table of mpsp, mpsp_pricing, mpsp_lader_pricing, mpbp, brand_pack
      # and seller_brand_packs where brand_packs are in given input brand_packs
      #
      # @param brand_packs [Model] brand packs model
      # 
      # @return [Model] Joined model 
      #
      def self.get_aggregated_mpsps_by_brand_packs(brand_packs)
        MARKETPLACE_SELLING_PACK_MODEL.includes({marketplace_selling_pack_pricing: :marketplace_selling_pack_ladder_pricings},
        marketplace_brand_packs: [:marketplace_brand_pack_pricing, :brand_pack, :seller_brand_packs]).
        where(brand_packs: {id: brand_packs}).where.not(status: MARKETPLACE_SELLING_PACK_STATES::DELETED)
      end

      #
      # Joining tables MPSP, MPSP_Pricing, MPSP_LadderPricing, MPBP, MPBP_Pricing, BrandPack,
      # and SellingBrandPack all the object are used to build the response.
      # 
      # So to avoid making call to db for each mpsp first joining the table in one query
      # and then accessing the results
      # 
      def self.get_aggregated_not_deleted_mpsps(marketplace_selling_packs)
        marketplace_selling_packs = MARKETPLACE_SELLING_PACK_MODEL.includes({marketplace_selling_pack_pricing: :marketplace_selling_pack_ladder_pricings},
            marketplace_brand_packs: [:marketplace_brand_pack_pricing, :brand_pack, :seller_brand_packs]).
            where(id: marketplace_selling_packs).where.not(status: MARKETPLACE_SELLING_PACK_STATES::DELETED)
      end

      #
      # This function return all the brand pack in the given sub_categories
      #
      def get_mpsps_by_mpsp_sub_categories(mpsp_sub_categories)
        MARKETPLACE_SELLING_PACK_MODEL.where(mpsp_sub_category: mpsp_sub_categories)
      end

      # validate if all the image_id present in image_id_array is present or not
      #
      # @param image_id_array [Array] array containing all the image_ids
      #
      # @error [InvalidArgumentsError] if image_id not present in image table
      #
      def validate_images(image_id_array)
        if IMAGE_SERVICE_HELPER.validate_images(image_id_array) == false
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::IMAGE_LINKING_FAILED)
        end
      end

      # 
      # This function return true if mpsp is active and all its parent nodes [sub_category, category, department]
      # are active else returns false
      #
      # @param marketplace_selling_pack [Object] mpsp
      # 
      # @return [Boolean] true if mpsp is active and all parent nodes are active else false
      #
      def self.is_node_active?(marketplace_selling_pack)
        mpsp_sub_category = marketplace_selling_pack.mpsp_sub_category
        return marketplace_selling_pack.active? && MPSP_CATEGORY_DAO.is_node_active?(mpsp_sub_category)
      end

      # 
      # This function join all tables required to return all information
      # corresponding to mpsp and brand_packs
      #
      # @param brand_packs [ActiveRecordRelationArray] marketplace_selling_packs
      # 
      # @return [ActiveRecordRelationArray] Joined object containing 
      #
      def self.get_aggregated_mpsps_and_master_product_details(marketplace_selling_packs)

        return [] if marketplace_selling_packs.blank?

        marketplace_selling_packs.eager_load(:marketplace_selling_pack_pricing,
                                      marketplace_brand_packs: [:marketplace_selling_pack_marketplace_brand_packs,
                                      :marketplace_brand_pack_pricing,
                                      brand_pack: [ product: [sub_brand: [brand: :company ] ] ] ], 
                                      mpsp_sub_category: [mpsp_parent_category: :mpsp_department ])
      end

    end
  end
end

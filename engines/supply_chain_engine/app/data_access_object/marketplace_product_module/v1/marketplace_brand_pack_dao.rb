#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Data Access Object to interact with MarketplaceBrandPackModel
    #
    class MarketplaceBrandPackDao < SupplyChainBaseModule::V1::BaseDao

      MARKETPLACE_BRAND_PACK_MODEL = MarketplaceProductModule::V1::MarketplaceBrandPack
      MARKETPLACE_BRAND_PACK_PRICING_DAO = MarketplacePricingModule::V1::MarketplaceBrandPackPricingDao
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      def initialize
        super
      end

      def create(args)
        mpbp_pricing_dao = MARKETPLACE_BRAND_PACK_PRICING_DAO.new
        marketplace_brand_packs = get_mpbp_by_brand_pack(args[:mpbp][:brand_pack_id])
        if marketplace_brand_packs.present?
          marketplace_brand_pack = marketplace_brand_packs.first
        end
        if marketplace_brand_pack.blank?
          marketplace_brand_pack = MARKETPLACE_BRAND_PACK_MODEL.new(
            model_params(args[:mpbp], MARKETPLACE_BRAND_PACK_MODEL))
        end
        marketplace_brand_pack.seller_brand_packs << args[:sbp]
        marketplace_brand_pack.marketplace_brand_pack_pricing = mpbp_pricing_dao.new_marketplace_brand_pack_pricing(
          args[:pricing])
        begin
          marketplace_brand_pack.save!
          return marketplace_brand_pack
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            'Not able to create Marketplace Brand Pack, ' + e.message)
        end
      end

      def update(args, marketplace_brand_pack)
        mpbp_pricing_dao = MARKETPLACE_BRAND_PACK_PRICING_DAO.new
        if args[:sbp].present? && args[:mpbp][:brand_pack_id].present?
          marketplace_brand_pack.seller_brand_packs << args[:sbp]
        end 
        marketplace_brand_pack.update_attributes!(model_params(args[:mpbp], MARKETPLACE_BRAND_PACK_MODEL))
        marketplace_brand_pack.marketplace_brand_pack_pricing = mpbp_pricing_dao.update_marketplace_brand_pack_pricing(
          args[:pricing], marketplace_brand_pack.marketplace_brand_pack_pricing)
        begin
          marketplace_brand_pack.save!
          return marketplace_brand_pack
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            'Not able to update Marketplace Brand Pack, ' + e.message)
        end
      end

      def get_by_id(id)
        get_model_by_id(MARKETPLACE_BRAND_PACK_MODEL, id)
      end

      def get_all(pagination_params = {})
        get_all_element(MARKETPLACE_BRAND_PACK_MODEL, pagination_params)
      end

      #
      # Find the MPBP by brand pack
      #
      # @param brand pack id [integer]
      #
      # @return [MPBP model] return MPBP model else nil
      #
      def get_mpbp_by_brand_pack(brand_pack_id)
        MARKETPLACE_BRAND_PACK_MODEL.where({ brand_pack_id: brand_pack_id })
      end
    end
  end
end

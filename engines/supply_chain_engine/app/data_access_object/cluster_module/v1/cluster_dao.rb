#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # Data Access Object to interact with ClusterContext Model
    #
    class ClusterDao < SupplyChainBaseModule::V1::BaseDao

      CLUSTER_MODEL = ClusterModule::V1::Cluster
      CLUSTER_CONTEXT_DAO = ClusterModule::V1::ClusterContextDao
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper
      MARKETPLACE_SELLING_PACK_DAO = MarketplaceProductModule::V1::MarketplaceSellingPackDao
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      def initialize
        super
      end

      # 
      # create cluster
      #
      # @param args [Hash] model args
      # 
      # @return [Model] Cluster
      #
      def create(args)

        cluster_context_dao = CLUSTER_CONTEXT_DAO.new
        cluster = create_model(args, CLUSTER_MODEL, 'CLUSTER')
        cluster.marketplace_selling_pack_ids = args[:mpsp_ids] if args[:mpsp_ids].present?
        cluster_context = cluster_context_dao.create(args[:context].merge(cluster_id: cluster.id))
        cluster.images = args[:images] if args[:images].present?
        cluster.save
        bind_image_with_resource(cluster.images, CLUSTER_MODEL.name) if args[:images].present?
        return cluster
      end


      # 
      # update cluster
      #
      # @param args [Hash] model args
      # @param cluster_context [Model] cluster
      # 
      # @return [Model] Cluster
      #
      def update(args, cluster)

        cluster = update_model(args, cluster, 'CLUSTER')
        cluster.marketplace_selling_pack_ids = args[:mpsp_ids] if args[:mpsp_ids].present?
        cluster.images = args[:images] if args[:images].present?
        cluster.save

        cluster_context_dao = CLUSTER_CONTEXT_DAO.new
        cluster_context_dao.update(args[:context], cluster.cluster_context) if args[:context].present?

        bind_image_with_resource(cluster.images, CLUSTER_MODEL.name) if args[:images].present?
        return cluster
      end

      #
      # Return cluster by input id or nil if not present
      #
      # @param id [Integer] cluster id
      # 
      # @return [Model] cluster
      #
      def get_by_id(id)
        get_model_by_id(CLUSTER_MODEL, id)
      end

      # 
      # Return clusters after applying all the pagination_params
      #
      # @param filters [Hash] containg condition of pagination
      # 
      # @return [Array] Array of cluster satisfying filters
      #
      def get_all(pagination_params = {})
        get_all_element(CLUSTER_MODEL, pagination_params)
      end

      # 
      # Return clusters after applying all the filters
      #
      # @param filters [Hash] containg condition of pagination
      # 
      # @return [Array] Array of cluster satisfying filters
      #
      def get_clusters_by_level_and_level_id(filters)
        set_pagination_properties(filters, CLUSTER_MODEL)

        clusters = CLUSTER_MODEL.eager_load(:cluster_context).where.not(status: COMMON_MODEL_STATES::DELETED).
                                                   where(cluster_contexts: {level: filters[:level], level_id: filters[:level_id]}).
                                                   order(@sort_order).limit(@per_page).offset((@page_no - 1) * @per_page)

        page_count = (clusters.count / @per_page).ceil
        return {elements: clusters, page_count: page_count}
      end

      # 
      # Return clusters after applying all the filters
      #
      # @param filters [Hash] containg condition of pagination
      # 
      # @return [Array] Array of cluster satisfying filters
      #
      def get_clusters_by_level(filters)
        set_pagination_properties(filters, CLUSTER_MODEL)

        clusters = CLUSTER_MODEL.eager_load(:cluster_context).where.not(status: COMMON_MODEL_STATES::DELETED).
                                                   where(cluster_contexts: {level: filters[:level]}).
                                                   order(@sort_order).limit(@per_page).offset((@page_no - 1) * @per_page)
        page_count = (clusters.count / @per_page).ceil
        return {elements: clusters, page_count: page_count}
      end

      # 
      # Return clusters after applying all the filters
      #
      # @param filters [Hash] containg condition of pagination
      # 
      # @return [Array] Array of cluster satisfying filters
      #
      def get_clusters_by_level_id(filters)
        set_pagination_properties(filters, CLUSTER_MODEL)
        clusters = CLUSTER_MODEL.eager_load(:cluster_context).where.not(status: COMMON_MODEL_STATES::DELETED).
                                                   where(cluster_contexts: {level_id: filters[:level_id]}).
                                                   order(@sort_order).limit(@per_page).offset((@page_no - 1) * @per_page)
        page_count = (clusters.count / @per_page).ceil
        return {elements: clusters, page_count: page_count}
      end

      # 
      # Return clusters after applying all the filters
      #
      # @param filters [Hash] containg condition of pagination
      # 
      # @return [Array] Array of cluster satisfying filters
      #
      def get_global_clusters(filters)
        set_pagination_properties(filters, CLUSTER_MODEL)

        clusters = CLUSTER_MODEL.eager_load(:cluster_context).where.not(status: COMMON_MODEL_STATES::DELETED).
                                                   where(cluster_contexts: {is_global: true}).
                                                   order(@sort_order).limit(@per_page).offset((@page_no - 1) * @per_page)
        page_count = (clusters.count / @per_page).ceil
        return {elements: clusters, page_count: page_count}
      end

      # 
      # Bind images with resource
      #
      # @param image_ids [Array] Array of image ids
      # @param resource_name [String] Mode name
      #
      def bind_image_with_resource(image_ids, resource_name)
        IMAGE_SERVICE_HELPER.bind_image_with_resource(image_ids, resource_name)
      end
    end
  end
end

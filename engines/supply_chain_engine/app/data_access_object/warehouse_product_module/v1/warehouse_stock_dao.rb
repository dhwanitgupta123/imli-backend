#
# Module to handle all the functionalities related to warehouse product module
# 
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Data Access Object to interact with Warehouse stock Model
    #
    class WarehouseStockDao < SupplyChainBaseModule::V1::BaseDao
      WAREHOUSE_STOCK_MODEL = WarehouseProductModule::V1::WarehouseStock
      WAREHOUSE_STOCK_STATES = WarehouseProductModule::V1::ModelStates::V1::WarehouseStockStates
      def initialize(params={})
        @params = params
        super
      end

      #
      # Function to create the warehouse stock object
      #
      def create_warehouse_stock(param)
        warehouse_stock = WAREHOUSE_STOCK_MODEL.new(model_params(param, WAREHOUSE_STOCK_MODEL))
        begin
          warehouse_stock.save!
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to stock the product, ' + e.message)
        end
        return warehouse_stock
      end

      #
      # function to stock new inward of WBP
      #
      def stock_new_warehouse_brand_pack(param)
        return create_warehouse_stock({
          quantity: param[:quantity],
          date_of_purchase: param[:date_of_purchase],
          status: WAREHOUSE_STOCK_STATES::INWARD
          })
      end

      #
      # function to stock outward of WBP
      #
      def outward_warehouse_brand_pack(quantity)
        param = {
          status: WAREHOUSE_STOCK_STATES::OUTWARD,
          quantity: quantity
        }
        return create_warehouse_stock(param)
      end

      #
      # function to stock inward of WBP
      #
      def inward_warehouse_brand_pack(quantity)
        param = {
          status: WAREHOUSE_STOCK_STATES::INWARD,
          quantity: quantity
        }
        return create_warehouse_stock(param)
      end

      #
      # function to stock return/cancelled inward of WBP
      #
      def inward_cancelled_warehouse_brand_pack(quantity)
        param = {
          status: WAREHOUSE_STOCK_STATES::CANCELLED_INWARD,
          quantity: quantity
        }
        return create_warehouse_stock(param)
      end

      #
      # function to stock expired/damaged WBP
      #
      def stock_warehouse_brand_pack_status(param)
        param = {
          status: param[:status],
          quantity: param[:quantity]
        }
        return create_warehouse_stock(param)
      end

      #
      # Function to audit the stock of WBP
      #
      def audit_warehouse_brand_pack(warehouse_stocks)
        stock_details = get_stocks_for_wbp(warehouse_stocks)
        stock_numbers = get_stock_details(stock_details)
        param = {
          status: WAREHOUSE_STOCK_STATES::AUDITED,
          quantity: stock_numbers[:available]
        }
        return create_warehouse_stock(param)
      end

      #
      # Function to calculate totalavailable, inward outward etc.
      #
      def get_wbp_stock_details(warehouse_stocks, outbound_frequency_period_in_days=0)
        return { available: 0, inward: 0, expired: 0, damaged: 0 , outward: 0, last_audit: 0 , audit_date: 0 } if warehouse_stocks.blank?
        last_audit_stock = warehouse_stocks.where(status: WAREHOUSE_STOCK_STATES::AUDITED).order(id: :desc).first
        last_audit = last_audit_stock.present? ? last_audit_stock.quantity : 0
        audit_date = last_audit_stock.present? ? last_audit_stock.created_at : 0
        stock_details = get_stocks_for_wbp(warehouse_stocks)
        stock_numbers = get_stock_details(stock_details)
        if outbound_frequency_period_in_days.present?
          sold_frequency = get_sold_freq_in_last_x_days(outbound_frequency_period_in_days, warehouse_stocks)
        end
        return {
          available: stock_numbers[:available],
          inward: stock_numbers[:inward],
          expired: stock_numbers[:expired],
          damaged: stock_numbers[:damaged],
          outward: stock_numbers[:outward],
          sold_in_x_days: sold_frequency,
          last_audit: last_audit,
          audit_date: audit_date.to_i
        }
      end

      private

      #
      # Function to get the stock objects of WBP after last audit
      #
      def get_stocks_for_wbp(warehouse_stocks)
        last_audit_stock = warehouse_stocks.where(status: WAREHOUSE_STOCK_STATES::AUDITED).order(id: :desc).first
        if last_audit_stock.present?
          stock_details = warehouse_stocks.where('id >= :last_audit_id', { last_audit_id: last_audit_stock.id })
        else
          stock_details = warehouse_stocks
        end
        return stock_details
      end

      #
      # Function to get count of available, expired, damaged products
      #
      def get_stock_details(stock_details)
        inward = 0
        outward = 0
        available = 0
        expired = 0
        damaged = 0
        audited = 0
        stock_details.each do |stock|
          if stock.status == WAREHOUSE_STOCK_STATES::INWARD
            inward += stock.quantity
          end
          if stock.status == WAREHOUSE_STOCK_STATES::EXPIRED
            expired += stock.quantity
          end
          if stock.status == WAREHOUSE_STOCK_STATES::DAMAGED
            damaged += stock.quantity
          end
          if stock.status == WAREHOUSE_STOCK_STATES::OUTWARD
            outward += stock.quantity
          end
          if stock.status == WAREHOUSE_STOCK_STATES::AUDITED
            audited += stock.quantity
          end
          if stock.status == WAREHOUSE_STOCK_STATES::CANCELLED_INWARD
            inward += stock.quantity
          end
          available = audited + inward - expired - damaged - outward
        end
        return { available: available, inward: inward, expired: expired, damaged: damaged, outward: outward }
      end

      #
      # Function to fetch how many WBP sold in last x days
      # 
      def get_sold_freq_in_last_x_days(outbound_frequency_period_in_days=0, warehouse_stocks)
        to_time = Time.zone.now
        from_time = to_time - outbound_frequency_period_in_days.days
        outward_stocks = warehouse_stocks.where(status: WAREHOUSE_STOCK_STATES::OUTWARD, created_at: [from_time..to_time])
        sold_frequency = 0
        if outward_stocks.present?
          outward_stocks.each do |outward_stock|
            sold_frequency += outward_stock.quantity
          end
        end
        cancelled_inward_stocks = warehouse_stocks.where(status: WAREHOUSE_STOCK_STATES::CANCELLED_INWARD, created_at: [from_time..to_time])
        if cancelled_inward_stocks.present?
          cancelled_inward_stocks.each do |stock|
            sold_frequency -= stock.quantity
          end
        end
        return sold_frequency
      end
    end
  end
end

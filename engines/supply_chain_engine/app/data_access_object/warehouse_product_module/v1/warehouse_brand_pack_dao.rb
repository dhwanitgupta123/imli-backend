#
# Module to handle all the functionalities related to warehouse brand pack
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Data Access Object to interact with WarehouseBrandPack Model
    #
    class WarehouseBrandPackDao < SupplyChainBaseModule::V1::BaseDao
      WBP_MODEL = WarehouseProductModule::V1::WarehouseBrandPack
      PAGINATION_UTIL = CommonModule::V1::Pagination
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      COMMON_STATES = SupplyChainCommonModule::V1::CommonStates
      CONTENT = CommonModule::V1::Content
      WAREHOUSE_PRICING_DAO = WarehousePricingModule::V1::WarehousePricingDao
      WAREHOUSE_DAO = SupplyChainModule::V1::WarehouseDao
      CATEGORY_UTIL = CategorizationModule::V1::CategoryUtil
      def initialize(params={})
        @params = params
        super
      end

      #
      # Create the WBP
      #
      # @param args [hash] wbp details
      #
      # @return [wbp model]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        warehouse_pricing_dao = WAREHOUSE_PRICING_DAO.new
        warehouse_brand_packs = get_wbp_by_brand_pack_warehouse(args[:wbp])
        if warehouse_brand_packs.present?
          warehouse_brand_pack = warehouse_brand_packs.first
          warehouse_brand_pack.threshold_stock_value = args[:wbp][:threshold_stock_value] if args[:wbp][:threshold_stock_value].present?
          warehouse_brand_pack.outbound_frequency_period_in_days = args[:wbp][:outbound_frequency_period_in_days] if args[:wbp][:outbound_frequency_period_in_days].present?
        end
        if warehouse_brand_pack.blank?
          warehouse_brand_pack = WBP_MODEL.new(args[:wbp])
        end
        warehouse_brand_pack.inventory_brand_packs << args[:inventory_brand_pack]
        if warehouse_brand_pack.warehouse_pricings.blank?
          warehouse_brand_pack.warehouse_pricings << warehouse_pricing_dao.new_warehouse_pricing(args[:pricing])
        end
        begin
          warehouse_brand_pack.save!
          return warehouse_brand_pack
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create Warehouse Brand Pack, ' + e.message)
        end
      end

      #
      # Update the given wbp
      #
      # @param args [hash] wbp update details
      # @param wbp [Model] WarehousebrandPack
      #
      # @return [WBP model] updated WBP
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, wbp)
        warehouse_pricing_dao = WAREHOUSE_PRICING_DAO.new
        wbp.warehouse_id = args[:wbp][:warehouse_id] if args[:wbp][:warehouse_id].present?
        if args[:inventory_brand_pack].present? && args[:wbp][:brand_pack_id].present?
          wbp.brand_pack_id = args[:wbp][:brand_pack_id]
          unless wbp.inventory_brand_packs.include? args[:inventory_brand_pack]
            wbp.inventory_brand_packs << args[:inventory_brand_pack]
          end
        end
        if args[:wbp][:outbound_frequency_period_in_days].present?
          wbp.outbound_frequency_period_in_days = args[:wbp][:outbound_frequency_period_in_days]
        end
        if args[:wbp][:threshold_stock_value].present?
          wbp.threshold_stock_value = args[:wbp][:threshold_stock_value]
        end
        warehouse_pricing_dao.update_warehouse_pricing(args[:pricing], wbp.warehouse_pricings.first) if args[:pricing].present?
        begin
          wbp.save!
          return wbp
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update Warehouse Brand Pack, ' + e.message)
        end
      end

      #
      # Find the wbp by id
      #
      # @param args [Integer] wbp id
      #
      # @return [companyModel] return wbp model else nil
      #
      def get_wbp_by_id(wbp_id)
        begin
          WBP_MODEL.find(wbp_id)
        rescue ActiveRecord::RecordNotFound => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_BRAND_PACK_NOT_FOUND)
        end
      end

      #
      # Find the wbp by brand pack & warehouse
      #
      # @param args [hash] contains brand pack and warehouse
      #
      # @return [WBP model] return wbp model else nil
      #
      def get_wbp_by_brand_pack_warehouse(args)
        WBP_MODEL.where({ warehouse_id: args[:warehouse_id], brand_pack_id: args[:brand_pack_id] })
      end

      #
      # function to return array of all the WBPs paginated
      #
      # @return [array of WBP object]
      def paginated_wbps(paginate_param)
        valid_paginate_params(paginate_param)
        if paginate_param[:state].present?
          return get_wbps_by_status(paginate_param)
        else
          return get_all_wbps(paginate_param)
        end
      end

      # 
      # function to validate the paginated params
      #
      def valid_paginate_params(paginate_params)
        if paginate_params[:order].present? && paginate_params[:order] != 'ASC' && paginate_params[:order] != 'DESC'
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WRONG_ORDER)
        end
        if paginate_params[:sort_by].present? && !WBP_MODEL.attribute_names.include?(paginate_params[:sort_by])
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::VALID_SORT_BY)
        end
      end

      #
      # function to return paginated Wbps based on the status
      #
      def get_wbps_by_status(paginate_param)
        set_pagination_properties(paginate_param)
        wbps = WBP_MODEL.where({ status: @state }).order(@sort_order)
        return get_paginated_wbps(wbps)
      end

      #
      # function to return paginated all WBPs
      #
      def get_all_wbps(paginate_param)
        set_pagination_properties(paginate_param)
        wbps = WBP_MODEL.where.not({ status: COMMON_STATES::DELETED }).order(@sort_order)
        return get_paginated_wbps(wbps)
      end

      #
      # function to set pagination properties
      #
      def set_pagination_properties(paginate_param)
        @per_page = (paginate_param[:per_page] || PAGINATION_UTIL::PER_PAGE).to_f
        @page_no = (paginate_param[:page_no] || PAGINATION_UTIL::PAGE_NO).to_f
        sort_by = paginate_param[:sort_by] || PAGINATION_UTIL::SORT_BY
        order = paginate_param[:order] || PAGINATION_UTIL::ORDER
        @sort_order = sort_by + ' ' + order
        @state = paginate_param[:state]
      end

      #
      # function to paginate the WBPs
      #
      def get_paginated_wbps(wbps)
        page_count = (wbps.count / @per_page).ceil
        paginated_wbps = wbps.limit(@per_page).offset((@page_no - 1) * @per_page)
        return { warehouse_brand_pack: paginated_wbps, page_count: page_count }
      end

      # 
      # function to change the state of the wbp based on the event to trigger
      # @param args [args] [hash] contains the event to trigger the status change
      # 
      # @return [warehouse object]
      def change_state(args)
        wbp = get_wbp_by_id(args[:id])
        case args[:event].to_i
        when COMMON_EVENTS::ACTIVATE
          wbp.trigger_event('activate')
        when COMMON_EVENTS::DEACTIVATE
          wbp.trigger_event('deactivate')
        when COMMON_EVENTS::SOFT_DELETE
          wbp.trigger_event('soft_delete')
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
        end
        return wbp
      end

      #
      # Function to fetch wbp based on BP & warheouse
      #
      def get_wbp_by_parameters(brand_pack, warehouse)
        wbp = WBP_MODEL.where(brand_pack: brand_pack, warehouse: warehouse).first
      end

      #
      # Function to fetch WBP by warheouse id
      #
      def get_warehouse_brand_pack_by_warehouse_id(warehouse_id)
        wbps = WBP_MODEL.where(warehouse_id: warehouse_id)
        return { warehouse_brand_pack: wbps }
      end

      #
      # Function to fetch WBP based on Filters of sub categories
      #
      def get_wbp_by_sub_categories(sub_categories, warehouse)
        return WBP_MODEL.includes(warehouse_pricings: :warehouse_stocks).includes(
          :warehouse).includes(brand_pack: :sub_category).includes(brand_pack: :product).where(
          warehouse: warehouse, brand_packs: {sub_category_id: sub_categories})
      end

      #
      # Function to fetch WBP based on Filters of Products
      #
      def get_wbp_by_products(wbps, products)
        return wbps.where(brand_packs: { product_id: products })
      end

      #
      # Function to get join table of BWP, BP, & products for a particular warehouse
      #
      def get_wbp_joined_with_filter_table(warehouse)
        return WBP_MODEL.includes(warehouse_pricings: :warehouse_stocks).includes(
          :warehouse).includes(brand_pack: :product).where(warehouse: warehouse)
      end

      #
      # Function to fetch WBPS based on filters & pagination params
      #
      def get_filtered_warehouse_brand_pack_by_warehouse_id(args)
        sub_categories = CATEGORY_UTIL.get_sub_categories({
          department_id: args[:department_id],
          category_id: args[:category_id],
          sub_category_id: args[:sub_category_id]
        })
        warehouse = get_warehouse_by_id(args[:id])
        if sub_categories.present?
          wbps = get_wbp_by_sub_categories(sub_categories, warehouse)
        else
          wbps = warehouse.warehouse_brand_packs
        end
        set_pagination_properties(args)
        page_count = (wbps.count / @per_page).ceil
        warehouse_brand_packs = wbps.limit(@per_page).offset((@page_no - 1) * @per_page)
        return { warehouse_brand_packs: warehouse_brand_packs, page_count: page_count }
      end

      #
      # Fuction to fetch warehouse by it's ID
      #
      def get_warehouse_by_id(warehouse_id)
        warehouse_dao = WAREHOUSE_DAO.new
        warehouse = warehouse_dao.get_warehouse_by_id(warehouse_id)
        return warehouse
      end
    end
  end
end

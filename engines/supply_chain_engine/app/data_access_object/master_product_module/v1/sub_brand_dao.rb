#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Data Access Object to interact with brandModel
    #
    class SubBrandDao < SupplyChainBaseModule::V1::BaseDao
      SUB_BRAND_MODEL = MasterProductModule::V1::SubBrand
      def initialize
        super
      end

      #
      # Create the brand
      #
      # @param args [name] sub_brand name
      #
      # @return [sub_brandModel]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        new_sub_brand = SUB_BRAND_MODEL.new(args)

        begin
          new_sub_brand.save!
          return new_sub_brand
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create Sub Brand, ' + e.message)
        end
      end

      #
      # Update the given sub_brand with name
      #
      # @param name [String] new sub_brand name
      # @param sub_brand [Model] sub_brandModel
      #
      # @return [sub_brandModel] updated sub_brand
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, sub_brand)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
          CONTENT::SUB_BRAND_MISSING) if sub_brand.nil? || args.nil?
        begin
          sub_brand.update_attributes!(args)
          return sub_brand
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update Sub Brand, ' + e.message)
        end
      end

      #
      # Find the sub_brand by name
      #
      # @param name [String] sub_brand name
      #
      # @return [sub_brandModel] return sub_brand model else nil
      #
      def get_sub_brand_by_id(sub_brand_id)
        SUB_BRAND_MODEL.find_by(id: sub_brand_id)
      end

      #
      # Returns all the sub_brands
      #
      # @return [Array] array of sub_brand model
      #
      def get_all(pagination_params = {})
        get_all_element(SUB_BRAND_MODEL, pagination_params)
      end

      #
      # validate if sub_brand exists or not if exists then call dao to change the state of sub_brand
      # else return error
      #
      # @param sub_brand_id [String] sub_brand id of which status is to be changed
      # @param event [String] event to trigger to change the status 
      #
      # @error [InvalidArgumentsError] if sub_brand with give sub_brand_id doesn't exists or if it fails to update state
      #
      def change_sub_brand_state(sub_brand_id, event)
        sub_brand = SUB_BRAND_MODEL.find_by(id: sub_brand_id)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::SUB_BRAND_NOT_FOUND) if sub_brand.nil?
        change_state(sub_brand, event)
        return sub_brand
      end

      #
      # Return sub_brand by sub_brand name and brand_id
      # 
      # @return [SubBrandModel] single sub_brand assocaited with the brand_id
      # 
      def get_sub_brand_by_name_and_brand(name, brand_id)
        SUB_BRAND_MODEL.find_by(name: name, brand_id: brand_id)
      end

      #
      # delete all the records
      #
      def delete_all
        SUB_BRAND_MODEL.delete_all
      end

      #
      # Return sub_brand by given sub_brands
      # 
      # @return [SubBrandModel] sub_brands assocaited with the brands
      # 
      def get_sub_brands_by_brands(brands)
        SUB_BRAND_MODEL.where(brand_id: brands)
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Data Access Object to interact with BrandPack Model
    #
    class BrandPackDao < SupplyChainBaseModule::V1::BaseDao
      BRAND_PACK_MODEL = MasterProductModule::V1::BrandPack
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper
      CATEGORY_UTIL = CategorizationModule::V1::CategoryUtil
      SUB_BRAND_UTIL = MasterProductModule::V1::SubBrandUtil
      BRAND_PACK_UTIL = MasterProductModule::V1::BrandPackUtil

      def initialize
        super
      end

      #
      # Create the brand_pack
      #
      # @param args [name] brand_pack name
      #
      # @return [brand_packModel]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        if args[:images].present?
          image_id_array = IMAGE_SERVICE_HELPER.get_sorted_image_id_array_by_priority(args[:images])
          validate_images(image_id_array)
          args[:images] = image_id_array
        end
        params = model_params(args, BRAND_PACK_MODEL)
        params = params.merge(images: args[:images]) if args[:images].present?
        new_brand_pack = BRAND_PACK_MODEL.new(params)

        begin
          new_brand_pack.save!
          bind_image_with_resource(new_brand_pack.images, BRAND_PACK_MODEL.name) if args[:images].present?
          return new_brand_pack
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create Brand Pack, ' + e.message)
        end
      end

      #
      # Update the given brand_pack with name
      #
      # @param name [String] new brand_pack name
      # @param brand_pack [Model] brand_packModel
      #
      # @return [brand_packModel] updated brand_pack
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, brand_pack)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
          CONTENT::BRAND_PACK_MISSING) if brand_pack.blank? || args.blank?

        if args[:images].present?
          image_id_array = IMAGE_SERVICE_HELPER.get_sorted_image_id_array_by_priority(args[:images])
          validate_images(image_id_array)
          args[:images] = image_id_array
        end
        params = model_params(args, BRAND_PACK_MODEL)
        params = params.merge(images: args[:images]) if args[:images].present?
        begin
          brand_pack.update_attributes!(params)
          bind_image_with_resource(brand_pack.images, BRAND_PACK_MODEL.name) if args[:images].present?
          return brand_pack
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update Brand Pack, ' + e.message)
        end
      end

      def get_brand_pack_by_id(brand_pack_id)
        BRAND_PACK_MODEL.find_by(id: brand_pack_id)
      end

      def get_by_id(id)
        BRAND_PACK_MODEL.find_by(id: id)
      end

      #
      # Returns all the brand packs
      #
      # @return [Array] array of brand_packs model
      #
      def get_all(pagination_params = {})
        get_all_element(BRAND_PACK_MODEL, pagination_params)
      end

      #
      # Fetch elements after applying filters and with pagination
      # Currently filters are:
      #   - department_id, category_id, sub_category_id, company_id, brand_id, sub_brand_id
      # Finally,
      # -sub_categories are fetched and brandPacks are filtered over it
      # -sub_brands are fetched and corresponding brandPacks are filtered over it
      # -if both of above are present, the filtered brandPacks from sub_categories filteration
      #  are used to filter again using the sub_brands filteration and the same are returned
      #
      # @param filter_params [JSON] [Hash of filter params]
      # 
      # @return [JSON] [Hash of paginated brand_packs {:elements, :page_count}]
      #
      def get_paginated_filtered_brand_packs(filter_params)
        sub_categories = CATEGORY_UTIL.get_sub_categories({
          department_id: filter_params[:department_id],
          category_id: filter_params[:category_id],
          sub_category_id: filter_params[:sub_category_id]
          })
        
        sub_brands = SUB_BRAND_UTIL.get_sub_brands({
          company_id: filter_params[:company_id],
          brand_id: filter_params[:brand_id],
          sub_brand_id: filter_params[:sub_brand_id]
        })
        
        if sub_categories.present? || sub_brands.present?
          if sub_categories.present?
            filtered_brand_packs = get_brand_pack_by_sub_categories(sub_categories)
          end

          if sub_brands.present?
            filtered_brand_packs = BRAND_PACK_UTIL.get_brand_packs({
              brand_packs: filtered_brand_packs,
              sub_brands: sub_brands
            })
          end
          # Call BaseDao function to apply pagination over filtered brand_packs
          
          brand_packs = get_paginated_element(BRAND_PACK_MODEL, filtered_brand_packs, filter_params)
        else
          brand_packs = get_all(filter_params)
        end

        return brand_packs
      end

      #
      # Get brand packs by department and category
      #
      # @param args [JSON] [Hash of departments, category and sub-category]
      #
      # @return [Array] [Array of BrandPack object]
      #
      def get_all_but_filtered_brand_packs(filter_params)
        sub_categories = CATEGORY_UTIL.get_sub_categories({
          department_id: filter_params[:department_id],
          category_id: filter_params[:category_id],
          sub_category_id: filter_params[:sub_category_id]
          })
        if sub_categories.present?
          brand_packs = get_brand_pack_by_sub_categories(sub_categories)
        else
          filter_params = filter_params.merge({per_page: PAGINATION_UTIL::INFINITE_PER_PAGE})
          brand_packs = get_all(filter_params)
          brand_packs = brand_packs[:elements]
        end
        return brand_packs
      end

      #
      # validate if brand_pack exists or not if exists then call dao to change the state of brand_pack
      # else return error
      #
      # @param brand_pack_id [String] brand_pack id of which status is to be changed
      # @param event [String] event to trigger to change the status
      #
      # @error [InvalidArgumentsError] if brand_pack with give brand_pack_id doesn't exists or if it fails to update state
      #
      def change_brand_pack_state(brand_pack_id, event)
        brand_pack = BRAND_PACK_MODEL.find_by(id: brand_pack_id)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_NOT_FOUND) if brand_pack.nil?
        change_state(brand_pack, event)
        return brand_pack
      end

      #
      # delete all the records
      #
      def delete_all
        BRAND_PACK_MODEL.delete_all
      end

      # validate if all the image_id present in image_id_array is present or not
      #
      # @param image_id_array [Array] array containing all the image_ids
      #
      # @error [InvalidArgumentsError] if image_id not present in image table
      #
      def validate_images(image_id_array)
        if IMAGE_SERVICE_HELPER.validate_images(image_id_array) == false
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::IMAGE_LINKING_FAILED)
        end
      end

      #
      # This function return all the brand pack in the given sub_categories
      #
      def get_brand_pack_by_sub_categories(sub_categories)
        BRAND_PACK_MODEL.where(sub_category: sub_categories)
      end

      #
      # This function return all the brand packs in the given brand_packs by given products
      # or if brand_packs aren't passed, it filters BrandPack model by given products
      #
      def filter_brand_packs_by_products(args)
        if args[:brand_packs].nil? 
          BRAND_PACK_MODEL.where(product: args[:products])
        else
          args[:brand_packs].where(product: args[:products])
        end
      end

      # 
      # Bind images with resource
      #
      # @param image_ids [Array] Array of image ids
      # @param resource_name [String] Mode name
      #
      def bind_image_with_resource(image_ids, resource_name)
        IMAGE_SERVICE_HELPER.bind_image_with_resource(image_ids, resource_name)
      end

      #
      # Function to find brand_pack by it's code
      #
      def get_brand_pack_by_code(brand_pack_code)
        brand_pack = BRAND_PACK_MODEL.where(brand_pack_code: brand_pack_code).first
        if brand_pack.blank?
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT::BRAND_PACK_CODE_NOT_PRESENT%{code: brand_pack_code})
        end
        return brand_pack
      end

      # 
      # This function join all tables required to return all information
      # corresponding to brand_pack
      #
      # @param brand_packs [ActiveRecordRelationArray] brand packs
      # 
      # @return [ActiveRecordRelationArray] Joined object containing 
      #
      def self.get_master_products_data(brand_packs)
        return [] if brand_packs.blank?
        brand_packs.eager_load(product: [sub_brand: [ brand: :company] ] ).includes(sub_category: [parent_category: :department])
      end
    end
  end
end


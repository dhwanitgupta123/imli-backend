#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Data Access Object to interact with brandModel
    #
    class BrandDao < SupplyChainBaseModule::V1::BaseDao
      BRAND_MODEL = MasterProductModule::V1::Brand
      def initialize
        super
      end

      #
      # Create the brand
      #
      # @param args [name] brand name
      #
      # @return [brandModel]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        new_brand = BRAND_MODEL.new(args)
        begin
          new_brand.save!
          return new_brand
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create Brand, ' + e.message)
        end
      end

      #
      # Update the given brand with name
      #
      # @param name [String] new brand name
      # @param brand [Model] brandModel
      #
      # @return [brandModel] updated brand
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, brand)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_MISSING) if brand.nil? || args.nil?
        begin
          brand.update_attributes!(args)
          return brand
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update Brand, ' + e.message)
        end
      end

      #
      # Find the brand by name
      #
      # @param name [String] brand name
      #
      # @return [brandModel] return brand model else nil
      #
      def get_brand_by_id(brand_id)
        BRAND_MODEL.find_by(id: brand_id)
      end

      #
      # Returns all the brands
      #
      # @return [Array] array of brand model
      #
     def get_all(pagination_params = {})
        get_all_element(BRAND_MODEL, pagination_params)
      end

      #
      # validate if brand exists or not if exists then call dao to change the state of brand
      # else return error
      #
      # @param brand_id [String] brand id of which status is to be changed
      # @param event [String] event to trigger to change the status 
      #
      # @error [InvalidArgumentsError] if brand with give brand_id doesn't exists or if it fails to update state
      #
      def change_brand_state(brand_id, event)
        brand = BRAND_MODEL.find_by(id: brand_id)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_NOT_FOUND) if brand.nil?
        change_state(brand, event)
        return brand
      end

      #
      # Return brand by brand name and company_id
      # 
      # @return [BrandModel] single brand assocaited with the company_id
      # 
      def get_brand_by_name_and_company(name, company_id)
        BRAND_MODEL.find_by(name: name, company_id: company_id)
      end

      #
      # Return brands by company_id
      # 
      # @return [BrandModel] brands assocaited with the company_id
      # 
      def get_brands_by_company(company_id)
        BRAND_MODEL.where(company_id: company_id)
      end

      #
      # delete all the records
      #
      def delete_all
        BRAND_MODEL.delete_all
      end
    end
  end
end

#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    #
    # GetSellerBrandPacks api, it validates the request, call the
    # seller brand pack service and return the JsonResponse
    #
    class GetSellerBrandPacksApi < SupplyChainBaseModule::V1::BaseApi
      SELLER_BRAND_PACK_SERVICE = SellerProductModule::V1::SellerBrandPackService
      SELLER_BRAND_PACK_MAPPER = SellerProductModule::V1::SellerBrandPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the seller brand packs
      #
      # @return [JsonResponse]
      #
      def enact(request)
        seller_brand_pack_service_class = SELLER_BRAND_PACK_SERVICE.new

        begin
          seller_brand_packs = seller_brand_pack_service_class.get_all_seller_brand_packs(request)
          seller_brand_pack_array = SELLER_BRAND_PACK_MAPPER.map_seller_brand_packs_to_array(seller_brand_packs)
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_seller_brand_pack_response(seller_brand_pack_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end
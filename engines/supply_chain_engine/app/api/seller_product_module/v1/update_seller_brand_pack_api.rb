#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    #
    # UpdateSellerBrandPack api, it validates the request, call the
    # seller brand pack service and return the JsonResponse
    #
    class UpdateSellerBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      SELLER_BRAND_PACK_SERVICE = SellerProductModule::V1::SellerBrandPackService
      SELLER_BRAND_PACK_MAPPER = SellerProductModule::V1::SellerBrandPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to update seller brand pack and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have seller brand pack arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request)
        seller_brand_pack_service_class = SELLER_BRAND_PACK_SERVICE.new
        begin
          validate_request(request)
          seller_brand_pack = seller_brand_pack_service_class.transactional_update_seller_brand_pack(request)
          seller_brand_pack_array = SELLER_BRAND_PACK_MAPPER.map_seller_brand_pack_to_hash(seller_brand_pack)
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_single_seller_brand_pack_response(seller_brand_pack_array)
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::SELLER_BRAND_PACK_MISSING)
        end
        if request[:brand_pack_id].present?
          if request[:warehouse_brand_pack_id].blank?
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_BRAND_PACK_MISSING)
          end
        end
        if request[:warehouse_brand_pack_id].present?
          if request[:brand_pack_id].blank?
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_MISSING)
          end
        end
        if request[:pricing].present?
          if request[:pricing][:vat].present? && request[:pricing][:vat] < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_NON_NEGATIVE%{ field: 'VAT' })
          end
          if request[:pricing][:spat].present? && request[:pricing][:spat] <= 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_ONLY_POSITIVE%{ field: 'SPAT' })
          end
          if request[:pricing][:margin].present? && request[:pricing][:margin] < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_NON_NEGATIVE%{ field: 'Margin' })
          end
          if request[:pricing][:service_tax].present? && request[:pricing][:service_tax] < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_ONLY_POSITIVE%{ field: 'Service Tax' })
          end
          if request[:pricing][:spat_per_unit].present? && request[:pricing][:spat_per_unit] <= 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_NON_NEGATIVE%{ field: 'SPAT/unit' })
          end
        end
      end
    end
  end
end
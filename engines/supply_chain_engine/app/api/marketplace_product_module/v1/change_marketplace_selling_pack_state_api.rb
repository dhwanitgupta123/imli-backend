#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # ChangeMarketplaceSellingPackStateApi api, it validates the request, call the
    # marketplace_selling_pack service to change the state of given marketplace_selling_pack id
    #
    class ChangeMarketplaceSellingPackStateApi < SupplyChainBaseModule::V1::BaseApi
      MARKETPLACE_SELLING_PACK_SERVICE = MarketplaceProductModule::V1::MarketplaceSellingPackService
      MARKETPLACE_SELLING_PACK_MAPPER = MarketplaceProductModule::V1::MarketplaceSellingPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to change marketplace_selling_pack state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, change state params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        marketplace_selling_pack_service_class = MARKETPLACE_SELLING_PACK_SERVICE.new
        begin
          validate_request(request)
          marketplace_selling_pack = marketplace_selling_pack_service_class.change_marketplace_selling_pack_state({ id: request[:id], event: request[:event].to_i })
          marketplace_selling_pack = MARKETPLACE_SELLING_PACK_MAPPER.map_marketplace_selling_pack_to_hash(marketplace_selling_pack)
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_single_marketplace_selling_pack_response(marketplace_selling_pack)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERROR_UTIL::RunTimeError => e
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        end
      end

      private

      #
      # Validates the required params of change state
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::MARKETPLACE_SELLING_PACK_MISSING)
        end
        if request[:event].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end
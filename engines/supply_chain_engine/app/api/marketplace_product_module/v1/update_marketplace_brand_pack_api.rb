#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # UpdateMarketplaceBrandPack api, it validates the request, call the
    # marketplace_brand_pack service and return the JsonResponse
    #
    class UpdateMarketplaceBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      MARKETPLACE_BRAND_PACK_SERVICE = MarketplaceProductModule::V1::MarketplaceBrandPackService
      MARKETPLACE_BRAND_PACK_MAPPER = MarketplaceProductModule::V1::MarketplaceBrandPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to update marketplace_brand_pack and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have marketplace_brand_pack arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request)
        marketplace_brand_pack_service_class = MARKETPLACE_BRAND_PACK_SERVICE.new

        begin
          validate_request(request)
          marketplace_brand_pack = marketplace_brand_pack_service_class.transactional_update_marketplace_brand_pack(request)
          marketplace_brand_pack = MARKETPLACE_BRAND_PACK_MAPPER.map_marketplace_brand_pack_to_hash(marketplace_brand_pack)
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_single_marketplace_brand_pack_response(marketplace_brand_pack)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::MARKETPLACE_BRAND_PACK_MISSING)
        end
        if request[:brand_pack_id].present?
          if request[:seller_brand_pack_id].blank?
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::SELLER_BRAND_PACK_MISSING)
          end
        end
        if request[:seller_brand_pack_id].present?
          if request[:brand_pack_id].blank?
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_MISSING)
          end
        end

        if request[:pricing].present?
          if request[:pricing][:selling_price].present? && request[:pricing][:selling_price].to_f <= 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_ONLY_POSITIVE%{ field: 'Selling price' })
          end
          if request[:pricing][:buying_price].present? && request[:pricing][:buying_price].to_f <= 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_ONLY_POSITIVE%{ field: 'Buying price' })
          end
          if request[:pricing][:savings].present? && request[:pricing][:savings].to_f < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_NON_NEGATIVE%{ field: 'Savings' })
          end
          if request[:pricing][:mrp].present? && request[:pricing][:mrp].to_f <= 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_ONLY_POSITIVE%{ field: 'MRP' })
          end
          if request[:pricing][:vat].present? && request[:pricing][:vat].to_f < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_NON_NEGATIVE%{ field: 'VAT' })
          end
          if request[:pricing][:spat].present? && request[:pricing][:spat].to_f <= 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_ONLY_POSITIVE%{ field: 'SPAT' })
          end
          if request[:pricing][:margin].present? && request[:pricing][:margin].to_f < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_NON_NEGATIVE%{ field: 'Margin' })
          end
        end
      end
    end
  end
end
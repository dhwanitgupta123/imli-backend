#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # UpdateMarketplaceSellingPack api, it validates the request, call the
    # marketplace_selling_pack service and return the JsonResponse
    #
    class UpdateMarketplaceSellingPackApi < SupplyChainBaseModule::V1::BaseApi
      MARKETPLACE_SELLING_PACK_SERVICE = MarketplaceProductModule::V1::MarketplaceSellingPackService
      MARKETPLACE_SELLING_PACK_MAPPER = MarketplaceProductModule::V1::MarketplaceSellingPackMapper
      GENERAL_HELPER = CommonModule::V1::GeneralHelper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to update marketplace_selling_pack and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have marketplace_selling_pack arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request)
        marketplace_selling_pack_service_class = MARKETPLACE_SELLING_PACK_SERVICE.new

        begin
          request[:is_ladder_pricing_active] = GENERAL_HELPER.string_to_boolean(request[:is_ladder_pricing_active]) if request[:is_ladder_pricing_active].present?
          request[:dirty_bit] = GENERAL_HELPER.string_to_boolean(request[:dirty_bit]) if request[:dirty_bit].present?
          validate_request(request)
          marketplace_selling_pack = marketplace_selling_pack_service_class.transactional_update_marketplace_selling_pack(request)
          marketplace_selling_pack = MARKETPLACE_SELLING_PACK_MAPPER.map_marketplace_selling_pack_to_hash(marketplace_selling_pack)
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_single_marketplace_selling_pack_response(marketplace_selling_pack)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::RunTimeError => e
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::MARKETPLACE_SELLING_PACK_MISSING)
        end
        if request[:max_quantity].present? && request[:max_quantity].to_f <= 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_ONLY_POSITIVE%{ field: 'Max quantity' })
        end
        if request[:pricing].present?
          if request[:pricing][:base_selling_price].present? && request[:pricing][:base_selling_price].to_f <= 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_ONLY_POSITIVE%{ field: 'Base Selling price' })
          end
          if request[:pricing][:vat].present? && request[:pricing][:vat].to_f < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_NON_NEGATIVE%{ field: 'VAT' })
          end
          if request[:pricing][:mrp].present? && request[:pricing][:mrp].to_f <= 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_ONLY_POSITIVE%{ field: 'MRP' })
          end
          if request[:is_ladder_pricing_active].present? && request[:is_ladder_pricing_active]
            if request[:pricing][:ladder].present?
              request[:pricing][:ladder].each do |ladder|
                if ladder[:quantity].blank? || ladder[:quantity].to_f <= 0
                  raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
                    CONTENT::FIELD_ONLY_POSITIVE%{ field: 'Quantity' })
                end
                if ladder[:selling_price].blank? || ladder[:selling_price].to_f <= 0
                  raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
                    CONTENT::FIELD_ONLY_POSITIVE%{ field: 'Selling price' })
                end
              end
              if request[:pricing][:base_selling_price] != request[:pricing][:ladder].first[:selling_price] && request[:pricing][:ladder].first[:quantity].to_i == 1
                raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
                  CONTENT::SHOULD_BE_EQUAL%{ field: 'Base Selling Price and Ladder Selling Price for quantity = 1' })
              end
            else
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
                CONTENT::FIELD_MUST_PRESENT%{ field: 'Ladder Pricing details' })
            end
          end
        end
      end
    end
  end
end

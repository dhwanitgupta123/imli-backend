#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetInventoryBrandPacks api, it validates the request, call the
    # inventory_brand_pack service and return the JsonResponse
    #
    class GetInventoryBrandPacksApi < SupplyChainBaseModule::V1::BaseApi
      INVENTORY_BRAND_PACK_SERVICE = InventoryProductModule::V1::InventoryBrandPackService
      INVENTORY_BRAND_PACK_MAPPER = InventoryProductModule::V1::InventoryBrandPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the inventory_brand_packs
      #
      # @return [JsonResponse]
      #
      def enact(request)
        inventory_brand_pack_service_class = INVENTORY_BRAND_PACK_SERVICE.new

        begin
          response = inventory_brand_pack_service_class.get_all_inventory_brand_packs(request)
          inventory_brand_packs = response[:elements]
          page_count = response[:page_count]
          inventory_brand_pack_array = INVENTORY_BRAND_PACK_MAPPER.map_inventory_brand_packs_to_array(inventory_brand_packs)
          return INVENTORY_PRODUCT_RESPONSE_DECORATOR.create_inventory_brand_pack_response(inventory_brand_pack_array, page_count)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return INVENTORY_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end

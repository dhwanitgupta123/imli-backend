#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # UpdateInventoryBrandPack api, it validates the request, call the
    # inventory_brand_pack service and return the JsonResponse
    #
    class UpdateInventoryBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      INVENTORY_BRAND_PACK_SERVICE = InventoryProductModule::V1::InventoryBrandPackService
      INVENTORY_BRAND_PACK_MAPPER = InventoryProductModule::V1::InventoryBrandPackMapper
      IBP_UTIL = InventoryProductModule::V1::InventoryBrandPackUtil
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to update inventory_brand_pack and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have inventory_brand_pack arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request)
        inventory_brand_pack_service_class = INVENTORY_BRAND_PACK_SERVICE.new

        begin
          validate_request(request)
          verified = true
          inventory_brand_pack = inventory_brand_pack_service_class.transactional_update_inventory_brand_pack(request.merge(verified: verified))
          inventory_brand_pack = INVENTORY_BRAND_PACK_MAPPER.map_inventory_brand_pack_to_hash(inventory_brand_pack)
          return INVENTORY_PRODUCT_RESPONSE_DECORATOR.create_single_inventory_brand_pack_response(inventory_brand_pack)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return INVENTORY_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        IBP_UTIL.validate_update_ibp_request(request)
      end
    end
  end
end

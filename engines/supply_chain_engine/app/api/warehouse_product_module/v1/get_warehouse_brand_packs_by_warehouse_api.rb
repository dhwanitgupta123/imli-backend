#
# Module to handle all the functionalities related to warehouse brand pack
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # GetWarehouseBrandPacksByWarehouseApi api, it validates the request, call the
    # warehouse_brand_pack service and return the JsonResponse
    #
    class GetWarehouseBrandPacksByWarehouseApi < SupplyChainBaseModule::V1::BaseApi
      WBP_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      WBP_MAPPER = WarehouseProductModule::V1::WarehouseBrandPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return warehouse_brand_packs corresponding to a particuar warehouse
      #
      # @return [JsonResponse]
      #
      def enact(request)
        warehouse_brand_pack_service_class = WBP_SERVICE.new

        if request.blank? || request[:id].blank?
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(
            CONTENT::WAREHOUSE_MISSING)
        end

        begin
          wbp_hash = warehouse_brand_pack_service_class.get_warehouse_brand_pack_by_warehouse_id(request)
          wbp_array = WBP_MAPPER.map_wbps_to_array(wbp_hash)
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_wbp_response(wbp_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end

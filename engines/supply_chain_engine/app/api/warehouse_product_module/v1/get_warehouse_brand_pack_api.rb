#
# Module to handle all the functionalities related to warehouse brand pack
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Get warehouse Brand Pack api, it validates the request, call the
    # warehouse service and return the JsonResponse
    #
    class GetWarehouseBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      WBP_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      WBP_MAPPER = WarehouseProductModule::V1::WarehouseBrandPackMapper
      CONTENT = CommonModule::V1::Content
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to get WBP and returns the
      # success or error response
      #
      # @param request [integer] Request WBP id
      #
      # @return [JsonResponse]
      #
      def enact(wbp_id)
        wbp_service = WBP_SERVICE.new
        begin
          validate_request(wbp_id)
          wbp = wbp_service.get_wbp(wbp_id)
          wbp_array = WBP_MAPPER.map_wbp_to_hash(wbp)
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_single_wbp_response(wbp_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params WBP id
      #
      # @param request [integer WBP id]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(wbp_id)
        if wbp_id.blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_BRAND_PACK_MISSING)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to warehouse brand pack
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Add warehouse Brand Pack api, it validates the request, call the
    # warehouse service and return the JsonResponse
    #
    class AddWarehouseBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      WBP_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      WBP_MAPPER = WarehouseProductModule::V1::WarehouseBrandPackMapper
      CONTENT = CommonModule::V1::Content
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to create WBP and returns the
      # success or error response
      #
      # @param request [Hash] Request object, WBP params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        wbp_service = WBP_SERVICE.new
        begin
          validate_request(request)
          wbp = wbp_service.transactional_create_warehouse_brand_pack(request)
          wbp_array = WBP_MAPPER.map_wbp_to_hash(wbp)
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_single_wbp_response(wbp_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params warehouse id & IBP id
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:warehouse_id].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_MISSING)
        end
        if request[:inventory_brand_pack_id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVENTORY_BRAND_PACK_MISSING)
        end
        if request[:brand_pack_id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_MISSING)
        end
        if request[:threshold_stock_value].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_MUST_PRESENT%{ field: 'Threshold stock value'})
        end
        if request[:outbound_frequency_period_in_days].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_MUST_PRESENT%{ field: 'Outbound Frequency period'})
        end
        unless request[:threshold_stock_value].numeric?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::ONLY_NUMBERS_ALLOWED%{ field: 'Threshold stock value'})
        end
        unless request[:outbound_frequency_period_in_days].numeric?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::ONLY_NUMBERS_ALLOWED%{ field: 'Outbound Frequency period'})
        end
        if request[:threshold_stock_value].to_i < 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_NON_NEGATIVE%{ field: 'Threshold stock value'})
        end
        if request[:outbound_frequency_period_in_days].to_i < 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_NON_NEGATIVE%{ field: 'Outbound Frequency period'})
        end
      end
    end
  end
end

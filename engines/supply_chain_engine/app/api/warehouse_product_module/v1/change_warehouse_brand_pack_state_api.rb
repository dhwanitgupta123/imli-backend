#
# Module to handle all the functionalities related to warehouse brand pack
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Change warehouse Brand Pack state api, it validates the request, call the
    # warehouse service and return the JsonResponse
    #
    class ChangeWarehouseBrandPackStateApi < SupplyChainBaseModule::V1::BaseApi
      WBP_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      WBP_MAPPER = WarehouseProductModule::V1::WarehouseBrandPackMapper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      CONTENT = CommonModule::V1::Content
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to change WBP state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, WBP change state params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        wbp_service = WBP_SERVICE.new
        begin
          validate_request(request)
          wbp = wbp_service.change_state(request)
          wbp_array = WBP_MAPPER.map_wbp_to_hash(wbp)
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_single_wbp_response(wbp_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params state change event
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:event].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end

        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end

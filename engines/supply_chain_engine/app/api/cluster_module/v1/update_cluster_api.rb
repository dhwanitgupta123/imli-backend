#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # UpdateClusterApi validates the request, call the
    # cluster service and return the JsonResponse
    #
    class UpdateClusterApi < SupplyChainBaseModule::V1::BaseApi

      CLUSTER_SERVICE = ClusterModule::V1::ClusterService
      CLUSTER_RESPONSE_DECORATOR = ClusterModule::V1::ClusterResponseDecorator
      CLUSTER_MAPPER = ClusterModule::V1::ClusterMapper

      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to update cluster and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have cluster arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request)
        cluster_service = CLUSTER_SERVICE.new(@params)
        begin
          validate_request(request)
          cluster = cluster_service.update_cluster(request)
          cluster = CLUSTER_MAPPER.map_cluster_to_hash(cluster)
          return CLUSTER_RESPONSE_DECORATOR.create_single_cluster_response(cluster)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return CLUSTER_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::CLUSTER_MISSING)
        end
      end
    end
  end
end

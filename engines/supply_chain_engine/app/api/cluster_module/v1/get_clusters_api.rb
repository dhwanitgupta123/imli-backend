#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # GetClustersApi validates the request, call the
    # cluster service and return the JsonResponse
    #
    class GetClustersApi < SupplyChainBaseModule::V1::BaseApi

      CLUSTER_SERVICE = ClusterModule::V1::ClusterService
      CLUSTER_RESPONSE_DECORATOR = ClusterModule::V1::ClusterResponseDecorator
      CLUSTER_MAPPER = ClusterModule::V1::ClusterMapper

      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the clusters
      #
      # @return [JsonResponse]
      #
      def enact(request)

        cluster_service = CLUSTER_SERVICE.new(@params)
        begin
          clusters_hash = cluster_service.get_filtered_clusters(request)
          clusters = CLUSTER_MAPPER.map_cluster_array_to_array_of_hash(clusters_hash[:elements])
          return CLUSTER_RESPONSE_DECORATOR.create_clusters_response(clusters, clusters_hash[:page_count])
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return CLUSTER_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # ChangeClusterStateApi validates the request, call the
    # cluster service to change the state of given cluster id
    #
    class ChangeClusterStateApi < SupplyChainBaseModule::V1::BaseApi

      CLUSTER_SERVICE = ClusterModule::V1::ClusterService
      CLUSTER_RESPONSE_DECORATOR = ClusterModule::V1::ClusterResponseDecorator
      CLUSTER_MAPPER = ClusterModule::V1::ClusterMapper

      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to change cluster state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, change state params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        cluster_service = CLUSTER_SERVICE.new(@params)
        begin
          validate_request(request)
          cluster = cluster_service.change_cluster_state(request)
          cluster = CLUSTER_MAPPER.map_cluster_to_hash(cluster)
          return CLUSTER_RESPONSE_DECORATOR.create_single_cluster_response(cluster)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return CLUSTER_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return CLUSTER_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        end
      end

      private

      #
      # Validates the required params of change state
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::CLUSTER_MISSING)
        end
        if request[:event].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end

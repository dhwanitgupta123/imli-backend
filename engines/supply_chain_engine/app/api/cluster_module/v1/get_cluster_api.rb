#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # GetClusterApi validates the request, call the
    # cluster service and return the JsonResponse
    #
    class GetClusterApi < SupplyChainBaseModule::V1::BaseApi

      CLUSTER_SERVICE = ClusterModule::V1::ClusterService
      CLUSTER_RESPONSE_DECORATOR = ClusterModule::V1::ClusterResponseDecorator
      CLUSTER_MAPPER = ClusterModule::V1::ClusterMapper

      def initialize(params)
        @params = params
        super
      end

      #
      # Return cluster corresponding to id
      #
      # @return [JsonResponse]
      #
      def enact(request)

        cluster_service = CLUSTER_SERVICE.new(@params)
        begin
          validate_request(request)
          cluster = cluster_service.get_cluster_by_id(request[:id])
          cluster = CLUSTER_MAPPER.map_cluster_to_hash(cluster)
          return CLUSTER_RESPONSE_DECORATOR.create_single_cluster_response(cluster)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return CLUSTER_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      #
      # Validates the required params cluster
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::CLUSTER_MISSING)
        end
      end
    end
  end
end

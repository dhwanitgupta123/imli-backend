#
# Module to handle all the functionalities related to Packaging
#
module PackagingModule
  #
  # Version1 for Packaging module
  #
  module V1
    #
    # GetPackagingBox api, it validates the request, call the
    # packaging_box service and return the JsonResponse
    #
    class GetPackagingBoxApi < SupplyChainBaseModule::V1::BaseApi
      PACKAGING_BOX_SERVICE = PackagingModule::V1::PackagingBoxService
      PACKAGING_BOX_MAPPER = PackagingModule::V1::PackagingBoxMapper
      PACKAGING_BOX_RESPONSE_DECORATOR = PackagingModule::V1::PackagingBoxResponseDecorator

      def initialize(params='')
        @params = params
        super
      end

      #
      # Return packaging_box corresponding to id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        packaging_box_service_class = PACKAGING_BOX_SERVICE.new(@params)

        begin
          validate_request(request)
          packaging_box = packaging_box_service_class.get_packaging_box_by_id(request[:id])
          get_packaging_box_api_response = PACKAGING_BOX_MAPPER.get_packaging_box_api_response_hash(packaging_box)
          return PACKAGING_BOX_RESPONSE_DECORATOR.create_get_packaging_box_response(get_packaging_box_api_response)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return PACKAGING_BOX_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params packaging_box name
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          return PACKAGING_RESPONSE_DECORATOR.create_response_invalid_arguments_error("Id is required parameter")
        end
      end

    end
  end
end
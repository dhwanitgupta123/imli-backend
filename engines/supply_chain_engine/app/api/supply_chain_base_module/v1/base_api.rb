#
# Module to handle all the functionalities related to API
#
module SupplyChainBaseModule
  #
  # Version1 for address module
  #
  module V1
    #
    # BaseApi to inject basic funcionalities
    #
    class BaseApi
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      SUPPLY_CHAIN_RESPONSE_DECORATOR = SupplyChainModule::V1::SupplyChainResponseDecorator
      WAREHOUSE_PRODUCT_RESPONSE_DECORATOR = WarehouseProductModule::V1::WarehouseProductResponseDecorator
      SELLER_PRODUCT_RESPONSE_DECORATOR = SellerProductModule::V1::SellerProductResponseDecorator
      MASTER_PRODUCT_RESPONSE_DECORATOR = MasterProductModule::V1::MasterProductResponseDecorator
      INVENTORY_PRODUCT_RESPONSE_DECORATOR = InventoryProductModule::V1::InventoryProductDecorator
      MARKETPLACE_PRODUCT_RESPONSE_DECORATOR = MarketplaceProductModule::V1::MarketplaceProductResponseDecorator
      CONTENT = CommonModule::V1::Content
      def initialize(params)
      end
    end
  end
end

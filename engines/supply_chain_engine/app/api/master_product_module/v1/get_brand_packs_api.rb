#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # GetBrandPacks api, it validates the request, call the
    # brand_pack service and return the JsonResponse
    #
    class GetBrandPacksApi < SupplyChainBaseModule::V1::BaseApi
      BRAND_PACK_SERVICE = MasterProductModule::V1::BrandPackService
      BRAND_PACK_MAPPER = MasterProductModule::V1::BrandPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the brand_packs
      #
      # @return [JsonResponse]
      #
      def enact(request)
        brand_pack_service_class = BRAND_PACK_SERVICE.new

        begin
          response = brand_pack_service_class.get_all_brand_packs(request)
          brand_packs = response[:elements]
          page_count = response[:page_count]
          brand_pack_array = BRAND_PACK_MAPPER.map_brand_packs_to_array(brand_packs)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_brand_pack_response(brand_pack_array, page_count)
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # UpdateBrandPack api, it validates the request, call the
    # brand_pack service and return the JsonResponse
    #
    class UpdateBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      BRAND_PACK_SERVICE = MasterProductModule::V1::BrandPackService
      BRAND_PACK_MAPPER = MasterProductModule::V1::BrandPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to update brand_pack and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request, brand_pack_id)
        brand_pack_service_class = BRAND_PACK_SERVICE.new

        begin
          validate_request(request)
          brand_pack = brand_pack_service_class.update_brand_pack(request, brand_pack_id)
          brand_pack = BRAND_PACK_MAPPER.map_brand_pack_to_hash(brand_pack)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_brand_pack_response(brand_pack)
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_MISSING)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # GetBrand api, it validates the request, call the
    # brand service and return the JsonResponse
    #
    class GetBrandApi < SupplyChainBaseModule::V1::BaseApi
      BRAND_SERVICE = MasterProductModule::V1::BrandService
      BRAND_MAPPER = MasterProductModule::V1::BrandMapper
      def initialize(params)
        super
        @params = params
      end

      #
      # Return brand corresponding to given id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        brand_service_class = BRAND_SERVICE.new

        if request.blank? || request[:id].blank?
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(
            CONTENT::BRAND_MISSING)
        end

        begin
          brand = brand_service_class.get_brand_by_id(request[:id])
          brand = BRAND_MAPPER.map_brand_to_hash(brand)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_brand_response(brand)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # AddSubBrand api, it validates the request, call the
    # sub_brand service and return the JsonResponse
    #
    class AddSubBrandApi < SupplyChainBaseModule::V1::BaseApi
      SUB_BRAND_SERVICE = MasterProductModule::V1::SubBrandService
      SUB_BRAND_MAPPER = MasterProductModule::V1::SubBrandMapper

      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to create sub_brand and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have pincode, area,
      #   city, state, country and status (optional)
      #
      # @return [JsonResponse]
      #
      def enact(request)
        sub_brand_service_class = SUB_BRAND_SERVICE.new

        begin
          validate_request(request)
          sub_brand = sub_brand_service_class.create_sub_brand(request)
          sub_brand = SUB_BRAND_MAPPER.map_sub_brand_to_hash(sub_brand)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_sub_brand_response(sub_brand)
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params brand_id
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil? || request[:brand_id].nil? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_MISSING)
        end
      end
    end
  end
end

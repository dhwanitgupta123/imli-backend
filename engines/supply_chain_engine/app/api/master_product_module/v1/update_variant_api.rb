#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # UpdateProduct api, it validates the request, call the
    # product service and return the JsonResponse
    #
    class UpdateVariantApi < SupplyChainBaseModule::V1::BaseApi
      VARIANT_SERVICE = MasterProductModule::V1::VariantService
      VARIANT_MAPPER = MasterProductModule::V1::VariantMapper

      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to update product and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request)
        variant_service_class = VARIANT_SERVICE.new

        begin
          validate_request(request)
          variant = variant_service_class.update_variant(request)
          variant_array = VARIANT_MAPPER.map_variant_to_hash(variant)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_variant_response(variant_array)
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::VARIANT_MISSING)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to master brand_pack
#
module MasterProductModule
  #
  # Version1 for master brand_pack module
  #
  module V1
    #
    # GetBrandPack api, it validates the request, call the
    # brand_pack service and return the JsonResponse
    #
    class GetBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      BRAND_PACK_SERVICE = MasterProductModule::V1::BrandPackService
      BRAND_PACK_MAPPER = MasterProductModule::V1::BrandPackMapper
      def initialize(params)
        super
        @params = params
      end

      #
      # Return brand_pack corresponding to given id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        brand_pack_service_class = BRAND_PACK_SERVICE.new

        if request.blank? || request[:id].blank?
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(
            CONTENT::BRAND_PACK_MISSING)
        end

        begin
          brand_pack = brand_pack_service_class.get_brand_pack_by_id(request[:id])
          # Deprecating this function call as perr current setup of creating IBP together with Brand pack
          # brand_pack = BRAND_PACK_MAPPER.map_brand_pack_to_hash(brand_pack)
          brand_pack = BRAND_PACK_MAPPER.map_brand_pack_and_mpbp_to_hash(brand_pack)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_brand_pack_response(brand_pack)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end

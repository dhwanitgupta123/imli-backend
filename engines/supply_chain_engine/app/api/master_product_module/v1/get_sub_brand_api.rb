#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # GetSubBrand api, it validates the request, call the
    # sub_brand service and return the JsonResponse
    #
    class GetSubBrandApi < SupplyChainBaseModule::V1::BaseApi
      SUB_BRAND_SERVICE = MasterProductModule::V1::SubBrandService
      SUB_BRAND_MAPPER = MasterProductModule::V1::SubBrandMapper
      def initialize(params)
        super
        @params = params
      end

      #
      # Return sub_brand corresponding to given id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        sub_brand_service_class = SUB_BRAND_SERVICE.new

        if request.blank? || request[:id].blank?
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(
            CONTENT::SUB_BRAND_MISSING)
        end

        begin
          sub_brand = sub_brand_service_class.get_sub_brand_by_id(request[:id])
          sub_brand = SUB_BRAND_MAPPER.map_sub_brand_to_hash(sub_brand)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_sub_brand_response(sub_brand)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end

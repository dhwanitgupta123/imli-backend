#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # ChangeBrandStateApi api, it validates the request, call the
    # brand service to change the state of given brand id
    #
    class ChangeBrandStateApi < SupplyChainBaseModule::V1::BaseApi
      BRAND_SERVICE = MasterProductModule::V1::BrandService
      BRAND_MAPPER = MasterProductModule::V1::BrandMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to change brand state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, change state params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        brand_service_class = BRAND_SERVICE.new
        begin
          validate_request(request)
          brand = brand_service_class.change_brand_state(request[:id], request[:event].to_i)
          brand = BRAND_MAPPER.map_brand_to_hash(brand)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_brand_response(brand)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        end
      end

      private

      #
      # Validates the required params of change state
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_MISSING)
        end
        if request[:event].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # AddProduct api, it validates the request, call the
    # product service and return the JsonResponse
    #
    class AddProductApi < SupplyChainBaseModule::V1::BaseApi
      PRODUCT_SERVICE = MasterProductModule::V1::ProductService
      PRODUCT_MAPPER = MasterProductModule::V1::ProductMapper

      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to create product and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have prodcut model params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        product_service_class = PRODUCT_SERVICE.new

        begin
          validate_request(request)
          product = product_service_class.create_product(request)
          product = PRODUCT_MAPPER.map_product_to_hash(product)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_product_response(product)
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params sub_brand_id
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil? || request[:sub_brand_id].nil? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::SUB_BRAND_MISSING)
        end
      end
    end
  end
end

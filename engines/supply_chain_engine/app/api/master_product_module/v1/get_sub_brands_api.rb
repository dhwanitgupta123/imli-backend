#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # GetSubBrands api, it validates the request, call the
    # sub_brand service and return the JsonResponse
    #
    class GetSubBrandsApi < SupplyChainBaseModule::V1::BaseApi
      SUB_BRAND_SERVICE = MasterProductModule::V1::SubBrandService
      SUB_BRAND_MAPPER = MasterProductModule::V1::SubBrandMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the sub_brands
      #
      # @return [JsonResponse]
      #
      def enact(request)
        sub_brand_service_class = SUB_BRAND_SERVICE.new

        begin
          response = sub_brand_service_class.get_all_sub_brands(request)
          sub_brands = response[:elements]
          page_count = response[:page_count]
          sub_brand_array = SUB_BRAND_MAPPER.map_sub_brands_to_array(sub_brands)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_sub_brand_response(sub_brand_array, page_count)
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end

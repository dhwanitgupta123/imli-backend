#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # UpdateProduct api, it validates the request, call the
    # product service and return the JsonResponse
    #
    class UpdateProductApi < SupplyChainBaseModule::V1::BaseApi
      PRODUCT_SERVICE = MasterProductModule::V1::ProductService
      PRODUCT_MAPPER = MasterProductModule::V1::ProductMapper
      def initialize(params)
        super
        @params = params
      end

      #
      # Function takes input corresponding to update product and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request, product_id)
        product_service_class = PRODUCT_SERVICE.new

        begin
          validate_request(request)
          product = product_service_class.update_product(request, product_id)
          product = PRODUCT_MAPPER.map_product_to_hash(product)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_product_response(product) 
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PRODUCT_MISSING)
        end
      end
    end
  end
end

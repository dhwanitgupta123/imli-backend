#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # ChangeSubBrandStateApi api, it validates the request, call the
    # sub_brand service to change the state of given sub_brand id
    #
    class ChangeSubBrandStateApi < SupplyChainBaseModule::V1::BaseApi
      SUB_BRAND_SERVICE = MasterProductModule::V1::SubBrandService
      SUB_BRAND_MAPPER = MasterProductModule::V1::SubBrandMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to change sub_brand state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, change state params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        sub_brand_service_class = SUB_BRAND_SERVICE.new
        begin
          validate_request(request)
          sub_brand = sub_brand_service_class.change_sub_brand_state(request[:id], request[:event].to_i)
          sub_brand = SUB_BRAND_MAPPER.map_sub_brand_to_hash(sub_brand)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_sub_brand_response(sub_brand)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        end
      end

      private

      #
      # Validates the required params of change state
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::SUB_BRAND_MISSING)
        end
        if request[:event].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end

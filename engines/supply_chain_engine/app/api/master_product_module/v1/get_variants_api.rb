#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # GetVariants api, it validates the request, call the
    # variant service and return the JsonResponse
    #
    class GetVariantsApi < SupplyChainBaseModule::V1::BaseApi
      VARIANT_SERVICE = MasterProductModule::V1::VariantService
      VARIANT_MAPPER = MasterProductModule::V1::VariantMapper

      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the variants
      #
      # @return [JsonResponse]
      #
      def enact(request)
        variant_service_class = VARIANT_SERVICE.new

        begin
          variants = variant_service_class.get_all_variants
          variant_array = VARIANT_MAPPER.map_variants_to_array(variants)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_variant_response(variant_array)
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end

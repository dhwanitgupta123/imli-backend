#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # SearchMarketplaceSellingPackByQuery api, it validates the request, call the
    # marketplace_selling_pack_index_service and return the JsonResponse
    #
    # We are using bran-pack-index that is saved in elastic search repository
    # our indexed field contain "display_name context status"
    # this API breaks the query in tokens by whitespace and return the top_n
    # matching results which is specified by user or it take default value from config
    #
    class SearchMarketplaceSellingPackByQueryApi < SupplyChainBaseModule::V1::BaseApi

      MARKETPLACE_SELLING_PACK_INDEX_SERVICE = SearchModule::V1::MarketplaceSellingPackIndexService
      def initialize(params)
        super
        @params = params
      end

      #
      # Function takes input query validate if it's length is greater than 3
      # then call the corresponding service to retrieve the result else
      # throws failure response
      #
      # @param request [Hash] Request object, it should have query
      # and top_n (optional) number of results to return
      #
      # @return [JsonResponse]
      #
      def enact(request)

        begin
          validate_request(request)
          marketplace_selling_pack_index_service = MARKETPLACE_SELLING_PACK_INDEX_SERVICE.new
          top_n = request[:top_n] || SUPPLY_CHAIN_ENGINE_CONFIG['DEFAULT_N_RESULTS']
          marketplace_selling_pack_model_reader = marketplace_selling_pack_index_service.get_results(request[:query], top_n.to_i)
          MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_search_marketplace_selling_pack_response(marketplace_selling_pack_model_reader)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::RunTimeError
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_runtime_error
        end
      end

      def validate_request(request)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Request should contain query') if request.blank? || request[:query].blank?

        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Query length should be greater than 3') if request[:query].length < 3
      end
    end
  end
end

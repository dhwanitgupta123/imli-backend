#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Get C&C api, it validates the request, call the
    # C&C service and return the JsonResponse
    #
    class GetCashAndCarryApi < SupplyChainBaseModule::V1::BaseApi
      CASH_AND_CARRY_SERVICE = SupplyChainModule::V1::CashAndCarryService
      CASH_AND_CARRY_MAPPER = SupplyChainModule::V1::CashAndCarryMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to get C&C and returns the
      # success or error response
      #
      # @param request [integer] Request, C&C id
      #
      # @return [JsonResponse]
      #
      def enact(c_and_c_id)
        cash_and_carry_service = CASH_AND_CARRY_SERVICE.new
        begin
          validate_request(c_and_c_id)
          c_and_c = cash_and_carry_service.get_cash_and_carry(c_and_c_id)
          c_and_c_array = CASH_AND_CARRY_MAPPER.map_cash_and_carry_to_hash(c_and_c)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_cash_and_carry_response(c_and_c_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params C&C id to be fetched
      #
      # @param request [C&C id]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(c_and_c_id)
        if c_and_c_id.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::CASH_AND_CARRY_MISSING)
        end
      end
    end
  end
end

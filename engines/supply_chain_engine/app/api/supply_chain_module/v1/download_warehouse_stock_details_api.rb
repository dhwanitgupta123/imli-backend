#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Download warehouse stock details api, it validates the request, call the
    # warehouse service and return the JsonResponse
    #
    class DownloadWarehouseStockDetailsApi < SupplyChainBaseModule::V1::BaseApi
      WAREHOUSE_SERVICE = SupplyChainModule::V1::WarehouseService
      WAREHOUSE_MAPPER = SupplyChainModule::V1::WarehouseMapper
      WAREHOUSE_UTIL = SupplyChainModule::V1::WarehouseUtil
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding ID to get warehouse and returns the
      # success or error response
      #
      # @param request [integer] Request, Warehouse id
      #
      # @return [JsonResponse]
      #
      def enact(params)
        warehouse_service = WAREHOUSE_SERVICE.new
        begin
          validate_request(params)
          warehouse_stock_details = warehouse_service.download_warehouse_stock_details(params)
          warehouse_stock = WAREHOUSE_MAPPER.map_warehouse_stock_to_hash(warehouse_stock_details[:stock_details])
          link = WAREHOUSE_UTIL.generate_stock_sheet_link(warehouse_stock)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_warehouse_response({
            link: link
          })
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError, CUSTOM_ERROR_UTIL::FileNotFound => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params warehouse id to be fetched
      #
      # @param request [Warehouse id]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(params)
        if params[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_MISSING)
        end
      end
    end
  end
end

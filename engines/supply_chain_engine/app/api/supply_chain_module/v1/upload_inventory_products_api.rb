#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # UploadInventoryProductsApi, it validates the request, call the
    # inventory service and return the JsonResponse
    #
    class UploadInventoryProductsApi < SupplyChainBaseModule::V1::BaseApi
      INVENTORY_SERVICE = SupplyChainModule::V1::InventoryService
      INVENTORY_MAPPER = SupplyChainModule::V1::InventoryMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to upload inventory and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have inventory arguments to upload
      #
      # @return [JsonResponse]
      #
      def enact(request)
        inventory_service_class = INVENTORY_SERVICE.new

        begin
          validate_request(request)
          inventory_service_class.upload_products(request)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_success_response
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request[:file_path].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{ field: 'Products File' })
        end
      end
    end
  end
end

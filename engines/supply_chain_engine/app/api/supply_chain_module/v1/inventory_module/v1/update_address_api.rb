#
# Module to handle all the functionalities related to inventory
#
module SupplyChainModule::V1
  module InventoryModule::V1
    #
    # Update inventory address api, it validates the request, call the
    # inventory service and return the JsonResponse
    #
    class UpdateAddressApi < SupplyChainBaseModule::V1::BaseApi
      INVENTORY_SERVICE = SupplyChainModule::V1::InventoryService
      INVENTORY_MAPPER = SupplyChainModule::V1::InventoryMapper
      def initialize(params)
        @params = params
        super
      end

      # To update an address of a inventory
      # 
      # Parameters::
      #   * address: [hash] inventory_name, address_line1, address_line2, landmark, area_id
      # 
      # Description::
      #   * API function to update address of a inventory
      #    
      # @return [response] 
      # 
      def enact(request)
        inventory_service = INVENTORY_SERVICE.new
        begin
          validate_request(request)
          inventory = inventory_service.update_inventory_address(request)
          inventory_address = INVENTORY_MAPPER.map_inventory_address_to_hash(inventory)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_inventory_response(inventory_address)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::RecordNotFoundError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        end
      end

      private

      #
      # Validates the required params inventory address
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PARAMETER_MISSING)
        end
        if request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{ field: 'Address Id' })
        end
      end
    end
  end
end

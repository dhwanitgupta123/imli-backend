#
# Module to handle all the functionalities related to inventory
#
module SupplyChainModule::V1
  module InventoryModule::V1
    #
    # Get all inventory address api, it validates the request, call the
    # inventory service and return the JsonResponse
    #
    class GetAllAddressApi < SupplyChainBaseModule::V1::BaseApi
      INVENTORY_SERVICE = SupplyChainModule::V1::InventoryService
      INVENTORY_MAPPER = SupplyChainModule::V1::InventoryMapper
      def initialize(params)
        @params = params
        super
      end

      # To get all addresses of a inventory
      # 
      # Description::
      #   * API function to get all addresses of a inventory
      #    
      # @return [response] 
      # 
      def enact(inventory_id)
        inventory_service = INVENTORY_SERVICE.new
        begin
          validate_request(inventory_id)
          inventory = inventory_service.get_all_inventory_address(inventory_id)
          inventory_address = INVENTORY_MAPPER.map_inventory_address_to_hash(inventory)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_inventory_response(inventory_address)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the request to get all address of a inventory
      #
      def validate_request(inventory_id)
        if inventory_id.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{ field: 'Inventory' })
        end
      end
    end
  end
end

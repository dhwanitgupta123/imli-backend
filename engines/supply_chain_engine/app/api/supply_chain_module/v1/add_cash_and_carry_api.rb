#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Add cash and carry api, it validates the request, call the
    # cash and carry service and return the JsonResponse
    #
    class AddCashAndCarryApi < SupplyChainBaseModule::V1::BaseApi
      CASH_AND_CARRY_SERVICE = SupplyChainModule::V1::CashAndCarryService
      CASH_AND_CARRY_MAPPER = SupplyChainModule::V1::CashAndCarryMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to create c&c and returns the
      # success or error response
      #
      # @param request [Hash] Request object, c&C params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        cash_and_carry_service_class = CASH_AND_CARRY_SERVICE.new
        begin
          validate_request(request)
          c_and_c = cash_and_carry_service_class.create_cash_and_carry(request)
          c_and_c_array = CASH_AND_CARRY_MAPPER.map_cash_and_carry_to_hash(c_and_c)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_cash_and_carry_response(c_and_c_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params c_and_c
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:name].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::CASH_AND_CARRY_NAME_MISSING)
        end
      end
    end
  end
end

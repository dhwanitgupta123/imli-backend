#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetDistributor api, it validates the request, call the
    # distributor service and return the JsonResponse
    #
    class GetDistributorApi < SupplyChainBaseModule::V1::BaseApi
      DISTRIBUTOR_SERVICE = SupplyChainModule::V1::DistributorService
      DISTRIBUTOR_MAPPER = SupplyChainModule::V1::DistributorMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return distributor corresponding to id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        distributor_service_class = DISTRIBUTOR_SERVICE.new

        if request.blank? || request[:id].blank?
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(
            CONTENT::DISTRIBUTOR_MISSING)
        end

        begin
          distributor = distributor_service_class.get_distributor_by_id(request[:id])
          distributor = DISTRIBUTOR_MAPPER.map_distributor_to_hash(distributor)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_distributor_response(distributor)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end

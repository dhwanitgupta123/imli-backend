#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # ChangeDistributorStateApi api, it validates the request, call the
    # distributor service to change the state of given distributor id
    #
    class ChangeDistributorStateApi < SupplyChainBaseModule::V1::BaseApi
      DISTRIBUTOR_SERVICE = SupplyChainModule::V1::DistributorService
      DISTRIBUTOR_MAPPER = SupplyChainModule::V1::DistributorMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to change distributor state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, change state params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        distributor_service_class = DISTRIBUTOR_SERVICE.new
        begin
          validate_request(request)
          distributor = distributor_service_class.change_distributor_state({ id: request[:id], event: request[:event].to_i })
          distributor = DISTRIBUTOR_MAPPER.map_distributor_to_hash(distributor)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_distributor_response(distributor)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        end
      end

      private

      #
      # Validates the required params of change state
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::DISTRIBUTOR_MISSING)
        end
        if request[:event].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end

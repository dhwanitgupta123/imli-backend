#
# Module to handle all the functionalities related to warehouse
#
module SupplyChainModule::V1
  module WarehouseModule::V1
    #
    # Get all warehouse address api, it validates the request, call the
    # wareshouse service and return the JsonResponse
    #
    class GetAllAddressApi < SupplyChainBaseModule::V1::BaseApi
      WAREHOUSE_SERVICE = SupplyChainModule::V1::WarehouseService
      WAREHOUSE_MAPPER = SupplyChainModule::V1::WarehouseMapper
      def initialize(params)
        @params = params
        super
      end

      # To get all addresses of a warehouse
      # 
      # Description::
      #   * API function to get all addresses of a warehouse
      #    
      # @return [response] 
      # 
      def enact(warehouse_id)
        warehouse_service = WAREHOUSE_SERVICE.new
        begin
          validate_request(warehouse_id)
          warehouse = warehouse_service.get_all_warehouse_address(warehouse_id)
          warehouse_address = WAREHOUSE_MAPPER.map_warehouse_address_to_array(warehouse)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_warehouse_response(warehouse_address)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the request to get all address of a warehouse
      #
      def validate_request(warehouse_id)
        if warehouse_id.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{ field: 'Warehouse' })
        end
      end
    end
  end
end

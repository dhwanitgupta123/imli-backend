#
# Module to handle all the functionalities related to warehouse
#
module SupplyChainModule::V1
  module WarehouseModule::V1
    #
    # Add warehouse address api, it validates the request, call the
    # wareshouse service and return the JsonResponse
    #
    class AddAddressApi < SupplyChainBaseModule::V1::BaseApi
      WAREHOUSE_SERVICE = SupplyChainModule::V1::WarehouseService
      WAREHOUSE_MAPPER = SupplyChainModule::V1::WarehouseMapper
      def initialize(params)
        @params = params
        super
      end

      # To add an address of a warehouse
      # 
      # Parameters::
      #   * address: [hash] warehouse_name, address_line1, address_line2, landmark, area_id
      #   * address_line1, address_line2, area_id is must
      # 
      # Description::
      #   * API function to add address of a warehouse
      #    
      # @return [response] 
      # 
      def enact(request)
        warehouse_service = WAREHOUSE_SERVICE.new
        begin
          validate_request(request)
          warehouse = warehouse_service.add_warehouse_address(request)
          warehouse_address = WAREHOUSE_MAPPER.map_warehouse_address_to_array(warehouse)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_warehouse_response(warehouse_address)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::RecordNotFoundError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        end
      end

      private

      #
      # Validates the required params warehouse address
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PARAMETER_MISSING)
        end
        if request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{ field: 'Warehouse' })
        end
        if request[:addresses].first[:address_line1].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{ field: 'Address line 1' })
        end
        if request[:addresses].first[:area_id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{ field: 'Area' })
        end
      end
    end
  end
end

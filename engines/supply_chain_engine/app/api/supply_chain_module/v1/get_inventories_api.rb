#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetInventories api, it validates the request, call the
    # inventory service and return the JsonResponse
    #
    class GetInventoriesApi < SupplyChainBaseModule::V1::BaseApi
      INVENTORY_SERVICE = SupplyChainModule::V1::InventoryService
      INVENTORY_MAPPER = SupplyChainModule::V1::InventoryMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the inventories
      #
      # @return [JsonResponse]
      #
      def enact(request)
        inventory_service_class = INVENTORY_SERVICE.new

        begin
          response = inventory_service_class.get_all_inventories(request)
          inventories = response[:elements]
          page_count = response[:page_count]
          inventory_array = INVENTORY_MAPPER.map_inventories_to_array(inventories)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_inventory_response(inventory_array, page_count)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # AddDistributor api, it validates the request, call the
    # distributor service and return the JsonResponse
    #
    class VendorRegistrationApi < SupplyChainBaseModule::V1::BaseApi
      DISTRIBUTOR_SERVICE = SupplyChainModule::V1::DistributorService
      DISTRIBUTOR_MAPPER = SupplyChainModule::V1::DistributorMapper
      GENERAL_HELPER = CommonModule::V1::GeneralHelper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to create distributor and returns the
      # success or error response
      #
      # @param request [Hash] Request object, distributor params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        distributor_service_class = DISTRIBUTOR_SERVICE.new
        begin
          validate_request(request)
          distributor = distributor_service_class.transactional_create_distributor_with_inventory_and_address(request)
          distributor = DISTRIBUTOR_MAPPER.map_distributor_to_hash(distributor)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_distributor_response(distributor)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::RecordNotFoundError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        end
      end

      private

      #
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:name].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::DISTRIBUTOR_NAME_MISSING)
        end
        if request[:cst_no].blank? && request[:tin_no].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{ field: 'CST/TIN no.' })
        end
        if request[:contact_number].present?
          unless GENERAL_HELPER.phone_number_valid?(request[:contact_number])
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WRONG_PHONE_NUMBER)
          end
        end
        if request[:email_ids].present?
          request[:email_ids].each do |email_id|
            unless GENERAL_HELPER.email_id_valid?(email_id)
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EMAIL_ID)
            end
          end
        end
        if request[:inventory].present?
          if request[:inventory][:address].present?
            request = request[:inventory][:address]
            if request[:address_line1].blank?
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{ field: 'Address line 1' })
            end
            if request[:area_id].blank?
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{ field: 'Area' })
            end
          # else
          #   raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{ field: 'Address details' })
          end
        # else
        #   raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{ field: 'Address details' })
        end
      end
    end
  end
end

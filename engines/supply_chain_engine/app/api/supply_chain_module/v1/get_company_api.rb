#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetCompanies api, it validates the request, call the
    # company service and return the JsonResponse
    #
    class GetCompanyApi < SupplyChainBaseModule::V1::BaseApi
      COMPANY_SERVICE = SupplyChainModule::V1::CompanyService
      COMPANY_MAPPER = SupplyChainModule::V1::CompanyMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return company corresponding to id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        company_service_class = COMPANY_SERVICE.new

        if request.blank? || request[:id].blank?
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(
            CONTENT::COMPANY_MISSING)
        end

        begin
          company = company_service_class.get_company_by_id(request[:id])
          company = COMPANY_MAPPER.map_company_to_hash(company)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_company_response(company)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetSellers api, it validates the request, call the
    # seller service and return the JsonResponse
    #
    class GetSellersApi < SupplyChainBaseModule::V1::BaseApi
      SELLER_SERVICE = SupplyChainModule::V1::SellerService
      SELLER_MAPPER = SupplyChainModule::V1::SellerMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the sellers
      #
      # @return [JsonResponse]
      #
      def enact(request)
        seller_service_class = SELLER_SERVICE.new

        begin
          sellers = seller_service_class.get_all_sellers(request)
          seller_array = SELLER_MAPPER.map_sellers_to_array(sellers)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_seller_response(seller_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end
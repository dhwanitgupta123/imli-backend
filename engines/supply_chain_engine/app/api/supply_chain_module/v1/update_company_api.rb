#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # UpdateCompany api, it validates the request, call the
    # company service and return the JsonResponse
    #
    class UpdateCompanyApi < SupplyChainBaseModule::V1::BaseApi
      COMPANY_SERVICE = SupplyChainModule::V1::CompanyService
      COMPANY_MAPPER = SupplyChainModule::V1::CompanyMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to update company and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have company arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request, company_id)
        company_service_class = COMPANY_SERVICE.new

        begin
          validate_request(request)
          company = company_service_class.update_company(request, company_id)
          company = COMPANY_MAPPER.map_company_to_hash(company)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_company_response(company)
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::COMPANY_MISSING)
        end
      end
    end
  end
end

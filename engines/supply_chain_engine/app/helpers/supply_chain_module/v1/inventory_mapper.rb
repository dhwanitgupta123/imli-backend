#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Map inventory object to Json
    #
    module InventoryMapper

      def self.map_inventory_to_hash(inventory)
        return {} if inventory.blank?
        company = inventory.distributor.company
        company_name = 'ALL'
        company_name = company.name if company.present?
        return {
          id: inventory.id,
          name: inventory.name + ' (' + company_name + ')',
          status: inventory.status,
          distributor_id: inventory.distributor_id,
          address: get_inventory_address_details(inventory.inventory_address)
        }
      end

      def self.map_inventories_to_array(inventories)
        inventories_array = []
        return inventories_array if inventories.blank?
        inventories.each do |inventory|
          inventories_array.push(map_inventory_to_hash(inventory))
        end
        inventories_array
      end

      def self.map_inventory_to_array(inventory)
        [ map_inventory_to_hash(inventory) ]
      end

      def self.map_short_inventory_to_hash(inventory)
        return {} if inventory.blank?
        company = inventory.distributor.company
        company_name = 'ALL'
        company_name = company.name if company.present?
        return {
          id: inventory.id,
          name: inventory.name + ' (' + company_name + ')',
          status: inventory.status,
          distributor_id: inventory.distributor_id,
        }
      end

      def self.get_inventory_address_details(address)
        return {} if address.blank?
        response = {
          id: address.id,
          inventory_name: address.inventory_name || "",
          address_line1: address.address_line1 || "",
          address_line2: address.address_line2 || "",
          landmark: address.landmark || "",
          area: address.area.name || "",
          city: address.area.city.name || "",
          state: address.area.city.state.name || "",
          country: address.area.city.state.country.name || "",
          pincode: address.area.pincode || ""
        }
        return response
      end

      def self.map_inventory_address_to_hash(inventory)
        return {} if inventory.blank?
        response = {
          id: inventory.id,
          address: get_inventory_address_details(inventory.inventory_address)
        }
        return response
      end
    end
  end
end

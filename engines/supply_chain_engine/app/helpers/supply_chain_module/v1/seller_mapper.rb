#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Map seller object to Json
    #
    module SellerMapper

      def self.map_seller_to_hash(seller)
        {
          id: seller.id,
          name: seller.name,
          status: seller.status
        }
      end

      def self.map_sellers_to_array(args)
        sellers = args[:seller]
        sellers_array = []
        sellers.each do |seller|
          sellers_array.push(map_seller_to_hash(seller))
        end
        return { seller: sellers_array, page_count: args[:page_count] }
      end

      def self.map_seller_to_array(seller_args)
        seller = [ map_seller_to_hash(seller_args) ]
        return { seller: seller }
      end
    end
  end
end
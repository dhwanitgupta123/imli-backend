#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Map distributor object to Json
    #
    module DistributorMapper

      COMPANY_MAPPER = SupplyChainModule::V1::CompanyMapper
      INVENTORY_MAPPER = SupplyChainModule::V1::InventoryMapper

      def self.map_distributor_to_hash(distributor)
        return {} if distributor.blank?
        company_hash = COMPANY_MAPPER.map_short_company_to_hash(distributor.company)
        inventory_hash = INVENTORY_MAPPER.map_inventory_to_hash(distributor.inventory)
        company_name = 'ALL'
        company_name = company_hash[:name] if company_hash[:name].present?
        response = {
          id: distributor.id,
          name: distributor.name,
          cst_no: distributor.cst_no || '',
          tin_no: distributor.tin_no || '',
          bank_name: distributor.bank_name || '',
          bank_branch_name: distributor.branch_name || '',
          ifsc_code: distributor.ifsc_code || '',
          bank_account_name: distributor.bank_account_name || '',
          bank_account_number: distributor.bank_account_number || '',
          contact_person: distributor.contact_person || '',
          contact_number: distributor.contact_number || '',
          email_ids: distributor.email_ids || [],
          status: distributor.status,
          vendor_code: distributor.vendor_code || '',
          company: company_hash,
          inventory: inventory_hash
        }
        return response
      end

      def self.map_short_distributor_to_hash(distributor)
        return {} if distributor.blank?
        company_hash = COMPANY_MAPPER.map_short_company_to_hash(distributor.company)
        inventory_hash = INVENTORY_MAPPER.map_inventory_to_hash(distributor.inventory)
        company_name = 'ALL'
        company_name = company_hash[:name] if company_hash[:name].present?
        response = {
          id: distributor.id,
          name: distributor.name + ' (' + company_name + ')',
          status: distributor.status,
          vendor_code: distributor.vendor_code || ''
        }
        return response
      end

      def self.map_distributors_to_array(distributors)
        distributors_array = []
        return distributors_array if distributors.blank?
        distributors.each do |distributor|
          distributors_array.push(map_short_distributor_to_hash(distributor))
        end
        distributors_array
      end

      def self.map_distributor_to_array(distributor)
        [ map_distributor_to_hash(distributor) ]
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module WarehouseProductModule
  #
  # Version1 for WarehouseProduct module
  #
  module V1
    #
    # Map WarehouseBrandPack object to Json
    #
    module WarehouseBrandPackMapper

      PRICING_MAPPER = WarehousePricingModule::V1::WarehousePricingMapper
      BRAND_PACK_MAPPER = MasterProductModule::V1::BrandPackMapper

      def self.map_wbp_to_hash(wbp)
        if wbp.warehouse_pricings.present?
          pricing = PRICING_MAPPER.map_warehouse_pricing_to_hash(wbp.warehouse_pricings.first)
        end

        if wbp.brand_pack.present?
          brand_pack_hash = BRAND_PACK_MAPPER.map_brand_pack_to_hash(wbp.brand_pack)
        end

        response = {
          id: wbp.id,
          warehouse_id: wbp.warehouse_id,
          inventory_brand_packs: map_inventory_brand_packs(wbp.inventory_brand_packs),
          brand_pack_id: wbp.brand_pack_id,
          status: wbp.status,
          threshold_stock_value: wbp.threshold_stock_value,
          outbound_frequency_period_in_days: wbp.outbound_frequency_period_in_days,
          pricing: pricing,
          brand_pack: brand_pack_hash
        }
        return response
      end

      def self.get_wbp_short_hash(warehouse_brand_pack)
        if warehouse_brand_pack.brand_pack.present?
          brand_pack_hash = BRAND_PACK_MAPPER.map_short_brand_pack_to_hash(warehouse_brand_pack.brand_pack)
        end

        response = {
          id: warehouse_brand_pack.id,
          brand_pack: brand_pack_hash
        }
      end

      def self.map_wbps_to_array(args)
        wbps = args[:warehouse_brand_pack]
        wbp_array = []
        wbps.each do |wbp|
          wbp_array.push(map_wbp_to_hash(wbp))
        end
        return { warehouse_brand_pack: wbp_array, page_count: args[:page_count] }
      end

      def self.map_wbp_to_array(wbp)
        wbp = [ map_wbp_to_hash(wbp) ]
        return { warehouse_brand_pack: wbp }
      end

      def self.map_wbp_stock_threshold_to_hash(wbp)
        response = {
          threshold_stock_value: wbp.threshold_stock_value || 0,
          outbound_frequency_period_in_days: wbp.outbound_frequency_period_in_days || 0
        }
        return response
      end

      def self.map_wbps_stock_threshold_to_array(wbps)
        wbps_stock_array = []
        return wbps_stock_array if wbps.blank?
        wbps.each do |wbp|
          wbps_stock_array.push(map_wbp_stock_threshold_to_hash(wbp))
        end
        return wbps_stock_array
      end

      def self.map_inventory_brand_packs(inventory_brand_packs)
        response_array = []
        inventory_brand_packs.each do |ibp|
          response_array.push(ibp.id)
        end
        return response_array
      end

      def self.get_wbp_stock_hash(wbp_stock)
        wbp_hash = get_wbp_short_hash(wbp_stock[:warehouse_brand_pack])
        stock_hash = {
          inward: wbp_stock[:inward],
          outward: wbp_stock[:outward],
          expired: wbp_stock[:expired],
          damaged: wbp_stock[:damaged],
          available: wbp_stock[:available],
          last_audit_quantity: wbp_stock[:last_audit],
          audit_date: wbp_stock[:audit_date]
        }
        wbp_hash = wbp_hash.merge(stock: stock_hash)
        return wbp_hash
      end

      def self.get_wbp_procurement_hash(wbp_procurement)
        wbp_hash = get_wbp_short_hash(wbp_procurement[:warehouse_brand_pack])
        wbp_hash = wbp_hash.merge(map_wbp_stock_threshold_to_hash(wbp_procurement[:warehouse_brand_pack]))
        procurement_hash = {
          available: wbp_procurement[:available],
          sold_in_x_days: wbp_procurement[:sold_in_x_days] || 0
        }
        wbp_hash = wbp_hash.merge(procurement: procurement_hash)
        return wbp_hash
      end
    end
  end
end

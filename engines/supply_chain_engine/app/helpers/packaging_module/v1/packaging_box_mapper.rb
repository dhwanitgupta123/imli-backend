#
# Module to handle all the functionalities related to Packaging
#
module PackagingModule
  #
  # Version1 for Packaging module
  #
  module V1
    #
    # Map packaging_box object to Json
    #
    module PackagingBoxMapper

      def self.map_packaging_box_to_hash(packaging_box)
        {
          id: packaging_box.id,
          label: packaging_box.label,
          length: packaging_box.length,
          width: packaging_box.width,
          height: packaging_box.height,
          cost: packaging_box.cost,
          status: packaging_box.status
        }
      end

      def self.map_packaging_boxes_to_array(packaging_boxes)
        packaging_box_array = []
        packaging_boxes.each do |packaging_box|
          packaging_box_array.push(map_packaging_box_to_hash(packaging_box))
        end
        packaging_box_array
      end

      def self.add_packaging_box_api_response_hash(packaging_box)
        return {
          packaging_box: map_packaging_box_to_hash(packaging_box)
        }
      end

      def self.get_all_packaging_boxes_api_response_array(response)
        return {
          packaging_boxes: map_packaging_boxes_to_array(response[:elements]),
          page_count: response[:page_count]
        }
      end

      def self.get_packaging_box_api_response_hash(packaging_box)
        return {
          packaging_box: map_packaging_box_to_hash(packaging_box)
        }
      end

      def self.update_packaging_box_api_response_hash(packaging_box)
        return {
          packaging_box: map_packaging_box_to_hash(packaging_box)
        }
      end

      def self.change_packaging_box_state_api_response_hash(packaging_box)
        return {
          packaging_box: map_packaging_box_to_hash(packaging_box)
        }
      end

    end
  end
end
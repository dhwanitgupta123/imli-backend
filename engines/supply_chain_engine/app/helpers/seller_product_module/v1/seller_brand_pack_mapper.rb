#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    #
    # Map seller object to Json
    #
    module SellerBrandPackMapper

      PRICING_MAPPER = SellerPricingModule::V1::SellerPricingMapper
      BRAND_PACK_MAPPER = MasterProductModule::V1::BrandPackMapper

      def self.map_seller_brand_pack_to_hash(seller_brand_pack)
        if seller_brand_pack.seller_pricing.present?
          pricing = PRICING_MAPPER.map_seller_pricing_to_hash(seller_brand_pack.seller_pricing)
        end

        if seller_brand_pack.brand_pack.present?
          brand_pack_hash = BRAND_PACK_MAPPER.map_brand_pack_to_hash(seller_brand_pack.brand_pack)
        end
        response = {
          id: seller_brand_pack.id,
          status: seller_brand_pack.status,
          seller_id: seller_brand_pack.seller_id,
          warehouse_brand_packs: map_warehouse_brand_packs(seller_brand_pack.warehouse_brand_packs),
          pricing: pricing,
          brand_pack: brand_pack_hash
        }
        return response
      end

      def self.map_seller_brand_packs_to_array(args)
        seller_brand_packs = args[:seller_brand_pack]
        seller_brand_packs_array = []
        seller_brand_packs.each do |seller_brand_pack|
          seller_brand_packs_array.push(map_seller_brand_pack_to_hash(seller_brand_pack))
        end
        return { seller_brand_pack: seller_brand_packs_array, page_count: args[:page_count] }
      end

      def self.map_seller_brand_pack_to_array(seller_brand_pack_args)
        [ map_seller_brand_pack_to_hash(seller_brand_pack_args) ]
      end

      def self.map_warehouse_brand_packs(wbps)
        response_array = []
        wbps.each do |wbp|
          response_array.push(wbp.id)
        end
        return response_array
      end
    end
  end
end
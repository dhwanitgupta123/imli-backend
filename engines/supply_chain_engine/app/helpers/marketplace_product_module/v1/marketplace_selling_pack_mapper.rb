#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Map marketplace_selling_pack object to Json 
    #
    module MarketplaceSellingPackMapper

      MARKETPLACE_BRAND_PACK_MAPPER = MarketplaceProductModule::V1::MarketplaceBrandPackMapper
      PRICING_MAPPER = MarketplacePricingModule::V1::MarketplaceSellingPackPricingMapper
      MARKETPLACE_SELLING_PACK_DAO = MarketplaceProductModule::V1::MarketplaceSellingPackDao
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper
      DESCRIPTION_HELPER = DescriptionsModule::V1::DescriptionHelper

      def self.map_marketplace_selling_pack_to_hash(marketplace_selling_pack)
        return {} if marketplace_selling_pack.blank?

        if marketplace_selling_pack.marketplace_selling_pack_marketplace_brand_packs.present?
          marketplace_brand_packs = map_marketplace_brand_pack_quantity(
            marketplace_selling_pack.marketplace_selling_pack_marketplace_brand_packs)
        end
        if marketplace_selling_pack.marketplace_selling_pack_pricing.present?
          pricing = PRICING_MAPPER.map_marketplace_selling_pack_pricing_to_hash(
            marketplace_selling_pack.marketplace_selling_pack_pricing,
            marketplace_selling_pack.max_quantity,
            marketplace_selling_pack.is_ladder_pricing_active
          )
        end

        description_id = ''
        description_id = marketplace_selling_pack.description[:id] if marketplace_selling_pack.description.present?

        response = {
          id: marketplace_selling_pack.id,
          status: marketplace_selling_pack.status,
          primary_tags: marketplace_selling_pack.primary_tags,
          secondary_tags: marketplace_selling_pack.secondary_tags,
          display_name: marketplace_selling_pack.display_name,
          display_pack_size: marketplace_selling_pack.display_pack_size,
          max_quantity: marketplace_selling_pack.max_quantity,
          marketplace_brand_packs: marketplace_brand_packs,
          is_on_sale: marketplace_selling_pack.is_on_sale,
          mpsp_sub_category_id: marketplace_selling_pack.mpsp_sub_category_id,
          is_ladder_pricing_active: marketplace_selling_pack.is_ladder_pricing_active,
          dirty_bit: marketplace_selling_pack.dirty_bit,
          images: IMAGE_SERVICE_HELPER.get_images_hash_by_ids(marketplace_selling_pack.images),
          pricing: pricing,
          description_id: description_id,
          resource: marketplace_selling_pack.class.name
        }
        return response
      end

      def self.map_marketplace_selling_packs_to_array(marketplace_selling_packs, joined = false)
        marketplace_selling_pack_array = []
        return marketplace_selling_pack_array if marketplace_selling_packs.blank?
        
        if joined == false
          marketplace_selling_packs = MARKETPLACE_SELLING_PACK_DAO.get_aggregated_mpsps(marketplace_selling_packs)
        end

        marketplace_selling_packs.each do |marketplace_selling_pack|
          marketplace_selling_pack_array.push(map_marketplace_selling_pack_to_hash(marketplace_selling_pack))
        end
        marketplace_selling_pack_array
      end

      def self.map_marketplace_selling_pack_to_array(marketplace_selling_pack)
        [ map_marketplace_selling_pack_to_hash(marketplace_selling_pack) ]
      end

      def self.map_marketplace_brand_pack_quantity(mpsp_quantities)
        marketplace_brand_packs = []
        return marketplace_brand_packs if mpsp_quantities.blank?

        mpsp_quantities.each do |mpsp_quantity|
          mpbp = MARKETPLACE_BRAND_PACK_MAPPER.map_marketplace_brand_pack_to_hash(
            mpsp_quantity.marketplace_brand_pack)
          response = {
            marketplace_brand_pack: mpbp,
            quantity: mpsp_quantity.quantity
          }
          marketplace_brand_packs.push(response)
        end
        return marketplace_brand_packs
      end

    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Map product object to Json
    #
    module ProductMapper
      def self.map_product_to_hash(product)
        {
          id: product.id,
          name: product.name,
          logo_url: product.logo_url,
          sub_brand_id: product.sub_brand_id,
          initials: product.initials,
          product_code: product.product_code,
          status: product.status
        }
      end

      def self.map_products_to_array(products)
        products_array = []
        products.each do |product|
          products_array.push(map_product_to_hash(product))
        end
        products_array
      end

      def self.map_product_to_array(product)
        [ map_product_to_hash(product) ]
      end
    end
  end
end
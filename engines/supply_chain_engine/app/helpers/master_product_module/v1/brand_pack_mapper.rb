#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Map brand_pack object to Json
    #
    module BrandPackMapper

      IMAGE_SERVIC_HELPER = ImageServiceModule::V1::ImageServiceHelper
      IBP_MAPPER = InventoryProductModule::V1::InventoryBrandPackMapper
      WBP_MAPPER = WarehouseProductModule::V1::WarehouseBrandPackMapper

      def self.map_brand_pack_to_hash(brand_pack)
        return {} if brand_pack.blank?

        response = {
          id: brand_pack.id,
          sku: brand_pack.sku,
          mrp: brand_pack.mrp,
          sku_size: brand_pack.sku_size,
          unit: brand_pack.unit,
          description: brand_pack.description,
          shelf_life: brand_pack.shelf_life,
          images: IMAGE_SERVIC_HELPER.get_images_hash_by_ids(brand_pack.images),
          product_id: brand_pack.product_id,
          status: brand_pack.status,
          sub_category_id: brand_pack.sub_category_id,
          brand_pack_code: brand_pack.brand_pack_code,
          article_code: brand_pack.article_code,
          retailer_pack: brand_pack.retailer_pack
        }
        return response
      end

      def self.map_brand_pack_and_mpbp_to_hash(brand_pack)
        return {} if brand_pack.blank?
        if brand_pack.inventory_brand_packs.present?
          inventory_brand_packs_hash = IBP_MAPPER.map_short_inventory_brand_packs_to_array_with_inventory(brand_pack.inventory_brand_packs)
        end
        if brand_pack.warehouse_brand_packs.present?
          wbp_stock_hash = WBP_MAPPER.map_wbps_stock_threshold_to_array(brand_pack.warehouse_brand_packs)
        end
        response = {
          id: brand_pack.id,
          sku: brand_pack.sku,
          mrp: brand_pack.mrp,
          sku_size: brand_pack.sku_size,
          unit: brand_pack.unit,
          description: brand_pack.description,
          shelf_life: brand_pack.shelf_life,
          images: IMAGE_SERVIC_HELPER.get_images_hash_by_ids(brand_pack.images),
          product_id: brand_pack.product_id,
          status: brand_pack.status,
          sub_category_id: brand_pack.sub_category_id,
          brand_pack_code: brand_pack.brand_pack_code,
          article_code: brand_pack.article_code,
          retailer_pack: brand_pack.retailer_pack,
          inventory_brand_packs: inventory_brand_packs_hash || [],
          warehouse_brand_packs: wbp_stock_hash || []
        }
        return response
      end

      def self.map_short_brand_pack_to_hash(brand_pack)
        return {} if brand_pack.blank?
        response = {
          id: brand_pack.id,
          sku: brand_pack.sku,
          brand_pack_code: brand_pack.brand_pack_code,
          unit: brand_pack.unit,
          mrp: brand_pack.mrp,
          sku_size: brand_pack.sku_size
        }
        return response
      end

      def self.map_brand_packs_to_array(brand_packs)
        brand_packs_array = []
        brand_packs.each do |brand_pack|
          brand_packs_array.push(map_brand_pack_to_hash(brand_pack))
        end
        brand_packs_array
      end

      def self.map_brand_pack_to_array(brand_pack)
        [map_brand_pack_to_hash(brand_pack)]
      end
    end
  end
end

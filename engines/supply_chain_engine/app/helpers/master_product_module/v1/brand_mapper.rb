#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Map brand object to Json
    #
    module BrandMapper
      def self.map_brand_to_hash(brand)
        {
          id: brand.id,
          name: brand.name,
          code: brand.code,
          logo_url: brand.logo_url,
          company_id: brand.company_id,
          initials: brand.initials,
          status: brand.status
        }
      end

      def self.map_brands_to_array(brands)
        brands_array = []
        brands.each do |brand|
          brands_array.push(map_brand_to_hash(brand))
        end
        brands_array
      end
    end
  end
end

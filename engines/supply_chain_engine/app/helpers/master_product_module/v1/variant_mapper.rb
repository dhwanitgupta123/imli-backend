#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Map variant object to Json
    #
    module VariantMapper

      #
      # Create hash of variant 
      #
      def self.map_variant_to_hash(variant)
        {
          id: variant.id,
          name: variant.name,
          initials: variant.initials
        }
      end

      #
      # Create array of hash of variants
      #
      def self.map_variants_to_array(variants)
        variants_array = []
        variants.each do |variant|
          variants_array.push(map_variant_to_hash(variant))
        end
        variants_array
      end

      #
      # Map hash of variant to array
      #
      def self.map_variant_to_array(variant)
        [ map_variant_to_hash(variant) ]
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
  	#
    # Module to generate BrandService responses & other functionalities
    #
    # @author [kartik]
    #
    module BrandHelper
    	#
      # defining global variable with versioning
      #
    	SUB_BRAND_HELPER = MasterProductModule::V1::SubBrandHelper

    	#
      # Creates brands to sub_brands tree with dummy brand and sub_brand
      #
      def self.get_brands_array(brands)
        brands_array = []
        
        return brands_array if brands.blank?

        brands.each do |brand|
          brand_hash = get_brand_detail(brand)
          sub_brands = SUB_BRAND_HELPER.get_sub_brands_array(brand.sub_brands)

          brand_hash = brand_hash.merge({sub_brands: sub_brands})
          brands_array.push(brand_hash)
        end

        return brands_array
      end

      #
      # Creates brand hash with it's details.
      # Extension: Can be extended with more details later
      #
      def self.get_brand_detail(brand)
        brand_hash = {}
        return brand_hash if brand.blank?

        brand_hash = {
          id: brand.id,
          name: brand.name,
        }   
      end

    end
  end
end
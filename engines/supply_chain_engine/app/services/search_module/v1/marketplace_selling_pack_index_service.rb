#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # Logic layer which is responsible to create marketplace_selling_pack index
    # and retrieve results
    #
    class MarketplaceSellingPackIndexService < BaseModule::V1::BaseService

      CATEGORIZATION_API_RESPONSE_DECORATOR = CategorizationModule::V1::MpspCategorizationApiResponseDecorator
      QUERY_HELPER = SearchModule::V1::QueryHelper
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      QUERY_LOG_WORKER = LoggingModule::V1::QueryLogWorker
      PAGINATION_UTIL = CommonModule::V1::Pagination
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates
      MARKETPLACE_SELLING_PACK_MAPPER = MarketplaceProductModule::V1::MarketplaceSellingPackMapper
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize
        @marketplace_selling_pack_dao = MarketplaceProductModule::V1::MarketplaceSellingPackDao
        @marketplace_selling_pack_search_model = SearchModule::V1::MarketplaceSellingPackSearchModel
        @marketplace_selling_pack_search_model_reader = SearchModule::V1::MarketplaceSellingPackModelReader
        initialize_marketplace_selling_pack_repository
      end

      #
      # This function checks if marketplace-selling-pack-index already present or not
      # if not then retrieve data from MarketplaceSellingPack table and index the data
      #
      # but if force == 'true' then it will always re-index the data
      #
      # @param force [String] to specify force re-indexing
      #
      def initialize_index(force = 'false')
        return false if @marketplace_selling_pack_repository.nil? || !repository_exists?
        return false if (force != 'true') && index_exists?
        
        delete_previous_index

        pagination_params = {}
        pagination_params[:per_page] = PAGINATION_UTIL::INFINITE_PER_PAGE

        marketplace_selling_pack_dao_class = @marketplace_selling_pack_dao.new

        all_marketplace_selling_pack = marketplace_selling_pack_dao_class.get_all(pagination_params)
        all_marketplace_selling_pack = all_marketplace_selling_pack[:elements]

        all_marketplace_selling_pack.each do |marketplace_selling_pack|
          marketplace_selling_pack = get_marketplace_selling_pack_search_model(marketplace_selling_pack)
          save(marketplace_selling_pack)
        end
      end

      #
      # save the document in repositroy
      #
      def save(marketplace_selling_pack)
        @marketplace_selling_pack_repository.save(marketplace_selling_pack)
      end

      #
      # This function breaks the query by white space convert it into tokens
      # and corresponding to each token it creates
      # 'fuzzy query'
      # 'regexp query'
      # 'term query'
      # and concat them in boolean query through OR ('should') operator
      #
      # Example : query = 'tata tea'
      # 1. tokens = ['tata', 'tea']
      # 2. let regexp_query for token1 in text field  = regexp1 and for token2 = regexp2
      # 3. let fuzzy_query for token1 in text field = fuzzy1 and for token2 = fuzzy2
      # 4. let fuzzy_query for token1 in context field = c_fuzzy1 and for token2 = c_fuzzy2
      # 5. let match_query = match_query
      # 6. then boolean_query = {"should": [regexp1, regexp2, fuzzy1, fuzzy2, c_fuzzy1, c_fuzzy2], "must" :[match_query]}
      # where
      #   => regexp1 = { regexp => { 'tata.*', 2 } }, here 2 is to boost query which means
      #                                               we are giving 2 times weight to this field 
      #   => regexp2 = { regexp => {'tea.*', 2 } } same as above 
      #   => fuzzy1 = {fuzzy => { 'tata', 1 } } here first param is value and second is fuzzyness
      #                                        fuzzyness specify edit distance parameter, which means
      #                                        if fuzzyness = 1 then query which can be converted to
      #                                        indexed string using one operation then it will return true
      #   => fuzzy2 = { fuzzy => {'tea', 1 } } same as above
      #   ABOVE QUERIES ARE CORRESPONDING TO "TEXT" field
      #   => c_fuzzy1 = { fuzzy => {'tea', 1 } } but this is for field "CONTEXT"
      #   => match_query = { match => { 'status', 1} } this will return document with status 1
      #   => boolean_query => {boolean => { 'should': [regexp1, regexp2, fuzzy1, fuzzy2, c_fuzzy1, c_fuzzy2] , 'must': [match_query]}}
      #   above query is same as (regexp1 OR regexp2 OR fuzzy1 OR fuzzy2 OR c_fuzzy1 OR c_fuzzy2) AND (match_query)
      #
      # @param query [String] query string
      # @param n = 10 [Integer] top n results
      #
      # @return [Array] Array of marketplace_selling_pack object
      #
      def get_results(query, n = 10)
        raise CUSTOM_ERRORS_UTIL::RunTimeError.new(CONTENT_UTIL::MISSING_REPOSITORY)  if @marketplace_selling_pack_repository.nil? ||
                                                                            !repository_exists? || !index_exists?

        query = query.downcase

        boolean_query = QUERY_HELPER.get_boolean_query

        tokens = query.split(' ')
        match_status_query = QUERY_HELPER.get_match_query('status', 1)
        match_parent_active_query = QUERY_HELPER.get_match_query('is_parent_active', true)
        tokens.each do |token|
          regexp_query = QUERY_HELPER.get_regexp_query('primary_tags', token + '.*', 5)
          fuzzy_query = QUERY_HELPER.get_fuzzy_query('primary_tags', token, 1, 0.2)
          fuzzy_query_context = QUERY_HELPER.get_fuzzy_query('secondary_tags', token, 1, 0.01)
          boolean_query = QUERY_HELPER.add_clause_in_boolean_query('should', regexp_query, boolean_query)
          boolean_query = QUERY_HELPER.add_clause_in_boolean_query('should', fuzzy_query, boolean_query)
          boolean_query = QUERY_HELPER.add_clause_in_boolean_query('should', fuzzy_query_context, boolean_query)
        end
        boolean_query = QUERY_HELPER.add_clause_in_boolean_query('must', match_status_query, boolean_query)
        boolean_query = QUERY_HELPER.add_clause_in_boolean_query('must', match_parent_active_query, boolean_query)
        results = search(boolean_query, n)
        marketplace_selling_pack_search_model_reader = @marketplace_selling_pack_search_model_reader.new(results)
        QUERY_LOG_WORKER.perform_async(query, marketplace_selling_pack_search_model_reader.get_attribute_array)
        return marketplace_selling_pack_search_model_reader
      end

      # 
      # This function set is_parent_active to true for all mpsp indexes
      #
      # @param marketplace_selling_packs [Array]  mpsps
      #
      def activate_mpsps_parent_status(marketplace_selling_packs)
        return if marketplace_selling_packs.blank?

        marketplace_selling_packs.each do |marketplace_selling_pack|
          update_marketplace_selling_pack_parent_status(marketplace_selling_pack.id, true)
        end
      end

      # 
      # This function set is_parent_active to false for all mpsp indexes
      #
      # @param marketplace_selling_packs [Array]  mpsps
      #
      def deactivate_mpsps_parent_status(marketplace_selling_packs)
        return if marketplace_selling_packs.blank?

        marketplace_selling_packs.each do |marketplace_selling_pack|
          update_marketplace_selling_pack_parent_status(marketplace_selling_pack.id, false)
        end
      end

      #
      # update marketplace_selling_pack is_parent_active corresponding to the marketplace_selling_pack_id in repository
      #
      # @param marketplace_selling_pack_id [Integer] brand pack id to update
      # @param [Booleand] is_parent_active
      #
      # @error [RunTimeError] If it is not able to find the document with marketplace_selling_pack_id
      #
      def update_marketplace_selling_pack_parent_status(marketplace_selling_pack_id, is_parent_active)
        begin
          @marketplace_selling_pack_repository.update({id: marketplace_selling_pack_id, is_parent_active: is_parent_active})
        rescue => e
          raise CUSTOM_ERRORS_UTIL::RunTimeError.new(CONTENT_UTIL::MISSING_DOCUMENT) 
        end
      end

      #
      # update marketplace_selling_pack status corresponding to the marketplace_selling_pack_id in repository
      #
      # @param marketplace_selling_pack_id [Integer] brand pack id to update
      # @param [Integer] status of branc pack to update
      #
      # @error [RunTimeError] If it is not able to find the document with marketplace_selling_pack_id
      #
      def update_marketplace_selling_pack_status(marketplace_selling_pack_id, status)

        begin
          @marketplace_selling_pack_repository.update({id: marketplace_selling_pack_id, status: status})
        rescue => e
          raise CUSTOM_ERRORS_UTIL::RunTimeError.new(CONTENT_UTIL::MISSING_DOCUMENT)
        end
      end


      #
      # This function add the mpsp in mpsp_index
      #
      # @param marketplace_selling_pack [Model] model object of mpsp
      #
      def add_document(marketplace_selling_pack)
        return unless get_document_by_id(marketplace_selling_pack.id) == false

        marketplace_selling_pack_model = get_marketplace_selling_pack_search_model(marketplace_selling_pack)
        begin
          @marketplace_selling_pack_repository.save(marketplace_selling_pack_model)
        rescue => e
          raise CUSTOM_ERRORS_UTIL::RunTimeError.new(CONTENT_UTIL::UNABLE_TO_ADD_DOCUMENT)
        end
      end

      #
      # This function update the index with given mpsp
      #
      # @param marketplace_selling_pack [MOdel] updated model of mpsp
      #
      # @error [RunTimeError] if document with given not found
      #
      def update_document(marketplace_selling_pack)

        marketplace_selling_pack_model = get_marketplace_selling_pack_search_model(marketplace_selling_pack)
        begin
          @marketplace_selling_pack_repository.update(marketplace_selling_pack_model)
        rescue => e
          raise CUSTOM_ERRORS_UTIL::RunTimeError.new(CONTENT_UTIL::MISSING_DOCUMENT)
        end
      end

      #
      # This will delete the index if it exists
      #
      def delete_index
        return true unless @marketplace_selling_pack_repository.client.indices.exists(index: @marketplace_selling_pack_repository.index)
        @marketplace_selling_pack_repository.client.indices.delete(index: @marketplace_selling_pack_repository.index)
      end

      #
      # Return document by given id
      #
      # @param id [Integer] id of mpsp
      # 
      # @return [Document] this return document if it succeed to find
      # @return [False] if document not found in the repository
      #
      def get_document_by_id(id)
        begin
          @marketplace_selling_pack_repository.find(id)
        rescue Elasticsearch::Persistence::Repository::DocumentNotFound => e
          return false
        end
      end

      #
      # Search query in repository
      #
      def search(query, n)
        @marketplace_selling_pack_repository.search(query: query, size: n).to_a
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # initializing marketplace_selling_pack repository
      #
      def initialize_marketplace_selling_pack_repository
        @marketplace_selling_pack_repository = MARKETPLACE_SELLING_PACK_INDEX_REPOSITORY
      end

      #
      # This function checks if marketplace_selling_pack repository connected successfully to elastic search server
      # it returns true if connected successfully else false
      #
      # @return [Boolean] true if exists else false
      #
      def repository_exists?
        begin
          @marketplace_selling_pack_repository.client.cluster.health
        rescue Faraday::ConnectionFailed
          return false
        end
        true
      end

      #
      # This function check if index with index-name specified in marketplace_selling_pack repository exists in
      # elastic search server of not
      #
      # @return [Boolean] true if index exists else false
      #
      def index_exists?
        begin
          return false unless @marketplace_selling_pack_repository.client.indices.exists(index: @marketplace_selling_pack_repository.index)
          return true if @marketplace_selling_pack_repository.count > 0
        rescue Faraday::ConnectionFailed
          return false
        end
        return false
      end

      #
      # map marketplace_selling_pack to marketplace_selling_pack model object
      #
      # @param marketplace_selling_pack [Object] MarketplaceSellingPack object
      #
      # @return [MarketplaceSellingPackModel] marketplace_selling_pack_reader object
      #
      def get_marketplace_selling_pack_search_model(marketplace_selling_pack)
        mpsp_hash = CATEGORIZATION_API_RESPONSE_DECORATOR.create_display_product_hash(marketplace_selling_pack)
        @marketplace_selling_pack_search_model.new(
          {
            id: marketplace_selling_pack.id,
            primary_tags: marketplace_selling_pack.primary_tags,
            secondary_tags: marketplace_selling_pack.secondary_tags,
            status: marketplace_selling_pack.status,
            product: mpsp_hash,
            is_parent_active: is_node_active?(marketplace_selling_pack)
        })
      end

      #
      # This function re-initialize index, delete the existing one
      #
      def delete_previous_index
        @marketplace_selling_pack_repository.create_index! force: true
      end

      # 
      # This function return true if mpsp is active and all its parent nodes [sub_category, category, department]
      # are active else returns false
      #
      # @param marketplace_selling_pack [Object] mpsp
      # 
      # @return [Boolean] true if mpsp is active and all parent nodes are active else false
      #
      def is_node_active?(marketplace_selling_pack)
        return @marketplace_selling_pack_dao.is_node_active?(marketplace_selling_pack)
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class MarketplaceBrandPackService < SupplyChainBaseModule::V1::BaseService

      MARKETPLACE_BRAND_PACK_DAO = MarketplaceProductModule::V1::MarketplaceBrandPackDao
      BRAND_PACK_DAO = MasterProductModule::V1::BrandPackDao
      BRAND_PACK_SERVICE = MasterProductModule::V1::BrandPackService
      SLLER_BRAND_PACK_DAO = SellerProductModule::V1::SellerBrandPackDao
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates
      INVENTORY_BRAND_PACK_SERVICE = InventoryProductModule::V1::InventoryBrandPackService
      SELLER_BRAND_PACK_SERVICE = SellerProductModule::V1::SellerBrandPackService
      WAREHOUSE_BRAND_PACK_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      INVENTORY_SERVICE = SupplyChainModule::V1::InventoryService
      WAREHOUSE_SERVICE = SupplyChainModule::V1::WarehouseService
      SELLER_SERVICE = SupplyChainModule::V1::SellerService

      def initialize(params = {})
        super
        @params = params
        @marketplace_brand_pack_dao = MARKETPLACE_BRAND_PACK_DAO.new
        @brand_pack_dao = BRAND_PACK_DAO.new
        @seller_brand_pack_dao = SLLER_BRAND_PACK_DAO.new
        @inventory_brand_pack_service = INVENTORY_BRAND_PACK_SERVICE.new
        @seller_brand_pack_service = SELLER_BRAND_PACK_SERVICE.new
        @warehouse_brand_pack_service = WAREHOUSE_BRAND_PACK_SERVICE.new
        @inventory_service = INVENTORY_SERVICE.new
        @warehouse_service = WAREHOUSE_SERVICE.new
        @seller_service = SELLER_SERVICE.new
        @brand_pack_service = BRAND_PACK_SERVICE.new
      end

       # 
      # Transactional creation of MPBP
      #
      def transactional_create_marketplace_brand_pack(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return create_marketplace_brand_pack(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # Transactional updation of MPBP
      #
      def transactional_update_marketplace_brand_pack(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return update_marketplace_brand_pack(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      def create_marketplace_brand_pack(args)
        brand_pack = validate_if_exists(args[:brand_pack_id], @brand_pack_dao, 'BrandPack')
        sbp = @seller_brand_pack_dao.get_seller_brand_pack_by_id(args[:seller_brand_pack_id])
        if sbp.brand_pack != brand_pack
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_NOT_FOUND_IN_SELLER)
        end
        args = { mpbp: args, sbp: sbp, pricing: args[:pricing]}
        create_model(args, @marketplace_brand_pack_dao)
      end

      def update_marketplace_brand_pack(args)
        if args[:brand_pack_id].present? && args[:seller_brand_pack_id].present?
          brand_pack = validate_if_exists(args[:brand_pack_id], @brand_pack_dao, 'BrandPack')
          sbp = @seller_brand_pack_dao.get_seller_brand_pack_by_id(args[:seller_brand_pack_id])
          if sbp.brand_pack != brand_pack
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_NOT_FOUND_IN_SELLER)
          end
        end
        args = { mpbp: args, sbp: sbp, id: args[:id] , pricing: args[:pricing]}
        update_model(args, @marketplace_brand_pack_dao, 'MarketplaceBrandPack')
      end

      def get_marketplace_brand_pack_by_id(id)
        validate_if_exists(id, @marketplace_brand_pack_dao, 'MarketplaceBrandPack')
      end

      def get_all_marketplace_brand_pack(pagination_params = {})
        get_all_elements(@marketplace_brand_pack_dao, pagination_params)
      end

      def change_marketplace_brand_pack_state(args)
        if args[:event].to_i == COMMON_EVENTS::ACTIVATE
          mpbp = get_marketplace_brand_pack_by_id(args[:id])
          @brand_pack_service.brand_pack_active?(mpbp.brand_pack)
        end
        change_state(args, @marketplace_brand_pack_dao, 'MarketplaceBrandPack')
      end

      #
      # Function to check if mpbp is active & it's atomic brand pack as well
      #
      def mpbp_active?(mpbp)
        if mpbp.active?
          @brand_pack_service.brand_pack_active?(mpbp.brand_pack)
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INACTIVE_MPBP)
        end
      end

      # 
      # Transactional creation of MPBP
      #
      def transactional_create_mpbp_and_brand_pack(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return create_mpbp_and_brand_pack(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      # 
      # This function creates brand pack and corresponding mpbb
      #
      # @param args [Hash] it contain brand_pack model arguments
      # 
      # @return [BrandPackModel] it returns brand pack
      #
      def create_mpbp_and_brand_pack(args)
        brand_pack = @brand_pack_service.create_brand_pack(args)
        create_mpbp_from_brand_pack(brand_pack)
        return brand_pack
      end

      #
      # This function create mpbp from brand pack, it first create ibp then wbp then sbp
      # and finally mpbp
      #
      # @param brand_pack [BrandPackModel] Brand Pack object
      #
      def create_mpbp_from_brand_pack(brand_pack)

        inventory_brand_pack = create_inventory_brand_pack(brand_pack)
        warehouse_brand_pack = create_warehouse_brand_pack(brand_pack, inventory_brand_pack)
        seller_brand_pack = create_seller_brand_pack(brand_pack, warehouse_brand_pack)
        marketplace_brand_pack_args = 
          {
            brand_pack_id: brand_pack.id,
            seller_brand_pack_id: seller_brand_pack.id,
            status: COMMON_MODEL_STATES::ACTIVE
          }
        create_marketplace_brand_pack(marketplace_brand_pack_args)
      end

      # 
      # THIS IS VALID ONLY WHEN THERE IS ONLY ONE SELLER
      # This function get the first seller and creates seller_brand_pack
      # corresponding to the wbp and seller
      #
      # @param brand_pack [BrandPackModel] brand pack object
      # @param warehouse_brand_pack [WarehouseBrandPack] wbp object 
      # 
      # @return [SellerBrandPackModel] sbp model
      #
      def create_seller_brand_pack(brand_pack, warehouse_brand_pack)

        sellers = @seller_service.get_all_sellers
        seller = sellers[:seller].first

        seller_brand_pack_args = 
        {
          seller_id: seller.id,
          warehouse_brand_pack_id: warehouse_brand_pack.id,
          brand_pack_id: brand_pack.id, 
          status: COMMON_MODEL_STATES::ACTIVE
        }
        @seller_brand_pack_service.create_seller_brand_pack(seller_brand_pack_args)
      end

      # 
      # THIS IS VALID ONLY WHEN THERE IS ONLY ONE WAREHOUSE
      # This function get the first warehouse and creates warehouse_brand_pack
      # corresponding to the ibp and warehouse
      #
      # @param brand_pack [BrandPackModel] brand pack object
      # @param inventory_brand_pack [InventoryBrandPack] ibp object 
      # 
      # @return [WarehouseBrandPackModel] wbp model
      #
      def create_warehouse_brand_pack(brand_pack, inventory_brand_pack)

        warehouses = @warehouse_service.get_all_warehouses
        warehouse = warehouses[:warehouse].first

        warehouse_brand_pack_args = 
          {
            warehouse_id: warehouse.id,
            brand_pack_id: brand_pack.id,
            inventory_brand_pack_id: inventory_brand_pack.id,
            status: COMMON_MODEL_STATES::ACTIVE
          }
        @warehouse_brand_pack_service.create(warehouse_brand_pack_args)
      end


      # 
      # THIS IS VALID ONLY WHEN THERE IS ONLY ONE INVENTORY
      # This function get the first inventory and creates inventory_brand_pack
      # corresponding to the brand_pack and inventory
      #
      # @param brand_pack [BrandPackModel] brand pack object
      # 
      # @return [InventoryBrandPackModel] ibp model
      #
      def create_inventory_brand_pack(brand_pack)

        inventories = @inventory_service.get_all_inventories
        # Default inventory created in populate data rake task
        inventory = inventories[:elements].first
        inventory_brand_pack_args = 
          {
            brand_pack_id: brand_pack.id,
            inventory_id: inventory.id,
            status: COMMON_MODEL_STATES::ACTIVE
          }

        @inventory_brand_pack_service.create_inventory_brand_pack(inventory_brand_pack_args)
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class MarketplaceSellingPackService < SupplyChainBaseModule::V1::BaseService

      MARKETPLACE_SELLING_PACK_DAO = MarketplaceProductModule::V1::MarketplaceSellingPackDao
      MARKETPLACE_BRAND_PACK_DAO = MarketplaceProductModule::V1::MarketplaceBrandPackDao
      BRAND_PACK_DAO = MasterProductModule::V1::BrandPackDao
      MPSP_PRICING_SERVICE = MarketplacePricingModule::V1::MarketplaceSellingPackPricingService
      MARKETPLACE_SELLING_PACK_INDEX_SERVICE = SearchModule::V1::MarketplaceSellingPackIndexService
      MARKETPLACE_BRAND_PACK_SERVICE = MarketplaceProductModule::V1::MarketplaceBrandPackService
      CATEGORY_DAO = CategorizationModule::V1::CategoryDao
      MPSP_CATEGORY_DAO = CategorizationModule::V1::MpspCategoryDao
      MPSP_CATEGORY_MODEL = CategorizationModule::V1::MpspCategory
      IMAGE_SERVICE = ImageServiceModule::V1::ImageService
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize
        super
        @marketplace_selling_pack_dao = MARKETPLACE_SELLING_PACK_DAO.new
        @marketplace_brand_pack_dao = MARKETPLACE_BRAND_PACK_DAO.new
        @mpsp_index_service = MARKETPLACE_SELLING_PACK_INDEX_SERVICE.new
        @mpsp_pricing_service = MPSP_PRICING_SERVICE.new
        @brand_pack_dao = BRAND_PACK_DAO.new
        @category_dao = CATEGORY_DAO.new
        @marketplace_brand_pack_service = MARKETPLACE_BRAND_PACK_SERVICE.new
        @mpsp_category_dao = MPSP_CATEGORY_DAO.new
        @image_service = IMAGE_SERVICE.new
      end

      def transactional_create_marketplace_selling_pack(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return create_marketplace_selling_pack(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      def create_marketplace_selling_pack(args)

        mpsp_sub_category = MPSP_CATEGORY_MODEL.find_by_mpsp_category_id(args[:mpsp_sub_category_id])

        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::SUB_CATEGORY_NOT_PRESENT) if mpsp_sub_category.nil? || mpsp_sub_category.mpsp_parent_category_id.nil?
        
        pricing = args[:pricing]
        marketplace_brand_pack_quantity_array = []
        args[:marketplace_brand_packs].each do |marketplace_brand_pack|
          mpbp = validate_if_exists(
            marketplace_brand_pack[:marketplace_brand_pack_id], @marketplace_brand_pack_dao, 'MarketplaceBrandPack')
          mpbp_quantity = { marketplace_brand_pack: mpbp, quantity: marketplace_brand_pack[:quantity] }
          marketplace_brand_pack_quantity_array.push(mpbp_quantity)
        end
        args = { mpsp: args, marketplace_brand_packs: marketplace_brand_pack_quantity_array }
        marketplace_selling_pack = @marketplace_selling_pack_dao.new_marketplace_selling_pack(args)
        marketplace_selling_pack.marketplace_selling_pack_pricing = @mpsp_pricing_service.new_marketplace_selling_pack_pricing(
          pricing,
          marketplace_brand_pack_quantity_array,
          args[:mpsp][:max_quantity],
          args[:mpsp][:is_ladder_pricing_active]
        )
        marketplace_selling_pack = @marketplace_selling_pack_dao.save_marketplace_selling_pack(marketplace_selling_pack)
        @mpsp_index_service.add_document(marketplace_selling_pack)

        return marketplace_selling_pack
      end

      def transactional_update_marketplace_selling_pack(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return update_marketplace_selling_pack(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      def update_marketplace_selling_pack(args)
  
        if args[:mpsp_sub_category_id].present?
          mpsp_sub_category = MPSP_CATEGORY_MODEL.find_by_mpsp_category_id(args[:mpsp_sub_category_id])
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::SUB_CATEGORY_NOT_PRESENT) if mpsp_sub_category.nil? || mpsp_sub_category.mpsp_parent_category_id.nil?
        end
        marketplace_selling_pack = get_marketplace_selling_pack_by_id(args[:id])
        if marketplace_selling_pack.active? && args[:dirty_bit]
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::DIRTY_BIT_SET)
        end
        marketplace_selling_pack = update_model(args, @marketplace_selling_pack_dao, 'MarketplaceSellingPack')
        if args[:pricing].present?
          marketplace_selling_pack.marketplace_selling_pack_pricing = @mpsp_pricing_service.update_marketplace_selling_pack_pricing(
            args[:pricing],
            marketplace_selling_pack.marketplace_selling_pack_pricing,
            marketplace_selling_pack.max_quantity,
            marketplace_selling_pack.is_ladder_pricing_active
          )
        end
        @mpsp_index_service.update_document(marketplace_selling_pack)

        return marketplace_selling_pack
      end

      def get_marketplace_selling_pack_by_id(id)
        marketplace_selling_pack = validate_if_exists(id, @marketplace_selling_pack_dao, 'MarketplaceSellingPack')
        return marketplace_selling_pack
      end

      def get_all_marketplace_selling_pack(pagination_params = {})
        get_all_elements(@marketplace_selling_pack_dao, pagination_params)
      end

      def change_marketplace_selling_pack_state(args)
        ActiveRecord::Base.transaction do
          if args[:event].to_i == COMMON_EVENTS::ACTIVATE
            mpsp = get_marketplace_selling_pack_by_id(args[:id])
            mpsp.marketplace_brand_packs.each do |mpbp|
              @marketplace_brand_pack_service.mpbp_active?(mpbp)
            end
            change_dirty_bit({ id: args[:id], dirty_bit: false })
          end
          marketplace_selling_pack = change_state(args, @marketplace_selling_pack_dao, 'MarketplaceSellingPack')
          @mpsp_index_service.update_marketplace_selling_pack_status(marketplace_selling_pack.id, marketplace_selling_pack.status)
          return marketplace_selling_pack
        end
      end

      # 
      # This function return all the mpsps after applying filters
      #
      # @param args [Hash] it may contain category_id, sub_category_id and department_id
      # 
      # @return [JoinedTable] mpsps joined table
      #
      def get_mpsp_by_mpsp_department_and_mpsp_category(args)
        
        mpsp_sub_categories = get_mpsp_sub_categories(args)
        
        if mpsp_sub_categories.present?
          mpsps = @marketplace_selling_pack_dao.get_mpsps_by_mpsp_sub_categories(mpsp_sub_categories)
        else
          mpsps = @marketplace_selling_pack_dao.get_all
          mpsps = mpsps[:elements]
        end

        return MARKETPLACE_SELLING_PACK_DAO.get_aggregated_not_deleted_mpsps(mpsps)
      end

      # 
      # This function determines the sub_categories
      #
      # @param args [Hash] it may contain category_id, sub_category_id and department_id
      # 
      # @return [Model] sub categories
      #
      def get_mpsp_sub_categories(args)
        
        return args[:mpsp_sub_category_id] if args[:mpsp_sub_category_id].present?

        return @mpsp_category_dao.get_mpsp_sub_categories_by_mpsp_categories(args[:mpsp_category_id]) if args[:mpsp_category_id].present?

        if args[:mpsp_department_id].present?
          mpsp_categories = @mpsp_category_dao.get_mpsp_categories_by_mpsp_department(args[:mpsp_department_id])
          return @mpsp_category_dao.get_mpsp_sub_categories_by_mpsp_categories(mpsp_categories)
        end

        return nil
      end

      def change_dirty_bit(args)
        marketplace_selling_pack = update_model(args, @marketplace_selling_pack_dao, 'MarketplaceSellingPack')
      end
    end
  end
end

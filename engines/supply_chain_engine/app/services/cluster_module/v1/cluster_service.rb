#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class ClusterService < SupplyChainBaseModule::V1::BaseService

      CLUSTER_DAO = ClusterModule::V1::ClusterDao
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper
      CLUSTER_TYPES_UTIL = ClusterModule::V1::ClusterTypes
      MARKETPLACE_SELLING_PACK_MODEL = MarketplaceProductModule::V1::MarketplaceSellingPack
      CLUSTER_LEVELS_UTIL = ClusterModule::V1::ClusterLevels
      
      def initialize(params)
        @params = params
        @cluster_dao = CLUSTER_DAO.new
        super
      end

      # 
      # create cluster
      #
      # @param args [Hash] model args
      # 
      # @return [Model] Cluster
      #
      def create_cluster(args)

        validate_level(args[:context][:level]) if args[:context].present? && args[:context][:level].present?
        validate_type(args[:cluster_type]) if args[:cluster_type].present?

        if args[:images].present?
          image_id_array = IMAGE_SERVICE_HELPER.get_sorted_image_id_array_by_priority(args[:images])
          validate_images(image_id_array)
          args[:images] = image_id_array
        end

        validate_mpsps(args[:mpsp_ids]) if args[:mpsp_ids].present?
        create_model(args, @cluster_dao)
      end

      # 
      # validate if cluster exist and then update cluster
      #
      # @param args [Hash] model args
      # 
      # @return [Model] Cluster
      #
      def update_cluster(args)
        validate_level(args[:context][:level]) if args[:context].present? && args[:context][:level].present?
        validate_type(args[:cluster_type]) if args[:cluster_type].present?

        if args[:images].present?
          image_id_array = IMAGE_SERVICE_HELPER.get_sorted_image_id_array_by_priority(args[:images])
          validate_images(image_id_array)
          args[:images] = image_id_array
        end

        validate_mpsps(args[:mpsp_ids]) if args[:mpsp_ids].present?

        update_model(args, @cluster_dao, 'Cluster')
      end

      # 
      # change cluster state
      #
      # @param args [Hash] contain event and cluster id
      # 
      # @return [Model] Cluster
      #
      def change_cluster_state(args)
        change_state(args, @cluster_dao, 'Cluster')
      end

      # 
      # Return cluster by input id or nil if not present
      #
      # @param id [Integer] cluster id
      # 
      # @return [Model] cluster
      #
      def get_cluster_by_id(id)
        validate_if_exists(id, @cluster_dao, 'Cluster')
      end

      # 
      # Return array of cluster satisfying pagination conditions
      # according to params it call specific queries and paginate
      # it at same time
      #
      # @param pagination_params [Hash] conditions
      # 
      # @return [Array] array of clusters
      #
      def get_filtered_clusters(pagination_params)
        if pagination_params[:level_id].present? && pagination_params[:level].present?
          @cluster_dao.get_clusters_by_level_and_level_id(pagination_params)
        elsif pagination_params[:level_id].present?
          @cluster_dao.get_clusters_by_level_id(pagination_params)
        elsif pagination_params[:level].present?
          @cluster_dao.get_clusters_by_level(pagination_params)
        else
          get_all_elements(@cluster_dao, pagination_params)
        end
      end

      # validate if all the image_id present in image_id_array is present or not
      #
      # @param image_id_array [Array] array containing all the image_ids
      #
      # @error [InvalidArgumentsError] if image_id not present in image table
      #
      def validate_images(image_id_array)
        if IMAGE_SERVICE_HELPER.validate_images(image_id_array) == false
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::IMAGE_LINKING_FAILED)
        end
      end

      # validate if all the mpsps present in mpsp_id_array is present or not
      #
      # @param mpsp_ids [Array] array containing all the mpsp ids
      #
      # @error [InvalidArgumentsError] if mpsp_id not present in mpsp table
      #
      def validate_mpsps(mpsp_ids)
        mpsp_ids= mpsp_ids.map(&:to_i)

        valid_mpsp_ids = MARKETPLACE_SELLING_PACK_MODEL.ids

        remaining_mpsp_ids = mpsp_ids - valid_mpsp_ids

        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Marketplace Selling Pack with ids ' + remaining_mpsp_ids.to_s + ' missing') if remaining_mpsp_ids.present?
      end

      # 
      # This function validate if type of cluster is valid or not
      #
      # @param type [Integer] type of cluster
      # 
      # @error [InvalidArgumentsError] if type of cluster is not supported
      #
      def validate_type(type)
        cluster_type = CLUSTER_TYPES_UTIL.get_cluster_type_by_type(type)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.
              new('Supported Types are ' + CLUSTER_TYPES_UTIL::CLUSTER_TYPES.to_s) if cluster_type.nil?
      end

      # 
      # This function validate if level of cluster is valid or not
      #
      # @param type [Integer] level of cluster
      # 
      # @error [InvalidArgumentsError] if level of cluster is not supported
      #
      def validate_level(level)
        cluster_level = CLUSTER_LEVELS_UTIL.get_cluster_level_by_level(level)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.
              new('Supported Levels are ' + CLUSTER_LEVELS_UTIL::CLUSTER_LEVELS.to_s) if cluster_level.nil?
      end
    end
  end
end

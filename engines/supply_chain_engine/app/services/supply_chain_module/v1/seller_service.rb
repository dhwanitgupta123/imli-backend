#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class SellerService < SupplyChainBaseModule::V1::BaseService
      SELLER_DAO = SupplyChainModule::V1::SellerDao
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      def initialize(params = {})
        @params = params
        super
      end

      #
      # create seller
      #
      # @param args [Hash] contain params for creating seller
      #
      # @return [SellerObject]
      #
      # @error [InvalidArgumentsError] if it fails to create seller
      #
      def create_seller(args)
        seller_dao_class = SELLER_DAO.new

        seller = seller_dao_class.get_seller_by_name(args[:name])

        if seller.nil?
          begin
            seller = seller_dao_class.create(args)
          rescue CUSTOM_ERRORS_UTIL::InvalidArgumentsError => e
            raise e
          end
        end
        return seller
      end

      #
      # Return all the sellers
      #
      # @return [List] list of sellers
      #
      def get_all_sellers(paginate_params = {})
        seller_dao_class = SELLER_DAO.new
        seller = seller_dao_class.paginated_seller(paginate_params)
        return seller
      end


      #
      # Return the requested seller details
      #
      # @return [List] object of seller
      #
      def get_seller(seller_id)
        seller_dao_class = SELLER_DAO.new
        seller = seller_dao_class.get_seller_by_id(seller_id)
        return seller
      end

      # 
      # function to call dao changes state function to change the status of seller
      # 
      # @param request [request] [hash] contains event type to change state
      # 
      # @return [object] [Seller]
      def change_state(request)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new('Seller should be present') if request[:id].blank?
        seller_dao_class = SELLER_DAO.new
        seller = seller_dao_class.change_state(request)
        return seller
      end

      #
      # update seller
      #
      # @param args [Hash] contain params for updating seller
      #
      # @return [seller object]
      #
      # @error [InvalidArgumentsError] if it fails to update seller
      #
      def update(args)
        seller_dao_class = SELLER_DAO.new

        seller = seller_dao_class.get_seller_by_id(args[:id])

        seller_dao_class.update(args, seller)
        return seller
      end

    end
  end
end
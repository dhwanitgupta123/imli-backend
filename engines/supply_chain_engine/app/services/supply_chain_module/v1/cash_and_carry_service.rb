#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class CashAndCarryService < SupplyChainBaseModule::V1::BaseService
      CASH_AND_CARRY_DAO = SupplyChainModule::V1::CashAndCarryDao
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      def initialize
        super
      end

      #
      # create cash and carry
      #
      # @param args [Hash] contain params for creating cash & carry
      #
      # @return [c&c object]
      #
      # @error [InvalidArgumentsError] if it fails to create c & c
      #
      def create_cash_and_carry(args)
        cash_and_carry_dao_class = CASH_AND_CARRY_DAO.new
        begin
          c_and_c = cash_and_carry_dao_class.create(args)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          raise e
        end
        return c_and_c
      end

      #
      # Return all the c & c
      #
      # @return [List] list of c&c
      #
      def get_all_cash_and_carry(paginate_params)
        cash_and_carry_dao_class = CASH_AND_CARRY_DAO.new
        cash_and_carry = cash_and_carry_dao_class.paginated_cash_and_carry(paginate_params)
        return cash_and_carry
      end

      # 
      # function to call dao changes state function to change the status of c&c
      # 
      # @param request [request] [hash] contains event type to change state
      # 
      # @return [object] [C&C]
      def change_state(request)
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::CASH_AND_CARRY_MISSING) if request[:id].blank?
        cash_and_carry_dao_class = CASH_AND_CARRY_DAO.new
        c_and_c = cash_and_carry_dao_class.change_state(request)
        return c_and_c
      end

      #
      # update cash and carry
      #
      # @param args [Hash] contain params for updating c&c
      #
      # @return [c&c object]
      #
      # @error [InvalidArgumentsError] if it fails to update c&c
      #
      def update(args)
        cash_and_carry_dao_class = CASH_AND_CARRY_DAO.new

        c_and_c = cash_and_carry_dao_class.get_cash_and_carry_by_id(args[:id])

        cash_and_carry_dao_class.update(args, c_and_c)
        return c_and_c
      end

      #
      # Return the requested c&c details
      #
      # @return [List] object of c&c
      #
      def get_cash_and_carry(c_and_c_id)
        cash_and_carry_dao_class = CASH_AND_CARRY_DAO.new
        c_and_c = cash_and_carry_dao_class.get_cash_and_carry_by_id(c_and_c_id)
        return c_and_c
      end
    end
  end
end

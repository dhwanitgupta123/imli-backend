module SupplyChainModule::V1
  module WarehouseModule::V1
    class WarehouseAddressService < SupplyChainBaseModule::V1::BaseService
      WAREHOUSE_ADDRESS_DAO = SupplyChainModule::V1::WarehouseModule::V1::WarehouseAddressDao
      AREA_SERVICE = AddressModule::V1::AreaService
      def initialize(params={})
        super
      end

      #
      # Function to add address for a warehouse
      #
      def add_address(args)
        warehouse_address_dao = WAREHOUSE_ADDRESS_DAO.new
        area_service = AREA_SERVICE.new
        area = area_service.get_area_by_id(args[:addresses].first[:area_id])
        address = warehouse_address_dao.add_address(args)
        return address
      end

      # 
      # Function to update address of a warehouse
      #
      def update_address(args)
        warehouse_address_dao = WAREHOUSE_ADDRESS_DAO.new
        area_service = AREA_SERVICE.new
        area = area_service.get_area_by_id(args[:addresses].first[:area_id]) if args[:addresses].first[:area_id].present?
        present_address = warehouse_address_dao.get_address_from_id(args[:address_id])
        validate_address_owner(present_address, args[:id].to_i)
        address = warehouse_address_dao.update_address(args, present_address)
        return address    
      end

      #
      # Function to validate if address belongs to the proper warehouse
      #
      def validate_address_owner(address, warehouse_id)
        unless address.warehouse_id == warehouse_id.to_i
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WRONG_WAREHOUSE_ADDRESS_REFERENCE)
        end
      end

      #
      # Function to get address of a warehouse using ID
      #
      def get_warehouse_address_by_id(id)
        warehouse_address_dao = WAREHOUSE_ADDRESS_DAO.new
        return warehouse_address_dao.get_address_from_id(id)
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class InventoryService < SupplyChainBaseModule::V1::BaseService

      INVENTORY_DAO = SupplyChainModule::V1::InventoryDao
      DISTRIBUTOR_DAO = SupplyChainModule::V1::DistributorDao
      BRAND_PACK_DAO = MasterProductModule::V1::BrandPackDao
      IBP_SERVICE = InventoryProductModule::V1::InventoryBrandPackService
      IBP_DAO = InventoryProductModule::V1::InventoryBrandPackDao
      INVENTORY_ADDRESS_SERVICE = SupplyChainModule::V1::InventoryModule::V1::InventoryAddressService
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates
      CST_TAX = BigDecimal('2')
      OCTRAI_TAX = BigDecimal('5')

      def initialize(params={})
        super
        @distributor_dao = DISTRIBUTOR_DAO.new
        @inventory_dao = INVENTORY_DAO.new
      end

      def create_inventory(args)
        validate_if_exists(args[:distributor_id], @distributor_dao, 'Distributor')
        create_model(args, @inventory_dao)
      end 

      def update_inventory(args)
        validate_if_exists(args[:distributor_id], @distributor_dao, 'Distributor') if args[:distributor_id].present?
        update_model(args, @inventory_dao, 'Inventory')
      end

      def get_inventory_by_id(id)
        validate_if_exists(id, @inventory_dao, 'Inventory')
      end

      def get_all_inventories(pagination_params = {})
        get_all_elements(@inventory_dao, pagination_params)
      end

      def change_inventory_state(args)
        change_state(args, @inventory_dao, 'Inventory')
      end

      def add_inventory_address(request)
        inventory_dao = INVENTORY_DAO.new
        inventory = get_inventory_by_id(request[:id])
        inventory_address_service = INVENTORY_ADDRESS_SERVICE.new
        inventory.inventory_address = inventory_address_service.add_address(request)
        return inventory
      end

      def update_inventory_address(request)
        inventory_dao = INVENTORY_DAO.new
        inventory = get_inventory_by_id(request[:id])
        inventory_address_service = INVENTORY_ADDRESS_SERVICE.new
        inventory_address_service.update_address(request)
        return inventory
      end

      def get_all_inventory_address(inventory_id)
        inventory_dao = INVENTORY_DAO.new
        inventory = get_inventory_by_id(inventory_id)
        return inventory
      end

      #
      # Function to update/create IBP for a inventory from a CSV
      #
      def upload_products(request)
        ibp_dao = IBP_DAO.new
        count = 0
        CSV.foreach(request[:file_path]) do |row|
          if count == 0
            map_data_fields(row)
            count = 1
            next
          end
          next if row[@field_map[:brand_pack_code]].blank?
          inventory = get_inventory_by_vendor_code(row[@field_map[:vendor_code]])
          brand_pack = get_brand_pack_by_code(row[@field_map[:brand_pack_code]])
          row = verify_row(row)
          ibp_args = {
            inventory_id: inventory.id,
            brand_pack_id: brand_pack.id,
            status: COMMON_MODEL_STATES::ACTIVE,
            pricing: {
              net_landing_price: BigDecimal(row[@field_map[:buying_price]]),
              margin: BigDecimal(row[@field_map[:margin]]),
              vat: BigDecimal(row[@field_map[:vat]]),
              cst: CST_TAX,
              octrai: OCTRAI_TAX
            }
          }
          ibp_service = IBP_SERVICE.new
          inventory_brand_pack = ibp_dao.ibp_of_brand_pack_for_a_vendor(inventory, brand_pack)
          if inventory_brand_pack.blank?
            ibp = ibp_service.transactional_create_inventory_brand_pack(ibp_args)
          else
            ibp_dao.update(ibp_args, inventory_brand_pack)
          end
        end
      end

      #
      # Function to verify the arguments of the row
      # 
      def verify_row(row)
        row[@field_map[:buying_price]] = '0.0' unless row[@field_map[:buying_price]].numeric?
        if row[@field_map[:vat]].present?
          row[@field_map[:vat]] = row[@field_map[:vat]].remove('%')
          row[@field_map[:vat]] = '0.0' unless row[@field_map[:vat]].numeric?
        else
          row[@field_map[:vat]] = '0.0'
        end
        if row[@field_map[:margin]].present?
          row[@field_map[:margin]] = row[@field_map[:margin]].remove('%')
          row[@field_map[:margin]] = '0.0' unless row[@field_map[:margin]].numeric?
        else
          row[@field_map[:margin]] = '0.0'
        end
        return row
      end
      #
      # Function to fetch inventory using it's vendor code
      #
      def get_inventory_by_vendor_code(vendor_code)
        if vendor_code.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MISSING%{field: 'Vendor code'})
        end
        distributor_dao = DISTRIBUTOR_DAO.new
        distributor = distributor_dao.get_distributor_by_vendor_code(vendor_code)
        return distributor.inventory
      end

      #
      # Function to fetch Brand pack using it's code
      #
      def get_brand_pack_by_code(brand_pack_code)
        if brand_pack_code.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MISSING%{field: 'Brand pack code'})
        end
        brand_pack_dao = BRAND_PACK_DAO.new
        brand_pack = brand_pack_dao.get_brand_pack_by_code(brand_pack_code)
        return brand_pack
      end

      #
      # This function maps data fields from the CSV
      #
      def map_data_fields(row)
        index = 0
        @field_map = {}
        row.each do |col|
          case col.downcase
          when 'bp_code'
            @field_map[:brand_pack_code] = index
          when 'company'
            @field_map[:company] = index
          when 'vendor_id'
            @field_map[:vendor_code] = index
          when 'vat'
            @field_map[:vat] = index
          when 'margin'
            @field_map[:margin] = index
          when 'price_per_unit_excluding_tax'
            @field_map[:buying_price] = index
          end
          index += 1
        end
      end
    end
  end
end

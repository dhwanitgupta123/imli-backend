#
# Module to handle all the functionalities related to Packaging
#
module PackagingModule
  #
  # Version1 for Packaging module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class PackagingBoxService < SupplyChainBaseModule::V1::BaseService

      PACKAGING_BOX_DAO = PackagingModule::V1::PackagingBoxDao

      def initialize(params='')
        super
        @params = params
        @packaging_box_dao = PACKAGING_BOX_DAO.new(@params)
      end

      def create_packaging_box(args)
        create_model(args, @packaging_box_dao)
      end

      def update_packaging_box(args)
        update_model(args, @packaging_box_dao, 'PackagingBox')
      end

      def get_packaging_box_by_id(id)
        validate_if_exists(id, @packaging_box_dao, 'PackagingBox')
      end

      def get_all_packaging_box(pagination_params = {})
        get_all_elements(@packaging_box_dao, pagination_params)
      end

      def change_packaging_box_state(args)
        change_state(args, @packaging_box_dao, 'PackagingBox')
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class ProductService < SupplyChainBaseModule::V1::BaseService
      PRODUCT_DAO = MasterProductModule::V1::ProductDao
      SUB_BRAND_DAO = MasterProductModule::V1::SubBrandDao
      VARIANT_DAO = MasterProductModule::V1::VariantDao
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      PRODUCT_HELPER = MasterProductModule::V1::ProductHelper
      def initialize
        super
      end

      #
      # create product
      #
      # @param args [Hash] contain params for creating product
      #
      # @return [productObject]
      #
      # @error [InvalidArgumentsError] if it fails to create product
      #
      def create_product(args)
        product_dao_class = PRODUCT_DAO.new
        
        sub_brand = valid_sub_brand(args)
        variant = valid_variant(args)

        product_code = PRODUCT_HELPER.create_product_code({
          sub_brand: sub_brand,
          variant: variant,
          initials: args[:initials]
        })
        begin
          product = product_dao_class.create(args.merge({ product_code: product_code }))
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          raise e
        end
        product
      end

      #
      # update product
      #
      # @param args [Hash] contain params for updating product
      #
      # @return [productObject]
      #
      # @error [InvalidArgumentsError] if it fails to update product
      #
      def update_product(args, product_id)
        product_dao_class = PRODUCT_DAO.new
        sub_brand_dao_class = SUB_BRAND_DAO.new

        sub_brand = valid_sub_brand(args)
        variant = valid_variant(args)
        
        product = get_product_by_id(product_id)
        product_code = PRODUCT_HELPER.update_product_code({
          sub_brand: sub_brand,
          variant: variant,
          product: product
        })
        product_dao_class.update(args.merge({ product_code: product_code }), product)
      end

      #
      # Return all the products
      #
      # @return [List] list of products
      #
      def get_all_products(pagination_params = {})
        product_dao_class = PRODUCT_DAO.new
        product_dao_class.get_paginated_filtered_products(pagination_params)
      end

      # 
      # function check if it's a valud sub brand
      #
      def valid_sub_brand(args)
        sub_brand_dao_class = SUB_BRAND_DAO.new
        if args[:sub_brand_id].present?
          sub_brand = sub_brand_dao_class.get_sub_brand_by_id(args[:sub_brand_id])
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::SUB_BRAND_NOT_FOUND) if sub_brand.nil?
        end
        return sub_brand
      end

      # 
      # funtion to check if it's a valid variant
      #
      def valid_variant(args)
        variant_dao_class = VARIANT_DAO.new
        if args[:variant_id].present?
          variant = variant_dao_class.get_variant_by_id(args[:variant_id])
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::VARIANT_NOT_FOUND) if variant.nil?
        end
        return variant
      end

      #
      # change the state of product
      #
      # @param id [Integer] product_id of which status is to be changed
      # @param event [String] event to trigger to change the status 
      #
      def change_product_state(id, event)
        product_dao_class = PRODUCT_DAO.new
        return product_dao_class.change_product_state(id, event)
      end

      #
      # return product corresponding to the id
      #
      # @param id [Integer] product id
      #
      # @return  product [Model] compnay object
      #
      # @error [InvalidArgumentsError] if product doesn't exists
      #
      def get_product_by_id(id)
        product_dao_class = PRODUCT_DAO.new

        product = product_dao_class.get_product_by_id(id)

        if product.nil?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PRODUCT_NOT_FOUND)
        end

        return product
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class VariantService < SupplyChainBaseModule::V1::BaseService
      VARIANT_DAO = MasterProductModule::V1::VariantDao
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      def initialize
        super
      end

      #
      # create variant
      #
      # @param args [Hash] contain params for creating variant
      #
      # @return [variant Object]
      #
      # @error [InvalidArgumentsError] if it fails to create variant
      #
      def create_variant(args)
        variant_dao_class = VARIANT_DAO.new
        begin
          variant = variant_dao_class.create(args)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          raise e
        end
        variant
      end

      #
      # update variant
      #
      # @param args [Hash] contain params for updating variant
      #
      # @return [variant Object]
      #
      # @error [InvalidArgumentsError] if it fails to update variant
      #
      def update_variant(args)
        variant_dao_class = VARIANT_DAO.new
        variant = variant_dao_class.get_variant_by_id(args[:id])

        if variant.nil?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::VARIANT_NOT_FOUND)
        end
        variant_dao_class.update(args, variant)
        return variant
      end

      #
      # Return all the products
      #
      # @return [List] list of products
      #
      def get_all_variants
        variant_dao_class = VARIANT_DAO.new
        variant_dao_class.get_all_variants
      end
    end
  end
end

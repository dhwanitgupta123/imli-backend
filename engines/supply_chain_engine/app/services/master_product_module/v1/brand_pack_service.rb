#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Logic layer for brand pack specific queries
    #
    class BrandPackService < SupplyChainBaseModule::V1::BaseService

      BRAND_PACK_DAO = MasterProductModule::V1::BrandPackDao
      PRODUCT_DAO = MasterProductModule::V1::ProductDao
      CATEGORY_MODEL = CategorizationModule::V1::Category
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      BRAND_PACK_HELPER = MasterProductModule::V1::BrandPackHelper
      IMAGE_SERVICE = ImageServiceModule::V1::ImageService
      WAREHOUSE_MODEL = SupplyChainModule::V1::Warehouse
      IBP_SERVICE = InventoryProductModule::V1::InventoryBrandPackService
      WBP_DAO = WarehouseProductModule::V1::WarehouseBrandPackDao
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      def initialize
        super
        @image_service = IMAGE_SERVICE.new
      end

      #
      # create brand_pack
      #
      # @param args [Hash] contain params for creating brand pack
      #
      # @return [BrandPackObject]
      #
      # @error [InvalidArgumentsError] if it fails to create brand_pack
      #
      def create_brand_pack(args)

        brand_pack_dao_class = BRAND_PACK_DAO.new
        product_dao_class = PRODUCT_DAO.new

        product = product_dao_class.get_product_by_id(args[:product_id])

        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PRODUCT_NOT_FOUND) if product.nil?

        # Not using this code as Brand pack code is auto generated now
        # if args[:brand_pack_code].blank?
        #   args[:brand_pack_code] = BRAND_PACK_HELPER.create_brand_pack_code({
        #     product: product,
        #     sku_size: args[:sku_size],
        #     unit: args[:unit]
        #   })
        # end

        sub_category = CATEGORY_MODEL.find_by_category_id(args[:sub_category_id])

        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('SubCategory does not exists') if sub_category.nil? || sub_category.parent_category_id.nil?

        begin
          brand_pack = brand_pack_dao_class.create(args)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          raise e
        end
        brand_pack
      end


      # 
      # Transactional creation of BP, IBP, WBP, SBP, MPBP
      #
      def transactional_create_brand_pack_and_mpbp(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return create_brand_pack_and_mpbp(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # create brand_pack
      #
      # @param args [Hash] contain params for creating brand pack
      #
      # @return [BrandPackObject]
      #
      # @error [InvalidArgumentsError] if it fails to create brand_pack
      #
      def create_brand_pack_and_mpbp(args)
        brand_pack_dao_class = BRAND_PACK_DAO.new
        product_dao_class = PRODUCT_DAO.new
        ibp_service = IBP_SERVICE.new
        wbp_dao = WBP_DAO.new
        warehouse = WAREHOUSE_MODEL.all.first
        product = product_dao_class.get_product_by_id(args[:product_id])
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PRODUCT_NOT_FOUND) if product.nil?
        sub_category = CATEGORY_MODEL.find_by_category_id(args[:sub_category_id])
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('SubCategory does not exists') if sub_category.nil? || sub_category.parent_category_id.nil?
        begin
          brand_pack = brand_pack_dao_class.create(args)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          raise e
        end
        if args[:inventory_brand_packs].present?
          args[:inventory_brand_packs].each do |inventory_brand_pack|
            ibp_service.create_ibp_to_mpbp(inventory_brand_pack.merge(brand_pack_id: brand_pack.id))
          end
        end
        if args[:warehouse_brand_packs].present?
          args[:warehouse_brand_packs].each do |warehouse_brand_pack|
            wbp = wbp_dao.get_wbp_by_parameters(brand_pack, warehouse)
            wbp_dao.update({
              wbp: warehouse_brand_pack
            }, wbp)
          end
        end
        brand_pack
      end

      #
      # update brand_pack
      #
      # @param args [Hash] contain params for updating brand pack
      #
      # @return [BrandPackObject]
      #
      # @error [InvalidArgumentsError] if it fails to update brand_pack
      #
      def update_brand_pack(args, brand_pack_id)
        brand_pack_dao_class = BRAND_PACK_DAO.new
        product_dao_class = PRODUCT_DAO.new

        if args[:product_id].present?
          product = product_dao_class.get_product_by_id(args[:product_id])
         raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PRODUCT_NOT_FOUND) if product.nil?
        end

        if args[:sub_category_id].present?
          sub_category = CATEGORY_MODEL.find_by_category_id(args[:sub_category_id])
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('SubCategory does not exists') if sub_category.nil? || sub_category.parent_category_id.nil?
        end

        brand_pack = get_brand_pack_by_id(brand_pack_id)

        if brand_pack.nil?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('brand_pack does not exists')
        end

        # Not using this code as Brand pack code is auto generated now
        # if brand_pack.brand_pack_code.blank? && args[:brand_pack_code].blank?
        #   args[:brand_pack_code] = BRAND_PACK_HELPER.update_brand_pack_code({
        #     product: product,
        #     sku_size: args[:sku_size],
        #     unit: args[:unit],
        #     brand_pack: brand_pack
        #   })
        # end

        brand_pack = brand_pack_dao_class.update(args, brand_pack)
        return brand_pack
      end

      # 
      # Transactional updation of BP, IBP
      #
      def transactional_update_brand_pack_and_mpbp(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return update_brand_pack_and_mpbp(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # update brand_pack, IBP
      #
      # @param args [Hash] contain params for updating brand pack, IBP
      #
      # @return [BrandPackObject]
      #
      # @error [InvalidArgumentsError] if it fails to update brand_pack
      #
      def update_brand_pack_and_mpbp(args)
        brand_pack_dao_class = BRAND_PACK_DAO.new
        product_dao_class = PRODUCT_DAO.new
        ibp_service = IBP_SERVICE.new
        wbp_dao = WBP_DAO.new
        warehouse = WAREHOUSE_MODEL.all.first
        if args[:product_id].present?
          product = product_dao_class.get_product_by_id(args[:product_id])
         raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PRODUCT_NOT_FOUND) if product.nil?
        end
        if args[:sub_category_id].present?
          sub_category = CATEGORY_MODEL.find_by_category_id(args[:sub_category_id])
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('SubCategory does not exists') if sub_category.nil? || sub_category.parent_category_id.nil?
        end
        brand_pack = get_brand_pack_by_id(args[:id])
        if brand_pack.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('brand_pack does not exists')
        end
        brand_pack = brand_pack_dao_class.update(args, brand_pack)
        if args[:inventory_brand_packs].present?
          args[:inventory_brand_packs].each do |inventory_brand_pack|
            if inventory_brand_pack[:id].present?
              ibp_service.update_inventory_brand_pack(inventory_brand_pack.merge(brand_pack_id: brand_pack.id))
            else
              ibp_service.create_ibp_to_mpbp(inventory_brand_pack.merge(brand_pack_id: brand_pack.id))
            end
          end
        end
        if args[:warehouse_brand_packs].present?
          args[:warehouse_brand_packs].each do |warehouse_brand_pack|
            wbp = wbp_dao.get_wbp_by_parameters(brand_pack, warehouse)
            wbp_dao.update({
              wbp: warehouse_brand_pack
            }, wbp) if wbp.present?
          end
        end
        return brand_pack
      end

      #
      # Return all the brand_packs
      #
      # @return [List] list of brand_packs
      #
      def get_all_brand_packs(filter_params = {})
        brand_pack_dao_class = BRAND_PACK_DAO.new
        brand_pack_dao_class.get_paginated_filtered_brand_packs(filter_params)
      end

      #
      # change the state of brand pack
      #
      # @param id [Integer] brand_pack_id of which status is to be changed
      # @param event [String] event to trigger to change the status
      #
      def change_brand_pack_state(id, event)
        brand_pack_dao_class = BRAND_PACK_DAO.new
        return brand_pack_dao_class.change_brand_pack_state(id, event)
      end

      #
      # return brand_pack corresponding to the id
      #
      # @param id [Integer] brand_pack id
      #
      # @return  brand_pack [Model] compnay object
      #
      # @error [InvalidArgumentsError] if brand_pack doesn't exists
      #
      def get_brand_pack_by_id(id)
        brand_pack_dao_class = BRAND_PACK_DAO.new

        brand_pack = brand_pack_dao_class.get_brand_pack_by_id(id)

        if brand_pack.nil?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_NOT_FOUND)
        end

        return brand_pack
      end

      #
      # Get display products/MPSPs for a given BrandPack
      # Return empty array if it is not active
      #
      # @param brand_pack [Object] This is output of join query on Active BrandPacks, Active MPBP and
      #                             Active MPSP
      #
      # @return [Array] [Array of display products/MPSPs]
      #
      def get_marketplace_selling_packs_for_brand_pack(brand_pack)
        
        display_products = []
        marketplace_brand_packs = brand_pack.marketplace_brand_packs
        marketplace_brand_packs.each do |marketplace_brand_pack|
          mp_selling_packs = marketplace_brand_pack.marketplace_selling_packs
          display_products = display_products | mp_selling_packs
        end
        return display_products
      end

      #
      # Function check if Brand pack is active
      #
      def brand_pack_active?(brand_pack)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INACTIVE_BP) unless brand_pack.active?
      end
    end
  end
end

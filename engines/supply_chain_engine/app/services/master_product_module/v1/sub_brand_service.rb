#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class SubBrandService < SupplyChainBaseModule::V1::BaseService
      SUB_BRAND_DAO = MasterProductModule::V1::SubBrandDao
      BRAND_DAO = MasterProductModule::V1::BrandDao
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      def initialize
        super
      end

      #
      # create sub_brand
      #
      # @param args [Hash] contain params for creating sub_brand
      #
      # @return [SubBrandObject]
      #
      # @error [InvalidArgumentsError] if it fails to create sub_brand
      #
      def create_sub_brand(args)
        sub_brand_dao_class = SUB_BRAND_DAO.new
        brand_dao_class = BRAND_DAO.new

        brand = brand_dao_class.get_brand_by_id(args[:brand_id])

        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_NOT_FOUND) if brand.nil?

        begin
          sub_brand = sub_brand_dao_class.create(args)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          raise e
        end
        sub_brand
      end

      #
      # update sub_brand
      #
      # @param args [Hash] contain params for updating sub_brand
      #
      # @return [SubBrandObject]
      #
      # @error [InvalidArgumentsError] if it fails to update sub_brand
      #
      def update_sub_brand(args, sub_brand_id)
        sub_brand_dao_class = SUB_BRAND_DAO.new
        brand_dao_class = BRAND_DAO.new

        if args[:brand_id].present?
          brand = brand_dao_class.get_brand_by_id(args[:brand_id])

         raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_NOT_FOUND) if brand.nil?
        end
        
        sub_brand = get_sub_brand_by_id(sub_brand_id)

        sub_brand_dao_class.update(args, sub_brand)
      end

      #
      # Return all the sub_brands
      #
      # @return [List] list of sub_brands
      #
      def get_all_sub_brands(pagination_params = {})
        sub_brand_dao_class = SUB_BRAND_DAO.new
        get_all_elements(sub_brand_dao_class, pagination_params)
      end

      #
      # change the state of sub brand
      #
      # @param id [Integer] sub_brand_id of which status is to be changed
      # @param event [String] event to trigger to change the status 
      #
      def change_sub_brand_state(id, event)
        sub_brand_dao_class = SUB_BRAND_DAO.new
        return sub_brand_dao_class.change_sub_brand_state(id, event)
      end

      #
      # return sub_brand corresponding to the id
      #
      # @param id [Integer] sub_brand id
      #
      # @return  sub_brand [Model] compnay object
      #
      # @error [InvalidArgumentsError] if sub_brand doesn't exists
      #
      def get_sub_brand_by_id(id)
        sub_brand_dao_class = SUB_BRAND_DAO.new

        sub_brand = sub_brand_dao_class.get_sub_brand_by_id(id)

        if sub_brand.nil?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::SUB_BRAND_NOT_FOUND)
        end

        return sub_brand
      end
    end
  end
end

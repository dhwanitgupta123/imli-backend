#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Logic layer for brand specific queries
    #
    class BrandService < SupplyChainBaseModule::V1::BaseService
      BRAND_DAO = MasterProductModule::V1::BrandDao
      COMPANY_DAO = SupplyChainModule::V1::CompanyDao
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      def initialize
        super
      end

      #
      # create brand
      #
      # @param args [Hash] contain params for creating brand
      #
      # @return [BrandObject]
      #
      # @error [InvalidArgumentsError] if it fails to create brand
      #
      def create_brand(args)
        brand_dao_class = BRAND_DAO.new
        company_dao_class = COMPANY_DAO.new

        company = company_dao_class.get_company_by_id(args[:company_id])

        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::COMPANY_NOT_FOUND) if company.nil?

        begin
          brand = brand_dao_class.create (args)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          raise e
        end
        brand
      end

      #
      # update brand
      #
      # @param args [Hash] contain params for updating brand
      #
      # @return [BrandObject]
      #
      # @error [InvalidArgumentsError] if it fails to update brand
      #
      def update_brand(args, brand_id)
        brand_dao_class = BRAND_DAO.new
        company_dao_class = COMPANY_DAO.new

        if args[:company_id].present?
          company = company_dao_class.get_company_by_id(args[:company_id])

         raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::COMPANY_NOT_FOUND) if company.nil?
        end
        
        brand = get_brand_by_id(brand_id)

        brand_dao_class.update(args, brand)
      end

      #
      # Return all the brands
      #
      # @return [List] list of brands
      #
      def get_all_brands(pagination_params = {})
        brand_dao_class = BRAND_DAO.new
        get_all_elements(brand_dao_class, pagination_params)
      end

      #
      # change the state of brand
      #
      # @param id [Integer] brand_id of which status is to be changed
      # @param event [String] event to trigger to change the status 
      #
      def change_brand_state(id, event)
        brand_dao_class = BRAND_DAO.new
        return brand_dao_class.change_brand_state(id, event)
      end

      #
      # return brand corresponding to the id
      #
      # @param id [Integer] brand id
      #
      # @return  brand [Model] compnay object
      #
      # @error [InvalidArgumentsError] if brand doesn't exists
      #
      def get_brand_by_id(id)
        brand_dao_class = BRAND_DAO.new

        brand = brand_dao_class.get_brand_by_id(id)

        if brand.nil?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_NOT_FOUND)
        end

        return brand
      end
    end
  end
end

#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class SellerBrandPackService < SupplyChainBaseModule::V1::BaseService
      SELLER_BRAND_PACK_DAO = SellerProductModule::V1::SellerBrandPackDao
      SELLER_DAO = SupplyChainModule::V1::SellerDao
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      WBP_DAO = WarehouseProductModule::V1::WarehouseBrandPackDao
      BRAND_PACK_DAO = MasterProductModule::V1::BrandPackDao
      def initialize(params = {})
        @params = params
        super
      end

      # 
      # Transactional creation of SBP
      #
      def transactional_create_seller_brand_pack(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return create_seller_brand_pack(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # Transactional updation of SBP
      #
      def transactional_update_seller_brand_pack(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return update_seller_brand_pack(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # create seller brand pack
      #
      # @param args [Hash] contain params for creating seller brand pack
      #
      # @return [SellerBrandPackObject]
      #
      # @error [InvalidArgumentsError] if it fails to create seller brand pack
      #
      def create_seller_brand_pack(args)
        seller_brand_pack_dao_class = SELLER_BRAND_PACK_DAO.new
        references = validate_reference_params(args)
        begin
          args = {
            sbp: {
              seller_id: args[:seller_id],
              brand_pack_id: args[:brand_pack_id]
            },
            warehouse_brand_pack: references[:wbp],
            pricing: args[:pricing]
          }
          seller_brand_pack = seller_brand_pack_dao_class.create(args)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          raise e
        end
        return seller_brand_pack
      end

      #
      # update seller brand pack
      #
      # @param args [Hash] contain params for updating seller brand pack
      #
      # @return [SellerBrandPackObject]
      #
      # @error [InvalidArgumentsError] if it fails to update seller brand pack
      #
      def update_seller_brand_pack(args)
        seller_brand_pack_dao_class = SELLER_BRAND_PACK_DAO.new
        seller_brand_pack = seller_brand_pack_dao_class.get_seller_brand_pack_by_id(args[:id])
        references = validate_reference_params(args)
        args = {
            sbp: {
              seller_id: args[:seller_id],
              brand_pack_id: args[:brand_pack_id]
            },
            warehouse_brand_pack: references[:wbp],
            pricing: args[:pricing]
          }
        seller_brand_pack_dao_class.update(args, seller_brand_pack)
      end

      #
      # Return all the seller brand packs
      #
      # @return [List] list of seller brand packs
      #
      def get_all_seller_brand_packs(paginate_params)
        seller_brand_pack_dao_class = SELLER_BRAND_PACK_DAO.new
        seller_brand_pack = seller_brand_pack_dao_class.paginated_seller_brand_pack(paginate_params)
        return seller_brand_pack
      end

      #
      # Return the requested seller brand pack details
      #
      # @return [List] object of seller brand pack
      #
      def get_seller_brand_pack(seller_brand_pack_id)
        seller_brand_pack_dao = SELLER_BRAND_PACK_DAO.new
        seller_brand_pack = seller_brand_pack_dao.get_seller_brand_pack_by_id(seller_brand_pack_id)
        return seller_brand_pack
      end

      # 
      # function to call dao changes state function to change the status of seller brand pack
      # 
      # @param request [request] [hash] contains event type to change state
      # 
      # @return [object] [SellerBrandPack]
      def change_state(request)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
          CONTENT::SELLER_BRAND_PACK_MISSING) if request[:id].blank?
        seller_brand_pack_dao_class = SELLER_BRAND_PACK_DAO.new
        seller_brand_pack = seller_brand_pack_dao_class.change_state(request)
        return seller_brand_pack
      end

      # 
      # Function to check if referencing attributes exists
      #
      def validate_reference_params(args)
        if args[:seller_id].present?
          seller = get_seller(args[:seller_id])
        end

        if  args[:warehouse_brand_pack_id].present? && args[:brand_pack_id].present?
          wbp = get_warehouse_brand_pack(args[:warehouse_brand_pack_id])
          brand_pack = get_brand_pack(args[:brand_pack_id])
          if wbp.brand_pack != brand_pack
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_NOT_FOUND_IN_WAREHOUSE)
          end
        end
        return { seller: seller, wbp: wbp, mbp: brand_pack }
      end

      # 
      # Function to get seller by id
      #
      def get_seller(seller_id)
        seller_dao = SELLER_DAO.new
        seller = seller_dao.get_seller_by_id(seller_id)
      end

      # 
      # function to get WBP by ID
      #
      def get_warehouse_brand_pack(warehouse_brand_pack_id)
        wbp_dao = WBP_DAO.new
        wbp = wbp_dao.get_wbp_by_id(warehouse_brand_pack_id)
        return wbp
      end

      # 
      # Function to get Brand pack by id
      #
      def get_brand_pack(brand_pack_id)
        brand_pack_dao = BRAND_PACK_DAO.new
        brand_pack = brand_pack_dao.get_brand_pack_by_id(brand_pack_id)
        if brand_pack == nil
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_NOT_FOUND)
        end
        return brand_pack
      end
    end
  end
end
#
# Module to handle all the functionalities related to warehouse brand pack
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Service layer for Warehouse brand pack
    #
    class WarehouseBrandPackService < SupplyChainBaseModule::V1::BaseService
      WBP_DAO = WarehouseProductModule::V1::WarehouseBrandPackDao
      SBP_SERVICE = SellerProductModule::V1::SellerBrandPackService
      WAREHOUSE_DAO = SupplyChainModule::V1::WarehouseDao
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      IBP_DAO = InventoryProductModule::V1::InventoryBrandPackDao
      BRAND_PACK_DAO = MasterProductModule::V1::BrandPackDao
      WAREHOUSE_STOCK_SERVICE = WarehouseProductModule::V1::WarehouseStockService
      SELLER_SERVICE = SupplyChainModule::V1::SellerService
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates
      MARKETPLACE_BRAND_PACK_SERVICE = MarketplaceProductModule::V1::MarketplaceBrandPackService
      GENERAL_HELPER = CommonModule::V1::GeneralHelper
      def initialize(params={})
        @params = params
        super
      end

      # 
      # Transactional creation of WBP
      #
      def transactional_create_warehouse_brand_pack(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return create(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # Transactional updation of WBP
      #
      def transactional_update_warehouse_brand_pack(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return update(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # Transactional stocking of WBPs
      #
      def transactional_stock_warehouse_brand_packs(args)
        transactional_function = Proc.new do |args|
          return stock_warehouse_brand_packs(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # Transactions stocking of WBPs which damaged & expired
      #
      def transactional_stock_warehouse_brand_packs_status(args)
        transactional_function = Proc.new do |args|
          return stock_warehouse_brand_packs_status(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # create warehouse brand pack
      #
      # @param args [Hash] contain params for creating WBP
      #
      # @return [WBP object]
      #
      # @error [InvalidArgumentsError] if it fails to create WBP
      #
      def create(args)
        wbp_dao = WBP_DAO.new
        references = validate_reference_params(args)
        if args[:pricing].blank?
          args[:pricing] = {
            mrp: references[:mbp].mrp,
            selling_price: references[:ibp].inventory_pricing.net_landing_price,
            service_tax: references[:ibp].inventory_pricing.service_tax,
            vat: references[:ibp].inventory_pricing.vat,
            cst: references[:ibp].inventory_pricing.cst
          }
        end
        begin
          args = {
            wbp: {
              warehouse_id: args[:warehouse_id],
              brand_pack_id: args[:brand_pack_id],
              status: COMMON_MODEL_STATES::ACTIVE
            },
            inventory_brand_pack: references[:ibp],
            pricing: args[:pricing]
          }
          if args[:threshold_stock_value].present?
            args[:wbp][:threshold_stock_value] = args[:threshold_stock_value]
          end
          if args[:outbound_frequency_period_in_days].present?
            args[:wbp][:outbound_frequency_period_in_days] = args[:outbound_frequency_period_in_days]
          end
          wbp = wbp_dao.create(args)
          create_mpbp_from_brand_pack(references[:mbp], wbp)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          raise e
        end
        return wbp
      end

      #
      # This function create mpbp from brand pack, it first create wbp then sbp
      # and finally mpbp
      #
      # @param brand_pack [BrandPackModel] Brand Pack object
      #
      def create_mpbp_from_brand_pack(brand_pack, wbp)
        mpbp_service = MARKETPLACE_BRAND_PACK_SERVICE.new(@params)
        seller_brand_pack = create_seller_brand_pack(brand_pack, wbp)
        marketplace_brand_pack_args = 
          {
            brand_pack_id: brand_pack.id,
            seller_brand_pack_id: seller_brand_pack.id,
            status: COMMON_MODEL_STATES::ACTIVE
          }
        mpbp_service.create_marketplace_brand_pack(marketplace_brand_pack_args)
      end

      # 
      # THIS IS VALID ONLY WHEN THERE IS ONLY ONE WAREHOUSE
      # This function get the first warehouse and creates warehouse_brand_pack
      # corresponding to the ibp and warehouse
      #
      # @param inventory_order_product [inventory_order_product] inventory_order_product object
      # 
      # @return [WarehouseBrandPackModel] wbp model
      #
      def create_warehouse_brand_pack(inventory_order,inventory_order_product)
        warehouse_brand_pack_args = 
          {
            warehouse_id: inventory_order.warehouse.id,
            brand_pack_id: inventory_order_product.inventory_brand_pack.brand_pack.id,
            inventory_brand_pack_id: inventory_order_product.inventory_brand_pack.id,
            status: COMMON_MODEL_STATES::ACTIVE,
            pricing: {
              mrp: inventory_order_product.mrp,
              selling_price: inventory_order_product.cost_price,
              service_tax: inventory_order_product.service_tax,
              vat: inventory_order_product.vat,
              cst: inventory_order_product.cst
            }
          }
        create(warehouse_brand_pack_args)
      end

      # 
      # THIS IS VALID ONLY WHEN THERE IS ONLY ONE SELLER
      # This function get the first seller and creates seller_brand_pack
      # corresponding to the wbp and seller
      #
      # @param brand_pack [BrandPackModel] brand pack object
      # @param warehouse_brand_pack [WarehouseBrandPack] wbp object 
      # 
      # @return [SellerBrandPackModel] sbp model
      #
      def create_seller_brand_pack(brand_pack, warehouse_brand_pack)
        seller_service = SELLER_SERVICE.new(@params)
        sellers = seller_service.get_all_sellers
        seller = sellers[:seller].first

        seller_brand_pack_args = 
        {
          seller_id: seller.id,
          warehouse_brand_pack_id: warehouse_brand_pack.id,
          brand_pack_id: brand_pack.id, 
          status: COMMON_MODEL_STATES::ACTIVE
        }
        seller_brand_pack_service = SBP_SERVICE.new(@params)
        seller_brand_pack_service.create_seller_brand_pack(seller_brand_pack_args)
      end

      #
      # Return all the warehouse brand packs
      #
      # @return [List] list of WBPs
      #
      def get_all_wbps(paginate_params={})
        wbp_dao = WBP_DAO.new
        wbps = wbp_dao.paginated_wbps(paginate_params)
        return wbps
      end

      #
      # Return the requested warehouse details
      #
      # @return [List] object of warehouse
      #
      def get_wbp(warehouse_id)
        wbp_dao = WBP_DAO.new
        wbp = wbp_dao.get_wbp_by_id(warehouse_id)
        return wbp
      end

      #
      # update WBP
      #
      # @param args [Hash] contain params for updating WBP
      #
      # @return [WBP object]
      #
      # @error [InvalidArgumentsError] if it fails to update WBP
      #
      def update(args)
        wbp_dao = WBP_DAO.new

        wbp = wbp_dao.get_wbp_by_id(args[:id])
        references = validate_reference_params(args)
        args = {
            wbp: {
              warehouse_id: args[:warehouse_id],
              brand_pack_id: args[:brand_pack_id],
              threshold_stock_value: args[:threshold_stock_value],
              outbound_frequency_period_in_days: args[:outbound_frequency_period_in_days]
            },
            inventory_brand_pack: references[:ibp],
            pricing: args[:pricing]
          }
        wbp_dao.update(args, wbp)
      end

      # 
      # function to call dao changes state function to change the status of warehouse
      # 
      # @param request [request] [hash] contains event type to change state
      # 
      # @return [object] [warehouse]
      def change_state(request)
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(
          CONTENT::WAREHOUSE_BRAND_PACK_MISSING) if request[:id].blank?
        wbp_dao = WBP_DAO.new
        wbp = wbp_dao.change_state(request)
        return wbp
      end

      # 
      # Function to check if referencing attributes exists
      #
      def validate_reference_params(args)
        if args[:warehouse_id].present?
          warehouse = get_warehouse(args[:warehouse_id])
        end

        if  args[:inventory_brand_pack_id].present? && args[:brand_pack_id].present?
          ibp = get_inventory_brand_pack(args[:inventory_brand_pack_id])
          brand_pack = get_brand_pack(args[:brand_pack_id])
          if ibp.brand_pack != brand_pack
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_NOT_FOUND_IN_INVENTORY)
          end
        end
        return { warehouse: warehouse, ibp: ibp, mbp: brand_pack }
      end

      # 
      # Function to get warehouse by id
      #
      def get_warehouse(warehouse_id)
        warehouse_dao = WAREHOUSE_DAO.new
        warehouse = warehouse_dao.get_warehouse_by_id(warehouse_id)
      end

      # 
      # function to get IBP by ID
      #
      def get_inventory_brand_pack(inventory_brand_pack_id)
        ibp_dao = IBP_DAO.new
        ibp = ibp_dao.get_by_id(inventory_brand_pack_id)
        if ibp == nil
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVENTORY_BRAND_PACK_NOT_FOUND)
        end
        return ibp
      end

      # 
      # Function to get Brand pack by id
      #
      def get_brand_pack(brand_pack_id)
        brand_pack_dao = BRAND_PACK_DAO.new
        brand_pack = brand_pack_dao.get_brand_pack_by_id(brand_pack_id)
        if brand_pack == nil
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_NOT_FOUND)
        end
        return brand_pack
      end

      #
      # Function to stock the quantity of WBPs
      #
      def stock_warehouse_brand_packs(args)
        verify_references_of_wbp_for_stocking(args)
        args[:params].each do |param|
          stock_warehouse_brand_pack(param)
        end
      end

      #
      # Function to store the quantity of a WBP as a part of INWARD activity
      #
      def stock_warehouse_brand_pack(param)
        warehouse_stock_service = WAREHOUSE_STOCK_SERVICE.new(@params)
        warehouse_brand_pack = get_wbp(param[:id])
        pricing = warehouse_brand_pack.warehouse_pricings.first
        pricing.warehouse_stocks << warehouse_stock_service.stock_new_warehouse_brand_pack(param)
      end

      #
      # Function to mark the quantity of WBPs which are going out of Warehouse
      #
      def out_warehouse_brand_packs(brand_packs_by_quantity)
        return if brand_packs_by_quantity.blank?
        warehouse_stock_service = WAREHOUSE_STOCK_SERVICE.new(@params)
        warehouse = brand_packs_by_quantity.first[:brand_pack].warehouse_brand_packs.first.warehouse
        brand_packs_by_quantity.each do |param|
          warehouse_brand_pack = get_wbp_by_parameters(param[:brand_pack], warehouse)
          pricing = warehouse_brand_pack.warehouse_pricings.first
          ignore_stock_availability = GENERAL_HELPER.string_to_boolean(CACHE_UTIL.read('IGNORE_STOCK_AVAILABILITY'))
          if warehouse_stock_service.stock_is_available?(pricing.warehouse_stocks) || ignore_stock_availability
            pricing.warehouse_stocks << warehouse_stock_service.outward_warehouse_brand_pack(param[:units_to_procure])
          else
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::STOCK_NOT_AVAILABLE%{product: warehouse_brand_pack.brand_pack.sku})
          end
        end
      end

      #
      # Function to mark the quantity of WBPs which are coming back to warehouse after being packed
      #
      def inward_warehouse_brand_packs(brand_packs_by_quantity)
        return if brand_packs_by_quantity.blank?
        warehouse_stock_service = WAREHOUSE_STOCK_SERVICE.new(@params)
        warehouse = brand_packs_by_quantity.first[:brand_pack].warehouse_brand_packs.first.warehouse
        brand_packs_by_quantity.each do |param|
          warehouse_brand_pack = get_wbp_by_parameters(param[:brand_pack], warehouse)
          pricing = warehouse_brand_pack.warehouse_pricings.first
          pricing.warehouse_stocks << warehouse_stock_service.inward_warehouse_brand_pack(param[:units_to_procure])
        end
      end

      #
      # Function to mark the quantity of WBPs which are coming back to warehouse after being packed
      #
      def inward_cancelled_warehouse_brand_packs(brand_packs_by_quantity)
        return if brand_packs_by_quantity.blank?
        warehouse_stock_service = WAREHOUSE_STOCK_SERVICE.new(@params)
        warehouse = brand_packs_by_quantity.first[:brand_pack].warehouse_brand_packs.first.warehouse
        brand_packs_by_quantity.each do |param|
          warehouse_brand_pack = get_wbp_by_parameters(param[:brand_pack], warehouse)
          pricing = warehouse_brand_pack.warehouse_pricings.first
          pricing.warehouse_stocks << warehouse_stock_service.inward_cancelled_warehouse_brand_pack(param[:units_to_procure])
        end
      end

      #
      # Function to verify if the references of stocking are params are correct
      #
      def verify_references_of_wbp_for_stocking(args)
        warehouse = get_warehouse(args[:warehouse_id])
        args[:params].each do |param|
          verify_wbp_belongs_to_warehouse(param[:id], warehouse)
        end
      end

      #
      # Function to verify if WBP belongs to the correct warehouse
      #
      def verify_wbp_belongs_to_warehouse(wbp_id, warehouse)
        wbp = get_wbp(wbp_id)
        if wbp.warehouse != warehouse
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WRONG_WAREHOUSE_BRAND_PACK)
        end
      end

      #
      # Function to stock the quantity of WBPs which are expired & damaged
      #
      def stock_warehouse_brand_packs_status(args)
        verify_references_of_wbp_for_stocking(args)
        args[:params].each do |param|
          stock_warehouse_brand_pack_status(param)
        end
      end

      #
      # Function to store the quantity of a WBP as a part of EXPIRED & DAMAGED activity
      #
      def stock_warehouse_brand_pack_status(param)
        warehouse_stock_service = WAREHOUSE_STOCK_SERVICE.new(@params)
        warehouse_brand_pack = get_wbp(param[:id])
        pricing = warehouse_brand_pack.warehouse_pricings.first
        pricing.warehouse_stocks << warehouse_stock_service.stock_warehouse_brand_pack_status(param)
      end

      #
      # Function to fetch WBPs based on brand pack & Warheouse
      #
      def get_wbp_by_parameters(brand_pack, warehouse)
        wbp_dao = WBP_DAO.new(@params)
        wbp = wbp_dao.get_wbp_by_parameters(brand_pack, warehouse)
        return wbp
      end

      #
      # Function to mark auditted the stock deetails in warehouse
      #
      def audit_warehouse_brand_packs(warehouse_brand_packs)
        warehouse_stock_service = WAREHOUSE_STOCK_SERVICE.new(@params)
        warehouse_brand_packs.each do |warehouse_brand_pack|
          pricing = warehouse_brand_pack.warehouse_pricings.first
          pricing.warehouse_stocks << warehouse_stock_service.audit_warehouse_brand_pack(pricing.warehouse_stocks)
        end
      end

      #
      # Function to fetch all wbps by it's warehouse ID
      #
      def get_warehouse_brand_pack_by_warehouse_id(args)
        wbp_dao = WBP_DAO.new(@params)
        wbp_details = wbp_dao.get_filtered_warehouse_brand_pack_by_warehouse_id(args)
        return { warehouse_brand_pack: wbp_details[:warehouse_brand_packs], page_count: wbp_details[:page_count] }
      end

      #
      # Function to fetch stock details of all the WBPs
      #
      def get_wbps_stock_detail(warehouse_brand_packs)
        warehouse_stock_service = WAREHOUSE_STOCK_SERVICE.new(@params)
        wbp_stock_array = []
        return wbp_stock_array if warehouse_brand_packs.blank?
        warehouse_brand_packs.each do |warehouse_brand_pack|
          params = {
            warehouse_brand_pack: warehouse_brand_pack
          }
          pricing = warehouse_brand_pack.warehouse_pricings.first
          params = params.merge(warehouse_stock_service.get_wbp_stock_details(pricing.warehouse_stocks, warehouse_brand_pack.outbound_frequency_period_in_days))
          wbp_stock_array.push(params)
        end
        wbp_stock_array = wbp_stock_array.sort{|a,b| b[:available] <=> a[:available]}
        return wbp_stock_array
      end
    end
  end
end

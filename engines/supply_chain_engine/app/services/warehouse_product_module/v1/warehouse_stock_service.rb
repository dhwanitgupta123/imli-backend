#
# Module to handle all the functionalities related to warehouse product module
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Service layer for Warehouse stock
    #
    class WarehouseStockService < SupplyChainBaseModule::V1::BaseService
      WAREHOUSE_STOCK_DAO = WarehouseProductModule::V1::WarehouseStockDao
      def initialize(params = {})
        @params = params
        super
      end

      #
      # Function to store the quantity of a WBP as a part of INWARD activity
      #
      def stock_new_warehouse_brand_pack(param)
        warehouse_stock_dao = WAREHOUSE_STOCK_DAO.new(@params)
        return warehouse_stock_dao.stock_new_warehouse_brand_pack(param)
      end

      #
      # Function to mark the quantity of WBP which is going out of Warehouse
      #
      def outward_warehouse_brand_pack(quantity)
        warehouse_stock_dao = WAREHOUSE_STOCK_DAO.new(@params)
        return warehouse_stock_dao.outward_warehouse_brand_pack(quantity)
      end

      #
      # Function to mark the quantity of WBP which is coming in to warehouse after being packed
      #
      def inward_warehouse_brand_pack(quantity)
        warehouse_stock_dao = WAREHOUSE_STOCK_DAO.new(@params)
        return warehouse_stock_dao.inward_warehouse_brand_pack(quantity)
      end

      #
      # Function to mark the quantity of WBP which is coming in to warehouse after being packed
      #
      def inward_cancelled_warehouse_brand_pack(quantity)
        warehouse_stock_dao = WAREHOUSE_STOCK_DAO.new(@params)
        return warehouse_stock_dao.inward_cancelled_warehouse_brand_pack(quantity)
      end

      def audit_warehouse_brand_pack(warehouse_stocks)
        warehouse_stock_dao = WAREHOUSE_STOCK_DAO.new(@params)
        warehouse_stock_dao.audit_warehouse_brand_pack(warehouse_stocks)
      end

      def stock_warehouse_brand_pack_status(param)
        warehouse_stock_dao = WAREHOUSE_STOCK_DAO.new(@params)
        return warehouse_stock_dao.stock_warehouse_brand_pack_status(param)
      end

      def get_wbp_stock_details(warehouse_stocks, outbound_frequency_period_in_days = 0)
        warehouse_stock_dao = WAREHOUSE_STOCK_DAO.new(@params)
        warehouse_stock_dao.get_wbp_stock_details(warehouse_stocks, outbound_frequency_period_in_days)
      end

      def stock_is_available?(stocks_detail)
        warehouse_stock_dao = WAREHOUSE_STOCK_DAO.new(@params)
        stock_availability = warehouse_stock_dao.get_wbp_stock_details(stocks_detail)
        if stock_availability[:available] > 0 
          return true 
        else
          return false
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Warehouse util class to implement utility functions
    #    
    class WarehouseUtil

      STOCK_SHEET_NAME = 'stock_sheet.csv'
      PROCUREMENT_SHEET_NAME = 'procurement_sheet.csv'
      FILE_SERVICE_HELPER = FileServiceModule::V1::FileServiceHelper

      #
      # Function to generate link of Procurement sheet
      #
      def self.generate_procurement_sheet_link(warehouse_procurement)
        generate_procurement_sheet(warehouse_procurement)
        return upload_to_s3(@path, PROCUREMENT_SHEET_NAME)
      end

      #
      # Function to generate Stock sheet link on s3
      #
      def self.generate_stock_sheet_link(warehouse_stock)
        generate_stock_sheet(warehouse_stock)
        return upload_to_s3(@path, STOCK_SHEET_NAME)
      end

      #
      # Function to upload CSV on s3
      #
      def self.upload_to_s3(path, file_name)
        # Upload to S3 and get signed URL
        signed_csv_expiry_duration = 5.minutes
        signed_csv_link = FILE_SERVICE_HELPER.upload_file_to_s3_and_get_signed_url(
          path, file_name, signed_csv_expiry_duration)
        File.delete(path)
        return signed_csv_link
      end

      #
      # Function to generate procurement sheet locally
      #
      def self.generate_procurement_sheet(warehouse_procurement)
        @path = Rails.root.join(PROCUREMENT_SHEET_NAME)
        output_file = CSV.open(PROCUREMENT_SHEET_NAME,'w')
        ##Header##
        header = ['bp_code', 'sku', 'stock_available', 'mrp', 'threshold', 'x_days', 'order_per_x_days']
        output_file << header
        warehouse_procurement.each do |wbp_procurement|
          bp_code = wbp_procurement[:brand_pack][:brand_pack_code]
          sku = wbp_procurement[:brand_pack][:sku]
          stock_available = wbp_procurement[:procurement][:available]
          mrp = wbp_procurement[:brand_pack][:mrp]
          threshold = wbp_procurement[:threshold_stock_value]
          x_days = wbp_procurement[:outbound_frequency_period_in_days]
          order_per_x_days = wbp_procurement[:procurement][:sold_in_x_days]
          row = [bp_code, sku, stock_available, mrp, threshold, x_days, order_per_x_days]
          output_file << row
          row = []
        end
        output_file.close
      end

      #
      # Function to generate stock sheet locally
      #
      def self.generate_stock_sheet(warehouse_stock)
        @path = Rails.root.join(STOCK_SHEET_NAME)
        output_file = CSV.open(STOCK_SHEET_NAME,'w')
        ##Header##
        header = ['last_audited_date', 'bp_code', 'sku', 'starting_stock_value', 'inward', 'outward', 'expired', 'damaged', 'ideal_stock_available']
        output_file << header
        warehouse_stock.each do |wbp_stock|
          last_audited_date = wbp_stock[:stock][:audit_date]
          bp_code = wbp_stock[:brand_pack][:brand_pack_code]
          sku = wbp_stock[:brand_pack][:sku]
          starting_stock_value = wbp_stock[:stock][:last_audit_quantity]
          inward = wbp_stock[:stock][:inward]
          outward = wbp_stock[:stock][:outward]
          expired = wbp_stock[:stock][:expired]
          damaged = wbp_stock[:stock][:damaged]
          ideal_stock_available = wbp_stock[:stock][:available]
          row = [last_audited_date, bp_code, sku, starting_stock_value, inward, outward, expired, damaged, ideal_stock_available]
          output_file << row
          row = []
        end
        output_file.close
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Product Util to contain utils used for Product
    #
    class ProductUtil

      PRODUCT_DAO = MasterProductModule::V1::ProductDao

      # 
      # This function determines the products
      #
      # @param args [Hash] it may contain sub_brands, and more (as per future requirements)
      # 
      # @return [Model] products
      #
      def self.filter_products(args)
        product_dao = PRODUCT_DAO.new

        if args[:sub_brands].present?
          products = product_dao.get_products_by_sub_brands(args[:sub_brands])
          return products
        end
        
        return nil
      end

    end
  end
end

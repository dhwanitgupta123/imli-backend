#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Helper class to create JsonResponse
    #
    class InventoryProductDecorator < BaseModule::V1::BaseResponseDecorator
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content

      def self.create_inventory_brand_pack_response(inventory_brand_packs, page_count=1)
        {
          payload: {
            inventory_brand_packs: inventory_brand_packs
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: page_count
          }
        }
      end

      def self.create_single_inventory_brand_pack_response(inventory_brand_pack)
        {
          payload: {
            inventory_brand_pack: inventory_brand_pack
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end
    end
  end
end
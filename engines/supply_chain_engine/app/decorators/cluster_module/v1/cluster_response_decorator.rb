#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # Helper class to create JsonResponse
    #
    class ClusterResponseDecorator < BaseModule::V1::BaseResponseDecorator

      def self.create_single_cluster_response(cluster)
        {
          payload: {
            cluster: cluster
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_clusters_response(clusters, page_count)
        {
          payload: {
            clusters: clusters
          },
          meta: {
            page_count: page_count
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end
    end
  end
end

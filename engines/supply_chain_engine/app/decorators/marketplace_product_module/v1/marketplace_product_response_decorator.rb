#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Helper class to create JsonResponse
    #
    class MarketplaceProductResponseDecorator < BaseModule::V1::BaseResponseDecorator
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content

      def self.create_marketplace_selling_pack_response(marketplace_selling_packs, page_count)
        {
          payload: {
            marketplace_selling_packs: marketplace_selling_packs
          },
          meta: {
            page_count: page_count
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_single_marketplace_selling_pack_response(marketplace_selling_pack)
        {
          payload: {
            marketplace_selling_pack: marketplace_selling_pack
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_marketplace_brand_pack_response(marketplace_brand_packs, page_count)
        {
          payload: {
            marketplace_brand_packs: marketplace_brand_packs
          },
          meta: {
            page_count: page_count
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_single_marketplace_brand_pack_response(marketplace_brand_pack)
        {
          payload: {
            marketplace_brand_pack: marketplace_brand_pack
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_search_marketplace_selling_pack_response(marketplace_selling_pack_model_reader)
        {
          payload: {
            products: marketplace_selling_pack_model_reader.get_attribute_array
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end
    end
  end
end

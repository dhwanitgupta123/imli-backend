#
# Module to handle all the functionalities related to supply chain
#
module WarehouseProductModule
  #
  # Version1 for WarehouseProduct module
  #
  module V1
    #
    # Helper class to create JsonResponse
    #
    class WarehouseProductResponseDecorator < BaseModule::V1::BaseResponseDecorator
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content

      def self.create_wbp_response(args)
        {
          payload: {
            warehouse_brand_packs: args[:warehouse_brand_pack]
          },
          meta: {
            page_count: args[:page_count]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_single_wbp_response(wbp)
        {
          payload: {
            warehouse_brand_pack: wbp
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end
    end
  end
end

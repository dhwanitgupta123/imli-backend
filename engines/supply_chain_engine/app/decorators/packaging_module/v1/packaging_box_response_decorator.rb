#
# Module to handle all the functionalities related to Packaging
#
module PackagingModule
  #
  # Version1 for Packaging module
  #
  module V1
    #
    # Map packaging_box object to Json
    #
    class PackagingBoxResponseDecorator < BaseModule::V1::BaseResponseDecorator

      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content

      #
      # Response for add time slot api
      #
      def self.create_add_packaging_box_response(response)
        return {
          payload: {
            packaging_box: response[:packaging_box]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_get_all_packaging_boxes_response(response)
        return {
          payload: {
            packaging_boxes: response[:packaging_boxes]
          },
          meta: {
            page_count: response[:page_count]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_get_packaging_box_response(response)
        return {
          payload: {
            packaging_box: response[:packaging_box]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_update_packaging_box_response(response)
        return {
          payload: {
            packaging_box: response[:packaging_box]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_change_packaging_box_state_response(response)
        return {
          payload: {
            message: 'State changed successfuly'
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

    end
  end
end
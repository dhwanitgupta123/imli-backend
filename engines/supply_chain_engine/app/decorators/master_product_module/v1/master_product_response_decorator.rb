#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Helper class to create JsonResponse
    #
    class MasterProductResponseDecorator < BaseModule::V1::BaseResponseDecorator
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      
      def self.create_brand_response(brands, page_count)
        {
          payload: {
            brands: brands 
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: page_count
          }
        }
      end

      def self.create_single_brand_response(brand)
        {
          payload: {
            brand: brand
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_sub_brand_response(sub_brands, page_count)
        {
          payload: {
            sub_brands: sub_brands
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: page_count
          }
        }
      end

      def self.create_single_sub_brand_response(sub_brand)
        {
          payload: {
            sub_brand: sub_brand
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_product_response(products, page_count)
        {
          payload: {
            products: products
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: page_count
          }
        }
      end

      def self.create_single_product_response(product)
        {
          payload: {
            product: product
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_brand_pack_response(brand_packs, page_count)
        {
          payload: {
            brand_packs: brand_packs
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: page_count
          }
        }
      end

      def self.create_single_brand_pack_response(brand_pack)
        {
          payload: {
            brand_pack: brand_pack
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end


      #
      # Create JSON response of Variants
      #
      def self.create_variant_response(variants)
        {
          payload: {
            variants: variants
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end


      #
      # Create JSON response of Variant
      #
      def self.create_single_variant_response(variant)
        {
          payload: {
            variant: variant
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end
    end
  end
end

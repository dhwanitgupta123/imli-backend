#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    #
    # Helper class to create JsonResponse
    #
    class SellerProductResponseDecorator < BaseModule::V1::BaseResponseDecorator
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content

      def self.create_single_seller_brand_pack_response(seller_brand_pack)
        {
          payload: {
            seller_brand_pack: seller_brand_pack
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_seller_brand_pack_response(seller_brand_pack)
        {
          payload: {
            seller_brand_packs: seller_brand_pack[:seller_brand_pack]
          },
          meta: {
            page_count: seller_brand_pack[:page_count]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end
    end
  end
end
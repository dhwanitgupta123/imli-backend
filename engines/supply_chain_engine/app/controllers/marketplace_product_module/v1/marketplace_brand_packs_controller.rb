#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # MarketplaceBrandPack controller service class to route panel API calls to the MarketplaceBrandPackApi
    #
    class MarketplaceBrandPacksController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_MARKETPLACE_BRAND_PACK = MarketplaceProductModule::V1::AddMarketplaceBrandPackApi
      UPDATE_MARKETPLACE_BRAND_PACK = MarketplaceProductModule::V1::UpdateMarketplaceBrandPackApi
      GET_ALL_MARKETPLACE_BRAND_PACK = MarketplaceProductModule::V1::GetAllMarketplaceBrandPackApi
      GET_MARKETPLACE_BRAND_PACK_API = MarketplaceProductModule::V1::GetMarketplaceBrandPackApi
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      CREATE_MPBP_AND_BRAND_PACK_API = MarketplaceProductModule::V1::CreateMpbpAndBrandPackApi
      CHANGE_MARKETPLACE_BRAND_PACK_STATE_API = MarketplaceProductModule::V1::ChangeMarketplaceBrandPackStateApi

      def initialize
      end

      #
      # function to add marketplace_brand_pack
      #
      # post /add_marketplace_brand_pack
      #
      # Request:: request object contain
      #   * marketplace_brand_pack_params [hash] it contains name
      #
      # Response::
      #   * If action succeed then returns Success Status code with create marketplace_brand_pack
      #    else returns BadRequest response
      #
      def add_marketplace_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_marketplace_brand_pack_api = ADD_MARKETPLACE_BRAND_PACK.new(deciding_params)
        response = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_params)
        send_response(response)
      end

      swagger_controller :marketplace_brand_pack, 'MarketplaceProductModule APIs'
      swagger_api :add_marketplace_brand_pack do
        summary 'It creates the marketplace_brand_pack with given input'
        notes 'create marketplace_brand_pack'
        param :body, :marketplace_brand_pack_request, :marketplace_brand_pack_request, :required, 'create_marketplace_brand_pack request'
        response :bad_request, 'if request params are incorrect or api fails to create marketplace_brand_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :marketplace_brand_pack_request do
        description 'request schema for /create_category API'
        property :marketplace_brand_pack, :marketplace_brand_pack_hash, :required, 'marketplace_brand_pack model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :marketplace_brand_pack_hash do
        description 'details of marketplace_brand_pack'
        property :brand_pack_id, :integer, :required, 'reference of brand_pack'
        property :seller_brand_pack_id, :integer, :required, 'reference of seller_brand_pack'
        property :is_on_sale, :boolean, :required, 'If a product is on sale'
        property :is_ladder_pricing_active, :boolean, :required, 'If product has ladder pricing active'
        property :pricing, :marketplace_brand_pack_pricing, :required,
                 'it has details of marketplace brand pack'
      end

      swagger_model :marketplace_brand_pack_pricing do
        description 'details of marketplace_brand_pack pricings'
        property :spat, :double, :required, 'Selling price and tax'
        property :margin, :double, :required, 'margin on selling price'
        property :service_tax, :double, :required, 'service tax to be applied'
        property :vat, :double, :required, 'vat tax to be applied'
        property :discount, :double, :optional, 'discount offered'
        property :buying_price, :double, :required, 'buying price of MPBP'
        property :selling_price, :double, :required, 'selling price of MPBP'
        property :savings, :double, :required, 'savings on MPBP'
        property :mrp, :double, :required, 'MRP of MPBP'
      end

      #
      # THIS IS TEMPORARY API TO CREATE MPBP WHILE CREATING BRAND_PACK AS THERE IS ONLY
      # ONE INVENTORY
      # ONE WAREHOUSE
      # ONE SELLER
      # HENCE THERE IS ALWAYS ONE TO ONE MAPPING
      #
      def create_mpbp_and_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        create_mpbp_and_brand_pack_api = CREATE_MPBP_AND_BRAND_PACK_API.new(deciding_params)
        response = create_mpbp_and_brand_pack_api.enact(brand_pack_params)
        send_response(response)
      end

      swagger_controller :marketplace_brand_pack, 'MasterProductModule APIs'
      swagger_api :create_mpbp_and_brand_pack do
        summary 'It creates the brand_pack with given input'
        notes 'create brand_pack'
        param :body, :brand_pack_request, :brand_pack_request, :required, 'create_brand_pack request'
        response :bad_request, 'if request params are incorrect or api fails to create brand_pack'
      end

      swagger_model :brand_pack_request do
        description 'request schema for /create_category API'
        property :brand_pack, :brand_pack_hash, :required, 'brand_pack model hash', 'brand_pack' => { '$ref' => 'category_hash' }
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :brand_pack_hash do
        description 'details of brand_pack'
        property :sku, :string, :required, 'name of brand_pack'
        property :images, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :image_hash_array } }        
        property :mrp, :float, :optional, 'mrp'
        property :sku_size, :string, :optional, 'sku size'
        property :unit, :string, :optional, 'unit'
        property :description, :string, :optional, 'description'
        property :shelf_life, :string, :optional, 'shelf_life'
        property :retailer_pack, :string, :optional, 'retailer pack'
        property :article_code, :string, :optional, 'unique code for brand pack'
        property :product_id, :integer, :required, 'product'
        property :sub_category_id, :integer, :required, 'sub category'
        property :brand_pack_code, :string, :optional, 'brand pack code'
      end

      #
      # function to update marketplace_brand_pack
      #
      # post /update_marketplace_brand_pack
      #
      # Request:: request object contain
      #   * marketplace_brand_pack_params [hash] it contains name
      #
      # Response::
      #   * If action succeed then returns Success Status code with update marketplace_brand_pack
      #    else returns BadRequest response
      #
      def update_marketplace_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_marketplace_brand_pack_api = UPDATE_MARKETPLACE_BRAND_PACK.new(deciding_params)
        response = update_marketplace_brand_pack_api.enact(marketplace_brand_pack_params.merge({ id: get_id_from_params }))
        send_response(response)
      end

      swagger_controller :marketplace_brand_pack, 'MarketplaceProductModule APIs'
      swagger_api :update_marketplace_brand_pack do
        summary 'It updates the marketplace_brand_pack with given input'
        notes 'update marketplace_brand_pack'
        param :path, :id, :integer, :required, 'marketplace_brand_pack to update'
        param :body, :update_marketplace_brand_pack_request, :update_marketplace_brand_pack_request, :required, 'create_marketplace_brand_pack request'
        response :bad_request, 'if request params are incorrect or api fails to create marketplace_brand_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :update_marketplace_brand_pack_request do
        description 'request schema for /create_category API'
        property :marketplace_brand_pack, :marketplace_brand_pack_hash, :required, 'marketplace_brand_pack model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      #
      # function to get all marketplace_brand_packs
      #
      # GET /get_all_marketplace_brand_packs
      #
      # Response::
      #   * list of marketplace_brand_packs
      #
      def get_all_marketplace_brand_packs
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_marketplace_brand_packs_api = GET_ALL_MARKETPLACE_BRAND_PACK.new(deciding_params)
        response = get_marketplace_brand_packs_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :marketplace_brand_pack, 'MarketplaceProductModule APIs'
      swagger_api :get_all_marketplace_brand_packs do
        summary 'It returns details of all the marketplace_brand_packs'
        notes 'get_all_marketplace_brand_packs API returns the details of all the marketplace_brand_packs'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all marketplace_brand_pack'
        param :query, :per_page, :integer, :optional, 'how many marketplace_brand_pack to display in a page'
        param :query, :state, :integer, :optional, 'to fetch marketplace_brand_packs based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to change the status of marketplace_brand_pack
      #
      # PUT /marketplace_brand_pack/state/:id
      #
      # Request::
      #   * marketplace_brand_pack_id [integer] id of marketplace_brand_pack of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_marketplace_brand_pack_state_api = CHANGE_MARKETPLACE_BRAND_PACK_STATE_API.new(deciding_params)
        response = change_marketplace_brand_pack_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :marketplace_brand_pack, 'MarketplaceProductModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of marketplace_brand_pack'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the marketplace_brand_pack & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'marketplace_brand_pack_id to be deactivated/activated'
        param :body, :change_marketplace_brand_pack_state_request, :change_marketplace_brand_pack_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or marketplace_brand_pack not found or it fails to change state'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_marketplace_brand_pack_state_request do
        description 'request schema for /change_state API'
        property :marketplace_brand_pack, :change_marketplace_brand_pack_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_marketplace_brand_pack_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      def delete_marketplace_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_marketplace_brand_pack_state_api = CHANGE_MARKETPLACE_BRAND_PACK_STATE_API.new(deciding_params)
        response = change_marketplace_brand_pack_state_api.enact({ 
                      id: get_id_from_params, 
                      event: COMMON_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :marketplace_brand_pack, 'MarketplaceProductModule APIs'
      swagger_api :delete_marketplace_brand_pack do
        summary 'It delete the marketplace_brand_pack'
        notes 'delete_marketplace_brand_pack API change the status of marketplace_brand_pack to soft delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :path, :id, :integer, :required, 'marketplace_brand_pack_id to be deleted'
        response :bad_request, 'wrong parameters or marketplace_brand_pack not found or it fails to delete'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a marketplace_brand_pack
      #
      # GET /marketplace_brand_packs/:id
      #
      # Request::
      #   * marketplace_brand_pack_id [integer] id of marketplace_brand_pack of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_marketplace_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_marketplace_brand_pack_api = GET_MARKETPLACE_BRAND_PACK_API.new(deciding_params)
        response = get_marketplace_brand_pack_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :marketplace_brand_pack, 'MarketplaceProductModule APIs'
      swagger_api :get_marketplace_brand_pack do
        summary 'It return a marketplace_brand_pack'
        notes 'get_marketplace_brand_pack API return a single marketplace_brand_pack corresponding to the id'
        param :path, :id, :integer, :required, 'marketplace_brand_pack_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or marketplace_brand_pack not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private 

      def brand_pack_params
        if params[:brand_pack].present? &&
            params[:brand_pack][:images].present? &&
            params[:brand_pack][:images].is_a?(String)

          params[:brand_pack][:images] = JSON.parse(params[:brand_pack][:images])

        end
        params.require(:brand_pack).permit(:sku, :product_id, :mrp, :sku_size, :unit, :brand_pack_code,
                                         :description, :shelf_life, :article_code, :retailer_pack,
                                         :display_name, :display_pack_size, :sub_category_id, images: [:image_id, :priority])
      end

      def marketplace_brand_pack_params
        params.require(:marketplace_brand_pack).permit(
          :id, 
          :brand_pack_id, 
          :seller_brand_pack_id,
          :is_on_sale,
          :is_ladder_pricing_active,
          pricing: [:spat, :margin, :service_tax, :vat, :discount, :buying_price, :selling_price, :savings, :mrp]
        )
      end

      def change_state_params
        params.require(:marketplace_brand_pack).permit(:event).merge({ id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end
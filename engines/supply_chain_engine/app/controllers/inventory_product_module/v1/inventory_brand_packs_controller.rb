#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # InventoryBrandPack controller service class to route panel API calls to the InventoryBrandPackApi
    #
    class InventoryBrandPacksController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_INVENTORY_BRAND_PACK = InventoryProductModule::V1::AddInventoryBrandPackApi
      UPDATE_INVENTORY_BRAND_PACK = InventoryProductModule::V1::UpdateInventoryBrandPackApi
      GET_INVENTORY_BRAND_PACKS = InventoryProductModule::V1::GetInventoryBrandPacksApi
      GET_INVENTORY_BRAND_PACK_API = InventoryProductModule::V1::GetInventoryBrandPackApi
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      CHANGE_INVENTORY_BRAND_PACK_STATE_API = InventoryProductModule::V1::ChangeInventoryBrandPackStateApi
      GET_INVENTORY_BRAND_PACKS_BY_INVENTORY = InventoryProductModule::V1::GetInventoryBrandPacksByInventoryApi

      def initialize
      end

      #
      # function to add inventory_brand_pack
      #
      # post /add_inventory_brand_pack
      #
      # Request:: request object contain
      #   * inventory_brand_pack_params [hash] it contains inventory_id
      #
      # Response::
      #   * If action succeed then returns Success Status code with create inventory_brand_pack
      #    else returns BadRequest response
      #
      def add_inventory_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_inventory_brand_pack_api = ADD_INVENTORY_BRAND_PACK.new(deciding_params)
        response = add_inventory_brand_pack_api.enact(inventory_brand_pack_params)
        send_response(response)
      end

      swagger_controller :inventory_brand_pack, 'SupplyChainModule APIs'
      swagger_api :add_inventory_brand_pack do
        summary 'It creates the inventory_brand_pack with given input'
        notes 'create inventory_brand_pack'
        param :body, :inventory_brand_pack_request, :inventory_brand_pack_request, :required, 'create_inventory_brand_pack request'
        response :bad_request, 'if request params are incorrect or api fails to create inventory_brand_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :inventory_brand_pack_request do
        description 'request schema for /create_category API'
        property :inventory_brand_pack, :inventory_brand_pack_hash, :required, 'inventory_brand_pack model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :inventory_brand_pack_hash do
        description 'details of inventory_brand_pack'
        property :inventory_id, :integer, :required, 'inventory id'
        property :brand_pack_id, :integer, :required, 'master brand pack id'
        property :distributor_pack_code, :string, :optional, 'Distributor prod code'
        property :pricing, :inventory_pricing, :required,
                 'it has details of pricings of Inventory brand pack'
      end

      swagger_model :inventory_pricing do
        description 'details of inventory_brand_pack pricings'
        property :net_landing_price, :double, :required, 'Net landing Price'
        property :margin, :double, :required, 'margin on selling price'
        property :vat, :double, :required, 'vat tax to be applied'
        property :cst, :double, :required, 'CST if applicable'
        property :octrai, :double, :required, 'octrai if applicable'
      end

      #
      # function to update inventory_brand_pack
      #
      # post /update_inventory_brand_pack
      #
      # Request:: request object contain
      #   * inventory_brand_pack_params [hash] it contains inventory_id
      #
      # Response::
      #   * If action succeed then returns Success Status code with update inventory_brand_pack
      #    else returns BadRequest response
      #
      def update_inventory_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_inventory_brand_pack_api = UPDATE_INVENTORY_BRAND_PACK.new(deciding_params)
        response = update_inventory_brand_pack_api.enact(inventory_brand_pack_params.merge({ id: get_id_from_params }))
        send_response(response)
      end

      swagger_controller :inventory_brand_pack, 'SupplyChainModule APIs'
      swagger_api :update_inventory_brand_pack do
        summary 'It updates the inventory_brand_pack with given input'
        notes 'update inventory_brand_pack'
        param :path, :id, :integer, :required, 'inventory_brand_pack to update'
        param :body, :update_inventory_brand_pack_request, :update_inventory_brand_pack_request, :required, 'create_inventory_brand_pack request'
        response :bad_request, 'if request params are incorrect or api fails to create inventory_brand_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :update_inventory_brand_pack_request do
        description 'request schema for /create_category API'
        property :inventory_brand_pack, :inventory_brand_pack_hash, :required, 'inventory_brand_pack model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      #
      # function to get all inventory_brand_packs
      #
      # GET /get_all_inventory_brand_packs
      #
      # Response::
      #   * list of inventory_brand_packs
      #
      def get_all_inventory_brand_packs
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_inventory_brand_packs_api = GET_INVENTORY_BRAND_PACKS.new(deciding_params)
        response = get_inventory_brand_packs_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :inventory_brand_packs, 'SupplyChainModule APIs'
      swagger_api :get_all_inventory_brand_packs do
        summary 'It returns details of all the inventory brand packs'
        notes 'get_all_inventory_brand_packs API returns the details of all the IBP inventory brand pack'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all inventory brand packs'
        param :query, :per_page, :integer, :optional, 'how many inventory brand packs to display in a page'
        param :query, :state, :integer, :optional, 'to fetch inventory brand packs based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end


      #
      # function to change the status of inventory_brand_pack
      #
      # PUT /inventory_brand_pack/state/:id
      #
      # Request::
      #   * inventory_brand_pack_id [integer] id of inventory_brand_pack of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_inventory_brand_pack_state_api = CHANGE_INVENTORY_BRAND_PACK_STATE_API.new(deciding_params)
        response = change_inventory_brand_pack_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :inventory_brand_pack, 'SupplyChainModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of inventory_brand_pack'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the inventory_brand_pack & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'inventory_brand_pack_id to be deactivated/activated'
        param :body, :change_inventory_brand_pack_state_request, :change_inventory_brand_pack_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or inventory_brand_pack not found or it fails to change state'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_inventory_brand_pack_state_request do
        description 'request schema for /change_state API'
        property :inventory_brand_pack, :change_inventory_brand_pack_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_inventory_brand_pack_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      def delete_inventory_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_inventory_brand_pack_state_api = CHANGE_INVENTORY_BRAND_PACK_STATE_API.new(deciding_params)
        response = change_inventory_brand_pack_state_api.enact({ 
                      id: get_id_from_params, 
                      event: COMMON_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :inventory_brand_pack, 'SupplyChainModule APIs'
      swagger_api :delete_inventory_brand_pack do
        summary 'It delete the inventory_brand_pack'
        notes 'delete_inventory_brand_pack API change the status of inventory_brand_pack to soft delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :path, :id, :integer, :required, 'inventory_brand_pack_id to be deleted'
        response :bad_request, 'wrong parameters or inventory_brand_pack not found or it fails to delete'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a inventory_brand_pack
      #
      # GET /inventory_brand_packs/:id
      #
      # Request::
      #   * inventory_brand_pack_id [integer] id of inventory_brand_pack of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_inventory_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_inventory_brand_pack_api = GET_INVENTORY_BRAND_PACK_API.new(deciding_params)
        response = get_inventory_brand_pack_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :inventory_brand_pack, 'SupplyChainModule APIs'
      swagger_api :get_inventory_brand_pack do
        summary 'It return a inventory_brand_pack'
        notes 'get_inventory_brand_pack API return a single inventory_brand_pack corresponding to the id'
        param :path, :id, :integer, :required, 'inventory_brand_pack_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or inventory_brand_pack not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get all inventory_brand_packs of an inventory
      #
      # GET /inventory/:id/inventory_brand_packs
      #
      # Response::
      #   * list of inventory_brand_packs of a particular inventory
      #
      def get_all_inventory_brand_packs_by_inventory
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_inventory_brand_packs_by_inventory_api = GET_INVENTORY_BRAND_PACKS_BY_INVENTORY.new(deciding_params)
        response = get_inventory_brand_packs_by_inventory_api.enact(get_all_inventory_brand_packs_by_inventory_params.merge(id: params[:id]))
        send_response(response)
      end

      swagger_controller :inventory_brand_packs, 'SupplyChainModule APIs'
      swagger_api :get_all_inventory_brand_packs_by_inventory do
        summary 'It returns details of all the inventory brand packs'
        notes 'get_all_inventory_brand_packs API returns the details of all the IBP inventory brand pack'
        param :path, :id, :integer, :required, 'Inventory ID'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all wbps stock details'
        param :query, :per_page, :integer, :optional, 'how many wbps to display in a page'
        param :query, :department_id, :string, :optional, 'to fetch products based on their department'
        param :query, :category_id, :string, :optional, 'to fetch products based on their category'
        param :query, :sub_category_id, :string, :optional, 'to fetch products based on their sub_category'
        param :query, :company_id, :string, :optional, 'to fetch products based on their company'
        param :query, :brand_id, :string, :optional, 'to fetch products based on their brand'
        param :query, :sub_brand_id, :string, :optional, 'to fetch products based on their sub_brand'
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private 

      def inventory_brand_pack_params
        params.require(:inventory_brand_pack).permit(
          :id, 
          :inventory_id, 
          :brand_pack_id,
          :distributor_pack_code,
          pricing: [:net_landing_price, :margin, :vat, :cst, :octrai]
        )
      end

      def change_state_params
        params.require(:inventory_brand_pack).permit(:event).merge({ id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end

      def get_all_inventory_brand_packs_by_inventory_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order, :department_id, :category_id, :sub_category_id,
          :company_id, :brand_id, :sub_brand_id)
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Company controller service class to route panel API calls to the CompanyApi
    #
    class CompanyController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_COMPANY = SupplyChainModule::V1::AddCompanyApi
      UPDATE_COMPANY = SupplyChainModule::V1::UpdateCompanyApi
      GET_COMPANIES = SupplyChainModule::V1::GetCompaniesApi
      GET_COMPANY_API = SupplyChainModule::V1::GetCompanyApi
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      CHANGE_COMPANY_STATE_API = SupplyChainModule::V1::ChangeCompanyStateApi
      GET_COMPANIES_TREE_API = SupplyChainModule::V1::GetCompaniesTreeApi

      def initialize
      end

      #
      # function to add company
      #
      # post /add_company
      #
      # Request:: request object contain
      #   * company_params [hash] it contains name, logo_url, vat_number, tin_number, cst,
      #                    account_details, trade_promotion_margin, data_sharing_margin, incentives,
      #                     reconciliation_period, :legal_document_url
      #
      # Response::
      #   * If action succeed then returns Success Status code with create company
      #    else returns BadRequest response
      #
      def add_company
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_company_api = ADD_COMPANY.new(deciding_params)
        response = add_company_api.enact(company_params)
        send_response(response)
      end

      swagger_controller :company, 'SupplyChainModule APIs'
      swagger_api :add_company do
        summary 'It creates the company with given input'
        notes 'create company'
        param :body, :company_request, :company_request, :required, 'create_company request'
        response :bad_request, 'if request params are incorrect or api fails to create company'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :company_request do
        description 'request schema for /create_category API'
        property :company, :company_hash, :required, 'company model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :company_hash do
        description 'details of company'
        property :name, :string, :required, 'name of company'
        property :logo_url, :string, :optional, 'logo url for company'
        property :vat_number, :string, :optional, 'vat_number'
        property :tin_number, :string, :optional, 'tin_number'
        property :cst, :string, :optional, 'cst'
        property :account_details, :string, :optional, 'account_number'
        property :trade_promotion_margin, :string, :optional, 'trade_promotion_margin'
        property :data_sharing_margin, :string, :optional, 'data_sharing_margin'
        property :incentives, :string, :optional, 'incentives'
        property :reconciliation_period, :string, :optional, 'reconciliation_period'
        property :legal_document_url, :string, :optional, 'legal_document_url'
        property :initials, :string, :required, 'initials of company'
      end

      #
      # function to update company
      #
      # post /update_company
      #
      # Request:: request object contain
      #   * company_params [hash] it contains name, logo_url, vat_number, tin_number, cst,
      #                    account_details, trade_promotion_margin, data_sharing_margin, incentives,
      #                     reconciliation_period, :legal_document_url
      #
      # Response::
      #   * If action succeed then returns Success Status code with update company
      #    else returns BadRequest response
      #
      def update_company
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_company_api = UPDATE_COMPANY.new(deciding_params)
        response = update_company_api.enact(company_params, get_id_from_params)
        send_response(response)
      end

      swagger_controller :company, 'SupplyChainModule APIs'
      swagger_api :update_company do
        summary 'It updates the company with given input'
        notes 'update company'
        param :path, :id, :integer, :required, 'company to update'
        param :body, :update_company_request, :update_company_request, :required, 'create_company request'
        response :bad_request, 'if request params are incorrect or api fails to create company'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :update_company_request do
        description 'request schema for /create_category API'
        property :company, :update_company_hash, :required, 'company model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :update_company_hash do
        description 'details of company'
        property :name, :string, :required, 'name of company'
        property :logo_url, :string, :optional, 'logo url for company'
        property :vat_number, :string, :optional, 'vat_number'
        property :tin_number, :string, :optional, 'tin_number'
        property :cst, :string, :optional, 'cst'
        property :account_details, :string, :optional, 'account_number'
        property :trade_promotion_margin, :string, :optional, 'trade_promotion_margin'
        property :data_sharing_margin, :string, :optional, 'data_sharing_margin'
        property :incentives, :string, :optional, 'incentives'
        property :reconciliation_period, :string, :optional, 'reconciliation_period'
        property :legal_document_url, :string, :optional, 'legal_document_url'
      end

      #
      # function to get all companies
      #
      # GET /get_all_companies
      #
      # Response::
      #   * list of companies
      #
      def get_all_companies
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_companies_api = GET_COMPANIES.new(deciding_params)
        response = get_companies_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :company, 'SupplyChainModule APIs'
      swagger_api :get_all_companies do
        summary 'It returns details of all the companies'
        notes 'get_all_companies API returns the details of all the companies'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all company'
        param :query, :per_page, :integer, :optional, 'how many company to display in a page'
        param :query, :state, :integer, :optional, 'to fetch companies based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end


      #
      # function to change the status of company
      #
      # PUT /company/state/:id
      #
      # Request::
      #   * company_id [integer] id of company of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_company_state_api = CHANGE_COMPANY_STATE_API.new(deciding_params)
        response = change_company_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :company, 'SupplyChainModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of company'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the company & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'company_id to be deactivated/activated'
        param :body, :change_company_state_request, :change_company_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or company not found or it fails to change state'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_company_state_request do
        description 'request schema for /change_state API'
        property :company, :change_company_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_company_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      def delete_company
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_company_state_api = CHANGE_COMPANY_STATE_API.new(deciding_params)
        response = change_company_state_api.enact({ 
                      id: get_id_from_params, 
                      event: COMMON_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :company, 'SupplyChainModule APIs'
      swagger_api :delete_company do
        summary 'It delete the company'
        notes 'delete_company API change the status of company to soft delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :path, :id, :integer, :required, 'company_id to be deleted'
        response :bad_request, 'wrong parameters or company not found or it fails to delete'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a company
      #
      # GET /companies/:id
      #
      # Request::
      #   * company_id [integer] id of company of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_company
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_company_api = GET_COMPANY_API.new(deciding_params)
        response = get_company_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :company, 'SupplyChainModule APIs'
      swagger_api :get_company do
        summary 'It return a company'
        notes 'get_company API return a single company corresponding to the id'
        param :path, :id, :integer, :required, 'company_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or company not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get label of all the companies, brands and sub_brands
      #
      # GET /supply_chain/companies/tree
      #
      # Response::
      #   * sends response with all the companies, brands and sub_brands labels
      #
      def get_companies_tree
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_companies_tree_api = GET_COMPANIES_TREE_API.new(deciding_params)
        response = get_companies_tree_api.enact
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :company, 'SupplyChainModule APIs'
      swagger_api :get_companies_tree do
        summary 'It returns details of all the companies with its tree'
        notes 'get_companies_tree API returns the details of all the companies with its tree'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private 

      def company_params
        params.require(:company).permit(:id, :name, :logo_url, :vat_number, :tin_number, :cst, :account_details,
                                        :trade_promotion_margin, :data_sharing_margin, :incentives,
                                        :reconciliation_period, :legal_document_url, :initials)
      end

      def update_company_params
        params.require(:company).permit(:name, :logo_url, :vat_number, :tin_number, :cst, :account_details,
                                        :trade_promotion_margin, :data_sharing_margin, :incentives,
                                        :reconciliation_period, :legal_document_url)
      end

      def change_state_params
        params.require(:company).permit(:event).merge({ id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end
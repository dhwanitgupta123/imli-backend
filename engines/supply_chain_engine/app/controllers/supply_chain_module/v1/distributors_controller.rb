#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Distributor controller service class to route panel API calls to the DistributorApi
    #
    class DistributorsController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_DISTRIBUTOR = SupplyChainModule::V1::AddDistributorApi
      UPDATE_DISTRIBUTOR = SupplyChainModule::V1::UpdateDistributorApi
      GET_DISTRIBUTORS = SupplyChainModule::V1::GetDistributorsApi
      GET_DISTRIBUTOR_API = SupplyChainModule::V1::GetDistributorApi
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      VENDOR_REGISTRATION = SupplyChainModule::V1::VendorRegistrationApi
      CHANGE_DISTRIBUTOR_STATE_API = SupplyChainModule::V1::ChangeDistributorStateApi

      def initialize
      end

      #
      # function to add distributor
      #
      # post /add_distributor
      #
      # Request:: request object contain
      #   * distributor_params [hash] it contains name
      #
      # Response::
      #   * If action succeed then returns Success Status code with create distributor
      #    else returns BadRequest response
      #
      def add_distributor
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_distributor_api = ADD_DISTRIBUTOR.new(deciding_params)
        response = add_distributor_api.enact(distributor_params)
        send_response(response)
      end

      swagger_controller :distributor, 'SupplyChainModule APIs'
      swagger_api :add_distributor do
        summary 'It creates the distributor with given input'
        notes 'create distributor'
        param :body, :distributor_request, :distributor_request, :required, 'create_distributor request'
        response :bad_request, 'if request params are incorrect or api fails to create distributor'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :distributor_request do
        description 'request schema for /create_category API'
        property :distributor, :distributor_hash, :required, 'distributor model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :distributor_hash do
        description 'details of distributor'
        property :name, :string, :required, 'name of distributor'
        property :cst_no, :string, :required, 'CST no of Distributor'
        property :tin_no, :string, :required, 'TIN no of Distributor'
        property :bank_account_number, :string, :required, 'bank_account_number of distributor'
        property :bank_account_name, :string, :required, 'bank_account_name of distributor'
        property :bank_name, :string, :required, 'name of bank'
        property :branch_name, :string, :required, 'branch name of bank'
        property :ifsc_code, :string, :required, 'IFSC code of bank branch'
        property :contact_person, :string, :required, 'contact_person of distributor'
        property :contact_number, :string, :required, 'contact_number of distributor'
        property :email_ids, :array, :required, 'email ids of contact person', { 'items': { 'type': 'string' } }
        property :company_id, :string, :optional, 'Company of Distributor'
      end

      #
      # function to update distributor
      #
      # post /update_distributor
      #
      # Request:: request object contain
      #   * distributor_params [hash] it contains name
      #
      # Response::
      #   * If action succeed then returns Success Status code with update distributor
      #    else returns BadRequest response
      #
      def update_distributor
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_distributor_api = UPDATE_DISTRIBUTOR.new(deciding_params)
        response = update_distributor_api.enact(distributor_params.merge({ id: get_id_from_params }))
        send_response(response)
      end

      swagger_controller :distributor, 'SupplyChainModule APIs'
      swagger_api :update_distributor do
        summary 'It updates the distributor with given input'
        notes 'update distributor'
        param :path, :id, :integer, :required, 'distributor to update'
        param :body, :update_distributor_request, :update_distributor_request, :required, 'create_distributor request'
        response :bad_request, 'if request params are incorrect or api fails to create distributor'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :update_distributor_request do
        description 'request schema for /create_category API'
        property :distributor, :distributor_hash, :required, 'distributor model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      #
      # function to get all distributors
      #
      # GET /get_all_distributors
      #
      # Response::
      #   * list of distributors
      #
      def get_all_distributors
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_distributors_api = GET_DISTRIBUTORS.new(deciding_params)
        response = get_distributors_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :distributor, 'SupplyChainModule APIs'
      swagger_api :get_all_distributors do
        summary 'It returns details of all the distributors'
        notes 'get_all_distributors API returns the details of all the distributors'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all distributor'
        param :query, :per_page, :integer, :optional, 'how many distributor to display in a page'
        param :query, :state, :integer, :optional, 'to fetch distributors based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to change the status of distributor
      #
      # PUT /distributor/state/:id
      #
      # Request::
      #   * distributor_id [integer] id of distributor of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_distributor_state_api = CHANGE_DISTRIBUTOR_STATE_API.new(deciding_params)
        response = change_distributor_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :distributor, 'SupplyChainModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of distributor'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the distributor & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'distributor_id to be deactivated/activated'
        param :body, :change_distributor_state_request, :change_distributor_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or distributor not found or it fails to change state'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_distributor_state_request do
        description 'request schema for /change_state API'
        property :distributor, :change_distributor_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_distributor_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      def delete_distributor
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_distributor_state_api = CHANGE_DISTRIBUTOR_STATE_API.new(deciding_params)
        response = change_distributor_state_api.enact({ 
                      id: get_id_from_params, 
                      event: COMMON_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :distributor, 'SupplyChainModule APIs'
      swagger_api :delete_distributor do
        summary 'It delete the distributor'
        notes 'delete_distributor API change the status of distributor to soft delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :path, :id, :integer, :required, 'distributor_id to be deleted'
        response :bad_request, 'wrong parameters or distributor not found or it fails to delete'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a distributor
      #
      # GET /distributors/:id
      #
      # Request::
      #   * distributor_id [integer] id of distributor of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_distributor
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_distributor_api = GET_DISTRIBUTOR_API.new(deciding_params)
        response = get_distributor_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :distributor, 'SupplyChainModule APIs'
      swagger_api :get_distributor do
        summary 'It return a distributor'
        notes 'get_distributor API return a single distributor corresponding to the id'
        param :path, :id, :integer, :required, 'distributor_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or distributor not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to register a vendor
      #
      # post /vendor_registration
      #
      # Request:: request object contain
      #   * distributor_params [hash] it contains name, address details
      #
      # Response::
      #   * If action succeed then returns Success Status code with create distributor
      #    else returns BadRequest response
      #
      def vendor_registration
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        vendor_registration_api = VENDOR_REGISTRATION.new(deciding_params)
        response = vendor_registration_api.enact(vendor_registration_params)
        send_response(response)
      end

      swagger_controller :distributor, 'SupplyChainModule APIs'
      swagger_api :vendor_registration do
        summary 'It creates the distributor with given input'
        notes 'create distributor, inventory add address'
        param :body, :vendor_request, :vendor_registration_req, :required, 'vendor_registration request'
        response :bad_request, 'if request params are incorrect or api fails to create distributor'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :vendor_registration_req do
        description 'request schema for /vendor_registration API'
        property :distributor, :vendor_hash, :required, 'distributor model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :vendor_hash do
        description 'details of distributor'
        property :name, :string, :required, 'name of distributor'
        property :cst_no, :string, :required, 'CST no of Distributor'
        property :tin_no, :string, :required, 'TIN no of Distributor'
        property :bank_account_number, :string, :required, 'bank_account_number of distributor'
        property :bank_account_name, :string, :required, 'bank_account_name of distributor'
        property :bank_name, :string, :required, 'name of bank'
        property :branch_name, :string, :required, 'branch name of bank'
        property :ifsc_code, :string, :required, 'IFSC code of bank branch'
        property :contact_person, :string, :required, 'contact_person of distributor'
        property :contact_number, :string, :required, 'contact_number of distributor'
        property :email_ids, :array, :required, 'email ids of contact person', { 'items': { 'type': 'string' } }
        property :company_id, :string, :optional, 'Company of Distributor'
        property :inventory, :inventory_addr_req, :required, 'address hash of inventory'
      end

      swagger_model :inventory_addr_req do
        description 'request schema for /vendor_registration API'
        property :address, :inventory_addr_hash, :required, 'address model hash'
      end

      swagger_model :inventory_addr_hash do
        description 'Address hash with address related details of user'
        property :inventory_name, :string, :optional, 'Inventory name'
        property :address_line1, :string, :optional, 'inventory address'
        property :address_line2, :string, :optional, 'inventory Locality Name'
        property :landmark, :string, :optional, 'Address landmark'
        property :area_id, :string, :required, 'Area id of the area, fetched from pincode'
      end

      private 

      def distributor_params
        if params[:distributor].present? && 
            params[:distributor][:email_ids].present? &&
              params[:distributor][:email_ids].is_a?(String)
          params[:distributor][:email_ids] = JSON.parse(params[:distributor][:email_ids])
        end
        params.require(:distributor).permit(:id, :name, :cst_no, :tin_no, :company_id, :bank_account_name,
          :bank_account_number, :contact_number, :contact_person, :bank_name, :ifsc_code, :branch_name, email_ids: [])
      end

      def change_state_params
        params.require(:distributor).permit(:event).merge({ id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def vendor_registration_params
        if params[:distributor].present? && 
            params[:distributor][:email_ids].present? &&
              params[:distributor][:email_ids].is_a?(String)
          params[:distributor][:email_ids] = JSON.parse(params[:distributor][:email_ids])
        end
        params.require(:distributor).permit(:id, :name, :cst_no, :tin_no, :company_id,:bank_account_name,
          :bank_account_number, :contact_number, :contact_person, :bank_name, :ifsc_code, :branch_name,
          email_ids: [], inventory:[address: [:inventory_name, :address_line1, :address_line2, :landmark, :area_id]])
      end

      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end

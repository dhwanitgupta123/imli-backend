#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Variant Controller to route Panel APi call variant APIs
    #
    class VariantsController < BaseModule::V1::ApplicationController
      #
      # initialize API Classes
      #
      ADD_VARIANT = MasterProductModule::V1::AddVariantApi
      UPDATE_VARIANT = MasterProductModule::V1::UpdateVariantApi
      GET_VARIANT = MasterProductModule::V1::GetVariantsApi
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper

      def initialize

      end

      #
      # function to add variant
      #
      # post /variants/new
      #
      # Request:: request object contain
      #   * variant_params [hash] it contains variant, initials
      #
      # Response::
      #   * If action succeed then returns Success Status code with create variant
      #    else returns BadRequest response
      #
      def add_variant
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_variant_api = ADD_VARIANT.new(deciding_params)
        response = add_variant_api.enact(add_variant_params)
        send_response(response)
      end

      swagger_controller :variants, 'MasterProductModule APIs'
      swagger_api :add_variant do
        summary 'It creates the variant with given input'
        notes 'create variant'
        param :body, :variant_request, :variant_request, :required, 'create_variant request'
        response :bad_request, 'if request params are incorrect or api fails to create product'
      end

      swagger_model :variant_request do
        description 'request schema for /create_variant API'
        property :variant, :variant_hash, :required, 'variant model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :variant_hash do
        description 'details of variant'
        property :name, :string, :required, 'variant of product'
        property :initials, :string, :required, 'initials of variant'
      end

      #
      # function to update variant
      #
      # PUT /variants/:id
      #
      # Request:: request object contain
      #   * variant_params [hash] it contains variant, initials
      #
      # Response::
      #   * If action succeed then returns Success Status code with update variant
      #    else returns BadRequest response
      #
      def update_variant
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_variant_api = UPDATE_VARIANT.new(deciding_params)
        response = update_variant_api.enact(update_variant_params)
        send_response(response)
      end

      swagger_controller :variants, 'MasterProductModule APIs'
      swagger_api :update_variant do
        summary 'It updates the variant with given input'
        notes 'update variant'
        param :path, :id, :integer, :required, 'variant to update'
        param :body, :update_variant_request, :update_variant_request, :required, 'create_variant request'
        response :bad_request, 'if request params are incorrect or api fails to create variant'
      end

      swagger_model :update_variant_request do
        description 'request schema for /create_category API'
        property :variant, :variant_hash, :required, 'variant model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      #
      # function to get all variants
      #
      # GET /variants
      #
      # Response::
      #   * list of variants
      #
      def get_all_variants
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_variants_api = GET_VARIANT.new(deciding_params)
        response = get_variants_api.enact(params)
        send_response(response)
      end

      swagger_controller :variants, 'MasterProductModule APIs'
      swagger_api :get_all_variants do
        param :query, :session_token, :string, :required, 'to authenticate user'
        summary 'It returns details of all the variants'
        notes 'API returns the details of all the variants'
      end

      private 

      #
      # White params for add variant api
      #
      def add_variant_params
        params.require(:variant).permit(:name, :initials)
      end

      #
      # white params to update variant
      #
      def update_variant_params
        params.require(:variant).permit(:name, :initials).merge({ id: params[:id] })
      end
    end
  end
end

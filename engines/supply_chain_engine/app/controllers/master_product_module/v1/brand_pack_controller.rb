#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # BrandPack controller service class to route panel API calls to the BrandPackApi
    #
    class BrandPackController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_BRAND_PACK = MasterProductModule::V1::AddBrandPackApi
      ADD_BRAND_PACK_MPBP_API = MasterProductModule::V1::AddBrandPackAndMpbpApi
      UPDATE_BRAND_PACK_MPBP_API = MasterProductModule::V1::UpdateBrandPackAndMpbpApi
      UPDATE_BRAND_PACK = MasterProductModule::V1::UpdateBrandPackApi
      GET_BRAND_PACKS = MasterProductModule::V1::GetBrandPacksApi
      GET_BRAND_PACK_API = MasterProductModule::V1::GetBrandPackApi
      CHANGE_BRAND_PACK_STATE_API = MasterProductModule::V1::ChangeBrandPackStateApi
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      def initialize
      end

      #
      # function to add brand_pack
      #
      # post /add_brand_pack
      #
      # Request:: request object contain
      #   * brand_pack_params [hash] it contains sku, images, product_id, mrp,
      #                       sku_size, unit, description, shelf_life
      #
      # Response::
      #   * If action succeed then returns Success Status code with create brand_pack
      #    else returns BadRequest response
      #
      def add_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_brand_pack_api = ADD_BRAND_PACK.new(deciding_params)
        response = add_brand_pack_api.enact(brand_pack_params)
        send_response(response)
      end

      swagger_controller :brand_pack, 'MasterProductModule APIs'
      swagger_api :add_brand_pack do
        summary 'It creates the brand_pack with given input'
        notes 'create brand_pack'
        param :body, :brand_pack_request, :brand_pack_request, :required, 'create_brand_pack request'
        response :bad_request, 'if request params are incorrect or api fails to create brand_pack'
      end

      swagger_model :brand_pack_request do
        description 'request schema for /create_category API'
        property :brand_pack, :brand_pack_hash, :required, 'brand_pack model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :brand_pack_hash do
        description 'details of brand_pack'
        property :sku, :string, :required, 'name of brand_pack'
        property :images, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :image_hash_array } }        
        property :mrp, :string, :optional, 'mrp'
        property :sku_size, :string, :optional, 'sku size'
        property :unit, :string, :optional, 'unit'
        property :description, :string, :optional, 'description'
        property :shelf_life, :string, :optional, 'shelf_life'
        property :retailer_pack, :string, :optional, 'retailer pack'
        property :article_code, :string, :optional, 'unique code for brand pack'
        property :product_id, :string, :required, 'product'
        property :sub_category_id, :string, :required, 'sub category'
      end


      #
      # function to update brand_pack
      #
      # post /update_brand_pack
      #
      # Request:: request object contain
      #   * brand_pack_params [hash] it contains sku, images, product_id, mrp,
      #                       sku_size, unit, description, shelf_life
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated brand_pack
      #    else returns BadRequest response
      #
      def update_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_brand_pack_api = UPDATE_BRAND_PACK.new(deciding_params)
        response = update_brand_pack_api.enact(brand_pack_params, get_id_from_params)
        send_response(response)
      end

      swagger_controller :brand_pack, 'MasterProductModule APIs'
      swagger_api :update_brand_pack do
        summary 'It updates the brand_pack with given input'
        notes 'update brand_pack'
        param :path, :id, :integer, :required, 'brand pack to update'
        param :body, :update_brand_pack_request, :update_brand_pack_request, :required, 'create_brand_pack request'
        response :bad_request, 'if request params are incorrect or api fails to create brand_pack'
      end

      swagger_model :update_brand_pack_request do
        description 'request schema for /create_category API'
        property :brand_pack, :update_brand_pack_hash, :required, 'brand_pack model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :update_brand_pack_hash do
        description 'details of brand_pack'
        property :sku, :string, :required, 'name of brand_pack'
        property :images, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :image_hash_array } }
        property :mrp, :float, :optional, 'mrp'
        property :sku_size, :string, :optional, 'sku size'
        property :unit, :string, :optional, 'unit'
        property :description, :string, :optional, 'description'
        property :shelf_life, :string, :optional, 'shelf_life'
        property :product_id, :integer, :required, 'product'
      end

      swagger_model :image_hash_array do
        description 'details of image id and corresponding priority'
        property :image_id, :integer, :required, 'reference to image'
        property :priority, :integer, :required, 'priority of image'
      end
      #
      # function to get all brand_packs
      #
      # GET /get_all_brand_packs
      #
      # Response::
      #   * list of brand_packs
      #
      def get_all_brand_packs
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_brand_packs_api = GET_BRAND_PACKS.new(deciding_params)
        response = get_brand_packs_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :brand_pack, 'SupplyChainModule APIs'
      swagger_api :get_all_brand_packs do
        summary 'It returns details of all the brand packs'
        notes 'get_all_brand_packs API returns the details of all the brand packs'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all brand packs'
        param :query, :per_page, :integer, :optional, 'how many brand packs to display in a page'
        param :query, :state, :integer, :optional, 'to fetch brand packs based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        param :query, :department_id, :string, :optional, 'to fetch products based on their department'
        param :query, :category_id, :string, :optional, 'to fetch products based on their category'
        param :query, :sub_category_id, :string, :optional, 'to fetch products based on their sub_category'
        param :query, :company_id, :string, :optional, 'to fetch products based on their company'
        param :query, :brand_id, :string, :optional, 'to fetch products based on their brand'
        param :query, :sub_brand_id, :string, :optional, 'to fetch products based on their sub_brand'
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to change the status of brand_pack
      #
      # PUT /brand_pack/state/:id
      #
      # Request::
      #   * brand_pack_id [integer] id of brand_pack of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_brand_pack_state_api = CHANGE_BRAND_PACK_STATE_API.new(deciding_params)
        response = change_brand_pack_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :brand_pack, 'SupplyChainModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of brand_pack'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the brand_pack & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'brand_pack_id to be deactivated/activated'
        param :body, :change_brand_pack_state_request, :change_brand_pack_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or brand_pack not found or it fails to change state'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_brand_pack_state_request do
        description 'request schema for /change_state API'
        property :brand_pack, :change_brand_pack_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_brand_pack_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      #
      # function to delete brand_pack
      #
      # DELETE /brand_pack/state/:id
      #
      # Request::
      #   * brand_pack_id [integer] id of brand_pack of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def delete_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_brand_pack_state_api = CHANGE_BRAND_PACK_STATE_API.new(deciding_params)
        response = change_brand_pack_state_api.enact({ 
                      id: get_id_from_params,
                      event: COMMON_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :brand_pack, 'SupplyChainModule APIs'
      swagger_api :delete_brand_pack do
        summary 'It delete the brand_pack'
        notes 'delete_brand_pack API change the status of brand_pack to soft delete'
        param :path, :id, :integer, :required, 'brand_pack_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or brand_pack not found or it fails to delete'
      end

      #
      # function to get a brand_pack
      #
      # GET /brand_packs/:id
      #
      # Request::
      #   * brand_pack_id [integer] id of brand_pack of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_brand_pack_api = GET_BRAND_PACK_API.new(deciding_params)
        response = get_brand_pack_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :brand_pack, 'SupplyChainModule APIs'
      swagger_api :get_brand_pack do
        summary 'It return a brand_pack'
        notes 'get_company API return a single brand_pack corresponding to the id'
        param :path, :id, :integer, :required, 'brand_pack_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or brand_pack not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to add brand_pack, IBP, WBP, SBP, MPBP
      #
      # post /brand_packs/marketplace_brand_packs/new
      #
      # Request:: request object contain
      #   * brand_pack_params [hash] it contains sku, images, product_id, mrp,
      #                       sku_size, unit, description, shelf_life hash of IBPs
      #
      # Response::
      #   * If action succeed then returns Success Status code with create brand_pack
      #    else returns BadRequest response
      #
      def add_brand_pack_to_mpbp
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_brand_pack_to_mpbp_api = ADD_BRAND_PACK_MPBP_API.new(deciding_params)
        response = add_brand_pack_to_mpbp_api.enact(add_brand_pack_to_mpbp_params)
        send_response(response)
      end

      swagger_controller :brand_pack, 'MasterProductModule APIs'
      swagger_api :add_brand_pack_to_mpbp do
        summary 'It creates the brand_pack, ibp, wbp, sbp & mpbp with given input'
        notes 'create brand_pack ibp, wbp, sbp, mpbp'
        param :body, :brand_pack_to_mpbp_request, :brand_pack_to_mpbp_request, :required, 'create_brand_pack request'
        response :bad_request, 'if request params are incorrect or api fails to create brand_pack'
      end

      swagger_model :brand_pack_to_mpbp_request do
        description 'request schema for /create_category API'
        property :brand_pack, :bp_hash, :required, 'brand_pack model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :bp_hash do
        description 'details of brand_pack'
        property :sku, :string, :required, 'name of brand_pack'
        property :images, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :image_hash_array } }        
        property :mrp, :string, :optional, 'mrp'
        property :sku_size, :string, :optional, 'sku size'
        property :unit, :string, :optional, 'unit'
        property :description, :string, :optional, 'description'
        property :shelf_life, :string, :optional, 'shelf_life'
        property :retailer_pack, :string, :optional, 'retailer pack'
        property :article_code, :string, :optional, 'unique code for brand pack'
        property :product_id, :string, :required, 'product'
        property :sub_category_id, :string, :required, 'sub category'
        property :inventory_brand_packs, :array, :required, 'inventory_brand_pack model hash',
                 { 'items': { 'type': :ibp_hash } }
        property :warehouse_brand_packs, :array, :required, 'wbp stock details',
                 { 'items': { 'type': :wbp_hash } }
      end

      swagger_model :ibp_hash do
        description 'details of inventory_brand_pack'
        property :inventory_id, :string, :required, 'inventory id'
        property :distributor_pack_code, :string, :optional, 'Distributor prod code'
        property :pricing, :inventory_pricing_hash, :required,
                 'it has details of pricings of Inventory brand pack'
      end

      swagger_model :inventory_pricing_hash do
        description 'details of inventory_brand_pack pricings'
        property :net_landing_price, :string, :required, 'Net landing Price'
        property :margin, :string, :required, 'margin on selling price'
        property :vat, :string, :required, 'vat tax to be applied'
        property :cst, :string, :required, 'CST if applicable'
        property :octrai, :string, :required, 'octrai if applicable'
      end

      swagger_model :wbp_hash do
        description 'details of warehouse_brand_pack, threshold stock & freq'
        property :threshold_stock_value, :string, :required, 'threshold_stock_value'
        property :outbound_frequency_period_in_days, :string, :optional, 'outbound_frequency_period_in_days'
      end

      #
      # function to update brand_pack, IBP, WBP, SBP, MPBP
      #
      # put /brand_packs/marketplace_brand_packs/:id
      #
      # Request:: request object contain
      #   * brand_pack_params [hash] it contains sku, images, product_id, mrp,
      #                       sku_size, unit, description, shelf_life, hash of IBPs
      #
      # Response::
      #   * If action succeed then returns Success Status code with create brand_pack
      #    else returns BadRequest response
      #
      def update_brand_pack_to_mpbp
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_brand_pack_to_mpbp_api = UPDATE_BRAND_PACK_MPBP_API.new(deciding_params)
        response = update_brand_pack_to_mpbp_api.enact(update_brand_pack_to_mpbp_params.merge(id: params[:id]))
        send_response(response)
      end

      swagger_controller :brand_pack, 'MasterProductModule APIs'
      swagger_api :update_brand_pack_to_mpbp do
        summary 'It updates the brand_pack, ibp, wbp, sbp & mpbp with given input'
        notes 'update brand_pack ibp, wbp, sbp, mpbp'
        param :path, :id, :integer, :required, 'brand pack to update'
        param :body, :brand_pack_to_mpbp_update_request, :brand_pack_to_mpbp_update_request, :required, 'create_brand_pack request'
        response :bad_request, 'if request params are incorrect or api fails to create brand_pack'
      end

      swagger_model :brand_pack_to_mpbp_update_request do
        description 'request schema for /create_category API'
        property :brand_pack, :bp_update_hash, :required, 'brand_pack model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :bp_update_hash do
        description 'details of brand_pack'
        property :sku, :string, :required, 'name of brand_pack'
        property :images, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :image_hash_array } }        
        property :mrp, :string, :optional, 'mrp'
        property :sku_size, :string, :optional, 'sku size'
        property :unit, :string, :optional, 'unit'
        property :description, :string, :optional, 'description'
        property :shelf_life, :string, :optional, 'shelf_life'
        property :retailer_pack, :string, :optional, 'retailer pack'
        property :article_code, :string, :optional, 'unique code for brand pack'
        property :product_id, :string, :required, 'product'
        property :sub_category_id, :string, :required, 'sub category'
        property :inventory_brand_packs, :array, :required, 'inventory_brand_pack model hash',
                 { 'items': { 'type': :update_ibp_hash } }
        property :warehouse_brand_packs, :array, :required, 'wbp stock details',
                 { 'items': { 'type': :wbp_hash } }
      end

      swagger_model :update_ibp_hash do
        description 'details of inventory_brand_pack'
        property :id, :string, :required, 'IBP id'
        property :inventory_id, :string, :required, 'inventory id'
        property :distributor_pack_code, :string, :optional, 'Distributor prod code'
        property :pricing, :inventory_pricing_hash, :required,
                 'it has details of pricings of Inventory brand pack'
      end

      private

      def brand_pack_params
        if params[:brand_pack].present? &&
            params[:brand_pack][:images].present? &&
            params[:brand_pack][:images].is_a?(String)

          params[:brand_pack][:images] = JSON.parse(params[:brand_pack][:images])

        end
        params.require(:brand_pack).permit(:sku, :product_id, :mrp, :sku_size, :unit,
                                         :description, :shelf_life, :article_code, :retailer_pack,
                                         :display_name, :display_pack_size, :sub_category_id, images: [:image_id, :priority])
      end

      def add_brand_pack_to_mpbp_params
        if params[:brand_pack].present? &&
            params[:brand_pack][:images].present? &&
            params[:brand_pack][:images].is_a?(String)
          params[:brand_pack][:images] = JSON.parse(params[:brand_pack][:images])
        end
        if params[:brand_pack].present? &&
            params[:brand_pack][:inventory_brand_packs].present? &&
            params[:brand_pack][:inventory_brand_packs].is_a?(String)
          params[:brand_pack][:inventory_brand_packs] = JSON.parse(params[:brand_pack][:inventory_brand_packs])
        end
        if params[:brand_pack].present? &&
            params[:brand_pack][:warehouse_brand_packs].present? &&
            params[:brand_pack][:warehouse_brand_packs].is_a?(String)
          params[:brand_pack][:warehouse_brand_packs] = JSON.parse(params[:brand_pack][:warehouse_brand_packs])
        end
        params.require(:brand_pack).permit(:sku, :product_id, :mrp, :sku_size, :unit,
                                         :description, :shelf_life, :article_code, :retailer_pack,
                                         :display_name, :display_pack_size, :sub_category_id, images: [:image_id, :priority],
                                         inventory_brand_packs: [:inventory_id, :distributor_pack_code,
                                         pricing: [:net_landing_price, :margin, :vat, :cst, :octrai]],
                                         warehouse_brand_packs: [:threshold_stock_value, :outbound_frequency_period_in_days])
      end

      def update_brand_pack_to_mpbp_params
        if params[:brand_pack].present? &&
            params[:brand_pack][:images].present? &&
            params[:brand_pack][:images].is_a?(String)
          params[:brand_pack][:images] = JSON.parse(params[:brand_pack][:images])
        end
        if params[:brand_pack].present? &&
            params[:brand_pack][:inventory_brand_packs].present? &&
            params[:brand_pack][:inventory_brand_packs].is_a?(String)
          params[:brand_pack][:inventory_brand_packs] = JSON.parse(params[:brand_pack][:inventory_brand_packs])
        end
        if params[:brand_pack].present? &&
            params[:brand_pack][:warehouse_brand_packs].present? &&
            params[:brand_pack][:warehouse_brand_packs].is_a?(String)
          params[:brand_pack][:warehouse_brand_packs] = JSON.parse(params[:brand_pack][:warehouse_brand_packs])
        end
        params.require(:brand_pack).permit(:sku, :product_id, :mrp, :sku_size, :unit,
                                         :description, :shelf_life, :article_code, :retailer_pack,
                                         :display_name, :display_pack_size, :sub_category_id, images: [:image_id, :priority],
                                         inventory_brand_packs: [:id, :inventory_id, :distributor_pack_code,
                                         pricing: [:net_landing_price, :margin, :vat, :cst, :octrai]],
                                         warehouse_brand_packs: [:threshold_stock_value, :outbound_frequency_period_in_days])
      end

      def change_state_params
        params.require(:brand_pack).permit(:event).merge({ id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order, :department_id, :category_id, :sub_category_id,
          :company_id, :brand_id, :sub_brand_id)
      end
    end
  end
end

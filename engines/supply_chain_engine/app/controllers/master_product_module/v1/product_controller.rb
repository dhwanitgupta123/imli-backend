#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Product controller service class to route panel API calls to the ProductApi
    #
    class ProductController < BaseModule::V1::ApplicationController
      #
      # initialize API Classes
      #
      ADD_PRODUCT = MasterProductModule::V1::AddProductApi
      UPDATE_PRODUCT = MasterProductModule::V1::UpdateProductApi
      GET_PRODUCTS = MasterProductModule::V1::GetProductsApi
      GET_PRODUCT_API = MasterProductModule::V1::GetProductApi
      CHANGE_PRODUCT_STATE_API = MasterProductModule::V1::ChangeProductStateApi
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      def initialize
      end

      #
      # function to add product
      #
      # post /add_product
      #
      # Request:: request object contain
      #   * product_params [hash] it contains name, logo_url, sub_brand_id
      #
      # Response::
      #   * If action succeed then returns Success Status code with create product
      #    else returns BadRequest response
      #
      def add_product
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_product_api = ADD_PRODUCT.new(deciding_params)
        response = add_product_api.enact(product_params)
        send_response(response)
      end

      swagger_controller :product, 'MasterProductModule APIs'
      swagger_api :add_product do
        summary 'It creates the product with given input'
        notes 'create product'
        param :body, :product_request, :product_request, :required, 'create_product request'
        response :bad_request, 'if request params are incorrect or api fails to create product'
      end

      swagger_model :product_request do
        description 'request schema for /create_category API'
        property :product, :product_hash, :required, 'product model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :product_hash do
        description 'details of product'
        property :name, :string, :required, 'name of product'
        property :logo_url, :string, :optional, 'logo url for product'
        property :sub_brand_id, :integer, :required, 'sub_brand'
        property :initials, :string, :required, 'initials of product'
        property :variant_id, :integer, :optional, 'ID of variant'
      end

      #
      # function to update product
      #
      # post /update_product
      #
      # Request:: request object contain
      #   * product_params [hash] it contains name, logo_url, sub_brand_id
      #
      # Response::
      #   * If action succeed then returns Success Status code with update product
      #    else returns BadRequest response
      #
      def update_product
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_product_api = UPDATE_PRODUCT.new(deciding_params)
        response = update_product_api.enact(product_params, get_id_from_params)
        send_response(response)
      end

      swagger_controller :product, 'MasterProductModule APIs'
      swagger_api :update_product do
        summary 'It updates the product with given input'
        notes 'update product'
        param :path, :id, :integer, :required, 'product to update'
        param :body, :update_product_request, :update_product_request, :required, 'create_product request'
        response :bad_request, 'if request params are incorrect or api fails to create product'
      end

      swagger_model :update_product_request do
        description 'request schema for /update_product API'
        property :product, :update_product_hash, :required, 'product model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :update_product_hash do
        description 'details of product'
        property :name, :string, :required, 'name of product'
        property :logo_url, :string, :optional, 'logo url for product'
        property :sub_brand_id, :integer, :optional, 'sub_brand'
        property :variant_id, :integer, :optional, 'ID of variant'
      end

      #
      # function to get all products
      #
      # GET /get_all_products
      #
      # Response::
      #   * list of products
      #
      def get_all_products
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_products_api = GET_PRODUCTS.new(deciding_params)
        response = get_products_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :product, 'SupplyChainModule APIs'
      swagger_api :get_all_products do
        summary 'It returns details of all the products'
        notes 'get_all_products API returns the details of all the products'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all product'
        param :query, :per_page, :integer, :optional, 'how many product to display in a page'
        param :query, :state, :integer, :optional, 'to fetch products based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        param :query, :company_id, :string, :optional, 'to fetch products based on their company'
        param :query, :brand_id, :string, :optional, 'to fetch products based on their brand'
        param :query, :sub_brand_id, :string, :optional, 'to fetch products based on their sub_brand'
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to change the status of product
      #
      # PUT /product/state/:id
      #
      # Request::
      #   * product_id [integer] id of product of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_product_state_api = CHANGE_PRODUCT_STATE_API.new(deciding_params)
        response = change_product_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :product, 'SupplyChainModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of product'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the product & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'product_id to be deactivated/activated'
        param :body, :change_product_state_request, :change_product_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or product not found or it fails to change state'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_product_state_request do
        description 'request schema for /change_state API'
        property :product, :change_product_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_product_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      #
      # function to delete product
      #
      # DELETE /product/state/:id
      #
      # Request::
      #   * product_id [integer] id of product of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def delete_product
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_product_state_api = CHANGE_PRODUCT_STATE_API.new(deciding_params)
        response = change_product_state_api.enact({ 
                      id: get_id_from_params, 
                      event: COMMON_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :product, 'SupplyChainModule APIs'
      swagger_api :delete_product do
        summary 'It delete the product'
        notes 'delete_product API change the status of product to soft delete'
        param :path, :id, :integer, :required, 'product_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or product not found or it fails to delete'
      end

      #
      # function to get a product
      #
      # GET /products/:id
      #
      # Request::
      #   * product_id [integer] id of product of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_product
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_product_api = GET_PRODUCT_API.new(deciding_params)
        response = get_product_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :product, 'SupplyChainModule APIs'
      swagger_api :get_product do
        summary 'It return a product'
        notes 'get_company API return a single product corresponding to the id'
        param :path, :id, :integer, :required, 'product_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or product not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private 

      def product_params
        params.require(:product).permit(:name, :logo_url, :sub_brand_id, :initials, :variant_id)
      end

      def update_product_params
        params.require(:product).permit(:name, :logo_url, :sub_brand_id, :variant_id)
      end

      def change_state_params
        params.require(:product).permit(:event).merge({ id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order, :company_id, :brand_id, :sub_brand_id)
      end
    end
  end
end
#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # BrandController controller service class to route panel API calls to the BrandApi
    #
    class BrandController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_BRAND = MasterProductModule::V1::AddBrandApi
      UPDATE_BRAND = MasterProductModule::V1::UpdateBrandApi
      GET_BRANDS = MasterProductModule::V1::GetBrandsApi
      GET_BRAND_API = MasterProductModule::V1::GetBrandApi
      CHANGE_BRAND_STATE_API = MasterProductModule::V1::ChangeBrandStateApi
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      def initialize
      end

      #
      # function to add brand
      #
      # post /add_brand
      #
      # Request:: request object contain
      #   * brand_params [hash] it contains name, logo_url, code and brand_id
      #
      # Response::
      #   * If action succeed then returns Success Status code with create brand
      #    else returns BadRequest response
      #
      def add_brand
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_brand_api = ADD_BRAND.new(deciding_params)
        response = add_brand_api.enact(brand_params)
        send_response(response)
      end

      swagger_controller :brand, 'MasterProductModule APIs'
      swagger_api :add_brand do
        summary 'It creates the brand with given input'
        notes 'create brand'
        param :body, :brand_request, :brand_request, :required, 'create_brand request'
        response :bad_request, 'if request params are incorrect or api fails to create brand'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :brand_request do
        description 'request schema for /create_category API'
        property :brand, :brand_hash, :required, 'brand model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :brand_hash do
        description 'details of brand'
        property :name, :string, :required, 'name of brand'
        property :logo_url, :string, :optional, 'logo url for brand'
        property :code, :string, :optional, 'code for brand'
        property :company_id, :integer, :required, 'company'
        property :initials, :string, :required, 'initials of brand'
      end

      #
      # function to update brand
      #
      # post /update_brand
      #
      # Request:: request object contain
      #   * brand_params [hash] it may contain name, logo_url, code and brand_id
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated brand
      #    else returns BadRequest response
      #
      def update_brand
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_brand_api = UPDATE_BRAND.new(deciding_params)
        response = update_brand_api.enact(brand_params, get_id_from_params)
        send_response(response)
      end

      swagger_controller :brand, 'MasterProductModule APIs'
      swagger_api :update_brand do
        summary 'It updates the brand with given input'
        notes 'update brand'
        param :path, :id, :integer, :required, 'brand to update'
        param :body, :update_brand_request, :update_brand_request, :required, 'create_brand request'
        response :bad_request, 'if request params are incorrect or api fails to create brand'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :update_brand_request do
        description 'request schema for /create_category API'
        property :brand, :update_brand_hash, :required, 'brand model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :update_brand_hash do
        description 'details of brand'
        property :name, :string, :required, 'name of brand'
        property :logo_url, :string, :optional, 'logo url for brand'
        property :code, :string, :optional, 'code for brand'
        property :company_id, :integer, :required, 'company'
      end

      #
      # function to get all brands
      #
      # GET /get_all_brands
      #
      # Response::
      #   * list of brands
      #
      def get_all_brands
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_brands_api = GET_BRANDS.new(deciding_params)
        response = get_brands_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :brand, 'SupplyChainModule APIs'
      swagger_api :get_all_brands do
        summary 'It returns details of all the brands'
        notes 'get_all_brands API returns the details of all the brands'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all brand'
        param :query, :per_page, :integer, :optional, 'how many brand to display in a page'
        param :query, :state, :integer, :optional, 'to fetch brands based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to change the status of brand
      #
      # PUT /brand/state/:id
      #
      # Request::
      #   * brand_id [integer] id of brand of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_brand_state_api = CHANGE_BRAND_STATE_API.new(deciding_params)
        response = change_brand_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :brand, 'SupplyChainModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of brand'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the brand & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'brand_id to be deactivated/activated'
        param :body, :change_brand_state_request, :change_brand_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or brand not found or it fails to change state'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_brand_state_request do
        description 'request schema for /change_state API'
        property :brand, :change_brand_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_brand_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      #
      # function to delete brand
      #
      # DELETE /brand/state/:id
      #
      # Request::
      #   * brand_id [integer] id of brand of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def delete_brand
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_brand_state_api = CHANGE_BRAND_STATE_API.new(deciding_params)
        response = change_brand_state_api.enact({ 
                      id: get_id_from_params, 
                      event: COMMON_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :brand, 'SupplyChainModule APIs'
      swagger_api :delete_brand do
        summary 'It delete the brand'
        notes 'delete_brand API change the status of brand to soft delete'
        param :path, :id, :integer, :required, 'brand_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or brand not found or it fails to delete'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a brand
      #
      # GET /brands/:id
      #
      # Request::
      #   * brand_id [integer] id of brand of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_brand
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_brand_api = GET_BRAND_API.new(deciding_params)
        response = get_brand_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :brand, 'SupplyChainModule APIs'
      swagger_api :get_brand do
        summary 'It return a brand'
        notes 'get_company API return a single brand corresponding to the id'
        param :path, :id, :integer, :required, 'brand_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or brand not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private

      def brand_params
        params.require(:brand).permit(:name, :code, :logo_url, :company_id, :initials)
      end

      def update_brand_params
        params.require(:brand).permit(:name, :code, :logo_url, :company_id)
      end

      def change_state_params
        params.require(:brand).permit(:event).merge({ id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # SubBrand controller service class to route panel API calls to the SSubBrandApi
    #
    class SubBrandController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_SUB_BRAND = MasterProductModule::V1::AddSubBrandApi
      UPDATE_SUB_BRAND = MasterProductModule::V1::UpdateSubBrandApi
      GET_SUB_BRANDS = MasterProductModule::V1::GetSubBrandsApi
      GET_SUB_BRAND_API = MasterProductModule::V1::GetSubBrandApi
      CHANGE_SUB_BRAND_STATE_API = MasterProductModule::V1::ChangeSubBrandStateApi
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      def initialize
      end

      #
      # function to add sub_brand
      #
      # post /add_sub_brand
      #
      # Request:: request object contain
      #   * sub_brand_params [hash] it contains name, logo_url, brand_id
      #
      # Response::
      #   * If action succeed then returns Success Status code with create sub_brand
      #    else returns BadRequest response
      #
      def add_sub_brand
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_sub_brand_api = ADD_SUB_BRAND.new(deciding_params)
        response = add_sub_brand_api.enact(sub_brand_params)
        send_response(response)
      end

      swagger_controller :sub_brand, 'MasterProductModule APIs'
      swagger_api :add_sub_brand do
        summary 'It creates the sub_brand with given input'
        notes 'create sub_brand'
        param :body, :sub_brand_request, :sub_brand_request, :required, 'create_sub_brand request'
        response :bad_request, 'if request params are incorrect or api fails to create sub_brand'
      end

      swagger_model :sub_brand_request do
        description 'request schema for /create_category API'
        property :sub_brand, :sub_brand_hash, :required, 'sub_brand model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :sub_brand_hash do
        description 'details of sub_brand'
        property :name, :string, :required, 'name of sub_brand'
        property :logo_url, :string, :optional, 'logo url for sub_brand'
        property :brand_id, :integer, :required, 'brand'
        property :initials, :string, :required, 'initials of sub brand'
      end

      #
      # function to update sub_brand
      #
      # post /update_sub_brand
      #
      # Request:: request object contain
      #   * sub_brand_params [hash] it contains name, logo_url, brand_id
      #
      # Response::
      #   * If action succeed then returns Success Status code with update sub_brand
      #    else returns BadRequest response
      #
      def update_sub_brand
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_sub_brand_api = UPDATE_SUB_BRAND.new(deciding_params)
        response = update_sub_brand_api.enact(sub_brand_params, get_id_from_params)
        send_response(response)
      end

      swagger_controller :sub_brand, 'MasterProductModule APIs'
      swagger_api :update_sub_brand do
        summary 'It updates the sub_brand with given input'
        notes 'update sub_brand'
        param :path, :id, :integer, :required, 'sub brand to update'
        param :body, :update_sub_brand_request, :update_sub_brand_request, :required, 'create_sub_brand request'
        response :bad_request, 'if request params are incorrect or api fails to create sub_brand'
      end

      swagger_model :update_sub_brand_request do
        description 'request schema for /create_category API'
        property :sub_brand, :update_sub_brand_hash, :required, 'sub_brand model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :update_sub_brand_hash do
        description 'details of sub_brand'
        property :name, :string, :required, 'name of sub_brand'
        property :logo_url, :string, :optional, 'logo url for sub_brand'
        property :brand_id, :integer, :required, 'brand'
      end

      #
      # function to get all sub_brands
      #
      # GET /get_all_sub_brands
      #
      # Response::
      #   * list of sub_brands
      #
      def get_all_sub_brands
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_sub_brands_api = GET_SUB_BRANDS.new(deciding_params)
        response = get_sub_brands_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :sub_brand, 'SupplyChainModule APIs'
      swagger_api :get_all_sub_brands do
        summary 'It returns details of all the sub_brands'
        notes 'get_all_sub_brands API returns the details of all the sub_brands'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all sub_brand'
        param :query, :per_page, :integer, :optional, 'how many sub_brand to display in a page'
        param :query, :state, :integer, :optional, 'to fetch sub_brands based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to change the status of sub_brand
      #
      # PUT /sub_brand/state/:id
      #
      # Request::
      #   * sub_brand_id [integer] id of sub_brand of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_sub_brand_state_api = CHANGE_SUB_BRAND_STATE_API.new(deciding_params)
        response = change_sub_brand_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :sub_brand, 'SupplyChainModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of sub_brand'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the sub_brand & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'sub_brand_id to be deactivated/activated'
        param :body, :change_sub_brand_state_request, :change_sub_brand_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or sub_brand not found or it fails to change state'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_sub_brand_state_request do
        description 'request schema for /change_state API'
        property :sub_brand, :change_sub_brand_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_sub_brand_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      #
      # function to delete sub_brand
      #
      # DELETE /sub_brand/state/:id
      #
      # Request::
      #   * sub_brand_id [integer] id of sub_brand of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def delete_sub_brand
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_sub_brand_state_api = CHANGE_SUB_BRAND_STATE_API.new(deciding_params)
        response = change_sub_brand_state_api.enact({ 
                      id: get_id_from_params, 
                      event: COMMON_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :sub_brand, 'SupplyChainModule APIs'
      swagger_api :delete_sub_brand do
        summary 'It delete the sub_brand'
        notes 'delete_sub_brand API change the status of sub_brand to soft delete'
        param :path, :id, :integer, :required, 'sub_brand_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or sub_brand not found or it fails to delete'
      end

            #
      # function to get a product
      #
      # GET /products/:id
      #
      # Request::
      #   * product_id [integer] id of product of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_product
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_product_api = GET_BRAND_PACK_API.new(deciding_params)
        response = get_product_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :product, 'SupplyChainModule APIs'
      swagger_api :get_product do
        summary 'It return a product'
        notes 'get_company API return a single product corresponding to the id'
        param :path, :id, :integer, :required, 'product_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or product not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a sub_brand
      #
      # GET /sub_brands/:id
      #
      # Request::
      #   * sub_brand_id [integer] id of sub_brand of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_sub_brand
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_sub_brand_api = GET_SUB_BRAND_API.new(deciding_params)
        response = get_sub_brand_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :sub_brand, 'SupplyChainModule APIs'
      swagger_api :get_sub_brand do
        summary 'It return a sub_brand'
        notes 'get_company API return a single sub_brand corresponding to the id'
        param :path, :id, :integer, :required, 'sub_brand_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or sub_brand not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private

      def sub_brand_params
        params.require(:sub_brand).permit(:name, :logo_url, :brand_id, :initials)
      end

      def update_sub_brand_params
        params.require(:sub_brand).permit(:name, :logo_url, :brand_id)
      end

      def change_state_params
        params.require(:sub_brand).permit(:event).merge({ id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end

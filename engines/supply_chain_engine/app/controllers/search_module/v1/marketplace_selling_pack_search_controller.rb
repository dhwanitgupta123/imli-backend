#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # MarketplaceSellingPackSearch controller service class to route panel API calls to the SearchApi
    #
    class MarketplaceSellingPackSearchController < BaseModule::V1::ApplicationController
      #
      # initialize SearchMarketplaceSellingPackByQuery Class
      #
      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
        @search_marketplace_selling_pack_by_query = SearchModule::V1::SearchMarketplaceSellingPackByQueryApi
      end

      #
      # function to get all marketplace_selling_pack which match query
      #
      # GET product/search
      #
      # Request::
      #   * query [string] query string
      #   * top_n [string] number of results to return
      #
      # Response::
      #   * send list of marketplace_selling_pack
      #
      _LogActivity_
      def get_marketplace_selling_pack_by_query
        deciding_params = @application_helper.get_deciding_params(params)
        search_marketplace_selling_pack_by_query_api = @search_marketplace_selling_pack_by_query.new deciding_params
        response = search_marketplace_selling_pack_by_query_api.enact(params)
        send_response(response)
      end

      swagger_controller :marketplace_selling_pack_search, 'SearchModule APIs'
      swagger_api :get_marketplace_selling_pack_by_query do
        summary 'It returns list of marketplace_selling_pack mactching the query'
        notes 'get_marketplace_selling_pack_by_query API returns list of marketplace_selling_pack mactching the query'
        param :query, :query, :string, :required, 'query string'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :top_n, :string, :optional, 'number of results'
        response :bad_request, 'wrong parameters, query length < 3 or request in blank'
        response :internal_server_error, 'run time error if elastic search server fails'
      end
    end
  end
end

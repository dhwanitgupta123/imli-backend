#
# Module to handle all the functionalities related to warehouse brand pack
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Warehouse Brand Packs controller service class to route panel API calls to the WBP API
    #
    class WarehouseBrandPacksController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_WAREHOUSE_BRAND_PACK = WarehouseProductModule::V1::AddWarehouseBrandPackApi
      GET_ALL_WAREHOUSE_BRAND_PACKS = WarehouseProductModule::V1::GetAllWarehouseBrandPackApi
      CHANGE_STATE = WarehouseProductModule::V1::ChangeWarehouseBrandPackStateApi
      GET_WAREHOUSE_BRAND_PACK = WarehouseProductModule::V1::GetWarehouseBrandPackApi
      UPDATE_WAREHOUSE_BRAND_PACK = WarehouseProductModule::V1::UpdateWarehouseBrandPackApi
      STOCK_WAREHOUSE_BRAND_PACKS = WarehouseProductModule::V1::StockWarehouseBrandPacksApi
      STOCK_WAREHOUSE_BRAND_PACKS_STATUS = WarehouseProductModule::V1::StockWarehouseBrandPacksStatusApi
      GET_WAREHOUSE_BRAND_PACKS_BY_WAREHOUSE = WarehouseProductModule::V1::GetWarehouseBrandPacksByWarehouseApi
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      def initialize
      end

      #
      # function to add warehouse brand pack
      #
      # post /warehouse_brand_packs/new
      #
      # Request:: request object contain
      #   * warehouse_brand_pack_params [hash] it contains details of wbp
      #
      # Response::
      #   * If action succeed then returns Success Status code with create wbp
      #    else returns BadRequest response
      #
      def add_warehouse_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_warehouse_brand_pack_api = ADD_WAREHOUSE_BRAND_PACK.new(deciding_params)
        response = add_warehouse_brand_pack_api.enact(warehouse_brand_pack_params)
        send_response(response)
      end

      swagger_controller :warehouse_brand_packs, 'WarehouseProductModule APIs'
      swagger_api :add_warehouse_brand_pack do
        summary 'It creates the warehouse brand pack with given input'
        notes 'create warehouse brand pack'
        param :body, :add_wbp_api, :add_wbp_api, :required, 'add_warehouse_brand_pack request'
        response :bad_request, 'if request params are incorrect or api fails to create WBP'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :add_wbp_api do
        description 'request schema for /add_warehouse_brand_pack API'
        property :warehouse_brand_pack, :add_wbp_hash, :required, 'warehouse brand pack model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :add_wbp_hash do
        description 'details of warehouse brand pack'
        property :warehouse_id, :integer, :required, 'id of warehouse it belongs to'
        property :inventory_brand_pack_id, :integer, :required, 'id of IBP it refers to'
        property :brand_pack_id, :integer, :required, 'id of MBP it refers to'
        property :outbound_frequency_period_in_days, :string, :required, 'Freq period in days for outbound brand pack'
        property :threshold_stock_value, :string, :required, 'Threshold stock value at which we should trigger the PO'
      end

      #
      # function to change state of WBP
      #
      # put /warehouse_brand_packs/:id/change_state
      #
      # Request:: request object contain
      #   * id [integer] id of WBP whose state is to be changed
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated WBP
      #    else returns BadRequest response
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_state_api = CHANGE_STATE.new(deciding_params)
        response = change_state_api.enact(change_state_params)
        send_response(response)
      end

      swagger_controller :warehouse_brand_packs, 'WarehouseProductModule APIs'
      swagger_api :change_state do
        summary 'It changes the state of WBP'
        notes 'change_state API takes event & id as input and triggers the event to change the state of the WBP'
        param :path, :id, :integer, :required, 'WBP to update'
        param :body, :wbp_change_state, :wbp_change_state, :required, 'change_state request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of WBP'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :wbp_change_state do
        description 'request schema for /change_state API'
        property :warehouse_brand_pack, :wbp_change_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :wbp_change_state_hash do
        description 'event to be triggered'
        property :event, :integer, :required, 'event to trigger status change'
      end

      #
      # function to get all WBPs
      #
      # GET /warehouse_brand_packs
      #
      # Response::
      #   * list of WBPs
      #
      def get_all_warehouse_brand_packs
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_all_warehouse_brand_packs_api = GET_ALL_WAREHOUSE_BRAND_PACKS.new(deciding_params)
        response = get_all_warehouse_brand_packs_api.enact(pagination_params)
        send_response(response)
      end

      swagger_controller :warehouse_brand_packs, 'WarehouseProductModule APIs'
      swagger_api :get_all_warehouse_brand_packs do
        summary 'It returns details of all the WBPs'
        notes 'API returns the details of all the WBPs'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all WBPs'
        param :query, :per_page, :integer, :optional, 'how many WBP to display in a page'
        param :query, :state, :integer, :optional, 'to fetch WBP based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'id' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a WBP
      #
      # GET /warehouse_brand_packs/:id
      #
      # Response::
      #   * Details of a WBP including it's including it's IBPs & pricing, stocking
      #
      def get_warehouse_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_warehouse_brand_pack_api = GET_WAREHOUSE_BRAND_PACK.new(deciding_params)
        response = get_warehouse_brand_pack_api.enact(params[:id])
        send_response(response)
      end

      swagger_controller :warehouse_brand_packs, 'WarehouseProductModule APIs'
      swagger_api :get_warehouse_brand_pack do
        summary 'It returns details of a the WBP'
        notes 'API returns the details of a WBP'
        param :path, :id, :integer, :required, 'WBP to get'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'User does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to delete a WBP
      #
      # DELETE /warehouse_brand_packs/:id
      #
      # Response::
      #   * Success if WBP found
      #
      def delete_warehouse_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_state_api = CHANGE_STATE.new(deciding_params)
        response = change_state_api.enact({ id: params[:id], event: COMMON_EVENTS::SOFT_DELETE })
        send_response(response)
      end

      swagger_controller :warehouse_brand_packs, 'WarehouseProductModule APIs'
      swagger_api :delete_warehouse_brand_pack do
        summary 'It soft deletes a WBP'
        notes 'delete_warehouse_brand_pack API calls ChangeWarehouseBrandPackStateApi to make its state deleted'
        param :path, :id, :integer, :required, 'WBP to delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'User does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to update a WBP
      #
      # PUT /warehouse_brand_packs/:id
      #
      # Response::
      #   * Success if WBP updated
      #
      def update_warehouse_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_warehouse_brand_pack_api = UPDATE_WAREHOUSE_BRAND_PACK.new(deciding_params)
        response = update_warehouse_brand_pack_api.enact(update_warehouse_brand_pack_params.merge(id: params[:id]))
        send_response(response)
      end

      swagger_controller :warehouse_brand_packs, 'WarehouseProductModule APIs'
      swagger_api :update_warehouse_brand_pack do
        summary 'It updates a WBP'
        notes 'update_warehouse_brand_pack API updates the details of WBP'
        param :path, :id, :integer, :required, 'WBP to update'
        param :body, :update_wbp_api, :update_wbp_api, :required, 'add_warehouse_brand_pack request'
        response :bad_request, 'User does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :update_wbp_api do
        description 'request schema for /add_warehouse_brand_pack API'
        property :warehouse_brand_pack, :update_wbp_hash, :required, 'warehouse brand pack model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :update_wbp_hash do
        description 'details of warehouse brand pack'
        property :warehouse_id, :integer, :required, 'id of warehouse it belongs to'
        property :inventory_brand_pack_id, :integer, :required, 'id of IBP it refers to'
        property :brand_pack_id, :integer, :required, 'id of MBP it refers to'
        property :outbound_frequency_period_in_days, :string, :required, 'Freq period in days for outbound brand pack'
        property :threshold_stock_value, :string, :required, 'Threshold stock value at which we should trigger the PO'
        property :pricing, :warehouse_pricing, :required,
                 'it has details of warehouse brand pack'
      end

      swagger_model :warehouse_pricing do
        description 'details of warehouse_brand_pack pricings'
        property :mrp, :double, :required, 'MRP of product'
        property :selling_price, :double, :required, 'selling price of WBP'
        property :service_tax, :double, :required, 'service tax to be applied'
        property :vat, :double, :required, 'vat tax to be applied'
        property :cst, :double, :required, 'cst tax to be applied'
      end

      #
      # Function to add the stocking details of the wbps
      # 
      # Post /warehouse_brand_packs/stock/:warehouse_id
      # 
      # Response::
      #   * Success if WBP is stocked
      # 
      def stock_warehouse_brand_packs
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        stock_warehouse_brand_packs_api = STOCK_WAREHOUSE_BRAND_PACKS.new(deciding_params)
        response = stock_warehouse_brand_packs_api.enact({ params: stock_warehouse_brand_pack_params[:warehouse_brand_packs],
                                                           warehouse_id: params[:warehouse_id] })
        send_response(response)
      end

      swagger_controller :warehouse_brand_packs, 'WarehouseProductModule APIs'
      swagger_api :stock_warehouse_brand_packs do
        summary 'It stocks the WBP'
        notes 'It takes the ID, purchase date & quantity of WBP to be stocked'
        param :path, :warehouse_id, :integer, :required, 'warehouse id of wbp which are going to be stocked'
        param :body, :stock_wbps, :stock_wbps, :required, 'stock wbp request'
        response :bad_request, 'if request params are incorrect or api fails to stock the WBP'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :stock_wbps do
        description 'request schema for /warehouse_brand_packs/stock API'
        property :warehouse_brand_packs, :array, :required, 'hash of wbp stock details', { 'items': { 'type': :stock_wbp_details } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :stock_wbp_details do
        description 'Stocking details of a WBP'
        property :id, :string, :required, 'ID of WBP'
        property :quantity, :string, :required, 'quantity of WBP'
        property :date_of_purchase, :string, :required, 'Date of purchase of WBP'
      end

      #
      # Function to mark the WBPs expired & damaged
      # 
      # Post /warehouse_brand_packs/stock/status/:warehouse_id
      # 
      # Response::
      #   * Success if WBP is stocked
      # 
      def stock_warehouse_brand_packs_status
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        stock_warehouse_brand_packs_status_api = STOCK_WAREHOUSE_BRAND_PACKS_STATUS.new(deciding_params)
        response = stock_warehouse_brand_packs_status_api.enact({ params: stock_warehouse_brand_pack_status_params[:warehouse_brand_packs],
                                                           warehouse_id: params[:warehouse_id] })
        send_response(response)
      end

      swagger_controller :warehouse_brand_packs, 'WarehouseProductModule APIs'
      swagger_api :stock_warehouse_brand_packs_status do
        summary 'It stocks the WBP with status expired & damaged'
        notes 'It takes the ID, status & quantity of WBP to be stocked'
        param :path, :warehouse_id, :integer, :required, 'warehouse id of wbp which are going to be stocked'
        param :body, :stock_wbps_status, :stock_wbps_status, :required, 'stock wbp status request'
        response :bad_request, 'if request params are incorrect or api fails to stock the WBP'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :stock_wbps_status do
        description 'request schema for /warehouse_brand_packs/stock/status API'
        property :warehouse_brand_packs, :array, :required, 'hash of wbp stock details', { 'items': { 'type': :stock_wbp_status_details } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :stock_wbp_status_details do
        description 'Stocking details of a WBP'
        property :id, :string, :required, 'ID of WBP'
        property :quantity, :string, :required, 'quantity of WBP'
        property :status, :string, :required, 'Status of WBP'
      end

      #
      # function to get all warehouse_brand_packs of a warehouse
      #
      # GET /warehouse/:id/warehouse_brand_packs
      #
      # Response::
      #   * list of warehouse_brand_packs of a particular warehouse
      #
      def get_all_warehouse_brand_packs_by_warehouse
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_warehouse_brand_packs_by_warehouse_api = GET_WAREHOUSE_BRAND_PACKS_BY_WAREHOUSE.new(deciding_params)
        response = get_warehouse_brand_packs_by_warehouse_api.enact(get_all_warehouse_brand_packs_by_warehouse_params.merge(id: params[:id]))
        send_response(response)
      end

      swagger_controller :warehouse_brand_packs, 'SupplyChainModule APIs'
      swagger_api :get_all_warehouse_brand_packs_by_warehouse do
        summary 'It returns details of all the warehouse brand packs'
        notes 'get_all_warehouse_brand_packs API returns the details of all the warehouse brand pack'
        param :path, :id, :string, :required, 'ID of warehouse of which WBP are requested'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all wbps stock details'
        param :query, :per_page, :integer, :optional, 'how many wbps to display in a page'
        param :query, :department_id, :string, :optional, 'to fetch products based on their department'
        param :query, :category_id, :string, :optional, 'to fetch products based on their category'
        param :query, :sub_category_id, :string, :optional, 'to fetch products based on their sub_category'
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private 

      def warehouse_brand_pack_params
        params.require(:warehouse_brand_pack).permit(
          :warehouse_id, 
          :inventory_brand_pack_id, 
          :brand_pack_id,
          :outbound_frequency_period_in_days,
          :threshold_stock_value
        )
      end

      def update_warehouse_brand_pack_params
        params.require(:warehouse_brand_pack).permit(
          :warehouse_id, 
          :inventory_brand_pack_id, 
          :brand_pack_id,
          :outbound_frequency_period_in_days,
          :threshold_stock_value,
          pricing: [:mrp, :service_tax, :vat, :cst, :selling_price]
        )
      end

      #
      # White list params for change state API
      #
      def change_state_params
        params.require(:warehouse_brand_pack).permit(:event).merge({ id: params[:id], action: params[:action] })
      end

      #
      # White list params for get_all_cash_and_carry API
      #
      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end

      def stock_warehouse_brand_pack_params
        if params[:warehouse_brand_packs].present? && 
            params[:warehouse_brand_packs].is_a?(String)
          params[:warehouse_brand_packs] = JSON.parse(params[:warehouse_brand_packs])
        end
        params.require(:warehouse_brand_packs)
        params.permit(warehouse_brand_packs: [:id, :quantity, :date_of_purchase])
      end

      def stock_warehouse_brand_pack_status_params
        if params[:warehouse_brand_packs].present? && 
            params[:warehouse_brand_packs].is_a?(String)
          params[:warehouse_brand_packs] = JSON.parse(params[:warehouse_brand_packs])
        end
        params.require(:warehouse_brand_packs)
        params.permit(warehouse_brand_packs: [:id, :quantity, :status])
      end

      def get_all_warehouse_brand_packs_by_warehouse_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order, :department_id, :category_id, :sub_category_id)
      end
    end
  end
end

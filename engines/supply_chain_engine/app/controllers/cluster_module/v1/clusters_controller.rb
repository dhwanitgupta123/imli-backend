#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # ClustersController contains API related to cluster
    #
    class ClustersController < BaseModule::V1::ApplicationController

      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      ADD_CLUSTER_API = ClusterModule::V1::AddClusterApi
      UPDATE_CLUSTER_API = ClusterModule::V1::UpdateClusterApi
      CHANGE_CLUSTER_STATE_API = ClusterModule::V1::ChangeClusterStateApi
      GET_CLUSTER_API = ClusterModule::V1::GetClusterApi
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      GET_CLUSTERS_API = ClusterModule::V1::GetClustersApi

      def initialize
      end
      #
      # function to add cluster
      #
      # post /add_cluster
      #
      # Request:: request object contain
      #   * cluster_params [hash] it contains label cluster_type, images, mpsp_ods, context
      #
      # Response::
      #   * If action succeed then returns Success Status code with create cluster
      #    else returns BadRequest response
      #
      def add_cluster

        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_cluster_api = ADD_CLUSTER_API.new(deciding_params)
        response = add_cluster_api.enact(cluster_params)
        send_response(response)
      end

      swagger_controller :clusters, 'ClusterModule APIs'
      swagger_api :add_cluster do
        summary 'It creates the cluster with given input'
        notes 'create cluster'
        param :body, :cluster_request, :cluster_request, :required, 'create_cluster request'
        response :bad_request, 'if request params are incorrect or api fails to create cluster'
      end

      swagger_model :cluster_request do
        description 'request schema for /create_category API'
        property :cluster, :cluster_hash, :required, 'cluster model hash', 'cluster' => { '$ref' => 'category_hash' }
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :cluster_hash do
        description 'details of cluster'
        property :images, :array, :optional,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :image_hash_array } }        
        property :label, :string, :optional, 'description'
        property :cluster_type, :integer, :required, 'Type of cluster'
        property :context, :context_hash, :required, 'context hash of cluster'
        property :mpsp_ids, :array, :optional, 'product_id in cluster',
                   { 'items': { 'type': 'integer' } }
      end

      swagger_model :image_hash_array do
        description 'details of image id and corresponding priority'
        property :image_id, :integer, :required, 'reference to image'
        property :priority, :integer, :required, 'priority of image'
      end

      swagger_model :context_hash do
        description 'details of image id and corresponding priority'
        property :level_id, :integer, :required, 'level id corresponding to level'
        property :level, :integer, :required, 'level of context'
        property :description, :string, :optional, 'description of cluster'
      end

      #
      # function to update cluster
      #
      # post /update_cluster
      #
      # Request:: request object contain
      #   * cluster_params [hash] it contains label cluster_type, images, mpsp_ods, context
      #
      # Response::
      #   * If action succeed then returns Success Status code with create cluster
      #    else returns BadRequest response
      #
      def update_cluster
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_cluster_api = UPDATE_CLUSTER_API.new(deciding_params)
        
        response = update_cluster_api.enact(cluster_params.merge(id: get_id_from_params))
        send_response(response)
      end

      swagger_controller :clusters, 'ClusterModule APIs'
      swagger_api :update_cluster do
        summary 'It creates the cluster with given input'
        notes 'create cluster'
        param :path, :id, :integer, :required, 'cluster to update'
        param :body, :cluster_request, :cluster_request, :required, 'create_cluster request'
        response :bad_request, 'if request params are incorrect or api fails to create cluster'
      end

      #
      # function to change the status of cluster
      #
      # PUT /cluster/state/:id
      #
      # Request::
      #   * cluster_id [integer] id of cluster of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_cluster_state_api = CHANGE_CLUSTER_STATE_API.new(deciding_params)
        response = change_cluster_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :clusters, 'ClusterModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of cluster'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the cluster & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'cluster_id to be deactivated/activated'
        param :body, :change_cluster_state_request, :change_cluster_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or cluster not found or it fails to change state'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_cluster_state_request do
        description 'request schema for /change_state API'
        property :cluster, :change_cluster_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_cluster_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      #
      # function to get a cluster
      #
      # GET /clusters/:id
      #
      # Request::
      #   * cluster_id [integer] id of cluster of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_cluster
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_cluster_api = GET_CLUSTER_API.new(deciding_params)
        response = get_cluster_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :clusters, 'ClusterModule APIs'
      swagger_api :get_cluster do
        summary 'It return a cluster'
        notes 'get_company API return a single cluster corresponding to the id'
        param :path, :id, :integer, :required, 'cluster_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or cluster not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to delete cluster
      #
      # DELETE /clusters/state/:id
      #
      # Request::
      #   * cluster_id [integer] id of cluster of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def delete_cluster
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_cluster_state_api = CHANGE_CLUSTER_STATE_API.new(deciding_params)
        response = change_cluster_state_api.enact({ 
                      id: get_id_from_params,
                      event: COMMON_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :clusters, 'ClusterModule APIs'
      swagger_api :delete_cluster do
        summary 'It delete the cluster'
        notes 'delete_cluster API change the status of cluster to soft delete'
        param :path, :id, :integer, :required, 'cluster_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or cluster not found or it fails to delete'
      end

      #
      # function to get all clusters
      #
      # GET /get_all_clusters
      #
      # Response::
      #   * list of clusters
      #
      def get_all_clusters
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_clusters_api = GET_CLUSTERS_API.new(deciding_params)
        response = get_clusters_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :clusters, 'ClusterModule APIs'
      swagger_api :get_all_clusters do
        summary 'It returns details of all the Clusters'
        notes 'get_all_clusters API returns the details of all the Clusters'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all Clusters'
        param :query, :per_page, :integer, :optional, 'how many Clusters to display in a page'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        param :query, :level_id, :integer, :optional, 'level id of level'
        param :query, :level, :integer, :optional, 'level of cluster'
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end


      private

      def cluster_params
        if params[:cluster].present? &&
            params[:cluster][:images].present? &&
            params[:cluster][:images].is_a?(String)

          params[:cluster][:images] = JSON.parse(params[:cluster][:images])

        end
        params.require(:cluster).permit(:label, :cluster_type, :description, context: [:level_id, :level], images: [:image_id, :priority], :mpsp_ids => [], )
      end

      def change_state_params
        params.require(:cluster).permit(:event).merge({ id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :sort_by, :order, :level_id, :level)
      end
    end
  end
end

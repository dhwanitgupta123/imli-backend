#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    #
    # Seller brand pack controller service class to route panel API calls to the SellerBrandPackApi
    #
    class SellerBrandPackController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_SELLER_BRAND_PACK = SellerProductModule::V1::AddSellerBrandPackApi
      UPDATE_SELLER_BRAND_PACK = SellerProductModule::V1::UpdateSellerBrandPackApi
      GET_SELLER_BRAND_PACKS = SellerProductModule::V1::GetSellerBrandPacksApi
      CHANGE_STATE = SellerProductModule::V1::ChangeSellerBrandPackStateApi
      GET_SELLER_BRAND_PACK = SellerProductModule::V1::GetSellerBrandPackApi
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      def initialize
      end

      #
      # function to add seller brand pack
      #
      # post /add_seller_brand_pack
      #
      # Request:: request object contain
      #   * seller_brand_Pack_params [hash] it contains marketplace brand pack id
      #
      # Response::
      #   * If action succeed then returns Success Status code with create seller brand pack
      #    else returns BadRequest response
      #
      def add_seller_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_seller_brand_pack_api = ADD_SELLER_BRAND_PACK.new(deciding_params)
        response = add_seller_brand_pack_api.enact(seller_brand_pack_params)
        send_response(response)
      end

      swagger_controller :seller_brand_pack, 'SellerProductModule APIs'
      swagger_api :add_seller_brand_pack do
        summary 'It creates the seller brand Pack with given input'
        notes 'create seller brand Pack'
        param :body, :add_seller_brand_pack_request, :add_seller_brand_pack_request, :required, 'add_seller_brand_pack_request'
        response :bad_request, 'if request params are incorrect or api fails to create seller brand Pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :add_seller_brand_pack_request do
        description 'request schema for /add_seller_brand_pack API'
        property :seller_brand_pack, :add_seller_brand_pack, :required, 'seller brand Pack model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :add_seller_brand_pack do
        description 'details of seller brand Pack'
        property :seller_id, :string, :required, 'id of seller'
        property :warehouse_brand_pack_id, :integer, :required, 'id of IBP it refers to'
        property :brand_pack_id, :string, :required, 'id of brand pack'
        property :pricing, :seller_pricing, :required,
                 'it has details of seller brand pack'
      end

      swagger_model :seller_pricing do
        description 'details of seller_brand_pack pricings'
        property :spat, :double, :required, 'Selling price and tax'
        property :margin, :double, :required, 'margin on selling price'
        property :service_tax, :double, :required, 'service tax to be applied'
        property :vat, :double, :required, 'vat tax to be applied'
        property :spat_per_unit, :double, :required, 'SPAT per unit pack size'
      end

      #
      # function to change state of seller brand pack
      #
      # post /seller_brand_pack/:id/change_state
      #
      # Request:: request object contain
      #   * id [integer] id of seller brand pack whose state is to be changed
      #
      # Response::
      #   * If action succeed then returns Success Status code with update seller
      #    else returns BadRequest response
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_state_api = CHANGE_STATE.new(deciding_params)
        response = change_state_api.enact(change_state_params)
        send_response(response)
      end

      swagger_controller :seller_brand_pack, 'SellerProductModule APIs'
      swagger_api :change_state do
        summary 'It changes the state of seller brand pack'
        notes 'change_state API takes event & id as input and triggers the event to change the state of the seller brand pack'
        param :path, :id, :integer, :required, 'seller brand pack to update'
        param :body, :change_state, :change_state, :required, 'change_state request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of seller brand pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :change_state do
        description 'request schema for /change_state API'
        property :seller_brand_pack, :change_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_state_hash do
        description 'event to be triggered'
        property :event, :integer, :required, 'event to trigger status change'
      end

      #
      # function to update seller brand pack
      #
      # put /update_seller_brand_pack
      #
      # Request:: request object contain
      #   * seller_brand_pack_params [hash] it contains seller brand pack params
      #
      # Response::
      #   * If action succeed then returns Success Status code with update seller brand pack
      #    else returns BadRequest response
      #
      def update_seller_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_seller_brand_pack_api = UPDATE_SELLER_BRAND_PACK.new(deciding_params)
        response = update_seller_brand_pack_api.enact(seller_brand_pack_params.merge(id: params[:id]))
        send_response(response)
      end

      swagger_controller :seller_brand_pack, 'SellerProductModule APIs'
      swagger_api :update_seller_brand_pack do
        summary 'It updates the seller brand Pack with given input'
        notes 'update seller brand Pack'
        param :path, :id, :integer, :required, 'seller brand Pack to update'
        param :body, :add_seller_brand_pack_request, :add_seller_brand_pack_request, :required,
        'update_seller_brand_pack request'
        response :bad_request, 'if request params are incorrect or api fails to create seller brand Pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get all seller brand packs
      #
      # GET /get_all_seller_brand_packs
      #
      # Response::
      #   * list of seller brand packs
      #
      def get_all_seller_brand_packs
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_seller_brand_packs_api = GET_SELLER_BRAND_PACKS.new(deciding_params)
        response = get_seller_brand_packs_api.enact(pagination_params)
        send_response(response)
      end

      swagger_controller :seller_brand_pack, 'SellerProductModule APIs'
      swagger_api :get_all_seller_brand_packs do
        summary 'It returns details of all the seller brand packs'
        notes 'API returns the details of all the seller brand packs'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all seller brand packs'
        param :query, :per_page, :integer, :optional, 'how many seller to display in a page'
        param :query, :state, :integer, :optional, 'to fetch seller based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'id' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a SBP
      #
      # GET /seller_brand_pack/:id
      #
      # Response::
      #   * Details of a SBP including it's including it's WBPs & pricing, stocking
      #
      def get_seller_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_seller_brand_pack_api = GET_SELLER_BRAND_PACK.new(deciding_params)
        response = get_seller_brand_pack_api.enact(params[:id])
        send_response(response)
      end

      swagger_controller :seller_brand_packs, 'SellerProductModule APIs'
      swagger_api :get_seller_brand_pack do
        summary 'It returns details of a the SBP'
        notes 'API returns the details of a SBP'
        param :path, :id, :integer, :required, 'SBP to get'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'User does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to delete a seller brand pack
      #
      # DELETE /seller_brand_pack/:id
      #
      # Response::
      #   * Success if seller found & deleted
      #
      def delete_seller_brand_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_state_api = CHANGE_STATE.new(deciding_params)
        response = change_state_api.enact({ id: params[:id], event: COMMON_EVENTS::SOFT_DELETE })
        send_response(response)
      end

      swagger_controller :seller_brand_pack, 'SellerProductModule APIs'
      swagger_api :delete_seller_brand_pack do
        summary 'It soft deletes a seller brand pack'
        notes 'delete_seller API calls ChangeSellerBrandPackStateApi to make its state deleted'
        param :path, :id, :integer, :required, 'seller to delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'User does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private 

      def seller_brand_pack_params
        params.require(:seller_brand_pack).permit(
          :seller_id, 
          :brand_pack_id, 
          :warehouse_brand_pack_id,
          pricing: [:spat, :margin, :service_tax, :vat, :spat_per_unit]
        )
      end

      #
      # White list params for change state API
      #
      def change_state_params
        params.require(:seller_brand_pack).permit(:event).merge({ id: params[:id], action: params[:action] })
      end

      #
      # White list params for get_all_seller API
      #
      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
      
    end
  end
end
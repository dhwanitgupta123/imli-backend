$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "supply_chain_engine/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "supply_chain_engine"
  s.version     = SupplyChainEngine::VERSION
  s.authors     = ["Dhwanit Gupta"]
  s.email       = ["dhwanitgupta123@gmail.com"]
  s.homepage    = "https://trawlly.com"
  s.summary     = "Summary of SupplyChainEngine."
  s.description = "Description of SupplyChainEngine."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.2.3"

  s.test_files = Dir["spec/**/*"]
end

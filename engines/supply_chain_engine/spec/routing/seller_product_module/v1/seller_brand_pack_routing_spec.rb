#
# Module to handle all the functionalities related to seller product
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SellerBrandPackController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_seller_brand_pack' do
          expect(:post => 'supply_chain/seller_brand_packs/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'seller_product_module/v1/seller_brand_pack',
              'action' => 'add_seller_brand_pack'
            })
        end

        it 'routes to #get_all_seller_brand_packs' do
          expect(:get => 'supply_chain/seller_brand_packs').to route_to(
            {
              'format' => 'json',
              'controller' => 'seller_product_module/v1/seller_brand_pack',
              'action' => 'get_all_seller_brand_packs'
            })
        end

        it 'routes to #get_seller_brand_pack' do
          expect(:get => 'supply_chain/seller_brand_packs/:id').to route_to(
            {
              'format' => 'json',
              'controller' => 'seller_product_module/v1/seller_brand_pack',
              'action' => 'get_seller_brand_pack',
              'id' => ':id'
            })
        end

        it 'routes to #change_state of seller brand back' do
          expect(:put => 'supply_chain/seller_brand_packs/state/:id').to route_to(
            {
              'format' => 'json',
              'controller' => 'seller_product_module/v1/seller_brand_pack',
              'action' => 'change_state',
              'id'=> ':id'
            })
        end

        it 'routes to #delete_seller_brand_pack' do
          expect(:delete => 'supply_chain/seller_brand_packs/:id').to route_to(
            {
              'format' => 'json',
              'controller' => 'seller_product_module/v1/seller_brand_pack',
              'action' => 'delete_seller_brand_pack',
              'id'=> ':id'
            })
        end

        it 'routes to #update_seller_brand_pack' do
          expect(:put => 'supply_chain/seller_brand_packs/:id').to route_to(
            {
              'id' => ':id',
              'format' => 'json',
              'controller' => 'seller_product_module/v1/seller_brand_pack',
              'action' => 'update_seller_brand_pack'
            })
        end

      end
    end
  end
end
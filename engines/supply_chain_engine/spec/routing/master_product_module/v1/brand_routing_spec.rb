#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe BrandController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_brand' do
          expect(:post => 'supply_chain/brands/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'master_product_module/v1/brand',
              'action' => 'add_brand'
            })
        end

        it 'routes to #update_brand' do
          expect(:put => 'supply_chain/brands/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'master_product_module/v1/brand',
              'action'=>'update_brand',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_brands' do
          expect(:get => 'supply_chain/brands').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/brand',
              'action' => 'get_all_brands'
            })
        end

        it 'routes to #change brand state' do
          expect(:put => 'supply_chain/brands/state/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/brand',
              'action' => 'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #delete brand' do
          expect(:delete => 'supply_chain/brands/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/brand',
              'action' => 'delete_brand',
              'id'=>'1'
            })
        end

        it 'routes to #get_brand' do
          expect(:get => 'supply_chain/brands/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/brand',
              'action' => 'get_brand',
              'id'=>'1'
            })
        end
      end
    end
  end
end
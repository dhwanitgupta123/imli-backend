#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe VariantsController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_variant' do
          expect(:post => 'supply_chain/variants/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'master_product_module/v1/variants',
              'action' => 'add_variant'
            })
        end

        it 'routes to #update_variant' do
          expect(:put => 'supply_chain/variants/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'master_product_module/v1/variants',
              'action'=>'update_variant',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_variantss' do
          expect(:get => 'supply_chain/variants').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/variants',
              'action' => 'get_all_variants'
            })
        end
      end
    end
  end
end
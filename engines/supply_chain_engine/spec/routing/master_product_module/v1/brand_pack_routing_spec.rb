#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe BrandPackController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_brand_pack' do
          expect(:post => 'supply_chain/brand_packs/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'master_product_module/v1/brand_pack',
              'action' => 'add_brand_pack'
            })
        end

        it 'routes to #update_brand_pack' do
          expect(:put => 'supply_chain/brand_packs/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'master_product_module/v1/brand_pack',
              'action'=>'update_brand_pack',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_brand_packs' do
          expect(:get => 'supply_chain/brand_packs').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/brand_pack',
              'action' => 'get_all_brand_packs'
            })
        end

        it 'routes to #change brand pack state' do
          expect(:put => 'supply_chain/brand_packs/state/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/brand_pack',
              'action' => 'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #delete brand_pack' do
          expect(:delete => 'supply_chain/brand_packs/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/brand_pack',
              'action' => 'delete_brand_pack',
              'id'=>'1'
            })
        end

        it 'routes to #get_brand_pack' do
          expect(:get => 'supply_chain/brand_packs/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/brand_pack',
              'action' => 'get_brand_pack',
              'id'=>'1'
            })
        end
      end
    end
  end
end
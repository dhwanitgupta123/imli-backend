#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    require 'rails_helper'

    RSpec.describe ClustersController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_cluster' do
          expect(:post => 'clusters/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'cluster_module/v1/clusters',
              'action' => 'add_cluster'
            })
        end

        it 'routes to #update_cluster' do
          expect(:put => 'clusters/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'cluster_module/v1/clusters',
              'action'=>'update_cluster',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_clusters' do
          expect(:get => 'clusters').to route_to(
            {
              'format' => 'json',
              'controller' => 'cluster_module/v1/clusters',
              'action' => 'get_all_clusters'
            })
        end

        it 'routes to #change cluster state' do
          expect(:put => 'clusters/state/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'cluster_module/v1/clusters',
              'action' => 'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #delete clusters' do
          expect(:delete => 'clusters/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'cluster_module/v1/clusters',
              'action' => 'delete_cluster',
              'id'=>'1'
            })
        end

        it 'routes to #get_clusters' do
          expect(:get => 'clusters/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'cluster_module/v1/clusters',
              'action' => 'get_cluster',
              'id'=>'1'
            })
        end
      end
    end
  end
end
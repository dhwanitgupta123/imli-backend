#
# Module to handle all the functionalities related to supply chain product
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe CashAndCarryController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_cash_and_carry' do
          expect(:post => 'supply_chain/cash_and_carry/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'supply_chain_module/v1/cash_and_carry',
              'action' => 'add_cash_and_carry'
            })
        end

        it 'routes to #change_state' do
          expect(:put => 'supply_chain/cash_and_carry/state/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'supply_chain_module/v1/cash_and_carry',
              'action'=>'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_cash_and_carry' do
          expect(:get => 'supply_chain/cash_and_carry').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/cash_and_carry',
              'action' => 'get_all_cash_and_carry'
            })
        end

        it 'routes to #get_cash_and_carry' do
          expect(:get => 'supply_chain/cash_and_carry/1').to route_to(
            {
              'id' => '1',
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/cash_and_carry',
              'action' => 'get_cash_and_carry'
            })
        end

        it 'routes to #update_cash_and_carry' do
          expect(:put => 'supply_chain/cash_and_carry/1').to route_to(
            {
              'id' => '1',
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/cash_and_carry',
              'action' => 'update_cash_and_carry'
            })
        end

        it 'routes to #delete_cash_and_carry' do
          expect(:delete => 'supply_chain/cash_and_carry/1').to route_to(
            {
              'id' => '1',
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/cash_and_carry',
              'action' => 'delete_cash_and_carry'
            })
        end
      end
    end
  end
end
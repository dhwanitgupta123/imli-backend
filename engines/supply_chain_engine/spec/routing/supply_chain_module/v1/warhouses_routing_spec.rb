#
# Module to handle all the functionalities related to supply chain product
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe WarehousesController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_warehouse' do
          expect(:post => 'supply_chain/warehouses/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'supply_chain_module/v1/warehouses',
              'action' => 'add_warehouse'
            })
        end

        it 'routes to #change_state' do
          expect(:put => 'supply_chain/warehouses/state/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'supply_chain_module/v1/warehouses',
              'action'=>'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_warehouses' do
          expect(:get => 'supply_chain/warehouses').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/warehouses',
              'action' => 'get_all_warehouses'
            })
        end

        it 'routes to #get_warehouse' do
          expect(:get => 'supply_chain/warehouses/1').to route_to(
            {
              'id' => '1',
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/warehouses',
              'action' => 'get_warehouse'
            })
        end

        it 'routes to #update_warehouse' do
          expect(:put => 'supply_chain/warehouses/1').to route_to(
            {
              'id' => '1',
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/warehouses',
              'action' => 'update_warehouse'
            })
        end

        it 'routes to #delete_warehouse' do
          expect(:delete => 'supply_chain/warehouses/1').to route_to(
            {
              'id' => '1',
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/warehouses',
              'action' => 'delete_warehouse'
            })
        end
      end
    end
  end
end
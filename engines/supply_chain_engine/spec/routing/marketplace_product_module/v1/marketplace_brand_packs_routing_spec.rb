#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MarketplaceBrandPacksController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #create_mpbp_and_brand_pack' do
          expect(:post => 'supply_chain/marketplace_brand_packs/brand_pack').to route_to(
            {
              'format' =>'json',
              'controller' => 'marketplace_product_module/v1/marketplace_brand_packs',
              'action' => 'create_mpbp_and_brand_pack'
            })
        end

        it 'routes to #add_marketplace_brand_pack' do
          expect(:post => 'supply_chain/marketplace_brand_packs/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'marketplace_product_module/v1/marketplace_brand_packs',
              'action' => 'add_marketplace_brand_pack'
            })
        end

        it 'routes to #update_marketplace_brand_pack' do
          expect(:put => 'supply_chain/marketplace_brand_packs/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'marketplace_product_module/v1/marketplace_brand_packs',
              'action'=>'update_marketplace_brand_pack',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_marketplace_brand_packs' do
          expect(:get => 'supply_chain/marketplace_brand_packs').to route_to(
            {
              'format' => 'json',
              'controller' => 'marketplace_product_module/v1/marketplace_brand_packs',
              'action' => 'get_all_marketplace_brand_packs'
            })
        end

        it 'routes to #change marketplace_brand pack state' do
          expect(:put => 'supply_chain/marketplace_brand_packs/state/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'marketplace_product_module/v1/marketplace_brand_packs',
              'action' => 'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #delete marketplace_brand_packs' do
          expect(:delete => 'supply_chain/marketplace_brand_packs/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'marketplace_product_module/v1/marketplace_brand_packs',
              'action' => 'delete_marketplace_brand_pack',
              'id'=>'1'
            })
        end

        it 'routes to #get_marketplace_brand_packs' do
          expect(:get => 'supply_chain/marketplace_brand_packs/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'marketplace_product_module/v1/marketplace_brand_packs',
              'action' => 'get_marketplace_brand_pack',
              'id'=>'1'
            })
        end
      end
    end
  end
end
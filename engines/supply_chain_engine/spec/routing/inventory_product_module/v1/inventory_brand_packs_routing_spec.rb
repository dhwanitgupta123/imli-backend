#
# Module to handle all the functionalities related to master product
#
module InventoryProductModule
  #
  # Version1 for inventory product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe InventoryBrandPacksController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_inventory_brand_pack' do
          expect(:post => 'supply_chain/inventory_brand_packs/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'inventory_product_module/v1/inventory_brand_packs',
              'action' => 'add_inventory_brand_pack'
            })
        end

        it 'routes to #update_inventory_brand_pack' do
          expect(:put => 'supply_chain/inventory_brand_packs/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'inventory_product_module/v1/inventory_brand_packs',
              'action'=>'update_inventory_brand_pack',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_inventory_brand_packs' do
          expect(:get => 'supply_chain/inventory_brand_packs').to route_to(
            {
              'format' => 'json',
              'controller' => 'inventory_product_module/v1/inventory_brand_packs',
              'action' => 'get_all_inventory_brand_packs'
            })
        end

        it 'routes to #change inventory brand pack state' do
          expect(:put => 'supply_chain/inventory_brand_packs/state/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'inventory_product_module/v1/inventory_brand_packs',
              'action' => 'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #delete inventory_brand_packs' do
          expect(:delete => 'supply_chain/inventory_brand_packs/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'inventory_product_module/v1/inventory_brand_packs',
              'action' => 'delete_inventory_brand_pack',
              'id'=>'1'
            })
        end

        it 'routes to #get_inventory_brand_packs' do
          expect(:get => 'supply_chain/inventory_brand_packs/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'inventory_product_module/v1/inventory_brand_packs',
              'action' => 'get_inventory_brand_pack',
              'id'=>'1'
            })
        end
      end
    end
  end
end
#
# Module to handle all the functionalities related to master product
#
module ClusterModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe ClusterModule::V1::ClustersController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add_cluster) { ClusterModule::V1::AddClusterApi }
      let(:update_cluster) { ClusterModule::V1::UpdateClusterApi }
      let(:get_clusters) { ClusterModule::V1::GetClustersApi }
      let(:get_cluster) { ClusterModule::V1::GetClusterApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:change_cluster_state_api) { ClusterModule::V1::ChangeClusterStateApi }
      let(:stub_ok_response) {
          {
            payload: {
              clusters: [
                {
                  cluster: {
                    label: Faker::Lorem.characters(6)
                  }
                }
              ]
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add cluster' do
        it 'returns created cluster successfully' do
          add_cluster.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_cluster, cluster: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add_cluster.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_cluster, cluster: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update cluster' do
        it 'returns updated cluster successfully' do
          update_cluster.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_cluster, id: 1, cluster: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_cluster.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_cluster, id:1, cluster: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all clusters' do
        it 'returns all clusters successfully' do
          get_clusters.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_clusters
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end

      describe 'change cluster state' do
        it 'return updated cluster' do
          change_cluster_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          put :change_state, id: 1, cluster: { event: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_cluster_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, cluster: { event: 0 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'delete cluster' do
        it 'return updated cluster' do
          change_cluster_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          delete :delete_cluster, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_cluster_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          delete :delete_cluster, id: 2
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get cluster' do
        it 'with valid argument' do
          get_cluster.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_cluster, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'with invalid arguments' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_cluster.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_cluster, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end

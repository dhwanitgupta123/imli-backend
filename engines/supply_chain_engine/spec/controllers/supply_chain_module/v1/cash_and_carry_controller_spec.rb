#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::CashAndCarryController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add_cash_and_carry) { SupplyChainModule::V1::AddCashAndCarryApi }
      let(:change_state) { SupplyChainModule::V1::ChangeCashAndCarryStateApi }
      let(:get_all_cash_and_carry) { SupplyChainModule::V1::GetAllCashAndCarryApi }
      let(:get_cash_and_carry) { SupplyChainModule::V1::GetCashAndCarryApi }
      let(:update_cash_and_carry) { SupplyChainModule::V1::UpdateCashAndCarryApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:stub_ok_response) {
          {
            payload: {
              cash_and_carrys: [
                {
                  cash_and_carry: {
                    name: Faker::Lorem.characters(6)
                  }
                }
              ]
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add cash_and_carry' do
        it 'returns created cash_and_carry successfully' do
          add_cash_and_carry.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_cash_and_carry, cash_and_carry: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add_cash_and_carry.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_cash_and_carry, cash_and_carry: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'change_state cash_and_carry' do
        it 'returns updated cash_and_carry successfully' do
          change_state.any_instance.stub(:enact).and_return(stub_ok_response)
          put :change_state, id: 1, cash_and_carry: { event: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, cash_and_carry: { event: 1 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when soft delete request is sent' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, cash_and_carry: { event: 3 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all cash_and_carrys' do
        it 'returns all cash_and_carrys successfully' do
          get_all_cash_and_carry.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_cash_and_carry, cash_and_carry: { sort_by: 'name' }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end

      describe 'get cash and carry' do
        it 'return cash and carry successfully' do
          get_cash_and_carry.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_cash_and_carry, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when c&c does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_cash_and_carry.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_cash_and_carry, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update cash and carry' do
        it 'returns updated cash and carry successfully' do
          update_cash_and_carry.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_cash_and_carry, id: 1, cash_and_carry: { name: Faker::Name.name }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when c&c does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_cash_and_carry.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_cash_and_carry, id: 1, cash_and_carry: {name: Faker::Name.name }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when request does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_cash_and_carry.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_cash_and_carry, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'delete cash and carry' do
        it 'return deleted cash and carry successfully' do
          change_state.any_instance.stub(:enact).and_return(stub_ok_response)
          delete :delete_cash_and_carry, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when c&c does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          delete :delete_cash_and_carry, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end

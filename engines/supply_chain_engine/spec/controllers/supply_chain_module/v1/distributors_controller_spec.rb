#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::DistributorsController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add_distributor) { SupplyChainModule::V1::AddDistributorApi }
      let(:update_distributor) { SupplyChainModule::V1::UpdateDistributorApi }
      let(:get_distributors) { SupplyChainModule::V1::GetDistributorsApi }
      let(:change_distributor_state_api) { SupplyChainModule::V1::ChangeDistributorStateApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:stub_ok_response) {
          {
            payload: {
              distributor: {
                name: Faker::Lorem.characters(6),
                cst_no: Faker::Lorem.characters(10),
                tin_no: Faker::Lorem.characters(10)
              }
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add distributor' do
        it 'returns created distributor successfully' do
          add_distributor.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_distributor, distributor: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add_distributor.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_distributor, distributor: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update distributor' do
        it 'returns updated distributor successfully' do
          update_distributor.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_distributor, id: 1, distributor: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_distributor.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_distributor, id: 1, distributor: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all distributors' do
        it 'returns all distributors successfully' do
          get_distributors.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_distributors
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end

      describe 'change distributor state' do
        it 'return updated distributor' do
          change_distributor_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          put :change_state, id: 1, distributor: { event: common_events::ACTIVATE }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_distributor_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, distributor: { event: common_events::ACTIVATE }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'delete distributor' do
        it 'return updated distributor' do
          change_distributor_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          delete :delete_distributor, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_distributor_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          delete :delete_distributor, id: 2
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe InventoryProductModule::V1::InventoryBrandPacksController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add_inventory_brand_pack) { InventoryProductModule::V1::AddInventoryBrandPackApi }
      let(:update_inventory_brand_pack) { InventoryProductModule::V1::UpdateInventoryBrandPackApi }
      let(:get_inventory_brand_packs) { InventoryProductModule::V1::GetInventoryBrandPacksApi }
      let(:change_inventory_brand_pack_state_api) { InventoryProductModule::V1::ChangeInventoryBrandPackStateApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:stub_ok_response) {
          {
            payload: {
              inventory_brand_packs: [
                name: Faker::Lorem.characters(6)
              ]
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add inventory_brand_pack' do
        it 'returns created inventory_brand_pack successfully' do
          add_inventory_brand_pack.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_inventory_brand_pack, inventory_brand_pack: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add_inventory_brand_pack.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_inventory_brand_pack, inventory_brand_pack: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update inventory_brand_pack' do
        it 'returns updated inventory_brand_pack successfully' do
          update_inventory_brand_pack.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_inventory_brand_pack, id: 1, inventory_brand_pack: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_inventory_brand_pack.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_inventory_brand_pack, id: 1, inventory_brand_pack: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all inventory_brand_packs' do
        it 'returns all inventory_brand_packs successfully' do
          get_inventory_brand_packs.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_inventory_brand_packs
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end

      describe 'change inventory_brand_pack state' do
        it 'return updated inventory_brand_pack' do
          change_inventory_brand_pack_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          put :change_state, id: 1, inventory_brand_pack: { event: common_events::ACTIVATE }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_inventory_brand_pack_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, inventory_brand_pack: { event: common_events::ACTIVATE }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'delete inventory_brand_pack' do
        it 'return updated inventory_brand_pack' do
          change_inventory_brand_pack_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          delete :delete_inventory_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_inventory_brand_pack_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          delete :delete_inventory_brand_pack, id: 2
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end

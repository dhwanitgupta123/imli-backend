#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::SubBrandController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add_sub_brand) { MasterProductModule::V1::AddSubBrandApi }
      let(:update_sub_brand) { MasterProductModule::V1::UpdateSubBrandApi }
      let(:get_sub_brands) { MasterProductModule::V1::GetSubBrandsApi }
      let(:get_sub_brand) { MasterProductModule::V1::GetSubBrandApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:change_sub_brand_state_api) { MasterProductModule::V1::ChangeSubBrandStateApi }
      let(:stub_ok_response) {
          {
            payload: {
              sub_brands: [
                {
                  sub_brand: {
                    name: Faker::Lorem.characters(6)
                  }
                }
              ]
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add sub_brand' do
        it 'returns created sub_brand successfully' do
          add_sub_brand.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_sub_brand, sub_brand: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add_sub_brand.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_sub_brand, sub_brand: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update sub_brand' do
        it 'returns updated sub_brand successfully' do
          update_sub_brand.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_sub_brand, id: 1, sub_brand: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_sub_brand.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_sub_brand, id: 1, sub_brand: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all sub_brands' do
        it 'returns all sub_brands successfully' do
          get_sub_brands.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_sub_brands
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end

      describe 'change sub brand state' do
        it 'return updated sub_brand' do
          change_sub_brand_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          put :change_state, id: 1, sub_brand: { event: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_sub_brand_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, sub_brand: { event: 0 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'delete sub_brand' do
        it 'return updated sub_brand' do
          change_sub_brand_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          delete :delete_sub_brand, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_sub_brand_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          delete :delete_sub_brand, id: 2
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get sub_brand' do
        it 'with valid argument' do
          get_sub_brand.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_sub_brand, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'with invalid arguments' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_sub_brand.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_sub_brand, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end

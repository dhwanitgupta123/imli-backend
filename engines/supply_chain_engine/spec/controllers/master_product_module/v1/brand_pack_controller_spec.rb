#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::BrandPackController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add_brand_pack) { MasterProductModule::V1::AddBrandPackApi }
      let(:update_brand_pack) { MasterProductModule::V1::UpdateBrandPackApi }
      let(:get_brand_packs) { MasterProductModule::V1::GetBrandPacksApi }
      let(:get_brand_pack) { MasterProductModule::V1::GetBrandPackApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:change_brand_pack_state_api) { MasterProductModule::V1::ChangeBrandPackStateApi }
      let(:stub_ok_response) {
          {
            payload: {
              brand_packs: [
                {
                  brand_pack: {
                    name: Faker::Lorem.characters(6)
                  }
                }
              ]
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add brand_pack' do
        it 'returns created brand_pack successfully' do
          add_brand_pack.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_brand_pack, brand_pack: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add_brand_pack.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_brand_pack, brand_pack: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update brand_pack' do
        it 'returns updated brand_pack successfully' do
          update_brand_pack.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_brand_pack, id: 1, brand_pack: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_brand_pack.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_brand_pack, id: 1, brand_pack: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all brand_packs' do
        it 'returns all brand_packs successfully' do
          get_brand_packs.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_brand_packs
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end

      describe 'change brand_pack state' do
        it 'return updated brand_pack' do
          change_brand_pack_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          put :change_state, id: 1, brand_pack: { event: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_brand_pack_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, brand_pack: { event: 0 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'delete brand_pack' do
        it 'return updated brand_pack' do
          change_brand_pack_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          delete :delete_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_brand_pack_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          delete :delete_brand_pack, id: 2
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get brand pack' do
        it 'with valid argument' do
          get_brand_pack.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'with invalid arguments' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_brand_pack.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end

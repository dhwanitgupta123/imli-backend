#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::VariantsController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add_variant) { MasterProductModule::V1::AddVariantApi }
      let(:update_variant) { MasterProductModule::V1::UpdateVariantApi }
      let(:get_variants) { MasterProductModule::V1::GetVariantsApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:stub_ok_response) {
          {
            payload: {
              variants: [
                {
                  variant: {
                    name: Faker::Lorem.characters(6),
                    initials: Faker::Lorem.characters(2)
                  }
                }
              ]
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add variant' do
        it 'returns created variant successfully' do
          add_variant.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_variant, variant: { name: Faker::Lorem.characters(6), initials: Faker::Lorem.characters(2) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add_variant.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_variant, variant: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update variant' do
        it 'returns updated variant successfully' do
          update_variant.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_variant, id: 1, variant: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_variant.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_variant, id: 1, variant: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all variants' do
        it 'returns all variants successfully' do
          get_variants.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_variants
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end
    end
  end
end

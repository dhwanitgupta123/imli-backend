#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceProductModule::V1::MarketplaceSellingPacksController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add) { MarketplaceProductModule::V1::AddMarketplaceSellingPackApi }
      let(:change_state) { MarketplaceProductModule::V1::ChangeMarketplaceSellingPackStateApi }
      let(:get_all_api) { MarketplaceProductModule::V1::GetAllMarketplaceSellingPackApi }
      let(:get_api) { MarketplaceProductModule::V1::GetMarketplaceSellingPackApi }
      let(:update) { MarketplaceProductModule::V1::UpdateMarketplaceSellingPackApi }
      let(:get_mpsp_by_department_and_category_api) { MarketplaceProductModule::V1::GetMpspByDepartmentAndCategoryApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:stub_ok_response) {
          {
            payload: {
              marketplace_selling_pack: {
                marketplace_selling_pack_id: Faker::Number.number(1)
              }
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add marketplace selling pack' do
        it 'returns created mbp successfully' do
          add.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_marketplace_selling_pack, marketplace_selling_pack: { marketplace_brand_pack_id: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_marketplace_selling_pack, marketplace_selling_pack: { marketplace_brand_pack_id: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'change_state mbp' do
        it 'returns updated mbp successfully' do
          change_state.any_instance.stub(:enact).and_return(stub_ok_response)
          put :change_state, id: 1, marketplace_selling_pack: { event: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, marketplace_selling_pack: { event: 1 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when soft delete request is sent' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, marketplace_selling_pack: { event: 3 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all mbps' do
        it 'returns all mbps successfully' do
          get_all_api.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_marketplace_selling_packs, order: 'name'
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_all_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_all_marketplace_selling_packs, order: 'name'
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get mbp' do
        it 'return mbp successfully' do
          get_api.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_marketplace_selling_pack, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when mbp does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_marketplace_selling_pack, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update mbp' do
        it 'returns updated mbp successfully' do
          update.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_marketplace_selling_pack, id: 1, marketplace_selling_pack: { marketplace_brand_pack_id: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when mbp does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_marketplace_selling_pack, id: 1, marketplace_selling_pack: {id: 1 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when request does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_marketplace_selling_pack, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'delete mbp' do
        it 'return deleted mbp successfully' do
          change_state.any_instance.stub(:enact).and_return(stub_ok_response)
          delete :delete_marketplace_selling_pack, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when mbp does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          delete :delete_marketplace_selling_pack, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get mbps by department category and sub_category' do
        it 'returns all mbps successfully' do
          get_mpsp_by_department_and_category_api.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_mpsp_by_department_and_category
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_mpsp_by_department_and_category_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_mpsp_by_department_and_category
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceProductModule::V1::MarketplaceBrandPacksController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add) { MarketplaceProductModule::V1::AddMarketplaceBrandPackApi }
      let(:create_mpbp_and_brand_pack_api) { MarketplaceProductModule::V1::CreateMpbpAndBrandPackApi }
      let(:change_state) { MarketplaceProductModule::V1::ChangeMarketplaceBrandPackStateApi }
      let(:get_all_api) { MarketplaceProductModule::V1::GetAllMarketplaceBrandPackApi }
      let(:get_api) { MarketplaceProductModule::V1::GetMarketplaceBrandPackApi }
      let(:update) { MarketplaceProductModule::V1::UpdateMarketplaceBrandPackApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:stub_ok_response) {
          {
            payload: {
              marketplace_brand_pack: {
                brand_pack_id: Faker::Number.number(1)
              }
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add marketplace brand pack' do
        it 'returns created wbp successfully' do
          add.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_marketplace_brand_pack, marketplace_brand_pack: { brand_pack_id: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_marketplace_brand_pack, marketplace_brand_pack: { brand_pack_id: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'create_mpbp_and_brand_pack' do
        it 'returns created bp successfully' do
          expect_any_instance_of(create_mpbp_and_brand_pack_api).to receive(:enact).and_return(stub_ok_response)
          post :create_mpbp_and_brand_pack, brand_pack: { sku: Faker::Lorem.word }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          expect_any_instance_of(create_mpbp_and_brand_pack_api).to receive(:enact).and_return(stub_bad_request_response)
          post :create_mpbp_and_brand_pack,  brand_pack: { sku: Faker::Lorem.word }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'change_state wbp' do
        it 'returns updated wbp successfully' do
          change_state.any_instance.stub(:enact).and_return(stub_ok_response)
          put :change_state, id: 1, marketplace_brand_pack: { event: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, marketplace_brand_pack: { event: 1 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when soft delete request is sent' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, marketplace_brand_pack: { event: 3 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all wbps' do
        it 'returns all wbps successfully' do
          get_all_api.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_marketplace_brand_packs, order: 'name'
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_all_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_all_marketplace_brand_packs, order: 'name'
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get wbp' do
        it 'return wbp successfully' do
          get_api.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_marketplace_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when wbp does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_marketplace_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update wbp' do
        it 'returns updated wbp successfully' do
          update.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_marketplace_brand_pack, id: 1, marketplace_brand_pack: { brand_pack_id: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when wbp does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_marketplace_brand_pack, id: 1, marketplace_brand_pack: {id: 1 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when request does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_marketplace_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'delete wbp' do
        it 'return deleted wbp successfully' do
          change_state.any_instance.stub(:enact).and_return(stub_ok_response)
          delete :delete_marketplace_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when wbp does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          delete :delete_marketplace_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end

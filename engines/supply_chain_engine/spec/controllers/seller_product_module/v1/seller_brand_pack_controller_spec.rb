#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SellerProductModule::V1::SellerBrandPackController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add) { SellerProductModule::V1::AddSellerBrandPackApi }
      let(:change_state) { SellerProductModule::V1::ChangeSellerBrandPackStateApi }
      let(:get_all_api) { SellerProductModule::V1::GetSellerBrandPacksApi }
      let(:get_api) { SellerProductModule::V1::GetSellerBrandPackApi }
      let(:update) { SellerProductModule::V1::UpdateSellerBrandPackApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:stub_ok_response) {
          {
            payload: {
              seller_brand_pack: {
              }
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add seller brand pack' do
        it 'returns created sbp successfully' do
          add.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_seller_brand_pack, seller_brand_pack: { seller_id: 1, brand_pack_id: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_seller_brand_pack, seller_brand_pack: { seller_id: nil, brand_pack_id: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'change_state sbp' do
        it 'returns updated sbp successfully' do
          change_state.any_instance.stub(:enact).and_return(stub_ok_response)
          put :change_state, id: 1, seller_brand_pack: { event: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, seller_brand_pack: { event: 1 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when soft delete request is sent' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, seller_brand_pack: { event: 3 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all sbps' do
        it 'returns all sbps successfully' do
          get_all_api.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_seller_brand_packs
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end

      describe 'get sbp' do
        it 'return sbp successfully' do
          get_api.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_seller_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when sbp does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_seller_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update sbp' do
        it 'returns updated sbp successfully' do
          update.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_seller_brand_pack, id: 1, seller_brand_pack: { seller_id: 1, brand_pack_id: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when sbp does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_seller_brand_pack, id: 1, seller_brand_pack: {id: 1 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when request does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_seller_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'delete sbp' do
        it 'return deleted sbp successfully' do
          change_state.any_instance.stub(:enact).and_return(stub_ok_response)
          delete :delete_seller_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when sbp does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          delete :delete_seller_brand_pack, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end
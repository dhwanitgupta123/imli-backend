#
# Module to handle all the functionalities related to supply chain
#
module SellerProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SellerProductModule::V1::SellerBrandPackDao do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:sbp_model) { SellerProductModule::V1::SellerBrandPack }
      let(:sbp_dao) { SellerProductModule::V1::SellerBrandPackDao.new }
      let(:events) { SupplyChainCommonModule::V1::CommonEvents }

      let(:seller_brand_pack_dao) { SellerProductModule::V1::SellerBrandPackDao.new }
      
      let(:company) { FactoryGirl.create(:company) }
      let(:brand) { FactoryGirl.create(:brand, company: company) }
      let(:sub_brand) { FactoryGirl.create(:sub_brand, brand: brand) }
      let(:product) { FactoryGirl.create(:product, sub_brand: sub_brand) }
      let(:department) { FactoryGirl.create(:department) }
      let(:category) { FactoryGirl.create(:category, department: department) }
      let(:sub_category) { FactoryGirl.create(:category, parent_category: category) }
      let(:brand_pack) { FactoryGirl.create(:brand_pack, product: product, sub_category: sub_category) }
      let(:seller_brand_pack) { FactoryGirl.create(:seller_brand_pack, brand_pack: brand_pack) }
      let(:request) { {} }

      context 'create sbp' do
        it 'with nil seller_id' do
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse_pricing = FactoryGirl.build(:warehouse_pricing)
          wbp = FactoryGirl.build(:warehouse_brand_pack, brand_pack: brand_pack, warehouse_pricings: [warehouse_pricing])
          wbp.inventory_brand_packs << ibp
          request = {
            sbp: {
              seller: nil,
              brand_pack: brand_pack
            },
            warehouse_brand_pack: wbp,
            pricing: {
              spat: Faker::Commerce.price,
              margin: Faker::Commerce.price,
              service_tax: Faker::Commerce.price,
              vat: Faker::Commerce.price,
              spat_per_unit: Faker::Commerce.price
            }
          }
          expect{ sbp_dao.create(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
          wbp.warehouse_pricings.delete_all
          wbp.delete
          ibp.inventory_pricing.delete
          ibp.delete
          brand_pack.delete
        end

        it 'with nil brand_pack_id' do
          brand_pack = FactoryGirl.create(:brand_pack)
          seller = FactoryGirl.create(:seller)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse_pricing = FactoryGirl.build(:warehouse_pricing)
          wbp = FactoryGirl.build(:warehouse_brand_pack, brand_pack: brand_pack, warehouse_pricings: [warehouse_pricing])
          wbp.inventory_brand_packs << ibp
          request = {
            sbp: {
              seller: seller,
              brand_pack: nil
            },
            warehouse_brand_pack: wbp
          }
          expect{ sbp_dao.create(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
          ibp.inventory_pricing.delete
          ibp.delete
          wbp.warehouse_pricings.delete_all
          wbp.delete
          brand_pack.delete
          seller.delete
        end

        it 'with new params ' do
          seller = FactoryGirl.create(:seller)
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse_pricing = FactoryGirl.build(:warehouse_pricing)
          wbp = FactoryGirl.build(:warehouse_brand_pack, brand_pack: brand_pack, warehouse_pricings: [warehouse_pricing])
          wbp.inventory_brand_packs << ibp
          request = {
            sbp: {
              seller: seller,
              brand_pack: brand_pack
            },
            warehouse_brand_pack: wbp
          }
          seller_brand_pack = sbp_dao.create(request)
          expect(seller_brand_pack.seller).to eq(request[:sbp][:seller])
          ibp.inventory_pricing.delete
          wbp.warehouse_pricings.delete_all
          ibp.delete
          wbp.delete
          seller_brand_pack.seller_pricing.delete
          seller_brand_pack.delete
          brand_pack.delete
          seller.delete
        end
      end

      context 'get all sbps' do
        let(:pagination_params) { { sort_by: 'id', order: 'ASC' } }
        it 'with wrong sort_by' do
          expect{ sbp_dao.paginated_seller_brand_pack(pagination_params.merge(sort_by: 'abc')) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with wrong order' do
          expect{ sbp_dao.paginated_seller_brand_pack(pagination_params.merge(sort_by: 'abc')) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          expect(sbp_dao.paginated_seller_brand_pack(pagination_params)).to_not be_nil
        end
      end

      context 'change wbp state' do
        it 'with wrong transition flow' do
          seller = FactoryGirl.create(:seller)
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse_pricing = FactoryGirl.build(:warehouse_pricing)
          wbp = FactoryGirl.build(:warehouse_brand_pack, brand_pack: brand_pack, warehouse_pricings: [warehouse_pricing])
          wbp.inventory_brand_packs << ibp
          request = {
            sbp: {
              seller: seller,
              brand_pack: brand_pack
            },
            warehouse_brand_pack: wbp
          }
          seller_brand_pack = sbp_dao.create(request)
          change_state = { id: seller_brand_pack.id, event: events::ACTIVATE }
          sbp_model.any_instance.stub(:trigger_event).and_raise(
            custom_error_util::PreConditionRequiredError)
          expect{ sbp_dao.change_state(change_state) }.to raise_error(
            custom_error_util::PreConditionRequiredError)
          ibp.inventory_pricing.delete
          wbp.warehouse_pricings.delete_all
          ibp.delete
          wbp.delete
          seller_brand_pack.seller_pricing.delete
          seller_brand_pack.delete
          brand_pack.delete
          seller.delete
        end

        it 'with wrong event' do
          seller = FactoryGirl.create(:seller)
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse_pricing = FactoryGirl.build(:warehouse_pricing)
          wbp = FactoryGirl.build(:warehouse_brand_pack, brand_pack: brand_pack, warehouse_pricings: [warehouse_pricing])
          wbp.inventory_brand_packs << ibp
          request = {
            sbp: {
              seller: seller,
              brand_pack: brand_pack
            },
            warehouse_brand_pack: wbp
          }
          seller_brand_pack = sbp_dao.create(request)
          change_state = { id: seller_brand_pack.id, event: 5 }
          expect{ sbp_dao.change_state(change_state) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
          ibp.inventory_pricing.delete
          wbp.warehouse_pricings.delete_all
          ibp.delete
          wbp.delete
          seller_brand_pack.seller_pricing.delete
          seller_brand_pack.delete
          brand_pack.delete
          seller.delete
        end

        it 'with correct request' do
          seller = FactoryGirl.create(:seller)
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse_pricing = FactoryGirl.build(:warehouse_pricing)
          wbp = FactoryGirl.build(:warehouse_brand_pack, brand_pack: brand_pack, warehouse_pricings: [warehouse_pricing])
          wbp.inventory_brand_packs << ibp
          request = {
            sbp: {
              seller: seller,
              brand_pack: brand_pack
            },
            warehouse_brand_pack: wbp
          }
          seller_brand_pack = sbp_dao.create(request)
          change_state = { id: seller_brand_pack.id, event: events::ACTIVATE }
          expect(sbp_dao.change_state(change_state)).to_not be_nil
          ibp.inventory_pricing.delete
          wbp.warehouse_pricings.delete_all
          ibp.delete
          wbp.delete
          seller_brand_pack.seller_pricing.delete
          seller_brand_pack.delete
          brand_pack.delete
          seller.delete
        end
      end

      context 'get wbp' do
        it 'with wrong id' do
          expect{ sbp_dao.get_seller_brand_pack_by_id(Faker::Number.number(2)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          seller = FactoryGirl.create(:seller)
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse_pricing = FactoryGirl.build(:warehouse_pricing)
          wbp = FactoryGirl.build(:warehouse_brand_pack, brand_pack: brand_pack, warehouse_pricings: [warehouse_pricing])
          wbp.inventory_brand_packs << ibp
          request = {
            sbp: {
              seller: seller,
              brand_pack: brand_pack
            },
            warehouse_brand_pack: wbp
          }
          seller_brand_pack = sbp_dao.create(request)
          expect(sbp_dao.get_seller_brand_pack_by_id(seller_brand_pack.id)).to_not be_nil
          ibp.inventory_pricing.delete
          wbp.warehouse_pricings.delete_all
          ibp.delete
          wbp.delete
          seller_brand_pack.seller_pricing.delete
          seller_brand_pack.delete
          brand_pack.delete
          seller.delete
        end
      end

      context 'update wbp' do
        it 'with correct params' do
          seller = FactoryGirl.create(:seller)
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse_pricing = FactoryGirl.build(:warehouse_pricing)
          wbp = FactoryGirl.build(:warehouse_brand_pack, brand_pack: brand_pack, warehouse_pricings: [warehouse_pricing])
          wbp.inventory_brand_packs << ibp
          request = {
            sbp: {
              seller: seller,
              brand_pack: brand_pack
            },
            warehouse_brand_pack: wbp
          }
          seller_brand_pack = sbp_dao.create(request)
          ibp.brand_pack = brand_pack
          wbp.brand_pack = brand_pack
          expect(sbp_dao.update({ sbp: { seller_id: seller.id } }, seller_brand_pack)).to_not be_nil
          ibp.inventory_pricing.delete
          wbp.warehouse_pricings.delete_all
          ibp.delete
          wbp.delete
          seller_brand_pack.seller_pricing.delete
          seller_brand_pack.delete
          brand_pack.delete
          seller.delete
        end

        it 'with correct params' do
          seller = FactoryGirl.create(:seller)
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse_pricing = FactoryGirl.build(:warehouse_pricing)
          wbp = FactoryGirl.build(:warehouse_brand_pack, brand_pack: brand_pack, warehouse_pricings: [warehouse_pricing])
          wbp.inventory_brand_packs << ibp
          request = {
            sbp: {
              seller: seller,
              brand_pack: brand_pack
            },
            warehouse_brand_pack: wbp
          }
          seller_brand_pack = sbp_dao.create(request)
          ibp.brand_pack = brand_pack
          wbp.brand_pack = brand_pack
          args = { sbp: { brand_pack_id: brand_pack.id }, wbp: wbp }
          expect(sbp_dao.update(args, seller_brand_pack)).to_not be_nil
          ibp.inventory_pricing.delete
          wbp.warehouse_pricings.delete_all
          ibp.delete
          wbp.delete
          seller_brand_pack.seller_pricing.delete
          seller_brand_pack.delete
          brand_pack.delete
          seller.delete
        end
      end
    end
  end
end

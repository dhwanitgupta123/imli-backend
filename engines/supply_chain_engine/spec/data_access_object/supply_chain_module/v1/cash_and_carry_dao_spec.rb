#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::CashAndCarryDao do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:cash_and_carry_model) { SupplyChainModule::V1::CashAndCarry }
      let(:events) { SupplyChainCommonModule::V1::CommonEvents }

      let(:cash_and_carry_dao) { SupplyChainModule::V1::CashAndCarryDao.new }

      let(:cash_and_carry) { FactoryGirl.build(:cash_and_carry) }

      let(:request) {
        {
          name: Faker::Company.name
        }
      }

      context 'create cash_and_carry' do
        it 'with invalid cash_and_carry' do
          expect{ cash_and_carry_dao.create(request.merge(name: nil)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with new params ' do
          cash_and_carry = cash_and_carry_dao.create(request)
          expect(cash_and_carry.name).to eq(cash_and_carry[:name])
          cash_and_carry.destroy
        end
      end

      context 'get all cash_and_carry' do
        let(:pagination_params) { { sort_by: 'name', order: 'ASC' } }
        it 'with wrong sort_by' do
          expect{ cash_and_carry_dao.paginated_cash_and_carry(pagination_params.merge(sort_by: 'abc')) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with wrong order' do
          expect{ cash_and_carry_dao.paginated_cash_and_carry(pagination_params.merge(sort_by: 'abc')) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          expect(cash_and_carry_dao.paginated_cash_and_carry(pagination_params)).to_not be_nil
        end
      end

      context 'change cash_and_carry state' do
        let(:cash_and_carry) { FactoryGirl.create(:cash_and_carry) }
        let(:change_state) { { id: cash_and_carry.id, event: events::ACTIVATE }}
        it 'with wrong transition flow' do
          cash_and_carry_model.any_instance.stub(:trigger_event).and_raise(
            custom_error_util::PreConditionRequiredError)
          expect{ cash_and_carry_dao.change_state(change_state) }.to raise_error(
            custom_error_util::PreConditionRequiredError)
        end

        it 'with wrong event' do
          expect{ cash_and_carry_dao.change_state(change_state.merge(event: 5)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with correct request' do
          expect(cash_and_carry_dao.change_state(change_state)).to_not be_nil
        end
      end

      context 'get cash_and_carry' do
        let(:cash_and_carry) { FactoryGirl.create(:cash_and_carry) }
        let(:request) { cash_and_carry.id }
        it 'with wrong id' do
          expect{ cash_and_carry_dao.get_cash_and_carry_by_id(Faker::Number.number(2)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          expect(cash_and_carry_dao.get_cash_and_carry_by_id(request)).to_not be_nil
        end
      end

      context 'update cash_and_carry' do
        let(:cash_and_carry) { FactoryGirl.create(:cash_and_carry) }
        let(:request) { { name: Faker::Name.name } }
        it 'with wrong request' do
          expect{ cash_and_carry_dao.update(request.merge(name: nil), cash_and_carry) }.to raise_error(
            custom_error_util::InvalidArgumentsError)   
        end

        it 'with correct params' do
          expect(cash_and_carry_dao.update(request, cash_and_carry)).to_not be_nil
        end
      end
    end
  end
end

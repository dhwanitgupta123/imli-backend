#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SupplyChainModule::V1::InventoryDao, type: :dao do
      let(:inventory_dao) { SupplyChainModule::V1::InventoryDao.new }
      let(:inventory_name) { Faker::Company.name }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:common_states) { SupplyChainCommonModule::V1::CommonStates }
      let(:distributor) { FactoryGirl.create(:distributor) }
      let(:inventory_request) {
        {
          name: Faker::Company.name,
          distributor_id: distributor.id
        }
      }

      context 'create inventory' do
        it 'with valid params' do
          inventory = inventory_dao.create(inventory_request)
          expect(inventory.name).to eq(inventory_request[:name])
          inventory.destroy
        end

        it 'with invalid distributor' do
          expect { inventory_dao.create(inventory_request.merge(distributor_id: nil)) }
            .to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update inventory' do
        let(:new_name) { Faker::Company.name } 
        let(:inventory_before) { FactoryGirl.build(:inventory, distributor: distributor) }
        it 'with valid inventory' do
          inventory = inventory_dao.update({ name: new_name }, inventory_before)
          expect(inventory.name).to eq(new_name)
          inventory.destroy
        end

        it 'with invalid distributor name' do
          expect { inventory_dao.update({ distributor_id: nil }, inventory_before) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update inventory state' do
        it 'from inactive to active and active to inactive' do
          inventory = FactoryGirl.create(:inventory, distributor: distributor)
          inventory = inventory_dao.change_state(inventory, common_events::ACTIVATE)
          expect(inventory.status).to eq(common_states::ACTIVE)
          inventory = inventory_dao.change_state(inventory, common_events::DEACTIVATE)
          expect(inventory.status).to eq(common_states::INACTIVE)
          inventory.delete
        end

        it 'from active to soft_delete' do
          inventory = FactoryGirl.create(:inventory, distributor: distributor)
          active_inventory = inventory_dao.change_state(inventory, common_events::ACTIVATE)
          inventory = inventory_dao.change_state(active_inventory, common_events::SOFT_DELETE)
          expect(inventory.status).to eq(common_states::DELETED)
          inventory.delete
        end

        it 'from inactive to soft_delete' do
          inventory = FactoryGirl.create(:inventory, distributor: distributor)
          inventory = inventory_dao.change_state(inventory, common_events::SOFT_DELETE)
          expect(inventory.status).to eq(common_states::DELETED)
          inventory.delete
        end

        it 'raise error change state from S1 to S1' do
          inventory = FactoryGirl.create(:inventory, distributor: distributor)
          expect{ inventory_dao.change_state(inventory, common_events::DEACTIVATE) }.
          to raise_error(custom_error_util::PreConditionRequiredError)
          inventory.delete
        end
      end
    end
  end
end

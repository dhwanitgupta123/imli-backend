#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::WarehouseDao do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:warehouse_model) { SupplyChainModule::V1::Warehouse }
      let(:events) { SupplyChainCommonModule::V1::CommonEvents }

      let(:warehouse_dao) { SupplyChainModule::V1::WarehouseDao.new }

      let(:warehouse) { FactoryGirl.build(:warehouse) }

      let(:request) {
        {
          name: Faker::Company.name
        }
      }

      context 'create warehouse' do
        it 'with invalid warehouse' do
          expect{ warehouse_dao.create(request.merge(name: nil)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with invalid warehouse' do
          expect{ warehouse_dao.create(request.merge(cash_and_carry: nil)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with new params ' do
          cash_and_carry = FactoryGirl.create(:cash_and_carry)
          warehouse = warehouse_dao.create(request.merge(cash_and_carry: cash_and_carry))
          expect(warehouse.name).to eq(request[:name])
        end
      end

      context 'get all warehouses' do
        let(:pagination_params) { { sort_by: 'name', order: 'ASC' } }
        it 'with correct params' do
          expect(warehouse_dao.paginated_warehouse(pagination_params)).to_not be_nil
        end
      end

      context 'change warehouse state' do
        let(:warehouse) { FactoryGirl.create(:warehouse) }
        let(:change_state) { { id: warehouse.id, event: events::ACTIVATE }}
        it 'with wrong transition flow' do
          warehouse_model.any_instance.stub(:trigger_event).and_raise(
            custom_error_util::PreConditionRequiredError)
          expect{ warehouse_dao.change_state(change_state) }.to raise_error(
            custom_error_util::PreConditionRequiredError)
        end

        it 'with wrong event' do
          expect{ warehouse_dao.change_state(change_state.merge(event: 5)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with correct request' do
          expect(warehouse_dao.change_state(change_state)).to_not be_nil
        end
      end

      context 'get warehouse' do
        let(:warehouse) { FactoryGirl.create(:warehouse) }
        let(:request) { warehouse.id }
        it 'with wrong id' do
          expect{ warehouse_dao.get_warehouse_by_id(Faker::Number.number(2)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          expect(warehouse_dao.get_warehouse_by_id(request)).to_not be_nil
        end
      end

      context 'update warehouse' do
        let(:warehouse) { FactoryGirl.create(:warehouse) }
        let(:request) { { name: Faker::Name.name } }
        it 'with wrong request' do
          expect{ warehouse_dao.update(request.merge(name: nil), warehouse) }.to raise_error(
            custom_error_util::InvalidArgumentsError)   
        end

        it 'with correct params' do
          expect(warehouse_dao.update(request, warehouse)).to_not be_nil
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SupplyChainModule::V1::CompanyDao, type: :dao do
      let(:company_dao) { SupplyChainModule::V1::CompanyDao.new }
      let(:company_name) { Faker::Company.name }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:common_states) { SupplyChainCommonModule::V1::CommonStates }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:company_request) {
        {
          name: Faker::Company.name,
          logo_url: Faker::Company.logo,
          vat_number: Faker::Company.duns_number,
          tin_number: Faker::Company.duns_number,
          cst: Faker::Company.duns_number,
          account_details: Faker::Business.credit_card_number,
          trade_promotion_margin: Faker::Commerce.price,
          data_sharing_margin: Faker::Commerce.price,
          incentives: Faker::Lorem.characters(10),
          reconciliation_period: Faker::Time.between(DateTime.now - 1, DateTime.now),
          legal_document_url: Faker::Internet.url,
          initials: Faker::Lorem.characters(2)
        }
      }

      context 'create company' do
        it 'with valid params' do
          company = company_dao.create(company_request)
          expect(company.name).to eq(company_request[:name])
          company.destroy
        end

        it 'with invalid name' do
          expect { company_dao.create(company_request.merge(name: nil)) }
            .to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update company' do
        let(:new_name) { Faker::Company.name }
        let(:company_before) { FactoryGirl.build(:company, name: company_name) }
        it 'with valid company' do
          company = company_dao.update({ name: new_name }, company_before)
          expect(company.name).to eq(new_name)
          company.destroy
        end

        it 'with invalid company name' do
          expect { company_dao.update(nil, company_before) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update company state' do
        it 'from inactive to active and active to inactive' do
          company = FactoryGirl.create(:company, name: company_name)
          company = company_dao.change_company_state(company.id, common_events::DEACTIVATE)
          expect(company.status).to eq(common_states::INACTIVE)
          company = company_dao.change_company_state(company.id, common_events::ACTIVATE)
          expect(company.status).to eq(common_states::ACTIVE)
          company.delete
        end

        it 'from active to soft_delete' do
          company = FactoryGirl.create(:company, name: company_name)
          company = company_dao.change_company_state(company.id, common_events::SOFT_DELETE)
          expect(company.status).to eq(common_states::DELETED)
          company.delete
        end

        it 'from inactive to soft_delete' do
          company = FactoryGirl.create(:company, name: company_name)
          company = company_dao.change_company_state(company.id, common_events::SOFT_DELETE)
          expect(company.status).to eq(common_states::DELETED)
          company.delete
        end

        it 'raise error change state from S1 to S1' do
          company = FactoryGirl.create(:company, name: company_name)
          expect{ company_dao.change_company_state(company.id, common_events::ACTIVATE) }.
          to raise_error(custom_error_util::PreConditionRequiredError)
          company.delete
        end
      end

      context 'return company corresponding to id' do
        it 'return valid company' do
          company = FactoryGirl.create(:company, name: company_name)
          response = company_dao.get_company_by_id(company.id)
          expect(response).to eq(company)
          company.delete
        end

        it 'return nil with invalid args' do
          response = company_dao.get_company_by_id(Faker::Lorem.characters(10))
          expect(response).to eq(nil)
        end
      end
    end
  end
end

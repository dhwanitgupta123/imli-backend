#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe InventoryProductModule::V1::InventoryBrandPackDao, type: :dao do
      let(:inventory_brand_pack_dao) { InventoryProductModule::V1::InventoryBrandPackDao.new }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:common_states) { SupplyChainCommonModule::V1::CommonStates }
      let(:inventory_brand_pack) { FactoryGirl.create(:inventory_brand_pack) }
      let(:distributor) { FactoryGirl.build(:distributor, name: 'test') }
      let(:second_inventory) { FactoryGirl.create(:inventory, name: 'test', distributor: distributor) }
      let(:ibp_model) { InventoryProductModule::V1::InventoryBrandPack }
      let(:ibp_pricing_dao) { InventoryPricingModule::V1::InventoryPricingDao }

      let(:inventory_brand_pack_request) {
        {
          inventory_id: inventory_brand_pack.inventory_id,
          brand_pack_id: inventory_brand_pack.brand_pack_id,
          pricing: {
            net_landing_price: Faker::Commerce.price,
            margin: Faker::Commerce.price,
            service_tax: Faker::Commerce.price,
            vat: Faker::Commerce.price,
            spat_per_unit: Faker::Commerce.price
          }
        }
      }

      context 'create inventory_brand_pack' do
        it 'with valid params' do
          inventory_brand_pack = inventory_brand_pack_dao.create(inventory_brand_pack_request)
          expect(inventory_brand_pack.inventory_id).to eq(inventory_brand_pack_request[:inventory_id])
          inventory_brand_pack.destroy
        end

        it 'with invalid inventory' do
          expect { inventory_brand_pack_dao.create(inventory_brand_pack_request.merge(inventory_id: nil)) }
            .to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update inventory_brand_pack' do
        it 'with valid brand_pack' do
          inventory_brand_pack_before = inventory_brand_pack_dao.create(inventory_brand_pack_request)
          inventory_brand_pack = inventory_brand_pack_dao.update({ inventory_id: second_inventory.id }, inventory_brand_pack_before)
          expect(inventory_brand_pack.inventory_id).to eq(second_inventory.id)
          inventory_brand_pack.destroy
        end

        it 'with invalid distributor name' do
          inventory_brand_pack_before = inventory_brand_pack_dao.create(inventory_brand_pack_request)
          expect { inventory_brand_pack_dao.update({ inventory_id: nil }, inventory_brand_pack_before) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
            inventory_brand_pack_before.destroy
        end
      end

      context 'update inventory_brand_pack state' do
        it 'from inactive to active and active to inactive' do
          inventory_brand_pack = inventory_brand_pack_dao.create(inventory_brand_pack_request)
          inventory_brand_pack = inventory_brand_pack_dao.change_state(inventory_brand_pack, common_events::ACTIVATE)
          expect(inventory_brand_pack.status).to eq(common_states::ACTIVE)
          inventory_brand_pack = inventory_brand_pack_dao.change_state(inventory_brand_pack, common_events::DEACTIVATE)
          expect(inventory_brand_pack.status).to eq(common_states::INACTIVE)
        end

        it 'from active to soft_delete' do
          inventory_brand_pack = inventory_brand_pack_dao.create(inventory_brand_pack_request)
          active_inventory_brand_pack = inventory_brand_pack_dao.change_state(inventory_brand_pack, common_events::ACTIVATE)
          inventory_brand_pack = inventory_brand_pack_dao.change_state(active_inventory_brand_pack, common_events::SOFT_DELETE)
          expect(inventory_brand_pack.status).to eq(common_states::DELETED)
        end

        it 'from inactive to soft_delete' do
          inventory_brand_pack = inventory_brand_pack_dao.create(inventory_brand_pack_request)
          inventory_brand_pack = inventory_brand_pack_dao.change_state(inventory_brand_pack, common_events::SOFT_DELETE)
          expect(inventory_brand_pack.status).to eq(common_states::DELETED)
        end

        it 'raise error change state from S1 to S1' do
          inventory_brand_pack = inventory_brand_pack_dao.create(inventory_brand_pack_request)
          expect{ inventory_brand_pack_dao.change_state(inventory_brand_pack, common_events::DEACTIVATE) }.
          to raise_error(custom_error_util::PreConditionRequiredError)
          inventory_brand_pack.inventory_pricing.delete
          inventory_brand_pack.delete
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MasterProductModule::V1::ProductDao, type: :dao do
      let(:product_dao) { MasterProductModule::V1::ProductDao.new }
      let(:product_name) { Faker::Commerce.product_name }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:common_states) { SupplyChainCommonModule::V1::CommonStates }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:variant_name) { Faker::Commerce.name }

      let(:product_request) {
        {
          name: Faker::Commerce.product_name,
          logo_url: Faker::Company.logo,
          sub_brand: nil,
          initials: Faker::Lorem.characters(2)
        }
      }

      context 'create product' do
        it 'with valid params' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          product = product_dao.create(product_request.merge(sub_brand: sub_brand))
          expect(product.name).to eq(product_request[:name])
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy
        end

        it 'with invalid name' do
          expect { product_dao.create(product_request.merge(name: nil)) }
            .to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update product' do
        let(:new_name) { Faker::Company.name }
        let(:company) { FactoryGirl.create(:company) }
        let(:brand) { FactoryGirl.create(:brand, company: company) }
        let(:sub_brand) { FactoryGirl.create(:sub_brand, brand: brand) }
        let(:product_before) { FactoryGirl.build(:product, name: product_name, sub_brand: sub_brand) }
        let(:variant_before) { FactoryGirl.create(:variant, name: variant_name) }
        it 'with valid product' do
          product = product_dao.update({ name: new_name, variant: variant_before }, product_before)
          expect(product.name).to eq(new_name)
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy
        end

        it 'with invalid product name' do
          expect { product_dao.update(nil, product_before) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update product state' do
        
        it 'from inactive to active and active to inactive' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          product = FactoryGirl.create(:product, sub_brand: sub_brand)
          product = product_dao.change_product_state(product.id, common_events::DEACTIVATE)
          expect(product.status).to eq(common_states::INACTIVE)
          product = product_dao.change_product_state(product.id, common_events::ACTIVATE)
          expect(product.status).to eq(common_states::ACTIVE)
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy
        end

        it 'from active to soft_delete' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          product = FactoryGirl.create(:product, sub_brand: sub_brand)
          product = product_dao.change_product_state(product.id, common_events::SOFT_DELETE)
          expect(product.status).to eq(common_states::DELETED)
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy
        end

        it 'from inactive to soft_delete' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          product = FactoryGirl.create(:product, sub_brand: sub_brand)
          product = product_dao.change_product_state(product.id, common_events::SOFT_DELETE)
          expect(product.status).to eq(common_states::DELETED)
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy        
        end

        it 'raise error change state from S1 to S1' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          product = FactoryGirl.create(:product, sub_brand: sub_brand)
          expect{ product_dao.change_product_state(product.id, common_events::ACTIVATE) }.
          to raise_error(custom_error_util::PreConditionRequiredError)
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy        
        end
      end

      context 'return product corresponding to id' do
        it 'return valid product' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          product = FactoryGirl.create(:product, sub_brand: sub_brand)
          response = product_dao.get_product_by_id(product.id)
          expect(response).to eq(product)
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy 
        end

        it 'return nil with invalid args' do
          response = product_dao.get_product_by_id(Faker::Lorem.characters(10))
          expect(response).to eq(nil)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MasterProductModule::V1::VariantDao, type: :dao do
      let(:variant_dao) { MasterProductModule::V1::VariantDao.new }
      let(:variant_name) { Faker::Commerce.name }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }

      let(:variant_request) {
        {
          name: Faker::Company.name,
          initials: Faker::Lorem.characters(2)
        }
      }

      context 'create variant' do
        it 'with valid params' do
          variant = variant_dao.create(variant_request)
          expect(variant.name).to eq(variant_request[:name])
          variant.destroy
        end

        it 'with invalid name' do
          expect { variant_dao.create(variant_request.merge(name: nil)) }
            .to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end

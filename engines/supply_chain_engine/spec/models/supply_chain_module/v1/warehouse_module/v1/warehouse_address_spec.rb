module SupplyChainModule::V1
  module WarehouseModule::V1
    require 'rails_helper'

    RSpec.describe WarehouseAddress, type: :model do
      let(:area) { FactoryGirl.build_stubbed(:area) }

      it 'is valid' do
        FactoryGirl.build(:warehouse_address, warehouse_id: Faker::Number.number(2), area: area).should be_valid
      end

      it 'is invalid without address1' do
        FactoryGirl.build(:warehouse_address, address_line1: nil, warehouse_id: Faker::Number.number(2), area: area).
          should_not be_valid
      end

      it 'is invalid without area' do
        FactoryGirl.build(:warehouse_address, area: nil, warehouse_id: Faker::Number.number(2)).should_not be_valid
      end
    end
  end
end

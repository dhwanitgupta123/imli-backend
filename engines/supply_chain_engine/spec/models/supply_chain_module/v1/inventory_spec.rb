#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SupplyChainModule::V1::Inventory, type: :model do
      let(:inventory_name) { Faker::Company.name }
      let(:distributor) { FactoryGirl.build_stubbed(:distributor) }
      subject { FactoryGirl.build(:inventory, distributor: distributor) }

      it { should be_valid }

      it 'with nil distributor' do
        FactoryGirl.build(:inventory, name: inventory_name, distributor: nil).should_not be_valid
     end
    end
  end
end

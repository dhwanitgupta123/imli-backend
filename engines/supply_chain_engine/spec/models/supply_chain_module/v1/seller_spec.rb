#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SupplyChainModule::V1::Seller, type: :model do
      let(:seller_name) { Faker::Name.name }
      subject { FactoryGirl.build(:seller) }

      it { should be_valid }

      it 'with nil name' do
        FactoryGirl.build(:seller, name: nil).should_not be_valid
      end

      it 'with duplicate names' do
        FactoryGirl.create(:seller, name: seller_name)
        FactoryGirl.build(:seller, name: seller_name).should_not be_valid
      end
    end
  end
end
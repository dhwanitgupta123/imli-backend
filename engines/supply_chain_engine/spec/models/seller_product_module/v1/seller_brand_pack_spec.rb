#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SellerProductModule::V1::SellerBrandPack, type: :model do
      let(:ibp) { FactoryGirl.build_stubbed(:inventory_brand_pack) }
      let(:wbp) { FactoryGirl.build_stubbed(:warehouse_brand_pack) }
      let(:warehouse_pricing) { FactoryGirl.build_stubbed(:warehouse_pricing, warehouse_brand_pack: wbp) }
      let(:sbp) { FactoryGirl.build_stubbed(:seller_brand_pack) }

      it 'should be valid' do
        wbp.inventory_brand_packs << ibp
        sbp.warehouse_brand_packs << wbp
        expect(sbp).to be_valid
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe InventoryProductModule::V1::InventoryBrandPack, type: :model do
      let(:distributor) { FactoryGirl.build_stubbed(:distributor) }
      let(:inventory) { FactoryGirl.build_stubbed(:inventory, distributor: distributor) }
      let(:company) { FactoryGirl.build_stubbed(:company) }
      let(:brand) { FactoryGirl.build_stubbed(:brand, company: company) }
      let(:sub_brand) { FactoryGirl.build_stubbed(:sub_brand, brand: brand) }
      let(:product) { FactoryGirl.build_stubbed(:product, sub_brand: sub_brand) }
      let(:brand_pack) { FactoryGirl.build(:brand_pack, product: product) }

      subject { FactoryGirl.build(:inventory_brand_pack, inventory: inventory, brand_pack: brand_pack) }

      it { should be_valid }

      it 'with nil inventory' do
        FactoryGirl.build(:inventory_brand_pack, inventory: nil, brand_pack: brand_pack).should_not be_valid
      end

      it 'with nil brand_pack' do
        FactoryGirl.build(:inventory_brand_pack, inventory: inventory, brand_pack: nil).should_not be_valid
      end
    end
  end
end

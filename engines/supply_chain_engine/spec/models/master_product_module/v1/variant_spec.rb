#
# Module to handle all the functionalities related to supply chain
#
module MasterProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MasterProductModule::V1::Variant, type: :model do

      subject { FactoryGirl.build(:variant) }

      it { should be_valid }

      it 'with nil variant name' do
        FactoryGirl.build(:variant, name: nil).should_not be_valid
      end

      it 'with nil initials' do
        FactoryGirl.build(:variant, initials: nil).should_not be_valid
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module MasterProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MasterProductModule::V1::Product, type: :model do
      let(:company) { FactoryGirl.build_stubbed(:company) }
      let(:brand) { FactoryGirl.build_stubbed(:brand, company: company) }
      let(:sub_brand) { FactoryGirl.build_stubbed(:sub_brand, brand: brand) }

      subject { FactoryGirl.build(:product, sub_brand: sub_brand) }

      it { should be_valid }

      it 'with nil product name' do
        FactoryGirl.build(:product, sub_brand: sub_brand, name: nil).should_not be_valid
      end

      it 'with nil sub_brand' do
        FactoryGirl.build(:product, sub_brand: nil).should_not be_valid
      end

      it 'with nil initials' do
        FactoryGirl.build(:product, initials: nil).should_not be_valid
      end
    end
  end
end

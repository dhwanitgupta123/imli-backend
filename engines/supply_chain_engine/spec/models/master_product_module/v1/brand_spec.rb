#
# Module to handle all the functionalities related to supply chain
#
module MasterProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MasterProductModule::V1::Brand, type: :model do
      let(:company) { FactoryGirl.build_stubbed(:company) }
      subject { FactoryGirl.build(:brand, company: company) }

      it { should be_valid }

      it 'with nil brand name' do
        FactoryGirl.build(:brand, company: company, name: nil).should_not be_valid
      end

      it 'with nil company' do
        FactoryGirl.build(:brand, company: nil).should_not be_valid
      end

      it 'with nil initials' do
        FactoryGirl.build(:brand, initials: nil).should_not be_valid
      end
    end
  end
end

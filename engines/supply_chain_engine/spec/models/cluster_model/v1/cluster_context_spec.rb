#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # Model for clusters table
    #
    require 'rails_helper'

    RSpec.describe ClusterModule::V1::ClusterContext, type: :model do

      it 'should be valid' do
        cluster_context = FactoryGirl.build(:cluster_context)
        expect(cluster_context).to be_valid
      end

      it 'should not be valid without cluster' do
        cluster_context = FactoryGirl.build(:cluster_context, cluster: nil)
        expect(cluster_context).not_to be_valid
      end
    end
  end
end
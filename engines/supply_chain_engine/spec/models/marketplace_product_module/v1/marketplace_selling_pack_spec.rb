#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MarketplaceProductModule::V1::MarketplaceSellingPack, type: :model do
      let(:mpsp) { FactoryGirl.build(:marketplace_selling_pack) }

      it 'should be valid' do
        expect(mpsp).to be_valid
      end
    end
  end
end
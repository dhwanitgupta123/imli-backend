#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MarketplaceProductModule::V1::MarketplaceBrandPack, type: :model do
      let(:mpbp) { FactoryGirl.build(:marketplace_brand_pack) }

      it 'should be valid' do
        expect(mpbp).to be_valid
      end
    end
  end
end
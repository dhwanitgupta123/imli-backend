#
# Module to handle all the functionalities related to warehouse product module
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe WarehouseProductModule::V1::WarehouseBrandPack, type: :model do
      let(:ibp) { FactoryGirl.build_stubbed(:inventory_brand_pack) }
      let(:wbp) { FactoryGirl.build(:warehouse_brand_pack) }
      let(:warehouse_pricing) { FactoryGirl.build(:warehouse_pricing) }

      it 'should be valid' do
        wbp.inventory_brand_packs << ibp
        wbp.warehouse_pricings << warehouse_pricing
        expect(wbp).to be_valid
      end
    end
  end
end
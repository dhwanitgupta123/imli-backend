#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # Model for clusters table
    #
    FactoryGirl.define do
      factory :cluster, class: ClusterModule::V1::Cluster do
        label Faker::Lorem.characters(9)
        cluster_type Faker::Number.number(1)
      end
    end
  end
end

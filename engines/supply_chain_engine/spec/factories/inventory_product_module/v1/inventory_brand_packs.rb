#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    FactoryGirl.define do
      factory :inventory_brand_pack, class: InventoryProductModule::V1::InventoryBrandPack do
      	association :inventory, factory: :inventory
      	association :brand_pack, factory: :brand_pack
        association :inventory_pricing, factory: :inventory_pricing
      end
    end
  end
end

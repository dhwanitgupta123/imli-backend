#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    FactoryGirl.define do
      factory :brand_pack, class: MasterProductModule::V1::BrandPack do
        sku Faker::Company.ein
        mrp Faker::Commerce.price
        sku_size Faker::Lorem.characters(10)
        unit Faker::Lorem.characters(5)
        description Faker::Lorem.sentence
        shelf_life Faker::Internet.slug(Faker::Number.between(1, 12).to_s, 'months')
        article_code Faker::Number.number(5).to_s
        retailer_pack Faker::Number.number(2)
        brand_pack_code Faker::Number.number(4).to_s
        images [Faker::Number.number(1)]
        association :product, factory: :product
        association :sub_category, factory: :category
      end
    end
  end
end

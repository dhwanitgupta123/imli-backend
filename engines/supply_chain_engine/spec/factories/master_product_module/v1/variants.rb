#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    FactoryGirl.define do
      factory :variant, class: MasterProductModule::V1::Variant do
        name Faker::Commerce.product_name
        initials Faker::Lorem.characters(2)
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    FactoryGirl.define do
      factory :sub_brand, class: MasterProductModule::V1::SubBrand do
        name Faker::Company.name
        logo_url Faker::Company.logo
        association :brand, factory: :brand
        initials Faker::Lorem.characters(2)
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    FactoryGirl.define do
      factory :product, class: MasterProductModule::V1::Product do
        name Faker::Commerce.product_name
        logo_url Faker::Company.logo
        association :sub_brand, factory: :sub_brand
        initials Faker::Lorem.characters(2)
      end
    end
  end
end

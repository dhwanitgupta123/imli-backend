#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    FactoryGirl.define do
      factory :marketplace_brand_pack, class: MarketplaceProductModule::V1::MarketplaceBrandPack do
        association :brand_pack, factory: :brand_pack
        association :marketplace_brand_pack_pricing, factory: :marketplace_brand_pack_pricing
      end
    end
  end
end

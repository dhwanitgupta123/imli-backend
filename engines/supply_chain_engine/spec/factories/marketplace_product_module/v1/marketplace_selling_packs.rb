#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    FactoryGirl.define do
      factory :marketplace_selling_pack, class: MarketplaceProductModule::V1::MarketplaceSellingPack do
        max_quantity Faker::Number.number(1)
        association :marketplace_selling_pack_pricing, factory: :marketplace_selling_pack_pricing
        association :mpsp_sub_category, factory: :mpsp_category
        primary_tags Faker::Lorem.characters(20)
        secondary_tags Faker::Lorem.characters(20)
        display_name Faker::Lorem.characters(10)
        display_pack_size Faker::Lorem.characters(10)
      end
    end
  end
end

# Dummy Global variables to run in test cases
S3_IMAGE_STYLES ={
  xxxhdpi_large: '1376x1376>',
  xxxhdpi_medium: '460x460>',
  xxxhdpi_small: '192x192>',
  xxhdpi_large: '1032x1032>',
  xxhdpi_medium: '345x345>',
  xxhdpi_small: '144x144>',
  xhdpi_large: '688x688>',
  xhdpi_medium: '230x230>',
  xhdpi_small: '96x96>',
  hdpi_large: '516x516>',
  hdpi_medium: '173x173>',
  hdpi_small: '72x72>',
  mdpi_large: '344x344>',
  mdpi_medium: '115x115>',
  mdpi_small: '48x48>'
  }

S3_CONVERT_OPTIONS = {
  xhdpi_medium: '-quality 80'
}

APP_CONFIG = {}
APP_CONFIG['config'] = {}

#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe InventoryProductModule::V1::AddInventoryBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:inventory_brand_pack_mapper) { InventoryProductModule::V1::InventoryBrandPackMapper }
      let(:inventory_brand_pack_service) { InventoryProductModule::V1::InventoryBrandPackService }

      let(:inventory_brand_pack) { FactoryGirl.build(:inventory_brand_pack) }
      let(:add_inventory_brand_pack_api) { InventoryProductModule::V1::AddInventoryBrandPackApi.new(version) }

      let(:inventory_brand_pack_request) {
        {
          brand_pack_id: Faker::Number.number(1),
          inventory_id: Faker::Number.number(1),
          pricing: {
            net_landing_price: Faker::Commerce.price,
            margin: Faker::Commerce.price,
            octrai: Faker::Commerce.price,
            vat: Faker::Commerce.price,
            cst: Faker::Commerce.price
          }
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_inventory_brand_pack_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil brand_pack_id' do
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request.merge(brand_pack_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil pricing' do
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request.merge(pricing: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil net_landing_price' do
          inventory_brand_pack_request[:pricing].merge(net_landing_price: nil)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil margin' do
          inventory_brand_pack_request[:pricing].merge(margin: nil)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil service_tax' do
          inventory_brand_pack_request[:pricing].merge(service_tax: nil)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil vat' do
          inventory_brand_pack_request[:pricing].merge(vat: nil)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil octrai' do
          inventory_brand_pack_request[:pricing].merge(octrai: nil)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil octrai' do
          inventory_brand_pack_request[:pricing].merge(cst: nil)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong net_landing_price' do
          inventory_brand_pack_request[:pricing].merge(net_landing_price: -1)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong margin' do
          inventory_brand_pack_request[:pricing].merge(margin: -1)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong service_tax' do
          inventory_brand_pack_request[:pricing].merge(service_tax: -1)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong vat' do
          inventory_brand_pack_request[:pricing].merge(vat: -1)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong octrai' do
          inventory_brand_pack_request[:pricing].merge(octrai: -1)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong cst' do
          inventory_brand_pack_request[:pricing].merge(cst: -1)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(inventory_brand_pack_service).to receive(:transactional_create_inventory_brand_pack).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(inventory_brand_pack_service).to receive(:transactional_create_inventory_brand_pack).
            and_return(inventory_brand_pack)
          expect(inventory_brand_pack_mapper).to receive(:map_inventory_brand_pack_to_hash).and_return(inventory_brand_pack)
          data = add_inventory_brand_pack_api.enact(inventory_brand_pack_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

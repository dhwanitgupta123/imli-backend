#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe InventoryProductModule::V1::ChangeInventoryBrandPackStateApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:inventory_brand_pack_mapper) { InventoryProductModule::V1::InventoryBrandPackMapper }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:inventory_brand_pack_service) { InventoryProductModule::V1::InventoryBrandPackService }
      let(:inventory_brand_pack) { FactoryGirl.build(:inventory_brand_pack) }
      let(:change_inventory_brand_pack_state_api) { InventoryProductModule::V1::ChangeInventoryBrandPackStateApi.new(version) }

      let(:change_state_request) {
        {
          id: Faker::Number.number(1),
          event: Faker::Number.between(1, 3)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = change_inventory_brand_pack_state_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil event' do
          data = change_inventory_brand_pack_state_api.enact(change_state_request.merge(event: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(inventory_brand_pack_service).to receive(:change_inventory_brand_pack_state).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = change_inventory_brand_pack_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with precondition error args' do
          expect_any_instance_of(inventory_brand_pack_service).to receive(:change_inventory_brand_pack_state).
            and_raise(custom_errors_util::PreConditionRequiredError.new)
          data = change_inventory_brand_pack_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::PRE_CONDITION_REQUIRED)
        end

        it 'with valid args' do
          expect_any_instance_of(inventory_brand_pack_service).to receive(:change_inventory_brand_pack_state).
            and_return(inventory_brand_pack)
          expect(inventory_brand_pack_mapper).to receive(:map_inventory_brand_pack_to_hash).and_return(inventory_brand_pack)
          data = change_inventory_brand_pack_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

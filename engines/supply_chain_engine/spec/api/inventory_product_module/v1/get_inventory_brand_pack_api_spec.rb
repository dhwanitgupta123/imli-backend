#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe InventoryProductModule::V1::GetInventoryBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:inventory_brand_pack_mapper) { InventoryProductModule::V1::InventoryBrandPackMapper }
      let(:inventory_brand_pack_service) { InventoryProductModule::V1::InventoryBrandPackService }
      let(:inventory_brand_pack) { FactoryGirl.build(:inventory_brand_pack) }
      let(:get_inventory_brand_pack_api) { InventoryProductModule::V1::GetInventoryBrandPackApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(inventory_brand_pack_service).to receive(:get_inventory_brand_pack_by_id).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_inventory_brand_pack_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(inventory_brand_pack_service).to receive(:get_inventory_brand_pack_by_id).
            and_return(inventory_brand_pack)
          expect(inventory_brand_pack_mapper).to receive(:map_inventory_brand_pack_to_hash).and_return(inventory_brand_pack)
          data = get_inventory_brand_pack_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::GetCompanyApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:company_service) { SupplyChainModule::V1::CompanyService }
      let(:company) { FactoryGirl.build(:company) }
      let(:get_company_api) { SupplyChainModule::V1::GetCompanyApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          data = get_company_api.enact({id: nil})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(company_service).to receive(:get_company_by_id).
            and_return(company)
          data = get_company_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::AddCompanyApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:company_service) { SupplyChainModule::V1::CompanyService }
      let(:company) { FactoryGirl.build(:company) }
      let(:add_company_api) { SupplyChainModule::V1::AddCompanyApi.new(version) }

      let(:company_request) {
        {
          name: Faker::Company.name,
          logo_url: Faker::Company.logo,
          vat_number: Faker::Company.duns_number,
          tin_number: Faker::Company.duns_number,
          cst: Faker::Company.duns_number,
          account_details: Faker::Business.credit_card_number,
          trade_promotion_margin: Faker::Commerce.price,
          data_sharing_margin: Faker::Commerce.price,
          incentives: Faker::Lorem.characters(10),
          reconciliation_period: Faker::Time.between(DateTime.now - 1, DateTime.now),
          legal_document_url: Faker::Internet.url,
          initials: Faker::Lorem.characters(2)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_company_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil name' do
          data = add_company_api.enact(company_request.merge(name: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(company_service).to receive(:create_company).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_company_api.enact(company_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(company_service).to receive(:create_company).
            and_return(company)
          data = add_company_api.enact(company_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::UpdateInventoryApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:inventory_service) { SupplyChainModule::V1::InventoryService }
      let(:inventory) { FactoryGirl.build(:inventory) }
      let(:update_inventory_api) { SupplyChainModule::V1::UpdateInventoryApi.new(version) }

      let(:inventory_request) {
        {
          id: Faker::Number.number(1),
          name: Faker::Company.name,
          distributor_id: Faker::Number.number(1)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = update_inventory_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil id' do
          data = update_inventory_api.enact(inventory_request.merge(id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with service throwing exception' do
          expect_any_instance_of(inventory_service).to receive(:update_inventory).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = update_inventory_api.enact(inventory_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with service call success' do
          expect_any_instance_of(inventory_service).to receive(:update_inventory).
            and_return(inventory)
          data = update_inventory_api.enact(inventory_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::GetSellerApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:seller_service) { SupplyChainModule::V1::SellerService }
      let(:seller) { FactoryGirl.build(:seller) }
      let(:get_api) { SupplyChainModule::V1::GetSellerApi.new(version) }
      let(:request) { Faker::Number.number(1) }

      context 'enact ' do
        it 'with invalid request' do
          data = get_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          seller_service.any_instance.stub(:get_seller).and_raise(
            custom_errors_util::InvalidArgumentsError)
          data = get_api.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          seller_service.any_instance.stub(:get_seller).and_return(seller)
          data = get_api.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
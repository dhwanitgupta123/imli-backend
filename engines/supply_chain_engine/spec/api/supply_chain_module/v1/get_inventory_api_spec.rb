#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::GetInventoryApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:inventory_service) { SupplyChainModule::V1::InventoryService }
      let(:inventory) { FactoryGirl.build(:inventory) }
      let(:get_inventory_api) { SupplyChainModule::V1::GetInventoryApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(inventory_service).to receive(:get_inventory_by_id).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_inventory_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(inventory_service).to receive(:get_inventory_by_id).
            and_return(inventory)
          data = get_inventory_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

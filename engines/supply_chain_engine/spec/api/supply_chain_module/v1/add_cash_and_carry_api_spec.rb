#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::AddCashAndCarryApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:cash_and_carry_service) { SupplyChainModule::V1::CashAndCarryService }
      let(:c_and_c) { FactoryGirl.build(:cash_and_carry) }
      let(:add_cash_and_carry) { SupplyChainModule::V1::AddCashAndCarryApi.new(version) }

      let(:request) {
        {
          name: Faker::Company.name
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_cash_and_carry.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil name' do
          data = add_cash_and_carry.enact(request.merge(name: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(cash_and_carry_service).to receive(:create_cash_and_carry).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_cash_and_carry.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(cash_and_carry_service).to receive(:create_cash_and_carry).
            and_return(c_and_c)
          data = add_cash_and_carry.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::GetAllWarehouseApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:warehouse_Service) { SupplyChainModule::V1::WarehouseService }
      let(:warehouse) { { warehouse: [FactoryGirl.build(:warehouse)] } }
      let(:get_all) { SupplyChainModule::V1::GetAllWarehouseApi.new(version) }

      let(:request) {
        {
          sort_by: 'name'
        }
      }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(warehouse_Service).to receive(:get_all_warehouses).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_all.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(warehouse_Service).to receive(:get_all_warehouses).
            and_return(warehouse)
          data = get_all.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

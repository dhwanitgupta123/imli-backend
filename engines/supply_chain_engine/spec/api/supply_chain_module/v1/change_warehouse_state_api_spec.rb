#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::ChangeWarehouseStateApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:warehouse_service) { SupplyChainModule::V1::WarehouseService }
      let(:warehouse) { FactoryGirl.build(:warehouse) }
      let(:change_state) { SupplyChainModule::V1::ChangeWarehouseStateApi.new(version) }

      let(:request) {
        {
          event: Faker::Number.number(1),
          id: Faker::Number.number(1)
        }
      }

      context 'enact ' do
        it 'with wrong event' do
          data = change_state.enact(request.merge({ event: 8 }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'without id' do
          data = change_state.enact(request.merge({ id: nil }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(warehouse_service).to receive(:change_state).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = change_state.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(warehouse_service).to receive(:change_state).
            and_return(warehouse)
          data = change_state.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

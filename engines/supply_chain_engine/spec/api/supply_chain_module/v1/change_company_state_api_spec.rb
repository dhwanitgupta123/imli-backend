#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::ChangeCompanyStateApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:company_service) { SupplyChainModule::V1::CompanyService }
      let(:company) { FactoryGirl.build(:company) }
      let(:change_company_state_api) { SupplyChainModule::V1::ChangeCompanyStateApi.new(version) }

      let(:change_state_request) {
        {
          id: Faker::Number.number(1),
          event: Faker::Number.between(1, 3)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = change_company_state_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil event' do
          data = change_company_state_api.enact(change_state_request.merge(event: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(company_service).to receive(:change_company_state).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = change_company_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with precondition error args' do
          expect_any_instance_of(company_service).to receive(:change_company_state).
            and_raise(custom_errors_util::PreConditionRequiredError.new)
          data = change_company_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::PRE_CONDITION_REQUIRED)
        end

        it 'with valid args' do
          expect_any_instance_of(company_service).to receive(:change_company_state).
            and_return(company)
          data = change_company_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

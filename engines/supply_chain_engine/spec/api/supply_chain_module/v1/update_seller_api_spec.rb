#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::UpdateSellerApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:seller_service) { SupplyChainModule::V1::SellerService }
      let(:seller) { FactoryGirl.build(:seller) }
      let(:update) { SupplyChainModule::V1::UpdateSellerApi.new(version) }

      let(:request) {
        {
          name: Faker::Company.name,
          id: Faker::Number.number(1)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = update.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid request' do
          data = update.enact(request.merge(id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          seller_service.any_instance.stub(:update).and_raise(
            custom_errors_util::InvalidArgumentsError)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          seller_service.any_instance.stub(:update).and_return(seller)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
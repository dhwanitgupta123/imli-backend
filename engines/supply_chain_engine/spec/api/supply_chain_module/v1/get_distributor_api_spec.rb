#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::GetDistributorApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:distributor_service) { SupplyChainModule::V1::DistributorService }
      let(:distributor) { FactoryGirl.build(:distributor) }
      let(:get_distributor_api) { SupplyChainModule::V1::GetDistributorApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(distributor_service).to receive(:get_distributor_by_id).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_distributor_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(distributor_service).to receive(:get_distributor_by_id).
            and_return(distributor)
          data = get_distributor_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

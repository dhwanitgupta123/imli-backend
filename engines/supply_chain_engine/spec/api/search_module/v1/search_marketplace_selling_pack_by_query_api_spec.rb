#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SearchModule::V1::SearchMarketplaceSellingPackByQueryApi do
      let(:version) { 1 }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:search_mpsp_by_query) { SearchModule::V1::SearchMarketplaceSellingPackByQueryApi.new(version) }
      let(:marketplace_selling_pack_index_service) { SearchModule::V1::MarketplaceSellingPackIndexService }

      let(:attributes) {
        [
          {
            context: Faker::Lorem.characters(6),
            text: Faker::Lorem.characters(6)
          }
        ]
      }
      let(:marketplace_selling_pack) { SearchModule::V1::MarketplaceSellingPackSearchModel.new(attributes[0]) }
      let(:marketplace_selling_pack_reader) { SearchModule::V1::MarketplaceSellingPackModelReader }

      let(:request) {
        {
          query: Faker::Lorem.characters(6),
          top_n: '5'
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = search_mpsp_by_query.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil query' do
          data = search_mpsp_by_query.enact request.merge(query: nil)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with query less than 3 characters' do
          data = search_mpsp_by_query.enact request.merge(query: Faker::Lorem.characters(2))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid params' do
          expect_any_instance_of(marketplace_selling_pack_reader).to receive(:get_attribute_array).and_return(attributes)
          expect_any_instance_of(marketplace_selling_pack_index_service).to receive(:initialize_marketplace_selling_pack_repository).and_return(true)
          expect_any_instance_of(marketplace_selling_pack_index_service).to receive(:get_results).and_return(marketplace_selling_pack_reader.new(marketplace_selling_pack))
          data = search_mpsp_by_query.enact request
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

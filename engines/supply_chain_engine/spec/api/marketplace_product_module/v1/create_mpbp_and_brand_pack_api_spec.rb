#
# Module to handle all the functionalities related to master product
#
module MarketplaceProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceProductModule::V1::CreateMpbpAndBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:marketplace_brand_pack_service) { MarketplaceProductModule::V1::MarketplaceBrandPackService }
      let(:brand_pack) { FactoryGirl.build(:brand_pack) }
      let(:add_brand_pack_api) { MarketplaceProductModule::V1::CreateMpbpAndBrandPackApi.new(version) }
      let(:brand_pack_mapper) { MasterProductModule::V1::BrandPackMapper }
      
      let(:brand_pack_request) {
        {
          photo_url: Faker::Avatar.image,
          sku: Faker::Company.ein,
          mrp: Faker::Commerce.price,
          sku_size: Faker::Lorem.characters(10),
          unit: Faker::Lorem.characters(5),
          description: Faker::Lorem.sentence,
          shelf_life: Faker::Internet.slug(Faker::Number.between(1, 12).to_s, 'months'),
          product_id: 1,
          sub_category_id: 1,
        }
      }
      
      context 'enact ' do
        it 'with invalid request' do
          data = add_brand_pack_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil product_id' do
          data = add_brand_pack_api.enact(brand_pack_request.merge(product_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(marketplace_brand_pack_service).to receive(:transactional_create_mpbp_and_brand_pack).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_brand_pack_api.enact(brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(marketplace_brand_pack_service).to receive(:transactional_create_mpbp_and_brand_pack).
            and_return(brand_pack)
          expect(brand_pack_mapper).to receive(:map_brand_pack_to_hash).and_return(brand_pack)
          data = add_brand_pack_api.enact(brand_pack_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

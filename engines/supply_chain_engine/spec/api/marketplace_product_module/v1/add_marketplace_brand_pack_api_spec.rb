#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
  	require 'rails_helper'
    RSpec.describe MarketplaceProductModule::V1::AddMarketplaceBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:marketplace_brand_pack_service) { MarketplaceProductModule::V1::MarketplaceBrandPackService }
      let(:marketplace_brand_pack) { FactoryGirl.build_stubbed(:marketplace_brand_pack) }
      let(:add_marketplace_brand_pack_api) { MarketplaceProductModule::V1::AddMarketplaceBrandPackApi.new(version) }
      let(:mapper) { MarketplaceProductModule::V1::MarketplaceBrandPackMapper }
      let(:marketplace_brand_pack_request) {
        {
			    brand_pack_id: 1,
			    seller_brand_pack_id: 1,
          is_on_sale: true,
          is_ladder_pricing_active: true,
          pricing: {
            spat: Faker::Commerce.price,
            margin: Faker::Commerce.price,
            service_tax: Faker::Commerce.price,
            vat: Faker::Commerce.price,
            discount: Faker::Commerce.price,
            buying_price: Faker::Commerce.price,
            selling_price: Faker::Commerce.price,
            savings: Faker::Commerce.price,
            mrp: Faker::Commerce.price
          }
			  }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_marketplace_brand_pack_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil brand_pack_id' do
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request.merge(brand_pack_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil seller_brand_pack_id' do
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request.merge(seller_brand_pack_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil pricing' do
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request.merge(pricing: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil spat' do
          marketplace_brand_pack_request[:pricing].merge(spat: nil)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil margin' do
          marketplace_brand_pack_request[:pricing].merge(margin: nil)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil selling_price' do
          marketplace_brand_pack_request[:pricing].merge(selling_price: nil)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil vat' do
          marketplace_brand_pack_request[:pricing].merge(vat: nil)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil buying_price' do
          marketplace_brand_pack_request[:pricing].merge(buying_price: nil)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil savings' do
          marketplace_brand_pack_request[:pricing].merge(savings: nil)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil mrp' do
          marketplace_brand_pack_request[:pricing].merge(mrp: nil)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong spat' do
          marketplace_brand_pack_request[:pricing].merge(spat: -1)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong margin' do
          marketplace_brand_pack_request[:pricing].merge(margin: -1)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong selling_price' do
          marketplace_brand_pack_request[:pricing].merge(selling_price: -1)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong vat' do
          marketplace_brand_pack_request[:pricing].merge(vat: -1)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong buying_price' do
          marketplace_brand_pack_request[:pricing].merge(buying_price: -1)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong savings' do
          marketplace_brand_pack_request[:pricing].merge(savings: -1)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong mrp' do
          marketplace_brand_pack_request[:pricing].merge(mrp: -1)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(marketplace_brand_pack_service).to receive(:create_marketplace_brand_pack).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(marketplace_brand_pack_service).to receive(:create_marketplace_brand_pack).
            and_return(marketplace_brand_pack)
          mapper.stub(:map_marketplace_brand_pack_to_hash).and_return(marketplace_brand_pack_request)
          data = add_marketplace_brand_pack_api.enact(marketplace_brand_pack_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

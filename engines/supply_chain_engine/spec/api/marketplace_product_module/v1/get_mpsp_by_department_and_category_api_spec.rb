#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceProductModule::V1::GetMpspByDepartmentAndCategoryApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:marketplace_selling_pack_service) { MarketplaceProductModule::V1::MarketplaceSellingPackService }
      let(:marketplace_selling_pack) { FactoryGirl.build(:marketplace_selling_pack) }
      let(:get_mpsp_by_department_and_category_api) { MarketplaceProductModule::V1::GetMpspByDepartmentAndCategoryApi.new(version) }
      let(:mapper) { MarketplaceProductModule::V1::MarketplaceSellingPackMapper }

      context 'enact ' do
        it 'with valid args' do
          mpsp_array = []
          mpsp_array.push(marketplace_selling_pack)
          expect_any_instance_of(marketplace_selling_pack_service).to receive(:get_mpsp_by_mpsp_department_and_mpsp_category).
            and_return(marketplace_selling_pack)
          mapper.stub(:map_marketplace_selling_packs_to_array).and_return(mpsp_array)
          data = get_mpsp_by_department_and_category_api.enact({})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceProductModule::V1::GetMarketplaceBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:marketplace_brand_pack_service) { MarketplaceProductModule::V1::MarketplaceBrandPackService }
      let(:marketplace_brand_pack) { FactoryGirl.build_stubbed(:marketplace_brand_pack) }
      let(:get_api) { MarketplaceProductModule::V1::GetMarketplaceBrandPackApi.new(version) }
      let(:mapper) { MarketplaceProductModule::V1::MarketplaceBrandPackMapper }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(marketplace_brand_pack_service).to receive(:get_marketplace_brand_pack_by_id).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(marketplace_brand_pack_service).to receive(:get_marketplace_brand_pack_by_id).
            and_return(marketplace_brand_pack)
          mapper.stub(:map_marketplace_brand_pack_to_hash).and_return(marketplace_brand_pack)
          data = get_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

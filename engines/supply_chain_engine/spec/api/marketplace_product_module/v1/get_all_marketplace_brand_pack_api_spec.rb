#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceProductModule::V1::GetAllMarketplaceBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:marketplace_brand_pack_service) { MarketplaceProductModule::V1::MarketplaceBrandPackService }
      let(:marketplace_brand_pack) { FactoryGirl.build(:marketplace_brand_pack) }
      let(:get_all_api) { MarketplaceProductModule::V1::GetAllMarketplaceBrandPackApi.new(version) }
      let(:mapper) { MarketplaceProductModule::V1::MarketplaceBrandPackMapper }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(marketplace_brand_pack_service).to receive(:get_all_marketplace_brand_pack).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_all_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(marketplace_brand_pack_service).to receive(:get_all_marketplace_brand_pack).
            and_return(marketplace_brand_pack)
          mapper.stub(:map_marketplace_brand_packs_to_array).and_return(marketplace_brand_pack)
          data = get_all_api.enact({})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

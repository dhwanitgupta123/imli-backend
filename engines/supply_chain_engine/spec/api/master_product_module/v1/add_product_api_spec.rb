#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::AddProductApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:product_service) { MasterProductModule::V1::ProductService }
      let(:product) { FactoryGirl.build(:product) }
      let(:add_product_api) { MasterProductModule::V1::AddProductApi.new(version) }

      let(:product_request) {
        {
          name: Faker::Company.name,
          logo_url: Faker::Company.logo,
          sub_brand_id: 1,
          initials: Faker::Lorem.characters(2)
        }
      }
      
      context 'enact ' do
        it 'with invalid request' do
          data = add_product_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil sub_brand_id' do
          data = add_product_api.enact(product_request.merge(sub_brand_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(product_service).to receive(:create_product).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_product_api.enact(product_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(product_service).to receive(:create_product).
            and_return(product)
          data = add_product_api.enact(product_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

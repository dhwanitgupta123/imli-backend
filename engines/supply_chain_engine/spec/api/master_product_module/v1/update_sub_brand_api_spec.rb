#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::UpdateSubBrandApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:sub_brand_service) { MasterProductModule::V1::SubBrandService }
      let(:sub_brand) { FactoryGirl.build(:sub_brand) }
      let(:update_sub_brand_api) { MasterProductModule::V1::UpdateSubBrandApi.new(version) }

      let(:sub_brand_request) {
        {
          name: Faker::Company.name,
          logo_url: Faker::Company.logo,
          brand_id: 1,
          initials: Faker::Lorem.characters(2)
        }
      }
      
      context 'enact ' do
        it 'with invalid request' do
          data = update_sub_brand_api.enact({}, 1)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil brand_id' do
          data = update_sub_brand_api.enact(sub_brand_request.merge(brand_id: nil), 1)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          sub_brand_service.any_instance.stub(:update_sub_brand).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = update_sub_brand_api.enact(sub_brand_request, 1)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          sub_brand_service.any_instance.stub(:update_sub_brand).
            and_return(sub_brand)
          data = update_sub_brand_api.enact(sub_brand_request, id: sub_brand.id)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::AddVariantApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:variant_service) { MasterProductModule::V1::VariantService }
      let(:variant) { FactoryGirl.build(:variant) }
      let(:add_variant_api) { MasterProductModule::V1::AddVariantApi.new(version) }

      let(:variant_request) {
        {
          name: Faker::Company.name,
          initials: Faker::Lorem.characters(2)
        }
      }
      
      context 'enact ' do
        it 'with valid request' do
          data = add_variant_api.enact(variant_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end

        it 'with nil name' do
          data = add_variant_api.enact(variant_request.merge(name: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil initials' do
          data = add_variant_api.enact(variant_request.merge(initials: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end
    end
  end
end

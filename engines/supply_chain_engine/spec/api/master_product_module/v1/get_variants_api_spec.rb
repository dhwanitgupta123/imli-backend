#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::GetVariantsApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:variant_service) { MasterProductModule::V1::VariantService }
      let(:variant) { FactoryGirl.build(:variant) }
      let(:get_variants_api) { MasterProductModule::V1::GetVariantsApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(variant_service).to receive(:get_all_variants).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_variants_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          variant_array = []
          variant_array.push(variant)
          expect_any_instance_of(variant_service).to receive(:get_all_variants).
            and_return(variant_array)
          data = get_variants_api.enact({})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

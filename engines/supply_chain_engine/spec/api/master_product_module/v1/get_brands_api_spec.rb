#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::GetBrandsApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:brand_service) { MasterProductModule::V1::BrandService }
      let(:brand) { FactoryGirl.build(:brand) }
      let(:get_brands_api) { MasterProductModule::V1::GetBrandsApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(brand_service).to receive(:get_all_brands).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_brands_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          brand_array = []
          brand_array.push(brand)
          stub_response = { elements: brand_array, page_count: Faker::Number.number(1)}
          expect_any_instance_of(brand_service).to receive(:get_all_brands).
            and_return(stub_response)
          data = get_brands_api.enact({})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

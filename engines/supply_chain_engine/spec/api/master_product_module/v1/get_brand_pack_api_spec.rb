#
# Module to handle all the functionalities related to master brand_pack
#
module MasterProductModule
  #
  # Version1 for master brand_pack module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::GetBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:brand_pack_service) { MasterProductModule::V1::BrandPackService }
      let(:brand_pack) { FactoryGirl.build(:brand_pack) }
      let(:get_brand_pack_api) { MasterProductModule::V1::GetBrandPackApi.new(version) }
      let(:brand_pack_mapper) { MasterProductModule::V1::BrandPackMapper }

      context 'enact ' do
        it 'with invalid args' do
          data = get_brand_pack_api.enact({id: nil})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(brand_pack_service).to receive(:get_brand_pack_by_id).
            and_return(brand_pack)
          expect(brand_pack_mapper).to receive(:map_brand_pack_and_mpbp_to_hash).and_return(brand_pack)
          data = get_brand_pack_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

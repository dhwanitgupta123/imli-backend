#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::UpdateProductApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:product_service) { MasterProductModule::V1::ProductService }
      let(:product) { FactoryGirl.build(:product) }
      let(:update_product_api) { MasterProductModule::V1::UpdateProductApi.new(version) }

      let(:product_request) {
        {
          name: Faker::Company.name,
          logo_url: Faker::Company.logo,
          sub_brand_id: 1,
          initials: Faker::Lorem.characters(2)
        }
      }
      
      context 'enact ' do
        it 'with invalid request' do
          data = update_product_api.enact({}, 1)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil sub_brand_id' do
          data = update_product_api.enact(product_request.merge(sub_brand_id: nil), 1)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(product_service).to receive(:update_product).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = update_product_api.enact(product_request, 1)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(product_service).to receive(:update_product).
            and_return(product)
          data = update_product_api.enact(product_request, 1)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

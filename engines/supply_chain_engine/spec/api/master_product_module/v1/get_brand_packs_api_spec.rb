#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::GetBrandPacksApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:brand_pack_service) { MasterProductModule::V1::BrandPackService }
      let(:brand_pack) { FactoryGirl.build(:brand_pack) }
      let(:get_brand_packs_api) { MasterProductModule::V1::GetBrandPacksApi.new(version) }
      let(:brand_pack_mapper) { MasterProductModule::V1::BrandPackMapper }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(brand_pack_service).to receive(:get_all_brand_packs).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_brand_packs_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          brand_pack_array = []
          brand_pack_array.push(brand_pack)
          stub_response = { elements: brand_pack_array, page_count: Faker::Number.number(1)}
          expect_any_instance_of(brand_pack_service).to receive(:get_all_brand_packs).
            and_return(stub_response)
          expect(brand_pack_mapper).to receive(:map_brand_packs_to_array).and_return([brand_pack])
          data = get_brand_packs_api.enact({})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

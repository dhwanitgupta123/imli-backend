#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::GetProductApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:product_service) { MasterProductModule::V1::ProductService }
      let(:product) { FactoryGirl.build(:product) }
      let(:get_product_api) { MasterProductModule::V1::GetProductApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          data = get_product_api.enact({id: nil})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(product_service).to receive(:get_product_by_id).
            and_return(product)
          data = get_product_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

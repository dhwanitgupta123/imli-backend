#
# Module to handle all the functionalities related to warehouse product product
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
  	require 'rails_helper'
    RSpec.describe WarehouseProductModule::V1::ChangeWarehouseBrandPackStateApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:wbp_service) { WarehouseProductModule::V1::WarehouseBrandPackService }
      let(:ibp) { FactoryGirl.build_stubbed(:inventory_brand_pack) }
      let(:wbp) { FactoryGirl.build(:warehouse_brand_pack) }
      let(:change_state) { WarehouseProductModule::V1::ChangeWarehouseBrandPackStateApi.new(version) }
      let(:wbp_mapper) { WarehouseProductModule::V1::WarehouseBrandPackMapper }
      
      let(:request) {
        {
          event: Faker::Number.number(1),
          id: Faker::Number.number(1)
        }
      }

      context 'enact ' do
        it 'with wrong event' do
          data = change_state.enact(request.merge({ event: 8 }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'without id' do
          data = change_state.enact(request.merge({ id: nil }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(wbp_service).to receive(:change_state).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = change_state.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          wbp.inventory_brand_packs << ibp
          expect_any_instance_of(wbp_service).to receive(:change_state).
            and_return(wbp)
          expect(wbp_mapper).to receive(:map_wbp_to_hash).and_return(wbp)
          data = change_state.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end


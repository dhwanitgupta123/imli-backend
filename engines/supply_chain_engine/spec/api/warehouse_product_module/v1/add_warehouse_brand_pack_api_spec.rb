#
# Module to handle all the functionalities related to warehouse product product
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
  	require 'rails_helper'
    RSpec.describe WarehouseProductModule::V1::AddWarehouseBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:wbp_service) { WarehouseProductModule::V1::WarehouseBrandPackService }
      let(:add) { WarehouseProductModule::V1::AddWarehouseBrandPackApi.new(version) }
      let(:wbp_mapper) { WarehouseProductModule::V1::WarehouseBrandPackMapper }
      let(:request) {
        {
          warehouse_id: Faker::Number.number(1),
          inventory_brand_pack_id: Faker::Number.number(1),
          brand_pack_id: Faker::Number.number(1),
          threshold_stock_value: Faker::Number.number(1),
          outbound_frequency_period_in_days: Faker::Number.number(1)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil warehouse_id' do
          data = add.enact(request.merge(warehouse_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil inventory_brand_pack_id' do
          data = add.enact(request.merge(inventory_brand_pack_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil brand_pack_id' do
          data = add.enact(request.merge(brand_pack_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil threshold_stock_value' do
          data = add.enact(request.merge(threshold_stock_value: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil outbound_frequency_period_in_days' do
          data = add.enact(request.merge(outbound_frequency_period_in_days: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with negative outbound_frequency_period_in_days' do
          data = add.enact(request.merge(outbound_frequency_period_in_days: -1))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with negative threshold_stock_value' do
          data = add.enact(request.merge(threshold_stock_value: -1))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with non numeric outbound_frequency_period_in_days' do
          data = add.enact(request.merge(outbound_frequency_period_in_days: 'a'))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with non numeric threshold_stock_value' do
          data = add.enact(request.merge(threshold_stock_value: 'a'))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        # Not taking pricing params in request, populating from IBP & Brand pack internally
        # 
        # it 'with nil pricing' do
        #   data = add.enact(request.merge(pricing: nil))
        #   expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        # end

        # it 'with nil spat' do
        #   request[:pricing].merge(spat: nil)
        #   data = add.enact(request)
        #   expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        # end

        # it 'with nil margin' do
        #   request[:pricing].merge(margin: nil)
        #   data = add.enact(request)
        #   expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        # end

        # it 'with nil service_tax' do
        #   request[:pricing].merge(service_tax: nil)
        #   data = add.enact(request)
        #   expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        # end

        # it 'with nil vat' do
        #   request[:pricing].merge(vat: nil)
        #   data = add.enact(request)
        #   expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        # end

        # it 'with nil spat_per_unit' do
        #   request[:pricing].merge(spat_per_unit: nil)
        #   data = add.enact(request)
        #   expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        # end

        # it 'with wrong spat' do
        #   request[:pricing].merge(spat: -1)
        #   data = add.enact(request)
        #   expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        # end

        # it 'with wrong margin' do
        #   request[:pricing].merge(margin: -1)
        #   data = add.enact(request)
        #   expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        # end

        # it 'with wrong service_tax' do
        #   request[:pricing].merge(service_tax: -1)
        #   data = add.enact(request)
        #   expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        # end

        # it 'with wrong vat' do
        #   request[:pricing].merge(vat: -1)
        #   data = add.enact(request)
        #   expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        # end

        # it 'with wrong spat_per_unit' do
        #   request[:pricing].merge(spat_per_unit: -1)
        #   data = add.enact(request)
        #   expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        # end

        it 'with invalid args' do
          expect_any_instance_of(wbp_service).to receive(:create).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          ibp = FactoryGirl.build_stubbed(:inventory_brand_pack)
          wbp = FactoryGirl.build_stubbed(:warehouse_brand_pack)
          wbp.inventory_brand_packs << ibp
          expect_any_instance_of(wbp_service).to receive(:create).
            and_return(wbp)
          expect(wbp_mapper).to receive(:map_wbp_to_hash).and_return(wbp)
          data = add.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

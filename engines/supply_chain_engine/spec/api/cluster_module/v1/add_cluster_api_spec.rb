#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
        require 'rails_helper'
    RSpec.describe ClusterModule::V1::AddClusterApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:cluster_service) { ClusterModule::V1::ClusterService }
      let(:cluster) { FactoryGirl.build(:cluster) }
      let(:add_cluster_api) { ClusterModule::V1::AddClusterApi.new(version) }

      let(:cluster_request) {
        {
          label: Faker::Company.name,
          cluster_type: Faker::Number.number(1),
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_cluster_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil label' do
          data = add_cluster_api.enact(cluster_request.merge(label: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(cluster_service).to receive(:create_cluster).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_cluster_api.enact(cluster_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(cluster_service).to receive(:create_cluster).
            and_return(cluster)
          data = add_cluster_api.enact(cluster_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

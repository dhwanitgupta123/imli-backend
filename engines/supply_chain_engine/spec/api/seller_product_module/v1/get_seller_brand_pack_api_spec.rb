#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SellerProductModule::V1::GetSellerBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:seller_brand_pack_service) { SellerProductModule::V1::SellerBrandPackService }
      let(:product) { FactoryGirl.build(:product) }
      let(:brand_pack) { FactoryGirl.build(:brand_pack, product: product) }
      let(:seller_brand_pack) { FactoryGirl.build(:seller_brand_pack, brand_pack: brand_pack) }
      let(:get_api) { SellerProductModule::V1::GetSellerBrandPackApi.new(version) }
      let(:request) { Faker::Number.number(1) }
      let(:seller_brand_pack_mapper) { SellerProductModule::V1::SellerBrandPackMapper }

      context 'enact ' do
        it 'with invalid request' do
          data = get_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          seller_brand_pack_service.any_instance.stub(:get_seller_brand_pack).and_raise(
            custom_errors_util::InvalidArgumentsError)
          data = get_api.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          ibp = FactoryGirl.build_stubbed(:inventory_brand_pack)
          wbp = FactoryGirl.build_stubbed(:warehouse_brand_pack)
          sbp = FactoryGirl.build(:seller_brand_pack)
          wbp.inventory_brand_packs << ibp
          sbp.warehouse_brand_packs << wbp
          expect_any_instance_of(seller_brand_pack_service).to receive(:get_seller_brand_pack).
            and_return(sbp)
          expect(seller_brand_pack_mapper).to receive(:map_seller_brand_pack_to_hash).and_return(sbp)
          data = get_api.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
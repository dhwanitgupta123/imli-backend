#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SellerProductModule::V1::AddSellerBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:seller_brand_pack_service) { SellerProductModule::V1::SellerBrandPackService }
      let(:add_seller_brand_pack) { SellerProductModule::V1::AddSellerBrandPackApi.new(version) }
      let(:seller_brand_pack_mapper) { SellerProductModule::V1::SellerBrandPackMapper }
      let(:request) {
        {
          seller_id: Faker::Number.number(1),
          warehouse_brand_pack_id: Faker::Number.number(1),
          brand_pack_id: Faker::Number.number(1),
          pricing: {
            spat: Faker::Commerce.price,
            margin: Faker::Commerce.price,
            service_tax: Faker::Commerce.price,
            vat: Faker::Commerce.price,
            spat_per_unit: Faker::Commerce.price
          }
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_seller_brand_pack.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil seller id' do
          data = add_seller_brand_pack.enact(request.merge(seller_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil warehouse_brand_pack_id' do
          data = add_seller_brand_pack.enact(request.merge(warehouse_brand_pack_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil brand_pack_id' do
          data = add_seller_brand_pack.enact(request.merge(brand_pack_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil pricing' do
          data = add_seller_brand_pack.enact(request.merge(pricing: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil spat' do
          request[:pricing].merge(spat: nil)
          data = add_seller_brand_pack.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil margin' do
          request[:pricing].merge(margin: nil)
          data = add_seller_brand_pack.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil service_tax' do
          request[:pricing].merge(service_tax: nil)
          data = add_seller_brand_pack.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil vat' do
          request[:pricing].merge(vat: nil)
          data = add_seller_brand_pack.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil spat_per_unit' do
          request[:pricing].merge(spat_per_unit: nil)
          data = add_seller_brand_pack.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong spat' do
          request[:pricing].merge(spat: -1)
          data = add_seller_brand_pack.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong margin' do
          request[:pricing].merge(margin: -1)
          data = add_seller_brand_pack.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong service_tax' do
          request[:pricing].merge(service_tax: -1)
          data = add_seller_brand_pack.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong vat' do
          request[:pricing].merge(vat: -1)
          data = add_seller_brand_pack.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong spat_per_unit' do
          request[:pricing].merge(spat_per_unit: -1)
          data = add_seller_brand_pack.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(seller_brand_pack_service).to receive(:create_seller_brand_pack) .
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_seller_brand_pack.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          ibp = FactoryGirl.build_stubbed(:inventory_brand_pack)
          wbp = FactoryGirl.build_stubbed(:warehouse_brand_pack)
          sbp = FactoryGirl.build(:seller_brand_pack)
          wbp.inventory_brand_packs << ibp
          sbp.warehouse_brand_packs << wbp
          expect_any_instance_of(seller_brand_pack_service).to receive(:create_seller_brand_pack).
            and_return(sbp)
          expect(seller_brand_pack_mapper).to receive(:map_seller_brand_pack_to_hash).and_return(sbp)
          data = add_seller_brand_pack.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
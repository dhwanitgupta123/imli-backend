#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SellerProductModule::V1::ChangeSellerBrandPackStateApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:seller_brand_pack_service) { SellerProductModule::V1::SellerBrandPackService }
      let(:product) { FactoryGirl.build(:product) }
      let(:brand_pack) { FactoryGirl.build(:brand_pack, product: product) }
      let(:seller_brand_pack) { FactoryGirl.build(:seller_brand_pack, brand_pack: brand_pack) }
      let(:change_state) { SellerProductModule::V1::ChangeSellerBrandPackStateApi.new(version) }
      let(:seller_brand_pack_mapper) { SellerProductModule::V1::SellerBrandPackMapper }
      
      let(:request) {
        {
          event: 1,
          id: 1
        }
      }

      context 'enact ' do
        it 'with wrong event' do
          data = change_state.enact(request.merge({ event: 8 }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'without id' do
          data = change_state.enact(request.merge({ id: nil }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(seller_brand_pack_service).to receive(:change_state).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = change_state.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          ibp = FactoryGirl.build_stubbed(:inventory_brand_pack)
          wbp = FactoryGirl.build_stubbed(:warehouse_brand_pack)
          sbp = FactoryGirl.build(:seller_brand_pack)
          wbp.inventory_brand_packs << ibp
          sbp.warehouse_brand_packs << wbp
          expect_any_instance_of(seller_brand_pack_service).to receive(:change_state).
            and_return(sbp)
          expect(seller_brand_pack_mapper).to receive(:map_seller_brand_pack_to_hash).and_return(sbp)
          data = change_state.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
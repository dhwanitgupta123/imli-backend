#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SellerProductModule::V1::GetSellerBrandPacksApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:seller_brand_pack_service) { SellerProductModule::V1::SellerBrandPackService }
      let(:product) { FactoryGirl.build(:product) }
      let(:brand_pack) { FactoryGirl.build(:brand_pack, product: product) }
      let(:seller_brand_pack) { { seller_brand_pack: [FactoryGirl.build(:seller_brand_pack, brand_pack: brand_pack)] } }
      let(:get_all) { SellerProductModule::V1::GetSellerBrandPacksApi.new(version) }
      let(:seller_brand_pack_mapper) { SellerProductModule::V1::SellerBrandPackMapper }

      let(:request) {
        {
          sort_by: 'name'
        }
      }

      context 'enact ' do
        it 'with wrong order' do
          data = get_all.enact(request.merge({ order: 'xyz' }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong sort_by' do
          data = get_all.enact(request.merge({ sort_by: 'xyz' }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(seller_brand_pack_service).to receive(:get_all_seller_brand_packs).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_all.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          wbp = FactoryGirl.build_stubbed(:warehouse_brand_pack)
          seller_brand_pack[:seller_brand_pack].first.warehouse_brand_packs << wbp
          expect_any_instance_of(seller_brand_pack_service).to receive(:get_all_seller_brand_packs).
            and_return(seller_brand_pack)
          expect(seller_brand_pack_mapper).to receive(:map_seller_brand_packs_to_array).and_return(seller_brand_pack)
          data = get_all.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
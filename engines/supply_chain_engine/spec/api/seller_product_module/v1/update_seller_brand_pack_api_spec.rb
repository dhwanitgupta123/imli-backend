#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SellerProductModule::V1::UpdateSellerBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:seller_brand_pack_service) { SellerProductModule::V1::SellerBrandPackService }
      let(:product) { FactoryGirl.build(:product) }
      let(:brand_pack) { FactoryGirl.build(:brand_pack, product: product) }
      let(:seller_brand_pack) { FactoryGirl.build(:seller_brand_pack, brand_pack: brand_pack) }
      let(:update) { SellerProductModule::V1::UpdateSellerBrandPackApi.new(version) }
      let(:seller_brand_pack_mapper) { SellerProductModule::V1::SellerBrandPackMapper }
      
      let(:request) {
        {
          id: Faker::Number.number(1),
          seller_id: Faker::Number.number(1),
          pricing: {
            spat: Faker::Commerce.price,
            margin: Faker::Commerce.price,
            service_tax: Faker::Commerce.price,
            vat: Faker::Commerce.price,
            spat_per_unit: Faker::Commerce.price
          }
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = update.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid request' do
          data = update.enact(request.merge(id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong spat' do
          request[:pricing].merge(spat: -1)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong margin' do
          request[:pricing].merge(margin: -1)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong service_tax' do
          request[:pricing].merge(service_tax: -1)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong vat' do
          request[:pricing].merge(vat: -1)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong spat_per_unit' do
          request[:pricing].merge(spat_per_unit: -1)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          seller_brand_pack_service.any_instance.stub(:update_seller_brand_pack).and_raise(
            custom_errors_util::InvalidArgumentsError)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          wbp = FactoryGirl.build_stubbed(:warehouse_brand_pack)
          seller_brand_pack.warehouse_brand_packs << wbp
          expect(seller_brand_pack_mapper).to receive(:map_seller_brand_pack_to_hash).and_return(seller_brand_pack)
          seller_brand_pack_service.any_instance.stub(:update_seller_brand_pack).and_return(seller_brand_pack)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceProductModule::V1::MarketplaceBrandPackService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:marketplace_brand_pack_service) { MarketplaceProductModule::V1::MarketplaceBrandPackService.new }
      let(:marketplace_brand_pack_dao) { MarketplaceProductModule::V1::MarketplaceBrandPackDao }
      let(:seller_brand_pack_dao) { SellerProductModule::V1::SellerBrandPackDao }
      let(:brand_pack_dao) { MasterProductModule::V1::BrandPackDao }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:brand_pack) { FactoryGirl.build_stubbed(:brand_pack) }
      let(:seller_brand_pack) { FactoryGirl.build_stubbed(:seller_brand_pack) }
      let(:inventory_brand_pack) { FactoryGirl.build_stubbed(:inventory_brand_pack) }
      let(:warehouse_brand_pack) { FactoryGirl.build_stubbed(:warehouse_brand_pack) }
      let(:seller) { FactoryGirl.build_stubbed(:seller) }
      let(:inventory) { FactoryGirl.build_stubbed(:inventory) }
      let(:warehouse) { FactoryGirl.build_stubbed(:warehouse) }
      let(:marketplace_brand_pack) { FactoryGirl.build_stubbed(:marketplace_brand_pack) }
      let(:brand_pack_service) { MasterProductModule::V1::BrandPackService }
      let(:seller_service) { SupplyChainModule::V1::SellerService }
      let(:inventory_service) { SupplyChainModule::V1::InventoryService }
      let(:warehouse_service) { SupplyChainModule::V1::WarehouseService }
      let(:inventory_brand_pack_service) { InventoryProductModule::V1::InventoryBrandPackService }
      let(:seller_brand_pack_service) { SellerProductModule::V1::SellerBrandPackService }
      let(:warehouse_brand_pack_service) { WarehouseProductModule::V1::WarehouseBrandPackService }

      let(:marketplace_brand_pack_request) {
        {
          brand_pack_id: 1,
          seller_brand_pack_id: 1,
          selling_price: 100,
          mrp: 120,
          discount: 15
        }
      }

      context 'create marketplace_brand_pack' do
        it 'with wrong seller_brand_pack_id' do
          expect{ marketplace_brand_pack_service.create_marketplace_brand_pack(
            marketplace_brand_pack_request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong brand_pack_id' do
          expect{ marketplace_brand_pack_service.create_marketplace_brand_pack(
            marketplace_brand_pack_request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'wrong tuple of brand_pack_id & mark_id' do
          brand_pack_dao.any_instance.stub(:get_by_id).and_return(brand_pack)
          seller_brand_pack_dao.any_instance.stub(:get_seller_brand_pack_by_id).and_return(seller_brand_pack)
          expect{ marketplace_brand_pack_service.create_marketplace_brand_pack(
            marketplace_brand_pack_request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with new params ' do
          seller_brand_pack.brand_pack = brand_pack
          marketplace_brand_pack_dao.any_instance.stub(:create).and_return(marketplace_brand_pack)
          brand_pack_dao.any_instance.stub(:get_by_id).and_return(brand_pack)
          seller_brand_pack_dao.any_instance.stub(:get_seller_brand_pack_by_id).and_return(seller_brand_pack)
          expect(marketplace_brand_pack_service.create_marketplace_brand_pack(
            marketplace_brand_pack_request)).to_not be_nil
        end
      end

      context 'update marketplace_selling_pack' do
        it 'with wrong seller_brand_pack_id' do
          expect{ marketplace_brand_pack_service.update_marketplace_brand_pack(
            marketplace_brand_pack_request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong brand_pack_id' do
          expect{ marketplace_brand_pack_service.update_marketplace_brand_pack(
            marketplace_brand_pack_request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'wrong tuple of brand_pack_id & mark_id' do
          brand_pack_dao.any_instance.stub(:get_by_id).and_return(brand_pack)
          seller_brand_pack_dao.any_instance.stub(:get_seller_brand_pack_by_id).and_return(seller_brand_pack)
          expect{ marketplace_brand_pack_service.update_marketplace_brand_pack(
            marketplace_brand_pack_request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong update id ' do
          seller_brand_pack.brand_pack = brand_pack
          marketplace_brand_pack_dao.any_instance.stub(:update).and_return(marketplace_brand_pack)
          brand_pack_dao.any_instance.stub(:get_by_id).and_return(brand_pack)
          seller_brand_pack_dao.any_instance.stub(:get_seller_brand_pack_by_id).and_return(seller_brand_pack)
          expect{ marketplace_brand_pack_service.update_marketplace_brand_pack(
            marketplace_brand_pack_request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with new params ' do
          seller_brand_pack.brand_pack = brand_pack
          marketplace_brand_pack_dao.any_instance.stub(:update).and_return(marketplace_brand_pack)
          brand_pack_dao.any_instance.stub(:get_by_id).and_return(brand_pack)
          marketplace_brand_pack_dao.any_instance.stub(:get_by_id).and_return(marketplace_brand_pack)
          seller_brand_pack_dao.any_instance.stub(:get_seller_brand_pack_by_id).and_return(seller_brand_pack)
          expect(marketplace_brand_pack_service.update_marketplace_brand_pack(
            marketplace_brand_pack_request)).to_not be_nil
        end
      end

      context 'get all mpsp' do
        request = { sort_by: 'id' }
        it 'with wrong sort_by' do
          marketplace_brand_pack_dao.any_instance.stub(:get_all).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ marketplace_brand_pack_service.get_all_marketplace_brand_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with wrong order' do
          marketplace_brand_pack_dao.any_instance.stub(:get_all).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ marketplace_brand_pack_service.get_all_marketplace_brand_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          marketplace_brand_pack_dao.any_instance.stub(:get_all).and_return(marketplace_brand_pack)
          expect(marketplace_brand_pack_service.get_all_marketplace_brand_pack(request)).to_not be_nil
        end
      end

      context 'change mpbp state' do
        mpbp = FactoryGirl.create(:marketplace_brand_pack)

        let(:change_state_args) {
        {
          id: mpbp.id,
          event: common_events::ACTIVATE
        }
      }
        it 'with wrong transition flow' do
          expect_any_instance_of(marketplace_brand_pack_dao).to receive(:get_by_id).twice.
            and_return(marketplace_brand_pack)
          expect_any_instance_of(brand_pack_service).to receive(:brand_pack_active?).and_return(
            brand_pack)
          marketplace_brand_pack_dao.any_instance.stub(:change_state).and_raise(
            custom_error_util::PreConditionRequiredError)
          expect{ marketplace_brand_pack_service.change_marketplace_brand_pack_state(
            change_state_args) }.to raise_error(
            custom_error_util::PreConditionRequiredError)
        end

        it 'with wrong event' do
          expect_any_instance_of(marketplace_brand_pack_dao).to receive(:get_by_id).twice.
            and_return(marketplace_brand_pack)
          expect_any_instance_of(brand_pack_service).to receive(:brand_pack_active?).and_return(
            brand_pack)
          marketplace_brand_pack_dao.any_instance.stub(:change_state).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ marketplace_brand_pack_service.change_marketplace_brand_pack_state(
            change_state_args) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong request' do
          expect_any_instance_of(marketplace_brand_pack_dao).to receive(:get_by_id).twice.
            and_return(marketplace_brand_pack)
          expect_any_instance_of(brand_pack_service).to receive(:brand_pack_active?).and_return(
            brand_pack)
          marketplace_brand_pack_dao.any_instance.stub(:change_state).and_raise(
            custom_error_util::InsufficientDataError)
          expect{ marketplace_brand_pack_service.change_marketplace_brand_pack_state(
            change_state_args) }.to raise_error(
            custom_error_util::InsufficientDataError)
        end

        it 'with correct request' do
          expect_any_instance_of(marketplace_brand_pack_dao).to receive(:get_by_id).twice.
            and_return(marketplace_brand_pack)
          expect_any_instance_of(brand_pack_service).to receive(:brand_pack_active?).and_return(
            brand_pack)
          marketplace_brand_pack_dao.any_instance.stub(:change_state).and_return(marketplace_brand_pack)
          expect(marketplace_brand_pack_service.change_marketplace_brand_pack_state(
            change_state_args)).to_not be_nil
        end

        bp = mpbp.brand_pack
        product = bp.product
        sb = product.sub_brand
        brand = sb.brand
        company = brand.company
        mpbp.delete
        bp.delete
        product.delete
        sb.delete
        brand.delete
        company.delete
      end

      context 'get mpsp' do
        let(:id) { { id: Faker::Number.number(1) } }
        it 'with wrong id' do
          marketplace_brand_pack_dao.any_instance.stub(:get_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ marketplace_brand_pack_service.get_marketplace_brand_pack_by_id(id) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          marketplace_brand_pack_dao.any_instance.stub(:get_by_id).and_return(marketplace_brand_pack)
          expect(marketplace_brand_pack_service.get_marketplace_brand_pack_by_id(id)).to_not be_nil
        end
      end

      context 'create_mpbp_and_brand_pack' do
        let(:brand_pack_request) {
          {
            sku: Faker::Lorem.word,
            sku_size: Faker::Number.number(2),
            brand_pack_code: Faker::Number.number(4)
          }
        }
        it 'with valid args' do
          expect_any_instance_of(brand_pack_service).to receive(:create_brand_pack).and_return(brand_pack)
          expect_any_instance_of(inventory_service).to receive(:get_all_inventories).and_return({elements: [inventory]})
          expect_any_instance_of(inventory_brand_pack_service).to receive(:create_inventory_brand_pack).and_return(inventory_brand_pack)
          expect_any_instance_of(warehouse_service).to receive(:get_all_warehouses).and_return({warehouse: [warehouse]})
          expect_any_instance_of(warehouse_brand_pack_service).to receive(:create).and_return(warehouse_brand_pack)
          expect_any_instance_of(seller_service).to receive(:get_all_sellers).and_return({seller: [seller]})
          expect_any_instance_of(seller_brand_pack_service).to receive(:create_seller_brand_pack).and_return(seller_brand_pack)
          expect(marketplace_brand_pack_service).to receive(:create_marketplace_brand_pack).and_return(marketplace_brand_pack)
          expect(marketplace_brand_pack_service.create_mpbp_and_brand_pack(brand_pack_request)).to_not be_nil
        end
      end
    end
  end
end

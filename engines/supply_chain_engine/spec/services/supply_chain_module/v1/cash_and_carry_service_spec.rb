#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::CashAndCarryService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:cash_and_carry_service) { SupplyChainModule::V1::CashAndCarryService.new }

      let(:cash_and_carry_dao) { SupplyChainModule::V1::CashAndCarryDao }

      let(:cash_and_carry) { FactoryGirl.build(:cash_and_carry) }

      let(:request) {
        {
          name: Faker::Company.name
        }
      }

      context 'create cash_and_carry' do
        it 'with existing cash_and_carry' do
          cash_and_carry_dao.any_instance.stub(:create).and_raise(custom_error_util::InvalidArgumentsError)
          expect{ cash_and_carry_service.create_cash_and_carry(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with new params ' do
          cash_and_carry_dao.any_instance.stub(:create).and_return(cash_and_carry)
          expect(cash_and_carry_service.create_cash_and_carry(request)).to_not be_nil
        end
      end

      context 'get all cash_and_carry' do
        it 'with wrong sort_by' do
          cash_and_carry_dao.any_instance.stub(:paginated_cash_and_carry).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ cash_and_carry_service.get_all_cash_and_carry(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with wrong order' do
          cash_and_carry_dao.any_instance.stub(:paginated_cash_and_carry).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ cash_and_carry_service.get_all_cash_and_carry(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          cash_and_carry_dao.any_instance.stub(:paginated_cash_and_carry).and_return(cash_and_carry)
          expect(cash_and_carry_service.get_all_cash_and_carry(request)).to_not be_nil
        end
      end

      context 'change cash_and_carry state' do
        it 'with wrong transition flow' do
          cash_and_carry_dao.any_instance.stub(:change_state).and_raise(custom_error_util::PreConditionRequiredError)
          expect{ cash_and_carry_service.change_state(request.merge(id: 1)) }.to raise_error(
            custom_error_util::PreConditionRequiredError)
        end

        it 'with wrong event' do
          cash_and_carry_dao.any_instance.stub(:change_state).and_raise(custom_error_util::InvalidArgumentsError)
          expect{ cash_and_carry_service.change_state(request.merge(id: 1)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong request' do
          cash_and_carry_dao.any_instance.stub(:change_state).and_raise(custom_error_util::InsufficientDataError)
          expect{ cash_and_carry_service.change_state(request) }.to raise_error(
            custom_error_util::InsufficientDataError)
        end

        it 'with correct request' do
          cash_and_carry_dao.any_instance.stub(:change_state).and_return(cash_and_carry)
          expect(cash_and_carry_service.change_state(request.merge(id: 1))).to_not be_nil
        end
      end

      context 'update cash_and_carry' do

        it 'with wrong update id' do
          cash_and_carry_dao.any_instance.stub(:get_cash_and_carry_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ cash_and_carry_service.update(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong request' do
          cash_and_carry_dao.any_instance.stub(:update).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ cash_and_carry_service.update(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with correct request' do
          cash_and_carry_dao.any_instance.stub(:get_cash_and_carry_by_id).and_return(cash_and_carry)
          cash_and_carry_dao.any_instance.stub(:update).and_return(cash_and_carry)
          expect(cash_and_carry_service.update(request.merge(id: 1))).to_not be_nil
        end
      end

      context 'get cash_and_carry' do
        let(:id) { { id: Faker::Number.number(1) } }
        it 'with wrong id' do
          cash_and_carry_dao.any_instance.stub(:get_cash_and_carry_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ cash_and_carry_service.get_cash_and_carry(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          cash_and_carry_dao.any_instance.stub(:get_cash_and_carry_by_id).and_return(cash_and_carry)
          expect(cash_and_carry_service.get_cash_and_carry(request)).to_not be_nil
        end
      end
    end
  end
end

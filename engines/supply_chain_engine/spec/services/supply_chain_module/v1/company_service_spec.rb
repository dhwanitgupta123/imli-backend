#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::CompanyService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:company_service) { SupplyChainModule::V1::CompanyService.new }

      let(:company_dao) { SupplyChainModule::V1::CompanyDao }

      let(:company) { FactoryGirl.build(:company) }

      let(:company_request) {
        {
          name: Faker::Company.name,
          logo_url: Faker::Company.logo,
          vat_number: Faker::Company.duns_number,
          tin_number: Faker::Company.duns_number,
          cst: Faker::Company.duns_number,
          account_details: Faker::Business.credit_card_number,
          trade_promotion_margin: Faker::Commerce.price,
          data_sharing_margin: Faker::Commerce.price,
          incentives: Faker::Lorem.characters(10),
          reconciliation_period: Faker::Time.between(DateTime.now - 1, DateTime.now),
          legal_document_url: Faker::Internet.url
        }
      }

      context 'create company' do
        it 'with existing company' do
          expect_any_instance_of(company_dao).to receive(:get_company_by_name).
            and_return(company)

          response = company_service.create_company(company_request)

          expect(response).to equal(company)
        end

        it 'with new params ' do
          expect_any_instance_of(company_dao).to receive(:get_company_by_name).
            and_return(nil)
          expect_any_instance_of(company_dao).to receive(:create).
            and_return(company)

          response = company_service.create_company(company_request)

          expect(response).to equal(company)
        end

        it 'with invalid arguments' do
          expect_any_instance_of(company_dao).to receive(:get_company_by_name).
            and_return(nil)
          expect_any_instance_of(company_dao).to receive(:create).
            and_raise(custom_error_util::InvalidArgumentsError.new)

          expect{ company_service.create_company(company_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update company' do
        it 'with non existing company' do
          expect_any_instance_of(company_dao).to receive(:get_company_by_id).
            and_return(nil)
          expect{ company_service.update_company(company_request, company.id) }.
            to raise_error(custom_error_util::InvalidArgumentsError)       
        end

        it 'with existing company' do
          expect_any_instance_of(company_dao).to receive(:get_company_by_id).
            and_return(company)
          expect_any_instance_of(company_dao).to receive(:update).
            and_return(company)
          response = company_service.update_company(company_request, company.id)
          expect(response).to equal(company)
        end
      end

      context 'change company state' do
        it 'with existing company' do
          expect_any_instance_of(company_dao).to receive(:change_company_state).
            and_return(company)
          response = company_service.change_company_state(Faker::Number.number(1), Faker::Number.number(1))
          expect(response).to equal(company)
        end

        it 'with non existing company' do
           expect_any_instance_of(company_dao).to receive(:change_company_state).
            and_raise(custom_error_util::InvalidArgumentsError.new)
          expect{ company_service.change_company_state(Faker::Number.number(1), Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'get company by id' do
        it 'with existing company' do
          expect_any_instance_of(company_dao).to receive(:get_company_by_id).
            and_return(company)
          response = company_service.get_company_by_id(Faker::Number.number(1))
          expect(response).to equal(company)
        end

        it 'with non existing company' do
          expect_any_instance_of(company_dao).to receive(:get_company_by_id).
            and_return(nil)
          expect{ company_service.get_company_by_id(Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end

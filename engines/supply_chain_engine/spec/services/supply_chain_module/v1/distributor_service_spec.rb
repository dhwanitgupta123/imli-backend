#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::DistributorService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:distributor_service) { SupplyChainModule::V1::DistributorService.new }
      let(:common_states) { SupplyChainCommonModule::V1::CommonStates }
      let(:distributor_dao) { SupplyChainModule::V1::DistributorDao }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:distributor) { FactoryGirl.build(:distributor) }

      let(:add_inventory_api) { SupplyChainModule::V1::AddInventoryApi }
      let(:inventory) { FactoryGirl.build(:inventory, distributor: distributor) }

      let(:distributor_request) {
        {
          name: Faker::Company.name,
          cst_no: Faker::Lorem.characters(10),
          tin_no: Faker::Lorem.characters(10),
          status: common_states::ACTIVE
        }
      }

      let(:update_distributor_request) {
        {
          id: Faker::Number.number(1),
          name: Faker::Company.name,
          cst_no: Faker::Lorem.characters(10),
          tin_no: Faker::Lorem.characters(10)
        }
      }

      let(:change_state_args) {
        {
          id: Faker::Number.number(1),
          event: common_events::ACTIVATE
        }
      }

      context 'create distributor' do
        it 'with valid arguments' do
          expect_any_instance_of(distributor_dao).to receive(:create).
            and_return(distributor)

          response = distributor_service.create_distributor(distributor_request)

          expect(response).to equal(distributor)
        end

        it 'with invalid arguments' do

          expect_any_instance_of(distributor_dao).to receive(:create).
            and_raise(custom_error_util::InvalidArgumentsError.new)

          expect{ distributor_service.create_distributor(distributor_request.merge(name: nil)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'create distributor with inventory' do
        it 'with valid arguments' do
          expect(distributor_service).to receive(:create_distributor).with(distributor_request).
            and_return(distributor)
          expect_any_instance_of(add_inventory_api).to receive(:enact).
            and_return(inventory)
          response = distributor_service.create_distributor_with_inventory(distributor_request)
          expect(response).to equal(distributor)
        end

        it 'with invalid arguments' do
          expect_any_instance_of(distributor_dao).to receive(:create).
            and_raise(custom_error_util::InvalidArgumentsError.new)
          expect{ distributor_service.create_distributor_with_inventory(distributor_request.merge(name: nil)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update distributor' do
        it 'with non existing distributor' do
          expect_any_instance_of(distributor_dao).to receive(:get_by_id).
            and_return(nil)
          expect{ distributor_service.update_distributor(update_distributor_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)       
        end

        it 'with existing distributor' do
          expect_any_instance_of(distributor_dao).to receive(:get_by_id).
            and_return(distributor)
          expect_any_instance_of(distributor_dao).to receive(:update).
            and_return(distributor)
          response = distributor_service.update_distributor(update_distributor_request)
          expect(response).to equal(distributor)
        end
      end

      context 'change distributor state' do

        it 'with existing distributor' do
          expect_any_instance_of(distributor_dao).to receive(:get_by_id).
            and_return(distributor)
          expect_any_instance_of(distributor_dao).to receive(:change_state).
            and_return(distributor)
          response = distributor_service.change_distributor_state(change_state_args)
          expect(response).to equal(distributor)
        end

        it 'with non existing distributor' do
          expect_any_instance_of(distributor_dao).to receive(:get_by_id).
            and_return(distributor)
           expect_any_instance_of(distributor_dao).to receive(:change_state).
            and_raise(custom_error_util::InvalidArgumentsError.new)
          expect{ distributor_service.change_distributor_state(change_state_args) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'get distributor by id' do
        it 'with existing distributor' do
          expect_any_instance_of(distributor_dao).to receive(:get_by_id).
            and_return(distributor)
          response = distributor_service.get_distributor_by_id(Faker::Number.number(1))
          expect(response).to equal(distributor)
        end

        it 'with non existing distributor' do
          expect_any_instance_of(distributor_dao).to receive(:get_by_id).
            and_return(nil)
          expect{ distributor_service.get_distributor_by_id(Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end

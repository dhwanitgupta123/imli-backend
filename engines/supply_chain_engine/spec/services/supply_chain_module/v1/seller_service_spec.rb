#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::SellerService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:seller_service) { SupplyChainModule::V1::SellerService.new }

      let(:seller_dao) { SupplyChainModule::V1::SellerDao }

      let(:seller) { FactoryGirl.build(:seller) }

      let(:request) {
        {
          name: Faker::Company.name
        }
      }

      context 'create seller' do
        it 'with existing seller' do
          seller_dao.any_instance.stub(:create).and_raise(custom_error_util::InvalidArgumentsError)
          expect{ seller_service.create_seller(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with new params ' do
          seller_dao.any_instance.stub(:create).and_return(seller)
          expect(seller_service.create_seller(request)).to_not be_nil
        end
      end

      context 'get all sellers' do
        it 'with wrong sort_by' do
          seller_dao.any_instance.stub(:paginated_seller).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ seller_service.get_all_sellers(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with wrong order' do
          seller_dao.any_instance.stub(:paginated_seller).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ seller_service.get_all_sellers(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          seller_dao.any_instance.stub(:paginated_seller).and_return(seller)
          expect(seller_service.get_all_sellers(request)).to_not be_nil
        end
      end

      context 'change seller state' do
        it 'with wrong transition flow' do
          seller_dao.any_instance.stub(:change_state).and_raise(custom_error_util::PreConditionRequiredError)
          expect{ seller_service.change_state(request.merge(id: 1)) }.to raise_error(
            custom_error_util::PreConditionRequiredError)
        end

        it 'with wrong event' do
          seller_dao.any_instance.stub(:change_state).and_raise(custom_error_util::InvalidArgumentsError)
          expect{ seller_service.change_state(request.merge(id: 1)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong request' do
          seller_dao.any_instance.stub(:change_state).and_raise(custom_error_util::InsufficientDataError)
          expect{ seller_service.change_state(request) }.to raise_error(
            custom_error_util::InsufficientDataError)
        end

        it 'with correct request' do
          seller_dao.any_instance.stub(:change_state).and_return(seller)
          expect(seller_service.change_state(request.merge(id: 1))).to_not be_nil
        end
      end

      context 'get seller' do
        let(:id) { { id: Faker::Number.number(1) } }
        it 'with wrong id' do
          seller_dao.any_instance.stub(:get_seller_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ seller_service.get_seller(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          seller_dao.any_instance.stub(:get_seller_by_id).and_return(seller)
          expect(seller_service.get_seller(request)).to_not be_nil
        end
      end

      context 'update seller' do

        it 'with wrong update id' do
          seller_dao.any_instance.stub(:get_seller_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ seller_service.update(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong request' do
          seller_dao.any_instance.stub(:update).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ seller_service.update(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with correct request' do
          seller_dao.any_instance.stub(:get_seller_by_id).and_return(seller)
          seller_dao.any_instance.stub(:update).and_return(seller)
          expect(seller_service.update(request.merge(id: 1))).to_not be_nil
        end
      end

      context 'get seller' do
        let(:id) { { id: Faker::Number.number(1) } }
        it 'with wrong id' do
          seller_dao.any_instance.stub(:get_seller_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ seller_service.get_seller(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          seller_dao.any_instance.stub(:get_seller_by_id).and_return(seller)
          expect(seller_service.get_seller(request)).to_not be_nil
        end
      end
      
    end
  end
end
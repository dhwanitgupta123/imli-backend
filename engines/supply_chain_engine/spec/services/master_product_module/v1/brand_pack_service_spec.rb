#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::BrandPackService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:brand_pack_service) { MasterProductModule::V1::BrandPackService.new }
      let(:sub_category_model) { CategorizationModule::V1::Category }
      let(:image_sevice) { ImageServiceModule::V1::ImageService }

      let(:department) { FactoryGirl.build_stubbed(:department) }
      let(:category) { FactoryGirl.build_stubbed(:category, department: department) }
      let(:sub_category) { FactoryGirl.build_stubbed(:category, parent_category: category) }

      let(:brand_pack_helper) { MasterProductModule::V1::BrandPackHelper }
      let(:brand_pack_dao) { MasterProductModule::V1::BrandPackDao }
      let(:product_dao) { MasterProductModule::V1::ProductDao }

      let(:brand_pack) { FactoryGirl.build_stubbed(:brand_pack) }
      let(:product) { FactoryGirl.build_stubbed(:product) }
      let(:brand_pack_code) { Faker::Lorem.characters(10) }
      let(:brand_pack_request) {
        {
          sku: Faker::Company.ein,
          mrp: Faker::Commerce.price,
          sku_size: Faker::Lorem.characters(10),
          unit: Faker::Lorem.characters(5),
          description: Faker::Lorem.sentence,
          shelf_life: Faker::Internet.slug(Faker::Number.between(1, 12).to_s, 'months'),
          product_id: 1,
          sub_category_id: 1
        }
      }

      context 'create brand_pack' do
        it 'without product' do
          expect_any_instance_of(product_dao).to receive(:get_product_by_id).
            and_return(nil)

          expect{ brand_pack_service.create_brand_pack(brand_pack_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with existing product ' do
          expect(sub_category_model).to receive(:find_by_category_id).and_return(sub_category)
          expect_any_instance_of(product_dao).to receive(:get_product_by_id).
            and_return(product)
          expect_any_instance_of(brand_pack_dao).to receive(:create).
            and_return(brand_pack)
          brand_pack_helper.stub(:create_brand_pack_code).and_return(brand_pack_code)
          response = brand_pack_service.create_brand_pack(brand_pack_request)

          expect(response).to equal(brand_pack)
        end

        it 'with invalid arguments' do
          expect_any_instance_of(product_dao).to receive(:get_product_by_id).
            and_return(product)
          expect(sub_category_model).to receive(:find_by_category_id).and_return(sub_category)
          expect_any_instance_of(brand_pack_dao).to receive(:create).
            and_raise(custom_error_util::InvalidArgumentsError.new)
          brand_pack_helper.stub(:create_brand_pack_code).and_return(brand_pack_code)
          expect{ brand_pack_service.create_brand_pack(brand_pack_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with non existing sub_category' do
          expect_any_instance_of(product_dao).to receive(:get_product_by_id).
            and_return(product)
          expect(sub_category_model).to receive(:find_by_category_id).and_return(nil)

          expect{ brand_pack_service.create_brand_pack(brand_pack_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update brand_pack' do
        it ' product_id with non existing product' do
          expect_any_instance_of(product_dao).to receive(:get_product_by_id).
            and_return(nil)
          expect{ brand_pack_service.update_brand_pack(brand_pack_request, 1) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it ' product_id with not existing sub_category' do
          expect_any_instance_of(product_dao).to receive(:get_product_by_id).
            and_return(product)
          expect(sub_category_model).to receive(:find_by_category_id).and_return(nil)

          expect{ brand_pack_service.update_brand_pack(brand_pack_request, 1) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it ' product_id with existing product and sub category' do

          expect(sub_category_model).to receive(:find_by_category_id).and_return(sub_category)
          expect_any_instance_of(product_dao).to receive(:get_product_by_id).
            and_return(product)
          expect_any_instance_of(brand_pack_dao).to receive(:get_brand_pack_by_id).
            and_return(brand_pack)
          brand_pack_helper.stub(:update_brand_pack_code).and_return(brand_pack_code)
          expect_any_instance_of(brand_pack_dao).to receive(:update).
            and_return(brand_pack)
          response = brand_pack_service.update_brand_pack(brand_pack_request, 1)

          expect(response).to equal(brand_pack)
        end

        it 'with non existing brand_pack' do
          expect_any_instance_of(product_dao).to receive(:get_product_by_id).
            and_return(product)
          expect(sub_category_model).to receive(:find_by_category_id).and_return(sub_category)

          expect_any_instance_of(brand_pack_dao).to receive(:get_brand_pack_by_id).
            and_return(nil)

          expect{ brand_pack_service.update_brand_pack(brand_pack_request, 1) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'change brand_pack state' do
        it 'with existing brand_pack' do
          expect_any_instance_of(brand_pack_dao).to receive(:change_brand_pack_state).
            and_return(brand_pack)
          response = brand_pack_service.change_brand_pack_state(Faker::Number.number(1), Faker::Number.number(1))
          expect(response).to equal(brand_pack)
        end

        it 'with non existing brand_pack' do
           expect_any_instance_of(brand_pack_dao).to receive(:change_brand_pack_state).
            and_raise(custom_error_util::InvalidArgumentsError.new)
          expect{ brand_pack_service.change_brand_pack_state(Faker::Number.number(1), Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'get brand_pack by id' do
        it 'with existing brand_pack' do
          expect_any_instance_of(brand_pack_dao).to receive(:get_brand_pack_by_id).
            and_return(brand_pack)
          response = brand_pack_service.get_brand_pack_by_id(Faker::Number.number(1))
          expect(response).to equal(brand_pack)
        end

        it 'with non existing brand_pack' do
          expect_any_instance_of(brand_pack_dao).to receive(:get_brand_pack_by_id).
            and_return(nil)
          expect{ brand_pack_service.get_brand_pack_by_id(Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end

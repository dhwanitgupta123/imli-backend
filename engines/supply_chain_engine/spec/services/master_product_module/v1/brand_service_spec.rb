#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::BrandService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:brand_service) { MasterProductModule::V1::BrandService.new }

      let(:brand_dao) { MasterProductModule::V1::BrandDao }
      let(:company_dao) { SupplyChainModule::V1::CompanyDao }

      let(:brand) { FactoryGirl.build(:brand) }
      let(:company) { FactoryGirl.build(:company) }

      let(:brand_request) {
        {
          name: Faker::Company.name,
          code: Faker::Lorem.characters(5),
          logo_url: Faker::Company.logo,
          company_id: 1
        }
      }

      context 'create brand' do
        it 'without company' do
          expect_any_instance_of(company_dao).to receive(:get_company_by_id).
            and_return(nil)

          expect{ brand_service.create_brand(brand_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with existing Company ' do
          expect_any_instance_of(company_dao).to receive(:get_company_by_id).
            and_return(company)
          expect_any_instance_of(brand_dao).to receive(:create).
            and_return(brand)

          response = brand_service.create_brand(brand_request)

          expect(response).to equal(brand)
        end

        it 'with invalid arguments' do
          expect_any_instance_of(company_dao).to receive(:get_company_by_id).
            and_return(company)
          expect_any_instance_of(brand_dao).to receive(:create).
            and_raise(custom_error_util::InvalidArgumentsError.new)

          expect{ brand_service.create_brand(brand_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update brand' do
        it ' company_id with non existing company' do
          expect_any_instance_of(company_dao).to receive(:get_company_by_id).
            and_return(nil)

          expect{ brand_service.update_brand(brand_request, 1) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it ' company_id with existing company' do
          expect_any_instance_of(company_dao).to receive(:get_company_by_id).
            and_return(company)
          expect_any_instance_of(brand_dao).to receive(:get_brand_by_id).
            and_return(brand)

          expect_any_instance_of(brand_dao).to receive(:update).
            and_return(brand)
          response = brand_service.update_brand(brand_request, 1)

          expect(response).to equal(brand)
        end

        it 'with non existing brand' do
          expect_any_instance_of(company_dao).to receive(:get_company_by_id).
            and_return(company)
          expect_any_instance_of(brand_dao).to receive(:get_brand_by_id).
            and_return(nil)

          expect{ brand_service.update_brand(brand_request, 1) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'change brand state' do
        it 'with existing brand' do
          expect_any_instance_of(brand_dao).to receive(:change_brand_state).
            and_return(brand)
          response = brand_service.change_brand_state(Faker::Number.number(1), Faker::Number.number(1))
          expect(response).to equal(brand)
        end

        it 'with non existing brand' do
           expect_any_instance_of(brand_dao).to receive(:change_brand_state).
            and_raise(custom_error_util::InvalidArgumentsError.new)
          expect{ brand_service.change_brand_state(Faker::Number.number(1), Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'get brand by id' do
        it 'with existing brand' do
          expect_any_instance_of(brand_dao).to receive(:get_brand_by_id).
            and_return(brand)
          response = brand_service.get_brand_by_id(Faker::Number.number(1))
          expect(response).to equal(brand)
        end

        it 'with non existing brand' do
          expect_any_instance_of(brand_dao).to receive(:get_brand_by_id).
            and_return(nil)
          expect{ brand_service.get_brand_by_id(Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end

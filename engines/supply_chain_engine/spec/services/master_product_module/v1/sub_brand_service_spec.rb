#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::SubBrandService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:sub_brand_service) { MasterProductModule::V1::SubBrandService.new }

      let(:sub_brand_dao) { MasterProductModule::V1::SubBrandDao }
      let(:brand_dao) { MasterProductModule::V1::BrandDao }

      let(:sub_brand) { FactoryGirl.build_stubbed(:sub_brand) }
      let(:brand) { FactoryGirl.build_stubbed(:brand) }

      let(:sub_brand_request) {
        {
          name: Faker::Company.name,
          logo_url: Faker::Company.logo,
          brand_id: 1
        }
      }

      context 'create sub_brand' do
        it 'without brand' do
          expect_any_instance_of(brand_dao).to receive(:get_brand_by_id).
            and_return(nil)

          expect{ sub_brand_service.create_sub_brand(sub_brand_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with existing brand ' do
          expect_any_instance_of(brand_dao).to receive(:get_brand_by_id).
            and_return(brand)
          expect_any_instance_of(sub_brand_dao).to receive(:create).
            and_return(sub_brand)

          response = sub_brand_service.create_sub_brand(sub_brand_request)

          expect(response).to equal(sub_brand)
        end

        it 'with invalid arguments' do
          expect_any_instance_of(brand_dao).to receive(:get_brand_by_id).
            and_return(brand)
          expect_any_instance_of(sub_brand_dao).to receive(:create).
            and_raise(custom_error_util::InvalidArgumentsError.new)

          expect{ sub_brand_service.create_sub_brand(sub_brand_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update sub_brand' do
        it ' brand_id with non existing brand' do
          expect_any_instance_of(brand_dao).to receive(:get_brand_by_id).
            and_return(nil)

          expect{ sub_brand_service.update_sub_brand(sub_brand_request, 1) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it ' brand_id with existing brand' do
          expect_any_instance_of(brand_dao).to receive(:get_brand_by_id).
            and_return(brand)
          expect_any_instance_of(sub_brand_dao).to receive(:get_sub_brand_by_id).
            and_return(sub_brand)

          expect_any_instance_of(sub_brand_dao).to receive(:update).
            and_return(sub_brand)
          response = sub_brand_service.update_sub_brand(sub_brand_request, 1)

          expect(response).to equal(sub_brand)
        end

        it 'with non existing sub_brand' do
          expect_any_instance_of(brand_dao).to receive(:get_brand_by_id).
            and_return(brand)
          expect_any_instance_of(sub_brand_dao).to receive(:get_sub_brand_by_id).
            and_return(nil)

          expect{ sub_brand_service.update_sub_brand(sub_brand_request, 1) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'change sub_brand state' do
        it 'with existing sub_brand' do
          expect_any_instance_of(sub_brand_dao).to receive(:change_sub_brand_state).
            and_return(sub_brand)
          response = sub_brand_service.change_sub_brand_state(Faker::Number.number(1), Faker::Number.number(1))
          expect(response).to equal(sub_brand)
        end

        it 'with non existing sub_brand' do
           expect_any_instance_of(sub_brand_dao).to receive(:change_sub_brand_state).
            and_raise(custom_error_util::InvalidArgumentsError.new)
          expect{ sub_brand_service.change_sub_brand_state(Faker::Number.number(1), Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'get sub_brand by id' do
        it 'with existing sub_brand' do
          expect_any_instance_of(sub_brand_dao).to receive(:get_sub_brand_by_id).
            and_return(sub_brand)
          response = sub_brand_service.get_sub_brand_by_id(Faker::Number.number(1))
          expect(response).to equal(sub_brand)
        end

        it 'with non existing product' do
          expect_any_instance_of(sub_brand_dao).to receive(:get_sub_brand_by_id).
            and_return(nil)
          expect{ sub_brand_service.get_sub_brand_by_id(Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end

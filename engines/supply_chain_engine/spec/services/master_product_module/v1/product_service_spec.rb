#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::ProductService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:product_service) { MasterProductModule::V1::ProductService.new }
      let(:product_helper) { MasterProductModule::V1::ProductHelper }
      let(:product_dao) { MasterProductModule::V1::ProductDao }
      let(:sub_brand_dao) { MasterProductModule::V1::SubBrandDao }
      let(:product) { FactoryGirl.build_stubbed(:product) }
      let(:sub_brand) { FactoryGirl.build_stubbed(:sub_brand) }
      let(:product_code) { Faker::Lorem.characters(10) }
      let(:product_request) {
        {
          name: Faker::Company.name,
          logo_url: Faker::Company.logo,
          sub_brand_id: 1
        }
      }

      context 'create product' do
        it 'without sub_brand' do
          expect_any_instance_of(sub_brand_dao).to receive(:get_sub_brand_by_id).
            and_return(nil)

          expect{ product_service.create_product(product_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with existing sub_brand ' do
          expect_any_instance_of(sub_brand_dao).to receive(:get_sub_brand_by_id).
            and_return(sub_brand)
          product_helper.stub(:create_product_code).and_return(product_code)
          expect_any_instance_of(product_dao).to receive(:create).
            and_return(product)

          response = product_service.create_product(product_request)

          expect(response).to equal(product)
        end

        it 'with invalid arguments' do
          expect_any_instance_of(sub_brand_dao).to receive(:get_sub_brand_by_id).
            and_return(sub_brand)
          product_helper.stub(:create_product_code).and_return(product_code)
          expect_any_instance_of(product_dao).to receive(:create).
            and_raise(custom_error_util::InvalidArgumentsError.new)

          expect{ product_service.create_product(product_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update product' do
        it ' sub_brand_id with non existing sub_brand' do
          expect_any_instance_of(sub_brand_dao).to receive(:get_sub_brand_by_id).
            and_return(nil)

          expect{ product_service.update_product(product_request, 1) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it ' sub_brand_id with existing sub_brand' do
          expect_any_instance_of(sub_brand_dao).to receive(:get_sub_brand_by_id).
            and_return(sub_brand)
          expect_any_instance_of(product_dao).to receive(:get_product_by_id).
            and_return(product)
          product_helper.stub(:update_product_code).and_return(product_code)

          expect_any_instance_of(product_dao).to receive(:update).
            and_return(product)
          response = product_service.update_product(product_request, 1)

          expect(response).to equal(product)
        end

        it 'with non existing product' do
          expect_any_instance_of(sub_brand_dao).to receive(:get_sub_brand_by_id).
            and_return(sub_brand)
          expect_any_instance_of(product_dao).to receive(:get_product_by_id).
            and_return(nil)

          expect{ product_service.update_product(product_request, 1) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'change product state' do
        it 'with existing product' do
          expect_any_instance_of(product_dao).to receive(:change_product_state).
            and_return(product)
          response = product_service.change_product_state(Faker::Number.number(1), Faker::Number.number(1))
          expect(response).to equal(product)
        end

        it 'with non existing product' do
           expect_any_instance_of(product_dao).to receive(:change_product_state).
            and_raise(custom_error_util::InvalidArgumentsError.new)
          expect{ product_service.change_product_state(Faker::Number.number(1), Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'get product by id' do
        it 'with existing product' do
          expect_any_instance_of(product_dao).to receive(:get_product_by_id).
            and_return(product)
          response = product_service.get_product_by_id(Faker::Number.number(1))
          expect(response).to equal(product)
        end

        it 'with non existing product' do
          expect_any_instance_of(product_dao).to receive(:get_product_by_id).
            and_return(nil)
          expect{ product_service.get_product_by_id(Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SearchModule::V1::MarketplaceSellingPackIndexService do
      let(:categorization_api_decorator) { CategorizationModule::V1::MpspCategorizationApiResponseDecorator }
      let(:marketplace_selling_pack_dao) { MarketplaceProductModule::V1::MarketplaceSellingPackDao }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:marketplace_selling_pack) { FactoryGirl.build_stubbed(:marketplace_selling_pack) }
      let(:marketplace_selling_pack_repository) { SearchModule::V1::MarketplaceSellingPackRepository }
      let(:mpsp_index_service_class) { SearchModule::V1::MarketplaceSellingPackIndexService }
      let(:mpsp_hash) {
        {
          id: Faker::Number.number(1),
          display_name: Faker::Lorem.characters(10),
          description: Faker::Lorem.characters(10)
        }
      }
      context 'search in index' do
        it 'with valid repository' do
          marketplace_selling_pack_model = SearchModule::V1::MarketplaceSellingPackSearchModel.new
          marketplace_selling_pack_index_service = SearchModule::V1::MarketplaceSellingPackIndexService.new
          marketplace_selling_pack_array = []
          marketplace_selling_pack_array.push(marketplace_selling_pack)
          expect(categorization_api_decorator).to receive(:create_display_product_hash).and_return(mpsp_hash)
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:get_all).and_return({ elements: marketplace_selling_pack_array })
          expect_any_instance_of(mpsp_index_service_class).to receive(:repository_exists?).twice.and_return(true)
          expect_any_instance_of(mpsp_index_service_class).to receive(:index_exists?).and_return(true)
          expect_any_instance_of(mpsp_index_service_class).to receive(:delete_previous_index).and_return(true)
          expect_any_instance_of(mpsp_index_service_class).to receive(:save).and_return(true)
          marketplace_selling_pack_index_service.initialize_index('true')
          marketplace_selling_pack_model_reader = SearchModule::V1::MarketplaceSellingPackModelReader.new({})
          expect_any_instance_of(mpsp_index_service_class).to receive(:search).and_return([marketplace_selling_pack_model])

          data = marketplace_selling_pack_index_service.get_results(Faker::Lorem.characters(10))
          expect(data).not_to equal(marketplace_selling_pack_model_reader)
        end

        it 'with invalid repository' do
          marketplace_selling_pack_index_service = SearchModule::V1::MarketplaceSellingPackIndexService.new
          expect_any_instance_of(mpsp_index_service_class).to receive(:repository_exists?).and_return(false)
          response = marketplace_selling_pack_index_service.initialize_index
          expect(response).to eq(false)
        end
      end
    end
  end
end

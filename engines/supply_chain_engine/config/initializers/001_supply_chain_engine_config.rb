# SupplyChain Engine CONFIG initialization should be done before engine initialization
# Load all environment specific application config variables into SUPPLY_CHAIN_ENGINE_CONFIG variable

supply_chain_engine_config_file = SupplyChainEngine::Engine.root.join("config", "supply_chain_engine_config.yml").to_s

# Global variable SUPPLY_CHAIN_ENGINE_CONFIG to store all environment specific config
# This is similar to APP_CONFIG in application
SUPPLY_CHAIN_ENGINE_CONFIG = {}

if File.exists?(supply_chain_engine_config_file)
  YAML.load_file(supply_chain_engine_config_file)[Rails.env].each do |key, value|
    SUPPLY_CHAIN_ENGINE_CONFIG[key.to_s] = value.to_s
  end # end YAML.load_file
end # end if

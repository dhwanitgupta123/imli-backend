require 'route_constraints'
SupplyChainEngine::Engine.routes.draw do
  # Predicate for versioning based on app version
  match_app_version_predicate = Proc.new do |version, request|
    CommonModule::V1::RoutingPredicates.match_app_version(version, request)
  end
  # Route for SupplyChainModule with versioning and constraint matching
  scope module: :supply_chain_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      
      get 'supply_chain/companies/tree' => 'company#get_companies_tree'
      post 'supply_chain/companies/new' => 'company#add_company'
      put 'supply_chain/companies/:id' => 'company#update_company'
      get 'supply_chain/companies' => 'company#get_all_companies'
      put 'supply_chain/companies/state/:id' => 'company#change_state'
      delete 'supply_chain/companies/:id' => 'company#delete_company'
      get 'supply_chain/companies/:id' => 'company#get_company'

      post 'supply_chain/sellers/new' => 'seller#add_seller'
      put 'supply_chain/sellers/state/:id' => 'seller#change_state'
      get 'supply_chain/sellers' => 'seller#get_all_sellers'
      get 'supply_chain/sellers/:id' => 'seller#get_seller'
      delete 'supply_chain/sellers/:id' => 'seller#delete_seller'
      put 'supply_chain/sellers/:id' => 'seller#update_seller'

      post 'supply_chain/distributors/new' => 'distributors#add_distributor'
      put 'supply_chain/distributors/:id' => 'distributors#update_distributor'
      get 'supply_chain/distributors' => 'distributors#get_all_distributors'
      put 'supply_chain/distributors/state/:id' => 'distributors#change_state'
      delete 'supply_chain/distributors/:id' => 'distributors#delete_distributor'
      get 'supply_chain/distributors/:id' => 'distributors#get_distributor'
      post 'supply_chain/distributors/register' => 'distributors#vendor_registration'

      post 'supply_chain/inventories/new' => 'inventories#add_inventory'
      put 'supply_chain/inventories/:id' => 'inventories#update_inventory'
      get 'supply_chain/inventories' => 'inventories#get_all_inventories'
      put 'supply_chain/inventories/state/:id' => 'inventories#change_state'
      delete 'supply_chain/inventories/:id' => 'inventories#delete_inventory'
      get 'supply_chain/inventories/:id' => 'inventories#get_inventory'
      post 'supply_chain/inventories/products' => 'inventories#upload_products'
      
      post 'supply_chain/cash_and_carry/new' => 'cash_and_carry#add_cash_and_carry'
      put 'supply_chain/cash_and_carry/state/:id' => 'cash_and_carry#change_state'
      get 'supply_chain/cash_and_carry' => 'cash_and_carry#get_all_cash_and_carry'
      get 'supply_chain/cash_and_carry/:id' => 'cash_and_carry#get_cash_and_carry'
      delete 'supply_chain/cash_and_carry/:id' => 'cash_and_carry#delete_cash_and_carry'
      put 'supply_chain/cash_and_carry/:id' => 'cash_and_carry#update_cash_and_carry'

      post 'supply_chain/warehouses/new' => 'warehouses#add_warehouse'
      put 'supply_chain/warehouses/state/:id' => 'warehouses#change_state'
      get 'supply_chain/warehouses' => 'warehouses#get_all_warehouses'
      get 'supply_chain/warehouses/:id' => 'warehouses#get_warehouse'
      delete 'supply_chain/warehouses/:id' => 'warehouses#delete_warehouse'
      put 'supply_chain/warehouses/:id' => 'warehouses#update_warehouse'
      post 'supply_chain/warehouses/eod' => 'warehouses#warehouse_end_of_day'
      get 'supply_chain/warehouses/stock/:id' => 'warehouses#get_warehouse_stock_details'
      get 'supply_chain/warehouses/procurement_prediction/:id' => 'warehouses#get_warehouse_procurement_prediction'
      get 'supply_chain/warehouses/stock/download/:id' => 'warehouses#download_warehouse_stock_details'
      get 'supply_chain/warehouses/procurement_prediction/download/:id' => 'warehouses#download_warehouse_procurement_prediction'

    end
  end
  # Route for MasterProductModule with versioning and constraint matching
  scope module: :master_product_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
  
      post 'supply_chain/brands/new' => 'brand#add_brand'
      put 'supply_chain/brands/:id' => 'brand#update_brand'
      get 'supply_chain/brands' => 'brand#get_all_brands'
      put 'supply_chain/brands/state/:id' => 'brand#change_state'
      delete 'supply_chain/brands/:id' => 'brand#delete_brand'
      get 'supply_chain/brands/:id' => 'brand#get_brand'

      post 'supply_chain/sub_brands/new' => 'sub_brand#add_sub_brand'
      put 'supply_chain/sub_brands/:id' => 'sub_brand#update_sub_brand'
      get 'supply_chain/sub_brands' => 'sub_brand#get_all_sub_brands'
      put 'supply_chain/sub_brands/state/:id' => 'sub_brand#change_state'
      delete 'supply_chain/sub_brands/:id' => 'sub_brand#delete_sub_brand'
      get 'supply_chain/sub_brands/:id' => 'sub_brand#get_sub_brand'

      post 'supply_chain/products/new' => 'product#add_product'
      put 'supply_chain/products/:id' => 'product#update_product'
      get 'supply_chain/products' => 'product#get_all_products'
      put 'supply_chain/products/state/:id' => 'product#change_state'
      delete 'supply_chain/products/:id' => 'product#delete_product'
      get 'supply_chain/products/:id' => 'product#get_product'

      post 'supply_chain/brand_packs/new' => 'brand_pack#add_brand_pack'
      post 'supply_chain/brand_packs/marketplace_brand_packs/new' => 'brand_pack#add_brand_pack_to_mpbp'
      put 'supply_chain/brand_packs/marketplace_brand_packs/:id' => 'brand_pack#update_brand_pack_to_mpbp'
      put 'supply_chain/brand_packs/:id' => 'brand_pack#update_brand_pack'
      get 'supply_chain/brand_packs' => 'brand_pack#get_all_brand_packs'
      put 'supply_chain/brand_packs/state/:id' => 'brand_pack#change_state'
      delete 'supply_chain/brand_packs/:id' => 'brand_pack#delete_brand_pack'
      get 'supply_chain/brand_packs/:id' => 'brand_pack#get_brand_pack'

      post 'supply_chain/variants/new' => 'variants#add_variant'
      put 'supply_chain/variants/:id' => 'variants#update_variant'
      get 'supply_chain/variants' => 'variants#get_all_variants'
    end
  end

  # Route for WarehouseProductModule with versioning and constraint matching
  scope module: :warehouse_product_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      post 'supply_chain/warehouse_brand_packs/new' => 'warehouse_brand_packs#add_warehouse_brand_pack'
      put 'supply_chain/warehouse_brand_packs/state/:id' => 'warehouse_brand_packs#change_state'
      get 'supply_chain/warehouse_brand_packs' => 'warehouse_brand_packs#get_all_warehouse_brand_packs'
      get 'supply_chain/warehouse_brand_packs/:id' => 'warehouse_brand_packs#get_warehouse_brand_pack'
      delete 'supply_chain/warehouse_brand_packs/:id' => 'warehouse_brand_packs#delete_warehouse_brand_pack'
      put 'supply_chain/warehouse_brand_packs/:id' => 'warehouse_brand_packs#update_warehouse_brand_pack'
      put 'supply_chain/warehouse_brand_packs/stock/:warehouse_id' => 'warehouse_brand_packs#stock_warehouse_brand_packs'
      put 'supply_chain/warehouse_brand_packs/stock/status/:warehouse_id' => 'warehouse_brand_packs#stock_warehouse_brand_packs_status'
      get 'warehouse/:id/warehouse_brand_packs' => 'warehouse_brand_packs#get_all_warehouse_brand_packs_by_warehouse'
    end
  end

  # Route for SellerProductModule with versioning and constraint matching
  scope module: :seller_product_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      
      post 'supply_chain/seller_brand_packs/new' => 'seller_brand_pack#add_seller_brand_pack'
      put 'supply_chain/seller_brand_packs/state/:id' => 'seller_brand_pack#change_state'
      get 'supply_chain/seller_brand_packs' => 'seller_brand_pack#get_all_seller_brand_packs'
      put 'supply_chain/seller_brand_packs/:id'  => 'seller_brand_pack#update_seller_brand_pack'
      get 'supply_chain/seller_brand_packs/:id' => 'seller_brand_pack#get_seller_brand_pack'
      delete 'supply_chain/seller_brand_packs/:id' => 'seller_brand_pack#delete_seller_brand_pack'
    end
  end 

   # Route for InventoryProductModule with versioning and constraint matching
  scope module: :inventory_product_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      post 'supply_chain/inventory_brand_packs/new' => 'inventory_brand_packs#add_inventory_brand_pack'
      put 'supply_chain/inventory_brand_packs/:id' => 'inventory_brand_packs#update_inventory_brand_pack'
      get 'supply_chain/inventory_brand_packs' => 'inventory_brand_packs#get_all_inventory_brand_packs'
      put 'supply_chain/inventory_brand_packs/state/:id' => 'inventory_brand_packs#change_state'
      delete 'supply_chain/inventory_brand_packs/:id' => 'inventory_brand_packs#delete_inventory_brand_pack'
      get 'supply_chain/inventory_brand_packs/:id' => 'inventory_brand_packs#get_inventory_brand_pack'
      get 'inventory/:id/inventory_brand_packs' => 'inventory_brand_packs#get_all_inventory_brand_packs_by_inventory'
    end
  end

  # Route for MarketplaceProductModule with versioning and constraint matching
  scope module: :marketplace_product_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      post 'supply_chain/marketplace_selling_packs/new' => 'marketplace_selling_packs#add_marketplace_selling_pack'
      put 'supply_chain/marketplace_selling_packs/:id' => 'marketplace_selling_packs#update_marketplace_selling_pack'
      get 'supply_chain/marketplace_selling_packs' => 'marketplace_selling_packs#get_all_marketplace_selling_packs'
      put 'supply_chain/marketplace_selling_packs/state/:id' => 'marketplace_selling_packs#change_state'
      delete 'supply_chain/marketplace_selling_packs/:id' => 'marketplace_selling_packs#delete_marketplace_selling_pack'
      get 'supply_chain/marketplace_selling_packs/:id' => 'marketplace_selling_packs#get_marketplace_selling_pack'
      get 'supply_chain/mpsp_by_department_and_category' => 'marketplace_selling_packs#get_mpsp_by_department_and_category'

      post 'supply_chain/marketplace_brand_packs/new' => 'marketplace_brand_packs#add_marketplace_brand_pack'
      put 'supply_chain/marketplace_brand_packs/:id' => 'marketplace_brand_packs#update_marketplace_brand_pack'
      get 'supply_chain/marketplace_brand_packs' => 'marketplace_brand_packs#get_all_marketplace_brand_packs'
      put 'supply_chain/marketplace_brand_packs/state/:id' => 'marketplace_brand_packs#change_state'
      delete 'supply_chain/marketplace_brand_packs/:id' => 'marketplace_brand_packs#delete_marketplace_brand_pack'
      get 'supply_chain/marketplace_brand_packs/:id' => 'marketplace_brand_packs#get_marketplace_brand_pack'

      post 'supply_chain/marketplace_brand_packs/brand_pack' => 'marketplace_brand_packs#create_mpbp_and_brand_pack'
    end
  end

  # Route for Warehoue Module with versioning & constraint matching
  scope module: :supply_chain_module, defaults: { format: 'json' } do
    scope module: :v1, defaults: { format: 'json' } do
      scope module: :warehouse_module, defaults: { format: 'json' } do
        scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
            post 'supply_chain/warehouse/address/new' => 'warehouse_addresses#add_address'
            put 'supply_chain/warehouse/address/:address_id' => 'warehouse_addresses#update_address'
            get 'supply_chain/warehouse/address' => 'warehouse_addresses#get_all_addresses'
        end
      end
    end
  end

  # Route for Inventory Module with versioning & constraint matching
  scope module: :supply_chain_module, defaults: { format: 'json' } do
    scope module: :v1, defaults: { format: 'json' } do
      scope module: :inventory_module, defaults: { format: 'json' } do
        scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
            post 'supply_chain/inventory/address/new' => 'inventory_addresses#add_address'
            put 'supply_chain/inventory/address/:address_id' => 'inventory_addresses#update_address'
            get 'supply_chain/inventory/address' => 'inventory_addresses#get_all_addresses'
        end
      end
    end
  end

  # Route for SearchModule with versioning and constraint matching
  scope module: :search_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      get 'product/search' => 'marketplace_selling_pack_search#get_marketplace_selling_pack_by_query'
    end
  end

  # Route for SearchModule with versioning and constraint matching
  scope module: :cluster_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      post 'clusters/new' => 'clusters#add_cluster'
      put 'clusters/:id' => 'clusters#update_cluster'
      put 'clusters/state/:id' => 'clusters#change_state'
      get 'clusters/:id' => 'clusters#get_cluster'
      delete 'clusters/:id' => 'clusters#delete_cluster'
      get 'clusters' =>  'clusters#get_all_clusters'
    end
  end

  # Route for PACKAGING MODULEwith versioning and constraint matching
  scope module: :packaging_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do

      post 'packaging_boxes/new'   => 'packaging_boxes#add_packaging_box'
      put 'packaging_boxes/:id'    => 'packaging_boxes#update_packaging_box'
      put 'packaging_boxes/state/:id' => 'packaging_boxes#change_state'
      get 'packaging_boxes/:id'    => 'packaging_boxes#get_packaging_box'
      delete 'packaging_boxes/:id' => 'packaging_boxes#delete_packaging_box'
      get 'packaging_boxes'        =>  'packaging_boxes#get_all_packaging_boxes'

    end
  end

end

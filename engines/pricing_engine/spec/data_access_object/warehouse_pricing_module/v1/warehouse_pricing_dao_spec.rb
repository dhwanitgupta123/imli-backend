#
# Module to handle all the functionalities related to warehouse pricing module
#
module WarehousePricingModule
  #
  # Version1 for warehouse Pricing module
  #
  module V1
    require 'rails_helper'
    RSpec.describe WarehousePricingModule::V1::WarehousePricingDao do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:warehouse_pricing_dao) { WarehousePricingModule::V1::WarehousePricingDao.new }
      let(:base_dao) { PricingBaseModule::V1::BaseDao }
      let(:warehouse_pricing) { FactoryGirl.build(:warehouse_pricing) }
      let(:args) {
        {
          mrp: Faker::Commerce.price.to_d,
          selling_price: Faker::Commerce.price.to_d,
          service_tax: Faker::Commerce.price.to_d,
          vat: Faker::Commerce.price.to_d,
          cst: Faker::Commerce.price.to_d
        }
      }

      describe 'new_warehouse_pricing' do
        it 'with valid params' do
          expect(warehouse_pricing_dao.new_warehouse_pricing(
            args)).to_not be_nil
        end
      end

      describe 'update_warehouse_pricing' do
        it 'with valid params' do
          expect(warehouse_pricing_dao.update_warehouse_pricing(
            args, warehouse_pricing)).to_not be_nil
        end

        it 'when ladder dao throws error' do
          base_dao.any_instance.should_receive(:update_model).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ warehouse_pricing_dao.update_warehouse_pricing(
            args, warehouse_pricing) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end   
      end
    end
  end
end

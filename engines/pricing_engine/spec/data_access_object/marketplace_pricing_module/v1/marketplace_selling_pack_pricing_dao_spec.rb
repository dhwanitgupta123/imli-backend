#
# Module to handle all the functionalities related to marketplace pricing module
#
module MarketplacePricingModule
  #
  # Version1 for MPSP Pricing module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplacePricingModule::V1::MarketplaceSellingPackPricingDao do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:mpsp_pricing_dao) { MarketplacePricingModule::V1::MarketplaceSellingPackPricingDao.new }
      let(:mpsp_ladder_pricing_dao) { MarketplacePricingModule::V1::MarketplaceSellingPackLadderPricingDao }
      let(:mpsp_ladder_pricing) { FactoryGirl.build(:marketplace_selling_pack_ladder_pricing) }
      let(:mpsp_pricing) { FactoryGirl.build(:marketplace_selling_pack_pricing) }
      let(:args) {
        {
          mrp: Faker::Commerce.price.to_d,
          base_selling_price: Faker::Commerce.price.to_d,
          savings: Faker::Commerce.price.to_d,
          service_tax: Faker::Commerce.price.to_d,
          vat: Faker::Commerce.price.to_d,
          discount: Faker::Commerce.price.to_d,
          taxes: Faker::Commerce.price.to_d,
          ladder: [
            {
              quantity: 1,
              selling_price: Faker::Commerce.price.to_d,
            },
            {
              quantity: 2,
              selling_price: Faker::Commerce.price.to_d,
            }
          ]
        }
      }
      let(:max_quantity) { 10 }
      let(:is_ladder_pricing_active) { true }

      describe 'new_marketplace_selling_pack_pricing' do
        it 'with valid params' do
          expect(mpsp_pricing_dao.new_marketplace_selling_pack_pricing(
            args, max_quantity, is_ladder_pricing_active)).to_not be_nil
        end
      end

      describe 'update_marketplace_selling_pack_pricing' do
        it 'with valid params' do
          mpsp_pricing.marketplace_selling_pack_ladder_pricings << mpsp_ladder_pricing
          expect(mpsp_pricing_dao.update_marketplace_selling_pack_pricing(
            args, mpsp_pricing, max_quantity, is_ladder_pricing_active)).to_not be_nil
        end

        it 'when ladder pricing not present' do
          expect(mpsp_pricing_dao.update_marketplace_selling_pack_pricing(
            args, mpsp_pricing, max_quantity, is_ladder_pricing_active)).to_not be_nil
        end

        it 'when ladder dao throws error' do
          mpsp_ladder_pricing_dao.any_instance.should_receive(:update_mpsp_ladder_pricing).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ mpsp_pricing_dao.update_marketplace_selling_pack_pricing(
            args, mpsp_pricing, max_quantity, is_ladder_pricing_active) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end   
      end

      describe 'get_mpsp_pricing_details' do
        it 'when ladder pricing is false' do
          mpsp_pricing.marketplace_selling_pack_ladder_pricings << mpsp_ladder_pricing
          pricing = mpsp_pricing_dao.get_mpsp_pricing_details(1, mpsp_pricing, false)
          expect(pricing[:mrp]).to eq(mpsp_pricing.mrp)
        end

        it 'when ladder is pricing is true' do
          mpsp_ladder_pricing_dao.any_instance.should_receive(:get_mpsp_ladder_pricing_details).and_return(mpsp_ladder_pricing)
          mpsp_pricing.marketplace_selling_pack_ladder_pricings << mpsp_ladder_pricing
          pricing = mpsp_pricing_dao.get_mpsp_pricing_details(1, mpsp_pricing, true)
          expect(pricing[:mrp]).to eq(mpsp_pricing.mrp)
        end
      end
    end
  end
end

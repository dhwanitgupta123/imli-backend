#
# Module to handle all the functionalities related to marketplace pricing module
#
module MarketplacePricingModule
  #
  # Version1 for MPSP Pricing module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplacePricingModule::V1::MarketplaceBrandPackPricingDao do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:mpbp_pricing_dao) { MarketplacePricingModule::V1::MarketplaceBrandPackPricingDao.new }
      let(:base_dao) { PricingBaseModule::V1::BaseDao }
      let(:mpbp_pricing) { FactoryGirl.build(:marketplace_brand_pack_pricing) }
      let(:args) {
        {
          spat: Faker::Commerce.price.to_d,
          margin: Faker::Commerce.price.to_d,
          service_tax: Faker::Commerce.price.to_d,
          vat: Faker::Commerce.price.to_d,
          discount: Faker::Commerce.price.to_d,
          buying_price: Faker::Commerce.price.to_d
        }
      }

      describe 'new_marketplace_brand_pack_pricing' do
        it 'with valid params' do
          expect(mpbp_pricing_dao.new_marketplace_brand_pack_pricing(
            args)).to_not be_nil
        end
      end

      describe 'update_marketplace_brand_pack_pricing' do
        it 'with valid params' do
          expect(mpbp_pricing_dao.update_marketplace_brand_pack_pricing(
            args, mpbp_pricing)).to_not be_nil
        end

        it 'when ladder dao throws error' do
          base_dao.any_instance.should_receive(:update_model).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ mpbp_pricing_dao.update_marketplace_brand_pack_pricing(
            args, mpbp_pricing) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end   
      end
    end
  end
end

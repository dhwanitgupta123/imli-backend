#
# Module to handle all the functionalities related to seller pricing chain
#
module SellerPricingModule
  #
  # Version1 for seller pricing module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SellerPricingModule::V1::SellerPricing, type: :model do

      it 'should be valid' do
      seller_pricing = FactoryGirl.build_stubbed(:seller_pricing)
        expect(seller_pricing).to be_valid
      end
    end
  end
end

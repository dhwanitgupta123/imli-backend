#
# Module to handle all the functionalities related to marketplace selling pack pricing chain
#
module SellerPricingModule
  #
  # Version1 for marketplace selling pack pricing module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MarketplacePricingModule::V1::MarketplaceSellingPackLadderPricing, type: :model do

      it 'should be valid' do
      marketplace_selling_pack_ladder_pricing = FactoryGirl.build_stubbed(:marketplace_selling_pack_ladder_pricing)
        expect(marketplace_selling_pack_ladder_pricing).to be_valid
      end

      it 'should be invalid' do
        marketplace_selling_pack_ladder_pricing = FactoryGirl.build_stubbed(:marketplace_selling_pack_ladder_pricing, marketplace_selling_pack_pricing: nil)
        expect(marketplace_selling_pack_ladder_pricing).to_not be_valid
      end
    end
  end
end

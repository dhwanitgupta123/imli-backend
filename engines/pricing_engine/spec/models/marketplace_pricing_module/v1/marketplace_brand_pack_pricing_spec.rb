#
# Module to handle all the functionalities related to marketplace brand pack pricing chain
#
module SellerPricingModule
  #
  # Version1 for marketplace brand pack pricing module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MarketplacePricingModule::V1::MarketplaceBrandPackPricing, type: :model do

      it 'should be valid' do
      marketplace_brand_pack_pricing = FactoryGirl.build_stubbed(:marketplace_brand_pack_pricing)
        expect(marketplace_brand_pack_pricing).to be_valid
      end
    end
  end
end

#
# Module to handle all the functionalities related to marketplace pricing
#
module MarketplacePricingModule
  #
  # Version1 for marketplace pricing module
  #
  module V1
    FactoryGirl.define do
      factory :marketplace_selling_pack_ladder_pricing, class: MarketplacePricingModule::V1::MarketplaceSellingPackLadderPricing do
      	association :marketplace_selling_pack_pricing, factory: :marketplace_selling_pack_pricing
        quantity 1
        selling_price Faker::Commerce.price.to_d
        additional_savings Faker::Commerce.price.to_d
      end
    end
  end
end

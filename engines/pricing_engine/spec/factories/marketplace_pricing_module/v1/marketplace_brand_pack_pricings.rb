#
# Module to handle all the functionalities related to marketplace pricing
#
module MarketplacePricingModule
  #
  # Version1 for marketplace pricing module
  #
  module V1
    FactoryGirl.define do
      factory :marketplace_brand_pack_pricing, class: MarketplacePricingModule::V1::MarketplaceBrandPackPricing do
        spat Faker::Commerce.price.to_d
        margin Faker::Commerce.price.to_d
        service_tax Faker::Commerce.price.to_d
        vat Faker::Commerce.price.to_d
        discount Faker::Commerce.price.to_d
        buying_price Faker::Commerce.price.to_d
      end
    end
  end
end

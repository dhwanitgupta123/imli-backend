#
# Module to handle all the functionalities related to warehouse pricing
#
module WarehousePricingModule
  #
  # Version1 for warehouse pricing module
  #
  module V1
    FactoryGirl.define do
      factory :warehouse_pricing, class: WarehousePricingModule::V1::WarehousePricing do
        mrp Faker::Commerce.price.to_d
        selling_price Faker::Commerce.price.to_d
        service_tax Faker::Commerce.price.to_d
        vat Faker::Commerce.price.to_d
        cst Faker::Commerce.price.to_d
      end
    end
  end
end

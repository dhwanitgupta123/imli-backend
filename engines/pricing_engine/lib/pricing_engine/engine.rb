module PricingEngine
  #
  # This adds the paths in $LOAD_PATH variable so that module inside these directory gets loaded in the environment
  # LocationEngine belongs to imli-backend-app uses many common functionalities of the application hence
  # added utils, helper, model and decorators of the app
  #
  class Engine < ::Rails::Engine
    config.autoload_paths += %W(
      #{config.root}/app/api
      #{config.root}/app/readers
      #{config.root}/app/data_access_object
      #{config.root}/app/services
      #{config.root}/../../app/services
      #{config.root}/../../app/utils
      #{config.root}/../../lib
      #{config.root}/../../app/helpers
      #{config.root}/../../app/models
      #{config.root}/../../app/decorators
      #{config.root}/../../app/controllers
      #{config.root}/../suplly_chain_engine/app/services
      #{config.root}/../suplly_chain_engine/app/utils
      #{config.root}/../suplly_chain_engine/lib
      #{config.root}/../suplly_chain_engine/app/helpers
      #{config.root}/../suplly_chain_engine/app/models
      #{config.root}/../suplly_chain_engine/app/decorators
      #{config.root}/../suplly_chain_engine/app/controllers
    )

    # RSpec & Factory Girl configuration
    config.generators do |g|
      g.test_framework :rspec, fixtures: false
      g.fixture_replacement :factory_girl, dir: 'spec/factories'
      g.assets false
      g.helper false
    end
  end
end

#
# Module to handle all the functionalities related to marketplace pricing
#
module MarketplacePricingModule
  #
  # Version1 for marketplace pricing module
  #
  module V1
    #
    # Map marketplace brand pack pricing object to Json
    #
    module MarketplaceBrandPackPricingMapper

      def self.map_marketplace_brand_pack_pricing_to_hash(mpbp_pricing)
        return {} if mpbp_pricing.blank?

        response = {
          spat: mpbp_pricing.spat,
          margin: mpbp_pricing.margin,
          service_tax: mpbp_pricing.service_tax,
          vat: mpbp_pricing.vat,
          discount: mpbp_pricing.discount,
          buying_price: mpbp_pricing.buying_price,
          selling_price: mpbp_pricing.selling_price,
          savings: mpbp_pricing.savings,
          mrp: mpbp_pricing.mrp
        }
        return response
      end
    end
  end
end

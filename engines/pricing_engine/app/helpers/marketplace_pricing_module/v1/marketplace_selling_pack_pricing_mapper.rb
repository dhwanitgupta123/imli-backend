#
# Module to handle all the functionalities related to marketplace pricing
#
module MarketplacePricingModule
  #
  # Version1 for marketplace pricing module
  #
  module V1
    #
    # Map marketplace selling pack pricing object to Json
    #
    module MarketplaceSellingPackPricingMapper

      LADDER = MarketplacePricingModule::V1::MarketplaceSellingPackLadderPricingMapper

      def self.map_marketplace_selling_pack_pricing_to_hash(mpsp_pricing, max_quantity, is_ladder_pricing_active)
        return {} if mpsp_pricing.blank?
        ladders  = []
        if mpsp_pricing.marketplace_selling_pack_ladder_pricings.present? && is_ladder_pricing_active
          ladders = LADDER.map_checkpoint_marketplace_selling_pack_ladder_pricing_to_hash(mpsp_pricing.marketplace_selling_pack_ladder_pricings, max_quantity)
        end
        response = {
          id: mpsp_pricing.id,
          mrp: mpsp_pricing.mrp,
          base_selling_price: mpsp_pricing.base_selling_price,
          savings: mpsp_pricing.savings,
          service_tax: mpsp_pricing.service_tax,
          vat: mpsp_pricing.vat,
          taxes: mpsp_pricing.taxes,
          discount: mpsp_pricing.discount,
          ladder: ladders
        }
        return response
      end
    end
  end
end

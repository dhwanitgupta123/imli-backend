#
# Module to handle all the functionalities related to pricing
#
module WarehousePricingModule
  #
  # Version1 for warehouse pricing module
  #
  module V1
    #
    # Map warehouse_pricing object to Json
    #
    module WarehousePricingMapper

    	def self.map_warehouse_pricing_to_hash(warehouse_pricing)
        return {} if warehouse_pricing.blank?

    		response = {
    			mrp: warehouse_pricing.mrp,
    			selling_price: warehouse_pricing.selling_price,
    			service_tax: warehouse_pricing.service_tax,
    			vat: warehouse_pricing.vat,
    			cst: warehouse_pricing.cst
    		}
    		return response
    	end
    end
  end
end

#
# Module to handle all the functionalities related to marketplace pricing
#
module MarketplacePricingModule
  #
  # Version1 for marketplace pricing module
  #
  module V1
    #
    # Service for MarketplaceSellingPackPricing table
    #
    class MarketplaceSellingPackPricingService

      MPSP_PRICING_DAO = MarketplacePricingModule::V1::MarketplaceSellingPackPricingDao
      # 
      # return new object of new MPSP pricing
      #
      def new_marketplace_selling_pack_pricing(args, marketplace_brand_pack_quantity_array, max_quantity, is_ladder_pricing_active=false)
        args = set_pricing_details(args, marketplace_brand_pack_quantity_array)
        @mpsp_pricing_dao = MPSP_PRICING_DAO.new
        mpsp_pricing = @mpsp_pricing_dao.new_marketplace_selling_pack_pricing(args, max_quantity, is_ladder_pricing_active)
        return mpsp_pricing
      end

      # 
      # update MPSP pricing details
      #
      def update_marketplace_selling_pack_pricing(args, mpsp_pricing, max_quantity, is_ladder_pricing_active=false)
        @mpsp_pricing_dao = MPSP_PRICING_DAO.new
        mpsp_pricing = @mpsp_pricing_dao.update_marketplace_selling_pack_pricing(args, mpsp_pricing, max_quantity, is_ladder_pricing_active)
        return mpsp_pricing
      end

      def set_pricing_details(args, marketplace_brand_pack_quantity_array)
        base_selling_price = 0
        mrp = 0
        marketplace_brand_pack_quantity_array.each do |mpbp_quantity|
          base_selling_price += mpbp_quantity[:marketplace_brand_pack].marketplace_brand_pack_pricing.selling_price.to_f * mpbp_quantity[:quantity].to_f
          mrp += mpbp_quantity[:marketplace_brand_pack].marketplace_brand_pack_pricing.mrp.to_f * mpbp_quantity[:quantity].to_f
        end
        args[:base_selling_price] =  args[:base_selling_price].blank? ? base_selling_price : BigDecimal(args[:base_selling_price])
        args[:mrp] = args[:mrp].blank? ? mrp : BigDecimal(args[:mrp])
        args[:savings] = args[:mrp] - args[:base_selling_price]
        return args
      end
    end
  end
end

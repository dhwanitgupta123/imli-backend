#
# Module to handle all the functionalities related to dao
#
module PricingBaseModule
  #
  # Version1 for base module
  #
  module V1
    class BaseDao
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      def initialize
      end

      #
      # Update the given model
      #
      # @param args [hash] arguments to update
      # @param model [Model] model
      #
      # @return [Model] updated model
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update_model(args, record, message = '')
        message = 'Model' if message.blank?
        begin
          record.update_attributes!(model_params(args, record))
          return record
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update ' + message + ', '+ e.message)
        end
      end

      #
      # this function only permit attributes required for the table
      #
      # @return [Hash] filterd params
      #
      def model_params(args, model)
        args = ActionController::Parameters.new(args)
        args.permit(model.attribute_names)
      end
    end
  end
end

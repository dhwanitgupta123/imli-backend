#
# Module to handle all the functionalities related to warehouse pricing
#
module WarehousePricingModule
  #
  # Version1 for warehouse pricing module
  #
  module V1
    #
    # DAO for WarehousePricing table
    #
    class WarehousePricingDao < PricingBaseModule::V1::BaseDao

      WAREHOUSE_PRICING_MODEL = WarehousePricingModule::V1::WarehousePricing

      # 
      # return new object of new_warehouse pricing
      #
      def new_warehouse_pricing(args)
        return WAREHOUSE_PRICING_MODEL.new(model_params(args, WAREHOUSE_PRICING_MODEL))
      end

      # 
      # update warehouse pricing details
      #
      def update_warehouse_pricing(args, warehouse_pricing)
        return update_model(args, warehouse_pricing, 'Warehouse pricing')
      end
    end
  end
end

#
# Module to handle all the functionalities related to marketplace pricing
#
module MarketplacePricingModule
  #
  # Version1 for marketplace pricing module
  #
  module V1
    #
    # DAO for MarketplaceSellingPackPricing table
    #
    class MarketplaceSellingPackPricingDao < PricingBaseModule::V1::BaseDao

      MPSP_PRICING_MODEL = MarketplacePricingModule::V1::MarketplaceSellingPackPricing
      MPSP_LADDER_PRICING_DAO = MarketplacePricingModule::V1::MarketplaceSellingPackLadderPricingDao
      # 
      # return new object of new MPSP pricing
      #
      def new_marketplace_selling_pack_pricing(args, max_quantity, is_ladder_pricing_active=false)
        mpsp_ladder_pricing_dao = MPSP_LADDER_PRICING_DAO.new
        mpsp_pricing = MPSP_PRICING_MODEL.new(model_params(args, MPSP_PRICING_MODEL))
        if is_ladder_pricing_active
          mpsp_pricing.marketplace_selling_pack_ladder_pricings = mpsp_ladder_pricing_dao.new_mpsp_ladder_pricing(args, max_quantity)
        end
        return mpsp_pricing
      end

      # 
      # update MPSP pricing details
      #
      def update_marketplace_selling_pack_pricing(args, mpsp_pricing, max_quantity, is_ladder_pricing_active=false)
        if args[:mrp].present? && args[:base_selling_price].present?
          args[:mrp] = BigDecimal(args[:mrp])
          args[:base_selling_price] = BigDecimal(args[:base_selling_price])
          args[:savings] = args[:mrp] - args[:base_selling_price]
        else
          args[:mrp] = mpsp_pricing.mrp
          args[:base_selling_price] = mpsp_pricing.base_selling_price
          args[:savings] = mpsp_pricing.savings
        end
        if args[:ladder].present? && is_ladder_pricing_active
          mpsp_ladder_pricing_dao = MPSP_LADDER_PRICING_DAO.new
          mpsp_pricing.marketplace_selling_pack_ladder_pricings = mpsp_ladder_pricing_dao.update_mpsp_ladder_pricing(args, mpsp_pricing.marketplace_selling_pack_ladder_pricings, max_quantity)
        end
        return update_model(args, mpsp_pricing, 'Marketplace selling pack pricing')
      end

      #
      # Get savings and total for product
      #
      # @param quantity [Integer] [quantity of Order product]
      # @param marketplace_selling_pack [Object] [MPSP corresponding to Order product]
      # @param is_ladder_pricing [Boolean] [is ladder pricing on]
      #
      # @return [JSON] [hash containing savings and selling_price]
      #
      def get_mpsp_pricing_details(quantity, mpsp_pricing, is_ladder_pricing)
        if quantity > 0
          if is_ladder_pricing
            mpsp_ladder_pricing_dao = MPSP_LADDER_PRICING_DAO.new
              mpsp_ladder_pricing = mpsp_ladder_pricing_dao.get_mpsp_ladder_pricing_details(quantity, mpsp_pricing.marketplace_selling_pack_ladder_pricings)
              selling_price = mpsp_ladder_pricing.selling_price
              additional_savings = mpsp_ladder_pricing.additional_savings
              # Adding additional savings to base savings
              savings = mpsp_pricing.savings + additional_savings
          else
            selling_price = mpsp_pricing.base_selling_price
            savings = mpsp_pricing.savings
          end
        else
          selling_price = BigDecimal('0.0')
          savings = BigDecimal('0.0')
        end
        mrp = mpsp_pricing.mrp
        vat = mpsp_pricing.vat
        service_tax = mpsp_pricing.service_tax
        taxes = mpsp_pricing.taxes
        discount = mpsp_pricing.discount
        return {
          savings: savings,
          selling_price: selling_price,
          mrp: mrp,
          vat: vat,
          service_tax: service_tax,
          taxes: taxes,
          discount: discount
        }
      end
    end
  end
end

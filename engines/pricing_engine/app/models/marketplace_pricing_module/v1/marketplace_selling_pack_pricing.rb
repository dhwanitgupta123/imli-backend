#
# Module to handle all the functionalities related to marketplace pricing
#
module MarketplacePricingModule
  #
  # Version1 for seller marketplace selling pack module
  #
  module V1
    #
    # Model for MarketplaceSellingPackPricing table
    #
    class MarketplaceSellingPackPricing < ActiveRecord::Base

      belongs_to :marketplace_selling_pack, class_name: 'MarketplaceProductModule::V1::MarketplaceSellingPack'

      has_many :marketplace_selling_pack_ladder_pricings, dependent: :destroy

    end
  end
end
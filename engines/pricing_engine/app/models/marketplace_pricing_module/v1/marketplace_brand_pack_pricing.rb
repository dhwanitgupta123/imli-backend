#
# Module to handle all the functionalities related to marketplace pricing
#
module MarketplacePricingModule
  #
  # Version1 for seller marketplace brand pack module
  #
  module V1
    #
    # Model for MarketplaceBrandPackPricing table
    #
    class MarketplaceBrandPackPricing < ActiveRecord::Base

      belongs_to :marketplace_brand_pack, class_name: 'MarketplaceProductModule::V1::MarketplaceBrandPack'
      
    end
  end
end
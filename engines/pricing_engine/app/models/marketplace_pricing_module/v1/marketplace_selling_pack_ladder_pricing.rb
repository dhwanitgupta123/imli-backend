#
# Module to handle all the functionalities related to marketplace pricing
#
module MarketplacePricingModule
  #
  # Version1 for seller marketplace selling pack module
  #
  module V1
    #
    # Model for MarketplaceSellingPackLadderPricing table
    #
    class MarketplaceSellingPackLadderPricing < ActiveRecord::Base

      belongs_to :marketplace_selling_pack_pricing
      validates :marketplace_selling_pack_pricing, presence: true
    end
  end
end
#
# Module to handle all the functionalities related to warehouse pricing
#
module WarehousePricingModule
  #
  # Version1 for warehouse pricing module
  #
  module V1
    #
    # Model for WarehousePricing table
    #
    class WarehousePricing < ActiveRecord::Base

      belongs_to :warehouse_brand_pack, class_name: 'WarehouseProductModule::V1::WarehouseBrandPack'
      has_many :warehouse_stocks, dependent: :destroy, class_name: 'WarehouseProductModule::V1::WarehouseStock'
    end
  end
end

#
# Module to handle all the functionalities related to seller pricing
#
module SellerPricingModule
  #
  # Version1 for seller pricing module
  #
  module V1
    #
    # Model for SellerPricing table
    #
    class SellerPricing < ActiveRecord::Base

      belongs_to :seller_brand_pack, class_name: 'SellerProductModule::V1::SellerBrandPack'

    end
  end
end

module OrderEngine
  class Engine < ::Rails::Engine
    config.generators do |g|
      g.test_framework      :rspec,        :fixture => false
      g.fixture_replacement :factory_girl, :dir => 'spec/factories'
      g.assets false
      g.helper false
    end

    # Auto load files in app/services
    config.autoload_paths += %W(
      #{config.root}/app/api
      #{config.root}/app/readers
      #{config.root}/app/data_access_object
      #{config.root}/app/services
      #{config.root}/app/decorators
      #{config.root}/app/helpers
      #{config.root}/app/models
      #{config.root}/app/controllers
      #{config.root}/../../app/workers
      #{config.root}/../../app/services
      #{config.root}/../../app/utils
      #{config.root}/../../lib
      #{config.root}/../../app/helpers
      #{config.root}/../../app/models
      #{config.root}/../../app/decorators
      #{config.root}/../../app/controllers
      #{config.root}/../pricing_engine/app/services
      #{config.root}/../pricing_engine/app/utils
      #{config.root}/../pricing_engine/lib
      #{config.root}/../pricing_engine/app/helpers
      #{config.root}/../pricing_engine/app/models
      #{config.root}/../pricing_engine/app/data_access_object
      #{config.root}/../location_engine/app/services
      #{config.root}/../location_engine/app/utils
      #{config.root}/../location_engine/lib
      #{config.root}/../location_engine/app/helpers
      #{config.root}/../location_engine/app/models
      #{config.root}/../location_engine/app/data_access_object
      #{config.root}/../location_engine/app/decorators
      #{config.root}/../supply_chain_engine/app/services
      #{config.root}/../supply_chain_engine/app/utils
      #{config.root}/../supply_chain_engine/lib
      #{config.root}/../supply_chain_engine/app/helpers
      #{config.root}/../supply_chain_engine/app/models
      #{config.root}/../supply_chain_engine/app/workers
      #{config.root}/../supply_chain_engine/app/data_access_object
    )
  end
end
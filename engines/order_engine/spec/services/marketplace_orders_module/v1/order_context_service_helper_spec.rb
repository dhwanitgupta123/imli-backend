#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceOrdersModule::V1::OrderContextService do
      let(:version) { 1 }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:order_context_service) { MarketplaceOrdersModule::V1::OrderContextService.new(version) }
      let(:order_context_dao) { MarketplaceOrdersModule::V1::OrderContextDao }

      let(:order) { FactoryGirl.build_stubbed(:order, user: user, address: address) }
      let(:order_context) { FactoryGirl.build(:order_context) }

      let(:order_id) { Faker::Number.number(1) }


      context 'record order context' do
        it 'creates the order context' do
          expect_any_instance_of(order_context_dao).to receive(:get_context_by_order_id).and_raise(custom_error_util::InvalidArgumentsError)
          expect_any_instance_of(order_context_dao).to receive(:create_order_context).and_return(order_context)
          data = order_context_service.record_order_context({order_id: order_id, order_user_tag: Faker::Number.number(1)})
          expect(data.order_id).to eq(order_context.order_id)
        end

        it 'update the order context' do
          expect_any_instance_of(order_context_dao).to receive(:get_context_by_order_id).and_return(order_context)
          expect_any_instance_of(order_context_dao).to receive(:update_order_context).and_return(order_context)
          data = order_context_service.record_order_context({order_id: order_id})
          expect(data.order_id).to eq(order_context.order_id)
        end
      end
    end
  end
end

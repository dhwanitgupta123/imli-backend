#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceOrdersModule::V1::OrderPanelService do
      let(:version) { 1 }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:order_dao) { MarketplaceOrdersModule::V1::OrderDao }
      let(:order_context_dao) { MarketplaceOrdersModule::V1::OrderContextDao }
      let(:order_panel_service) { MarketplaceOrdersModule::V1::OrderPanelService.new(version) }
      let(:order_product_service) { MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService.new(version) }
      let(:order_email_service) { CommunicationsModule::V1::Mailers::V1::OrderEmailWorker }

      let(:message) { FactoryGirl.build_stubbed(:message) }
      let(:provider) { FactoryGirl.build_stubbed(:provider, message: message) }
      let(:user) { FactoryGirl.build_stubbed(:user, provider: provider) }

      let(:area) { FactoryGirl.build_stubbed(:area) }
      let(:address) { FactoryGirl.build_stubbed(:address, area: area, user: user) }

      let(:order) { FactoryGirl.build_stubbed(:order, user: user, address: address) }
      let(:order_context) { FactoryGirl.build(:order_context) }
      let(:default_hour) { 6 }
      let(:default_day_difference) { 1 }
      let(:order_id) { Faker::Number.number(1) }

      context 'get order by id' do
        it 'with existing order' do
          expect_any_instance_of(order_dao).to receive(:get_order_by_order_id).and_return(order)
          response = order_panel_service.get_order_by_order_id(order.order_id)
          expect(response.order_id).to equal(order.order_id)
        end

        it 'with non existing order' do
          expect_any_instance_of(order_dao).to receive(:get_order_by_order_id).and_raise(custom_error_util::ResourceNotFoundError)
          expect{ order_panel_service.get_order_by_order_id(nil) }.to raise_error(custom_error_util::ResourceNotFoundError)
        end
      end

      context 'get all orders' do
        request = { sort_by: 'id' }
        it 'with wrong sort_by' do
          order_dao.any_instance.stub(:get_all).and_raise(custom_error_util::InvalidArgumentsError)
          expect{ order_panel_service.get_all_orders(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with wrong order' do
          order_dao.any_instance.stub(:get_all).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ order_panel_service.get_all_orders(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          order_dao.any_instance.stub(:get_all).and_return(order)
          expect(order_panel_service.get_all_orders(request)).to_not be_nil
        end
      end

      context 'get aggregated products' do

        it 'successfully without passing params' do
          expect(order_panel_service.get_aggregated_products_in_interval).to_not be_nil
        end

        it 'successfully with from time more than till time in params defaults to 6am to 6pm' do
          till_time = Time.zone.now.to_i
          from_time = till_time + 1
          response = order_panel_service.initialize_and_validate_time({ to_time: till_time, from_time: from_time})
          day_difference = ((response[:till_time].to_date) - (response[:from_time].to_date)).to_i
          expect(response[:from_time].hour).to eq(default_hour)
          expect(response[:till_time].hour).to eq(default_hour)
          expect(day_difference).to eq(default_day_difference)
          expect(order_panel_service.get_aggregated_products_in_interval).to_not be_nil
        end

        it 'successfully with params nil defaulting to 6am to 6pm' do
          response = order_panel_service.initialize_and_validate_time({ to_time: nil, from_time: nil})
          day_difference = ((response[:till_time].to_date) - (response[:from_time].to_date)).to_i
          expect(response[:from_time].hour).to eq(default_hour)
          expect(response[:till_time].hour).to eq(default_hour)
          expect(day_difference).to eq(default_day_difference)
          expect(order_panel_service.get_aggregated_products_in_interval).to_not be_nil
        end

      end
    end
  end
end

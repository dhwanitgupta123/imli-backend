#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    require 'rails_helper'

    RSpec.describe OrdersPanelController, type: :routing do

      # specifying engine routes
      routes { OrderEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #get_aggregated_products' do
          expect(:get => 'marketplace/orders/aggregated').to route_to(
            {
              'format' =>'json',
              'controller' => 'marketplace_orders_module/v1/orders_panel',
              'action' => 'get_aggregated_products'
            })
        end

        it 'routes to #get_order_by_order_id' do
          expect(:get => 'marketplace/orders/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'marketplace_orders_module/v1/orders_panel',
              'action'=>'get_order_by_order_id',
              'order_id'=>'1'
            })
        end

        it 'routes to #get_all_orders' do
          expect(:get => 'marketplace/orders').to route_to(
            {
              'format' => 'json',
              'controller' => 'marketplace_orders_module/v1/orders_panel',
              'action' => 'get_all_orders'
            })
        end

        it 'routes to #update_order_context' do
          expect(:put => 'orders/1/order_context').to route_to(
          {
            'format' => 'json',
            'controller' => 'marketplace_orders_module/v1/orders_panel',
            'action' => 'update_order_context',
            'id'=>'1'
          })
        end
      end
    end
  end
end
#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceOrdersModule::V1::OrdersPanelController, type: :controller do
      # specifying engine routes
      routes { OrderEngine::Engine.routes }

      # This should return the minimal set of values that should be in the session
      # in order to pass any filters (e.g. authentication) defined in
      # UsersController. Be sure to keep this updated too.
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      
      before(:each) do
        expect_any_instance_of(access_control_controller).to receive(:before_actions).and_return(true)
        expect_any_instance_of(access_control_controller).to receive(:after_actions).and_return(true)
      end
      let(:get_all_orders_api) { MarketplaceOrdersModule::V1::GetAllOrdersApi }
      let(:get_order_by_order_id_api) { MarketplaceOrdersModule::V1::GetOrderByOrderIdApi }
      let(:get_aggregated_products_api) { MarketplaceOrdersModule::V1::GetAggregatedProductsApi }
      let(:order_response_decorator) { MarketplaceOrdersModule::V1::OrdersResponseDecorator }
      let(:update_order_context_api) { MarketplaceOrdersModule::V1::UpdateOrderContextApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:order) { FactoryGirl.build_stubbed(:order, state_attributes: "{\"2\":\"{\\\"time\\\":\\\"1448060811\\\"}\"\}" ) }

      let(:order_params) { 
            {
              order: {
                    id: Faker::Number.number(1),
                    order_id: Faker::Lorem.characters(10),
                    ship_by: Faker::Date.forward(1),
                    status: Faker::Number.number(1)
                }
            }
        }

        let(:stub_ok_response) {
          {
            payload: {
              order:
                {
                id: Faker::Number.number(1),
                order_id: Faker::Lorem.characters(10),
                ship_by: Faker::Date.forward(1),
                status: Faker::Number.number(1)
              }
            },
            response: response_codes_util::SUCCESS
          }
        }

      describe 'get_order_by_order_id' do

        it 'returns order successfully' do
          expect_any_instance_of(get_order_by_order_id_api).to receive(:enact)
                .and_return(stub_ok_response)
          get :get_order_by_order_id, order_id: order_params[:order][:order_id]
          expect(response).to have_http_status(status_codes_util::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad response when nil order_id passed' do
          stub_bad_request_response = {
            'error': { message: content_util::INVALID_DATA_PASSED },
            'response': response_codes_util::BAD_REQUEST
          }
          stub_bad_response = order_response_decorator.create_response_bad_request
          expect_any_instance_of(get_order_by_order_id_api).to receive(:enact)
                .and_return(stub_bad_response)

          get :get_order_by_order_id, order_id: order_params[:order][:order_id]
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'return bad response when wrong order_id passed' do
          stub_bad_request_response = {
            'response': response_codes_util::BAD_REQUEST
          }
          stub_bad_response = order_response_decorator.create_response_bad_request
          expect_any_instance_of(get_order_by_order_id_api).to receive(:enact)
                .and_return(stub_bad_response)

          get :get_order_by_order_id, order_id: order_params[:order][:order_id]
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end

      describe 'get_all_orders' do
        it 'returns all orders successfully' do
          expect_any_instance_of(get_all_orders_api).to receive(:enact)
                .and_return(stub_ok_response)
          get :get_all_orders
          expect(response).to have_http_status(status_codes_util::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad response when wrong params passed' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          stub_bad_response = order_response_decorator.
            create_response_bad_request
          expect_any_instance_of(get_all_orders_api).to receive(:enact)
                .and_return(stub_bad_response)
          get :get_all_orders
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
          expect(JSON.parse(response.body)['error'].to_json).to eq(stub_bad_request_response[:error].to_json)
        end

      end

      describe 'get_aggregated_products' do

        let(:aggregated_products_params) {
          {
            aggregated_products: [
                {
                serial_number: Faker::Number.number(1),
                units_to_procure: Faker::Number.number(1),
                display_name: Faker::Lorem.characters(10),
                sku_size: Faker::Lorem.characters(5),
                unit: Faker::Lorem.characters(2)

              }
            ]
          }
        }
        it 'returns all aggregated products successfully' do
          stub_ok_response = order_response_decorator.
            create_aggregated_products_response(aggregated_products_params)
          expect_any_instance_of(get_aggregated_products_api).to receive(:enact)
                .and_return(stub_ok_response)
          get :get_aggregated_products
          expect(response).to have_http_status(status_codes_util::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end

      describe 'update_order_context' do
        it 'update order context' do
          stub_ok_response = order_response_decorator.create_success_response

          expect_any_instance_of(update_order_context_api).to receive(:enact)
                .and_return(stub_ok_response)
          put :update_order_context, id: Faker::Number.number(1), order_context: { order_user_tag: nil }

          expect(response).to have_http_status(status_codes_util::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end
    end
  end
end

module MarketplaceOrdersModule
  module V1
    FactoryGirl.define do
      factory :order, :class => MarketplaceOrdersModule::V1::Order do
        association :user, factory: :user
        association :address, factory: :address
        order_id Faker::Lorem.characters(10)
        ship_by Faker::Time.backward(1)
        status Faker::Number.number(1)
      end
    end
  end
end
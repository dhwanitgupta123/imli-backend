module MarketplaceOrdersModule
  module V1
    FactoryGirl.define do
      factory :payment, :class => MarketplaceOrdersModule::V1::Payment do
        association :order, factory: :order
        mode  Faker::Number.number(1)
        grand_total Faker::Commerce.price
        total_vat_tax Faker::Commerce.price
        total_service_tax Faker::Commerce.price
        payment_mode_savings Faker::Commerce.price
        discount Faker::Commerce.price
        gift_card Faker::Commerce.price
        total_savings Faker::Commerce.price
        net_total Faker::Commerce.price
        status Faker::Number.number(1)
      end
    end
  end
end

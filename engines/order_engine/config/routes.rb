OrderEngine::Engine.routes.draw do
  # Predicate for versioning based on app version
  match_app_version_predicate = Proc.new do |version, request|
    CommonModule::V1::RoutingPredicates.match_app_version(version, request)
  end

  # Route for MarketplaceOrdersModule with versioning and constraint matching
  scope module: :marketplace_orders_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      get  'marketplace/orders/aggregated' => 'orders_panel#get_aggregated_products'
      get  'marketplace/orders/stats' => 'order_data#get_orders_stats'

      get  'marketplace/orders'   => 'orders_panel#get_all_orders'
      get  'marketplace/orders/:order_id' => 'orders_panel#get_order_by_order_id'
      put  'orders/state/:id'     =>     'orders_panel#change_state'
      get  'marketplace/orders/packing_details/:order_id' => 'orders_panel#get_packing_details_by_order_id'
      get 'marketplace/orders/invoice/:order_id' => 'orders_panel#get_invoice_by_order_id'

      post 'cart/update'          =>    'carts#update_cart'
      get  'cart'                 =>    'carts#get_cart'
      post 'order/place'          =>    'orders#place_order'
      post 'payment/initiate'     =>    'payments#initiate_payment'
      get  'payment/success/:id'  =>    'payments#payment_successful'

      # Order APIs
      get '/orders/previous'      =>    'orders#get_previous_orders'
      get '/orders/active'        =>    'orders#get_active_orders'
      get '/orders/:id'           =>    'orders#get_order_details'
      put '/orders/:id/schedule'  =>    'orders#schedule_order'

      # Panel APIs
      put '/orders/:id/time_slot' =>    'orders_panel#edit_order_time_slot'
      put '/orders/:id/address'   =>    'orders_panel#edit_order_address'
      # Design discussion for order edit is at : https://ample.quip.com/YmOzA3hs957X
      put '/orders/:id/cart'      =>    'orders_panel#edit_order_cart'
      put 'orders/:id/order_context' => 'orders_panel#update_order_context'

      # TimeSlot APIs
      get '/time_slots'           =>    'time_slots#get_all_time_slots'
      get '/time_slots/:id'       =>    'time_slots#get_time_slot'
      post '/time_slots/new'      =>    'time_slots#add_time_slot'
      put '/time_slots/:id'       =>    'time_slots#update_time_slot'
      put '/time_slots/state/:id' =>    'time_slots#change_state'
      delete '/time_slots/:id'    =>    'time_slots#delete_time_slot'
    end
  end

  # Route for WarehouseOrdersModule with versioning and constraint matching
  scope module: :warehouse_orders_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      get  'warehouse/orders' => 'warehouse_orders#get_all_orders'
      get  'warehouse/orders/:id' => 'warehouse_orders#get_order_by_order_id'
    end
  end

  # Route for InventoryOrdersModule with versioning and constraint matching
  scope module: :inventory_orders_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      post  'inventory/orders/new' => 'inventory_orders#create_purchase_order'
      put   'inventory/orders/:id' => 'inventory_orders#update_purchase_order'
      post  'inventory/orders/place' => 'inventory_orders#place_purchase_order'
      get   'inventory/orders/:id' => 'inventory_orders#get_purchase_order'
      put   'inventory/orders/state/:id' => 'inventory_orders#change_purchase_order_state'
      put   'inventory/orders/verify/:id' => 'inventory_orders#verify_purchase_order'
      get   'inventory/orders/warehouse/:warehouse_id' => 'inventory_orders#get_all_purchase_order'
      put   'inventory/orders/stock/:id' => 'inventory_orders#stock_purchase_order'
    end
  end
end

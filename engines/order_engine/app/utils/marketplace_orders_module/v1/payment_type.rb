module MarketplaceOrdersModule
  module V1
    # Payment Instances are linked with payment details.
    # If you are adding any key here, Don't forget to modify its corresponding
    # yml file (config/payment_details.yml), and vice versa
    #
    class PaymentType
      #
      # Cluster_Levels array contain hash each containing
      # level and display_level
      # level -> Integer which represents hierarchy of cluster and saved in cluster_contexts table
      # display_level -> corresponding display level used in Panel
      # 
      # 
      PAYMENT_TYPE = [
        {
          id: 1,
          key: 'PAY',
          label: 'Pay'
        },
        {
          id: 2,
          key: 'REFUND',
          label: 'Refund'
        }
      ]

      def self.get_display_label_by_id(id)
        payment_type = PAYMENT_TYPE.select {|payment_type| payment_type[:id] == id.to_i}
        return payment_type[0][:label] if payment_type.present?
        return 'UNKNOWN'
      end

      def self.get_id_by_key(key)
        payment_type = PAYMENT_TYPE.select {|payment_type| payment_type[:key] == key.to_s}
        return payment_type[0][:id] if payment_type.present?
        return nil
      end

    end
  end
end

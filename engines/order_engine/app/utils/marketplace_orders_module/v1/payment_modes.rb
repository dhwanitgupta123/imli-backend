module MarketplaceOrdersModule
  module V1
    # Payment Modes are linked with payment details.
    # If you are adding any key here, Don't forget to modify its corresponding
    # yml file (config/payment_details.yml), and vice versa
    #
    class PaymentModes
      UNKNOWN = 1
      DEBIT_CARD = 2
      CREDIT_CARD = 3
      CARD_ON_DELIVERY = 4
      CASH_ON_DELIVERY = 5
      LINK = 6
      NET_BANKING = 7

      ALLOWED_MODES = [UNKNOWN, DEBIT_CARD, CREDIT_CARD, NET_BANKING, CARD_ON_DELIVERY, CASH_ON_DELIVERY, LINK]


      #
      # Fetch all offline mode of payment options
      #
      def self.get_offline_payment_modes
        return [CARD_ON_DELIVERY, CASH_ON_DELIVERY, LINK]
      end

      #
      # FEtch all offline modes of payment modes with gateways
      #
      def self.get_offline_modes_with_gateways
        return [
          {
            id: CARD_ON_DELIVERY,
            label: 'Card On Delivery',
            gateways: [
              {
                id: TransactionsModule::V1::GatewayType::M_SWIPE,
                label: 'M_Swipe'
              }
            ]
          },
          {
            id: CASH_ON_DELIVERY,
            label: 'Cash On Delivery',
            gateways: [
              {
                id: TransactionsModule::V1::GatewayType::CASH,
                label: 'Cash'
              }
            ]
          },
          {
            id: LINK,
            label: 'Link',
            gateways: [
              {
                id: TransactionsModule::V1::GatewayType::CITRUS_WEB,
                label: 'Citrus'
              },
              {
                id: TransactionsModule::V1::GatewayType::RAZORPAY,
                label: 'Razorpay'
              }
            ]
          }
        ]
      end

      #
      # Get display name for payment mode
      #
      # @param mode [Integer] [Mode of payment]
      #
      # @return [String] [Payment mode display name]
      #
      def self.get_payment_mode_display_name(mode)
        display_name = ''
        return display_name if mode.blank?

        case mode.to_i
        when DEBIT_CARD, CREDIT_CARD, NET_BANKING
          display_name = 'ONLINE'
        when CARD_ON_DELIVERY
          display_name = 'CARD ON DELIVERY'
        when CASH_ON_DELIVERY
          display_name = 'OFFLINE CASH'
        when LINK
          display_name = 'LINK'
        when UNKNOWN
          display_name = 'ONLINE' # It should be unknown but for clarity we are keeping it ONLINE
        end
        return display_name
      end

    end
  end
end

module MarketplaceOrdersModule
  module V1
    # Payment Instances are linked with payment details.
    # If you are adding any key here, Don't forget to modify its corresponding
    # yml file (config/payment_details.yml), and vice versa
    #
    class PaymentInstances

      #
      # Cluster_Levels array contain hash each containing
      # level and display_level
      # level -> Integer which represents hierarchy of cluster and saved in cluster_contexts table
      # display_level -> corresponding display level used in Panel
      #
      #
      PAYMENT_INSTANCES = [
        {
          id: 1,
          instance: 'PRE_ORDER',
          label: 'Pre Order Payment'
        },
        {
          id: 2,
          instance: 'POST_ORDER',
          label: 'Post Order Payment'
        }
      ]

      def self.get_display_instance_by_id(id)
        payment_instance = PAYMENT_INSTANCES.select {|payment_instance| payment_instance[:id] == id.to_i}
        return payment_instance[0][:label] if payment_instance.present?
        return 'UNKNOWN'
      end

      def self.get_id_by_instance(instance)
        payment_instance = PAYMENT_INSTANCES.select {|payment_instance| payment_instance[:instance] == instance.to_s}
        return payment_instance[0][:id] if payment_instance.present?
        return nil
      end

      #
      # This function return payment instance by payment mode
      # For example if payment_mode == 1 the instance = PRE_ORDER
      # else instance = POST_ORDER
      #
      #
      # FOR NOW WE ARE CONSIDERING TWO MODES ONLY AS APP ONLY PROVIDE 2 OPTIONS
      #
      def self.get_instance_by_mode(mode)
        case mode.to_i
        when MarketplaceOrdersModule::V1::PaymentModes::UNKNOWN
          return MarketplaceOrdersModule::V1::PaymentInstances.get_id_by_instance('PRE_ORDER')
        when MarketplaceOrdersModule::V1::PaymentModes::CARD_ON_DELIVERY
          return MarketplaceOrdersModule::V1::PaymentInstances.get_id_by_instance('POST_ORDER')
        end
      end

      #
      # Get display name for payment instance
      #
      # @param mode [Integer] [Payment Instance]
      #
      # @return [String] [Payment Instance display name]
      #
      def self.get_payment_instance_display_name(payment_instance)
        display_name = '' 
        case payment_instance.to_i
        when get_id_by_instance('PRE_ORDER')
          display_name = 'ONLINE'
        when get_id_by_instance('POST_ORDER')
          display_name = 'ON DELIVERY'
        end
        return display_name
      end
    end
  end
end

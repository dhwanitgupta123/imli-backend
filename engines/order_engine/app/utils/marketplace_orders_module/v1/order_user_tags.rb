module MarketplaceOrdersModule
  module V1
    class OrderUserTags
      #
      # ORDER_USER_TAGS array contain hash each containing
      # order_user_tag and label
      # order_user_tag -> extra indicator for order like order is placed for business use or for personal use
      # label -> corresponding display level used in Panel
      # 
      # 
      ORDER_USER_TAGS = [
        {
          id: 1,
          order_user_tag: 'CUSTOMER',
          label: 'Customer'
        },
        {
          id: 2,
          order_user_tag: 'RETAILER',
          label: 'Retailer'
        },
        {
          id: 3,
          order_user_tag: 'OFFICE',
          label: 'Office'
        }
      ]

      def self.get_order_user_tag_label_by_id(id)
        order_user_tag = ORDER_USER_TAGS.select {|order_user_tag| order_user_tag[:id] == id.to_i}
        return order_user_tag[0][:label] if order_user_tag.present?
        return ''
      end

      def self.get_id_by_order_user_tag(tag)
        order_user_tag = ORDER_USER_TAGS.select {|order_user_tag| order_user_tag[:order_user_tag] == tag.to_s}
        return order_user_tag[0][:id] if order_user_tag.present?
        return nil
      end
    end
  end
end

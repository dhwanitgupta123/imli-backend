module MarketplaceOrdersModule

  module V1

    module InvoiceHelper

      ADDRESS_DECORATOR = AddressModule::V1::AddressResponseDecorator
      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper
      ORDER_HELPER = MarketplaceOrdersModule::V1::OrderHelper
      CART_HELPER = MarketplaceOrdersModule::V1::CartHelper
      CART_DAO = MarketplaceOrdersModule::V1::CartDao
      CACHE_UTIL = CommonModule::V1::Cache
      PAYMENT_HELPER = MarketplaceOrdersModule::V1::PaymentHelper
      DATE_TIME_UTIL = CommonModule::V1::DateTimeUtil

      #
      # Get invoice details for invoice generation
      #
      def self.get_invoice_hash_for_invoice_generation(order)
        return {} if order.blank?
        user = order.user
        user_hash = USER_SERVICE_HELPER.get_user_details(order.user)
        order_hash = ORDER_HELPER.get_order_details(order)
        cart_hash = CART_HELPER.get_cart_complete_details(order.cart)
        products_hash = update_order_product_specification(order)
        cart_hash = cart_hash.merge({total_products: products_hash[:total_quantity]})
        address_hash = ADDRESS_DECORATOR.create_address_hash(order.address)
        primary_email = USER_SERVICE_HELPER.get_primary_mail(order.user)

        # Prints in format: Thursday, 20 November 2015
        order_date = DATE_TIME_UTIL.convert_epoch_to_time(order_hash[:order_date]).strftime("%A, %d %B %Y")

        payment = PAYMENT_HELPER.get_orders_payment_as_per_its_mode(order)
        payment_labels_hash = PAYMENT_HELPER.get_payment_display_labels_for_invoice(payment)

        invoice_hash = {
          id: order.id,
          order_id: order.order_id,
          order_status: order.status,
          first_name: user.first_name,
          cart: cart_hash,
          products: products_hash[:order_products],
          order: order_hash,
          payment: order_hash[:payment],
          address: address_hash,
          user: user_hash,
          email_id: primary_email.email_id,
          order_date: order_date,
          payment_details: payment_labels_hash
        }
        return invoice_hash
      end

      #
      # Update Order Product specification.
      # Iterate over order products and compute its effective pricing based
      # on its quantity
      #
      def self.update_order_product_specification(order)
        cart_dao = CART_DAO.new
        order_products_array = []
        total_quantity = 0
        cart = order.cart
        return [] if cart.blank? || cart.order_products.blank?
        # Get active products from cart
        order_products = cart_dao.active_products_in_cart(cart)
        # Iterate over Order products and fetch all displayable entities
        order_products.each do |order_product|
          order_product_hash = compute_order_product_pricing_details(order_product)
          total_quantity += order_product.quantity
          order_products_array.push(order_product_hash)
        end
        return { order_products: order_products_array, total_quantity: total_quantity }
      end

      #
      # Compute single order product pricing detail (based on quantity and ladder pricing)
      #
      def self.compute_order_product_pricing_details(order_product)
        cart_dao = CART_DAO.new
        quantity = order_product.quantity
        mpsp = order_product.marketplace_selling_pack
        order_product_details = cart_dao.get_savings_and_total_details_for_mpsp(order_product)
        # Compute savings including additional discount
        savings = order_product_details[:total_savings] || BigDecimal.new('0.0')
        additional_discount = order_product_details[:additional_discount] || BigDecimal.new('0.0')
        final_savings = savings + additional_discount
        return {
          id: mpsp[:id],
          quantity: quantity,
          display_name: mpsp[:display_name],
          display_pack_size: mpsp[:display_pack_size],
          total_mrp: order_product_details[:total_mrp],
          total_savings: savings,
          total_selling_price: order_product_details[:total_selling_price],
          additional_discount: additional_discount,
          final_savings: final_savings,
          final_price: order_product_details[:final_price]
        }
      end

    end
  end
end

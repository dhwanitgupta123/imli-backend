module MarketplaceOrdersModule

  module V1

    module OrderStockingHelper

      ORDER_HELPER = MarketplaceOrdersModule::V1::OrderHelper
      ORDER_MAPPER = MarketplaceOrdersModule::V1::OrderMapper

      WBP_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      CART_SERVICE = MarketplaceOrdersModule::V1::CartService

      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content


      #
      # Function to mark outward of the products being packer for MPSP order
      #
      def self.outward_warehouse_brand_packs(cart)
        if ORDER_HELPER.order_packed?(cart.order)
          warehouse_brand_pack_service = WBP_SERVICE.new(@params)
          cart_service = CART_SERVICE.new(@params)
          active_order_products = cart_service.get_active_order_products_in_cart(cart)
          brand_packs_by_quantity = ORDER_MAPPER.map_products_to_array(active_order_products)
          warehouse_brand_pack_service.out_warehouse_brand_packs(brand_packs_by_quantity)
        end
      end

      #
      # Function to mark inward of the products when the order is edited (product deleted) or forcefully cancelled by the
      # warehouse user after it's being packed
      #
      def self.inward_warehouse_brand_packs(cart)
        if ORDER_HELPER.order_packed?(cart.order)
          warehouse_brand_pack_service = WBP_SERVICE.new(@params)
          cart_service = CART_SERVICE.new(@params)
          active_order_products = cart_service.get_active_order_products_in_cart(cart)
          brand_packs_by_quantity = ORDER_MAPPER.map_products_to_array(active_order_products)
          warehouse_brand_pack_service.inward_cancelled_warehouse_brand_packs(brand_packs_by_quantity)
        end
      end

      #
      # Update warehouse stocking based on previous and new cart
      # If order is packed, then all the products of previous cart will be INwarded
      # and all products of new cart will be OUTwarded
      # Note: This will be executed only when Order linked with these carts are PACKED or more
      #
      # @param previous_cart [Object] [previous cart which is discarded]
      # @param new_cart [Object] [updated cart]
      #
      # @raise [InvalidArgumentsError] [If previous and new cart does not belongs to same order]
      #
      def self.update_stock_based_on_previous_cart(previous_cart, new_cart)
        if previous_cart.order != new_cart.order
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new("Order mismatch for carts")
        end
        inward_warehouse_brand_packs(previous_cart)
        outward_warehouse_brand_packs(new_cart)
      end

    end
  end
end

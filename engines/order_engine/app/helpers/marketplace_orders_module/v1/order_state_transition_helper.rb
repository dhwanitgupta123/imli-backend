module MarketplaceOrdersModule

  module V1

    module OrderStateTransitionHelper

      PAYMENT_HELPER = MarketplaceOrdersModule::V1::PaymentHelper
      PAYMENT_DAO = MarketplaceOrdersModule::V1::PaymentDao
      ORDER_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates

      # 
      # Shrink/Minify order state to best nearest order state
      # Forex: if order is in at_warehouse state, then nearest lower state is
      # Placed, and hence new state which is to be sent to the user (mobile app)
      # will be Placed.
      #
      # @param order [Object] [Order model object]
      #
      # @return [JSON] [Hash containing order parameters with new status]
      #
      # def self.shrink_order_state(order)
      #   return {} if order.blank?
      #   current_status = order.status
      #   minified_states = ORDER_STATES::MINIFIED_STATES
      #   # Iterate over minified states array and find the range
      #   # between which current state exists. Then floor of that range
      #   # will be the new status.
      #   # Ex. minified_states: [1,2,7,9] & current_status: 5, then new_status => 2
      #   index = 0
      #   until current_status.to_i < minified_states[index][:value].to_i do
      #     index += 1
      #   end
      #   if index > 0
      #     new_status = minified_states[index - 1]
      #   else
      #     new_status = ORDER_STATES.get_order_state_by_value(0)
      #   end
      #   payment_dao = PAYMENT_DAO.new
      #   order_payment = payment_dao.get_orders_payment_as_per_its_mode(order)
      #   payment_hash = PAYMENT_HELPER.get_payment_hash(order_payment)
      #   return {
      #     id: order.id,
      #     order_id: order.order_id,
      #     status: new_status,
      #     payment: payment_hash
      #   }
      # end

      # #
      # # Shrink states of all the orders to its nearest lower state.
      # #
      # # @param orders [List] [list of all Order model states]
      # #
      # # @return [Array] [array of all order hashes containing order hash with minified state]
      # #
      # def self.shrink_states_of_multiple_orders(orders)
      #   if orders.blank?
      #     return []
      #   end
      #   orders_array = []
      #   orders.each do |order|
      #     orders_array.push(shrink_order_state(order) )
      #   end
      #   return orders_array
      # end

      # 
      # Shrink/Minify order state to best nearest order state
      # Forex: if order is in at_warehouse state, then nearest lower state is
      # Placed, and hence new state which is to be sent to the user (mobile app)
      # will be Placed.
      #
      # @param order_status [Integer] [current status of the order]
      #
      # @return [JSON] [Hash containing new status of order]
      #
      # def self.get_minified_state_of_order(order_status)

      #   current_status = order_status
      #   minified_states = ORDER_STATES::MINIFIED_STATES
      #   if minified_states.blank?
      #     return ORDER_STATES.get_order_state_by_value(current_status.to_i)
      #   end
      #   # Iterate over minified states array and find the range
      #   # between which current state exists. Then floor of that range
      #   # will be the new status.
      #   # Ex. minified_states: [1,2,7,9] & current_status: 5, then new_status => 2
      #   index = 0
      #   until index > (minified_states.size - 1) || current_status.to_i < minified_states[index][:value].to_i do
      #     index += 1
      #   end
      #   if index > 0
      #     new_status = minified_states[index - 1]
      #   else
      #     new_status = ORDER_STATES.get_order_state_by_value(0)
      #   end
      #   return new_status
      # end

      #
      # Get state attributes hash from state_attributes string
      # This is a n-level MAP traversal, which returns below subtree
      # (if its a hash) or string if it can't be converted to a proper JSON
      #
      # @param state_attributes_string [String] [string of state attributes]
      #
      # @return [JSON] [Hash made from passed string]
      #
      def self.get_state_attributes_hash(state_attributes_string)
        return {} if state_attributes_string.blank?
        begin
          state_attributes_hash = JSON.parse(state_attributes_string)
          attr_hash = {}
          state_attributes_hash.each do |key, value|
            attr_value = get_state_attributes_hash(value)
            attr_hash[key.to_s] = attr_value
          end
          return attr_hash
        rescue
          return state_attributes_string
        end
      end

      #
      # Get filtered state attributes hash
      # Filtering is done on the basis of MINIFIED states, since this will be called
      # in case of mobile APIs
      #
      # @param state_attributes_string [String] [order state attributes value]
      #
      # @return [JSON] [Hash of state attributes]
      #
      def self.get_filtered_state_attributes_hash(state_attributes_string)
        full_attributes_hash = get_state_attributes_hash(state_attributes_string)
        return {} if full_attributes_hash.blank?

        filtered_attributes_hash = {}
        minified_states = ORDER_STATES::MINIFIED_STATES
        # Iterate over minified states and persist attributes related to them only
        minified_states.each do |state|
          key = state[:value]
          if full_attributes_hash[key.to_s].present?
            state_time = full_attributes_hash[key.to_s]['time']
            filtered_attributes_hash[key.to_s] = {
              label: state[:label],
              time: state_time
            }
          end
        end
        return filtered_attributes_hash
      end

      #
      # Get complete state attributes hash.
      # Timings and other attributes of all the states
      #
      # @param state_attributes_string [String] [Attributes string which is stored in DB]
      # 
      # @return [JSON] [Hash corresponding the stored attributes string]
      #
      def self.get_complete_state_attributes_hash(state_attributes_string)
        full_attributes_hash = get_state_attributes_hash(state_attributes_string)
        return {} if full_attributes_hash.blank?

        filtered_attributes_hash = {}
        all_states = ORDER_STATES.constants
        # Iterate over minified states and persist attributes related to them only
        all_states.each do |state|
          state_hash = ORDER_STATES.const_get(state)
          next if (!state_hash.kind_of?(Hash) )
          key = state_hash[:value]
          if full_attributes_hash[key.to_s].present?
            state_time = full_attributes_hash[key.to_s]['time']
            filtered_attributes_hash[key.to_s] = {
              label: state_hash[:label],
              time: state_time
            }
          end
        end
        return filtered_attributes_hash
      end

    end
  end
end
module MarketplaceOrdersModule

  module V1

    module TimeSlotHelper

      DATE_TIME_UTIL = CommonModule::V1::DateTimeUtil

      #
      # Get time slot hash
      #
      # @param slot [Object] [Time slot model object]
      #
      # @return [JSON] [Hash of relevant key-values of time slot]
      #
      def self.get_time_slot_hash(slot)
        return {} if slot.blank?
        slot_hash = {
          id: slot.id,
          from_time: DATE_TIME_UTIL.convert_time_to_epoch(slot.from_time),
          to_time: DATE_TIME_UTIL.convert_time_to_epoch(slot.to_time),
          label: slot.label
        }
        return slot_hash
      end

      def self.get_complete_slot_details(order)
        return {} if order.blank? || order.preferred_delivery_date.blank? || order.time_slot.blank?
        preferred_slot_hash = {
          slot_date: DATE_TIME_UTIL.convert_date_time_to_epoch(order.preferred_delivery_date),
          slot_date_label: order.preferred_delivery_date.strftime('%a , %d %b %Y'),
          time_slot: get_time_slot_hash(order.time_slot)
        }
        return preferred_slot_hash
      end

    end
  end
end

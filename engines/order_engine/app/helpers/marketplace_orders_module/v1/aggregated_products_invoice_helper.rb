#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # Map order object to Json
    #
    module AggregatedProductsInvoiceHelper

      #
      # map_aggregated_products_invoice_by_quantity_to_array function
      # - Since, aggregated_products Array can have multiple Brand Pack entries with
      #   same/mutliple quantities, hence the duplication is removed with quantities added
      #
      # @param aggregated_products [Array] aggregated brand packs
      #
      # - Contains Hash of following type:
      #  { BrandPack(Model) => quantity }
      #  where key being BP Model and value being quantity of BP model.
      #  There can be multiple same entries, as in:
      #  aggregated_products =
      # [
      #  { BP1(Model) => quantity1 },
      #  { BP1(Model) => quantity1 },
      #  { BP1(Model) => quantity2 },
      #  { BP2(Model) => quantity1 },
      # ]
      #
      # Summary-
      # This function returns an aggregated products array of products to procure
      # i.e. corresponding BrandPacks
      #
      # @return [Array] aggregated_products_array
      #
      def self.map_aggregated_products_invoice_by_quantity_to_array(aggregated_products)
        return [] if aggregated_products.blank?

        totalled_aggregation = Hash.new(0)
        aggregated_products.each do |aggregated_product|
          totalled_aggregation[aggregated_product[:brand_pack]] += aggregated_product[:units_to_procure]
        end

        aggregated_products_array = totalled_aggregation.collect{ |brand_pack, quantity| {
          units_to_procure: quantity,
          brand_pack_code: brand_pack.brand_pack_code,
          display_name: brand_pack.sku,
          sku_size: brand_pack.sku_size,
          unit: brand_pack.unit,
          retail_pack_size: brand_pack.retailer_pack,
          mrp: brand_pack.mrp,
          category_label: brand_pack.sub_category.parent_category.label || 'Others'
          }
        }

        # Sort the aggregation by category ids
        aggregated_products_array_sorted_by_category =
          aggregated_products_array.sort_by{ |hash| hash['category_label'] }

        return aggregated_products_array
      end

      #
      # group_brand_packs_to_category function to map brand_packs to category
      # @param order_products [type] [description]
      #
      # @return [Hash] of the form:
      # {
      #  category_label: [
      #     {
      #       order_product1
      #     },
      #     {
      #       order_product2
      #     },
      #     ..
      #     ...
      #
      #   ],
      #
      #   ..
      #   ...
      #
      # }
      #
      #
      def self.group_brand_packs_to_category(order_products)
        return [] if order_products.blank?
        aggregated_brand_packs_by_category = order_products.group_by {|brand_pack| brand_pack[:category_label] }
        return aggregated_brand_packs_by_category
      end
    end
  end
end

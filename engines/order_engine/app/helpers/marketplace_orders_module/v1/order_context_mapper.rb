#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # Map order context object to Json
    #
    module OrderContextMapper

      PLATFORM_TYPE_UTIL = CommonModule::V1::PlatformType

      #
      # Map order context mongo object to JSON
      #
      def self.map_order_context_to_hash(order_context)
        return {} if order_context.blank?
        order_context_hash = {
          order_id: order_context.order_id,
          app_version: order_context.app_version,
          platform_type: get_display_platform_type(order_context.platform_type),
          display_order_user_tag: MarketplaceOrdersModule::V1::OrderUserTags.get_order_user_tag_label_by_id(order_context.order_user_tag),
          order_user_tag: order_context.order_user_tag
        }
      end

      #
      # GET platform type display name
      # If platform type is nil, then send default platform as WEB
      #
      def self.get_display_platform_type(platform_type)
        return PLATFORM_TYPE_UTIL.get_display_platform_type_by_id(platform_type)
      end

    end # End of class
  end
end
#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # Map time slot object to Json
    #
    module TimeSlotMapper

      def self.map_time_slot_to_hash(time_slot)
        return {} if time_slot.blank?
        time_slot_hash = {
          id: time_slot.id,
          from_time: time_slot.from_time.to_i,
          to_time: time_slot.to_time.to_i,
          label: time_slot.label,
          status: time_slot.status
        }
        return time_slot_hash
      end

      def self.map_time_slots_to_array(time_slots)
        return [] if time_slots.blank?
        time_slots_array = []
        time_slots.each do |ts|
          time_slots_array.push(map_time_slot_to_hash(ts))
        end
        return time_slots_array
      end

      def self.add_time_slot_response_hash(time_slot)
        return {
          time_slot: map_time_slot_to_hash(time_slot)
        }
      end

      def self.get_all_time_slots_response_hash(response)
        return {
          time_slots: map_time_slots_to_array(response[:elements]),
          page_count: response[:page_count]
        }
      end

      def self.get_time_slot_response_hash(time_slot)
        return {
          time_slot: map_time_slot_to_hash(time_slot)
        }
      end

      def self.update_time_slot_response_hash(time_slot)
        return {
          time_slot: map_time_slot_to_hash(time_slot)
        }
      end

      def self.change_time_slot_state_response_hash(time_slot)
        return {
          time_slot: map_time_slot_to_hash(time_slot)
        }
      end

    end
  end
end
#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # Map order object to Json
    #
    module OrderMapper

      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator
      ADDRESS_RESPONSE_DECORATOR = AddressModule::V1::AddressResponseDecorator
      ORDER_HELPER = MarketplaceOrdersModule::V1::OrderHelper
      ORDER_PRODUCTS_DAO = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsDao

      # 
      # map_minimal_order_to_hash function
      # @param order [Order Model] Order for which minimal details hash is required
      # 
      # @return [Hash] response
      # 
      def self.map_minimal_order_to_hash(order)
        {
          id: order.id,
          order_id: order.order_id,
          ship_by: order.ship_by,
          status: order.status
        }
      end

      # 
      # map_detailed_order_to_hash function
      # @param order [Order Model] Order for which more order details hash is required
      # 
      # @return [Hash] response
      # 
      def self.map_detailed_order_to_hash(order)
        user = USER_RESPONSE_DECORATOR.get_user_details(order.user)
        address = ADDRESS_RESPONSE_DECORATOR.create_address_hash(order.address)
        {
          id: order.id,
          order_id: order.order_id,
          ship_by: order.ship_by,
          status: order.status,
          user: user,
          address: address

        }
      end

      # 
      # map_aggregated_products_to_array function
      # @param aggregated_order_products [Array] aggregated order products
      #  - Contains Hash of following type:
      #  { MPSP(Model) => quantity }
      #  where key being MPSP Model and value being quantity of MPSP model.
      #  There can be multiple same entries, as in:
      # aggregated_order_products =
      # [
      #  { MPSP1(Model) => quantity1 },
      #  { MPSP1(Model) => quantity1 },
      #  { MPSP1(Model) => quantity2 },
      #  { MPSP2(Model) => quantity1 },
      # ]
      # 
      # Summary-
      # This function returns an aggregated products array of products to procure
      # i.e. corresponding BrandPacks 
      # 
      # @return [Array] aggregated_products_array
      # 
      def self.map_aggregated_products_to_array(aggregated_order_products)
        aggregated_products_array = []
        return aggregated_products_array if aggregated_order_products.blank?

        aggregated_order_products.each do |aggregated_order_product, quantity|
          corresponding_mpsp_mpbp_entries = aggregated_order_product.marketplace_selling_pack_marketplace_brand_packs

          corresponding_mpsp_mpbp_entries.each do |mpsp_mpbp_entry|
            mpbp_detail = get_mpsp_detail_hash(mpsp_mpbp_entry, quantity)
            aggregated_products_array.push(mpbp_detail)
          end
        end
        
        return aggregated_products_array
      end

      # 
      # get_mpsp_detail_hash function to give hash of a mpsp with quantity computed
      #
      # @param mpsp_mpbp_entry [MPSP_MPBP Model] entry of marketplace_selling_pack_marketplace_brand_pack
      # @param quantity [String] quantity ordered of parent MPSP
      # 
      # @return [response]
      # 
      def self.get_mpsp_detail_hash(mpsp_mpbp_entry, quantity)
        quantity_ordered = quantity
        units_to_procure = quantity.to_i * (mpsp_mpbp_entry.quantity).to_i
        response = {
          brand_pack: mpsp_mpbp_entry.marketplace_brand_pack.brand_pack,
          units_to_procure: units_to_procure
        }
        return response
      end

      def self.map_orders_to_array(orders)
        orders_array = []
        orders.each do |order|
          orders_array.push(map_minimal_order_to_hash(order))
        end
        orders_array
      end

      def self.map_order_to_array(order)
        [ map_detailed_order_to_hash(order) ]
      end

      # 
      # map_products_to_array function
      # @param order_products [Array] order products
      # 
      # Summary-
      # This function returns an aggregated products array of products to procure
      # i.e. corresponding BrandPacks 
      # 
      # @return [Array] aggregated_products_array
      # 
      def self.map_products_to_array(order_products)
        aggregated_products_array = []
        return aggregated_products_array if order_products.blank?
        # Eager load order products till master products
        order_products = ORDER_PRODUCTS_DAO.load_order_products_till_master_products(order_products)

        order_products.each do |order_product|
          corresponding_mpsp_mpbp_entries = order_product.marketplace_selling_pack.marketplace_selling_pack_marketplace_brand_packs

          corresponding_mpsp_mpbp_entries.each do |mpsp_mpbp_entry|
            mpbp_detail = get_mpsp_detail_hash(mpsp_mpbp_entry, order_product.quantity)
            aggregated_products_array.push(mpbp_detail)
          end
        end
        return aggregated_products_array
      end

      def self.edit_order_time_slot_response_hash(order)
        order_hash = ORDER_HELPER.get_order_details(order)
        return {
          order: order_hash
        }
      end

      def self.edit_order_address_response_hash(order)
        order_hash = ORDER_HELPER.get_order_details(order)
        return {
          order: order_hash
        }
      end

      def self.edit_order_cart_response_hash(order)
        order_hash = ORDER_HELPER.get_order_details(order)
        return {
          order: order_hash
        }
      end

    end # End of class
  end
end

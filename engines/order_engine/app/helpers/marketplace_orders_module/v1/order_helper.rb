module MarketplaceOrdersModule

  module V1

    module OrderHelper

      PAYMENT_HELPER = MarketplaceOrdersModule::V1::PaymentHelper
      CART_HELPER = MarketplaceOrdersModule::V1::CartHelper
      TIME_SLOT_HELPER = MarketplaceOrdersModule::V1::TimeSlotHelper
      PAYMENT_DAO = MarketplaceOrdersModule::V1::PaymentDao
      ORDER_DAO = MarketplaceOrdersModule::V1::OrderDao
      ORDER_CONTEXT_MAPPER = MarketplaceOrdersModule::V1::OrderContextMapper
      ORDER_STATE_TRANSITION_HELPER = MarketplaceOrdersModule::V1::OrderStateTransitionHelper
      ORDER_MAPPER = MarketplaceOrdersModule::V1::OrderMapper
      ORDER_SERVICE = MarketplaceOrdersModule::V1::OrderService
      AGGREGATED_PRODUCTS_INVOICE_HELPER = MarketplaceOrdersModule::V1::AggregatedProductsInvoiceHelper
      ADDRESS_DECORATOR = AddressModule::V1::AddressResponseDecorator
      USER_DECORATOR = UsersModule::V1::UserResponseDecorator
      ORDER_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
      ORDER_MODEL = MarketplaceOrdersModule::V1::Order
      DATE_TIME_UTIL = CommonModule::V1::DateTimeUtil
      ORDER_CONTEXT_SERVICE = MarketplaceOrdersModule::V1::OrderContextService

      #
      # Get order details with only successfull payment details
      #
      def self.get_order_details(order)
        return {} if order.blank?
        # Get successfull payment of Order OR pending (in case of offline payment modes)
        payment_dao = PAYMENT_DAO.new
        order_payment = payment_dao.get_orders_payment_as_per_its_mode(order)
        payment_hash = PAYMENT_HELPER.get_payment_hash(order_payment)
        cart_hash = CART_HELPER.get_cart_hash(order.cart)
        address_hash = ADDRESS_DECORATOR.create_address_hash(order.address)
        order_state_hash = ORDER_STATES.get_order_state_by_value(order.status)
        preferred_slot_hash = TIME_SLOT_HELPER.get_complete_slot_details(order)
        order_date = get_order_date(order)

        return {
          id: order.id,
          order_id: order.order_id,
          order_date: order_date,
          status: order_state_hash,
          preferred_slot: preferred_slot_hash,
          cart: cart_hash,
          payment: payment_hash,
          address: address_hash
        }
      end

      # 
      # Get complete ORDER details, including user details, state attributes hash, etc.
      #
      # @param order [Object] [Order model object]
      # 
      # @return [JSON] [Order hash]
      #
      def self.get_complete_order_details(order)
        return {} if order.blank?
        order_hash = get_order_details(order)
        user_hash = USER_DECORATOR.get_user_details_for_order(order.user)
        state_attributes_hash = ORDER_STATE_TRANSITION_HELPER.get_complete_state_attributes_hash(order.state_attributes)

        order_hash = order_hash.merge({
          user: user_hash,
          state_attributes: state_attributes_hash
          })
        return order_hash
      end

      #
      # Fetch Array of Order hashes with complete details
      #
      # @param order [Array] [Array of Order model object]
      # 
      # @return [Array] [Array of Order hash]
      #
      def self.create_orders_array_with_complete_details(orders)
        return [] if orders.blank?
        orders_array = []
        orders.each do |order|
          orders_array.push(get_complete_order_details(order) )
        end
        return orders_array
      end

      #
      # Get order details with minified states and its corresponding state attributes
      #
      # @param order [Object] [Order for which details are needed]
      # 
      # @return [JSON] [Hash of order details]
      #
      def self.get_order_details_with_minified_states(order)
        order_hash = get_order_details(order)
        return order_hash if order_hash.blank?

        actual_state = order_hash[:status]
        minified_state = ORDER_STATES.get_minified_state(actual_state)
        filtered_state_attributes = ORDER_STATE_TRANSITION_HELPER.get_filtered_state_attributes_hash(order.state_attributes)

        order_hash = order_hash.merge({
          status: minified_state,
          state_attributes: filtered_state_attributes
          })
        return order_hash
      end

      #
      # Get order details with short orders
      #
      def self.create_short_orders_array(orders)
        if orders.blank?
          return []
        end
        orders_array = []
        orders.each do |order|
          orders_array.push(get_order_details(order) )
        end
        return orders_array
      end

      #
      # Create short details of order with minified states and attributes
      #
      # @param orders [Array] [Array of Order objects]
      # 
      # @return [Array] [Array of orders hash]
      #
      def self.create_short_orders_array_with_minified_states(orders)
        return [] if orders.blank?
        orders_array = []
        orders.each do |order|
          orders_array.push(get_order_details_with_minified_states(order) )
        end
        return orders_array
        
      end

      def self.get_carts_array_from_orders(orders)
        carts_array = []
        return carts_array if orders.blank?

        orders.each do |order|
          carts_array.push(order.cart)
        end
        return carts_array
      end

      def self.get_aggregated_bps_from_mpsps(order_products)
        return [] if order_products.blank?
        # aggregated brand packs are fetched from the order products (i.e. MPSPs)
        aggregated_brand_packs_array = 
          ORDER_MAPPER.map_products_to_array(order_products)

        # duplicated brand packs are removed and added for single instanced Brand Pack key 
        # and serial numbers are generated
        brand_packs_by_quantity = 
          AGGREGATED_PRODUCTS_INVOICE_HELPER.map_aggregated_products_invoice_by_quantity_to_array(aggregated_brand_packs_array) 
        
        return brand_packs_by_quantity
      end

      #
      # Get date of ORDER
      # Fetch its placed order time. IF not present, then return CREATED time.
      #
      # @param order [Object] [Order model object fro which date is to be computed]
      #
      # @return [EpochTime] [Time of order in epoch standard]
      #
      def self.get_order_date(order)
        return nil if order.blank?
        if ORDER_MODEL.attribute_names.include?('order_date') && order.order_date.present?
          return DATE_TIME_UTIL.convert_date_time_to_epoch(order.order_date)
        end
        # Fetch order placed date with the fallback on created_at date
        state_attributes_hash = ORDER_STATE_TRANSITION_HELPER.get_complete_state_attributes_hash(order.state_attributes)
        if state_attributes_hash.present?
          state_hash = state_attributes_hash[ORDER_STATES::PLACED[:value].to_s]
          return state_hash[:time] if (state_hash.present? && state_hash[:time].present?)
        end
        return order.created_at.to_i
      end

      #
      # Get order context hash for passed order
      #
      # @param order [Object] [Model object of order for which context is to be fetched]
      #
      # @return [JSON] [order context hash]
      #
      def self.get_order_context(order)
        return {} if order.blank?
        order_context_service = ORDER_CONTEXT_SERVICE.new
        order_context = order_context_service.get_order_context(order.id)
        order_context_hash = ORDER_CONTEXT_MAPPER.map_order_context_to_hash(order_context)
        return order_context_hash
      end

      #
      # Functiont to check if the order was packed or not
      #
      def self.order_packed?(order)
        state_attributes_hash = ORDER_STATE_TRANSITION_HELPER.get_state_attributes_hash(order.state_attributes)
        if state_attributes_hash[ORDER_STATES::PACKED[:value].to_s].present?
          return true
        else
          return false
        end
      end

    end
  end
end

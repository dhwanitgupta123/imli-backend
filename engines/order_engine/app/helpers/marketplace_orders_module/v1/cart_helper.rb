module MarketplaceOrdersModule

  module V1

    module CartHelper

      # Versioned classes to be kept as global constants
      ORDER_PRODUCTS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsResponseDecorator
      ORDER_DAO = MarketplaceOrdersModule::V1::OrderDao
      CART_DAO = MarketplaceOrdersModule::V1::CartDao
      CACHE_UTIL = CommonModule::V1::Cache

      # These values will be fetched from CONSUL/constants.yml file
      DEFAULT_MINIMUM_CART_VALUE_FOR_FREE_DELIVERY = CACHE_UTIL.read_int('DEFAULT_MINIMUM_CART_VALUE_FOR_FREE_DELIVERY') || BigDecimal.new('1999.0')
      DEFAULT_DELIVERY_COST = CACHE_UTIL.read_int('DEFAULT_DELIVERY_COST') || BigDecimal.new('149.0')
      DEFAULT_FREE_DELIVERY_COST = CACHE_UTIL.read_int('DEFAULT_FREE_DELIVERY_COST') || BigDecimal.new('0.0')
      DEFAULT_RESTRICTED_AMOUNT = CACHE_UTIL.read_int('DEFAULT_RESTRICTED_AMOUNT') || BigDecimal.new('1999.0')
      FIRST_ORDER_DELIVERY_COST = CACHE_UTIL.read_int('FIRST_ORDER_DELIVERY_COST') || BigDecimal.new('0.0')
      ORDER_COST = CACHE_UTIL.read_array('ORDER_COST')

      #
      # Get short cart details
      # - cart
      #   - id
      #   - stats : {...} including computed delivery charges and grand_total
      #
      def self.get_short_cart_details_with_pricing(cart)
        if cart.blank?
          return {}
        end
        payable_amount_details = compute_cart_payable_total(cart)
        cart_hash = get_cart_hash(cart)

        if cart_hash.present? && cart_hash[:stats].present?
          new_cart_stats = cart_hash[:stats].merge({
            delivery_charges: payable_amount_details[:delivery_charges].round(2),
            grand_total: payable_amount_details[:grand_total].round(2)
          })
          cart_hash[:stats] = new_cart_stats
        end
        return cart_hash
      end

      #
      # Get CART hash
      #   - cart
      #     - id: cart_id
      #     - status: cart status
      #     - stats: {...}
      #
      def self.get_cart_hash(cart)
        return {} if cart.blank?
        return {
          id: cart.id,
          status: cart.user_cart_status,
          stats: {
            total_mrp: cart.total_mrp.round(2),
            cart_savings: cart.cart_savings.round(2),
            cart_total: cart.cart_total.round(2)
          }
        }
      end

      #
      # Get complete cart details
      # - cart
      #   - id
      #   - stats : {...}
      #   - order_products: [ {}, {}, ...]
      #
      def self.get_cart_complete_details(cart)
        return {} if cart.blank?
        cart_dao = CART_DAO.new
        cart_details = get_short_cart_details_with_pricing(cart)
        if cart.order_products.present?
          active_order_products = cart_dao.active_products_in_cart(cart)
          order_products_complete_details = ORDER_PRODUCTS_RESPONSE_DECORATOR.get_complete_order_products_array(active_order_products)
        else
          order_products_complete_details = []
        end
          cart_details = cart_details.merge({ order_products: order_products_complete_details })
        return cart_details
      end

      #
      # Get delivery charges for the cart
      # If cart_total greater than some minimum fixed amount, then delivery charges will be Zero
      # Else, a minimum basic delivery charge will be applied
      #
      def self.get_delivery_charge(cart)
        cart_total = cart.cart_total
        delivery_charges = DEFAULT_DELIVERY_COST.to_d
        order_cost = get_order_cost_for_user(cart.user)
        # Convert hash to JSON
        order_cost = JSON.parse(JSON.generate(order_cost))
        if order_cost.present?
          if cart_total.to_d == 0 || cart_total.to_d >= order_cost["MINIMUM_CART_VALUE_FOR_FREE_DELIVERY"].to_d
            delivery_charges = order_cost["FREE_DELIVERY_COST"].to_d
          else
            delivery_charges = order_cost["DELIVERY_COST"].to_d
          end
        end
        return delivery_charges
      end

      #
      # Get order cost for user based on its current number of order
      #
      # @param user [Object] [User model object]
      # 
      # @return [JSON] [Hash of order cost]
      #
      def self.get_order_cost_for_user(user)
        order_cost = {}
        order_dao = ORDER_DAO.new
        order_number = order_dao.get_current_number_of_order_of_user(user) 
        order_cost = get_order_cost(order_number) if order_number.present?
        return order_cost
      end

      #
      # Get order cost based on order number
      # Fetch Order cost Array from constants and then iterate over it
      # to find hash as per the order number. Also, order_cost is interpolated
      # if its corresponding value is not present in array
      #
      # @param order_number [Integer] [number of Order]
      # 
      # @return [JSON] [Hash of order cost]
      # 
      def self.get_order_cost(current_order_number)
        order_cost_array = ORDER_COST   # ORDER_COST is global variable (Cached OrderCost Array)
        if order_cost_array.present?
          order_cost_array = order_cost_array.sort{|o1, o2| o1["ORDER_NUMBER"].to_i <=> o2["ORDER_NUMBER"].to_i}
          order_cost_array.each_with_index do |order_cost_hash, index|
            o_number = order_cost_hash["ORDER_NUMBER"].to_i
            next if current_order_number.to_i > o_number.to_i
            return order_cost_hash if current_order_number.to_i == o_number.to_i
            return order_cost_array[index-1] if current_order_number.to_i < o_number.to_i
          end
          return order_cost_array.last
        end
        # If order cost array not defined, then return default values
        return {
          "DELIVERY_COST": DEFAULT_DELIVERY_COST.to_d,
          "RESTRICTED_AMOUNT": DEFAULT_RESTRICTED_AMOUNT.to_d,
          "FREE_DELIVERY_COST": DEFAULT_FREE_DELIVERY_COST.to_d,
          "MINIMUM_CART_VALUE_FOR_FREE_DELIVERY": DEFAULT_MINIMUM_CART_VALUE_FOR_FREE_DELIVERY.to_d
        }
      end

      #
      # Fuction to check whether this is first order of the user?
      #
      # @param cart [Object] [Cart of the user]
      # 
      # @return [Boolean] [true if its first order of the user, else false]
      #
      def self.first_order_of_user?(cart)
        return false if cart.blank? || cart.user.blank?
        user = cart.user
        order_dao = ORDER_DAO.new
        return true if order_dao.is_first_order_of_user?(user)  
        return false      
      end

      #
      # Compute cart payable total amount with its relevant delivery charges
      #
      def self.compute_cart_payable_total(cart)
        delivery_charges = get_delivery_charge(cart)
        cart_total = cart.cart_total

        grand_total = cart_total + delivery_charges
        return { delivery_charges: delivery_charges, grand_total: grand_total }
      end

    end
  end
end

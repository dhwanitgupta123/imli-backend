module MarketplaceOrdersModule

  module V1

    module PaymentHelper
      CART_HELPER = MarketplaceOrdersModule::V1::CartHelper
      CACHE_UTIL = CommonModule::V1::Cache
      PAYMENT_MODES = MarketplaceOrdersModule::V1::PaymentModes
      PAYMENT_TYPE = MarketplaceOrdersModule::V1::PaymentType
      PAYMENT_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::PaymentStates
      PAYMENT_INSTANCES = MarketplaceOrdersModule::V1::PaymentInstances
      PAYMENT_SERVICE = MarketplaceOrdersModule::V1::PaymentService

      #
      # Fetch payment details hash
      # For structure, refer config/payment_details.yml
      #
      # @return [JSON] [Hash containing payment details hash]
      #
      def self.fetch_payment_details_hash
        payment_details = CACHE_UTIL.read_json('PAYMENT_DETAILS')
        if payment_details.blank?
          payment_file_path = OrderEngine::Engine.root.join("config", "payment_details.yml").to_s
          if File.file?(payment_file_path)
            payment_details  = YAML::load_file(payment_file_path)  # Returns as a HASH
            CACHE_UTIL.write('PAYMENT_DETAILS', JSON.generate(payment_details))
          end
        end
        return payment_details
      end

      #
      # Fetch cart specific payment details
      #
      # @param cart [Object] [Cart model object]
      # @param billing_amount [BigDecimal] [billing amount over which payment mode discounts need to apply]
      #
      # @return [JSON] [Hash containing mode, net_total and total_savings]
      #
      def self.fetch_cart_specific_payment_details(cart, billing_amount)
        payment_details = fetch_payment_details_hash
        details_array = []
        payment_details.keys.each do |key|
          details_hash = payment_details[key]

          payment_mode_details = compute_payment_mode_details(details_hash, billing_amount, cart.cart_savings)

          details_hash['mode'] = key.to_s
          details_hash['payment_mode_savings_value'] = payment_mode_details[:payment_mode_savings]
          details_hash['net_total'] = payment_mode_details[:net_total]
          details_hash['total_savings'] = payment_mode_details[:total_savings]

          details_array.push(details_hash)
        end
        return details_array
      end

      #
      # Fetch user billing details.
      # These are the details which are before payment is made and it computes
      # user specific discount and apply over grand total amount
      #
      # @param user [Object] [User model object for which billing details need to compute]
      # @param cart [Object] [Cart model object belongs to user]
      #
      # @return [JSON] [Hash containing delivery_charges, grand_total, discount, billing_amount]
      #
      def self.fetch_user_billing_details(user, cart)
        cart_payable_total = CART_HELPER.compute_cart_payable_total(cart)
        user_specific_discount = compute_users_discount(user)

        billing_amount = cart_payable_total[:grand_total] - user_specific_discount
        return {
          delivery_charges: cart_payable_total[:delivery_charges],
          grand_total: cart_payable_total[:grand_total],
          discount: user_specific_discount,
          billing_amount: billing_amount
        }
      end

      #
      # Compute payment mode details based on passed hash payment-mode-details hash,
      # billing amount and cart savings
      #
      # @return [JSON] [Hash of net_total, total_savings, and payment_mode_savings]
      #
      def self.compute_payment_mode_details(payment_mode_details, billing_amount, cart_savings)
          # Fetch reward percentage and apply over cart total and savings
          # Convert string to BigDecimal, since DB values are all BigDecimal
          payment_mode_savings_percentage = payment_mode_details['payment_mode_savings'].to_d  # to decimal
          payment_mode_savings_percentage = BigDecimal.new('0.0') if payment_mode_savings_percentage.blank?
          # Compute additional discount
          additional_discount = billing_amount * (payment_mode_savings_percentage / 100)

          payment_mode_savings_value = additional_discount.round(2)
          net_total = (billing_amount - additional_discount).round(2)
          total_savings = (cart_savings + additional_discount).round(2)
          # As per the calculation, consider user has to pay the computed net_total
          # amount. In case of any exceptions, this need to be handled in service layer
          payable_total = net_total

          return { net_total: net_total, payable_total: payable_total, total_savings: total_savings, payment_mode_savings: payment_mode_savings_value }
      end

      #
      # Compute users discount (based on its previledges and membership)
      #
      def self.compute_users_discount(user)
        # Need to return Users Discount with respect to its membership
        return BigDecimal.new('0.0')
      end

      #
      # Get display labels for payment
      #
      # @param payment [Object] [Payment model object]
      #
      # @return [JSON] [Hash of display names of payment mode and payment status]
      #
      def self.get_payment_display_labels(payment)
        return {} if payment.blank?
        mode_display_name = PAYMENT_MODES.get_payment_mode_display_name(payment.mode)
        status_display_name = PAYMENT_STATES.get_payment_state_display_name(payment.status)
        instance_display_name = PAYMENT_INSTANCES.get_display_instance_by_id(payment.payment_instance)
        return {
          mode_display_name: mode_display_name,
          status_display_name: status_display_name,
          instance_display_name: instance_display_name
        }
      end

      #
      # Get display labels of payment for invoice
      #
      # @param payment [Object] [Payment model object]
      #
      # @return [JSON] [Hash of display names of payment mode and payment status for invoice]
      #
      def self.get_payment_display_labels_for_invoice(payment)
        return {} if payment.blank?
        payment_labels = get_payment_display_labels(payment)
        mode_display_name = PAYMENT_MODES.get_payment_mode_display_name(payment.mode)
        status_display_name = PAYMENT_STATES.get_payment_display_for_invoice(payment.status)
        instance_display_name = PAYMENT_INSTANCES.get_payment_instance_display_name(payment.payment_instance)
        payment_labels = payment_labels.merge({
          mode_display_name: mode_display_name,
          status_display_name: status_display_name,
          instance_display_name: instance_display_name
          })
        return payment_labels
      end

      #
      # Get array of all supported offline mode of payment
      # Response should be array of Hash<id: mode_id, label: mode_name>
      #
      def self.get_offline_modes
        offline_modes = PAYMENT_MODES.get_offline_payment_modes
        offline_modes_array = []
        offline_modes.each do |mode|
          offline_modes_array.push({
            id: mode,
            label: PAYMENT_MODES.get_payment_mode_display_name(mode)
            })
        end
        return offline_modes_array
      end

      def self.get_offline_modes_with_gateways
        return PAYMENT_MODES.get_offline_modes_with_gateways
      end

      #
      # Get display labels for payment
      #
      # @param payment [Object] [Payment model object]
      #
      # @return [JSON] [Hash of display names of payment mode and payment status]
      #
      def self.get_payment_display_labels(payment)
        return {} if payment.blank?
        mode_display_name = PAYMENT_MODES.get_payment_mode_display_name(payment.mode)
        status_display_name = PAYMENT_STATES.get_payment_state_display_name(payment.status)
        instance_display_name = PAYMENT_INSTANCES.get_display_instance_by_id(payment.payment_instance)
        type_display_name = PAYMENT_TYPE.get_display_label_by_id(payment.payment_type)
        return {
          mode_display_name: mode_display_name,
          status_display_name: status_display_name,
          instance_display_name: instance_display_name,
          type_display_name: type_display_name
        }
      end

      #
      # Create payment hash from payment object
      #
      def self.get_payment_hash(payment)
        return {} if payment.blank?
        display_labels = get_payment_display_labels(payment)
        payment_hash = {
          id: payment.id,
          mode: payment.mode,
          payment_instance: payment.payment_instance,
          status: payment.status,
          total_vat_tax: payment.total_vat_tax.round(2),
          total_service_tax: payment.total_service_tax.round(2),
          delivery_charges: payment.delivery_charges.round(2),
          grand_total: payment.grand_total.round(2),
          discount: payment.discount.round(2),
          billing_amount: payment.billing_amount.round(2),
          payment_mode_savings: payment.payment_mode_savings.round(2),
          net_total: payment.net_total.round(2),
          payable_total: payment.payable_total.round(2),
          payment_type: payment.payment_type,
          total_savings: payment.total_savings.round(2),
          display_labels: display_labels
        }
        return payment_hash
      end

      #
      # @param order [Object] [Order model object]
      #
      # @return [Object] [Payment object]
      #
      def self.get_orders_payment_as_per_its_mode(order)
        payment_service = PAYMENT_SERVICE.new({})
        payment = payment_service.get_orders_payment_as_per_its_mode(order)
        return payment
      end
    end
  end
end

#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Helper to create hash of Inventory order products
    #
    module InventoryOrderProductsHelper

      INVENTORY_BRAND_PACK_MAPPER = InventoryProductModule::V1::InventoryBrandPackMapper
      INVENTORY_ORDER_PRODUCT_STATES = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderProductStates
      #
      # Function to get the hash details of inventory order products
      #
      def self.get_inventory_order_products_hash(inventory_order_products)
        return [] if inventory_order_products.blank?
        inventory_order_products_array = []
        inventory_order_products.each do |inventory_order_product|
          if inventory_order_product.buying_status == INVENTORY_ORDER_PRODUCT_STATES::ACTIVE
            inventory_brand_pack_hash = INVENTORY_BRAND_PACK_MAPPER.map_inventory_brand_pack_to_hash(inventory_order_product.inventory_brand_pack)
            args = {
              id: inventory_order_product.id,
              quantity_ordered: inventory_order_product.quantity_ordered,
              quantity_received: inventory_order_product.quantity_received,
              mrp: inventory_order_product.mrp.round(2),
              cost_price: inventory_order_product.cost_price.round(2),
              vat: inventory_order_product.vat.round(2),
              vat_value: inventory_order_product.vat_tax_value.round(2),
              service_tax: inventory_order_product.service_tax.round(2),
              service_tax_value: inventory_order_product.service_tax_value.round(2),
              cst: inventory_order_product.cst.round(2),
              cst_value: inventory_order_product.cst_value.round(2),
              octrai: inventory_order_product.octrai.round(2),
              octrai_value: inventory_order_product.octrai_value.round(2),
              buying_status: inventory_order_product.buying_status,
              inventory_brand_pack: inventory_brand_pack_hash
            }
            inventory_order_products_array.push(args)
          end
        end
        return inventory_order_products_array
      end
    end
  end
end

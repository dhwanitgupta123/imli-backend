#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Helper module to create hash for the response of Inventory order
    #
    module InventoryOrderHelper

      INVENTORY_ORDER_PAYMENT_HELPER = InventoryOrdersModule::V1::InventoryOrderPaymentHelper
      INVENTORY_ORDER_PRODUCTS_HELPER = InventoryOrdersModule::V1::InventoryOrderProductsHelper
      INVENTORY_ORDER_STATES = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderStates
      INVENTORY_ORDER_EVENTS = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderEvents
      WAREHOUSE_MAPPER = SupplyChainModule::V1::WarehouseMapper
      INVENTORY_MAPPER = SupplyChainModule::V1::InventoryMapper
      CACHE_UTIL = CommonModule::V1::Cache

      #
      # Function to get the hash details of inventory order and it's related objects
      #
      def self.get_inventory_order_hash(inventory_order)
        return {} if inventory_order.blank?
        payment = INVENTORY_ORDER_PAYMENT_HELPER.get_inventory_order_payment_hash(inventory_order.inventory_order_payment) if inventory_order.inventory_order_payment.present?
        payment = {} if payment.blank?
        order_products = INVENTORY_ORDER_PRODUCTS_HELPER.get_inventory_order_products_hash(inventory_order.inventory_order_products) if inventory_order.inventory_order_products.present?
        order_products = [] if order_products.blank?
        warehouse = WAREHOUSE_MAPPER.map_short_warehouse_to_hash(inventory_order.warehouse)
        address_hash = {
          shipping_address: WAREHOUSE_MAPPER.map_warehouse_address_to_hash(inventory_order.shipping_address),
          billing_address: WAREHOUSE_MAPPER.map_warehouse_address_to_hash(inventory_order.billing_address)
        }
        inventory_order_hash = get_short_order_details(inventory_order)
        inventory_order_hash = inventory_order_hash.merge(warehouse: warehouse)
        inventory_order_hash = inventory_order_hash.merge(address: address_hash)
        inventory_order_hash = inventory_order_hash.merge(products: order_products)
        inventory_order_hash = inventory_order_hash.merge(payment: payment)
        return inventory_order_hash
      end

      #
      # Function to get the hash of the inventory order state attributes
      #
      def self.get_inventory_order_state_attributes_hash(inventory_order)
        state_attribute_array = []
        state_attributes = INVENTORY_ORDER_STATES.get_inventory_order_state_attributes(inventory_order.id)
        return state_attribute_array if state_attributes.blank?
        state_attributes.each do |state_attribute|
          state_attribute_hash = {
            order_status: state_attribute.order_status,
            created_at: state_attribute.created_at.to_i
          }
          state_attribute_array.push(state_attribute_hash)
        end
        return state_attribute_array
      end

      #
      # Helper to get the short details of inventory order
      #
      def self.get_short_order_details(inventory_order)
        return {} if inventory_order.blank?
        order_date = get_purchase_order_date(inventory_order)
        inventory = INVENTORY_MAPPER.map_short_inventory_to_hash(inventory_order.inventory)
        if inventory_order.pdf_link.present?
          pdf_link = 'https://' + APP_CONFIG['config']['S3_HOST_NAME'] + '/' + APP_CONFIG['config']['S3_BUCKET'] + '/' + inventory_order.pdf_link
        end
        status_hash = INVENTORY_ORDER_STATES.get_order_state_by_value(inventory_order.status)
        return {
          id: inventory_order.id,
          order_id: inventory_order.order_id,
          inventory: inventory,
          status: status_hash,
          pdf_link: pdf_link || '',
          is_inter_state: inventory_order.is_inter_state || false,
          order_date: order_date
        }
      end

      #
      # Helper to create array of order details hash
      #
      def self.create_orders_array_with_short_details(inventory_orders)
        inventory_order_array = []
        return inventory_order_array if inventory_orders.blank?
        inventory_orders.each do |inventory_order|
          inventory_order_array.push(get_short_order_details(inventory_order))
        end
        return inventory_order_array
      end

      #
      # Function to get the purchase order date of the Inventory order
      #
      def self.get_purchase_order_date(inventory_order)
        state_attribute_array = get_inventory_order_state_attributes_hash(inventory_order)
        order_date = inventory_order.created_at.to_i
        state_attribute_array.each do |state_attribute|
          if state_attribute[:order_status] == INVENTORY_ORDER_STATES::PLACED[:label] && order_date < state_attribute[:created_at]
            order_date = state_attribute[:created_at]
          end
        end
        return order_date
      end

      def self.create_inventory_order_details_with_state_attributes_hash(inventory_order)
        inventory_order_hash = get_inventory_order_hash(inventory_order)
        state_attributes_hash = get_inventory_order_state_attributes_hash(inventory_order)
        allowed_events = INVENTORY_ORDER_EVENTS.get_allowed_events(inventory_order)
        inventory_order_hash = inventory_order_hash.merge(state_attributes: state_attributes_hash)
        inventory_order_hash = inventory_order_hash.merge(allowed_events: allowed_events)
        return inventory_order_hash
      end


      #
      # Initialize Order Time Filters
      #   - to_time: passed to_time (or default time passed)
      #   - from_time: passed from_time (or to_time minus 1 day)
      #
      # @param params [JSON] [Hash of interval params]
      # @param default_to_time [DateTime] [default value of to time]
      #
      # @return [JSON] [Hash of from_time and to_time]
      #
      def self.initialize_order_time_filters(params, default_to_time)
        to_time = params[:to_time]
        from_time = params[:from_time]

        final_to_time = to_time.present? ? Time.zone.at(to_time.to_i) : default_to_time
        final_from_time = from_time.present? ? Time.zone.at(from_time.to_i) : (final_to_time - 1.day)

        return ({till_time: final_to_time, from_time: final_from_time})
      end

      #
      # Generates Order ID (a unique number)
      #
      def self.generate_order_id(order)
        return 'IMLI-' + order.warehouse_id.to_s + '-' + order.inventory.distributor.vendor_code.to_s + '-' + order.id.to_s
      end
    end
  end
end

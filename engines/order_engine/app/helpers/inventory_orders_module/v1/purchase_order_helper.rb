#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Helper to create attributes hash for the Purchase order PDF generation
    #
    module PurchaseOrderHelper

      INVENTORY_ORDER_HELPER = InventoryOrdersModule::V1::InventoryOrderHelper
      INVENTORY_MAPPER = SupplyChainModule::V1::InventoryMapper
      DISTRIBUTOR_MAPPER = SupplyChainModule::V1::DistributorMapper
      CACHE = CommonModule::V1::Cache

      #
      # Function to create attributes for Purchase Order PDF
      #
      def self.generate_purchase_order(inventory_order)
        inventory_order_details = INVENTORY_ORDER_HELPER.get_inventory_order_hash(inventory_order)
        @is_inter_state = inventory_order_details[:is_inter_state]
        supplier_address = INVENTORY_MAPPER.get_inventory_address_details(inventory_order.inventory.inventory_address)
        supplier_details = DISTRIBUTOR_MAPPER.map_distributor_to_hash(inventory_order.inventory.distributor)
        product_details = get_product_details(inventory_order_details[:products])
        params = {
          po_number: inventory_order_details[:order_id],
          shipping_address: inventory_order_details[:address][:shipping_address],
          billing_address: inventory_order_details[:address][:billing_address],
          supplier_address: supplier_address,
          products: product_details[:products_array],
          total_quantity:  product_details[:total_quantity],
          total_octrai_tax: inventory_order_details[:payment][:total_octrai_value],
          total_tax: product_details[:total_tax],
          total_cost_price: product_details[:total_cost_price],
          net_total: product_details[:net_total] + inventory_order_details[:payment][:total_octrai_value],
          WAREHOUSE_TIN_NO: CACHE.read('TIN_VAT_NUMBER'),
          IMLI_LOGO_BLACK: CACHE.read('IMLI_LOGO_BLACK'),
          WAREHOUSE_NAME: CACHE.read('SELLER_NAME'),
          WAREHOUSE_ADDRESS_LINE_1: CACHE.read('SELLER_ADDRESS_LINE_1'),
          WAREHOUSE_ADDRESS_LINE_2: CACHE.read('SELLER_ADDRESS_LINE_2'),
          warehouse_cst_no: '',
          supplier_tin_no: supplier_details[:tin_no],
          supplier_cst_no: supplier_details[:cst_no],
          po_date: Time.zone.at(inventory_order_details[:order_date]).strftime("%d.%m.%Y")
        }
        return generate_purchase_order_pdf(params)
      end

      #
      # Function to get the Products hash required for the Purchase order
      #
      def self.get_product_details(products)
        products_array = []
        total_quantity = 0
        total_tax = BigDecimal('0')
        net_total = BigDecimal('0')
        total_cost_price = BigDecimal('0')
        return { products_array: [] } if products.blank?
        products.each do |product|
          tax = product[:vat]
          tax_value = product[:vat_value]
          if @is_inter_state
            tax = product[:cst]
            tax_value = product[:cst_value]
          end
          if product[:quantity_received].present?
            quantity = product[:quantity_received]
          else
            quantity = product[:quantity_ordered]
          end
          total_quantity += quantity
          product_detail = {
            brand_pack_code: product[:inventory_brand_pack][:brand_pack][:brand_pack_code],
            distributor_pack_code: product[:inventory_brand_pack][:distributor_pack_code],
            sku: product[:inventory_brand_pack][:brand_pack][:sku],
            sku_size: product[:inventory_brand_pack][:brand_pack][:sku_size],
            unit: product[:inventory_brand_pack][:brand_pack][:unit],
            mrp: product[:mrp],
            cost_price: product[:cost_price],
            quantity: quantity,
            tax: tax,
            tax_value: (tax_value * quantity).round(2),
            value: (product[:cost_price] * quantity).round(2)
          }
          total_tax += (tax_value * quantity).round(2)
          total_cost_price += (product[:cost_price] * quantity).round(2)
          products_array.push(product_detail)
        end
        net_total = total_tax + total_cost_price
        return {
          products_array: products_array,
          total_quantity: total_quantity,
          total_tax: total_tax,
          total_cost_price: total_cost_price,
          net_total: net_total
        }
      end

      #
      # Function to generate PDF of purchase order and store it locally
      #
      def self.generate_purchase_order_pdf(params)
        file = File.join(Rails.root,
                             'app',
                             'views',
                             'inventory_module',
                             'v1',
                             'purchase_order.liquid'
                            )
        file = File.read(file)
        template = Liquid::Template.parse(file)
        params = ActiveSupport::HashWithIndifferentAccess.new(params)
        template = template.render(params)
        wicked_pdf_instance = WickedPdf.new
        pdf = wicked_pdf_instance.pdf_from_string(template)
        file_path = "#{Rails.root}/public/"
        filename = "#{params[:po_number]}.pdf"
        pdf_path = file_path + filename
        File.open(pdf_path, 'wb') do |file|
          file << pdf
        end
        return { file_path: pdf_path, filename: filename }
      end
    end
  end
end

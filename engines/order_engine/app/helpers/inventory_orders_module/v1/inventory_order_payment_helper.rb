#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Helper to get the hash of Inventory order payment hash
    #
    module InventoryOrderPaymentHelper

      #
      # Function to get the hash details of inventory order payment
      #
      def self.get_inventory_order_payment_hash(inventory_order_payment)
        return {} if inventory_order_payment.blank?
        return {
          id: inventory_order_payment.id,
          total_mrp: inventory_order_payment.total_mrp.round(2),
          total_cost_price: inventory_order_payment.total_cost_price.round(2),
          total_vat_value: inventory_order_payment.total_vat_value.round(2),
          total_cst_value: inventory_order_payment.total_cst_value.round(2),
          total_octrai_value: inventory_order_payment.total_octrai_value.round(2),
          total_tax: inventory_order_payment.total_tax.round(2),
          net_total: inventory_order_payment.net_total.round(2),
          status: inventory_order_payment.status
        }
      end
    end
  end
end

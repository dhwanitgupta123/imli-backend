#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    
    class ChangeInventoryOrderStateApi < BaseOrdersModule::V1::BaseApi

      INVENTORY_ORDER_SERVICE = InventoryOrdersModule::V1::InventoryOrderService
      INVENTORY_ORDERS_RESPONSE_DECORATOR = InventoryOrdersModule::V1::InventoryOrdersResponseDecorator
      INVENTORY_ORDER_EVENTS = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderEvents
      INVENTORY_ORDER_HELPER = InventoryOrdersModule::V1::InventoryOrderHelper
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params)
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing event to trigger]
      # Request:
      # {
      #   inventory_order: {
      #     event: '1'
      #   }
      # }
      #
      # @return [Response] [response to be sent to the user]
      #
      def enact(request)
        begin
          validate_request(request)
          inventory_order_service = INVENTORY_ORDER_SERVICE.new(@params)
          response = inventory_order_service.change_purchase_order_state(request)
          inventory_order_hash = INVENTORY_ORDER_HELPER.get_inventory_order_hash(response)
          return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_inventory_order_details_response(inventory_order_hash)
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
          rescue CUSTOM_ERROR_UTIL::InsufficientDataError, CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
            return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        end
      end

      # 
      # Validate incoming request
      #
      # @param request [JSON] [Hash containing event to trigger change state ]
      # 
      # Allowed events are:: RECEIVE, CANCEL & RENEW Purchase order
      #
      # @raise [InsufficientDataError] [Tax details are negative]
      #
      def validate_request(request)
        error_array = []
        error_array.push('Inventory Order ID') if request[:id].blank?
        error_array.push('Event') if request[:event].blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
        allowed_event = [INVENTORY_ORDER_EVENTS::RECEIVE_PURCHASE_ORDER, INVENTORY_ORDER_EVENTS::CANCEL_PURCHASE_ORDER, INVENTORY_ORDER_EVENTS::RENEW_PURCHASE_ORDER]
        unless allowed_event.include?(request[:event].to_i)
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::INVALID_EVENT)
        end
      end
    end
  end
end
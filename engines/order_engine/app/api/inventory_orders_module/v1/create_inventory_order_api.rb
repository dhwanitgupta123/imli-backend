#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    
    class CreateInventoryOrderApi < BaseOrdersModule::V1::BaseApi

      INVENTORY_ORDER_SERVICE = InventoryOrdersModule::V1::InventoryOrderService
      INVENTORY_ORDERS_RESPONSE_DECORATOR = InventoryOrdersModule::V1::InventoryOrdersResponseDecorator
      INVENTORY_ORDER_HELPER = InventoryOrdersModule::V1::InventoryOrderHelper
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params)
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details]
      # Request:
      #  {
      #   inventory_order: {
      #     user_id: '1',
      #     warehouse: {
      #       id: '2'
      #     },
      #     inventory: {
      #       id: '3'
      #     },
      #     address: {
      #       shipping_address: {
      #         id: '4'
      #       },
      #       billing_address: {
      #         id: '5'
      #       }
      #     }
      #   }
      # }
      #
      #
      # @return [Response] [response to be sent to the user]
      #
      def enact(request)
        begin
          validate_request(request)
          inventory_order_service = INVENTORY_ORDER_SERVICE.new(@params)
          response = inventory_order_service.transactional_create_purchase_order(request)
          inventory_order_hash = INVENTORY_ORDER_HELPER.get_inventory_order_hash(response)
          return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_inventory_order_details_response(inventory_order_hash)
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
          rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
            return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
          rescue CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
            return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_not_found_error(e.message)
        end
      end

      # 
      # Validate incoming request
      #
      # @param request [JSON] [Hash containing warehouse, inventory & address details and address id ]
      #
      # @raise [InsufficientDataError] [if cart id or address id not present]
      #
      def validate_request(request)
        error_array = []
        error_array.push('Warehouse id') if request[:warehouse].blank? || request[:warehouse][:id].blank?
        error_array.push('Inventory id') if request[:inventory].blank? || request[:inventory][:id].blank?
        error_array.push('Addresses') if request[:address].blank?
        error_array.push('Shipping address') if request[:address][:shipping_address].blank? || request[:address][:shipping_address][:id].blank?
        error_array.push('Billing address') if request[:address][:billing_address].blank? || request[:address][:billing_address][:id].blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end
    end
  end
end
#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    
    class StockInventoryOrderApi < BaseOrdersModule::V1::BaseApi

      INVENTORY_ORDER_SERVICE = InventoryOrdersModule::V1::InventoryOrderService
      INVENTORY_ORDERS_RESPONSE_DECORATOR = InventoryOrdersModule::V1::InventoryOrdersResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      GENERAL_HELPER = CommonModule::V1::GeneralHelper

      def initialize(params)
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details]
      # Request:  
      # {
      #   inventory_order: {
      #     warehouse_id: '2',
      #     products: [
      #       {
      #         id: '1',
      #         quantity_received: '2'
      #       }
      #     ],
      #     payment: {
      #       total_octrai: '100'
      #     }
      #   }
      # }
      #
      # @return [Response] [response to be sent to the user]
      #
      def enact(request)
        begin
          validate_request(request)
          inventory_order_service = INVENTORY_ORDER_SERVICE.new(@params)
          response = inventory_order_service.transactional_stock_purchase_order(request)
          return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_success_response
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
          rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
            return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
          rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
            return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_not_found_error(e.message)
          rescue CUSTOM_ERROR_UTIL::S3FailedToUpload
            return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_response_runtime_error(CONTENT_UTIL::NOT_ABLE_TO_UPLOAD_FILE)
        end
      end

      # 
      # Validate incoming request
      #
      # @param request [JSON] [Hash containing warehouse, product details ]
      #
      # @raise [InsufficientDataError] [if warehouse id not present]
      #
      def validate_request(request)
        error_array = []
        error_array.push('Warehouse id') if request[:warehouse].blank? || request[:warehouse][:id].blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to APIs
#
module BaseOrdersModule
  #
  # Version1 for address module
  #
  module V1
    #
    # BaseApi to inject basic funcionalities
    #
    class BaseApi
      extend AnnotationsModule::V1::ExceptionHandlerAnnotations

      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params='')
        self.extend AnnotationsModule::V1::ExceptionHandlerAnnotations
        @custom_error_util = CommonModule::V1::CustomErrors
        @order_response_decorator = MarketplaceOrdersModule::V1::OrdersResponseDecorator
      end
    end
  end
end
#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    
    class GetCartApi < BaseOrdersModule::V1::BaseApi

      CART_SERVICE = MarketplaceOrdersModule::V1::CartService
      ORDERS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::OrdersResponseDecorator
      CARTS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::CartsResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params)
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details]
      # Request:
      #  - Session token of the user
      #
      # @return [Response] [response to be sent to the user]
      #
      def enact
        begin
          carts_service = CART_SERVICE.new(@params)
          # Need to add delivery cost dynamically in Mapper/Helper/Decorator
          response = carts_service.get_users_cart
          return CARTS_RESPONSE_DECORATOR.create_cart_details_response(response)
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError, CUSTOM_ERROR_UTIL::InvalidDataError => e
            return CARTS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
          rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
            return CARTS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
          rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
            return CARTS_RESPONSE_DECORATOR.create_not_found_error(e.message)
          # If unable to change state of CART (PreConditionRequired), then its a Internal Server Error
          rescue CUSTOM_ERROR_UTIL::RunTimeError, CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
            return CARTS_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        end
      end

    end
  end
end
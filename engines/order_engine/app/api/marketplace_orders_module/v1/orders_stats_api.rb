#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    #
    # OrdersStats api to fetch time slot
    #
    class OrdersStatsApi < BaseOrdersModule::V1::BaseApi

      ORDER_DATA_SERVICE = MarketplaceOrdersModule::V1::OrderDataService
      ORDERS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::OrdersResponseDecorator

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Generate the current orders stats and return response
      #
      # @return [JSON] [response to be sent]
      #
      _ExceptionHandler_
      def enact
        orders_data_service = ORDER_DATA_SERVICE.new(@params)
        orders_stats_array = orders_data_service.get_orders_stats
        return ORDERS_RESPONSE_DECORATOR.create_order_stats_response(orders_stats_array)
      end

    end
  end
end

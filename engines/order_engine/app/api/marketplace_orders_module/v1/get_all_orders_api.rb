#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # GetAllOrders api, it calls the
    # order_panel service and return the JsonResponse
    #
    class GetAllOrdersApi < BaseOrdersModule::V1::BaseApi

      ORDER_PANEL_SERVICE = MarketplaceOrdersModule::V1::OrderPanelService
      ORDER_MAPPER = MarketplaceOrdersModule::V1::OrderMapper
      ORDERS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::OrdersResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      def initialize(params)
        @params = params
      end

      #
      # Return all the orders
      #
      # @return [JsonResponse]
      #
      def enact(request)
        begin
          order_panel_service = ORDER_PANEL_SERVICE.new(@params)
          response = order_panel_service.get_all_orders_in_interval(request)
          orders = response[:elements]
          page_count = response[:page_count]
          #order_array = ORDER_MAPPER.map_orders_to_array(orders)
          return ORDERS_RESPONSE_DECORATOR.create_orders_response(orders, page_count)
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            return ORDERS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
          rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError => e
            return ORDERS_RESPONSE_DECORATOR.create_response_unauthenticated_user(e.message)
        end
      end
    end
  end
end
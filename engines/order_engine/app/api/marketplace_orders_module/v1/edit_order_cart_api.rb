#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    
    class EditOrderCartApi < BaseOrdersModule::V1::BaseApi

      ORDER_PANEL_SERVICE = MarketplaceOrdersModule::V1::OrderPanelService
      ORDERS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::OrdersResponseDecorator
      ORDER_MAPPER = MarketplaceOrdersModule::V1::OrderMapper
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params)
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing order details]
      #
      # @return [Response] [response to be sent to the user]
      #
      def enact(request)
        begin
          validate_request(request)
          edit_order_cart_request = convert_json_to_request(request)

          order_panel_service = ORDER_PANEL_SERVICE.new(@params)
          response = order_panel_service.transactional_edit_order_cart(edit_order_cart_request)
          edit_order_cart_response = ORDER_MAPPER.edit_order_cart_response_hash(response)
          return ORDERS_RESPONSE_DECORATOR.create_edit_order_cart_response(edit_order_cart_response)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return ORDERS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
          return ORDERS_RESPONSE_DECORATOR.create_not_found_error(e.message)
        rescue CUSTOM_ERROR_UTIL::RunTimeError => e
          return ORDERS_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return ORDERS_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        end
      end

      #
      # Validate incoming request
      #
      # @param request [JSON] [Hash containing order details ]
      #
      # @raise [InsufficientDataError] [if required parameters were not present or blank]
      #
      def validate_request(request)
        if request[:cart].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Cart'})
        end
        cart = request[:cart]
        error_array = []
        error_array.push('Cart id') if cart[:id].blank?
        error_array.push('Order Products Array') if cart[:order_products].blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end

      #
      # Convert JSON request to Request with proper type Objects
      # It converts string attributes to Integer/Decimal as per the requirement
      # Currently, the logic is hard-coded for this API. Need to move this logic to baseApi
      # using resources.reflections.keys and their respective obj.class
      #
      # @param request [JSON] [Hash of request]
      #
      # @return [JSON] [Request with proper type objects]
      #
      def convert_json_to_request(request)
        edit_order_cart_request = {}
        cart = request[:cart]
        order_products = cart[:order_products]
        op_array = []
        order_products.each do |op|
          product_hash = op[:product]
          additional_discount = BigDecimal.new('0.0')
          if op[:additional_discount].present?
            additional_discount = op[:additional_discount].to_d
            additional_discount = (-1 * additional_discount) if additional_discount < 0
          end
          op_array.push({
            quantity: op[:quantity].to_i,
            additional_discount: additional_discount,
            product: {
              id: product_hash[:id].to_i
            }
          })
        end
        edit_order_cart_request = {
          id: request[:id].to_i,
          cart: {
            id: cart[:id].to_i,
            order_products: op_array
          }
        }
        return edit_order_cart_request
      end

    end # End of class
  end
end
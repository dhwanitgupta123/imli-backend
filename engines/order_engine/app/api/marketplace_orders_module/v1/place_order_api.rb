#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    
    class PlaceOrderApi < BaseOrdersModule::V1::BaseApi

      ORDER_SERVICE = MarketplaceOrdersModule::V1::OrderService
      ORDERS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::OrdersResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params)
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details]
      # Request:
      #  - order
      #   {
      #     - cart
      #       * id : Cart id for which order is to be placed
      #     - address
      #       * id : Address in which order is to be delivered
      #   }
      #
      # @return [Response] [response to be sent to the user]
      #
      def enact(request)
        begin
          validate_request(request)
          order_service = ORDER_SERVICE.new(@params)
          # Need to add delivery cost dynamically in Mapper/Helper/Decorator
          response = order_service.transactional_place_order(request)
          return ORDERS_RESPONSE_DECORATOR.create_order_and_payment_details_response(response)
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            return ORDERS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
          rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
            return ORDERS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
          rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
            return ORDERS_RESPONSE_DECORATOR.create_not_found_error(e.message)
          # If unable to change state of CART (PreConditionRequired), then its a Internal Server Error
          rescue CUSTOM_ERROR_UTIL::RunTimeError, CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
            return ORDERS_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        end
      end

      # 
      # Validate incoming request
      #
      # @param request [JSON] [Hash containing cart id and address id ]
      #
      # @raise [InsufficientDataError] [if cart id or address id not present]
      #
      def validate_request(request)
        error_array = []
        error_array.push('Cart id') if request[:cart].blank? || request[:cart][:id].blank?
        error_array.push('Address id') if request[:address].blank? || request[:address][:id].blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end
    end
  end
end
#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # GetAggregatedProducts api, it calls the
    # order_panel service and return the JsonResponse
    #
    class GetAggregatedProductsApi < BaseOrdersModule::V1::BaseApi

      ORDER_PANEL_SERVICE = MarketplaceOrdersModule::V1::OrderPanelService
      ORDER_MAPPER = MarketplaceOrdersModule::V1::OrderMapper
      ORDERS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::OrdersResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      def initialize(params)
        @params = params
      end


      #
      # Return all the aggregated products
      #
      # @return [JsonResponse]
      #
      def enact(request)
        begin
          order_panel_service = ORDER_PANEL_SERVICE.new(@params)
          aggregated_products_array = order_panel_service.get_aggregated_products(request)
          return ORDERS_RESPONSE_DECORATOR.create_aggregated_products_response(aggregated_products_array)
        end
      end
    end
  end
end
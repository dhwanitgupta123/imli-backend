#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    
    class EditOrderAddressApi < BaseOrdersModule::V1::BaseApi

      ORDER_PANEL_SERVICE = MarketplaceOrdersModule::V1::OrderPanelService
      ORDERS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::OrdersResponseDecorator
      ORDER_MAPPER = MarketplaceOrdersModule::V1::OrderMapper
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params)
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing order details]
      #
      # @return [Response] [response to be sent to the user]
      #
      def enact(request)
        begin
          validate_request(request)

          order_panel_service = ORDER_PANEL_SERVICE.new(@params)
          response = order_panel_service.edit_order_address(request)
          edit_order_address_response = ORDER_MAPPER.edit_order_address_response_hash(response)
          return ORDERS_RESPONSE_DECORATOR.create_edit_order_address_response(edit_order_address_response)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return ORDERS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
          return ORDERS_RESPONSE_DECORATOR.create_not_found_error(e.message)
        rescue CUSTOM_ERROR_UTIL::RunTimeError => e
          return ORDERS_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return ORDERS_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        end
      end

      #
      # Validate incoming request
      #
      # @param request [JSON] [Hash containing order details ]
      #
      # @raise [InsufficientDataError] [if required parameters were not present or blank]
      #
      def validate_request(request)
        if request[:address].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Preferred Slot'})
        end
        address = request[:address]
        error_array = []
        error_array.push('id') if address[:id].blank?
        error_array.push('Nickname') if address[:nickname].blank?
        error_array.push('Address Line 1') if address[:address_line1].blank?
        error_array.push('Address Line 2') if address[:address_line2].blank?
        error_array.push('Landmark') if address[:landmark].blank?
        error_array.push('Recipient Name') if address[:recipient_name].blank?
        error_array.push('Pincode') if address[:pincode].blank?

        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end

    end # End of class
  end
end
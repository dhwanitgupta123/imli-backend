#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # GetPackingDetailsByOrderIdApi api, it calls the
    # order_panel service and return the JsonResponse
    #
    class GetPackingDetailsByOrderIdApi < BaseOrdersModule::V1::BaseApi

      ORDER_PANEL_SERVICE = MarketplaceOrdersModule::V1::OrderPanelService
      ORDERS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::OrdersResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      def initialize(params)
        @params = params
      end

      #
      # Return packing details S3 link for an order_id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        begin
          validate_request(request)
          order_panel_service = ORDER_PANEL_SERVICE.new(@params)
          order_packing_details = order_panel_service.get_packing_details_by_order_id(request[:order_id])

          return ORDERS_RESPONSE_DECORATOR.create_order_packing_details_response(order_packing_details)
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            return ORDERS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
          rescue CUSTOM_ERROR_UTIL::RecordNotFoundError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
            return ORDERS_RESPONSE_DECORATOR.create_not_found_error(e.message)
        end
      end

      def validate_request(request)
        raise @custom_error_util::InvalidArgumentsError.new(CONTENT::REQUEST_BLANK) if request.blank?
        raise @custom_error_util::InvalidArgumentsError.new(CONTENT::ORDER_ID_MISSING) if request[:order_id].blank?
      end
    end
  end
end

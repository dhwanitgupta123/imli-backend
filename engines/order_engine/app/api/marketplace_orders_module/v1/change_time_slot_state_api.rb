#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    #
    # AddTimeSlot api to add new time slot
    #
    class ChangeTimeSlotStateApi < BaseOrdersModule::V1::BaseApi

      TIME_SLOT_SERVICE = MarketplaceOrdersModule::V1::TimeSlotService
      TIME_SLOTS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::TimeSlotsResponseDecorator
      TIME_SLOT_MAPPER = MarketplaceOrdersModule::V1::TimeSlotMapper
      TIME_SLOT_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::TimeSlotEvents

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details
      #
      # @return [JSON] [response to be sent to the user]
      #
      def enact(request)
        begin
          validate_request(request)
          time_slot_service = TIME_SLOT_SERVICE.new(@params)
          response = time_slot_service.change_time_slot_state(request)
          change_time_slot_state_response = TIME_SLOT_MAPPER.change_time_slot_state_response_hash(response)
          return TIME_SLOTS_RESPONSE_DECORATOR.create_change_time_slot_state_response(change_time_slot_state_response)

        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError, CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return TIME_SLOTS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
          return TIME_SLOTS_RESPONSE_DECORATOR.create_not_found_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:event].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == TIME_SLOT_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end

    end
  end
end

#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    #
    # GetTimeSlot api to fetch time slot
    #
    class GetTimeSlotApi < BaseOrdersModule::V1::BaseApi

      TIME_SLOT_SERVICE = MarketplaceOrdersModule::V1::TimeSlotService
      TIME_SLOTS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::TimeSlotsResponseDecorator
      TIME_SLOT_MAPPER = MarketplaceOrdersModule::V1::TimeSlotMapper

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details
      #
      # @return [JSON] [response to be sent to the user]
      #
      def enact(request)
        begin
          validate_request(request)
          time_slot_service = TIME_SLOT_SERVICE.new(@params)
          response = time_slot_service.get_time_slot(request)
          get_time_slot_response = TIME_SLOT_MAPPER.get_time_slot_response_hash(response)
          return TIME_SLOTS_RESPONSE_DECORATOR.create_get_time_slot_response(get_time_slot_response)

        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return TIME_SLOTS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return TIME_SLOTS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
          return TIME_SLOTS_RESPONSE_DECORATOR.create_not_found_error(e.message)
        rescue CUSTOM_ERROR_UTIL::RunTimeError, CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return TIME_SLOTS_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        error_array = []
        error_array.push('TimeSlot id') if request[:id].blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end

    end
  end
end

#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    #
    # Update order context api, it validates the request, call the
    # Order panel service and return the JsonResponse
    #
    class UpdateOrderContextApi < BaseOrdersModule::V1::BaseApi
      ORDERS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::OrdersResponseDecorator
      CONTENT = CommonModule::V1::Content
      ORDER_CONTEXT_MAPPER = MarketplaceOrdersModule::V1::OrderContextMapper
      ORDER_CONTEXT_SERVICE = MarketplaceOrdersModule::V1::OrderContextService

      def initialize(params) 
        @params = params
        super
      end

      #
      # Function takes input corresponding to update order context and returns the
      # success or error response
      #
      # @param request [Hash] Request object, Order updaate context params
      #
      # @return [JsonResponse]
      #
      _ExceptionHandler_
      def enact(request)
        validate_request(request)
        order_context_service = ORDER_CONTEXT_SERVICE.new(@params)
        order_context = order_context_service.record_order_context(request)
        order_context_hash = ORDER_CONTEXT_MAPPER.map_order_context_to_hash(order_context)
        return ORDERS_RESPONSE_DECORATOR.create_order_context_response(order_context_hash)
      end

      private

      #
      # Validates the required params update context
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:order_id].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::ORDER_ID_MISSING)
        end
      end
    end
  end
end

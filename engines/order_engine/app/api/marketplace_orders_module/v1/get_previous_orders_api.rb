#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    
    class GetPreviousOrdersApi < BaseOrdersModule::V1::BaseApi

      ORDER_SERVICE = MarketplaceOrdersModule::V1::OrderService
      ORDERS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::OrdersResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params)
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @return [Response] [response to be sent to the user]
      #
      def enact
        begin
          order_service = ORDER_SERVICE.new(@params)
          # Need to add delivery cost dynamically in Mapper/Helper/Decorator
          response = order_service.get_previous_orders
          return ORDERS_RESPONSE_DECORATOR.create_short_orders_response_with_minified_states(response)
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            return ORDERS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
          rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
            return ORDERS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
          rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
            return ORDERS_RESPONSE_DECORATOR.create_not_found_error(e.message)
          # If unable to change state of CART (PreConditionRequired), then its a Internal Server Error
          rescue CUSTOM_ERROR_UTIL::RunTimeError, CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
            return ORDERS_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        end
      end

    end
  end
end
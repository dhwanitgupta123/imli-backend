module MarketplaceOrdersModule

  module V1

    class TimeSlotService < BaseOrdersModule::V1::BaseService

      TIME_SLOT_DAO = MarketplaceOrdersModule::V1::TimeSlotDao
      TIME_SLOT_HELPER = MarketplaceOrdersModule::V1::TimeSlotHelper
      DATE_TIME_UTIL = CommonModule::V1::DateTimeUtil
      TIME_SLOT_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::TimeSlotEvents

      MIN_SLOT_OFFSET = CACHE_UTIL.read_int('MIN_SLOT_OFFSET') || 24
      MAX_SLOT_OFFSET = CACHE_UTIL.read_int('MAX_SLOT_OFFSET') || 72

      #
      # Initialize TimeSlot service with passed params
      #
      def initialize(params = {})
        @params = params
      end

      #
      # Get available slots for ORDER
      #
      # @param order [Object] [Order whose available slots are to be computed]
      # 
      # @return [Array] [Array of the available slots]
      #
      def get_available_slots_for_order(order)
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        # Current time in UTC
        current_time = DATE_TIME_UTIL.get_current_time(false) # true because needed in UTC
        active_slots = time_slot_dao.get_all_active_slots
        start_available_time = current_time + DATE_TIME_UTIL.convert_to_seconds(MIN_SLOT_OFFSET)
        final_available_time = current_time + DATE_TIME_UTIL.convert_to_seconds(MAX_SLOT_OFFSET)

        # Date iterator starting from MIN date to MAX date
        date_iterator = start_available_time.to_date
        available_slots = []
        while date_iterator <= final_available_time.to_date
          per_day_hash = {}
          # Insert EPOCH date into the response hash
          per_day_hash['slot_date'] = DATE_TIME_UTIL.convert_date_time_to_epoch(date_iterator)
          per_day_hash['slot_date_label'] = date_iterator.strftime('%a , %d %b %Y')  # Keep it as per format
          time_slots_array = []
          active_slots.each do |slot|
            slot_hash = TIME_SLOT_HELPER.get_time_slot_hash(slot)

            # first day, slot lying between start_time is ignored.
            # last day, slot lying between final_time is accepted
            if (date_iterator == start_available_time.to_date && DATE_TIME_UTIL.compare(slot.from_time, start_available_time) == -1) || 
              ( date_iterator == final_available_time.to_date && DATE_TIME_UTIL.compare(slot.from_time, final_available_time) == 1)
              slot_hash['available'] = false
            else
              slot_hash['available'] = true
            end
            time_slots_array.push(slot_hash)
          end
          per_day_hash['time_slots'] = time_slots_array
          available_slots.push(per_day_hash)
          date_iterator = date_iterator.next_day
        end
        return available_slots
      end

      #
      # Add time slot
      #
      # @param time_slot_params [JSON] [Hash containing time slot arguments]
      #
      # @return [Object] [created Time slot]
      #
      def add_time_slot(time_slot_params)
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        from_time = DATE_TIME_UTIL.convert_epoch_to_time(time_slot_params[:from_time].to_i)
        to_time = DATE_TIME_UTIL.convert_epoch_to_time(time_slot_params[:to_time].to_i)
        time_slot = time_slot_dao.create_time_slot({
          from_time: from_time,
          to_time: to_time,
          label: time_slot_params[:label]
          })
        return time_slot
      end

      #
      # Update time slot
      # LOGIC:: If from_time and to_time are changed, then we should move old
      # time slot to DELETED state and create a new one with updated params
      #
      # @param time_slot_params [JSON] [Hash of time slot]
      #
      # @return [Object] [updated time slot]
      #
      def update_time_slot(time_slot_params)
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        id = time_slot_params[:id]
        time_slot = time_slot_dao.get_by_id(id)
        changed_from_time = DATE_TIME_UTIL.convert_epoch_to_time(time_slot_params[:from_time].to_i)
        changed_to_time = DATE_TIME_UTIL.convert_epoch_to_time(time_slot_params[:to_time].to_i)
        # If from time and to time are changed, then SOFT_DELETE the old one
        # and create a new one with updated params
        if time_slot.from_time != changed_from_time || time_slot.to_time != changed_to_time
          new_time_slot = time_slot.dup
          soft_delete_time_slot(time_slot)
          time_slot = new_time_slot
        end
        time_slot = time_slot_dao.update_time_slot({
          from_time: changed_from_time,
          to_time: changed_to_time,
          label: time_slot_params[:label]
          }, time_slot)
        return time_slot
      end

      # 
      # Get time slot
      #
      # @param time_slot_params [JSON] [Hash containing time slot id]
      # 
      # @return [Object] [time slot]
      #
      def get_time_slot(time_slot_params)
        time_slot_id = time_slot_params[:id]
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        time_slot = time_slot_dao.get_by_id(time_slot_id)
        return time_slot
      end

      # 
      # Get all filtered and paginated time slots
      #
      # @param time_slot_params [JSON] [Hash containing pagination params]
      # 
      # @return [Array] [Array of time slot objects]
      #
      def get_all_time_slots(pagination_params = {})
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        pagination_params = pagination_params.merge({
          sort_by: 'from_time',
          order: 'ASC'
          })
        time_slots = time_slot_dao.get_all(pagination_params)
        return time_slots
      end

      # 
      # Change state of time slot
      #
      # @param time_slot_params [JSON] [Hash containing time slot id and event]
      # 
      # @return [Object] [corresponding time slot]
      #
      def change_time_slot_state(args)
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        change_state(args, time_slot_dao, 'Time Slot')
      end

      # 
      # SOFT DELETE time slot by changing its state
      #
      # @param time_slot_params [Object] [time slot which is to be deleted]
      # 
      # @return [Object] [corresponding time slot]
      #
      def soft_delete_time_slot(time_slot)
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        time_slot_dao.change_state(time_slot, TIME_SLOT_EVENTS::SOFT_DELETE)
      end

      #
      # Check whether current time lies between the given slot
      #
      # @param slot [Object] [Time slot]
      # @param time [Time] [time which is to be checked]
      #
      # @return [Boolean] [true is time lies in slot else false]
      #
      def time_lies_within_slot(slot, time)
        return true if DATE_TIME_UTIL.compare(time, slot.from_time) == 1 && DATE_TIME_UTIL.compare(time, slot.to_time) == -1
        return false
      end

    end
  end
end
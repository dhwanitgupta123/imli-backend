module MarketplaceOrdersModule

  module V1

    class PaymentService < BaseOrdersModule::V1::BaseService
    # Including Transaction Helper library
    require 'transaction_helper'
      #
      # initialize CartsService Class
      #
      USER_SERVICE = UsersModule::V1::UserService
      ORDER_NOTIFICATION_SERVICE = MarketplaceOrdersModule::V1::OrderNotificationService
      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper
      EMAIL_TYPE = CommunicationsModule::V1::Mailers::V1::EmailType
      CART_DAO = MarketplaceOrdersModule::V1::CartDao
      ORDER_DAO = MarketplaceOrdersModule::V1::OrderDao
      PAYMENT_DAO = MarketplaceOrdersModule::V1::PaymentDao
      ADDRESS_DAO = AddressModule::V1::AddressDao
      MEMBERSHIP_SERVICE = UsersModule::V1::MembershipService
      PAYMENT_HELPER = MarketplaceOrdersModule::V1::PaymentHelper
      CART_HELPER = MarketplaceOrdersModule::V1::CartHelper
      INVOICE_HELPER = MarketplaceOrdersModule::V1::InvoiceHelper
      PROFILE_SERVICE = UsersModule::V1::ProfileService
      ORDER_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::OrderEvents
      CART_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::CartEvents
      PAYMENT_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::PaymentEvents
      PAYMENT_MODES = MarketplaceOrdersModule::V1::PaymentModes
      TRANSACTION_STATUS = TransactionsModule::V1::TransactionStatus

      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      CACHE_UTIL = CommonModule::V1::Cache
      PLATFORM_TYPE_UTIL = CommonModule::V1::PlatformType

      # Mailer
      ORDER_EMAIL_WORKER = CommunicationsModule::V1::Mailers::V1::OrderEmailWorker
      PAYMENT_INSTANCE = MarketplaceOrdersModule::V1::PaymentInstances
      # Shield service to fetch FEATURE values (static & user specific)
      SHIELD_SERVICE = SettingsModule::V1::ShieldService
      ORDER_CONTEXT_SERVICE = MarketplaceOrdersModule::V1::OrderContextService

      #
      # Initialize PAYMENT service with passed params
      #
      def initialize(params)
        @params = params
      end

      #
      # Execute Initiate Payment functionality with Transactional block.
      # If anything fails, payment, and its order will come back to its previous status
      #
      def transactional_initiate_payment(initiate_payment_params)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return initiate_payment(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: initiate_payment_params
          })
        transaction_block.run();
      end

      #
      # Execute Payment Successfull functionality with Transactional block.
      # If anything fails, payment and its order will come back to its previous status
      #
      def transactional_payment_successful(payment_params)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return offline_payment_successful(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: payment_params
          })
        transaction_block.run();
      end

      #
      # Initiate payment
      # Required Parameters:
      #   - id: order id
      #   - payment:
      #     - mode: mode of payment
      #
      # @param payment_params [JSON] [Hash of payment parameters containing payment_id]
      #
      # @return [Object] [Payment object which is succeeded]
      #
      # @raise [InvalidArgumentsError] [if payment is not in pending state or No active order associated with that payment]
      # @raise [ResourceNotFoundError] [if no active cart associated with requested order]
      #
      def initiate_payment(initiate_payment_params)
        # 1. Validate payment and Order parameters
        # 2. Create Payment object as per request parameters
        # 3. Validate Order and link it with the payment
        # 4. Return Payment object
        user_service = USER_SERVICE.new(@params)
        payment_dao = PAYMENT_DAO.new(@params)

        order_id = initiate_payment_params[:id]
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])

        initiated_order = get_users_initiated_order_with_id(user, order_id)
        if initiated_order.cart.blank?
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT_UTIL::NO_ACTIVE_CART)
        end

        final_payment_details = validate_payment_details(initiate_payment_params[:payment], user, initiated_order.cart)

        payment = create_payment_for_order(initiated_order, final_payment_details)

        return { payment: payment }
      end

      #
      # Payment Successful for OFFLINE payments
      # Parameters:
      #   - id: payment id
      #
      # @param payment_params [JSON] [Hash of payment parameters containing payment_id]
      #
      # @return [Object] [Payment object which is succeeded]
      #
      # @raise [InvalidArgumentsError] [if payment is not in pending state or No active order associated with that payment]
      #
      def offline_payment_successful(payment_params)
        return nil if payment_params.blank?
        user_service = USER_SERVICE.new(@params)

        payment_id = payment_params[:id]
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])

        payment = validate_payment_belongs_to_user(user, payment_id)
        if !is_payment_mode_offline?(payment)
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_PAYMENT_MODE)
        end
        if payment.blank? || !payment.pending?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::NO_PENDING_PAYMENT)
        end
        ApplicationLogger.debug('Offline Payment successful for payment id: ' + payment.id.to_s)
        payment = place_order_and_notify_user(user, payment)
        return { payment: payment }
      end

      #
      # Handle payment response.
      # Depending on the transaction status, it should call internal
      # payment_success or payment_failed methods to perform certain specific operations
      #
      # @param transaction_status [Integer] [status of the payment transaction]
      # @param args = '' [JSON] [optional Hash which contains extra parameters like payment_id, user_id]
      #
      # @return [Object] [Payment object with respect to passed id]
      #
      def handle_payment(transaction_status, args = '')
        if args.present? && args[:payment_id].present?
          ApplicationLogger.debug('Handle online Order payment with status: ' + transaction_status.to_s + ' , for payment id: ' + args[:payment_id].to_s)
        else
          ApplicationLogger.debug('Transaction callback received with NIL payment id for transaction: ' + transaction_status.to_s)
        end
        # Route to proper handler based upon transaction status
        case transaction_status.to_i
        when TRANSACTION_STATUS::SUCCESS
          # Call payment_successful
          return payment_successful(args)
        when TRANSACTION_STATUS::FAILED
          # Call payment_failed
          return payment_failed(args)
        when TRANSACTION_STATUS::CANCELLED
          # For now, call payment failed itself
          return payment_failed(args)
        end
      end

      def handle_refund(transaction_status, args = '')
        # TO-DO: Handle Order Refunds
      end

      #
      # Change payment status as per the transaction status for POST_ORDER
      #
      # @param transaction_status [Integer] [status of the payment transaction]
      # @param args = '' [JSON] [optional Hash which contains extra parameters like payment_id]
      #
      # @return [Object] [Payment object with respect to passed id]
      #
      def handle_post_order_payment(transaction_status, args = '')
        payment_dao = PAYMENT_DAO.new(@params)
        if args.present? && args[:payment_id].present?
          ApplicationLogger.debug('Handle POST_ORDER Order payment with status: ' + transaction_status.to_s + ' , for payment id: ' + args[:payment_id].to_s)
        else
          ApplicationLogger.debug('Transaction callback received with NIL payment id for transaction: ' + transaction_status.to_s)
        end
        # Validations for valid payment
        payment = payment_dao.get_payment_from_id(args[:payment_id])
        if payment.blank? || !payment.pending?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::NO_PENDING_PAYMENT)
        end
        if payment.blank? || payment.order.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::NO_ORDER_FOR_PAYMENT)
        end
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::NO_POST_ORDER_PAYMENT) unless is_payment_mode_offline?(payment)
        # If payment mode got changed, then move previous payment to DISCARD state
        # and create a new payment with requested mode. Change status of new payment object
        mode = args[:mode]
        if mode.present? && mode != payment.mode
          ApplicationLogger.info('Post order payment mode changed, hence discarding old payment. Old mode: ' + payment.mode.to_s + ' , new mode: ' + mode.to_s)
          payment = payment_dao.discard_old_payment_and_create_new(payment, { mode: mode})
        end
        # Change state of payment based on transaction status
        case transaction_status.to_i
          when TRANSACTION_STATUS::SUCCESS
            payment = payment_dao.change_state(payment, PAYMENT_EVENTS::PAYMENT_SUCCESS)
            ApplicationLogger.debug('POST ORDER Payment successful for payment id: ' + payment.id.to_s)
          when TRANSACTION_STATUS::FAILED
            payment = payment_dao.change_state(payment, PAYMENT_EVENTS::PAYMENT_FAIL)
          when TRANSACTION_STATUS::CANCELLED
            payment = payment_dao.change_state(payment, PAYMENT_EVENTS::PAYMENT_FAIL)
        end
        return { payment: payment }
      end


      #
      # Payment Failed. Called when payment of that order failed
      # It should change payment state to FAILED
      #
      # Parameters hash:
      #   - payment_id: payment id
      #   - user_id: id of user for which payment is to be made
      #
      # @param payment_params [JSON] [Hash of payment parameters containing payment_id]
      #
      # @return [Object] [Payment object which is succeeded]
      #
      # @raise [InvalidArgumentsError] [if no payment exists with respect to passed id OR payment is not in pending state]
      #
      def payment_failed(payment_params)
        order_notification_service = ORDER_NOTIFICATION_SERVICE.new
        return nil if payment_params.blank?
        user_service = USER_SERVICE.new(@params)
        payment_dao = PAYMENT_DAO.new(@params)
        payment_id = payment_params[:payment_id]
        user_id = payment_params[:user_id]
        user = user_service.get_user_from_user_id(user_id)

        payment = validate_payment_belongs_to_user(user, payment_id)
        # payment = payment_dao.get_payment_from_id(payment_id)
        if payment.blank? || !payment.pending?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::NO_PENDING_PAYMENT)
        end
        ApplicationLogger.debug('Payment failed for payment id: ' + payment.id.to_s)
        # Change payment state to FAILED
        payment = payment_dao.change_state(payment, PAYMENT_EVENTS::PAYMENT_FAIL)
        # Muting communication for now until content is finalized
        # order_notification_service.trigger_user_communication(payment.order, ORDER_EVENTS::PAYMENT_FAILED)
        return { payment: payment }
      end

      #
      # Payment Successfull
      # Parameters:
      #   - payment_id: payment id
      #   - user_id: id of user for which payment is to be made
      #
      # @param payment_params [JSON] [Hash of payment parameters containing payment_id]
      #
      # @return [Object] [Payment object which is succeeded]
      #
      # @raise [InvalidArgumentsError] [if payment is not in pending state or No active order associated with that payment]
      #
      def payment_successful(payment_params)
        return nil if payment_params.blank?
        user_service = USER_SERVICE.new(@params)

        payment_id = payment_params[:payment_id]
        user_id = payment_params[:user_id]
        user = user_service.get_user_from_user_id(user_id)

        payment = validate_payment_belongs_to_user(user, payment_id)
        if !payment.pending?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::NO_PENDING_PAYMENT)
        end
        ApplicationLogger.debug('Payment successful for payment id: ' + payment.id.to_s)
        payment = place_order_and_notify_user(user, payment)
        return { payment: payment }
      end

      #
      # Send order to placed state
      # Send CART to checked out state
      # Notify user by different means
      #
      # @param user [Object] [User whom to be notified]
      # @param payment [Object] [Payment of the Order which is to be placed]
      #
      def place_order_and_notify_user(user, payment)
        order_notification_service = ORDER_NOTIFICATION_SERVICE.new(@params)
        payment_dao = PAYMENT_DAO.new(@params)
        order_dao = ORDER_DAO.new(@params)
        cart_dao = CART_DAO.new(@params)
        order_context_service = ORDER_CONTEXT_SERVICE.new(@params)

        order = payment.order
        ApplicationLogger.debug('Payment successful for Order' + order.id.to_s + ' , with payment id: ' + payment.id.to_s)
        # Move Order to placed state and Payment to success state
        if order.present? && order.initiated?
          payment_response = update_payment_mode(payment)
          payment = payment_response[:payment]
          is_offline_payment = payment_response[:is_offline_payment]
          order_dao.change_state(order, ORDER_EVENTS::PLACE)
          # Record order date while placing order
          order = order_dao.record_placed_date(order, Time.zone.now)
          # Record order context, that is this order is placed by which medium, platform, app version (if any)
          order_context_service.record_order_context({
                order_id: order.id, 
                app_version: USER_SESSION[:app_version], 
                platform_type: PLATFORM_TYPE_UTIL.get_id_by_type(USER_SESSION[:platform_type])

              })
          
          ApplicationLogger.debug('Order ' + order.id.to_s + ' placed with payment id: ' + payment.id.to_s + ' and mode: ' + payment.mode.to_s)
        else
          ApplicationLogger.debug('Order is not initiated and payment successful called user_id: ' + user.id.to_s)
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::NO_ACTIVE_ORDER)
        end
        if order.cart.present? && !order.cart.checked_out?
          cart_dao.change_state(order.cart, CART_EVENTS::CHECKOUT)
          ApplicationLogger.debug('Cart ' + order.cart.id.to_s + ' moved to checked out state')
        else
          ApplicationLogger.debug('CART is not active and payment successful called user_id: ' + user.id.to_s)
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::NO_ACTIVE_CART)
        end

        begin
          # function call to update user's total savings
          update_user_total_savings(user, payment.total_savings)
          # Activate membership if first order placed
          if first_order_placed?(user)
            first_order_success_tasks(user)
          end
          # Send mail regarding new ORDER
          # order_notification_service = ORDER_NOTIFICATION_SERVICE.new(@params)
          inform_order_placed_to_user(order)
          sms_order_placed(order)
          notify_order_placed(order)
          if SHIELD_SERVICE.get_feature_value('ALLOW_INVOICE_MAIL_TO_IMLI') == '1'
            inform_order_placed_to_imli_team(order)
          end
          if SHIELD_SERVICE.get_feature_value('ALLOW_PACKING_MAIL') == '1'
            order_notification_service.inform_order_details_to_packer(order)
          end
          # ativate the Ample-Sample free trial  membership if it's a first order
        rescue => e
          ApplicationLogger.debug('Problem occured in notifying User:' + user.id.to_s)
        end
        return payment
      end

      #
      # Function to check if it was User's first order
      #
      def first_order_placed?(user)
        order_dao = ORDER_DAO.new(@params)
        return order_dao.first_order_placed?(user)
      end

      def first_order_success_tasks(user)
        membership_service  = MEMBERSHIP_SERVICE.new(@params)
        membership_service.activate_ample_sample_membership(user)
      end

      #
      # Validate payment belongs to current user or not
      #
      # @param user [Object] [User for which payment is to be made]
      # @param payment_id [Integer] [payment id]
      #
      # @return [Object] [Valid Payment object associated with the user]
      #
      # @raise [InvalidArgumentsError] [If payment not associated with any order or valid user]
      #
      def validate_payment_belongs_to_user(user, payment_id)
        payment_dao = PAYMENT_DAO.new(@params)
        payment = payment_dao.get_payment_from_id(payment_id)
        if payment.blank? || payment.order.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::NO_ORDER_FOR_PAYMENT)
        end
        if payment.order.user.blank? || payment.order.user != user
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::NOT_VALID_PAYMENT)
        end
        return payment
      end

      #
      # Inform Order placed to the user. Send email to the user's primary mail id
      # with complete invoice
      #
      # @param order [Object] [Order which is placed]
      #
      def inform_order_placed_to_user(order)
        begin
          mail_attributes = INVOICE_HELPER.get_invoice_hash_for_invoice_generation(order)
          first_name = order.user.first_name if order.user.present?
          email = USER_SERVICE_HELPER.get_primary_mail(order.user)
          email_subject = CACHE_UTIL.read('PLACED_ORDER_SUBJECT') % {order_id: order.order_id}
          pdf_name = CACHE_UTIL.read('INVOICE_ATTACHMENT_NAME') % {order_id: order.order_id}
          if email.present?
            mail_attributes = mail_attributes.merge({
              email_type: EMAIL_TYPE::PLACED_ORDER[:type],
              subject: email_subject.to_s,
              to_first_name: first_name,
              to_email_id: email.email_id,
              pdf_needed: true,
              pdf_name: pdf_name
              })
            ORDER_EMAIL_WORKER.perform_async(mail_attributes)
          end
        rescue => e
          ApplicationLogger.debug('Failed to inform placed order to IMLI Team : ' + user_id.to_s + ' , ' + e.message)
        end
      end

      #
      # Inform Order placed to the Imli team. Send email to the team's group mail id
      # with complete invoice details of user
      #
      # @param order [Object] [Order which is placed]
      #
      def inform_order_placed_to_imli_team(order)
        begin
          mail_attributes = INVOICE_HELPER.get_invoice_hash_for_invoice_generation(order)
          user_id = order.user.id if order.user.present?
          email_ids = CACHE_UTIL.read('IMLI_TEAM_MAIL_IDS')
          to_name = CACHE_UTIL.read('IMLI_TEAM_NAME')
          email_subject = CACHE_UTIL.read('NEW_ORDER_PLACED_SUBJECT') % {order_id: order.order_id, user_id: user_id}
          pdf_name = CACHE_UTIL.read('INVOICE_ATTACHMENT_NAME') % {order_id: order.order_id}
          if email_ids.present?
            mail_attributes = mail_attributes.merge({
              email_type: EMAIL_TYPE::PLACED_ORDER[:type],
              subject: email_subject.to_s,
              to_first_name: to_name,
              to_email_id: email_ids.to_s,
              multiple: true,
              pdf_needed: true,
              pdf_name: pdf_name
              })
            ORDER_EMAIL_WORKER.perform_async(mail_attributes)
          end
        rescue => e
          ApplicationLogger.debug('Failed to inform placed order to IMLI Team : ' + user_id.to_s + ' , ' + e.message)
        end
      end

      #
      # Function to send Order placed SMS to user
      #
      def sms_order_placed(order)
        begin
          user = order.user
          payment_dao = PAYMENT_DAO.new(@params)
          payment = payment_dao.get_orders_payment_as_per_its_mode(order)
          message_attributes = {
            phone_number: user.phone_number,
            first_name: user.first_name,
            order_savings: payment.total_savings.round(2).to_s,
            order_id: order.order_id,
            net_total: payment.net_total.round(2).to_s
          }
          SMS_WORKER.perform_async(message_attributes, SMS_TYPE::ORDER_PLACED)
        rescue => e
          ApplicationLogger.debug('Failed to send SMS for placed order to user : ' + user.id.to_s + ' , ' + e.message)
        end
      end

      #
      # Function to notify order placed to user
      #
      def notify_order_placed(order)
        begin
          payment_dao = PAYMENT_DAO.new(@params)
          payment = payment_dao.get_orders_payment_as_per_its_mode(order)
          params = {
            username: 'parse_user_' + order.user.id.to_s,
            notification_type: NOTIFICATION_TYPE::PLACED_ORDER[:type],
            order_savings: payment.total_savings.round(2).to_s
          }
          NOTIFICATION_WORKER.perform_async(params)
        rescue => e
          ApplicationLogger.debug('Failed to push notification for placed order to user : ' + user.id.to_s + ' , ' + e.message)
        end
      end

      #
      # Function to update the total savings of User once the order payment is successful
      #
      def update_user_total_savings(user, savings)
        profile_service = PROFILE_SERVICE.new(@params)
        profile_service.update_user_savings(user.profile, savings)
      end

      #
      # Get users order in INITIATED state, which also matches passed order id
      #
      # @param user [type] [description]
      # @param order_id [type] [description]
      #
      # @return [type] [description]
      def get_users_initiated_order_with_id(user, order_id)
        order_dao = ORDER_DAO.new(@params)
        order = order_dao.get_users_initiated_order_with_id(user, order_id)
        if order.blank?
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT_UTIL::NO_ACTIVE_ORDER)
        end
        return order
      end

      #
      # Create payment for order
      #   - 1. Find PENDING payment object associated with order
      #   - 2. If found, then use (return) the same
      #   - 3. If not found, then create a new payment object
      #
      # @param order [Object] [Order for which payment is to be made]
      # @param payment_details [JSON] [Hash of newly computed parameters]
      #
      # @return [Object] [New payment object created]
      #
      def create_payment_for_order(order, final_payment_details)
        payment_dao = PAYMENT_DAO.new(@params)
        initiated_payment = payment_dao.get_pending_payment_for_order(order)
        payment_args = {
          order: order,
          mode: final_payment_details[:mode],
          payment_instance: final_payment_details[:payment_instance],
          delivery_charges: final_payment_details[:delivery_charges],
          grand_total: final_payment_details[:grand_total],
          discount: final_payment_details[:discount],
          billing_amount: final_payment_details[:billing_amount],
          payment_mode_savings: final_payment_details[:payment_mode_savings],
          net_total: final_payment_details[:net_total],
          payable_total: final_payment_details[:payable_total],
          total_savings: final_payment_details[:total_savings]
        }
        if initiated_payment.blank?
          payment = payment_dao.create_new_payment(payment_args)
        else
          payment = payment_dao.update_payment(payment_args, initiated_payment)
        end
        return payment
      end

      #
      # Update MODE of payment
      # Update only if it is ONLINE mode of payment. In case of Offline modes,
      # payment will be moved to SUCCESS state later on
      #
      # @param payment [Object] [Payment object whose mode is to be updated]
      #
      # @return [Object] [updated Payment object]
      #
      def update_payment_mode(payment)
        payment_dao = PAYMENT_DAO.new(@params)
        offline_mode_of_payments = PAYMENT_MODES.get_offline_payment_modes
        is_offline_payment = true
        # Modify payment state only if It is ONLINE payment
        if !offline_mode_of_payments.include?(payment.mode)
          payment = payment_dao.change_state(payment, PAYMENT_EVENTS::PAYMENT_SUCCESS)
          is_offline_payment = false
        end
        return ({payment: payment, is_offline_payment: is_offline_payment})
      end

      #
      # Get whether mode of payment is OFFLINE or not
      #
      # @param payment [Object] [Payment object which is to be checked]
      #
      # @return [Boolean] [true is offline mode, else false]
      #
      def is_payment_mode_offline?(payment)
        return false if payment.blank?
        payment_dao = PAYMENT_DAO.new(@params)
        offline_mode_of_payments = PAYMENT_MODES.get_offline_payment_modes
        # Modify payment state only if It is ONLINE payment
        if offline_mode_of_payments.include?(payment.mode)
          return true
        end
        return false
      end


      #
      # Validate Payment details
      # Match incoming payment object with self computed payment object
      #
      # @param payment_params [JSON] [Hash of the payment params]
      # @param user [Object] [User object]
      # @param cart [Object] [Cart Object]
      #
      # @return [JSON] [Hash containing complete payment details]
      def validate_payment_details(payment_params, user, cart)
        computed_billing_details = PAYMENT_HELPER.fetch_user_billing_details(user, cart)
        # TO-DO: Check whether incoming mode is a valid MODE
        mode_of_payment = payment_params[:mode]
        payment_details_map = PAYMENT_HELPER.fetch_payment_details_hash[mode_of_payment.to_s]
        computed_payment_mode_details = PAYMENT_HELPER.compute_payment_mode_details(payment_details_map, computed_billing_details[:billing_amount], cart.cart_savings)

        error_array = []
        error_array.push('Delivery charges mismatch') if payment_params[:delivery_charges].present? && payment_params[:delivery_charges].to_d != computed_billing_details[:delivery_charges].to_d
        error_array.push('Grand Total mismatch') if payment_params[:grand_total].present? && payment_params[:grand_total].to_d !=  computed_billing_details[:grand_total].to_d
        error_array.push('Discount mismatch') if payment_params[:discount].present? && payment_params[:discount].to_d != computed_billing_details[:discount].to_d
        error_array.push('Billing amount mismatch') if payment_params[:billing_amount].present? && payment_params[:billing_amount].to_d != computed_billing_details[:billing_amount].to_d

        error_array.push('Reward mismatch') if payment_params[:payment_mode_savings].present? && payment_params[:payment_mode_savings].to_d != computed_payment_mode_details[:payment_mode_savings].to_d
        error_array.push('Net total mismatch') if payment_params[:net_total].present? && payment_params[:net_total].to_d != computed_payment_mode_details[:net_total].to_d
        error_array.push('Net savings mismatch') if payment_params[:total_savings].present? && payment_params[:total_savings].to_d != computed_payment_mode_details[:total_savings].to_d

        if error_array.present?
          error_string = error_array.join(', ')
          # Need to LOG into Mongo
          ApplicationLogger.debug('Payment Details Request is malformed, error: ' + error_string.to_s )
        end

        payment_instance = PAYMENT_INSTANCE.get_instance_by_mode(mode_of_payment)

        return {
          payment_instance: payment_instance,
          mode: mode_of_payment,
          delivery_charges: computed_billing_details[:delivery_charges],
          grand_total: computed_billing_details[:grand_total],
          discount: computed_billing_details[:discount],
          billing_amount: computed_billing_details[:billing_amount],
          payment_mode_savings: computed_payment_mode_details[:payment_mode_savings],
          net_total: computed_payment_mode_details[:net_total],
          payable_total: computed_payment_mode_details[:payable_total],
          total_savings: computed_payment_mode_details[:total_savings]
        }
      end

      #
      # @param order [Object] [Order model object]
      #
      # @return [Object] [Payment object]
      #
      def get_orders_payment_as_per_its_mode(order)
        payment_dao = PAYMENT_DAO.new(@params)
        payment = payment_dao.get_orders_payment_as_per_its_mode(order)
        return payment
      end

    end
  end
end

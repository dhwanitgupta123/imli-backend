#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    module MarketplaceCartsModule
      module V1
        # Service for order_products table
        class OrderProductsService < BaseOrdersModule::V1::BaseService
        	ORDER_PRODUCTS_DAO = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsDao
          ORDER_PRODUCTS_EVENTS = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::ModelStates::V1::OrderProductEvents
          MARKETPLACE_SELLING_PACK_DAO = MarketplaceProductModule::V1::MarketplaceSellingPackDao

          #
          # Initialize OrderProduct service with passed params
          #
          def initialize(params='')
            @params = params
          end

          # 
          # Create Order products
          # @param params [JSON] [Hash with cart, quantity and MPSP details]
          # 
          # @return [Object] [Order product which is created]
        	def create_order_products(params)
            final_price_details = get_final_price_for_order_product(params)
            params = params.merge({final_price: final_price_details[:final_price], additional_discount: final_price_details[:additional_discount]})
        		order_products_dao = ORDER_PRODUCTS_DAO.new
        		order_product = order_products_dao.create_order_products(params)
        	end

          # 
          # Add MPSP to cart by updating quantity of that MPSP in OrderProduct
          #
          # @param [order_product] [order product which is to be updated]
          # @param params [JSON] [Hash containing all required args(quantity, MPSP)]
          # 
          # @return [type] [description]
        	def add_mpsp_to_cart(order_product, params)
            final_price_details = get_final_price_for_order_product(params)
            params = params.merge({final_price: final_price_details[:final_price], additional_discount: final_price_details[:additional_discount]})
        		order_products_dao = ORDER_PRODUCTS_DAO.new
        		order_product = order_products_dao.update(params, order_product)
        	end

          #
          # Disable Order Product by deactivating it and making its quantity 0
          #
          # @param order_product [Object] [OrderProduct which is to be disabled]
          #
          def disable_order_product(order_product)
            order_products_dao = ORDER_PRODUCTS_DAO.new
            event = ORDER_PRODUCTS_EVENTS::DEACTIVATE
            order_product.quantity = 0
            unless order_product.inactive?
              order_product = order_products_dao.change_state(order_product, event)
            end
          end

          # 
          # Get active order products in multiple carts requested
          # @param carts [Array] Array containing multiple Cart ids
          # 
          # @return [ActiveRecord list] order_products as requested
          def get_active_order_products_in_carts(carts)
            order_products_dao = ORDER_PRODUCTS_DAO.new
            active_unique_order_products = order_products_dao.get_active_order_products_in_carts(carts)
            return active_unique_order_products
          end

          #
          # Compute final price of Order Product from its selling price and other discounts
          #
          # @param params [JSON] [Hash containing attributes of order product]
          #
          # Must params::
          #   selling_price: Selling price of the product
          #   additional_discount: Additional discount to be applied on that product
          #   quantity: quantity of the product
          #
          # @return [Decimal] [computed final price of order product]
          #
          def get_final_price_for_order_product(params)
            selling_price = params[:selling_price] || BigDecimal.new('0')
            additional_discount = params[:additional_discount] || BigDecimal.new('0')
            quantity = params[:quantity]
            # Compute final price
            total_selling_price = quantity * selling_price.to_d
            final_price = total_selling_price - additional_discount.to_d
            if final_price.to_i <= 0
              additional_discount = total_selling_price
              final_price = BigDecimal.new('0')
            end
            return {final_price: final_price, additional_discount: additional_discount}
          end

          #
          # Function to recompute order products pricing
          #
          def recompute_order_products(order_products)
            order_products_dao = ORDER_PRODUCTS_DAO.new
            marketplace_selling_pack_dao = MARKETPLACE_SELLING_PACK_DAO.new
            order_products.each do |order_product|
              mpsp = order_product.marketplace_selling_pack
              quantity = order_product.quantity
              if mpsp.active? && quantity > 0
                if quantity > mpsp.max_quantity
                  ApplicationLogger.debug("Order product quantity: " + quantity.to_s + " is greater than max_quantity: " + mpsp.max_quantity.to_s)
                  quantity = mpsp.max_quantity
                end
                pricing = marketplace_selling_pack_dao.get_savings_and_pricing_for_mpsp(quantity, mpsp)
                # Compute final price of order product, along with its additional discount
                computed_final_price = get_final_price_for_order_product({
                  quantity: quantity,
                  selling_price: pricing[:selling_price],
                  additional_discount: order_product.additional_discount
                })
                args = {
                  mrp: pricing[:mrp],
                  selling_price: pricing[:selling_price],
                  savings: pricing[:savings],
                  vat: pricing[:vat],
                  service_tax: pricing[:service_tax],
                  taxes: pricing[:taxes],
                  discount: pricing[:discount],
                  quantity: quantity,
                  additional_discount: computed_final_price[:additional_discount],
                  final_price: computed_final_price[:final_price]
                }
                order_product = order_products_dao.update(args, order_product)
              else
                disable_order_product(order_product)
              end
            end
          end
       	end
      end
    end
  end
end
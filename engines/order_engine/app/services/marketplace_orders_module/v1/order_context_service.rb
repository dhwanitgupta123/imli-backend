module MarketplaceOrdersModule
  module V1
    class OrderContextService < BaseOrdersModule::V1::BaseService

      #
      # Initialize PAYMENT service with passed params
      #
      def initialize(params='')
        @params = params
        @order_context_dao = MarketplaceOrdersModule::V1::OrderContextDao.new(params)
      end


      # 
      # This function checks if order context already exists or not
      # and accordingly create or update the order context
      #
      # @param args [Hash] arguments containing order id and other context variables
      # 
      # @return [Model] updated order_context
      #
      def record_order_context(args)

        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::ORDER_ID_MISSING) if args.blank? || args[:order_id].blank?
        
        begin
          order_context = get_order_context(args[:order_id])

          return @order_context_dao.create_order_context(args) if order_context.blank?

          return @order_context_dao.update_order_context(args, order_context)
        rescue => e
          ApplicationLogger.error('Unable to record order context for order id ' + args[:order_id].to_s + ' , failed with exception: ' + e.message.to_s )
        end
      end

      # 
      # This function fetch order context if it exist then return it else returns nil
      #
      # @param order_id [Integer] 
      # 
      # @return [Model] order context
      #
      def get_order_context(order_id)
        begin
          order_context = @order_context_dao.get_context_by_order_id(order_id)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          order_context = nil
        end
        return order_context
      end
    end
  end
end

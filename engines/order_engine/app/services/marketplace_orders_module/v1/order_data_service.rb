#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
  	#
  	# Orders Data service class responsible for all data related operations
  	# on Marketplace Orders
  	#
  	class OrderDataService < BaseOrdersModule::V1::BaseService

      ORDER_PRODUCTS_SERVICE = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService
      ORDER_DAO = MarketplaceOrdersModule::V1::OrderDao
      ORDER_HELPER = MarketplaceOrdersModule::V1::OrderHelper
      ORDER_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates

      #
      # initialize function to initialize params on class creation]
      #
  		def initialize(params='')
        super
        @params = params
        @order_products_service = ORDER_PRODUCTS_SERVICE.new(@params)
        @order_dao = ORDER_DAO.new(params)
      end

      #
      # @param order [Object] [Order model object] order for which aggregated products are needed
      #
      def compute_aggregated_products_by_order(order)
        return nil if order.blank?

        # the corresponding order products are fetched associated to order
        active_order_products = @order_products_service.get_active_order_products_in_carts(order.cart)
        brand_packs_by_quantity = ORDER_HELPER.get_aggregated_bps_from_mpsps(active_order_products)

        return brand_packs_by_quantity
      end

      #
      # get_orders_stats function is API function
      #
      # @return [Hash] [Orders related stats]
      #
      def get_orders_stats
        orders_count_by_state_array = get_orders_count_stats_by_states
        orders_stats = {
          states: orders_count_by_state_array
        }
        return orders_stats
      end


      #
      # get_orders_count_stats_by_states function computes orders count by order states
      #
      # @return [Hash] [Orders count by states]
      #
      def get_orders_count_stats_by_states
        allowed_states = ORDER_STATES::ALLOWED_POLLING_STATES
        orders_count_by_state_array = []
        allowed_states.each do |order_state|
          orders_in_state = (@order_dao.get_orders_by_status(order_state[:value])).count
          orders_count_by_state_array << ({
            value: order_state[:value],
            label: order_state[:label],
            count: orders_in_state
          })
        end
        return orders_count_by_state_array
      end
    end
  end
end

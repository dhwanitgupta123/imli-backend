module MarketplaceOrdersModule

  module V1

###############################################################################
#                                                                             #
# This is for the need for having a single contact to send any kind of        #
# notification related to Order                                               #
# Functions from PaymentService.place_order_and_notify_user should call this. #
# AS SOON AS POSSIBLE                                                         #
#                                                                             #
# #############################################################################

    class OrderNotificationService < BaseOrdersModule::V1::BaseService

      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper
      ORDER_HELPER = MarketplaceOrdersModule::V1::OrderHelper
      ORDER_PRODUCTS_SERVICE = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService
      # Mailer
      ORDER_EMAIL_WORKER = CommunicationsModule::V1::Mailers::V1::OrderEmailWorker
      PAYMENT_DAO = MarketplaceOrdersModule::V1::PaymentDao
      PROFILE_SERVICE = UsersModule::V1::ProfileService

      #
      # Initialize OrderNotificationService with passed params
      #
      def initialize(params = '')
        @params = params
      end

      #
      # Inform Order placed to the user. Send email to the user's primary mail id
      # with complete invoice
      #
      # @param order [Object] [Order which is placed]
      #
      def inform_order_placed_to_user(order, mail_attributes)
        begin
          first_name = order.user.first_name if order.user.present?
          email = USER_SERVICE_HELPER.get_primary_mail(order.user)
          email_subject = CACHE_UTIL.read('PLACED_ORDER_SUBJECT') % {order_id: order.order_id}
          if email.present?
            mail_attributes = mail_attributes.merge({
              email_type: EMAIL_TYPE::PLACED_ORDER,
              subject: email_subject.to_s,
              to_first_name: first_name,
              to_email_id: email.email_id
              })
            ORDER_EMAIL_WORKER.perform_async(mail_attributes)
          end
        rescue => e
          ApplicationLogger.debug('Failed to inform placed order to user : ' + user_id.to_s + ' , ' + e.message)
        end
      end

      #
      # Inform Order placed to the Imli team. Send email to the team's group mail id
      # with complete invoice details of user
      #
      # @param order [Object] [Order which is placed]
      #
      def inform_order_placed_to_imli_team(order, mail_attributes)
        begin
          user_id = order.user.id if order.user.present?
          email_ids = CACHE_UTIL.read('IMLI_TEAM_MAIL_IDS')
          to_name = CACHE_UTIL.read('IMLI_TEAM_NAME')
          email_subject = CACHE_UTIL.read('NEW_ORDER_PLACED_SUBJECT') % {order_id: order.order_id, user_id: user_id}
          if email_ids.present?
            mail_attributes = mail_attributes.merge({
              email_type: EMAIL_TYPE::PLACED_ORDER,
              subject: email_subject.to_s,
              to_first_name: to_name,
              to_email_id: email_ids.to_s,
              multiple: true
              })
            ORDER_EMAIL_WORKER.perform_async(mail_attributes)
          end
        rescue => e
          ApplicationLogger.debug('Failed to inform placed order to IMLI Team : ' + user_id.to_s + ' , ' + e.message)
        end
      end

      #
      # Function to send Order placed SMS to user
      #
      def sms_order_placed(order)
        begin
          user = order.user
          payment_dao = PAYMENT_DAO.new(@params)
          payment = payment_dao.get_orders_payment_as_per_its_mode(order)
          message_attributes = {
            phone_number: user.phone_number,
            first_name: user.first_name,
            order_savings: payment.total_savings,
            order_id: order.order_id,
            net_total: payment.net_total
          }
          SMS_WORKER.perform_async(message_attributes, SMS_TYPE::ORDER_PLACED)
        rescue => e
          ApplicationLogger.debug('Failed to send SMS for placed order to user : ' + user.id.to_s + ' , ' + e.message)
        end
      end

      #
      # Function to send Order details for packer via email to packers
      #
      def inform_order_details_to_packer(order)
        begin
          order_products_service = ORDER_PRODUCTS_SERVICE.new(@params)

          # the corresponding order products are fetched associated to order
          active_order_products = order_products_service.get_active_order_products_in_carts(order.cart)

          brand_packs_by_quantity = ORDER_HELPER.get_aggregated_bps_from_mpsps(active_order_products)

          email_ids = CACHE_UTIL.read('IMLI_TEAM_MAIL_IDS')
          to_name = CACHE_UTIL.read('IMLI_TEAM_NAME')

          email_subject = CACHE_UTIL.read('PACKER_EMAIL_SUBJECT') % {order_id: order.order_id}

          mail_attributes = {
              products: brand_packs_by_quantity,
              order_id: order.order_id,
              email_type: EMAIL_TYPE::SINGLE_ORDER_PACKING_DETAILS[:type],
              subject: email_subject.to_s,
              to_first_name: to_name,
              to_email_id: email_ids.to_s,
              multiple: true,
              pdf_needed: true
              }
          # Send mail to IMLI orderManagement a mail
          # regarding this invoice for packers to pack the order

          ORDER_EMAIL_WORKER.perform_async(mail_attributes)
        rescue => e
            ApplicationLogger.debug('Failed to inform about order packing for order_id : ' + order.order_id + ' , ' + e.message)
        end
      end

      # Functioon to trigger communication user based on event on order
      #
      def trigger_user_communication(order, event)
        if ORDER_EVENTS::ALLOWED_EVENTS.include?(event.to_i)
          case event.to_i
          when ORDER_EVENTS::PAYMENT_FAILED
            #TO-DO
          when ORDER_EVENTS::DISPATCH
            send_dispatched_communication(order)
          when ORDER_EVENTS::DELIVER
            send_delivered_communication(order)
          when ORDER_EVENTS::NO_RETRY_PAYMENT
            #TO-DO
          when ORDER_EVENTS::RETRY_SUCCESSFUL
            #TO-DO
          when ORDER_EVENTS::FULFILLMENT_SUCCESSFUL
            #TO-DO
          when ORDER_EVENTS::CANCEL_ORDER
            send_order_cancelled_notification(order)
            reduce_profile_savings(order)
          when ORDER_EVENTS::FORCE_CANCEL
            send_order_cancelled_notification(order)
            reduce_profile_savings(order)
          when  ORDER_EVENTS::PACKING_DONE
            #TO-DO
          end
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
        end
      end

      #
      # Function to subtract the savings of User
      # When the order is cancelled
      #
      def reduce_profile_savings(order)
        payment_dao = PAYMENT_DAO.new(@params)
        payment = payment_dao.get_orders_payment_as_per_its_mode(order)
        savings = -1 * payment.total_savings
        profile_service = PROFILE_SERVICE.new(@params)
        profile_service.update_user_savings(order.user.profile, savings)
      end

      #
      # Function to send communication to user when payment failed
      #
      def send_payment_failed_communication(order)
        sms_attributes = {
          phone_number: order.user.phone_number,
          first_name: order.user.first_name,
          order_id: order.order_id,
          customer_care: CACHE_UTIL.read('CUSTOMER_CARE_NUMBER')
        }
        SMS_WORKER.perform_async(sms_attributes, SMS_TYPE::PAYMENT_FAILURE)

        notification_attributes = {
          username: 'parse_user_' + order.user.id.to_s,
          notification_type: NOTIFICATION_TYPE::PAYMENT_FAILURE[:type]
        }
        NOTIFICATION_WORKER.perform_async(notification_attributes)
      end

      #
      # Function to send communication to user when order is out for delivery
      #
      def send_dispatched_communication(order)
        sms_attributes = {
          phone_number: order.user.phone_number,
          first_name: order.user.first_name,
          order_id: order.order_id
        }
        SMS_WORKER.perform_async(sms_attributes, SMS_TYPE::OUT_FOR_DELIVERY)

        notification_attributes = {
          username: 'parse_user_' + order.user.id.to_s,
          notification_type: NOTIFICATION_TYPE::OUT_FOR_DELIVERY[:type],
          order_id: order.order_id
        }
        NOTIFICATION_WORKER.perform_async(notification_attributes)
      end

      #
      # Function to send communication to user order is delivered
      #
      def send_delivered_communication(order)
        payment_dao = PAYMENT_DAO.new(@params)
        payment = payment_dao.get_orders_payment_as_per_its_mode(order)
        sms_attributes = {
          phone_number: order.user.phone_number,
          first_name: order.user.first_name,
          order_id: order.order_id,
          delivered_at: Time.zone.now.strftime("%d%b'%y,%I:%M%P"),
          order_savings: payment.total_savings
        }
        SMS_WORKER.perform_async(sms_attributes, SMS_TYPE::PRODUCT_DELIVERED)
      end

      #
      # Function to send communication to user when order is cancelled
      #
      def send_order_cancelled_notification(order)
        sms_attributes = {
          phone_number: order.user.phone_number,
          order_id: order.order_id
        }
        SMS_WORKER.perform_async(sms_attributes, SMS_TYPE::DELIVERY_CANCELLED)
        notification_attributes = {
          username: 'parse_user_' + order.user.id.to_s,
          notification_type: NOTIFICATION_TYPE::DELIVERY_CANCELLED[:type],
          order_id: order.order_id
        }
        NOTIFICATION_WORKER.perform_async(notification_attributes)
      end
    end
  end
end

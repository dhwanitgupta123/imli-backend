#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
  	#
  	# Orders Panel service class
  	#
  	class OrderPanelService < BaseOrdersModule::V1::BaseService

      ORDER_SERVICE = MarketplaceOrdersModule::V1::OrderService
      ORDER_PRODUCTS_SERVICE = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService
      PAYMENT_COMPUTATION_SERVICE = MarketplaceOrdersModule::V1::PaymentComputationService
      PAYMENT_SERVICE = MarketplaceOrdersModule::V1::PaymentService
  		ORDER_DAO = MarketplaceOrdersModule::V1::OrderDao
      CART_SERVICE = MarketplaceOrdersModule::V1::CartService
      CART_DAO = MarketplaceOrdersModule::V1::CartDao
      PAYMENT_DAO = MarketplaceOrdersModule::V1::PaymentDao
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      ORDER_HELPER = MarketplaceOrdersModule::V1::OrderHelper
      ORDER_MAPPER = MarketplaceOrdersModule::V1::OrderMapper
      EMAIL_TYPE = CommunicationsModule::V1::Mailers::V1::EmailType
      AGGREGATED_PRODUCTS_INVOICE_HELPER = MarketplaceOrdersModule::V1::AggregatedProductsInvoiceHelper
      CACHE_UTIL = CommonModule::V1::Cache
      ORDER_EMAIL_WORKER = CommunicationsModule::V1::Mailers::V1::OrderEmailWorker
      ORDER_NOTIFICATION = MarketplaceOrdersModule::V1::OrderNotificationService
      MEMBERSHIP_SERVICE = UsersModule::V1::MembershipService
      ADDRESS_SERVICE = AddressModule::V1::AddressService
      WBP_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      ORDER_STATE_HELPER = MarketplaceOrdersModule::V1::OrderStateTransitionHelper
      TEMPLATE_TYPES = CommonModule::V1::TemplateTypes
      INVOICE_HELPER = MarketplaceOrdersModule::V1::InvoiceHelper
      ORDER_DATA_SERVICE = MarketplaceOrdersModule::V1::OrderDataService
      FILE_SERVICE_HELPER = FileServiceModule::V1::FileServiceHelper
      PAYMENT_MODES = MarketplaceOrdersModule::V1::PaymentModes
      ORDER_STOCKING_HELPER = MarketplaceOrdersModule::V1::OrderStockingHelper
      BENEFIT_SERVICE = CommonModule::V1::BenefitService
      TRIGGER_ENUM_UTIL = TriggersModule::V1::TriggerEnum

      #
      # initialize function to initialize params on class creation]
      #
  		def initialize(params='')
        super
        @params = params
        @order_dao = ORDER_DAO.new(params)
      end

      #
      # get_all_orders function
      # @param pagination_params [Hash] pagination params can be passed, else will be treated blank
      #
      # @return [Array] returns all orders
      #
      def get_all_orders(pagination_params = {})
        get_all_elements(@order_dao, pagination_params)
      end

      #
      # get_all_orders function
      # Timeline:
      #   - to_time or current time
      #   - from_time or (till_time - 1)
      #
      # @param pagination_params [Hash] pagination params can be passed, else will be treated blank
      #
      # @return [Array] returns all orders
      #
      def get_all_orders_in_interval(request_params = {})
        default_to_time = Time.zone.now
        # Initialize and validate time params
        time_params = initialize_order_time_filters(
          {
            from_time: request_params[:from_time],
            to_time: request_params[:to_time]
          },
          default_to_time)
        response = get_all_elements_between_interval(@order_dao, request_params, time_params)
        return response
      end

      #
      # get_packing_details_by_order_id function
      # @param order_id [String] order_id for which packing details are needed
      #
      def get_packing_details_by_order_id(order_id)
        order = @order_dao.get_by_field('order_id', order_id)
        begin
          order_data_service = ORDER_DATA_SERVICE.new(@params)

          aggregated_products = order_data_service.compute_aggregated_products_by_order(order)

          # Building PDF
          relative_template_path = TEMPLATE_TYPES::SINGLE_ORDER_PACKING_DETAILS[:relative_location]
          pdf_file_name = 'Packer_mail_' + order.order_id

          pdf_file_params = {
              products: aggregated_products,
              order_id: order.order_id
              }
          pdf_generator = PdfGenerator.new()
          pdf_details = pdf_generator.generate_pdf(pdf_file_name, pdf_file_params, relative_template_path)

          # Upload to S3 and get signed URL
          signed_pdf_expiry_duration = 5.minutes
          signed_packer_pdf_link = FILE_SERVICE_HELPER.upload_file_to_s3_and_get_signed_url(
            pdf_details[:file_path], pdf_details[:filename], signed_pdf_expiry_duration)
          File.delete(pdf_details[:file_path]);

          return {order_packing_details: signed_packer_pdf_link}
        rescue => e
            ApplicationLogger.debug('Failed to generate packer list for order_id : ' + order.order_id + ' , ' + e.message)
        end
      end

      #
      # get_invoice_by_order_id function
      # @param order_id [String] order_id for which invoice is needed
      #
      def get_invoice_by_order_id(order_id)
        order = @order_dao.get_by_field('order_id', order_id)
        begin
          relative_template_path = TEMPLATE_TYPES::ORDER_INVOICE[:relative_location]
          pdf_file_name = CACHE_UTIL.read('INVOICE_ATTACHMENT_NAME') % {order_id: order.order_id}
          pdf_file_params = INVOICE_HELPER.get_invoice_hash_for_invoice_generation(order)

          assets = {
            'AMPLE_ORIGINAL_PURPLE_LOGO'=> CACHE_UTIL.read('AMPLE_ORIGINAL_PURPLE_LOGO'),
            'FACEBOOK_PURPLE_ICON'=> CACHE_UTIL.read('FACEBOOK_PURPLE_ICON'),
            'TWITTER_PURPLE_ICON'=> CACHE_UTIL.read('TWITTER_PURPLE_ICON'),
            'INSTAGRAM_PURPLE_ICON'=> CACHE_UTIL.read('INSTAGRAM_PURPLE_ICON'),
            'FACEBOOK_URL'=> CACHE_UTIL.read('FACEBOOK_URL'),
            'TWITTER_URL'=> CACHE_UTIL.read('TWITTER_URL'),
            'INSTAGRAM_URL'=> CACHE_UTIL.read('INSTAGRAM_URL'),
            'BUYAMPLE_HOMEPAGE' => CACHE_UTIL.read('BUYAMPLE_HOMEPAGE'),
            'SELLER_NAME' => CACHE_UTIL.read('SELLER_NAME'),
            'SELLER_ADDRESS_LINE_1' => CACHE_UTIL.read('SELLER_ADDRESS_LINE_1'),
            'SELLER_ADDRESS_LINE_2' => CACHE_UTIL.read('SELLER_ADDRESS_LINE_2'),
            'TIN_VAT_NUMBER' => CACHE_UTIL.read('TIN_VAT_NUMBER')
          }

          pdf_file_params = pdf_file_params.merge(assets)
          pdf_generator = PdfGenerator.new()
          pdf_details = pdf_generator.generate_pdf(pdf_file_name, pdf_file_params, relative_template_path)

          # Upload to S3 and get signed URL
          signed_pdf_expiry_duration = 5.minutes
          signed_invoice_pdf_link = FILE_SERVICE_HELPER.upload_file_to_s3_and_get_signed_url(
            pdf_details[:file_path], pdf_details[:filename], signed_pdf_expiry_duration)
          File.delete(pdf_details[:file_path]);

          return {invoice_link: signed_invoice_pdf_link}
        rescue => e
            ApplicationLogger.debug('Failed to generate invoice for order_id : ' + order.order_id + ' , ' + e.message)
        end
      end

      #
      # get_order_by_order_id function
      # @param field_value [String] field_value to be checked
      #
      def get_order_by_order_id(order_id)
        #TODO: This function can be extended to a generalized field typ too.
        order = @order_dao.get_order_by_order_id(order_id)
        return order
        #validate_if_exists(field_value, @order_dao, 'Order')
      end

      #
      # get_aggregated_products function is API function
      # @param params = {} [Hash] for future extendibility of this function to pass
      #   from_time and till_time parameters
      #
      # @return [Array] aggregated products
      #
      def get_aggregated_products(params = {})
        response = get_aggregated_products_in_interval(params)
      end

      #
      # get_aggregated_products_in_interval function is a
      #  logic function for get_aggregated_products API
      #
      # @param params = {} [Hash] for future extendibility of this function to pass
      #   from_time and till_time parameters
      #
      # @return [Array] aggregated products by quantity
      #
      def get_aggregated_products_in_interval(params = {})
        order_products_service = ORDER_PRODUCTS_SERVICE.new(@params)

        time_params = initialize_and_validate_time(params)

        from_time = time_params[:from_time]
        till_time = time_params[:till_time]

        # fetched orders in PLACED state in the given Time interval
        placed_orders_in_interval = @order_dao.get_placed_orders_in_interval({
          from_time: from_time,
          till_time: till_time
          })

        # all corresponding placed orders minimal details
        placed_orders_minimal_details = ORDER_MAPPER.map_orders_to_array(placed_orders_in_interval)

        # all corresponding carts are fetched associated to placed orders
        carts_array = ORDER_HELPER.get_carts_array_from_orders(placed_orders_in_interval)

        # all active order products in fetched carts are fetched
        active_order_products = order_products_service.get_active_order_products_in_carts(carts_array)

        # groups all order products fetched by marketplace_selling_pack and
        # adds up their quantities
        aggregated_order_products = active_order_products.group(:marketplace_selling_pack).sum(:quantity)

        # aggregated brand packs are fetched from the aggregated order products (i.e. MPSPs)
        aggregated_brand_packs_array =
          ORDER_MAPPER.map_aggregated_products_to_array(aggregated_order_products)

        # duplicated brand packs are removed and added for single instanced Brand Pack key
        # and serial numbers are generated
        aggregated_brand_packs_by_quantity =
          AGGREGATED_PRODUCTS_INVOICE_HELPER.map_aggregated_products_invoice_by_quantity_to_array(aggregated_brand_packs_array)

        # group the brand packs by category
        aggregated_brand_packs_by_category = AGGREGATED_PRODUCTS_INVOICE_HELPER.group_brand_packs_to_category(aggregated_brand_packs_by_quantity)

        # used to send aggregated orders email with required params passed
        send_aggregated_orders_mail({
          from_time: from_time,
          till_time: till_time,
          placed_orders_minimal_details: placed_orders_minimal_details,
          aggregated_products: aggregated_brand_packs_by_category })

        return aggregated_brand_packs_by_quantity
      end

      #
      # send_aggregated_orders_mail function
      # - Sends email with the required params
      #
      # @param params [Hash]
      # - [String] from_time
      # - [String] till_time
      # - [Array] aggregated_products
      # - [Array] placed_orders_minimal_details
      #
      def send_aggregated_orders_mail(params)
        begin
          from_time = params[:from_time]
          till_time = params[:till_time]
          aggregated_products = params[:aggregated_products]
          placed_orders_minimal_details = params[:placed_orders_minimal_details]

          email_ids = CACHE_UTIL.read('IMLI_TEAM_MAIL_IDS')
          to_name = CACHE_UTIL.read('IMLI_TEAM_NAME')

          formatted_from_time = from_time.strftime("%a, %d %b '%y (%I:%M%P)") # Prints in format: Sun, 17 Nov '91 (06:15am)
          formatted_till_time = till_time.strftime("%a, %d %b '%y (%I:%M%P)") # Prints in format: Sun, 17 Nov '91 (06:15am)

          email_subject = CACHE_UTIL.read('AGGREGATED_PRODUCTS_LIST_SUBJECT') + '[' + formatted_from_time.to_s + ' to ' + formatted_till_time.to_s + ']'

          # pdf needed only if the products are present
          pdf_needed = (aggregated_products.blank?) ? false : true

          mail_attributes = {
            placed_orders_minimal_details: placed_orders_minimal_details,
            aggregated_products: aggregated_products,
            from_time: formatted_from_time,
            till_time: formatted_till_time
            }
          mail_attributes = mail_attributes.merge({
            email_type: EMAIL_TYPE::AGGREGATED_PRODUCTS[:type],
            subject: email_subject.to_s,
            to_first_name: to_name,
            to_email_id: email_ids.to_s,
            pdf_needed: pdf_needed,
            multiple: true
            })
          # Send mail to IMLI orderManagement a mail
          # regarding this invoice for pickers to pick the brand packs required

          ORDER_EMAIL_WORKER.perform_async(mail_attributes)
        rescue => e
          ApplicationLogger.debug('Failed to inform aggregation reporting to IMLI Team : ' + ' , ' + e.message)
        end
      end

      #
      # Initialize Order Time Filters
      #   - to_time: passed to_time (or default time passed)
      #   - from_time: passed from_time (or to_time minus 1 day)
      #
      # @param params [JSON] [Hash of interval params]
      # @param default_to_time [DateTime] [default value of to time]
      #
      # @return [JSON] [Hash of from_time and to_time]
      #
      def initialize_order_time_filters(params, default_to_time)
        to_time = params[:to_time]
        from_time = params[:from_time]

        final_to_time = to_time.present? ? Time.zone.at(to_time.to_i) : default_to_time
        final_from_time = from_time.present? ? Time.zone.at(from_time.to_i) : (final_to_time - 1.day)

        # Take starting of from_time to ending of to_time into account
        final_from_time = final_from_time.beginning_of_day
        final_to_time = final_to_time.end_of_day
        return ({till_time: final_to_time, from_time: final_from_time})
      end

      #
      # initialize_and_validate_time function
      # @param params [Hash]
      # - Contains:
      #   - to_time    [String] (optional) - format: Epoch Unix Time Stamp
      #   - from_time  [String] (optional) - format: Epoch Unix Time Stamp
      #
      # If none of the above params is specified, the default time is picked up
      # for any of the missing params.
      # Then, if due to till Time < from Time, defalt times are used.
      #
      # @return [type] [description]
      def initialize_and_validate_time(params)
        till_time = Time.zone.now.change(hour: 06, minutes: 00)
        from_time = till_time - 1.day

        unless params[:to_time].blank?
          # override till_time params if present
          till_time = Time.zone.at(params[:to_time].to_i)
        end
        unless params[:from_time].blank?
          # override from_time params if present
          from_time = Time.zone.at(params[:from_time].to_i)
        end

        # checks if new custom time line is valid time line
        # else default 6am to 6am localzone timeline is utilized
        if (till_time < from_time)
          till_time = Time.zone.now.change(hour: 06, minutes: 00)
          from_time = till_time - 1.day
          puts CONTENT::USE_DEFAULT_TIME_PERIOD
        end

        return ({till_time: till_time, from_time: from_time})
      end

      #
      # Transational function to change the state of Order
      #
      def transactional_change_state(args)
        transactional_function = Proc.new do |args|
          return change_state(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # Function to change state of Order & trigger notifications correspondingly
      #
      def change_state(params)
        order = validate_if_exists(params[:id], @order_dao, 'Order')
        order = @order_dao.change_state(order, params[:event])
        order_notification = ORDER_NOTIFICATION.new
        order_notification.trigger_user_communication(order, params[:event])
        post_change_state_process(params, order)
        return order
      end

      #
      # Edit time slot for an order
      #
      # @param order_params [JSON] [New time slot params and order id]
      #
      # @return [Object] [updated order]
      #
      def edit_order_time_slot(order_params)
        order_service = ORDER_SERVICE.new(@params)
        id = order_params[:id]
        preferred_slot = order_params[:preferred_slot]
        # Validate whether order exists and then update its preferred date and slot
        order = validate_if_exists(id, @order_dao, 'Order')
        order = order_service.update_preferred_slot_for_order(order, preferred_slot)
        return order
      end

      #
      # Edit time slot for an order
      #
      # @param order_params [JSON] [New time slot params and order id]
      #
      # @return [Object] [updated order]
      #
      def edit_order_address(order_params)
        order_service = ORDER_SERVICE.new(@params)
        order_dao = ORDER_DAO.new(@params)
        id = order_params[:id]
        address = order_params[:address]
        # Validate whether order exists and then update address of the associated user
        order = validate_if_exists(id, @order_dao, 'Order')
        if order.address_id.to_i != address[:id].to_i
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::NOT_VALID_ORDER_ADDRESS)
        end
        user = order.user
        # Go for address updation only if request is valid
        if (is_valid_request_for_update_address?(order))
          begin
            # Update user's address
            address_service = ADDRESS_SERVICE.new(@params)
            address = address_service.create_new_and_persist_old_address_for_user({
              user: user,
              address: address
              })
            # Update this order with updated_address
            order = order_dao.update_order({ address: address }, order)
          rescue => e
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(e.message)
          end
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::NOT_VALID_REQUEST_FOR_ADDRESS_UPDATE)
        end
        return order
      end

      #
      # Transational function to edit order cart
      #
      # Args::
      #  id: id of the order which is to be editted
      #  cart: Hash containing 
      #    id: id of the cart
      #    order_products: Array of order_product hash
      #      product: Product hash containing MPSP id
      #      quantity: Quantity of the product
      #      additional_discount: additional discount to be put
      #
      def transactional_edit_order_cart(args)
        transactional_function = Proc.new do |args|
          return edit_order_cart(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # Edit Order's cart
      #
      # @param order_params [JSON] [Hash containing order params]
      #
      # Order Params structure::
      #  id: id of the order which is to be editted
      #  cart: Hash containing 
      #    id: id of the cart
      #    order_products: Array of order_product hash [{}, {}]
      #      product: Product hash containing MPSP id {id: mpsp_id}
      #      quantity: Quantity of the product
      #      additional_discount: additional discount to be put
      #
      #
      # @return [type] [description]
      def edit_order_cart(order_params)
        order_service = ORDER_SERVICE.new(@params)
        cart_service = CART_SERVICE.new(@params)
        order_dao = ORDER_DAO.new(@params)
        cart_dao = CART_DAO.new(@params)
        id = order_params[:id]
        cart_hash = order_params[:cart]
        # Validate whether order exists and then update address of the associated user
        order = validate_if_exists(id, @order_dao, 'Order')
        # Eager load complete order details before proceeding
        order = ORDER_DAO.load_order_details_data(order)
        cart = cart_dao.get_by_id(cart_hash[:id])
        if order.cart != cart
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::NOT_VALID_ORDER_CART)
        end
        # Eager load cart before proceeding further
        cart = CART_DAO.load_cart_details(cart)
        # Go for cart updation only if request is valid
        if (is_valid_request_for_update_cart?(order))
          begin
            # Move old cart to Discard state, add updated order products to
            # new cart. Recompute cart stats, recompute payment details
            order_products_array = cart_hash[:order_products]
            user = order.user
            previous_cart = order.cart
            new_cart = cart_dao.create_new_cart(user)
            order_products_array.each do |order_product|
              cart_service.validate_update_cart(order_product)
              new_cart = cart_service.update_order_product_to_cart(order_product, new_cart)
            end
            cart_service.monitor_cart_status(new_cart)
            new_cart = cart_dao.checkout_cart(new_cart)
            cart_dao.discard_cart(previous_cart)
            new_cart = cart_dao.re_compute_cart_stats(new_cart)
            order = order_dao.assign_cart_to_order(new_cart, order)
            # Update payment of that order
            update_payment_for_order({user: user, cart: new_cart, order: order})
            # Update Stocking as per new cart
            ORDER_STOCKING_HELPER.update_stock_based_on_previous_cart(previous_cart, new_cart)
          rescue => e
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(e.message)
          end
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::NOT_VALID_REQUEST_FOR_CART_UPDATE)
        end
        return order
      end

      #
      # Update payment for Order as per the newly updated cart
      #
      # @param args [JSON] [Hash containing certain must attributes 
      #  like user, cart, order]
      #
      # Params::
      #   user: User for the order
      #   cart: Newly updated cart
      #   order: Order whose payment is to be updated
      #
      # @return [Object] [New payment object with updated details]
      #
      def update_payment_for_order(args)
        payment_computation_service = PAYMENT_COMPUTATION_SERVICE.new(@params)
        payment_dao = PAYMENT_DAO.new(@params)
        user = args[:user]
        cart = args[:cart]
        order = args[:order]
        # Fetch last successful payment of the given ORDER
        old_payment = payment_dao.get_last_successful_payment(order)
        # Remaining computation of payment will be computed using COD mode
        payment_computation_mode = PAYMENT_MODES::CARD_ON_DELIVERY
        payment_hash = payment_computation_service.recompute_payment_details({user: user, cart: cart, order: order, payment_mode: payment_computation_mode})
        new_payment = payment_computation_service.reassign_new_payment_as_per_amount_diff({order: order, old_payment: old_payment, new_payment_hash: payment_hash})
        return new_payment
      end

      #
      # Function to perform processes after the state change of order
      #
      def post_change_state_process(params, order)
        case params[:event].to_i
          when ORDER_EVENTS::FULFILLMENT_SUCCESSFUL
            if first_order_fulfilled?(order.user)
              first_order_fulfilled_tasks(order.user)
            end
          when ORDER_EVENTS::PACKING_DONE
            out_warehouse_brand_packs(order)
          when ORDER_EVENTS::FORCE_CANCEL
            inward_warehouse_brand_packs(order)
          when ORDER_EVENTS::COMPLETE_ORDER
            if first_order_completed?(order.user)
              first_order_completed_tasks(order.user)
            end
          end
      end

      #
      # Function to check if it was User's first order
      #
      def first_order_completed?(user)
        order_dao = ORDER_DAO.new(@params)
        return order_dao.first_order_completed?(user)
      end

      # 
      # task to check for after first order completion
      #
      # @param user [Model] user model
      #
      def first_order_completed_tasks(user)
        benefit_service = BENEFIT_SERVICE.new(@params)
        benefit_service.apply_benefits_to_referrer({
              user: user,
              trigger: TRIGGER_ENUM_UTIL.get_trigger_id_by_trigger('AFTER_FIRST_ORDER') 
            })
      end

      #
      # Function to check if it was User's first order
      #
      def first_order_fulfilled?(user)
        order_dao = ORDER_DAO.new(@params)
        return order_dao.first_order_fulfilled?(user)
      end

      def first_order_fulfilled_tasks(user)
        membership_service  = MEMBERSHIP_SERVICE.new(@params)
        if user.referred_by.present?
          membership_service.reward_referral_one_month_membership(user.referred_by)
        end
      end

      #
      # is valid request for update address
      #
      # @param order [Object] [Order whose address update request comes]
      #
      # @return [Boolean] [true if valid request, else false]
      #
      def is_valid_request_for_update_address?(order)
        # For now, returning TRUE for all requests.
        # Later on, we need to check whether user has alrady ordered in this particular
        # address before. In short, <user, order, address> combination should not exist beforehand
        return true
      end

      #
      # is valid request for update cart for order
      #
      # @param order [Object] [Order whose address update request comes]
      #
      # @return [Boolean] [true if valid request, else false]
      #
      def is_valid_request_for_update_cart?(order)
        return true
      end

      #
      # Function to mark outward of the products being packer for MPSP order
      #
      def out_warehouse_brand_packs(order)
        ORDER_STOCKING_HELPER.outward_warehouse_brand_packs(order.cart)
      end

      #
      # Function to mark inward of the products when the order if focrfully cancelled by the
      # warehouse user after it's being packed
      #
      def inward_warehouse_brand_packs(order)
        ORDER_STOCKING_HELPER.inward_warehouse_brand_packs(order.cart)
      end

  	end
  end
end

#
# Module to handle all the functionalities related to Services
#
module BaseOrdersModule
  #
  # Version1 for base module
  #
  module V1
    #
    # Base module to inject generic utils
    #
    class BaseService
      
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      SMS_WORKER = CommunicationsModule::V1::Messengers::V1::SmsWorker
      SMS_TYPE = CommunicationsModule::V1::Messengers::V1::SmsType
      NOTIFICATION_TYPE = CommunicationsModule::V1::Notifications::V1::NotificationType
      NOTIFICATION_WORKER = CommunicationsModule::V1::Notifications::V1::OrderNotificationWorker
      EMAIL_TYPE = CommunicationsModule::V1::Mailers::V1::EmailType
      ORDER_EMAIL_WORKER = CommunicationsModule::V1::Mailers::V1::OrderEmailWorker
      ORDER_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::OrderEvents
      ORDER_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
      CACHE_UTIL = CommonModule::V1::Cache

      def initialize(params='')
      end


      #
      # This funtion call the get_by_id function of Dao
      #
      # @param id [Integer] id of the model
      # @param dao [DaoObject] DAO of which get_by_id function to call
      # @param message [String] custom message
      #
      # @return [Object] object if it find the record
      #
      # @error [InvalidArgumentsError] if record not found
      #
      def validate_if_exists(id, dao, message='')
        message = "Record " if message.blank?
        model = dao.get_by_id(id)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(message + ' not found') if model.nil?

        return model
      end

      #
      # This funtion call the get_all function of Dao
      #
      # @param dao [DaoObject] DAO of which get_by_id function to call
      #
      # @return [Array] array of model
      #
      def get_all_elements(dao, pagination_params)
        return dao.get_all(pagination_params)
      end

      #
      # This funtion call the get_all function of Dao
      # but with Interval params
      #
      # @param dao [DaoObject] DAO of which get_by_id function to call
      #
      # @return [Array] array of model
      #
      def get_all_elements_between_interval(dao, pagination_params, interval_params)
        return dao.get_all_orders_between_interval(pagination_params, interval_params)
      end

      #
      # This funtion call the change_state function of Dao
      #
      # @param args [Hash] contains corresponding argument of change state
      # @param dao [DaoObject] DAO of which change_state function to call
      # @param message [String] custom message
      #
      # @return [Object] object if it succeed to update else pass the error raised by DAO
      #
      def change_state(args, dao, message='')
        message = "Record " if message.blank?
        model = validate_if_exists(args[:id], dao, message)
        dao.change_state(model, args[:event])
      end

    end
  end
end
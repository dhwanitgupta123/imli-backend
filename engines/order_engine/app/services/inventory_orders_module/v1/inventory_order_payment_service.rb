#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Class to handle service functionalities of Inventory order payment
    #
    class InventoryOrderPaymentService < BaseOrdersModule::V1::BaseService

      INVENTORY_ORDER_PAYMENT_DAO = InventoryOrdersModule::V1::InventoryOrderPaymentDao

      #
      # Initialize order service with passed params
      #
      def initialize(params={})
        @params = params
      end

      #
      # Function to create instance of the invetory order payment
      #
      def new_inventory_order_payment
        inventory_order_payment_dao = INVENTORY_ORDER_PAYMENT_DAO.new(@params)
        return inventory_order_payment_dao.new_inventory_order_payment
      end

      #
      # Function to update the inventory order payment object
      # This Takes the prev order product and it's updated version and perform atomic update on
      # Inventory order payment.
      #
      def update_purchase_order_payment_details(inventory_order_payment, inventory_order_products)
        previous_iop_pricing_details = get_payment_details_for_ibp(inventory_order_products[:previous_order_product], inventory_order_products[:previous_quantity])
        current_iop_pricing_details = get_payment_details_for_ibp(inventory_order_products[:updated_order_product], inventory_order_products[:updated_quantity])
        inventory_order_payment = atomic_update_payment_stats(inventory_order_payment, previous_iop_pricing_details, current_iop_pricing_details)
        return inventory_order_payment
      end

      #
      # Function to perform atomic update on the Payemnt object based on the
      # prev & updated order product quantity & pricing.
      #
      def atomic_update_payment_stats(inventory_order_payment, previous_iop_pricing_details, current_iop_pricing_details)
        return inventory_order_payment if inventory_order_payment.blank? || previous_iop_pricing_details.blank? || current_iop_pricing_details.blank?
        inventory_order_payment_dao = INVENTORY_ORDER_PAYMENT_DAO.new(@params)
        total_mrp = inventory_order_payment.total_mrp + current_iop_pricing_details[:total_mrp] - previous_iop_pricing_details[:total_mrp]
        total_cost_price = inventory_order_payment.total_cost_price + current_iop_pricing_details[:total_cost_price] - previous_iop_pricing_details[:total_cost_price]
        total_vat_value = inventory_order_payment.total_vat_value + current_iop_pricing_details[:total_vat_value] - previous_iop_pricing_details[:total_vat_value]
        total_cst_value = inventory_order_payment.total_cst_value + current_iop_pricing_details[:total_cst_value] - previous_iop_pricing_details[:total_cst_value]
        total_tax = inventory_order_payment.total_tax + current_iop_pricing_details[:total_tax] - previous_iop_pricing_details[:total_tax]
        net_total = total_cost_price + total_tax
        args = {
          total_mrp: total_mrp,
          total_cost_price: total_cost_price,
          total_vat_value: total_vat_value,
          total_cst_value: total_cst_value,
          total_tax: total_tax,
          net_total: net_total
          }
        inventory_order_payment = inventory_order_payment_dao.update_inventory_order_payment(inventory_order_payment,args)
        return inventory_order_payment
      end

      #
      # Get payment details for IBP
      #
      # @param quantity [Object] [Order product]
      #
      # @return [JSON] [hash containing cost_price and mrp & tax]
      #
      def get_payment_details_for_ibp(inventory_order_product, quantity)
        if inventory_order_product.blank?
          return { total_mrp: 0, total_cost_price: 0, total_vat_value: 0, total_cst_value:0, total_tax: 0 }
        end
        total_cost_price = inventory_order_product.cost_price * quantity
        total_mrp = inventory_order_product.mrp * quantity
        total_vat_value = inventory_order_product.vat_tax_value * quantity
        total_cst_value = inventory_order_product.cst_value * quantity
        total_tax = total_cst_value + total_vat_value
        return {
          total_cost_price: total_cost_price,
          total_mrp: total_mrp,
          total_vat_value: total_vat_value,
          total_cst_value: total_cst_value,
          total_tax: total_tax
        }
      end

      #
      # Fucntion to add octrai tax to the payment object and update related fields
      #
      def add_octrai_tax_in_inventory_order(inventory_order_payment, payment_params)
        total_octrai_value = payment_params[:total_octrai]
        total_tax = inventory_order_payment.total_vat_value + inventory_order_payment.total_cst_value + total_octrai_value
        net_total = inventory_order_payment.total_cost_price + total_tax
        args = {
          total_octrai_value: total_octrai_value,
          total_tax: total_tax,
          net_total: net_total
        }
        inventory_order_payment_dao = INVENTORY_ORDER_PAYMENT_DAO.new(@params)
        inventory_order_payment = inventory_order_payment_dao.update_inventory_order_payment(inventory_order_payment,args)
      end
    end
  end
end

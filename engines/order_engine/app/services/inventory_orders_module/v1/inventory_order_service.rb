module InventoryOrdersModule

  module V1

    class InventoryOrderService < BaseOrdersModule::V1::BaseService

      USER_SERVICE = UsersModule::V1::UserService
      INVENTORY_ORDER_DAO = InventoryOrdersModule::V1::InventoryOrderDao
      INVENTORY_ORDER_PRODUCTS_SERVICE = InventoryOrdersModule::V1::InventoryOrderProductsService
      INVENTORY_ORDER_PAYMENT_SERVICE = InventoryOrdersModule::V1::InventoryOrderPaymentService
      WAREHOUSE_SERVICE = SupplyChainModule::V1::WarehouseService
      INVENTORY_SERVICE = SupplyChainModule::V1::InventoryService
      WAREHOUSE_ADDRESS_SERVICE = SupplyChainModule::V1::WarehouseModule::V1::WarehouseAddressService
      INVENTORY_BRAND_PACK_SERVICE = InventoryProductModule::V1::InventoryBrandPackService
      INVENTORY_BRAND_PACK_DAO = InventoryProductModule::V1::InventoryBrandPackDao
      INVENTORY_ORDER_EVENTS = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderEvents
      PURCHASE_ORDER_HELPER = InventoryOrdersModule::V1::PurchaseOrderHelper
      INVENTORY_ORDER_HELPER = InventoryOrdersModule::V1::InventoryOrderHelper
      WBP_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      FILE_SERVICE = FileServiceModule::V1::FileService

      #
      # Initialize order service with passed params
      #
      def initialize(params={})
        @params = params
        @inventory_service = INVENTORY_SERVICE.new(@params)
        @warehouse_service = WAREHOUSE_SERVICE.new(@params)
        @warehouse_address_service = WAREHOUSE_ADDRESS_SERVICE.new(@params)
      end

      #
      # Execute create purchase Order functionality with Transactional block.
      # @param place_order_params [JSON] [Hash containing w/h, inventory and address hashes]
      #
      # @return [JSON] [Hash containing order and payment details]
      #
      def transactional_create_purchase_order(purchase_order_params)
        transactional_function = Proc.new do |args|
          return create_purchase_order(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: purchase_order_params
          })
        transaction_block.run();
      end

      #
      # Create new purchase order by creating a new inventory Order object and 
      # linking cwarehouse, inventory & addresses with it
      #
      # @param place_order_params [JSON] [Hash containing w/h, inventory and address hashes]
      #
      # @return [JSON] [Hash containing order and payment details]
      #
      def create_purchase_order(purchase_order_params)
        user_service = USER_SERVICE.new(@params)
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        inventory_order_payment_service = INVENTORY_ORDER_PAYMENT_SERVICE.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
        references = validate_purchase_order_references(purchase_order_params)
        inventory_order = initiated_inventory_order_exisits(purchase_order_params)
        if inventory_order.blank?
          new_purchase_order_params = {
            user: user,
            warehouse_id: purchase_order_params[:warehouse][:id],
            inventory_id: purchase_order_params[:inventory][:id],
            shipping_address_id: purchase_order_params[:address][:shipping_address][:id],
            billing_address_id: purchase_order_params[:address][:billing_address][:id]
          }
          inventory_order = inventory_order_dao.create_new_inventory_order(new_purchase_order_params)
          inventory_order.inventory_order_payment = inventory_order_payment_service.new_inventory_order_payment
          inventory_order_id = INVENTORY_ORDER_HELPER.generate_order_id(inventory_order)
          inventory_order_dao.update_order_id(inventory_order_id, inventory_order)
        end
        return inventory_order
      end

      #
      # Execute create purchase Order functionality with Transactional block.
      # @param place_order_params [JSON] [Hash containing w/h, inventory and address hashes]
      #
      # @return [JSON] [Hash containing order and payment details]
      #
      def transactional_update_purchase_order(purchase_order_params)
        transactional_function = Proc.new do |args|
          return update_purchase_order(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: purchase_order_params
          })
        transaction_block.run();
      end

      #
      # update purchase order by adding order products to it & updating other references
      #
      # @param place_order_params [JSON] [Hash containing w/h, inventory and address hashes]
      #
      # @return [JSON] [Hash containing order and payment details]
      #
      def update_purchase_order(purchase_order_params)
        @is_inter_state = purchase_order_params[:is_inter_state]
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        references = validate_purchase_order_references(purchase_order_params)
        inventory_order = get_inventory_order(purchase_order_params[:id])
        inventory_order_initiated?(inventory_order)
        inventory_order = inventory_order_dao.update_inventory_order(inventory_order, purchase_order_params)
        inventory_order = update_products_to_inventory_order(inventory_order, purchase_order_params[:products])
        return inventory_order
      end

      #
      # Execute place purchase Order functionality with Transactional block.
      # @param place_order_params [JSON] [Hash containing w/h, inventory product and address hashes]
      #
      # @return [JSON] [Hash containing order and payment details]
      #
      def transactional_place_purchase_order(purchase_order_params)
        transactional_function = Proc.new do |args|
          return place_purchase_order(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: purchase_order_params
          })
        transaction_block.run();
      end

      #
      # place purchase order by applying CST & Octrai taxes on PO
      #
      # @param place_order_params [JSON] [Hash containing payment hash with CST & octrai]
      #
      # @return [JSON] [Hash containing order and payment details]
      #
      def place_purchase_order(purchase_order_params)
        inventory_order_payment_service = INVENTORY_ORDER_PAYMENT_SERVICE.new(@params)
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        file_service = FILE_SERVICE.new(@params)
        inventory_order = get_inventory_order(purchase_order_params[:id])
        inventory_order_initiated?(inventory_order)
        active_inventory_order_products_present?(inventory_order)
        inventory_order = inventory_order_dao.change_state(inventory_order, INVENTORY_ORDER_EVENTS::PLACE_PURCHASE_ORDER)
        pdf_details = PURCHASE_ORDER_HELPER.generate_purchase_order(inventory_order)
        if inventory_order.pdf_link.blank?
          pdf_link = file_service.upload_file_to_s3(pdf_details[:file_path], pdf_details[:filename])
          inventory_order = inventory_order_dao.update_inventory_order(inventory_order, {pdf_link: pdf_link[:path]})
        else
          file_service.update_file_to_s3(inventory_order.pdf_link, pdf_details[:file_path])
        end
        return inventory_order
      end

      #
      # Execute verify purchase Order functionality with Transactional block.
      # @param place_order_params [JSON] [Hash containing product & payment details]
      #
      # @return [JSON] [Hash containing order and payment details]
      #
      def transactional_verify_purchase_order(purchase_order_params)
        transactional_function = Proc.new do |args|
          return verify_purchase_order(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: purchase_order_params
          })
        transaction_block.run();
      end

      #
      # verify purchase order by adding qty received, updated mrp, cost_price & other payment details
      # & generating updated PO
      #
      # @param place_order_params [JSON] [Hash containing product & payment details]
      #
      # @return [JSON] [Hash containing order and payment details]
      #
      def verify_purchase_order(purchase_order_params)
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        file_service = FILE_SERVICE.new(@params)
        inventory_order = get_inventory_order(purchase_order_params[:id])
        inventory_order_received?(inventory_order)
        inventory_order = verify_products_in_inventory_order(inventory_order, purchase_order_params[:products])
        add_octrai_tax_in_inventory_order(inventory_order, purchase_order_params[:payment])
        inventory_order = inventory_order_dao.change_state(inventory_order, INVENTORY_ORDER_EVENTS::VERIFY_PURCHASE_ORDER)
        pdf_details = PURCHASE_ORDER_HELPER.generate_purchase_order(inventory_order)
        link = file_service.update_file_to_s3(inventory_order.pdf_link, pdf_details[:file_path])
        return inventory_order
      end

      #
      # Execute stock purchase Order functionality with Transactional block.
      # @param place_order_params [JSON] [Hash containing product details]
      #
      # @return [JSON] [Hash containing order details]
      #
      def transactional_stock_purchase_order(purchase_order_params)
        transactional_function = Proc.new do |args|
          return stock_purchase_order(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: purchase_order_params
          })
        transaction_block.run();
      end

      #
      # Function to get purchase order using it's ID
      #
      def get_purchase_order(inventory_order_id)
        inventory_order = get_inventory_order(inventory_order_id)
        return inventory_order
      end

      #
      # stock purchase order by stocking qty received in warheouse for the corresponding WBP
      #
      # @param purchase_order_params [JSON] [Hash containing product details]
      #
      # @return [JSON] [Hash containing order details]
      #
      def stock_purchase_order(purchase_order_params)
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        inventory_order = get_inventory_order(purchase_order_params[:id])
        inventory_order_verified?(inventory_order)
        warehouse = inventory_order_belongs_to_warehouse(inventory_order, purchase_order_params[:warehouse][:id])
        inventory_order = stock_inventory_order_products(inventory_order, warehouse)
        inventory_order = inventory_order_dao.change_state(inventory_order, INVENTORY_ORDER_EVENTS::STOCK_PURCHASE_ORDER)
        return inventory_order
      end

      #
      # Service function to change the state of purchase order to received
      #
      def change_purchase_order_state(request)
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        inventory_order = get_inventory_order(request[:id])
        inventory_order = inventory_order_dao.change_state(inventory_order, request[:event])
        return inventory_order
      end

      #
      # Function to validate if given references for a PO exist and are correct
      #
      def validate_purchase_order_references(purchase_order_params)
        inventory = @inventory_service.get_inventory_by_id(purchase_order_params[:inventory][:id]) if purchase_order_params[:inventory].present? && purchase_order_params[:inventory][:id].present?
        warehouse = @warehouse_service.get_warehouse(purchase_order_params[:warehouse][:id]) if purchase_order_params[:warehouse].present? && purchase_order_params[:warehouse][:id].present?
        if purchase_order_params[:address].present?
          billing_address = @warehouse_address_service.get_warehouse_address_by_id(purchase_order_params[:address][:billing_address][:id]) if purchase_order_params[:address][:billing_address][:id].present?
          shipping_address = @warehouse_address_service.get_warehouse_address_by_id(purchase_order_params[:address][:shipping_address][:id]) if purchase_order_params[:address][:shipping_address][:id].present?
          if purchase_order_params[:warehouse][:id].present?
            @warehouse_address_service.validate_address_owner(billing_address, purchase_order_params[:warehouse][:id])
            @warehouse_address_service.validate_address_owner(shipping_address, purchase_order_params[:warehouse][:id])
          end
        end
      end

      #
      # Function to check if the Purchase order exists for a pair of warehouse & inventory
      #
      def initiated_inventory_order_exisits(params)
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        return inventory_order_dao.initiated_inventory_warehouse_order(params)
      end

      #
      # Funciton to get Inventory order using it's id
      #
      def get_inventory_order(id)
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        return inventory_order_dao.get_inventory_order_by_id(id)
      end

      #
      # Function to check if the Purchase order is in initiates state
      #
      def inventory_order_initiated?(inventory_order)
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        return inventory_order_dao.inventory_order_initiated?(inventory_order)
      end

      #
      # Function to check if the Purchase order is in received state
      #
      def inventory_order_received?(inventory_order)
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        return inventory_order_dao.inventory_order_received?(inventory_order)
      end

      #
      # Function to check if the Purchase order is in verified state
      #
      def inventory_order_verified?(inventory_order)
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        return inventory_order_dao.inventory_order_verified?(inventory_order)
      end

      #
      # Function to inventory order with all the inventory order products
      #
      def update_products_to_inventory_order(inventory_order, product_params)
        inventory_order_products_service = INVENTORY_ORDER_PRODUCTS_SERVICE.new(@params)
        product_params.each do |inventory_order_product|
          validate_inventory_order_product(inventory_order_product)
          update_order_product_to_inventory_order(inventory_order, inventory_order_product)
        end
        return inventory_order
      end

      #
      # Function to update the inventory order products
      #
      def update_order_product_to_inventory_order(inventory_order, inventory_order_product)
        ibp_id = inventory_order_product[:inventory_brand_pack_id]
        quantity = inventory_order_product[:quantity_ordered].to_i
        inventory_brand_pack_service = INVENTORY_BRAND_PACK_SERVICE.new(@params)
        inventory_brand_pack = inventory_brand_pack_service.get_active_inventory_brand_pack_by_id(ibp_id)
        if inventory_brand_pack.inventory != inventory_order.inventory
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WRONG_INVENTORY_ORDER_PRODUCT)
        end
        # Update ibp to Inventory Order product
        inventory_order_products = update_ibp_to_order_product(inventory_order, inventory_brand_pack, quantity)

        # Update purchase order payment details
        update_purchase_order_payment_details(inventory_order_products, inventory_order.inventory_order_payment)
        return inventory_order
      end

      #
      # function to fetch IBP for inventory order products and update it to Inventory order product
      #
      def update_ibp_to_order_product(inventory_order, inventory_brand_pack, quantity)
        inventory_order_products_service = INVENTORY_ORDER_PRODUCTS_SERVICE.new(@params)
        inventory_brand_pack_dao = INVENTORY_BRAND_PACK_DAO.new(@params)
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        # Check if Product exists in cart or not. If exists, then update the same
        # OrderProduct or else create a new one
        inventory_order_product = inventory_order_dao.ibp_exists_in_purchase_order?({
          inventory_order: inventory_order,
          inventory_brand_pack: inventory_brand_pack
        })
        pricing = inventory_brand_pack_dao.get_pricing_for_ibp(inventory_brand_pack)
        service_tax_value = pricing.net_landing_price * pricing.service_tax * 0.01
        # If PO is inter state the we apply CST tax otherwise VAt tax
        if @is_inter_state
          vat_value = BigDecimal('0')
          cst_value = pricing.net_landing_price * pricing.cst * 0.01
        else
          vat_value = pricing.net_landing_price * pricing.vat * 0.01
          cst_value = BigDecimal('0')
        end
        octrai_value = (pricing.net_landing_price + cst_value + vat_value) * pricing.octrai * 0.01
        previous_order_product = nil
        updated_order_product = nil
        previous_quantity = 0
        if inventory_order_product.blank?
          # Create order product only when quantity is non zero
          if quantity != 0
            updated_order_product = inventory_order_products_service.create_inventory_order_products({
              inventory_order: inventory_order,
              inventory_brand_pack: inventory_brand_pack,
              quantity_ordered: quantity,
              mrp: inventory_brand_pack.brand_pack.mrp,
              cost_price: pricing.net_landing_price,
              vat: pricing.vat,
              vat_tax_value: vat_value,
              service_tax: pricing.service_tax,
              service_tax_value: service_tax_value,
              cst: pricing.cst,
              cst_value: cst_value,
              octrai: pricing.octrai,
              octrai_value: octrai_value
            })
          else
            return {
              previous_order_product: previous_order_product,
              updated_order_product: updated_order_product,
              previous_quantity: previous_quantity,
              updated_quantity: quantity
            }
          end
        else
          previous_order_product = inventory_order_product.dup
          previous_quantity = previous_order_product.quantity_ordered
          updated_order_product = inventory_order_products_service.update_inventory_order_products({
            inventory_order_product: inventory_order_product,
            args: {
              inventory_brand_pack: inventory_brand_pack,
              quantity_ordered: quantity,
              mrp: inventory_brand_pack.brand_pack.mrp,
              cost_price: pricing.net_landing_price,
              vat: pricing.vat,
              vat_tax_value: vat_value,
              service_tax: pricing.service_tax,
              service_tax_value: service_tax_value,
              cst: pricing.cst,
              cst_value: cst_value,
              octrai: pricing.octrai,
              octrai_value: octrai_value
            }
          })
        end
        return { 
          previous_order_product: previous_order_product,
          updated_order_product: updated_order_product,
          previous_quantity: previous_quantity,
          updated_quantity: updated_order_product.quantity_ordered
        }
      end

      #
      # Function to update payment details of IO after adding Inventory order product
      #
      def update_purchase_order_payment_details(inventory_order_products, inventory_order_payment)
        inventory_order_payment_service = INVENTORY_ORDER_PAYMENT_SERVICE.new(@params)
        inventory_order_payment_service.update_purchase_order_payment_details(inventory_order_payment, inventory_order_products)
      end

      #
      # Function validate the params for inventory order products
      #
      def validate_inventory_order_product(inventory_order_product)
        if inventory_order_product[:quantity_ordered].blank? || inventory_order_product[:quantity_ordered].to_i < 0
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::FIELD_MUST_PRESENT % {field: 'Inventory Order Product quantity'})
        end
        if inventory_order_product[:inventory_brand_pack_id].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::FIELD_MUST_PRESENT % {field: 'Inventory Brand Pack ID'})
        end
      end

      #
      # Function to validate all params for verify order produts API
      #
      def verify_products_in_inventory_order(inventory_order, product_params)
        @is_inter_state = inventory_order.is_inter_state
        inventory_order_products_service = INVENTORY_ORDER_PRODUCTS_SERVICE.new(@params)
        product_params.each do |inventory_order_product|
          inventory_order_product[:mrp] = BigDecimal(inventory_order_product[:mrp])
          inventory_order_product[:cost_price] = BigDecimal(inventory_order_product[:cost_price])
          verify_inventory_order_product_params(inventory_order_product)
          iop = verify_order_product_in_inventory_order(inventory_order, inventory_order_product)
          # Update iop to Inventory Order product
          inventory_order_products = update_product_in_inventory_order(inventory_order_product, iop, inventory_order_product[:quantity_received])
          # Update purchase order payment details
          update_purchase_order_payment_details(inventory_order_products, inventory_order.inventory_order_payment)
        end
        return inventory_order
      end

      #
      # Function to validate params of inevntory order product to be updated for verification
      #
      def verify_inventory_order_product_params(inventory_order_product)
        if inventory_order_product[:quantity_received].blank? || inventory_order_product[:quantity_received].to_i < 0
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::FIELD_MUST_PRESENT % {field: 'Inventory Order Product quantity'})
        end
        if inventory_order_product[:id].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::FIELD_MUST_PRESENT % {field: 'Inventory Order Product ID'})
        end
        if inventory_order_product[:mrp].blank? || inventory_order_product[:mrp] < 0
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::FIELD_MUST_PRESENT % {field: 'MRP'})
        end
        if inventory_order_product[:cost_price].blank? || inventory_order_product[:cost_price] < 0
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::FIELD_MUST_PRESENT % {field: 'Cost price'})
        end
      end

      #
      # Funtion to verify if IOP belongs to the inventory order
      #
      def verify_order_product_in_inventory_order(inventory_order, order_product_params)
        iop_id = order_product_params[:id]
        quantity = order_product_params[:quantity_received]
        inventory_order_product = verify_product_belongs_to_inventory_order(inventory_order, iop_id)
        return inventory_order_product
      end

      #
      # Function to verify referencing of Inventory Product and Order
      #
      def verify_product_belongs_to_inventory_order(inventory_order, iop_id)
        inventory_order_products_service = INVENTORY_ORDER_PRODUCTS_SERVICE.new(@params)
        inventory_order_product = inventory_order_products_service.get_inventory_order_product_by_id(iop_id)
        if inventory_order_product.inventory_order != inventory_order
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WRONG_INVENTORY_ORDER_PRODUCT_REFERENCE)
        end
        return inventory_order_product
      end

      #
      # Function to update the products for verification with quantity received & if MRP or cost price have chnged
      #
      def update_product_in_inventory_order(order_product_params, inventory_order_product, quantity)
        inventory_order_products_service = INVENTORY_ORDER_PRODUCTS_SERVICE.new(@params)
        inventory_brand_pack_dao = INVENTORY_BRAND_PACK_DAO.new(@params)
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        if order_product_params[:cost_price].present?
          if @is_inter_state
            vat_tax_value = BigDecimal('0')
            cst_value = order_product_params[:cost_price] * inventory_order_product.cst * 0.01
          else
            vat_tax_value = order_product_params[:cost_price] * inventory_order_product.vat * 0.01
            cst_value = BigDecimal('0')
          end
        end
        previous_order_product = inventory_order_product.dup
        updated_order_product = inventory_order_products_service.update_inventory_order_products({
          inventory_order_product: inventory_order_product,
          args: {
            quantity_received: quantity,
            mrp: order_product_params[:mrp],
            cost_price: order_product_params[:cost_price],
            cst_value: cst_value,
            vat_tax_value: vat_tax_value
          }
        })
        return { 
          previous_order_product: previous_order_product,
          updated_order_product: updated_order_product,
          previous_quantity: previous_order_product.quantity_ordered,
          updated_quantity: updated_order_product.quantity_received
         }
      end

      #
      # Function add octrai tax to the inventory order payment
      #
      def add_octrai_tax_in_inventory_order(inventory_order, payment_params)
        return if payment_params.blank?
        payment_params[:total_octrai] = BigDecimal(payment_params[:total_octrai]) if payment_params[:total_octrai].present?
        if payment_params[:total_octrai].present? && payment_params[:total_octrai] < 0
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::FIELD_NON_NEGATIVE % {field: 'Octrai Tax'})
        end
        inventory_order_payment_service = INVENTORY_ORDER_PAYMENT_SERVICE.new(@params)
        inventory_order_payment_service.add_octrai_tax_in_inventory_order(inventory_order.inventory_order_payment, payment_params)
      end

      #
      # get_all_orders function
      # Timeline:
      #   - to_time or current time
      #   - from_time or (till_time - 1)
      #
      # @param pagination_params [Hash] pagination params can be passed, else will be treated blank
      #
      # @return [Array] returns all orders
      #
      def get_all_orders_in_interval(request_params = {})
        default_to_time = Time.zone.now
        # Initialize and validate time params
        time_params = INVENTORY_ORDER_HELPER.initialize_order_time_filters(
          {
            from_time: request_params[:from_time],
            to_time: request_params[:to_time]
          },
          default_to_time)
        inventory_order_dao = INVENTORY_ORDER_DAO.new(@params)
        response = get_all_elements_between_interval(inventory_order_dao, request_params, time_params)
        return response
      end

      #
      # Function to check active inventory order products present in the purchase order
      #
      def active_inventory_order_products_present?(inventory_order)
        active_inventory_order_products = get_active_inventory_order_products(inventory_order)
        if active_inventory_order_products.blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::NO_INVENTORY_ORDER_PRODUCT_ADDED)
        end
        return active_inventory_order_products
      end

      #
      # Function to get active inventory order products in the purchase order
      #
      def get_active_inventory_order_products(inventory_order)
        inventory_order_products_service = INVENTORY_ORDER_PRODUCTS_SERVICE.new(@params)
        active_inventory_order_products = inventory_order_products_service.get_active_inventory_order_products(inventory_order)
        return active_inventory_order_products
      end

      #
      # Function to verify if inventory order belongs to this warehouse
      #
      def inventory_order_belongs_to_warehouse(inventory_order, warehouse_id)
        warehouse = @warehouse_service.get_warehouse(warehouse_id)
        if inventory_order.warehouse != warehouse
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::NOT_WAREHOUSE_INVENTORY_ORDER)
        end
        return warehouse
      end

      #
      # Function to validate all params for stock order products API
      #
      def stock_inventory_order_products(inventory_order, warehouse)
        inventory_order_products_service = INVENTORY_ORDER_PRODUCTS_SERVICE.new(@params)
        active_inventory_order_products = get_active_inventory_order_products(inventory_order)
        date_of_purchase = INVENTORY_ORDER_HELPER.get_purchase_order_date(inventory_order)
        wbp_service = WBP_SERVICE.new(@params)
        active_inventory_order_products.each do |inventory_order_product|
          wbp = get_wbp_for_ibp_and_warehouse(inventory_order_product.inventory_brand_pack.brand_pack, warehouse)
          if wbp.blank?
            wbp = create_wbp(inventory_order, inventory_order_product)
          end
          wbp_service.stock_warehouse_brand_pack({
            id: wbp.id,
            quantity: inventory_order_product.quantity_received,
            date_of_purchase: date_of_purchase
            })
        end
        return inventory_order
      end

      #
      # Function to fetch WBP corresponding to IBP & warehouse
      #
      def get_wbp_for_ibp_and_warehouse(brand_pack, warehouse)
        wbp_service = WBP_SERVICE.new(@params)
        return wbp_service.get_wbp_by_parameters(brand_pack, warehouse)
      end

      #
      # Function to create WBP if corresponding to IBP & Warehouse
      #
      def create_wbp(inventory_order, inventory_order_product)
        wbp_service = WBP_SERVICE.new(@params)
        return wbp_service.create_warehouse_brand_pack(inventory_order, inventory_order_product)
      end
    end
  end
end

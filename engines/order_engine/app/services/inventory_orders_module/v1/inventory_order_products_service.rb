module InventoryOrdersModule

  module V1

    class InventoryOrderProductsService < BaseOrdersModule::V1::BaseDao

      INVENTORY_ORDER_PRODUCT_DAO = InventoryOrdersModule::V1::InventoryOrderProductsDao
      #
      # Initialize Inventory Order product Service with passed params
      #
      def initialize(params='')
        @params = params
      end
      
      #
      # Create a new Inventory Order Product
      #
      # @param params [JSON] [Hash containing quantity, IBP]
      #
      # @raise [RunTimeError] [if creation failed]
      def create_inventory_order_products(params)
        inventory_order_product_dao = INVENTORY_ORDER_PRODUCT_DAO.new(@params)
        inventory_order_product = inventory_order_product_dao.create_inventory_order_products(params)
        return inventory_order_product
      end

      # 
      # Update a inventory order product as per the passed attributes
      #
      # @param args [JSON] [Hash containing quantity,  or IBP]
      # @param order_product [Object] [Order product object]
      # 
      # @return [Object] [Order product]
      def update_inventory_order_products(update_params)
        args = update_params[:args]
        inventory_order_product = update_params[:inventory_order_product]
        inventory_order_product_dao = INVENTORY_ORDER_PRODUCT_DAO.new(@params)
        inventory_order_product = inventory_order_product_dao.update_inventory_order_products(args, inventory_order_product)
        return inventory_order_product
      end

      #
      # Function to fetch updated INventory order products
      #
      def get_active_inventory_order_products(inventory_order)
        inventory_order_product_dao = INVENTORY_ORDER_PRODUCT_DAO.new(@params)
        inventory_order_products = inventory_order_product_dao.get_active_inventory_order_products(inventory_order)
        return inventory_order_products
      end

      #
      # function to fetch inventory order products by it's ID
      #
      def get_inventory_order_product_by_id(inventory_order_product_id)
        inventory_order_product_dao = INVENTORY_ORDER_PRODUCT_DAO.new(@params)
        inventory_order_product = inventory_order_product_dao.get_inventory_order_product_by_id(inventory_order_product_id)
        return inventory_order_product
      end
    end
  end
end

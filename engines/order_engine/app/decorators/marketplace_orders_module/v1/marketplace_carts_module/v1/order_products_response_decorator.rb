#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    module MarketplaceCartsModule
      module V1
        # Model for order_products table
        class OrderProductsResponseDecorator < BaseOrdersModule::V1::BaseCartsModule::V1::BaseResponseDecorator

          CATEGORIZATION_API_RESPONSE_DECORATOR = CategorizationModule::V1::MpspCategorizationApiResponseDecorator
          CONTENT = CommonModule::V1::Content


          #
          # Get Array of all order products with short details like order id and their respcive MPSP id
          #
          def self.get_order_products_array(order_products)
            if order_products.blank?
              return []
            end
            order_products_array = []
            order_products.each do |order_product|
              order_products_array.push(get_order_product_hash(order_product))
            end
            return order_products_array
          end

          #
          # Get order product hash with its short details like order id and their respcive MPSP id
          #
          def self.get_order_product_hash(order_product)
            if order_product.blank?
              return {}
            end
            return {
              id: order_product.id,
              quantity: order_product.quantity,
              product: {
                id: order_product.marketplace_selling_pack_id
              }
            }
          end

          #
          # Get Array of all order products with their COMPLETE details
          #
          def self.get_complete_order_products_array(order_products)
            if order_products.blank?
              return []
            end
            order_products_array = []
            order_products.each do |order_product|
              order_products_array.push(get_complete_order_product_details(order_product))
            end
            return order_products_array
          end

          #
          # Get order product hash with its COMPLETE details
          #
          def self.get_complete_order_product_details(order_product)
            if order_product.blank?
              return {}
            end
            display_product_hash = CATEGORIZATION_API_RESPONSE_DECORATOR.create_display_product_hash(order_product.marketplace_selling_pack)
            order_product_hash = {
              id: order_product.id,
              quantity: order_product.quantity,
              product: display_product_hash
            }
            return order_product_hash
          end

        end
      end
    end
  end
end
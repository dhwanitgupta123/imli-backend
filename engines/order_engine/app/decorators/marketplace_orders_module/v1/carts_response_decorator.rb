module MarketplaceOrdersModule
  module V1
    class CartsResponseDecorator < BaseOrdersModule::V1::BaseResponseDecorator
      
      CARTS_SERVICE = MarketplaceOrdersModule::V1::CartService
      ORDER_PRODUCTS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsResponseDecorator
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      CART_HELPER = MarketplaceOrdersModule::V1::CartHelper

      def self.create_cart_details_response(response)
        cart = response[:cart]
        if cart.blank?
          return {}
        end
        # order_products = response[:order_products]
        # order_products_hash = ORDER_PRODUCTS_RESPONSE_DECORATOR.get_complete_order_products_array(order_products)
        # Fetch cart details with pricing info and merge order_products hash with it
        cart_hash = CART_HELPER.get_cart_complete_details(cart)

        return {
          payload: {
            cart: cart_hash
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

    end
  end
end
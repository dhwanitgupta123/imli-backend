module MarketplaceOrdersModule
	module V1
		class OrdersResponseDecorator < BaseOrdersModule::V1::BaseResponseDecorator

			CARTS_SERVICE = MarketplaceOrdersModule::V1::CartService
      TIME_SLOT_SERVICE = MarketplaceOrdersModule::V1::TimeSlotService
			RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      CART_HELPER = MarketplaceOrdersModule::V1::CartHelper
      PAYMENT_HELPER = MarketplaceOrdersModule::V1::PaymentHelper
      TRANSACTION_HELPER = TransactionsModule::V1::TransactionHelper
      ORDER_HELPER = MarketplaceOrdersModule::V1::OrderHelper
      PAYMENT_DAO = MarketplaceOrdersModule::V1::PaymentDao
      TIME_SLOT_DAO = MarketplaceOrdersModule::V1::TimeSlotDao
      TIME_SLOT_MAPPER = MarketplaceOrdersModule::V1::TimeSlotMapper
      ORDER_STATE_TRANSITION_HELPER = MarketplaceOrdersModule::V1::OrderStateTransitionHelper
      INVOICE_HELPER = MarketplaceOrdersModule::V1::InvoiceHelper
      PAYMENT_MAPPER = MarketplaceOrdersModule::V1::PaymentMapper
      ORDER_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::OrderEvents
      ORDER_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
      DATE_TIME_UTIL = CommonModule::V1::DateTimeUtil
      ORDER_USER_TAGS = MarketplaceOrdersModule::V1::OrderUserTags

      #
      # Creat Order response with billing details and payment_details hash
      # Payment_details contain complete payment details with all the modes
      # and its respective computed total & savings
      #
			def self.create_order_and_payment_details_response(args)
        order = args[:order]
        payment_details = args[:payment_details]
        billing_details = args[:billing_details]
        # Fetch cart hash with its pricing details
        # TO-DO: No need for this after putting pricing details in ORDER hash
        # But, to make it backward compatible with Android POJO, need to keep it for now
        short_cart_with_pricing = CART_HELPER.get_short_cart_details_with_pricing(order.cart)
        available_slots = TIME_SLOT_SERVICE.new.get_available_slots_for_order(order)
        order_hash = ORDER_HELPER.get_order_details(order)
        order_hash = order_hash.merge({
          delivery_charges: billing_details[:delivery_charges],
          grand_total: billing_details[:grand_total],
          discount: billing_details[:discount],
          billing_amount: billing_details[:billing_amount],
          cart: short_cart_with_pricing
          })

        response = {
          payload: {
            order: order_hash,
            available_slots: available_slots,
            payment_details: payment_details    # Payment MODE details
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
			end

      def self.create_scheduled_order_response(order)
        order_hash = ORDER_HELPER.get_order_details(order)

        response = {
          payload: {
            order: order_hash
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # Create ORDERS response.
      # This is specific response for the Panels
      #
      # @param orders [Array] [Array of Order model objects]
      # @param page_count [Integer] [page count]
      #
      def self.create_orders_response(orders, page_count)
        orders_array = ORDER_HELPER.create_orders_array_with_complete_details(orders)
        return {
          payload: {
            orders: orders_array
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: page_count
          }
        }
      end

      #
      # Creaete single order response for PANELS
      # which contain order states, order date, allowed order events, etc.
      #
      # @param order [Object] [Order model object]
      #
      # @return [JSON] [Response hash]
      #
      def self.create_single_order_response(order)
        #order_hash = INVOICE_HELPER.get_invoice_hash_for_mail_template(order, true)
        order_hash = ORDER_HELPER.get_complete_order_details(order)
        products_hash = INVOICE_HELPER.update_order_product_specification(order)
        allowed_order_events = ORDER_EVENTS.get_allowed_events(order)
        allowed_payment_modes = PAYMENT_HELPER.get_offline_modes_with_gateways
        order_date = ORDER_HELPER.get_order_date(order)
        order_context = ORDER_HELPER.get_order_context(order)
        allowed_order_user_tags = ORDER_USER_TAGS::ORDER_USER_TAGS

        # Fetch transaction details for order payment
        payment_dao = PAYMENT_DAO.new
        order_payment = payment_dao.get_orders_payment_as_per_its_mode(order)
        transaction_details = TRANSACTION_HELPER.get_transaction_details_for_payment(order_payment)
        active_payments = payment_dao.get_active_payments_for_order_in_ascending_order(order)
        order_payments = PAYMENT_MAPPER.map_payments_to_array(active_payments)

        # Fetch available time slots
        time_slot_dao = TIME_SLOT_DAO.new
        active_time_slots = time_slot_dao.get_all_active_slots
        allowed_time_slots = TIME_SLOT_MAPPER.map_time_slots_to_array(active_time_slots)
        min_preferred_date = DATE_TIME_UTIL.convert_date_time_to_epoch(Time.zone.now)
        max_preferred_date = DATE_TIME_UTIL.convert_date_time_to_epoch(Time.zone.now + 7.days)

        order_hash = order_hash.merge({
          order_date: order_date,
          product: products_hash,
          allowed_events: allowed_order_events,
          allowed_payment_modes: allowed_payment_modes,
          allowed_time_slots: allowed_time_slots,
          min_preferred_date: min_preferred_date,
          max_preferred_date: max_preferred_date,
          transaction_details: transaction_details,
          order_context: order_context,
          order_payments: order_payments,
          allowed_order_user_tags: allowed_order_user_tags
        })
        return {
          payload: {
            order: order_hash
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      #
      # Create response of order details along with its payment object only
      # No cart specific parameters will be sent
      # Short response contains Order, its basic attributes and its payment object details
      #
      def self.create_short_orders_response(args)
        orders = args[:orders]
        response = {
          payload: {
            orders: ORDER_HELPER.create_short_orders_array(orders)
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # Creat short order response with nearest lower OrderState
      # Short response contains Order, its basic attributes and its payment object details
      #
      def self.create_short_orders_response_with_minified_states(args)
        orders = args[:orders]
        response = {
          payload: {
            orders: ORDER_HELPER.create_short_orders_array_with_minified_states(orders)
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # Creat complete order details response
      # Complete OrderDetails contain its basic attributes, complete payment object,
      # and associated cart details
      #
      def self.create_complete_orders_response(args)
        order = args[:order]
        return {} if order.blank?
        cart_complete_details = CART_HELPER.get_cart_complete_details(order.cart)
        # Get successfull payment of Order OR pending (in case of offline payment modes)
        payment_dao = PAYMENT_DAO.new
        order_payment = payment_dao.get_orders_payment_as_per_its_mode(order)
        payment_details = PAYMENT_HELPER.get_payment_hash(order_payment)
        # Fetch normal ORDER details and replace short cart with complete CART details
        order_hash = ORDER_HELPER.get_order_details(order)
        order_hash = order_hash.merge({
          cart: cart_complete_details
          })

        response = {
          payload: {
            order: order_hash
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # Create aggregated products response
      #
      # @param aggregated_products [Array] [Array of aggregated products hash]
      #
      # @return [JSON] [Response hash]
      #
      def self.create_aggregated_products_response(aggregated_products)
        response = {
          payload: {
            aggregated_products: aggregated_products
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      def self.create_edit_order_time_slot_response(response)
        response = {
          payload: {
            order: response[:order]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      def self.create_edit_order_address_response(response)
        response = {
          payload: {
            order: response[:order]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      def self.create_edit_order_cart_response(response)
        response = {
          payload: {
            order: response[:order]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

			#
      # Create response of order packing details
      #
      def self.create_order_packing_details_response(args)
        order_packing_link = args[:order_packing_details]
        response = {
          payload: {
            packing_link: order_packing_link
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

			#
      # Create response of invoice details
      #
			def self.create_invoice_link_response(args)
				invoice_link = args[:invoice_link]
        response = {
          payload: {
            invoice_link: invoice_link
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
			end

			#
			# Create orders stats response
			#
			# @param orders_stats [Hash] [Hash of order stats hash]
			#
			# @return [JSON] [Response hash]
			#
			def self.create_order_stats_response(orders_stats)
				response = {
					payload: {
						stats: orders_stats
					},
					response: RESPONSE_CODES_UTIL::SUCCESS
				}
				return response
			end

      #
      # Order context response
      #
      def self.create_order_context_response(order_context)
        {
          payload: {
            order_context: order_context
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

		end
	end
end

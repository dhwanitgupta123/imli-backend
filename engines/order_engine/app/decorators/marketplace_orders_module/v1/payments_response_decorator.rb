module MarketplaceOrdersModule
  module V1
    class PaymentsResponseDecorator < BaseOrdersModule::V1::BaseResponseDecorator

      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      PAYMENT_DAO = MarketplaceOrdersModule::V1::PaymentDao
      PAYMENT_HELPER = MarketplaceOrdersModule::V1::PaymentHelper
      ORDER_HELPER = MarketplaceOrdersModule::V1::OrderHelper

      #
      # Get payment hash details which contains Order and its Successfull payment only
      #
      def self.create_response_payment_hash(args)
        payment = args[:payment]
        return {} if payment.blank? || payment.order.blank?
        return {
          payload: {
            order: ORDER_HELPER.get_order_details(payment.order)  # Order internally contains payment object hash
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      #
      # Get payment details which has Order and its initiated/pending payment
      #
      def self.create_response_initiate_payment_hash(args)
        payment = args[:payment]
        return {} if payment.blank? || payment.order.blank?
        # payment_dao = PAYMENT_DAO.new({})
        # pending_payment = payment_dao.get_pending_payment_for_order(payment.order)
        pending_payment_hash = PAYMENT_HELPER.get_payment_hash(payment)
        # Fetch basic order details and merge it with pending payment object
        order_hash = ORDER_HELPER.get_order_details(payment.order)
        order_hash = order_hash.merge({
          payment: pending_payment_hash
          })
        return {
          payload: {
            order: order_hash  # Order internally contains pending payment object hash
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end


    end
  end
end
module MarketplaceOrdersModule
  module V1
    class TimeSlotsResponseDecorator < BaseOrdersModule::V1::BaseResponseDecorator

      TIME_SLOT_HELPER = MarketplaceOrdersModule::V1::TimeSlotHelper

      #
      # Response for add time slot api
      #
      def self.create_add_time_slot_response(response)
        return {
          payload: {
            time_slot: response[:time_slot]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_get_all_time_slots_response(response)
        return {
          payload: {
            time_slots: response[:time_slots]
          },
          meta: {
            page_count: response[:page_count]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_get_time_slot_response(response)
        return {
          payload: {
            time_slot: response[:time_slot]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_update_time_slot_response(response)
        return {
          payload: {
            time_slot: response[:time_slot]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_change_time_slot_state_response(response)
        return {
          payload: {
            message: 'State changed successfuly'
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end


    end
  end
end

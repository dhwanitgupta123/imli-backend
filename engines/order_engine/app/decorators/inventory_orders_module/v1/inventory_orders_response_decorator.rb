#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Class to create JSON response of Inventory Order
    #
    class InventoryOrdersResponseDecorator < BaseOrdersModule::V1::BaseResponseDecorator

      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      INVENTORY_ORDER_PAYMENT_HELPER = InventoryOrdersModule::V1::InventoryOrderPaymentHelper
      INVENTORY_ORDER_PRODUCTS_HELPER = InventoryOrdersModule::V1::InventoryOrderProductsHelper
      INVENTORY_ORDER_HELPER = InventoryOrdersModule::V1::InventoryOrderHelper


      #
      # Creat Order response with billing details and payment_details hash
      # Payment_details contain complete payment details with all the modes
      # and its respective computed total & savings
      #
      def self.create_inventory_order_details_response(inventory_order_hash)
        response = {
          payload: {
            inventory_order: inventory_order_hash
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # Create response of inventory order details
      # Short response contains Order, its basic attributes
      #
      def self.create_inventory_orders_response(inventory_orders, page_count)
        return {
          payload: {
            inventory_orders: inventory_orders
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: page_count
          }
        }
      end
    end
  end
end

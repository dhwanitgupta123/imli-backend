#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    module ModelStates

      module V1
        # Class for Order states
        class OrderStates

        	INITIATED             = { value: 1, label: 'INITIATED'}
        	PLACED                = { value: 2, label: 'PLACED' }
        	AT_SELLER             = { value: 3, label: 'AT SELLER' }
          AT_WAREHOUSE          = { value: 4, label: 'AT WAREHOUSE' }
          AT_FULFILLMENT_CENTER = { value: 5, label: 'AT FULFILLMENT_CENTER' }
          PACKED                = { value: 6, label: 'PACKED' }
          DISPATCHED            = { value: 7, label: 'DISPATCHED' }
          PAYMENT_FAILED        = { value: 8, label: 'PAYMENT FAILED' }
          DELIVERED             = { value: 9, label: 'DELIVERED' }
          RETURN_INITIATED      = { value: 10, label: 'RETURN INITIATED' }
          RETURN_REJECTED       = { value: 11, label: 'RETURN REJECTED' }
          RETURN_APPROVED       = { value: 12, label: 'RETURN APPROVED' }
          REFUND_INITIATED      = { value: 13, label: 'REFUND INITIATED' }
          REFUND_FAILED         = { value: 14, label: 'REFUND FAILED'}
          REFUND_SUCCESSFUL     = { value: 15, label: 'REFUND_SUCCESSFUL' }
          ABORTED               = { value: 16, label: 'ABORTED' }
          ORDER_CANCELLED       = { value: 17, label: 'CANCELLED BY USER' }
          PARTIALLY_REFUNDED    = { value: 18, label: 'PARTIALLY REFUNDED' }
          FULLY_REFUNDED        = { value: 19, label: 'REFUNDED' }
          FULFILLED             = { value: 20, label: 'FULFILLED' }
          FAILED                = { value: 21, label: 'FAILED' }
          CANCELLED_BY_ADMIN    = { value: 22, label: 'CANCELLED BY ADMIN'}
          COMPLETED             = { value: 23, label: 'COMPLETED'}

          # Shrunk States which is to be shown on frontend/App to User
          # NOTE: IT SHOULD ALWAYS BE IN SORTED ORDER (so that searching is easy and correct)
          MINIFIED_STATES = [INITIATED, PLACED, DISPATCHED, DELIVERED, ORDER_CANCELLED, FULLY_REFUNDED, FAILED]

          # Allowed states to be Polled for in Live Order Panel
          ALLOWED_POLLING_STATES = [PLACED, PACKED, DISPATCHED, DELIVERED, FULFILLED]
          #
          # Get order state corresponding to passed value
          #
          # @param order_state_value [Integer] [Value of the state]
          #
          # @return [JSON] [Hash of order state]
          #
          def self.get_order_state_by_value(order_state_value)
            state = {}
            array_index = 0
            self.constants.each_with_index do |var, index|
              array_index = index
              state = self.const_get(var)
              # Skip if an Hash is not defined as value
              # Used for Minified_States, Final_States
              next if !state.kind_of?(Hash)
              # Break the loop as we found the matched state
              break if state[:value].to_i == order_state_value.to_i
            end
            # Return empty hash if end of the MAP encountered
            return {} if self.constants.length == array_index + 1
            return state
          end

          #
          # Get all possible ORDER states
          # Skip clubbed states (Array of states)
          #
          # @return [type] [description]
          def self.get_all_order_states
            order_states_array = []
            self.constants.each do |key|
              value = self.const_get(key)
              # Skip if an Hash is not defined as value
              # Used for Minified_States, Final_States
              next if !value.kind_of?(Hash)

              order_states_array.push(value)
            end
            return order_states_array
          end

          #
          # Get minified state corresponding to given state,
          # Minified states are the states which need to be displayed in the App
          # (or all easily understandable/relevant to the customer)
          # Below we are creating a Many-to-One MAP between states
          #
          # Forex: For customer, only Delivered and Refunded state matters.
          # RETURN_INITIATED, RETURN_APPROVED, REFUND_INITIATED --> DELIVERED
          # FULLY_REFUNDED, PARTIALLY_REFUNDED --> REFUNDED
          #
          # @param order_state [JSON] [Hash of order state, id and label]
          #
          # @return [JSON] [Hash of minified state, id and label]
          #
          def self.get_minified_state(order_state)
            return order_state if order_state.blank?
            minified_state = order_state
            case order_state
            when INITIATED, PAYMENT_FAILED
              minified_state = INITIATED
            when PLACED, AT_SELLER, AT_WAREHOUSE, AT_FULFILLMENT_CENTER, PACKED
              minified_state = PLACED
            when DISPATCHED
              minified_state = DISPATCHED
            when DELIVERED, FULFILLED, RETURN_INITIATED, RETURN_APPROVED, RETURN_REJECTED, REFUND_INITIATED, REFUND_SUCCESSFUL, REFUND_FAILED
              minified_state = DELIVERED
            when ORDER_CANCELLED, CANCELLED_BY_ADMIN
              minified_state = ORDER_CANCELLED.merge({label: 'CANCELLED'})
            when PARTIALLY_REFUNDED, FULLY_REFUNDED
              minified_state = FULLY_REFUNDED.merge({label: 'REFUNDED'})
            when FAILED
              minified_state = FAILED
            end
            return minified_state
          end

   	    end
      end
    end
  end
end

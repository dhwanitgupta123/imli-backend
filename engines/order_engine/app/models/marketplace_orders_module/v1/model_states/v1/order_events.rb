#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    module ModelStates
      module V1
        # Class for Order events
        class OrderEvents

        	PLACE                          = 1
          PAYMENT_FAILED                 = 2
          APPROVED_BY_MARKETPLACE        = 3
          APPROVED_BY_SELLER             = 4
          APPROVED_BY_THIRD_PARTY_SELLER = 5
          ABORT                          = 6
          APPROVED_BY_WAREHOUSE          = 7
          APPROVED_BY_FULFILLMENT_CENTER = 8
          DISPATCH                       = 9
          DELIVER                        = 10
          NO_RETRY_PAYMENT               = 11
          RETRY_SUCCESSFUL               = 12
          FULFILLMENT_SUCCESSFUL         = 13
          RETURN_ORDER                   = 14
          RETURN_REJECTED                = 15
          RETURN_APPROVED                = 16
          RETURN_CANCELLED               = 17
          INITIATE_REFUND                = 18
          REFUND_FAILED                  = 19
          REFUND_SUCCESSFUL              = 20
          RETRY_REFUND                   = 21
          REFUND_PARTIAL                 = 22
          REFUND_FULLY                   = 23
          CANCEL_ORDER                   = 25
          PACKING_DONE                   = 26
          FORCE_CANCEL                   = 27
          COMPLETE_ORDER                 = 28

          #
          # These are current events that are in use by panel to change order state
          #
          ALLOWED_EVENTS = [
            CANCEL_ORDER, 
            DISPATCH, 
            DELIVER, 
            NO_RETRY_PAYMENT, 
            RETRY_SUCCESSFUL,
            FULFILLMENT_SUCCESSFUL,
            PACKING_DONE,
            FORCE_CANCEL,
            COMPLETE_ORDER
          ]

          #
          # Get all allowed events for a particular Order
          #
          # @param order [Object] [Order ]
          # 
          # @return [type] [description]
          #
          def self.get_allowed_events(order)
            return [] if order.blank?
            allowed_actual_events = order.current_state.events.keys
            return convert_allowed_actual_event_to_event_ids(allowed_actual_events)
          end

          private

          #
          # Fetch map of WorkflowEvents to its respective Hash (id and label)
          #
          def self.event_to_id_map
            event_map = {
              :place => { value: PLACE, label: 'PLACE ORDER' },
              :payment_failed => { value:  PAYMENT_FAILED, label: 'PAYMENT FAILED' },
              :approved_by_marketplace => { value:  APPROVED_BY_MARKETPLACE, label: 'APPROVED BY MARKETPLACE' },
              :approved_by_seller => { value:  APPROVED_BY_SELLER, label: 'APPROVED BY SELLER' },
              :approved_by_third_party_seller => { value:  APPROVED_BY_THIRD_PARTY_SELLER, label: 'APPROVED BY THIRD PARTY SELLER' },
              :abort => { value:  ABORT, label: 'ABORT THIS ORDER' },
              :approved_by_warehouse => { value:  APPROVED_BY_WAREHOUSE, label: 'APPROVED BY WAREHOUSE' },
              :approved_by_fulfillment_center => { value:  APPROVED_BY_FULFILLMENT_CENTER, label: 'APPROVED BY FULFILLMENT CENTER' },
              :dispatch => { value:  DISPATCH, label: 'ORDER DISPATCHED' },
              :deliver => { value:  DELIVER, label: 'ORDER DELIVERED' },
              :no_retry_payment => { value:  NO_RETRY_PAYMENT, label: 'RETRY PAYMENT' },
              :retry_successful => { value:  RETRY_SUCCESSFUL, label: 'SUCCESSFULY RETRIED' },
              :fulfillment_successful => { value:  FULFILLMENT_SUCCESSFUL, label: 'FULFILLMENT SUCCESSFUL' },
              :return_order => { value:  RETURN_ORDER, label: 'RETURN ORDER' },
              :return_rejected => { value:  RETURN_REJECTED, label: 'REJECT RETURN REQUEST' },
              :return_approved => { value:  RETURN_APPROVED, label: 'APPROVE RETURN' },
              :return_cancelled => { value:  RETURN_CANCELLED, label: 'PLACE ORDER' },
              :initiate_refund => { value:  INITIATE_REFUND, label: 'INITIATE REFUND FOR ORDER' },
              :refund_failed => { value:  REFUND_FAILED, label: 'UNABLE TO REFUND' },
              :refund_successful => { value:  REFUND_SUCCESSFUL, label: 'SUCCESSFULY REFUNDED' },
              :retry_refund => { value:  RETRY_REFUND, label: 'RETRY REFUND' },
              :refund_partial => { value:  REFUND_PARTIAL, label: 'REFUND PARTIAL AMOUNT' },
              :refund_fully => { value:  REFUND_FULLY, label: 'REFUND FULL AMOUNT' },
              :cancel_order => { value:  CANCEL_ORDER, label: 'CANCEL ORDER' },
              :packing_done => { value:  PACKING_DONE, label: 'ORDER PACKAGING DONE' },
              :force_cancel => { value: FORCE_CANCEL, label: 'FORCE CANCEL BY ADMIN' },
              :complete_order => { value: COMPLETE_ORDER, label: 'COMPLETE ORDER'}
            }
            return event_map
          end

          #
          # Convert actual events to their respective IDs but
          # with filter over only ALLOWED Events
          # Ex: actual_events_map will be like [:placed, :refunded]
          # Final array: [{value: 2, lablel: 'PLACED'}, {value: '23', label: 'REFUNDED'}]
          #
          # @param actual_events_map [Array] [Array of Symbols]
          # 
          # @return [Array] [Array of Hash]
          #
          def self.convert_allowed_actual_event_to_event_ids(actual_events_map)
            return [] if actual_events_map.blank?
            order_events_array = []
            event_map = event_to_id_map
            allowed_events = ALLOWED_EVENTS
            actual_events_map.each do |event|
              # Event is already a symbol. ex: ':placed'
              map = event_map[event]
              if map.present? && allowed_events.include?(map[:value])
                order_events_array.push(event_map[event])
              end
            end
            return order_events_array
          end

   	    end
      end
    end
  end
end

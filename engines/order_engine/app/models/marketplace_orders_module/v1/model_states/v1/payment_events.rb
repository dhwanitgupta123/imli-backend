#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    module ModelStates

      module V1
        # Class for Payment Events
        class PaymentEvents

          INITIATE           = 1
          PAYMENT_FAIL       = 2
          PAYMENT_SUCCESS    = 3
          DISCARD_PAYMENT    = 4

        end
      end
    end
  end
end
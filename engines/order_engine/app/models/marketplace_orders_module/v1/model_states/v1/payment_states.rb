#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    module ModelStates

      module V1
        # Class for Payment states
        class PaymentStates

          PENDING     = 1
          FAILED      = 2
          SUCCESS     = 3
          DISCARDED   = 4

          #
          # Get display name for payment state
          #
          # @param state [Integer] [Mode of payment]
          #
          # @return [String] [Payment state display name]
          #
          def self.get_payment_state_display_name(state)
            display_name = ''
            return display_name if state.blank?

            case state.to_i
            when PENDING
              display_name = 'PENDING'
            when FAILED
              display_name = 'FAILED'
            when SUCCESS
              display_name = 'SUCCESS'
            when DISCARDED
              display_name = 'DISCARDED'
            end
            return display_name
          end

          #
          # Get payment display name as per payment state for Order invoice
          #
          # @param state [Integer] [Mode of payment]
          #
          # @return [String] [Payment state display name]
          #
          def self.get_payment_display_for_invoice(state)
            display_name = ''
            return display_name if state.blank?

            case state.to_i
            when PENDING
              display_name = 'PAYABLE'
            when FAILED
              display_name = 'FAILED'
            when SUCCESS
              display_name = 'PAID'
            when DISCARDED
              display_name = 'DISCARDED'
            end
            return display_name
          end

        end
      end
    end
  end
end

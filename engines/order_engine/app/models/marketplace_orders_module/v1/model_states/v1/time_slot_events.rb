#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    module ModelStates

      module V1
        class TimeSlotEvents

          ACTIVATE     = 1
          DEACTIVATE   = 2
          SOFT_DELETE  = 3

        end
      end
    end
  end
end
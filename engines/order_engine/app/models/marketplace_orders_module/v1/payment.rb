#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    # Model for payments table
    class Payment < ActiveRecord::Base

      PAYMENT_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::PaymentStates
      CONTENT = CommonModule::V1::Content
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      belongs_to :order

      validates :order, presence: true

      #
      # Workflow to define states of the Payment
      #
      # Initial State => PENDING
      #
      # State Diagram::
      #   PENDING --payment_fail--> FAILED 
      #   PENDING --payment_success--> SUCCESS
      #
      # * payment_fail, payment_success are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :pending, PAYMENT_STATES::PENDING do
          event :payment_fail, transitions_to: :failed
          event :payment_success, transitions_to: :success
          event :discard_payment, transitions_to: :discarded
        end
        state :failed, PAYMENT_STATES::FAILED do
          event :discard_payment, transitions_to: :discarded
        end
        state :success, PAYMENT_STATES::SUCCESS do
          event :discard_payment, transitions_to: :discarded
        end
        state :discarded, PAYMENT_STATES::DISCARDED
      end

      #
      # Trigger Payment state change
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end

    end
  end
end

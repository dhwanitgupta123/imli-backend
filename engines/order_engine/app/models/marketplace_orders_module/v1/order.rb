#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for marketplace orders module
  #
  module V1
    # Model for orders table
    class Order < ActiveRecord::Base

      ORDER_MODEL_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
      CART_DAO = MarketplaceOrdersModule::V1::CartDao
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      belongs_to :address, class_name: 'AddressModule::V1::Address'

      # Order can have many Payment objects associated with it
      # Ex: One Order can have 2 failed payments and 1 success
      has_many :payments, dependent: :destroy

      has_many :carts, dependent: :destroy

      has_one :warehouse_order, class_name: '::WarehouseOrdersModule::V1::WarehouseOrder'

      belongs_to :user, class_name: 'UsersModule::V1::User'
      belongs_to :time_slot

      validates :address, presence: true

      #validates :cart, presence: true
      validate :presence_of_atleast_one_cart

      validates :user, presence: true

      #
      # Computed Attribute for Order Model
      # Fetches FINAL cart linked with the order
      #
      def cart
        cart_dao = CART_DAO.new
        return cart_dao.get_final_cart(self.carts)
      end

      #
      # Validation to check presence of atleast one cart
      # If validation failed, then insert cart validation into errors array
      #
      # @return [Integer] [zero if validation failed, 1 if passed]
      #
      def presence_of_atleast_one_cart
        if carts.blank?
          errors.add(:carts, "can't be blank. Atleast one cart should be linked")
          return 0
        end
        return 1
      end

      #
      # Workflow to define states of the Order
      #
      # Initial State => Initiated
      #
      #
      # * initiate, place are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :initiated, ORDER_MODEL_STATES::INITIATED[:value] do
          event :place, transitions_to: :placed
          event :payment_failed, transitions_to: :payment_failed
        end
        state :placed, ORDER_MODEL_STATES::PLACED[:value] do
          event :approved_by_marketplace, transitions_to: :at_seller
          event :packing_done, transitions_to: :packed
          # If Imli Team wants to cancel the placed order manually from Panel
          event :cancel_order, transitions_to: :order_cancelled
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :at_seller, ORDER_MODEL_STATES::AT_SELLER[:value] do
          event :approved_by_seller, transitions_to: :at_warehouse
          event :approved_by_third_party_seller, transitions_to: :packed
          event :abort, transitions_to: :aborted
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :at_warehouse, ORDER_MODEL_STATES::AT_WAREHOUSE[:value] do
          event :approved_by_warehouse, transitions_to: :at_fulfillment_center
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :at_fulfillment_center, ORDER_MODEL_STATES::AT_FULFILLMENT_CENTER[:value] do
          event :approved_by_fulfillment_center, transitions_to: :packed
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :packed, ORDER_MODEL_STATES::PACKED[:value] do
          event :dispatch, transitions_to: :dispatched
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :dispatched, ORDER_MODEL_STATES::DISPATCHED[:value] do
          event :deliver, transitions_to: :delivered
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :payment_failed, ORDER_MODEL_STATES::PAYMENT_FAILED[:value] do
          event :no_retry_payment, transitions_to: :failed
          event :retry_successful, transitions_to: :placed
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :delivered, ORDER_MODEL_STATES::DELIVERED[:value] do
          event :complete_order, transitions_to: :completed
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :completed, ORDER_MODEL_STATES::COMPLETED[:value] do
          event :fulfillment_successful, transitions_to: :fulfilled
          event :return_order, transitions_to: :return_initiated
        end
        state :return_initiated, ORDER_MODEL_STATES::RETURN_INITIATED[:value] do
          event :return_rejected, transitions_to: :return_rejected
          event :return_approved, transitions_to: :return_approved
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :return_rejected, ORDER_MODEL_STATES::RETURN_REJECTED[:value] do
          event :return_cancelled, transitions_to: :fulfilled
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :return_approved, ORDER_MODEL_STATES::RETURN_APPROVED[:value] do
          event :initiate_refund, transitions_to: :refund_initiated
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :refund_initiated, ORDER_MODEL_STATES::REFUND_INITIATED[:value] do
          event :refund_failed, transitions_to: :refund_failed
          event :refund_successful, transitions_to: :refund_successful
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :refund_failed, ORDER_MODEL_STATES::REFUND_FAILED[:value] do
          event :retry_refund, transitions_to: :refund_initiated
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :refund_successful, ORDER_MODEL_STATES::REFUND_SUCCESSFUL[:value] do
          event :refund_partial, transitions_to: :partially_refunded
          event :refund_fully, transitions_to: :fully_refunded
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :aborted, ORDER_MODEL_STATES::ABORTED[:value] do
          event :initiate_refund, transitions_to: :refund_initiated
          event :cancel_order, transitions_to: :order_cancelled
          event :force_cancel, transition_to: :cancelled_by_admin
        end
        state :order_cancelled, ORDER_MODEL_STATES::ORDER_CANCELLED[:value]
        state :cancelled_by_admin, ORDER_MODEL_STATES::CANCELLED_BY_ADMIN[:value]
        state :partially_refunded, ORDER_MODEL_STATES::PARTIALLY_REFUNDED[:value]
        state :fully_refunded, ORDER_MODEL_STATES::FULLY_REFUNDED[:value]
        state :fulfilled, ORDER_MODEL_STATES::FULFILLED[:value]
        state :failed, ORDER_MODEL_STATES::FAILED[:value]

        #
        # This function will be called whenever a transition complete (only onSuccess)
        # Log the transition of states and also store the timestamp into some persistent
        # storage.
        # Wiki WorkFlow transition: https://github.com/geekq/workflow#on_transition
        #
        # TO-DO: For now, we are using a new column: state_attributes which will persist
        # stringify version of Hash<String, Hash<String, String>>. Ex: {'1': {'time': '13...'}, '2': {...} }
        # Here, we can't predict the depth of this structure. It can be more complex when we need to store
        # specific attributes for a particular state OR we need to store (multiple) timestamps for the states which 
        # are traversed multiple times.  Hence Need to move to a separate table for StateTransition, which
        # can persist state transitions along with their timestamp and other attributes
        #
        after_transition do |from, to, triggering_event, *event_args|
          time = Time.zone.now
          user_id = USER_SESSION[:user_id] if USER_SESSION.present?
          ApplicationLogger.debug("Order with id #{self.id} changed state from #{from} -> #{to}, using event: #{triggering_event} by user id: #{user_id} at time: #{time.to_s}")
          update_state_attributes(self.status, time)
        end

      end

      #
      # Update state attributes.
      # Currently, Store the timestamp in epoch corresponding to the transitioned state
      #
      # @param order_status [Integer] [current status of Order]
      # @param time [String] [stringify version of current epoch time]
      #
      def update_state_attributes(order_status, time)
        state_hash = {}
        attributes_hash = {}
        begin
          # Convert state attributes into Hash<Status, StateHashString>
          attributes_hash = JSON.parse(self.state_attributes) if self.state_attributes.present?
          # Convert StateHashString into Hash<attr, value>
          state_hash = JSON.parse(attributes_hash[order_status.to_s]) if attributes_hash[order_status.to_s].present?
          # Store time of the state transition
          state_hash['time'] = (time.to_i).to_s
          attributes_hash[order_status.to_s] = JSON.generate(state_hash)
          self.state_attributes = JSON.generate(attributes_hash)
          self.save!
        rescue => e
          ApplicationLogger.debug('Unable to store order state attributes for status: ' + order_status.to_s + ' , error: ' + e.message.to_s)
        end
      end

      #
      # Trigger ORDER state change
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(CONTENT_UTIL::EVENT_NOT_ALLOWED)
        end
      end

    end
  end
end
#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    # Model for carts table
    class Cart < ActiveRecord::Base

      CART_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::CartStates
      CONTENT = CommonModule::V1::Content
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      has_many :order_products, dependent: :destroy, class_name: 'MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProduct'

      belongs_to :order, class_name: 'MarketplaceOrdersModule::V1::Order'

      belongs_to :user, class_name: 'UsersModule::V1::User'

      validates :user, presence: true

      #
      # Workflow to define states of the Cart
      #
      # Initial State => Inactive
      #
      # State Diagram::
      #   EMPTY_CART --add_to_cart--> ACTIVE 
      #   ACTIVE --empty_out--> EMPTY_CART
      #   ACTIVE  --checkout--> CHECKED_OUT
      #
      # * fill_in, empty_out, checkout are the event which triggers the state transition
      #
      include Workflow
      workflow_column :user_cart_status
      workflow do
        state :empty_cart, CART_STATES::EMPTY_CART do
          event :add_to_cart, transitions_to: :active
          event :discard_cart, transitions_to: :discarded
        end
        state :active, CART_STATES::ACTIVE do
          event :checkout, transitions_to: :checked_out
          event :empty_out, transitions_to: :empty_cart
          event :discard_cart, transitions_to: :discarded
        end
        state :checked_out, CART_STATES::CHECKED_OUT do
          event :discard_cart, transitions_to: :discarded
        end
        state :discarded, CART_STATES::DISCARDED
      end

      #
      # Trigger CART state change
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end
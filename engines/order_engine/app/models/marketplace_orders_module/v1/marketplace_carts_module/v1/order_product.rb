#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    module MarketplaceCartsModule
      module V1
        # Model for order_products table
        class OrderProduct < ActiveRecord::Base

          ORDER_PRODUCT_STATES = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::ModelStates::V1::OrderProductStates
          CONTENT = CommonModule::V1::Content
          CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

          belongs_to :cart

          belongs_to :marketplace_selling_pack, class_name: 'MarketplaceProductModule::V1::MarketplaceSellingPack'

          # Composite unique key : <cart + marketplace_selling_pack>
          # A cart can have unique order-product specific to one mpsp
          validates :marketplace_selling_pack, presence: true,
                        uniqueness: { scope: :cart }
          
          # must belongs to a cart
          validates :cart, presence: true

          #
          # Workflow to define states of the Order Product
          #
          # Initial State => Inactive
          #
          # # State Diagram::
          #   Inactive --activate--> Active
          #   Active  --deactivate--> Inactive
          #
          # * activate, deactivate are the event which triggers the state transition
          #
          include Workflow
          workflow_column :buying_status
          workflow do
            state :inactive, ORDER_PRODUCT_STATES::INACTIVE do
              event :activate, transitions_to: :active
            end
            state :active, ORDER_PRODUCT_STATES::ACTIVE do
              event :deactivate, transitions_to: :inactive
            end
          end

          def trigger_event(event)
            event = 'self.' + event + '!'
            begin
              eval(event)
            rescue Workflow::NoTransitionAllowed => e
               raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT::EVENT_NOT_ALLOWED)
            end
          end
        end
      end
    end
  end
end
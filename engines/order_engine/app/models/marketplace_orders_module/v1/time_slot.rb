#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    # Model for time slots table
    class TimeSlot < ActiveRecord::Base

      TIME_SLOT_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::TimeSlotStates
      CONTENT = CommonModule::V1::Content
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      has_many :orders

      #
      # Workflow to define states of the Payment
      #
      # Initial State => PENDING
      #
      # State Diagram::
      #   PENDING --payment_fail--> FAILED 
      #   PENDING --payment_success--> SUCCESS
      #
      # * payment_fail, payment_success are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, TIME_SLOT_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, TIME_SLOT_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, TIME_SLOT_STATES::DELETED
      end

      #
      # Trigger time slot state change
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end

    end
  end
end

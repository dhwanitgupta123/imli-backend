#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for marketplace orders module
  #
  module V1
    # Model for orders table
    class OrderContext
      #Making it mongoid document
      include Mongoid::Document
      #Saving timeStamp with each log
      include Mongoid::Timestamps
      
      # Order id for which context is to be stored
      field :order_id, type: Integer

      # App from which order is placed
      # version of the app
      field :app_version, type: String

      # PlatformId (from PlatformType enum) of platform which is used while placing order
      # platform type. ex: Android, IOS, etc.
      field :platform_type, type: Integer

      #
      # [order_user_tags ] -> Retailer, Consumer, Office
      #
      field :order_user_tag, type: Integer

      #indexing order_id as we querying on this field only
      index "order_id" => 1
      index "order_user_tag" => 1

    end # End of class
  end
end

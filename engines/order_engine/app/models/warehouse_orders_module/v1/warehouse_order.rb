#
# Module to handle all the functionalities related to orders
#
module WarehouseOrdersModule
  #
  # Version1 for Warehouse orders module
  #
  module V1
    # Model for Warehouse orders table
    class WarehouseOrder < ActiveRecord::Base

      ORDER_MODEL_STATES = WarehouseOrdersModule::V1::ModelStates::V1::OrderStates

      belongs_to :order, class_name: '::MarketplaceOrdersModule::V1::Order'

      validates :order, presence: true

      #
      # Workflow to define states of the Order
      #
      # Initial State => Initiated
      #
      #
      # * initiate, place are the event which triggers the state transition
      #
      include Workflow
      # will add after discussion
      

    end
  end
end
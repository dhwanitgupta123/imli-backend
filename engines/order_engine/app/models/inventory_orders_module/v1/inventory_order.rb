#
# Module to handle all the functionalities related to inventory orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Model for inventory orders table
    #
    class InventoryOrder < ActiveRecord::Base

      ORDER_MODEL_STATES = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderStates
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      belongs_to :billing_address, class_name: 'SupplyChainModule::V1::WarehouseModule::V1::WarehouseAddress'

      belongs_to :shipping_address, class_name: 'SupplyChainModule::V1::WarehouseModule::V1::WarehouseAddress'

      # has_many :payments, dependent: :destroy

      belongs_to :user, class_name: 'UsersModule::V1::User'

      belongs_to :warehouse, class_name: 'SupplyChainModule::V1::Warehouse'

      belongs_to :inventory, class_name: 'SupplyChainModule::V1::Inventory'

      has_one :inventory_order_payment, dependent: :destroy

      has_many :inventory_order_products, dependent: :destroy

      validates :shipping_address, presence: true

      validates :warehouse, presence: true

      validates :user, presence: true

      validates :inventory, presence: true

      #
      # Workflow to define states of the Order
      #
      # Initial State => Initiated
      # initiated => placed, event: place_purchase_order
      # placed => dispatched, event: dispatch_purchase_order
      # dispatched => received, event: receive_purchase_order
      # received => verified, event: verify_purchase_order
      # verified => stocked, event: stock_purchase_order
      #
      include Workflow
      workflow_column :status
      workflow do
        state :initiated, ORDER_MODEL_STATES::INITIATED[:value] do
          event :place_purchase_order, transitions_to: :placed
          event :cancel_purchase_order, transitions_to: :cancelled
        end
        state :placed, ORDER_MODEL_STATES::PLACED[:value] do
          event :receive_purchase_order, transitions_to: :received
          event :renew_purchase_order, transitions_to: :initiated
          event :cancel_purchase_order, transitions_to: :cancelled
        end
        state :received, ORDER_MODEL_STATES::RECEIVED[:value] do
          event :verify_purchase_order, transitions_to: :verified
        end
        state :verified, ORDER_MODEL_STATES::VERIFIED[:value] do
          event :stock_purchase_order, transitions_to: :stocked
        end
        state :dispatched, ORDER_MODEL_STATES::DISPATCHED[:value]
        state :stocked, ORDER_MODEL_STATES::STOCKED[:value]
        state :cancelled, ORDER_MODEL_STATES::CANCELLED[:value]

        #
        # This function will be called whenever a transition complete (only onSuccess)
        # Log the transition of states and also store the timestamp into some persistent
        # storage.
        # Wiki WorkFlow transition: https://github.com/geekq/workflow#on_transition
        #
        after_transition do |from, to, triggering_event, *event_args|
          time = Time.zone.now
          user_id = USER_SESSION[:user_id] if USER_SESSION.present?
          ApplicationLogger.debug("Order with id #{self.id} changed state from #{from} -> #{to}, using event: #{triggering_event} by user id: #{user_id} at time: #{time.to_s}")
        end

      end

      #
      # Trigger ORDER state change
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(CONTENT_UTIL::EVENT_NOT_ALLOWED)
        end
      end

    end
  end
end
#
# Module to handle all the functionalities related to inventory orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Model for inventory orders payment table
    #
    class InventoryOrderPayment < ActiveRecord::Base

      PAYMENT_STATES = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderPaymentStates
      CONTENT = CommonModule::V1::Content
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      belongs_to :inventory_order

      validates :inventory_order, presence: true

      #
      # Workflow to define states of the Payment
      #
      # Initial State => PENDING
      #
      # State Diagram::
      #   PENDING --partial_payment--> PARTIAL 
      #   PARTIAL --complete_payment--> COMPLETED
      #
      # * partial_payment, complete_payment are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :pending, PAYMENT_STATES::PENDING do
          event :partial_payment, transitions_to: :partial
        end
        state :partial, PAYMENT_STATES::PARTIAL do
          event :complete_payment, transitions_to: :completed
        end
        state :completed, PAYMENT_STATES::COMPLETED
      end

      #
      # Trigger Payment state change
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end

    end
  end
end

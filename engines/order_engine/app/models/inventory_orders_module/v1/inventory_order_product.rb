#
# Module to handle all the functionalities related to inventory orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Model for inventory order products table
    #
    class InventoryOrderProduct < ActiveRecord::Base

      ORDER_PRODUCT_STATES = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderProductStates
      CONTENT = CommonModule::V1::Content
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      belongs_to :inventory_order

      belongs_to :inventory_brand_pack, class_name: 'InventoryProductModule::V1::InventoryBrandPack'

      validates :inventory_brand_pack, presence: true,
                    uniqueness: { scope: :inventory_order }
      
      # must belongs to a inventory_order
      validates :inventory_order, presence: true

      #
      # Workflow to define states of the Order Product
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :buying_status
      workflow do
        state :inactive, ORDER_PRODUCT_STATES::INACTIVE do
          event :activate, transitions_to: :active
        end
        state :active, ORDER_PRODUCT_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
        end
      end

      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end

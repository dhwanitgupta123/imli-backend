#
# Module to handle all the functionalities related to invnentory orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    module ModelStates
      module V1
        #
        # Class for inventory Order events
        #
        class InventoryOrderEvents

          PLACE_PURCHASE_ORDER    = 1
          DISPATCH_PURCHASE_ORDER = 2
          RECEIVE_PURCHASE_ORDER  = 3
          VERIFY_PURCHASE_ORDER   = 4
          STOCK_PURCHASE_ORDER    = 5
          RENEW_PURCHASE_ORDER    = 6
          CANCEL_PURCHASE_ORDER   = 7

          #
          # Get all allowed events for a particular Inventory Order
          #
          # @param inventory_order [Object] [inventory order ]
          # 
          # @return [type] [description]
          #
          def self.get_allowed_events(inventory_order)
            return [] if inventory_order.blank?
            allowed_actual_events = inventory_order.current_state.events.keys
            return convert_allowed_actual_event_to_event_ids(allowed_actual_events)
          end

          private

          #
          # Fetch map of WorkflowEvents to its respective Hash (id and label)
          #
          def self.event_to_id_map
            event_map = {
              :place_purchase_order => { value: PLACE_PURCHASE_ORDER, label: 'PLACE ORDER' },
              :dispatch_purchase_order => { value:  DISPATCH_PURCHASE_ORDER, label: 'DISPATCH PURCHASE ORDER' },
              :receive_purchase_order => { value:  RECEIVE_PURCHASE_ORDER, label: 'RECEIVE PURCHASE ORDER' },
              :verify_purchase_order => { value:  VERIFY_PURCHASE_ORDER, label: 'VERIFY PURCHASE ORDER' },
              :stock_purchase_order => { value:  STOCK_PURCHASE_ORDER, label: 'STOCK PURCHASE ORDER' },
              :renew_purchase_order => { value:  RENEW_PURCHASE_ORDER, label: 'RENEW PURCHASE ORDER' },
              :cancel_purchase_order => { value:  CANCEL_PURCHASE_ORDER, label: 'CANCEL PURCHASE ORDER' }
            }
            return event_map
          end

          #
          # Convert actual events to their respective value label hash
          # Ex: actual_events_map will be like [:placed, :refunded]
          # Final array: [{value: 2, lablel: 'PLACED'}, {value: '23', label: 'REFUNDED'}]
          #
          # @param actual_events_map [Array] [Array of Symbols]
          # 
          # @return [Array] [Array of Hash]
          #
          def self.convert_allowed_actual_event_to_event_ids(actual_events_map)
            return [] if actual_events_map.blank?
            inventory_order_events_array = []
            event_map = event_to_id_map
            actual_events_map.each do |event|
              # Event is already a symbol. ex: ':placed'
              map = event_map[event]
              if map.present?
                inventory_order_events_array.push(event_map[event])
              end
            end
            return inventory_order_events_array
          end
        end
      end
    end
  end
end

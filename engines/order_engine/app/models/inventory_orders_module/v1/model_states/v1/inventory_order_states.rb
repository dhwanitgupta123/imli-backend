#
# Module to handle all the functionalities related to inventory orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    module ModelStates
      module V1
        #
        # Class for inventory Order states
        #
        class InventoryOrderStates

          INITIATED   = { value: 1, label: 'INITIATED'}
          PLACED      = { value: 2, label: 'PLACED' }
          RECEIVED    = { value: 3, label: 'RECEIVED' }
          VERIFIED    = { value: 4, label: 'VERIFIED' }
          STOCKED     = { value: 5, label: 'STOCKED' }
          DISPATCHED  = { value: 6, label: 'DISPATCHED' }
          CANCELLED   = { value: 7, label: 'CANCELLED' }

          CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
          CONTENT_UTIL = CommonModule::V1::Content
          CACHE_UTIL = CommonModule::V1::Cache

          #Making it mongoid document
          include Mongoid::Document
          #Saving timeStamp with each log
          include Mongoid::Timestamps

          field :order_id, type: Integer
          field :order_status, type: String

          #
          # Function to store new email id for invite
          #
          def save_inventory_order_state_attributes(order_state_attributes)
            order_state_attributes = InventoryOrderStates.new(order_state_attributes)
            begin
              order_state_attributes.save!
              rescue Mongoid::Errors::Validations, Mongoid::Errors::InvalidValue => e
                raise CUSTOM_ERRORS_UTIL::InvalidDataError.new('Not able to save inventory order state attrbutes')
            end
          end

          #
          # Function to get state attributes of a partiular order
          #
          def self.get_inventory_order_state_attributes(inventory_order_id)
            order_state_attributes = InventoryOrderStates.where(order_id: inventory_order_id.to_i)
            return order_state_attributes
          end

          #
          # Get order state hash corresponding to passed value
          #
          # @param order_state_value [Integer] [Value of the state]
          #
          # @return [JSON] [Hash of order state]
          #
          def self.get_order_state_by_value(order_state_value)
            state = {}
            array_index = 0
            self.constants.each_with_index do |var, index|
              array_index = index
              state = self.const_get(var)
              # Skip if an Hash is not defined as value
              # Used for Minified_States, Final_States
              next if !state.kind_of?(Hash)
              # Break the loop as we found the matched state
              break if state[:value].to_i == order_state_value.to_i
            end
            # Return empty hash if end of the MAP encountered
            return {} if self.constants.length == array_index + 1
            return state
          end
        end
      end
    end
  end
end

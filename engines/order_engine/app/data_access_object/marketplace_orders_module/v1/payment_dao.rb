module MarketplaceOrdersModule

  module V1

  	class PaymentDao < BaseOrdersModule::V1::BaseDao

      PAYMENT_MODEL = MarketplaceOrdersModule::V1::Payment
      PAYMENT_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::PaymentStates
      PAYMENT_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::PaymentEvents
      PAYMENT_MODES = MarketplaceOrdersModule::V1::PaymentModes
      PAYMENT_TYPE = MarketplaceOrdersModule::V1::PaymentType
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      #
      # Initialize PAYMENT DAO with passed params
      #
      def initialize(params = {})
        @params = params
        @default_payment_type = PAYMENT_TYPE.get_id_by_key('PAY')
        super
      end

      #
      # Create new payment object using passed params
      #
      # @param args [JSON] [Hash containing payment attributes, 
      # Refer payment model for attributes]
      # 
      # @return [Object] [created Payment object]
      #
      # @raise [RunTimeError] [if Payment object creation failed]
      #
      def create_new_payment(args)
        args = args.merge({payment_type: @default_payment_type}) if args[:payment_type].blank?
        begin
          payment = PAYMENT_MODEL.new(args)
          payment.save!
          return payment
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::RunTimeError.new(CONTENT::CREATION_FAILED % {model: 'Payment'})
        end
      end

      #
      # Update payment object using passed params
      #
      # @param args [JSON] [Hash containing payment attributes, 
      # Refer payment model for attributes]
      #
      # @return [Object] [updated Payment object]
      #
      # @raise [RunTimeError] [if Payment object creation failed]
      #
      def update_payment(args, payment)
        begin
          payment.update_attributes!(args)
          return payment
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::RunTimeError.new(CONTENT::UPDATION_FAILED % {model: 'Payment'})
        end
      end

      #
      # Get payment corresponding to passed id
      #
      # @param payment_id [Integer] [id for which payment need to be fetched]
      # 
      # @return [type] [description]
      def get_payment_from_id(payment_id)
        begin
          PAYMENT_MODEL.find(payment_id.to_i)
        rescue => e
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT::RESOURCE_NOT_FOUND % {resource: 'Payment'})
        end
      end

      #
      # Get only latest pending payment of Order
      #
      def get_pending_payment_for_order(order)
        active_payments = order.payments.
            where( status: PAYMENT_STATES::PENDING ).
            order('updated_at DESC').first
      end

      #
      # Get only latest successfull payment of Order
      #
      def get_successfull_payment_for_order(order)
        active_payments = order.payments.
            where( status: PAYMENT_STATES::SUCCESS ).
            order('updated_at DESC').first
      end

      #
      # Get payment of Order which is either successfull or pending (only in case
      # of offline payment modes)
      #
      # @param order [Object] [Order for which payment is needed]
      #
      # @return [Object] [Payment object if found as per criteria else nil]
      #
      def get_orders_payment_as_per_its_mode(order)
        active_payments = order.payments.
            where( status: [ PAYMENT_STATES::SUCCESS, PAYMENT_STATES::PENDING ] ).
            order('created_at DESC')
        return active_payments.first

        # successful_payment = active_payments.where(status: PAYMENT_STATES::SUCCESS).first
        # return successful_payment if successful_payment.present?
        # # Else, if payment is with Offline mode (ex: COD or Card-On-Delivery), then only return its 
        # # pending payment Object
        # pending_payment = active_payments.where(status: PAYMENT_STATES::PENDING).first
        # offline_mode_of_payments = PAYMENT_MODES.get_offline_payment_modes
        # if pending_payment.present? && offline_mode_of_payments.include?(pending_payment.mode.to_i)
        #   return pending_payment
        # end
        # # Else, return nil payment
        # return nil
      end


      def get_active_payments_for_order(order)
        active_payments = order.payments.
            where( status: [ PAYMENT_STATES::SUCCESS, PAYMENT_STATES::PENDING ] ).
            order('created_at DESC')
        return active_payments
      end

      def get_active_payments_for_order_in_ascending_order(order)
        active_payments = order.payments.
            where( status: [ PAYMENT_STATES::SUCCESS, PAYMENT_STATES::PENDING ] ).
            order('created_at ASC')
        return active_payments
      end

      def get_successful_payments(order)
        return order.payments.
                where( status: PAYMENT_STATES::SUCCESS )
      end

      def get_pending_payments(order)
        return order.payments.
                where( status: PAYMENT_STATES::PENDING )
      end

      #
      # Get latest active payment
      #
      def get_latest_payment(order)
        active_payments = get_active_payments_for_order(order)
        return nil if active_payments.blank?
        latest_active_payment = active_payments.order('created_at DESC').first
        return latest_active_payment
      end

      #
      # Get last successful payment for that order
      # Fetch SUCCESS
      #
      def get_last_successful_payment(order)
        success_payments = get_successful_payments(order)
        # Fetch only payments which has type 'PAY'
        #payment_type = PAYMENT_TYPE.get_id_by_key('PAY')
        last_success = success_payments.order('created_at DESC').first
        return last_success
      end

      #
      # Fetch PENDING COD payment for the order
      # Fetch PENDING + PAY combination
      #
      def get_pending_cod_payment(order)
        pending_payments = get_pending_payments(order)
        # Fetch only payments which has type 'PAY'
        payment_type = PAYMENT_TYPE.get_id_by_key('PAY')
        last_pending = pending_payments.where(payment_type: payment_type).order('created_at DESC').first
        return last_pending
      end

      #
      # Fetch PENDING REFUND payment for the order
      # Fetch PENDING + REFUND combination
      #
      def get_pending_refund_payment(order)
        refund_payment_type = PAYMENT_TYPE.get_id_by_key('REFUND')
        pending_refund_payment = order.payments.
            where( status: PAYMENT_STATES::PENDING, payment_type: refund_payment_type ).
            order('created_at DESC').first

        return pending_refund_payment
      end

      #
      # Discard old payment object (status: DISCARDED), and then add new payment
      # with new passed mode
      # This will be used when post order payment mode is different than pre-order selected mode
      #
      # @param old_payment [Object] [Old payment for the order]
      # @param args [JSON] [Hash containing new mode of payment]
      # 
      # @return [Object] [New created payment object]
      #
      def discard_old_payment_and_create_new(old_payment, args)
        # Create new payment with new mode
        new_payment = old_payment.dup
        new_payment.mode = args[:mode]
        new_payment.save!

        # Discard old payment object
        change_state(old_payment, PAYMENT_EVENTS::DISCARD_PAYMENT)
        return new_payment
      end

      def discard_payment(payment)
        change_state(payment, PAYMENT_EVENTS::DISCARD_PAYMENT)
      end

      #
      # Trigger PAYMENT change_state as per the event passed
      #
      def change_state(payment, event)
        case event.to_i
        when PAYMENT_EVENTS::INITIATE
          payment.trigger_event('pending')
        when PAYMENT_EVENTS::PAYMENT_FAIL
          payment.trigger_event('payment_fail')
        when PAYMENT_EVENTS::PAYMENT_SUCCESS
          payment.trigger_event('payment_success')
        when PAYMENT_EVENTS::DISCARD_PAYMENT
          payment.trigger_event('discard_payment')
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
        end
        return payment
      end

  	end
  end
end

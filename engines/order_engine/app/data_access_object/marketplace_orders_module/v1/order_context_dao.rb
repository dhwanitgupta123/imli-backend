module MarketplaceOrdersModule

  module V1

    class OrderContextDao < BaseOrdersModule::V1::BaseDao
      ORDER_CONTEXT = MarketplaceOrdersModule::V1::OrderContext
      ORDER_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
      ORDER_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::OrderEvents

      ORDER_PRODUCT_STATES = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::ModelStates::V1::OrderProductStates
      ORDER_MODEL = MarketplaceOrdersModule::V1::Order
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      #
      # Initialize the object with deciding params
      #
      def initialize(params = '')
        super
        @params = params
      end

      # 
      # Create order Context as per passed attributes
      #
      # @param args [JSON] [Hash containing order_id, app version, platform type]
      #
      # @return [Object] [Mongo object of order context]
      #
      def create_order_context(args)
        order_context_params = model_params(args, ORDER_CONTEXT)
        begin
          ORDER_CONTEXT.create!(order_context_params)
          rescue Mongoid::Errors::Validations, Mongoid::Errors::InvalidValue => e
            raise CUSTOM_ERRORS_UTIL::InvalidDataError.new('Not able to create order context')
        end
      end

      # 
      # Update order Context as per passed attributes
      #
      # @param args [JSON] [Hash containing order_id, app version, platform type]
      #
      # @return [Object] [Mongo object of order context]
      #
      def update_order_context(args, order_context)
        order_context_params = model_params(args, ORDER_CONTEXT)
        begin
          order_context.update_attributes!(order_context_params)
          return order_context
        rescue Mongoid::Errors::Validations => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update order context ' + e.message)
        end
      end

      # 
      # GET order Context according to passed order_id
      #
      # @param args [Integer] [id of the order for which context is to be fetched]
      #
      # @return [Object] [Mongo object of order context]
      #
      def get_context_by_order_id(order_id)
        begin
          ORDER_CONTEXT.find_by(order_id: order_id.to_i)
        rescue Mongoid::Errors::DocumentNotFound => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Invalid order id')
        end
      end

    end # End of class
  end
end

#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    module MarketplaceCartsModule
      module V1
        # Model for order_products table
        class OrderProductsDao < BaseOrdersModule::V1::BaseDao
        	ORDER_PRODUCTS_MODEL = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProduct
          ORDER_PRODUCT_EVENTS = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::ModelStates::V1::OrderProductEvents
          ORDER_PRODUCT_STATES = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::ModelStates::V1::OrderProductStates
          CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
          CONTENT = CommonModule::V1::Content

          #
          # Initialize Order product DAO with passed params
          #
          def initialize(params='')
            @params = params
          end
          
          #
          # Create a new Order Product
          #
          # @param params [JSON] [Hash containing quantity, cart and MPSP]
          #
          # @raise [RunTimeError] [if creation failed]
          def create_order_products(params)
            order_product = ORDER_PRODUCTS_MODEL.new(params)
            order_product[:buying_status] = ORDER_PRODUCT_STATES::ACTIVE
            begin
              order_product.save!
              # After creation, recompute order product state
              update_order_product_state(order_product)
              return order_product
            rescue ActiveRecord::RecordInvalid => e
              raise CUSTOM_ERROR_UTIL::RunTimeError.new(e.message)
            end
          end

          #
          # Change state of Order product as per the event sent
          #
          def change_state(order_product, event)
            case event.to_i
              when ORDER_PRODUCT_EVENTS::ACTIVATE
                order_product.trigger_event('activate')
              when ORDER_PRODUCT_EVENTS::DEACTIVATE
                order_product.trigger_event('deactivate')
              else
                raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
            end
            return order_product
          end

          # 
          # Update a order product as per the passed attributes
          #
          # @param args [JSON] [Hash containing quantity, cart or MPSP]
          # @param order_product [Object] [Order product object]
          # 
          # @return [Object] [Order product]
          def update(args, order_product)
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('order_product cannot be null') if order_product.nil? || args.nil?
            # Increment the quantity
            # args[:quantity] = args[:quantity].to_i + order_product.quantity.to_i

            begin
              order_product.update_attributes!(args)
              update_order_product_state(order_product)

              return order_product
            rescue ActiveRecord::RecordInvalid => e
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update order_product')
            end
          end

          def get_active_order_products_in_carts(carts)
            order_products = ORDER_PRODUCTS_MODEL
              .where({buying_status: ORDER_PRODUCT_STATES::ACTIVE,cart_id: carts}).where.not({ quantity: 0 })
            
            return order_products
          end

          # 
          # This function join all tables required to return all information
          # corresponding to order_products, mpsp and brand_packs
          #
          # @param brand_packs [ActiveRecordRelationArray] marketplace_brand_packs
          # 
          # @return [ActiveRecordRelationArray] Joined object containing 
          #
          def self.get_aggregated_order_products_and_mpsps_details(order_products)

            return [] if order_products.blank?

            order_products.eager_load(marketplace_selling_pack: [:marketplace_selling_pack_marketplace_brand_packs, 
                                                       marketplace_brand_packs: [:marketplace_brand_pack_pricing,
                                                       brand_pack: [ product: [sub_brand: [brand: :company ] ] ] ], 
                                                       mpsp_sub_category: [mpsp_parent_category: :mpsp_department ] ])
          end

          private

          #
          # Update Order product state
          # Make a order product INACTIVE if its quantity is 0
          # Make it ACTIVE if it has a positive quantity and was previously inactive
          #
          # @param order_product [Object] [order product whose state is to be updated]
          #
          def update_order_product_state(order_product)
            # If Order Product is INACTIVE and a new quantity is added, then ACTIVATE it again
            if order_product.inactive? && order_product.quantity.to_i > 0
              event = ORDER_PRODUCT_EVENTS::ACTIVATE
              change_state(order_product, event)
            end
            # If Order Product's quantity is 0, then DEACTIVATE it
            if order_product.active? && order_product.quantity.to_i <= 0
              event = ORDER_PRODUCT_EVENTS::DEACTIVATE
              change_state(order_product, event)
            end
          end

          def self.load_order_products_till_master_products(order_products)
            return [] if order_products.blank?

            order_products.eager_load(marketplace_selling_pack: [:marketplace_selling_pack_marketplace_brand_packs, 
                                                       marketplace_brand_packs: [:marketplace_brand_pack_pricing, :brand_pack]])
  
          end

       	end
      end
    end
  end
end
module MarketplaceOrdersModule

  module V1

  	class CartDao < BaseOrdersModule::V1::BaseDao

      CART_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::CartStates
      CART_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::CartEvents
      ORDER_PRODUCT_STATES = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::ModelStates::V1::OrderProductStates
      CARTS_MODEL = MarketplaceOrdersModule::V1::Cart
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      MARKETPLACE_SELLING_PACK_DAO = MarketplaceProductModule::V1::MarketplaceSellingPackDao
      INVOICE_HELPER = MarketplaceOrdersModule::V1::InvoiceHelper

      #
      # Initialize the object with deciding params
      #
      def initialize(params = '')
        @params = params
      end

      #
      # Find ACTIVE or EMPTY cart of the user
      # It returns recent cart
      #
      # @param user [Object] [User object]
      # 
      # @return [Object] [active or empty cart of the user]
      def find_existing_cart(user)
        # Allowing both Active and Empty cart
      	active_cart = user.carts.
            where({ user_cart_status: [CART_STATES::ACTIVE, CART_STATES::EMPTY_CART] }).
            order('updated_at DESC').first
      end

      def fetch_active_cart(carts)
        # Allowing both Active and Empty cart
        active_cart = carts.
            where({ user_cart_status: [CART_STATES::ACTIVE, CART_STATES::EMPTY_CART] }).
            order('updated_at DESC').first
        return active_cart
      end

      def get_final_cart(carts)
        active_carts = carts.where.not(user_cart_status: CART_STATES::DISCARDED)
        # Return CheckedOut cart which is created at the latest
        checkout_carts = active_carts.where(user_cart_status: CART_STATES::CHECKED_OUT).
                            order('created_at DESC')
        return checkout_carts.first if checkout_carts.present?
        return fetch_active_cart(active_carts)
      end

      #
      # Fetch cart from id
      #
      def get_by_id(id)
        get_model_by_id(CARTS_MODEL, id.to_i)
      end

      #
      # Create new EMPTY cart of the user
      #
      # @param user [Object] [User object]
      # 
      # @return [Object] [empty cart of the user]
      # 
      # @raise [RunTimeError] [if cart creation failed due to params passed]
      def create_new_cart(user)
      	cart = CARTS_MODEL.new(user: user)
      	cart[:user_cart_status] = CART_STATES::EMPTY_CART
      	begin
          cart.save!
          return cart
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::RunTimeError.new(CONTENT::CART_CREATION_FAILED)
        end
      end

      #
      # Update existing cart of the user
      #
      # @param [args] [Hash of all the updated attributes]
      # @param cart [Object] [Cart which is to be updated]
      # 
      # @return [Object] [updated cart of the user]
      # 
      # @raise [RunTimeError] [if cart updation failed due to params passed]
      def update_cart(args, cart)
        begin
          cart.update_attributes!(args)
          return cart
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::RunTimeError.new(CONTENT::UPDATION_FAILED % {model: 'Cart'})
        end
      end

      #
      # Get all active products in CART
      # To-Do: Call a function of OrderProducts DAO and pass 'cart.order_products' to it
      #
      def active_products_in_cart(cart)
        active_products = cart.order_products.
            where({ buying_status: ORDER_PRODUCT_STATES::ACTIVE }).where.not({ quantity: 0 })
      end

      #
      # Get COUNT of all active products in CART
      # To-Do: Call a function of OrderProducts DAO and pass 'cart.order_products' to it
      #
      def get_active_order_products_count_in_cart(cart)
        active_products = cart.order_products.
            where({ buying_status: ORDER_PRODUCT_STATES::ACTIVE }).count
      end

      #
      # Check whether MPSP exists in cart
      # It fetches all order products in CART which has passed MPSP id
      #
      # @return [Object] [Order product object which has requested MPSP]
      #
      def mpsp_exists_in_cart?(params)
        cart = params[:cart]
        mpsp = params[:marketplace_selling_pack]
        order_product = cart.order_products.
            where({ marketplace_selling_pack_id: mpsp.id }).first
      end

      #
      # Update cart savings and total as per the new quantity of requested MPSP
      #
      # @param args [JSON] [Hash containing cart, MPSP, previous quantity and current_quantity]
      #
      # @raise [RunTimeError] [if cart updation fails]
      #
      def update_cart_savings_and_total(args)
        cart = args[:cart]

        # For Ladder Pricing
        previous_op_pricing_details = get_savings_and_total_details_for_mpsp(args[:previous_order_product])
        current_op_pricing_details = get_savings_and_total_details_for_mpsp(args[:updated_order_product])
        
        # Update CART stats atomicaly
        cart = atomic_update_cart_stats(cart, previous_op_pricing_details, current_op_pricing_details)
        return cart
      end

      #
      # Update cart statistics in a single atomic operation (Using LOCKs)
      # Reason to do so: While executing similar requests in parallel threads, cart computations
      # get affected because of multiple non-atomic read and writes
      #
      # @param cart [Object] [Cart model object whose stats are to be changed]
      # @param previous_op_pricing_details [JSON] [Hash of old pricing stats]
      # @param current_op_pricing_details [JSON] [Hash of new pricing stats]
      #
      # @return [Object] [updated CART object]
      #
      def atomic_update_cart_stats(cart, previous_op_pricing_details, current_op_pricing_details)
        # Do not perform anything if params are blank
        return cart if cart.blank? || previous_op_pricing_details.blank? || current_op_pricing_details.blank?

        begin
          # Perform read write operations over 'cart' object (single row)
          # after acquiring lock over it. [i.e. In a single atomic operation]
          cart.with_lock do
            # New_cart_value = cart_total_value + current_value - previous_value
            new_cart_savings = cart.cart_savings + current_op_pricing_details[:total_savings] - previous_op_pricing_details[:total_savings]
            #new_cart_total = cart.cart_total + current_op_pricing_details[:total_selling_price] - previous_op_pricing_details[:total_selling_price]
            new_final_cart_price = cart.cart_total + current_op_pricing_details[:final_price] - previous_op_pricing_details[:final_price]
            new_total_mrp = cart.total_mrp + current_op_pricing_details[:total_mrp] - previous_op_pricing_details[:total_mrp]

            # Update cart
            cart.cart_savings = new_cart_savings
            cart.cart_total = new_final_cart_price
            cart.total_mrp = new_total_mrp
            cart.save!
          end
        rescue ActiveRecord::RecordInvalid => e
          ApplicationLogger.error('Unable to update cart stats values atomicaly, cart_id: ' + cart.id.to_s)
          raise CUSTOM_ERROR_UTIL::RunTimeError.new(e.message)
        end
        return cart
      end

      #
      # Get savings and total for product
      #
      # @param quantity [Object] [Order product]
      #
      # @return [JSON] [hash containing savings and selling_price]
      #
      def get_savings_and_total_details_for_mpsp(order_product)
        if order_product.blank?
          return { total_selling_price: 0, total_savings: 0, total_mrp: 0, additional_discount: 0, final_price: 0 }
        end
        total_selling_price = order_product.selling_price * order_product.quantity
        total_savings = order_product.savings * order_product.quantity
        total_mrp = order_product.mrp * order_product.quantity
        additional_discount = order_product.additional_discount
        total_final_price = order_product.final_price
        return {
          total_selling_price: total_selling_price,
          total_savings: total_savings,
          total_mrp: total_mrp,
          additional_discount: additional_discount,
          final_price: total_final_price
        }
      end

      #
      # Modify cart stats to default ZERO
      #
      # @param cart [Object] [Cart model object whose stats are to be nullified]
      # 
      # @return [Object] [Updated cart]
      #
      def nullify_cart_stats(cart)
        if cart.present?
          cart.total_mrp = BigDecimal.new('0.0')
          cart.cart_total = BigDecimal.new('0.0')
          cart.cart_savings = BigDecimal.new('0.0')
          begin
            cart.save!
          rescue => e
            # Unable to nullify cart
            ApplicationLogger.debug('Unable to nullify cart, id: ' + cart.id.to_s)
          end
          return cart
        end
      end

      #
      # RE-COMPUTE complete cart stats.
      # Traverse over all the active products of cart and re-compute
      # its total_mrp, cart_toal and cart_savings. Override the existing values
      # with the re-computed one
      #
      # @param cart [Object] [Cart whose stats are to be computed]
      #
      # @return [Object] [updated cart]
      #
      def re_compute_cart_stats(cart)
        return cart if cart.blank?
        order_products = active_products_in_cart(cart)
        # Iterate over Order products and fetch all displayable entities
        computed_total_mrp = BigDecimal.new('0.0')
        computed_total_savings = BigDecimal.new('0.0')
        computed_total_selling_price = BigDecimal.new('0.0')
        computed_additional_discount = BigDecimal.new('0.0')
        computed_final_price = BigDecimal.new('0.0')
        order_products.each do |order_product|
          computed_total_mrp += order_product.mrp * order_product.quantity
          computed_total_savings += order_product.savings * order_product.quantity
          computed_total_selling_price += order_product.selling_price * order_product.quantity
          computed_additional_discount += order_product.additional_discount
          computed_final_price += order_product.final_price
        end

        # Perform read write operations over 'cart' object (single row)
        # after acquiring lock over it. [i.e. In a single atomic operation]
        cart.with_lock do
          check_for_inconsistency(cart, {total_mrp: computed_total_mrp, cart_savings: computed_total_savings, cart_total: computed_final_price})

          # This is just normal database updation (itself atomic operation)
          cart.total_mrp = computed_total_mrp > 0 ? computed_total_mrp : BigDecimal.new('0.0')
          # cart.cart_total = computed_total_selling_price > 0 ? computed_total_selling_price : BigDecimal.new('0.0')
          cart.cart_total = computed_final_price > 0 ? computed_final_price : BigDecimal.new('0.0')
          cart.cart_savings = computed_total_savings > 0 ? computed_total_savings : BigDecimal.new('0.0')
          begin
            cart.save!
          rescue => e
            # Unable to nullify cart
            ApplicationLogger.debug('Unable to update cart re-computed values, cart_id: ' + cart.id.to_s)
          end
        end
        return cart
      end

      #
      # Check for CART stats inconsistency
      # and LOG it appropriately
      #
      # @param cart [Object] [Cart model object which is to be matched]
      # @param computed_cart_stats = {} [JSON] [Hash of computed attributes]
      #
      def check_for_inconsistency(cart, computed_cart_stats = {})
        return nil if cart.blank?

        error_array = []
        error_array.push('Total MRP mismatch') if computed_cart_stats[:total_mrp].present? && computed_cart_stats[:total_mrp].to_d != cart.total_mrp.to_d
        error_array.push('Total Savings mismatch') if computed_cart_stats[:cart_savings].present? && computed_cart_stats[:cart_savings].to_d != cart.cart_savings.to_d
        error_array.push('Cart Total mismatch') if computed_cart_stats[:cart_total].present? && computed_cart_stats[:cart_total].to_d != cart.cart_total.to_d

        if error_array.present?
          error_string = error_array.join(', ')
          # Need to LOG this inconsistency
          ApplicationLogger.warn('CART: ' + cart.id.to_s + ' stats different from re-computed stats, error: ' + error_string.to_s )
        end
      end

      #
      # Trigger CART change_state to empty
      #
      def change_cart_status_to_empty(cart)
        event = CART_EVENTS::EMPTY_OUT
        cart = change_state(cart, event)
      end

      #
      # Trigger CART change_state to ACTIVE
      #
      def change_cart_status_to_active(cart)
        event = CART_EVENTS::ADD_TO_CART
        cart = change_state(cart, event)
      end

      #
      # Trigger CART change_state to CHECKOUT
      #
      def checkout_cart(cart)
        event = CART_EVENTS::CHECKOUT
        cart = change_state(cart, event)
      end

      #
      # Trigger CART change_state to DISCARD_CART
      #
      def discard_cart(cart)
        event = CART_EVENTS::DISCARD_CART
        cart = change_state(cart, event)
      end

      #
      # Trigger CART change_state as per the event passed
      #
      def change_state(cart, event)
        case event.to_i
        when CART_EVENTS::ADD_TO_CART
          cart.trigger_event('add_to_cart')
        when CART_EVENTS::EMPTY_OUT
          cart.trigger_event('empty_out')
        when CART_EVENTS::CHECKOUT
          cart.trigger_event('checkout')
        when CART_EVENTS::DISCARD_CART
          cart.trigger_event('discard_cart')
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
        end
        return cart
      end

      def self.load_cart_details(cart)
        return nil if cart.blank?

        CARTS_MODEL.eager_load(order_products: [marketplace_selling_pack: [marketplace_selling_pack_pricing: :marketplace_selling_pack_ladder_pricings]]).
          where(id: cart.id).first

      end

  	end
  end
end
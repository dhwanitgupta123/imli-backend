#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Class to implement functionalities related to Data object of Iventory order Payment
    #
    class InventoryOrderPaymentDao < BaseOrdersModule::V1::BaseDao
      INVENTORY_ORDER_PAYMENT_STATES = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderPaymentStates
      INVENTORY_ORDER_PAYMENT_EVENTS = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderPaymentEvents
      INVENTORY_ORDER_PAYMENT_MODEL = InventoryOrdersModule::V1::InventoryOrderPayment
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      #
      # Initialize the object with deciding params
      #
      def initialize(params = '')
        @params = params
      end

      #
      # Create new Purchase ORDER payment
      #
      def new_inventory_order_payment
        inventory_payment_order = INVENTORY_ORDER_PAYMENT_MODEL.new
      end

      #
      # Update the inventory order payment
      #
      def update_inventory_order_payment(inventory_order_payment, args)
        begin
          inventory_order_payment.update_attributes!(args)
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update Inventory Order Payment, '+ e.message)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Class to implement functionalities related to Data object of Iventory order
    #
    class InventoryOrderDao < BaseOrdersModule::V1::BaseDao
      INVENTORY_ORDER_STATES = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderStates
      INVENTORY_ORDER_EVENTS = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderEvents
      INVENTORY_ORDER_MODEL = InventoryOrdersModule::V1::InventoryOrder
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      #
      # Initialize the object with deciding params
      #
      def initialize(params = '')
        super
        @params = params
      end

      #
      # Create new Purchase ORDER of the warehouse
      #
      # @param args [JSON] [Hash containing user, w/h inventory and address object]
      # 
      # @return [Object] [created invenotry order of the warehouse]
      # 
      # @raise [InvalidArgumentsError] [if order creation failed due to params passed]
      def create_new_inventory_order(args)
        inventory_order = INVENTORY_ORDER_MODEL.new(args)
        begin
          inventory_order.save!
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::CREATION_FAILED % {model: 'Inventory order'})
        end
        order_state_attributes = {
          order_id: inventory_order.id,
          order_status: INVENTORY_ORDER_STATES::INITIATED[:label]
        }
        inventory_order_states = INVENTORY_ORDER_STATES.new
        inventory_order_states.save_inventory_order_state_attributes(order_state_attributes)
        return inventory_order
      end

      #
      # Update the purchase order of the warehouse
      # 
      # @param inventory_order [object] Object of inventory order to be updated
      # @param args [json] [hash of params to be updated in the object]
      # 
      # @return [object] [inventory object]
      # 
      # @raise [InvalidArgumentsError] [If update query fails]
      def update_inventory_order(inventory_order, args)
        inventory_order.warehouse_id = args[:warehouse][:id] if args[:warehouse].present? && args[:warehouse][:id].present?
        inventory_order.inventory_id = args[:inventory][:id] if args[:inventory].present? && args[:inventory][:id].present?
        inventory_order.shipping_address_id = args[:address][:shipping_address][:id] if args[:address].present? && args[:address][:shipping_address][:id].present?
        inventory_order.billing_address_id = args[:address][:billing_address][:id] if args[:address].present? && args[:address][:billing_address][:id].present?
        inventory_order.pdf_link = args[:pdf_link] if args[:pdf_link].present?
        inventory_order.is_inter_state = args[:is_inter_state] if args[:is_inter_state].present? || args[:is_inter_state] == false
        begin
          inventory_order.save!
          return inventory_order
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(e.message)
        end
      end

      #
      # Update order_id in the ORDER of the warehouse
      #
      # @param order [Object] [order object of the warehouse]
      # @param order_id [String] [generated unique order id]
      # 
      # @return [Object] [updated order of the warehouse]
      # 
      # @raise [InvalidArgumentsError] [if order updation failed due to params passed]
      def update_order_id(order_id, inventory_order)
        begin
          inventory_order.order_id = order_id
          inventory_order.save!
          return inventory_order
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::CREATION_FAILED % {model: 'Inventory Order ID'})
        end
      end

      #
      # Function to return the Inventory order of state INITITATED for a particular
      # combination of warehouse & inventory.
      # 
      # @param [hash] [contains the info of warheouse & inventory ID]
      # 
      # @return [Active record object] [Object which is in initiated state]
      def initiated_inventory_warehouse_order(params)
        warheouse_id = params[:warehouse][:id]
        inventory_id = params[:inventory][:id]
        return INVENTORY_ORDER_MODEL.where(warehouse_id: warheouse_id, inventory_id: inventory_id, status: INVENTORY_ORDER_STATES::INITIATED[:value]).first
      end

      #
      # Function to get a particualr inventory order by its ID
      # 
      # @param [integer] [ID of the inventory order to be fetched]
      # 
      # @return [object] [Inventory order object for that ID]
      # 
      # @raise [InvalidArgumentsError] [If no order exists with that ID]
      def get_inventory_order_by_id(id)
        begin
          inventory_order = INVENTORY_ORDER_MODEL.find(id)
        rescue ActiveRecord::RecordNotFound => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::RESOURCE_NOT_FOUND % {resource: 'Inventory Order'})
        end
      end

      #
      # Function to check if in the Inventory order there exists a Order product which belongs to the given IBP
      # 
      # @param args [Hash] [Object of inventoy order & inventory brand pack]
      # 
      # @return [object] [Inventory order product if it exists]
      def ibp_exists_in_purchase_order?(args)
        inventory_order = args[:inventory_order]
        inventory_brand_pack = args[:inventory_brand_pack]
        inventory_order_product = inventory_order.inventory_order_products.where(inventory_brand_pack_id: inventory_brand_pack.id).first
        return inventory_order_product
      end

      #
      # Function to check if the order is in initiated state or not
      # 
      # @param [object] [inventory order which is to be checked]
      # 
      # @return [object] [inventory order if it is in initiated state]
      # 
      # @raise [InvalidArgumentsError] [If inventory order is not in initiated state]
      def inventory_order_initiated?(inventory_order)
        if inventory_order.initiated?
          return inventory_order
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::NO_INITIATED_PO)
        end
      end

      #
      # Function to check if the order is in received state or not
      # 
      # @param [object] [inventory order which is to be checked]
      # 
      # @return [object] [inventory order if it is in received state]
      # 
      # @raise [InvalidArgumentsError] [If inventory order is not in received state]
      def inventory_order_received?(inventory_order)
        if inventory_order.received?
          return inventory_order
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PO_NOT_RECEIVED)
        end
      end

      #
      # Function to check if the order is in verified state or not
      # 
      # @param [object] [inventory order which is to be checked]
      # 
      # @return [object] [inventory order if it is in verified state]
      # 
      # @raise [InvalidArgumentsError] [If inventory order is not in verified state]
      def inventory_order_verified?(inventory_order)
        if inventory_order.verified?
          return inventory_order
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PO_NOT_VERIFIED)
        end
      end

      #
      # Trigger ORDER change_state as per the event passed
      #
      def change_state(inventory_order, event)
        case event.to_i
        when INVENTORY_ORDER_EVENTS::PLACE_PURCHASE_ORDER
          inventory_order.trigger_event('place_purchase_order')
          order_status = INVENTORY_ORDER_STATES::PLACED[:label]
        when INVENTORY_ORDER_EVENTS::RECEIVE_PURCHASE_ORDER
          inventory_order.trigger_event('receive_purchase_order')
          order_status = INVENTORY_ORDER_STATES::RECEIVED[:label]
        when INVENTORY_ORDER_EVENTS::VERIFY_PURCHASE_ORDER
          inventory_order.trigger_event('verify_purchase_order')
          order_status = INVENTORY_ORDER_STATES::VERIFIED[:label]
        when INVENTORY_ORDER_EVENTS::STOCK_PURCHASE_ORDER
          inventory_order.trigger_event('stock_purchase_order')
          order_status = INVENTORY_ORDER_STATES::STOCKED[:label]
        when INVENTORY_ORDER_EVENTS::RENEW_PURCHASE_ORDER
          inventory_order.trigger_event('renew_purchase_order')
          order_status = INVENTORY_ORDER_STATES::INITIATED[:label]
        when INVENTORY_ORDER_EVENTS::CANCEL_PURCHASE_ORDER
          inventory_order.trigger_event('cancel_purchase_order')
          order_status = INVENTORY_ORDER_STATES::CANCELLED[:label]
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
        end
        order_state_attributes = {
          order_id: inventory_order.id,
          order_status: order_status
        }
        inventory_order_states = INVENTORY_ORDER_STATES.new
        inventory_order_states.save_inventory_order_state_attributes(order_state_attributes)
        return inventory_order
      end

      #
      # Get all ORDERS between a specified interval with pagination
      #
      # @param pagination_params = {} [JSON] [Hash of paginations parameters]
      # @param interval_params = {} [JSON] [Hash of time interval parameters, from_time, to_time]
      #
      # @return [Array] [Array of filtered Order model objects]
      #
      def get_all_orders_between_interval(pagination_params = {}, interval_params = {})
        set_pagination_properties(pagination_params, INVENTORY_ORDER_MODEL)
        set_interval_properties(interval_params)
        warehouse_id = pagination_params[:warehouse_id]
        if @state.blank?
          elements = INVENTORY_ORDER_MODEL.where(warehouse_id: warehouse_id, updated_at: @from_time..@till_time)
        else
          elements = INVENTORY_ORDER_MODEL.where(status: @state, warehouse_id: warehouse_id, updated_at: @from_time..@till_time)
        end
        page_count = (elements.count / @per_page).ceil
        inventory_orders = elements.order(@sort_order).
                                      limit(@per_page).offset((@page_no - 1) * @per_page)
        return {inventory_orders: inventory_orders, page_count: page_count}
      end
    end
  end
end

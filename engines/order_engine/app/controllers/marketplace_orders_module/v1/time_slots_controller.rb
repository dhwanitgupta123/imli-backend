#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # Defining Time Slot Controller
    # 
    class TimeSlotsController < BaseModule::V1::ApplicationController

      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      TIME_SLOT_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::TimeSlotEvents
      # APIs for time slot
      ADD_TIME_SLOT_API = MarketplaceOrdersModule::V1::AddTimeSlotApi
      UPDATE_TIME_SLOT_API = MarketplaceOrdersModule::V1::UpdateTimeSlotApi
      GET_TIME_SLOTS_API = MarketplaceOrdersModule::V1::GetAllTimeSlotsApi
      GET_TIME_SLOT_API = MarketplaceOrdersModule::V1::GetTimeSlotApi
      CHANGE_TIME_SLOT_STATE_API = MarketplaceOrdersModule::V1::ChangeTimeSlotStateApi

      #
      # before actions to be executed
      #
      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end
      #
      # initialize TimeSlotController Class
      #
      def initialize
        
      end

      #
      # function to add time_slot
      #
      # post /add_time_slot
      #
      # Request:: request object contain
      #   * time_slot_params : from_time, to_time, label
      #
      # Response::
      #   * If action succeed then returns Success Status code with create time_slot
      #    else returns BadRequest response
      #
      def add_time_slot
        add_time_slot_api = ADD_TIME_SLOT_API.new(@deciding_params)
        response = add_time_slot_api.enact(add_time_slot_params)
        send_response(response)
      end

      swagger_controller :time_slot, 'MarketplaceOrdersModule APIs'
      swagger_api :add_time_slot do
        summary 'It creates the time_slot with given input'
        notes 'create time_slot'
        param :body, :time_slot_request, :time_slot_request, :required, 'create_time_slot request'
        response :bad_request, 'if request params are incorrect or api fails to create time_slot'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :time_slot_request do
        description 'request schema for /add_time_slot API'
        property :time_slot, :time_slot_hash, :required, 'time_slot model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :time_slot_hash do
        description 'details of time_slot'
        property :from_time, :string, :required, 'from_time of time_slot'
        property :to_time, :string, :optional, 'to_time for time_slot'
        property :label, :string, :optional, 'label for time_slot'
      end

      #
      # function to update time slot
      #
      # post /update_time_slot
      #
      # Request:: request object contain
      #   * time_slot_params : from_time, to_time, label
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated brand
      #    else returns BadRequest response
      #
      def update_time_slot
        update_time_slot_api = UPDATE_TIME_SLOT_API.new(@deciding_params)
        update_time_slot_request = update_time_slot_params.merge({id: get_id_from_params})
        response = update_time_slot_api.enact(update_time_slot_request)
        send_response(response)
      end

      swagger_controller :time_slot, 'MarketplaceOrdersModule APIs'
      swagger_api :update_time_slot do
        summary 'It updates the time_slot with given input'
        notes 'update time_slot'
        param :path, :id, :integer, :required, 'time_slot to update'
        param :body, :update_time_slot_request, :update_time_slot_request, :required, 'create_time_slot request'
        response :bad_request, 'if request params are incorrect or api fails to create brand'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :update_time_slot_request do
        description 'request schema for /update_time_slot API'
        property :time_slot, :update_time_slot_hash, :required, 'time_slot model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :update_time_slot_hash do
        description 'details of time_slot'
        property :from_time, :string, :required, 'from_time of time_slot'
        property :to_time, :string, :optional, 'to_time for time_slot'
        property :label, :string, :optional, 'label for time_slot'
      end

      #
      # function to get all time slots as per pagination and filteration
      #
      # GET /get_all_time_slots
      #
      # Response::
      #   * list of filtered time slots
      #
      def get_all_time_slots
        get_time_slots_api = GET_TIME_SLOTS_API.new(@deciding_params)
        response = get_time_slots_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :time_slot, 'MarketplaceOrdersModule APIs'
      swagger_api :get_all_time_slots do
        summary 'It returns details of all the time slots'
        notes 'get_all_time_slots API returns the details of all the time slots'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all brand'
        param :query, :per_page, :integer, :optional, 'how many elements to display in a page'
        param :query, :state, :integer, :optional, 'to fetch elements based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to change the status of brand
      #
      # PUT /time_slot/state/:id
      #
      # Request::
      #   * time_slot_id [integer] id of time slot of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        change_time_slot_state_api = CHANGE_TIME_SLOT_STATE_API.new(@deciding_params)
        response = change_time_slot_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :time_slot, 'MarketplaceOrdersModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of time slot'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the brand & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'time_slot_id to be deactivated/activated'
        param :body, :change_time_slot_state_request, :change_time_slot_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or brand not found or it fails to change state'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_time_slot_state_request do
        description 'request schema for /change_state API'
        property :time_slot, :change_time_slot_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_time_slot_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      #
      # function to delete brand
      #
      # DELETE /time_slot/:id
      #
      # Request::
      #   * time_slot_id [integer] id of timeslot of which delete is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def delete_time_slot
        change_time_slot_state_api = CHANGE_TIME_SLOT_STATE_API.new(@deciding_params)
        response = change_time_slot_state_api.enact({ 
                      id: get_id_from_params, 
                      event: TIME_SLOT_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :time_slot, 'MarketplaceOrdersModule APIs'
      swagger_api :delete_time_slot do
        summary 'It delete the time slot'
        notes 'delete_time_slot API change the status of time_slot to soft delete'
        param :path, :id, :integer, :required, 'time_slot_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or brand not found or it fails to delete'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a time_slot
      #
      # GET /time_slots/:id
      #
      # Request::
      #   * time_slot_id [integer] id of timeslot of which delete is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_time_slot
        get_time_slot_api = GET_TIME_SLOT_API.new(@deciding_params)
        response = get_time_slot_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :time_slot, 'MarketplaceOrdersModule APIs'
      swagger_api :get_time_slot do
        summary 'It return a time_slot'
        notes 'get_time_slot API return a single time_slot corresponding to the id'
        param :path, :id, :integer, :required, 'time_slot_id to be fetched'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or brand not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end


      ###############################
      #       Private Functions     #
      ###############################

      private

      def add_time_slot_params
        params.require(:time_slot).permit(:from_time, :to_time, :label)
      end

      def update_time_slot_params
        params.require(:time_slot).permit(:from_time, :to_time, :label)
      end

      def change_state_params
        params.require(:time_slot).permit(:event).merge({ id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end

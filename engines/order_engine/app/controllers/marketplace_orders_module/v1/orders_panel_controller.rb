#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # Defining Orders Panel Controller
    #
    class OrdersPanelController < BaseModule::V1::ApplicationController

      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper

      #
      # before actions to be executed
      #
      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end
      #
      # initialize OrdersController Class
      #
      def initialize
        @get_all_orders = MarketplaceOrdersModule::V1::GetAllOrdersApi
        @get_order_by_order_id = MarketplaceOrdersModule::V1::GetOrderByOrderIdApi
        @get_packing_details_by_order_id = MarketplaceOrdersModule::V1::GetPackingDetailsByOrderIdApi
        @get_invoice_by_order_id = MarketplaceOrdersModule::V1::GetInvoiceByOrderIdApi
        @get_aggregated_products = MarketplaceOrdersModule::V1::GetAggregatedProductsApi
        @change_state_api = MarketplaceOrdersModule::V1::ChangeStateApi
        # Order Edit APIs
        @edit_order_address_api = MarketplaceOrdersModule::V1::EditOrderAddressApi
        @edit_order_time_slot_api = MarketplaceOrdersModule::V1::EditOrderTimeSlotApi
        @edit_order_cart_api = MarketplaceOrdersModule::V1::EditOrderCartApi
        @update_order_context_api = MarketplaceOrdersModule::V1::UpdateOrderContextApi
      end

      ##################################################################################
      #                             get_all_orders API                                 #
      ##################################################################################

      #
      # function to get all active order details with minimal details
      #
      # GET marketplace/orders
      #
      # Response::
      #   * send details of all orders
      #
      def get_all_orders
        get_all_orders_api = @get_all_orders.new(@deciding_params)
        response = get_all_orders_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :get_all_orders do
        summary 'It returns minimal details of all orders'
        notes 'get_all_orders API returns the minimal details of all orders'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :from_time, :integer, :optional, 'Epoch timestamp of from date-time'
        param :query, :to_time, :integer, :optional, 'Epoch timestamp of TO date-time'
        param :query, :page_no, :integer, :optional, 'page no from which to get all orders'
        param :query, :per_page, :integer, :optional, 'how many orders to display in a page'
        param :query, :state, :integer, :optional, 'to fetch orders based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                           get_order_by_order_id API                            #
      ##################################################################################

      #
      # function to get a marketplace order
      #
      # GET marketplace/orders/:order_id
      #
      # Request::
      #   * order_id [string] order_id of order which needs to be retrieved
      #
      # Response::
      #   * sends ok response to the panel with order details
      #
      def get_order_by_order_id
        get_order_by_order_id_api = @get_order_by_order_id.new(@deciding_params)
        response = get_order_by_order_id_api.enact({ order_id: get_order_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :get_order_by_order_id do
        summary 'It return a order with details'
        notes 'get_order_by_order_id API returns a single order details corresponding to the order_id'
        param :path, :order_id, :string, :required, 'order_id to be fetched'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or order not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                         get_aggregated_products API                            #
      ##################################################################################

      #
      # function to get aggregated products for pickers to pick
      #
      # GET marketplace/orders/aggregated
      #
      # Response::
      #   * sends ok response to the list of aggregated product and mails it
      #
      def get_aggregated_products
        get_aggregated_products = @get_aggregated_products.new(@deciding_params)
        response = get_aggregated_products.enact(get_aggregated_products_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :get_aggregated_products do
        summary 'It returns aggregated products with quantity details to be used by picker'
        notes 'get_aggregated_products API returns aggregated products with quantity details to be used by picker'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :from_time    , :integer, :optional, 'Epoch Unix Time Stamp'
        param :query, :to_time      , :integer, :optional, 'Epoch Unix Time Stamp'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                             change_state API                                   #
      ##################################################################################

      #
      # function to change state of Orders
      #
      # put /orders/state/:id
      #
      # Request:: request object contain
      #   * id [integer] id of Order whose state is to be changed
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated Order
      #    else returns BadRequest response
      #
      _LogActivity_
      def change_state
        change_state_api = @change_state_api.new(@deciding_params)
        response = change_state_api.enact(change_state_params)
        send_response(response)
      end

      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :change_state do
        summary 'It changes the state of Orders'
        notes 'change_state API takes event & id as input and triggers the event to change the state of the Order'
        param :path, :id, :integer, :required, 'Order to update'
        param :body, :order_change_state, :order_change_state, :required, 'change_state request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of order'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :order_change_state do
        description 'request schema for /change_state API'
        property :order, :order_change_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :order_change_state_hash do
        description 'event to be triggered'
        property :event, :integer, :required, 'event to trigger status change'
      end


      ##################################################################################
      #                         edit_order_time_slot API                               #
      ##################################################################################

      #
      # function to change state of Orders
      #
      # PUT /orders/:id/time_slot
      #
      # Request:: request object contain
      #   * id [integer] id of Order which is to be updated
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated Order
      #    else returns BadRequest response
      #
      _LogActivity_
      def edit_order_time_slot
        edit_order_time_slot_api = @edit_order_time_slot_api.new(@deciding_params)
        edit_order_time_slot_response = edit_order_time_slot_api.enact(edit_order_time_slot_params)
        send_response(edit_order_time_slot_response)
      end

      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :edit_order_time_slot do
        summary 'It changes the already placed Order'
        notes 'It takes various updated hashed and then update current order accordingly'
        param :path, :id, :integer, :required, 'Order to update'
        param :body, :order_time_slot_edit_hash, :order_time_slot_edit_hash, :required, 'order_edit request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of order'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :order_time_slot_edit_hash do
        description 'request schema for /order_edit API'
        property :order, :order_hash_for_order_time_slot_edit, :required, 'hash of order containing various attributes hashes'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :order_hash_for_order_time_slot_edit do
        description 'details of order_params_for_place_order'
        property :preferred_slot, :preferred_slot_hash_for_order_time_slot_edit, :required, 'preferred slot for the order'
      end
      swagger_model :preferred_slot_hash_for_order_time_slot_edit do
        description 'details of preferred_slot_hash_for_order_time_slot_edit'
        property :slot_date, :string, :required, 'slot date'
        property :time_slot, :time_slot_hash_for_order_time_slot_edit, 'time slot hash'
      end
      swagger_model :time_slot_hash_for_order_time_slot_edit do
        description 'details of time slot hash'
        property :id, :integer, :required, 'id of the chosen slot for the order'
      end

      ##################################################################################
      #                        get_packing_details_by_order_id API                     #
      ##################################################################################

      #
      # function to get packing details of marketplace order
      #
      # GET marketplace/orders/packing_details/:order_id
      #
      # Request::
      #   * order_id [string] order_id of order which needs to be retrieved
      #
      # Response::
      #   * sends ok response to the panel with packing details
      #
      def get_packing_details_by_order_id
        get_packing_details_by_order_id_api = @get_packing_details_by_order_id.new(@deciding_params)
        response = get_packing_details_by_order_id_api.enact({ order_id: get_order_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :get_packing_details_by_order_id do
        summary 'It returns packing details of an order'
        notes 'get_packing_details_by_order_id API returns a single order packing details corresponding to the order_id'
        param :path, :order_id, :string, :required, 'order_id to be fetched'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or order not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                           get_invoice_by_order_id API                          #
      ##################################################################################

      #
      # function to get invoice of marketplace order
      #
      # GET marketplace/orders/invoice/:order_id
      #
      # Request::
      #   * order_id [string] order_id of order which needs to be retrieved
      #
      # Response::
      #   * sends ok response to the panel with invoice
      #
      def get_invoice_by_order_id
        get_invoice_by_order_id_api = @get_invoice_by_order_id.new(@deciding_params)
        response = get_invoice_by_order_id_api.enact({ order_id: get_order_id_from_params })
        send_response(response)
      end

      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :get_invoice_by_order_id do
        summary 'It returns invoice of an order'
        notes 'get_invoice_by_order_id API returns invoice corresponding to the order_id'
        param :path, :order_id, :string, :required, 'order_id to be fetched'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or order not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                         edit_order_address API                                 #
      ##################################################################################

      #
      # function to change address of Orders
      #
      # PUT /orders/:id/address
      #
      # Request:: request object contain
      #   * id [integer] id of Order which is to be updated
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated Order
      #    else returns BadRequest response
      #
      _LogActivity_
      def edit_order_address
        edit_order_address_api = @edit_order_address_api.new(@deciding_params)
        edit_order_address_response = edit_order_address_api.enact(edit_order_address_params)
        send_response(edit_order_address_response)
      end

      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :edit_order_address do
        summary 'It changes the already placed Order'
        notes 'It takes various updated hashed and then update current order accordingly'
        param :path, :id, :integer, :required, 'Order to update'
        param :body, :order_address_edit_hash, :order_address_edit_hash, :required, 'order_address_edit request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of order'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :order_address_edit_hash do
        description 'request schema for /order_edit API'
        property :order, :order_hash_for_order_address_edit, :required, 'hash of order containing various attributes hashes'
        property :session_token, :string, :required, 'session token to authenticate user'
      end
      swagger_model :order_hash_for_order_address_edit do
        description "params for updating address of the order"
        property :address, :address_hash_for_order_address_edit, :required, "address hash with address details"
      end
      swagger_model :address_hash_for_order_address_edit do
        description "Address hash with address related details of user"
        property :id, :integer, :required, 'Address id'
        property :recipient_name, :string, :required, 'Name of the order recipient'
        property :nickname, :string, :optional, "Address nickname"
        property :address_line1, :string, :optional, "User Flat & Building name"
        property :address_line2, :string, :optional, "User Locality Name"
        property :landmark, :string, :optional, "Address landmark"
        property :pincode, :string, :optional, "Address pincode"
      end


      ##################################################################################
      #                         edit_order_cart API                                    #
      ##################################################################################

      #
      # function to change state of Orders
      #
      # PUT /orders/:id/cart
      #
      # Request:: request object contain
      #   * id [integer] id of Order which is to be updated
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated Order
      #    else returns BadRequest response
      #
      _LogActivity_
      def edit_order_cart
        edit_order_cart_api = @edit_order_cart_api.new(@deciding_params)
        edit_order_cart_response = edit_order_cart_api.enact(edit_order_cart_params)
        send_response(edit_order_cart_response)
      end

      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :edit_order_cart do
        summary 'It changes the already placed Order'
        notes 'It takes various updated hashed and then update current order accordingly'
        param :path, :id, :integer, :required, 'Order to update'
        param :body, :order_cart_edit_hash, :order_cart_edit_hash, :required, 'order_edit request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of order'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :order_cart_edit_hash do
        description 'request schema for /order_edit API'
        property :order, :order_hash_for_order_cart_edit, :required, 'hash of order containing various attributes hashes'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :order_hash_for_order_cart_edit do
        description 'details of order_params_for_place_order'
        property :cart, :cart_hash_for_order_cart_edit, :required, 'preferred slot for the order'
      end
      swagger_model :cart_hash_for_order_cart_edit do
        description 'details of cart_hash_for_order_cart_edit'
        property :id, :integer, :required, 'Id of the cart'
        property :order_products, :array, :required, 'MRP of MPSP',
                { 'items': { 'type': :order_products_array_for_order_cart_edit } }
      end
      swagger_model :order_products_array_for_order_cart_edit do
        description 'details of order product with quantity and MPSP to make OrderProduct'
        property :quantity, :integer, :required, 'quantity of order product'
        property :additional_discount, :required, 'additional discount to be applied on product'
        property :product, :product_hash_for_order_cart_edit, :required, 'product hash of MPSP'
      end
      swagger_model :product_hash_for_order_cart_edit do
        description 'details of MPSP with its id'
        property :id, :integer, :required, 'id reference to MPSP'
      end

      def update_order_context
        update_order_context_api = @update_order_context_api.new(@deciding_params)
        update_order_context_response = update_order_context_api.enact(update_order_context_params)
        send_response(update_order_context_response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :update_order_context do
        summary 'It update order context'
        notes 'update_order_context API update order context'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :id, :string, :required, 'order id'
        param :query, :order_user_tag, :integer, :optional, 'Tag for order usage'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      def pagination_params
        params.permit(:from_time, :to_time, :page_no, :per_page, :state, :sort_by, :order)
      end

      def get_order_id_from_params
        return params.require(:order_id)
      end

      def get_aggregated_products_params
        return params.permit(:from_time,:to_time)
      end

      #
      # White list params for change state API
      #
      def change_state_params
        params.require(:order).permit(:event).merge({ id: params[:id] })
      end

      #
      # white list parameters for edit order address request.
      #
      def edit_order_address_params
        params.require(:order).permit(:address => [:id, :nickname, :address_line1,
          :address_line2, :landmark, :recipient_name, :pincode]).merge({ id: params[:id] })
      end

      #
      # white list parameters for edit order time slot request.
      #
      def edit_order_time_slot_params
        params.require(:order).permit(preferred_slot: [:slot_date, time_slot: [:id]]).merge({ id: params[:id] })
      end

      #
      # white list parameters for edit order time slot request.
      #
      def edit_order_cart_params
        if params.present? && params[:order].present? &&
          params[:order][:cart].present? && params[:order][:cart][:order_products].present? &&
          params[:order][:cart][:order_products].is_a?(String)
          # Then, parse the string to JSON
          params[:order][:cart][:order_products] = JSON.parse(params[:order][:cart][:order_products])
        end
        params.require(:order).permit(cart: [:id, order_products: [:quantity, :additional_discount, product: [:id]]]).merge({ id: params[:id] })
      end

      def update_order_context_params
        # here order_id is integeral id of order
        params.require(:order_context).permit(:order_user_tag).merge({order_id: params[:id]})
      end
    end
  end
end

module MarketplaceOrdersModule

  module V1

    class PaymentsController < BaseModule::V1::ApplicationController
      #
      # initialize CartsController Class
      #
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      INITIATE_PAYMENT_API = MarketplaceOrdersModule::V1::InitiatePaymentApi
      PAYMENT_SUCCESSFUL_API = MarketplaceOrdersModule::V1::PaymentSuccessfulApi

      # This block will be executed before any API call / action call
      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end

      def initialize
        # Can initialize common instance variable, if needed
      end

      ##################################################################################
      #                             initiate_payment API                               #
      ##################################################################################

      #
      # Function to initiate payment
      # Create a payment and link it with User's order
      #
      # POST /payment/initiate
      #
      # Request::
      #   * initiate_payment_params [hash] it contains payment and its attributes
      # check private function initiate_payment_params for more details
      #
      # Response::
      #   * sends ok response to the panel
      #
      _LogActivity_
      def initiate_payment
        initiate_payment_api = INITIATE_PAYMENT_API.new(@deciding_params)
        response = initiate_payment_api.enact(initiate_payment_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :payments_controller, 'MarketplaceOrdersModule APIs'
      swagger_api :initiate_payment do
        summary 'It initiates payment for the user'
        notes 'initiates payment and update its attributes into payment object along with cart'
        param :body, :initiate_payment_request, :initiate_payment_request, :required,
              'initiate_payment_request request'
        response :bad_request, 'if request params are incorrect or api fails to place order'
        response :forbidden, 'User does not have permissions to access this page'
        response :not_found, 'Cart not found or User not found'
        response :run_time_error, 'if api fails to place order or update cart'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :initiate_payment_request do
        description 'request schema for /initiate_payment API'
        property :order, :order_params_for_initiate_payment, :required,
                 'order_params_for_initiate_payment model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :order_params_for_initiate_payment do
        description 'details of order_params_for_initiate_payment'
        property :id, :integer, :required, 'order id for which payment is to be made'
        property :payment, :payment_params_for_initiate_payment, :optional,
                'payment_params_for_initiate_payment model hash'
      end

      swagger_model :payment_params_for_initiate_payment do
        description 'details of payment_params_for_initiate_payment'
        property :mode, :integer, :required, 'mode of payment of the user'
        property :delivery_charges, :double, :required, 'delivery charge for the order'
        property :grand_total, :double, :required, 'grand total amount to be payed by the user'
        property :discount, :double, :required, 'extra discount over payment'
        property :billing_amount, :double, :required, 'billing amount of payment'
        property :payment_mode_savings, :double, :required, 'extra payment mode savings'
        property :net_total, :double, :required, 'total payable amount by the user'
        property :total_savings, :double, :required, 'total savings of the user'
      end


      ##################################################################################
      #                        payment_successful API                                #
      ##################################################################################

      #
      # Function to initiate payment
      # Create a payment and link it with User's order
      #
      # POST /payment/successfull/:id
      #
      # Request::
      #   * initiate_payment_params [hash] it contains payment and its attributes
      # check private function initiate_payment_params for more details
      #
      # Response::
      #   * sends ok response to the panel
      #
      _LogActivity_
      def payment_successful
        payment_successful_api = PAYMENT_SUCCESSFUL_API.new(@deciding_params)
        response = payment_successful_api.enact({id: params[:id]})
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :payments_controller, 'MarketplaceOrdersModule APIs'
      swagger_api :payment_successful do
        summary 'It initiates payment for the user'
        notes 'initiates payment and update its attributes into payment object along with cart'
        param :path, :id, :integer, :required, 'id of a successfull payment'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'if request params are incorrect or api fails to place order'
        response :forbidden, 'User does not have permissions to access this page'
        response :not_found, 'Cart not found or User not found'
        response :run_time_error, 'if api fails to place order or update cart'
      end

      private

      def initiate_payment_params
        params.require(:order).permit(:id, payment: [:mode, :delivery_charges, :grand_total, :discount, :billing_amount, :payment_mode_savings, :net_total, :total_savings])
      end

      def payment_successfull_params
        params.require(:payment).permit(:id)
      end

    end
  end
end
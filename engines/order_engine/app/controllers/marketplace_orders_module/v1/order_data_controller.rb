#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
  	# Orders Data controller class responsible for all data related operations
  	# on Marketplace Orders
  	#
    class OrderDataController < BaseModule::V1::ApplicationController
      #
      # initialize OrderDataController Class
      #
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      ORDERS_STATS_API = MarketplaceOrdersModule::V1::OrdersStatsApi

      # This block will be executed before any API call / action call
      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end

      def initialize
        # Can initialize common instance variable, if needed
      end

      ##################################################################################
      #                               order_stats API                                #
      ##################################################################################

      #
      # Function to get stats of orders
      #
      # GET marketplace/orders/stats
      #
      # Response::
      #   * sends ok response to the user
      #
      def get_orders_stats
        orders_stats_api = ORDERS_STATS_API.new(@deciding_params)
        response = orders_stats_api.enact
        send_response(response)
      end

      swagger_controller :order_data, 'MarketplaceOrdersModule APIs'
      swagger_api :get_orders_stats do
        summary 'It returns stats of orders'
        notes 'get_orders_stats API returns stats of orders'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end
    end
  end
end

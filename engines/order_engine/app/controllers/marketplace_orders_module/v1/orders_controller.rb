module MarketplaceOrdersModule

  module V1

    class OrdersController < BaseModule::V1::ApplicationController
      #
      # initialize CartsController Class
      #
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      PLACE_ORDER_API = MarketplaceOrdersModule::V1::PlaceOrderApi
      GET_ACTIVE_ORDERS_API = MarketplaceOrdersModule::V1::GetActiveOrdersApi
      GET_PREVIOUS_ORDERS_API = MarketplaceOrdersModule::V1::GetPreviousOrdersApi
      GET_ORDER_DETAILS_API = MarketplaceOrdersModule::V1::GetOrderDetailsApi
      SCHEDULE_ORDER_API = MarketplaceOrdersModule::V1::ScheduleOrderApi

      # This block will be executed before any API call / action call
      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end

      def initialize
        # Can initialize common instance variable, if needed
      end

      ##################################################################################
      #                             place_order API                                    #
      ##################################################################################

      #
      # Function to update cart
      # Create a cart if its not present already
      #
      # POST /cart/update
      #
      # Request::
      #   * update_cart_params [hash] it contains cart and its order products
      # check private function update_cart_params for more details
      #
      # Response::
      #   * sends ok response to the panel
      #
      _LogActivity_
      def place_order
        place_order_api = PLACE_ORDER_API.new(@deciding_params)
        response = place_order_api.enact(place_order_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_controller, 'MarketplaceOrdersModule APIs'
      swagger_api :place_order do
        summary 'It places order for the user'
        notes 'place order moves CART to checked out state and generates a order ID and link it with
        user, cart and address'
        param :body, :place_order_request, :place_order_request, :required,
              'place_order_request request'
        response :bad_request, 'if request params are incorrect or api fails to place order'
        response :forbidden, 'User does not have permissions to access this page'
        response :not_found, 'Cart not found or User not found'
        response :run_time_error, 'if api fails to place order or update cart'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :place_order_request do
        description 'request schema for /place_order API'
        property :order, :order_params_for_place_order, :required,
                 'order_params_for_place_order model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :order_params_for_place_order do
        description 'details of order_params_for_place_order'
        property :cart, :cart_hash_for_place_order, :required, 'cart of the user'
        property :address, :address_hash_for_place_order, :required, 'Adress of the user'
      end
      swagger_model :cart_hash_for_place_order do
        description 'details of cart_hash_for_place_order'
        property :id, :integer, :required, 'cart of the user'
      end
      swagger_model :address_hash_for_place_order do
        description 'details of address_hash_for_place_order'
        property :id, :integer, :required, 'address of the user'
      end


      ##################################################################################
      #                             get_previous_orders API                            #
      ##################################################################################

      #
      # Function to get all previous/completed orders of the user
      #
      # POST /orders/previous
      #
      # Response::
      #   * sends ok response to the user
      #
      _LogActivity_
      def get_previous_orders
        get_previous_orders_api = GET_PREVIOUS_ORDERS_API.new(@deciding_params)
        response = get_previous_orders_api.enact
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_controller, 'MarketplaceOrdersModule APIs'
      swagger_api :get_previous_orders do
        summary 'It fetches all completed orders'
        notes 'It fetches all the orders which are in final state like fulfilled, failed,
              refunded, canceled, etc.'
        param :query, :session_token, :integer, :required, 'session token of the user request'
        response :bad_request, 'if request params are incorrect or api fails to create/update cart'
        response :forbidden, 'User does not have permissions to access this page'
        response :not_found, 'User not found or no Active CART found'
      end

      ##################################################################################
      #                             get_active_orders API                                #
      ##################################################################################

      #
      # Function to get all active orders of the user
      # It will also fetch all OFFLINE Payment ORDERS too.
      #
      # POST /orders/active
      #
      # Response::
      #   * sends ok response to the user
      #
      _LogActivity_
      def get_active_orders
        get_active_orders_api = GET_ACTIVE_ORDERS_API.new(@deciding_params)
        response = get_active_orders_api.enact
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_controller, 'MarketplaceOrdersModule APIs'
      swagger_api :get_active_orders do
        summary 'It fetches all active orders'
        notes 'It fetches all the orders which are in intermediate state like initiated, placed,
              at_warehouse, dispatched, etc. It will also fetch all OFFLINE Payment ORDERS too.'
        param :query, :session_token, :integer, :required, 'session token of the user request'
        response :bad_request, 'if request params are incorrect or api fails to create/update cart'
        response :forbidden, 'User does not have permissions to access this page'
        response :not_found, 'User not found or no Active CART found'
      end

      ##################################################################################
      #                             get_order_details API                              #
      ##################################################################################

      #
      # Function to get all active orders of the user
      #
      # POST /orders/:id
      #
      # Response::
      #   * sends ok response to the user
      #
      _LogActivity_
      def get_order_details
        get_order_details_api = GET_ORDER_DETAILS_API.new(@deciding_params)
        response = get_order_details_api.enact({id: params[:id]})
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_controller, 'MarketplaceOrdersModule APIs'
      swagger_api :get_order_details do
        summary 'It fetches specific order details'
        notes 'It fetches complete details of the requested order, its id, status, cart, 
              cart product details, its payment, etc. It fetches successful payment object or
              its pending payment object if selected mode was OFFLINE Payment mode.'
        param :path, :id, :integer, :required, 'order id for which order details to be fetched'
        param :query, :session_token, :integer, :required, 'session token of the user request'
        response :bad_request, 'if request params are incorrect or api fails to create/update cart'
        response :forbidden, 'User does not have permissions to access this page'
        response :not_found, 'User not found or no Active CART found'
      end

      ##################################################################################
      #                          schedule_order API                                    #
      ##################################################################################

      #
      # Function to schedule order
      # Create a cart if its not present already
      #
      # POST /order/:id/schedule
      #
      # Request::
      #   * order [hash] it contains preferred slot hash
      # check private function schedule_order_params for more details
      #
      # Response::
      #   * sends ok response to the user
      #
      _LogActivity_
      def schedule_order
        schedule_order_api = SCHEDULE_ORDER_API.new(@deciding_params)
        schedule_order_request = schedule_order_params.merge({
          id: params[:id]
          })
        response = schedule_order_api.enact(schedule_order_request)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_controller, 'MarketplaceOrdersModule APIs'
      swagger_api :schedule_order do
        summary 'It schedules order for the user'
        notes 'schedules order as per the passed preferred date and time slot'
        param :path, :id, :integer, :required, 'id of order which is to be scheduled'
        param :body, :schedule_order_request, :schedule_order_request, :required,
              'place_order_request request'
        response :bad_request, 'if request params are incorrect or api fails to place order'
        response :forbidden, 'User does not have permissions to access this page'
        response :not_found, 'Cart not found or User not found'
        response :run_time_error, 'if api fails to place order or update cart'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :schedule_order_request do
        description 'request schema for /place_order API'
        property :order, :order_params_for_schedule_order, :required,
                 'order_params_for_place_order model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :order_params_for_schedule_order do
        description 'details of order_params_for_place_order'
        property :preferred_slot, :preferred_slot_hash_for_schedule_order, :required, 'preferred slot for the order'
      end
      swagger_model :preferred_slot_hash_for_schedule_order do
        description 'details of preferred_slot_hash_for_schedule_order'
        property :slot_date, :string, :required, 'slot date'
        property :time_slot, :time_slot_hash_for_scheduled_order, 'time slot hash'
      end
      swagger_model :time_slot_hash_for_scheduled_order do
        description 'details of time slot hash'
        property :id, :integer, :required, 'id of the chosen slot for the order'
      end

      ##################################################################################
      #                             private Function                                   #
      ##################################################################################

      private

      #
      # Update cart params containing hash
      #  - cart
      #   {
      #     - order_products(Array)
      #       [{
      #         * id: order product id
      #         * quantity: quantity of the order product (Exact quantity in integer)
      #         * product:
      #           {
      #             * id: selected product id (MPSP id)
      #           }
      #       }]
      #   }
      #
      def place_order_params
        params.require(:order).permit(cart: [:id], address: [:id])
      end

      def schedule_order_params
        params.require(:order).permit(preferred_slot: [:slot_date, time_slot: [:id]])
      end

    end
  end
end
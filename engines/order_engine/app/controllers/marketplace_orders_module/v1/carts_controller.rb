module MarketplaceOrdersModule

  module V1

    class CartsController < BaseModule::V1::ApplicationController
      #
      # initialize CartsController Class
      #
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      UPDATE_CART_API = MarketplaceOrdersModule::V1::UpdateCartApi
      GET_CART_API = MarketplaceOrdersModule::V1::GetCartApi

      # This block will be executed before any API call / action call
      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end

      def initialize
        # Can initialize common instance variable, if needed
      end

      ##################################################################################
      #                             update_cart API                                    #
      ##################################################################################

      #
      # Function to update cart
      # Create a cart if its not present already
      #
      # POST /cart/update
      #
      # Request::
      #   * update_cart_params [hash] it contains cart and its order products
      # check private function update_cart_params for more details
      #
      # Response::
      #   * sends ok response to the panel
      #
      def update_cart
        update_cart_api = UPDATE_CART_API.new(@deciding_params)
        response = update_cart_api.enact(update_cart_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :carts_controller, 'MarketplaceOrdersModule APIs'
      swagger_api :update_cart do
        summary 'It updates (or creates if not present) the cart with given input'
        notes 'update cart with given order products. It can create the cart if not present already'
        param :body, :update_cart_request, :update_cart_request, :required,
              'update_cart_request request'
        response :ok, 'if users cart is present', :users_cart_response
        response :bad_request, 'if request params are incorrect or api fails to create/update cart'
        response :forbidden, 'User does not have permissions to access this page'
        response :not_found, 'User not found'
        response :run_time_error, 'if api fails to create/update cart'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :update_cart_request do
        description 'request schema for /update_cart API'
        property :cart, :cart_params_for_update_cart, :required,
                 'cart_params_for_update_cart model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :cart_params_for_update_cart do
        description 'details of cart_params_for_update_cart'
        property :order_products, :array, :required, 'MRP of MPSP',
                { 'items': { 'type': :order_products_for_update_cart } }
      end

      swagger_model :order_products_for_update_cart do
        description 'details of order product with quantity and MPSP to make OrderProduct'
        property :quantity, :integer, :required, 'quantity of order product'
        property :product, :product_hash_for_update_cart, :required, 'product hash of MPSP'
      end

      swagger_model :product_hash_for_update_cart do
        description 'details of MPSP with its id'
        property :id, :integer, :required, 'id reference to MPSP'
      end

      ##################################################################################
      #                             get_cart API                                       #
      ##################################################################################

      #
      # Function to get users ACTIVE/EMPTY cart
      #
      # GET /cart
      #
      # Request::
      #   * update_cart_params [hash] it contains cart and its order products
      # check private function update_cart_params for more details
      #
      # Response::
      #   * sends ok response to the panel
      #
      _LogActivity_
      def get_cart
        get_cart_api = GET_CART_API.new(@deciding_params)
        response = get_cart_api.enact
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :carts_controller, 'MarketplaceOrdersModule APIs'
      swagger_api :get_cart do
        summary 'It updates (or creates if not present) the cart with given input'
        notes 'update cart with given order products. It can create the cart if not present already'
        param :query, :session_token, :integer, :required, 'session token of the user request'
        response :ok, 'if users cart is present', :users_cart_response
        response :bad_request, 'if request params are incorrect or api fails to create/update cart'
        response :forbidden, 'User does not have permissions to access this page'
        response :not_found, 'User not found or no Active CART found'
      end

      swagger_model :users_cart_response do
        description 'response schema for /get_cart API'
        property :cart, :cart_params_for_update_cart, :required,
                 'cart_params_for_update_cart model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end
      swagger_model :users_cart_response do
        description 'response schema for /get_cart API'
        property :id, :integer, :required, 'id of cart'
        property :stats, :cart_stats_response, :required, 'cart stats response'
                property :order_products, :array, :required, 'MRP of MPSP',
                { 'items': { 'type': :order_products_response } } 
      end
      swagger_model :cart_stats_response do
        description 'response schema for /get_cart API'
        property :user_id, :integer, :required, 'id of cart'
        property :user_cart_status, :integer, :required, 'cart stats response'
        property :cart_savings, :double, :required, 'order products response'
        property :cart_total, :double, :required, 'order products response'
        property :total_mrp, :double, :required, 'order products response'
        property :delivery_charges, :double, :required, 'order products response'
      end
      swagger_model :order_products_response do
        description 'response schema for /get_cart API'
        property :id, :integer, :required, 'id of cart'
        property :quantity, :integer, :required, 'cart stats response'
        property :product, :product_hash_for_order_products_response, :required, 'order products response'
      end
      swagger_model :product_hash_for_order_products_response do
        description 'response schema for /get_cart API'
        property :id, :integer, :required, 'id of cart'
      end

      ##################################################################################
      #                             private Function                                   #
      ##################################################################################

      private

      #
      # Update cart params containing hash
      #  - cart
      #   {
      #     - order_products(Array)
      #       [{
      #         * id: order product id
      #         * quantity: quantity of the order product (Exact quantity in integer)
      #         * product:
      #           {
      #             * id: selected product id (MPSP id)
      #           }
      #       }]
      #   }
      #
      def update_cart_params
        params.require(:cart).permit(order_products: [:quantity, product: [:id]])
      end

    end
  end
end
#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Class to implement controller functions of inventory order related APIs
    #
    class InventoryOrdersController < BaseModule::V1::ApplicationController
      #
      # initialize API Class
      #
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      CREATE_INVENTORY_ORDER_API = InventoryOrdersModule::V1::CreateInventoryOrderApi
      UPDATE_INVENTORY_ORDER_API = InventoryOrdersModule::V1::UpdateInventoryOrderApi
      PLACE_INVENTORY_ORDER_API = InventoryOrdersModule::V1::PlaceInventoryOrderApi
      GET_INVENTORY_ORDER_API = InventoryOrdersModule::V1::GetInventoryOrderApi
      CHANGE_INVENTORY_ORDER_STATE_API = InventoryOrdersModule::V1::ChangeInventoryOrderStateApi
      VERIFY_INVENTORY_ORDER_API = InventoryOrdersModule::V1::VerifyInventoryOrderApi
      GET_ALL_INVENTORY_ORDERS_API = InventoryOrdersModule::V1::GetAllInventoryOrdersApi
      STOCK_INVENTORY_ORDER_API = InventoryOrdersModule::V1::StockInventoryOrderApi
      # This block will be executed before any API call / action call
      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end

      def initialize
        # Can initialize common instance variable, if needed
      end

      ##################################################################################
      #                       create_purchase_order API                                #
      ##################################################################################

      #
      # Function to place a new Purchase order
      #
      # POST inventory/orders/new
      #
      # Request::
      #   * create_order_params [hash], it contains info of user, warehouse, inventory & address
      #
      # Response::
      #   * sends ok response to the panel
      #
      _LogActivity_
      def create_purchase_order
        create_inventory_order_api = CREATE_INVENTORY_ORDER_API.new(@deciding_params)
        response = create_inventory_order_api.enact(create_purchase_order_params)
        send_response(response)
      end

      swagger_controller :inventory_orders_controller, 'InventoryOrdersModule APIs'
      swagger_api :create_purchase_order do
        summary 'It creates a new purchase order at the distributor/brand'
        notes 'purchase_order_params creates a new PO for a particular Inventory for a particular warehouse'
        param :body, :purchase_order_request, :purchase_order_request, :required,
              'purchase_order_request'
        response :bad_request, 'if request params are incorrect or api fails to create purchase order'
        response :forbidden, 'User does not have permissions to access this page'
      end

      swagger_model :purchase_order_request do
        description 'request schema for /inventory/orders/new API'
        property :inventory_order, :purchase_order_params, :required,
                 'purchase_order_params model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :purchase_order_params do
        description 'hash for purchase order request'
        property :warehouse, :warehouse_details, :required, 'PO initiated at which warehouse'
        property :inventory, :inventory_details, :required, 'PO for which inventory'
        property :address, :address_details, :required, 'address details for PO'
      end

      swagger_model :warehouse_details do
        description 'hash of warehouse details'
        property :id, :string, :required, 'id of warehouse which is creating PO'
      end

      swagger_model :inventory_details do
        description 'hash of inventory details'
        property :id, :string, :required, 'id of inventory for which PO is being created'
      end

      swagger_model :address_details do
        description 'hash of address_details details'
        property :shipping_address, :shipping_address_details, :required, 'shipping_address_details'
        property :billing_address, :billing_address_details, :required, 'billing_address_details'
      end

      swagger_model :shipping_address_details do
        description 'hash of shipping_address details'
        property :id, :string, :required, 'id of shipping_address of PO'
      end

      swagger_model :billing_address_details do
        description 'hash of billing_address details'
        property :id, :string, :required, 'id of billing_address of PO'
      end

      ##################################################################################
      #                             update_purchase_order API                          #
      ##################################################################################

      #
      # Function to update a Purchase order
      #
      # PUT inventory/orders/:id
      #
      # Request::
      #   * update_order_params [hash], it contains info of user, warehouse, inventory & address
      #     and product to be added in the purchase order with it's qty.
      #
      # Response::
      #   * sends ok response to the panel
      #
      _LogActivity_
      def update_purchase_order
        update_inventory_order_api = UPDATE_INVENTORY_ORDER_API.new(@deciding_params)
        response = update_inventory_order_api.enact(update_purchase_order_params.merge(id: params[:id]))
        send_response(response)
      end

      swagger_controller :inventory_orders_controller, 'InventoryOrdersModule APIs'
      swagger_api :update_purchase_order do
        summary 'It updates a purchase order at the distributor/brand'
        notes 'purchase_order_params updates a PO for a particular Inventory for a particular warehouse'
        param :path, :id, :string, :required, 'ID of order to be updated'
        param :body, :update_purchase_order_request, :update_purchase_order_request, :required,
              'update_purchase_order_request'
        response :bad_request, 'if request params are incorrect or api fails to update purchase order'
        response :forbidden, 'User does not have permissions to access this page'
      end

      swagger_model :update_purchase_order_request do
        description 'request schema for PUT /inventory/orders/:id API'
        property :inventory_order, :update_purchase_order_params, :required,
                 'update_purchase_order_params model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :update_purchase_order_params do
        description 'hash for purchase order request'
        property :is_inter_state, :string, :required, 'If PO is placed on inter state ditributor'
        property :warehouse, :warehouse_details, :required, 'PO initiated at which warehouse'
        property :inventory, :inventory_details, :required, 'PO for which inventory'
        property :address, :address_details, :required, 'address details for PO'
        property :products, :array, :required, 'products to be added in PO',
                 { 'items': { 'type': :product_details } }
      end

      swagger_model :product_details do
        description 'array of hash of product to be added in PO with its quantity'
        property :inventory_brand_pack_id, :string, :required, 'ID of IBP'
        property :quantity_ordered, :string, :required, 'Quantity of IBP to be ordered'
      end

      ##################################################################################
      #                              place_purchase_order API                          #
      ##################################################################################

      #
      # Function to place a Purchase order
      #
      # PUT inventory/orders/place
      #
      # Request::
      #   * place_order_params [hash], it contains id of Purchase Order to be placed
      #
      # Response::
      #   * sends ok response to the panel
      #
      _LogActivity_
      def place_purchase_order
        place_inventory_order_api = PLACE_INVENTORY_ORDER_API.new(@deciding_params)
        response = place_inventory_order_api.enact(place_purchase_order_params)
        send_response(response)
      end

      swagger_controller :inventory_orders_controller, 'InventoryOrdersModule APIs'
      swagger_api :place_purchase_order do
        summary 'It places a purchase order at the distributor/brand'
        notes 'purchase_order_params places a PO for a particular Inventory for a particular warehouse'
        param :body, :place_purchase_order_request, :place_purchase_order_request, :required,
              'place_purchase_order_request'
        response :bad_request, 'if request params are incorrect or api fails to place purchase order'
        response :forbidden, 'User does not have permissions to access this page'
      end

      swagger_model :place_purchase_order_request do
        description 'request schema for PUT /inventory/orders/:id API'
        property :inventory_order, :place_purchase_order_params, :required,
                 'place_purchase_order_params model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :place_purchase_order_params do
        description 'hash for purchase order request'
        property :id, :string, :required, 'ID of PO to be placed'      
      end

      ##################################################################################
      #                        change_purchase_order_state API                         #
      ##################################################################################

      #
      # Function to change state of PO
      #
      # PUT inventory/orders/state/:id
      #
      # Request::
      #   * change_state_params [hash], event to trigger change state
      #
      # Response::
      #   * sends ok response to the panel
      #
      _LogActivity_
      def change_purchase_order_state
        change_inventory_order_state_api = CHANGE_INVENTORY_ORDER_STATE_API.new(@deciding_params)
        response = change_inventory_order_state_api.enact(change_purchase_order_state_params.merge(id: params[:id]))
        send_response(response)
      end

      swagger_controller :inventory_orders_controller, 'InventoryOrdersModule APIs'
      swagger_api :change_purchase_order_state do
        summary 'It changes the state of PO'
        notes 'changes the state of PO'
        param :path, :id, :string, :required, 'ID of PO of which state is to be changed'
        param :body, :change_purchase_order_state_hash, :change_purchase_order_state_hash, :required,
              'change_purchase_order_state_hash'
        response :bad_request, 'if request params are incorrect or api fails to change state'
        response :forbidden, 'User does not have permissions to access this page'
      end

      swagger_model :change_purchase_order_state_hash do
        description 'request schema for PUT /inventory/orders/state/:id API'
        property :inventory_order, :change_purchase_order_state_params, :required,
                 'receive_purchase_order model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :change_purchase_order_state_params do
        description 'hash for purchase order request'
        property :event, :string, :required, 'event to trigger status change'    
      end

      ##################################################################################
      #                              verify_purchase_order API                         #
      ##################################################################################

      #
      # Function to verify PO
      #
      # PUT inventory/orders/verify/:id
      #
      # Request::
      #   * verify_purchase_order [hash], to verify the PO ad update the attributes of PO
      #
      # Response::
      #   * sends ok response to the panel
      #
      _LogActivity_
      def verify_purchase_order
        verify_inventory_order_api = VERIFY_INVENTORY_ORDER_API.new(@deciding_params)
        response = verify_inventory_order_api.enact(verify_purchase_order_params.merge(id: params[:id]))
        send_response(response)
      end

      swagger_controller :inventory_orders_controller, 'InventoryOrdersModule APIs'
      swagger_api :verify_purchase_order do
        summary 'It changes the state of PO'
        notes 'changes the state of PO'
        param :path, :id, :string, :required, 'ID of PO, which is to be updated'
        param :body, :verify_purchase_order_hash, :verify_purchase_order_hash, :required,
              'verify_purchase_order_hash'
        response :bad_request, 'if request params are incorrect or api fails to change state'
        response :forbidden, 'User does not have permissions to access this page'
      end

      swagger_model :verify_purchase_order_hash do
        description 'request schema for PUT /inventory/orders/verify/:id API'
        property :inventory_order, :verify_purchase_order_params, :required,
                 'receive_purchase_order model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :verify_purchase_order_params do
        description 'hash for purchase order request'
        property :payment, :verify_payment_details, :required, 'payment details hash'   
        property :products, :array, :required, 'products detail hash', { 'items': { 'type': :verify_product_details } }
      end

      swagger_model :verify_product_details do
        description 'hash of products with final details'
        property :id, :string, :required, 'ID of inventory_order_product'
        property :quantity_received, :string, :required, 'Qty of inventory_order_product received'
        property :mrp, :string, :required, 'MRP of inventory_order_product'
        property :cost_price, :string, :required, 'cost_price of inventory_order_product'
      end

      swagger_model :verify_payment_details do
        description 'hash of final payment details'
        property :total_octrai, :string, :required, 'Total octrai on inventory_order'
      end
      ##################################################################################
      #                                get_purchase_order API                          #
      ##################################################################################

      #
      # Function to get details of a Purchase order
      #
      # API: GET inventory/orders/:id
      #
      # Request::
      #   * ID of PO of which details are requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      _LogActivity_
      def get_purchase_order
        get_inventory_order_api = GET_INVENTORY_ORDER_API.new(@deciding_params)
        response = get_inventory_order_api.enact(params[:id])
        send_response(response)
      end

      swagger_controller :inventory_orders_controller, 'InventoryOrdersModule APIs'
      swagger_api :get_purchase_order do
        summary 'It fetches the details of the purchase order'
        notes 'It fetches the details of the PO using the id given by the user'
        param :path, :id, :string, :required, 'ID of order to fetch'
        param :query, :session_token, :string, :required, 'to authenticate the user'
        response :bad_request, 'if PO does not exists with that ID'
        response :forbidden, 'User does not have permissions to access this page'
      end

      ##################################################################################
      #                              get_all_purchase_order API                        #
      ##################################################################################

      #
      # Function to get all Purchase order of a prticular warehouse
      #
      # API: GET inventory/orders/warehouse/:warehouse_id
      #
      # Request::
      #   * pagination params
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_all_purchase_order
        get_all_inventory_orders_api = GET_ALL_INVENTORY_ORDERS_API.new(@deciding_params)
        response = get_all_inventory_orders_api.enact(pagination_params.merge(warehouse_id: params[:warehouse_id]))
        send_response(response)
      end

      swagger_controller :inventory_orders_controller, 'InventoryOrdersModule APIs'
      swagger_api :get_all_purchase_order do
        summary 'It fetches the details of the purchase order'
        notes 'get_all_orders API returns the minimal details of all orders placed by a prticular warehouse'
        param :path, :warehouse_id, :string, :required, 'ID of warehouse of which IO to be fetched'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :from_time, :integer, :optional, 'Epoch timestamp of from date-time'
        param :query, :to_time, :integer, :optional, 'Epoch timestamp of TO date-time'
        param :query, :page_no, :integer, :optional, 'page no from which to get all orders'
        param :query, :per_page, :integer, :optional, 'how many orders to display in a page'
        param :query, :state, :integer, :optional, 'to fetch orders based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
      end

      ##################################################################################
      #                               stock_purchase_order API                         #
      ##################################################################################

      #
      # Function to stock PO
      #
      # PUT inventory/orders/stock/:id
      #
      # Request::
      #   * stock_purchase_order [hash], to stock the PO and update thewarehouse stock
      #
      # Response::
      #   * sends ok response to the panel
      #
      _LogActivity_
      def stock_purchase_order
        stock_inventory_order_api = STOCK_INVENTORY_ORDER_API.new(@deciding_params)
        response = stock_inventory_order_api.enact(stock_purchase_order_params.merge(id: params[:id]))
        send_response(response)
      end

      swagger_controller :inventory_orders_controller, 'InventoryOrdersModule APIs'
      swagger_api :stock_purchase_order do
        summary 'It stocks the PO in warehouse'
        notes 'Stocks the PO in warehouse & changes PO status to Stocked'
        param :path, :id, :string, :required, 'ID of PO, which is to be stocked'
        param :body, :stock_purchase_order_hash, :stock_purchase_order_hash, :required,
              'verify_purchase_order_hash'
        response :bad_request, 'if request params are incorrect or api fails to stock the PO'
        response :forbidden, 'User does not have permissions to access this page'
      end

      swagger_model :stock_purchase_order_hash do
        description 'request schema for PUT /inventory/orders/stock/:id API'
        property :inventory_order, :stock_purchase_order_params, :required,
                 'receive_purchase_order model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :stock_purchase_order_params do
        description 'hash for purchase order request'
        property :warehouse, :warehouse_details, :required, 'Warehouse in which PO to be stocked'
      end

      ##################################################################################
      #                             private Function                                   #
      ##################################################################################

      private

      #
      #  create_purchase_order_params  
      # {
      #   inventory_order: {
      #     warehouse: {
      #       id: '2'
      #     },
      #     inventory: {
      #       id: '3'
      #     },
      #     address: {
      #       shipping_address: {
      #         id: '4'
      #       },
      #       billing_address: {
      #         id: '5'
      #       }
      #     }
      #   }
      # }
      #
      def create_purchase_order_params
        params.require(:inventory_order).permit(warehouse: [:id], inventory: [:id], address: [shipping_address: [:id], billing_address: [:id]])
      end

      #
      #  update_purchase_order_params  
      # {
      #   inventory_order: {
      #     warehouse: {
      #       id: '2'
      #     },
      #     inventory: {
      #       id: '3'
      #     },
      #     address: {
      #       shipping_address: {
      #         id: '4'
      #       },
      #       billing_address: {
      #         id: '5'
      #       }
      #     },
      #     products: [
      #       {
      #         id: '6',
      #         quantity_ordered: '7'
      #       },
      #       {
      #         id: '8',
      #         quantity_ordered: '9'
      #       }
      #     ]
      #   }
      # }
      #
      def update_purchase_order_params
        if params[:inventory_order].present? && 
            params[:inventory_order][:products].present? && 
            params[:inventory_order][:products].is_a?(String)
          params[:inventory_order][:products] = JSON.parse(params[:inventory_order][:products])
        end
        params.require(:inventory_order).permit(:is_inter_state, warehouse: [:id], inventory: [:id], address: [shipping_address: [:id], billing_address: [:id]], products: [:inventory_brand_pack_id, :quantity_ordered])
      end

      #
      #  place_purchase_order_params  
      # {
      #   inventory_order: {
      #     id: '1'
      #   }
      # }
      #
      def place_purchase_order_params
        params.require(:inventory_order).permit(:id)
      end

      #
      # change_purchase_order_state_params
      # {
      #   inventory_order: {
      #     event: '1'
      #   }
      # }
      def change_purchase_order_state_params
        params.require(:inventory_order).permit(:event)
      end

      #
      # verify_purchase_order_params
      # {
      #   inventory_order: {
      #     products: [
      #       {
      #         id: '1',
      #         quantity_received: '2',
      #         mrp: '100',
      #         cost_price: '90',
      #       }
      #     ],
      #     payment: {
      #       total_octrai: '100'
      #     }
      #   }
      # }
      def verify_purchase_order_params
        if params[:inventory_order].present? && 
            params[:inventory_order][:products].present? && 
            params[:inventory_order][:products].is_a?(String)
          params[:inventory_order][:products] = JSON.parse(params[:inventory_order][:products])
        end
        params.require(:inventory_order).permit(products: [:id, :quantity_received, :mrp, :cost_price], payment: [:total_octrai])
      end

      #
      # stock_purchase_order_params
      # {
      #   inventory_order: {
      #     warehouse_id: '1',
      #     products: [
      #       {
      #         id: '1',
      #         quantity_received: '2'
      #       }
      #     ]
      #   }
      # }
      def stock_purchase_order_params
        params.require(:inventory_order).permit(warehouse: [:id])
      end

      def pagination_params
        params.permit(:from_time, :to_time, :page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end

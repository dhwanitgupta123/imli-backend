require 'route_constraints'
LocationEngine::Engine.routes.draw do
  # Predicate for versioning based on app version
  match_app_version_predicate = Proc.new do |version, request|
    CommonModule::V1::RoutingPredicates.match_app_version(version, request)
  end

  # Route for SearchModule with versioning and constraint matching
  scope module: :search_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      get 'location/get_area_by_query' => 'area_search#get_area_by_query'
    end
  end

  # Route for AddressModule with versioning and constraint matching
  scope module: :address_module, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: RouteConstraints.new(version: '1', default: true, predicate: match_app_version_predicate) do
      get 'areas' => 'areas#get_area_from_pincode'
      put 'areas/state' => 'areas#change_area_status'
      put 'areas/:id' => 'areas#update_area'
    end
  end
end
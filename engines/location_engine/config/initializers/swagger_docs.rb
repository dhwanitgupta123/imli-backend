class Swagger::Docs::Config
  def self.base_applications 
    [LocationEngine::Engine] 
  end
end

Swagger::Docs::Config.register_apis({
  '1.0' => {
    # the output location where your .json files are written to
    :api_file_path => 'public/api/v1/',
    # the URL base path to your API
    :base_path => '/',
    # if you want to delete all .json files at each generation
    :clean_directory => true,
    # To not camelize the model properties
    :camelize_model_properties => false,
    # json formatting
    :formatting => :pretty,
    # add custom attributes to api-docs
    :attributes => {
      :info => {
        'title' => 'Imli technologies pvt. ltd.'
      }
    }
  }
})

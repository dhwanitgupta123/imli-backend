$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'location_engine/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'location_engine'
  s.version     = LocationEngine::VERSION
  s.authors     = ['Dhwanit Gupta']
  s.email       = ['dhwanitgupta123@gmail.com']
  s.homepage    = 'https://www.trawly.com'
  s.summary     = 'Summary of LocationEngine.'
  s.description = 'Description of LocationEngine.'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.rdoc']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'rails', '~> 4.2.3'

  s.add_development_dependency 'rspec-rails'

  s.test_files = Dir['spec/**/*']
end

#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    require 'rails_helper'
    RSpec.describe AddressModule::V1::AddressResponseDecorator do
      let(:address_response_decorator) { AddressModule::V1::AddressResponseDecorator }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }

      let(:message) { FactoryGirl.build_stubbed(:message) }
      let(:provider) { FactoryGirl.build_stubbed(:provider, message: message) }
      let(:user) { FactoryGirl.build(:user, provider: provider) }
      let(:country) { FactoryGirl.build(:country) }
      let(:state) { FactoryGirl.build(:state, country: country) }
      let(:city) { FactoryGirl.build(:city, state: state) }
      let(:area) { FactoryGirl.build(:area, city: city) }
      let(:address) { FactoryGirl.build(:address, user: user, area: area) }
      
      it "with create_response_invalid_arguments_error" do
        response = address_response_decorator.create_response_invalid_arguments_error(message)
        expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
      end
      it "with create_non_existing_area_error" do
        response = address_response_decorator.create_non_existing_area_error
        expect(response[:response]).to eq(response_codes_util::NOT_FOUND)
      end
      it "with update_pincode_status_response" do
        response = address_response_decorator.update_pincode_status_response(area)
        expect(response[:response]).to eq(response_codes_util::SUCCESS)
      end
      it "with get_all_addresses_of_user_response" do
        response = address_response_decorator.get_all_addresses_of_user_response([address])
        expect(response[:response]).to eq(response_codes_util::SUCCESS)
      end
      it "with add_address_ok_response" do
        response = address_response_decorator.add_address_ok_response(address)
        expect(response[:response]).to eq(response_codes_util::SUCCESS)
      end
    end
  end
end
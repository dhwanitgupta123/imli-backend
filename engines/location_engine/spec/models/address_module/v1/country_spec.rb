module AddressModule
  module V1
    require 'rails_helper'

    RSpec.describe AddressModule::V1::Country, type: :model do
      subject { FactoryGirl.build(:country) }

      it { should be_valid }

      context 'with name ' do
        let(:valid_name) { Faker::Address.country }

        it 'nil' do
          FactoryGirl.build(:country, name: nil).should_not be_valid
        end

        it 'duplicate name' do
          FactoryGirl.create(:country, name: valid_name)
          FactoryGirl.build(:country, name: valid_name).should_not be_valid
        end
      end
    end
  end
end

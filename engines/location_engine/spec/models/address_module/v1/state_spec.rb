module AddressModule
  module V1
    require 'rails_helper'

    RSpec.describe AddressModule::V1::State, type: :model do
      let(:country) { FactoryGirl.create(:country) }
      subject { FactoryGirl.build(:state, country: country) }

      it { should be_valid }

      context 'with param ' do
        let(:valid_state) { Faker::Address.state }

        it ' name nil' do
          FactoryGirl.build(:state, name: nil, country: country).should_not be_valid
        end

        it ' country nil ' do
          FactoryGirl.build(:state, country: nil).should_not be_valid
        end
      end
    end
  end
end

module AddressModule
  module V1
    require 'rails_helper'

    RSpec.describe AddressModule::V1::City, type: :model do
      let(:country) { FactoryGirl.create(:country) }
      let(:state) { FactoryGirl.create(:state, country: country) }
      subject { FactoryGirl.create(:city, state: state) }

      it { should be_valid }

      context 'with param ' do
        let(:valid_city) { Faker::Address.city }

        it ' name nil' do
          FactoryGirl.build(:city, name: nil, state: state).should_not be_valid
        end

        it ' state nil ' do
          FactoryGirl.build(:city, state: nil).should_not be_valid
        end
      end
    end
  end
end

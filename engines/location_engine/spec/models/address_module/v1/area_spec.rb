module AddressModule
  module V1
    require 'rails_helper'

    RSpec.describe AddressModule::V1::Area, type: :model do
      let(:country) { FactoryGirl.create(:country) }
      let(:state) { FactoryGirl.create(:state, country: country) }
      let(:city) { FactoryGirl.create(:city, state: state) }
      subject { FactoryGirl.create(:area, city: city) }

      it { should be_valid }

      it 'with nil area name' do
        FactoryGirl.build(:area, city: city, name: nil).should_not be_valid
      end

      it 'with nil city name' do
        FactoryGirl.build(:area, city: nil).should_not be_valid
      end

      context 'with pincode ' do
        let(:valid_pincode) { Faker::Number.number(6) }

        it ' length not equal to 6' do
          FactoryGirl.build(:area, pincode: Faker::Number.number(5), city: city).should_not be_valid
        end

        it ' consist alphabets ' do
          FactoryGirl.build(:area, pincode: Faker::Lorem.characters(6), city: city).should_not be_valid
        end

        it ' duplicate entry ' do
          area = FactoryGirl.create(:area, pincode: valid_pincode, city: city)
          FactoryGirl.build(:area, pincode: valid_pincode, city: city).should_not be_valid
          area.delete
          city.delete
          state.delete
          country.delete
        end
      end
    end
  end
end

module SearchModule
  module V1
    require 'rails_helper'
    RSpec.describe SearchModule::V1::SearchAreaByQueryApi do
      let(:version) { 1 }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:search_area_by_query) { SearchModule::V1::SearchAreaByQueryApi.new(version) }
      let(:cache_util) { CommonModule::V1::Cache }
      let(:pincode_index_service) { SearchModule::V1::PincodeIndexService }

      let(:attributes) {
        [
          {
            title: Faker::Lorem.characters(6),
            pincode: Faker::Lorem.characters(6)
          }
        ]
      }
      let(:pincode) { SearchModule::V1::Pincode.new(attributes[0]) }
      let(:pincode_reader) { AddressModule::V1::PincodeModelReader }

      let(:request) {
        {
          query: Faker::Lorem.characters(6),
          top_n: '5'
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          allow(cache_util).to receive(:read).and_return('10')
          data = search_area_by_query.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil query' do
          expect_any_instance_of(pincode_reader).to receive(:get_attribute_array).and_return(attributes)
          expect_any_instance_of(pincode_index_service).to receive(:get_active_areas).and_return(pincode_reader.new(pincode))
          data = search_area_by_query.enact request.merge(query: nil)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end

        it 'with query less than 3 characters' do
          allow(cache_util).to receive(:read).and_return('10')
          data = search_area_by_query.enact request.merge(query: Faker::Lorem.characters(2))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid params' do
          allow(cache_util).to receive(:read).and_return('10')
          expect_any_instance_of(pincode_reader).to receive(:get_attribute_array).and_return(attributes)
          expect_any_instance_of(pincode_index_service).to receive(:initialize_pincode_repository).and_return(true)
          expect_any_instance_of(pincode_index_service).to receive(:get_results).and_return(pincode_reader.new(pincode))
          data = search_area_by_query.enact request
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end

module AddressModule
  module V1
    require 'rails_helper'
    RSpec.describe AddressModule::V1::AddArea do
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:add_area_api) { AddressModule::V1::AddArea.new }

      let(:area_service) { AddressModule::V1::AreaService }

      let(:pincode) { Faker::Number.number(6) }

      let(:city) { FactoryGirl.build_stubbed(:city) }
      let(:area) { FactoryGirl.build_stubbed(:area) }
      let(:area_reader) { AddressModule::V1::AreaReader.new(area) }

      let(:request) {
        {
          pincode: Faker::Number.number(6),
          area: Faker::Address.street_name,
          country: Faker::Address.country,
          state: Faker::Address.state,
          city: Faker::Address.city,
          is_active: true
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_area_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil pincode' do
          data = add_area_api.enact(request.merge(pincode: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid pincode' do
          data = add_area_api.enact(request.merge(pincode: Faker::Lorem.characters(6)))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with existing area' do
          expect_any_instance_of(area_service).to receive(:transactional_create_area).
            with(request).and_raise(custom_errors_util::RecordAlreadyExistsError.new)
          data = add_area_api.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid area' do
          expect_any_instance_of(area_service).to receive(:transactional_create_area).with(request).and_return(area_reader)
          data = add_area_api.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end

        it 'with un indexed pincode' do
          expect_any_instance_of(area_service).to receive(:transactional_create_area).and_raise(custom_errors_util::RunTimeError.new)
          data = add_area_api.enact(request)
          expect(data[:response]).to eq(response_codes::INTERNAL_SERVER_ERROR) 
        end
      end
    end
  end
end

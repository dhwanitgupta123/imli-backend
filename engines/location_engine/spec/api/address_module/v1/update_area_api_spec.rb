module AddressModule
  module V1
    require 'rails_helper'
    RSpec.describe AddressModule::V1::UpdateAreaApi do
      let(:version) { 1 }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:update_area) { AddressModule::V1::UpdateAreaApi.new(version) }

      let(:area_service) { AddressModule::V1::AreaService }
      let(:area_dao) { AddressModule::V1::AreaDao }


      let(:area) { FactoryGirl.build_stubbed(:area) }

      let(:request) {
        {
          name: area.name,
          id: area.id
        }
      }


      context 'enact ' do
        it 'with invalid request' do
          data = update_area.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil id' do
          data = update_area.enact(request.merge(id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with not existing area' do
          expect_any_instance_of(area_service).to receive(:get_area_by_id)
            .with(area.id).and_raise(custom_errors_util::RecordNotFoundError)
          data = update_area.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end
    end
  end
end

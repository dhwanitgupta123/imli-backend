module AddressModule
  module V1
    require 'rails_helper'
    RSpec.describe AddressModule::V1::ChangeAreaStatus do
      let(:version) { 1 }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:transactional_change_area_status_api) { AddressModule::V1::ChangeAreaStatus.new(version) }

      let(:area_service) { AddressModule::V1::AreaService }
      let(:area_dao) { AddressModule::V1::AreaDao }

      let(:pincode) { Faker::Number.number(6) }

      let(:event) { AddressModule::V1::ModelStates::V1::AreaEvents::ACTIVATE }
      let(:city) { FactoryGirl.build_stubbed(:city) }
      let(:area) { FactoryGirl.build_stubbed(:area) }
      let(:area_reader) { AddressModule::V1::AreaReader.new(area) }

      let(:request) {
        {
          event: event,
          pincode: pincode
        }
      }


      context 'enact ' do
        it 'with invalid request' do
          data = transactional_change_area_status_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil pincode' do
          request[:pincode] = nil
          data = transactional_change_area_status_api.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with pincode less than 6 digits' do
          request[:pincode] = Faker::Number.number(5).to_s
          data = transactional_change_area_status_api.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with pincode containing alphabets' do
          request[:pincode] = Faker::Lorem.characters(6)
          data = transactional_change_area_status_api.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with not existing area' do
          expect_any_instance_of(area_service).to receive(:get_area_from_pincode)
            .with(pincode).and_raise(custom_errors_util::RecordNotFoundError.new)
          data = transactional_change_area_status_api.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil event' do
          request[:event] = nil
          data = transactional_change_area_status_api.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid pincode and valid event' do
          expect_any_instance_of(area_service).to receive(:transactional_change_area_status).and_return(area)
          data = transactional_change_area_status_api.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end

        it 'with un indexed pincode' do
          expect_any_instance_of(area_service).to receive(:transactional_change_area_status).and_raise(custom_errors_util::RunTimeError.new)
          data = transactional_change_area_status_api.enact(request)
          expect(data[:response]).to eq(response_codes::INTERNAL_SERVER_ERROR) 
        end
      end
    end
  end
end

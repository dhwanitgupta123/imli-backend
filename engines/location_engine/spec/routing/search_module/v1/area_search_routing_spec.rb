#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    require 'rails_helper'

    RSpec.describe AreaSearchController, type: :routing do

      # specifying engine routes
      routes { LocationEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #get_area_by_query' do
          expect(:get => 'location/get_area_by_query').to route_to(
            {
              'format' => 'json',
              'controller' => 'search_module/v1/area_search',
              'action' => 'get_area_by_query'
            })
        end
      end
    end
  end
end
#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    require 'rails_helper'

    RSpec.describe AreasController, type: :routing do

      # specifying engine routes
      routes { LocationEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #get_area_from_pincode' do
          expect(:get => 'areas').to route_to(
            {
              'format' => 'json',
              'controller' => 'address_module/v1/areas',
              'action' => 'get_area_from_pincode'
            })
        end

        it 'routes to #change_area_status' do
          expect(:put => 'areas/state').to route_to(
            {
              'format' => 'json',
              'controller' => 'address_module/v1/areas',
              'action' => 'change_area_status'
            })
        end
      end
    end
  end
end
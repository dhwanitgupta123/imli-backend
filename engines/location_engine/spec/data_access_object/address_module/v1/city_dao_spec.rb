module AddressModule
	module V1
    require 'rails_helper'

    RSpec.describe AddressModule::V1::CityDao, type: :dao do
      let(:city_dao) { AddressModule::V1::CityDao.new }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:country) { FactoryGirl.create(:country) }
      let(:state) { FactoryGirl.create(:state, country: country) }
      let(:city_args) {
        {
          name: Faker::Address.city,
          state: state
        }
      }

      context 'create city' do
        it 'with valid name and state' do
          city = city_dao.create(city_args)
          expect(city.name).to eq(city_args[:name])
          expect(city.state_id).to eq(state.id)
          city.destroy
        end

        it 'with invalid name' do
          expect { city_dao.create(city_args.merge(name: nil)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with invalid state' do
          expect { city_dao.create(city_args.merge(state: nil)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update city' do
        let(:before_name) { Faker::Address.city }
        let(:city_before) { FactoryGirl.build(:city, name: before_name, state: state) }
        it 'with valid state' do
          city = city_dao.update(city_args, city_before)
          expect(city.name).to eq(city_args[:name])
          city.destroy
        end

        it 'with invalid city' do
          expect{ city_dao.update(city_args, nil) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with invalid city name' do
          expect{ city_dao.update(nil, city_before) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end

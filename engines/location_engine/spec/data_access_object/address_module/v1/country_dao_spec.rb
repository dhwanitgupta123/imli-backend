module AddressModule
  module V1
    require 'rails_helper'

    RSpec.describe AddressModule::V1::CountryDao, type: :dao do
      let(:country_dao) { AddressModule::V1::CountryDao.new }
      let(:country_name) { Faker::Address.country }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }

      context 'create country' do
        it 'with valid name' do
          country = country_dao.create(country_name)
          expect(country.name).to eq(country_name)
          country.destroy
        end

        it 'with invalid name' do
          expect { country_dao.create(nil) }
            .to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update country' do
        let(:new_name) { Faker::Address.country }
        let(:country_before) { FactoryGirl.build(:country, name: country_name) }
        it 'with valid country' do
          country = country_dao.update(new_name, country_before)
          expect(country.name).to eq(new_name)
          country.destroy
        end

        it 'with invalid country name' do
          expect { country_dao.update(nil,country_before) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end

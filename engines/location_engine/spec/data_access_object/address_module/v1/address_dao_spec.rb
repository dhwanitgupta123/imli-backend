module AddressModule
	module V1
    require 'rails_helper'

    RSpec.describe AddressModule::V1::AddressDao, type: :dao do
    	let(:address_dao) { AddressModule::V1::AddressDao.new }
    	let(:address_model) { AddressModule::V1::Address.new }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }


    	let(:country) { FactoryGirl.build_stubbed(:country) }
      let(:state)  { FactoryGirl.build_stubbed(:state, country: country) }
      let(:city)  { FactoryGirl.build_stubbed(:city, state: state) }
      let(:area)  { FactoryGirl.build_stubbed(:area, city: city) }
      let(:user)  { FactoryGirl.build_stubbed(:user) }
      let(:address)  { FactoryGirl.build_stubbed(:address, user: user, area: area) }

      let(:address_args) {
        {
          user_id: user.id,
          area_id: area.id,
          nickname: address.nickname,
          address_line1: address.address_line1,
          address_line2: address.address_line2,
          landmark: address.landmark
        }
      }

      context 'create address' do
        it 'with valid area and valid user' do
        	FactoryGirl.build_stubbed(:address, user: user, area: area).should be_valid
        end

        it 'with valid area and invalid user' do
        	address_args[:user_id] = nil
        	expect{ address_dao.create(address_args)}.
          to raise_error(custom_error_util::InvalidArgumentsError)
          FactoryGirl.build_stubbed(:address, user: nil, area: area).should_not be_valid
        end

        it 'with valid user and invalid area' do
          address_args[:area_id] = nil
        	expect{ address_dao.create(address_args)}.
          to raise_error(custom_error_util::InvalidArgumentsError)
          FactoryGirl.build_stubbed(:address, user: nil, area: area).should_not be_valid
        end
      end

      it 'with address nil' do
        expect{ address_dao.update(nil, nil)}.
        to raise_error(custom_error_util::InvalidArgumentsError)
      end

      it 'get address with wrong address id' do
      	expect{ address_dao.get_address_from_id("nil")}.
        to raise_error(custom_error_util::RecordNotFoundError)
      end

   	end
  end
end
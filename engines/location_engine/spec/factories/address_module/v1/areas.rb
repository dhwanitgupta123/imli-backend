module AddressModule
  module V1
    FactoryGirl.define do
      factory :area, :class => AddressModule::V1::Area do
      	association :city, factory: :city
        name Faker::Address.street_name
        pincode Faker::Number.number(6)
        initials Faker::Lorem.characters(3)
      end
    end
  end
end

module AddressModule
  module V1
    FactoryGirl.define do
      factory :address, :class => AddressModule::V1::Address do
        association :user, factory: :user
        association :area, factory: :area
        nickname ::Faker::Name.name
        address_line1 ::Faker::Address.street_address
        address_line2 ::Faker::Address.secondary_address
        landmark ::Faker::Address.street_name
      end
    end
  end
end


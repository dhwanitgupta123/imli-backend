# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160204101658) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string   "nickname"
    t.string   "address_line1"
    t.string   "address_line2"
    t.string   "landmark"
    t.integer  "address_type"
    t.integer  "user_id"
    t.integer  "area_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "recipient_name"
    t.integer  "status"
  end

  add_index "addresses", ["area_id"], name: "index_addresses_on_area_id", using: :btree
  add_index "addresses", ["user_id"], name: "index_addresses_on_user_id", using: :btree

  create_table "areas", force: :cascade do |t|
    t.string  "name"
    t.string  "pincode"
    t.integer "status"
    t.integer "city_id"
    t.string  "initials"
  end

  add_index "areas", ["city_id"], name: "index_areas_on_city_id", using: :btree

  create_table "benefits", force: :cascade do |t|
    t.string   "name"
    t.text     "details"
    t.string   "condition"
    t.string   "reward_value"
    t.string   "reward_type"
    t.integer  "status"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "benefit_key"
  end

  create_table "benefits_plans", force: :cascade do |t|
    t.integer  "plan_id"
    t.integer  "benefit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "benefits_plans", ["benefit_id"], name: "index_benefits_plans_on_benefit_id", using: :btree
  add_index "benefits_plans", ["plan_id"], name: "index_benefits_plans_on_plan_id", using: :btree

  create_table "brand_packs", force: :cascade do |t|
    t.integer  "images",          default: [],                                                      array: true
    t.string   "sku"
    t.float    "mrp",             default: 0.0
    t.string   "sku_size"
    t.string   "unit"
    t.string   "description"
    t.string   "shelf_life"
    t.string   "brand_pack_code", default: "nextval('brand_pack_code_seq'::regclass)"
    t.integer  "status"
    t.string   "article_code"
    t.string   "retailer_pack"
    t.integer  "product_id"
    t.integer  "sub_category_id"
    t.datetime "created_at",                                                           null: false
    t.datetime "updated_at",                                                           null: false
  end

  add_index "brand_packs", ["product_id"], name: "index_brand_packs_on_product_id", using: :btree
  add_index "brand_packs", ["sub_category_id"], name: "index_brand_packs_on_sub_category_id", using: :btree

  create_table "brands", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.string   "logo_url"
    t.string   "initials"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "company_id"
  end

  add_index "brands", ["company_id"], name: "index_brands_on_company_id", using: :btree

  create_table "carts", force: :cascade do |t|
    t.integer  "user_cart_status"
    t.decimal  "total_mrp",        default: 0.0
    t.decimal  "cart_savings",     default: 0.0
    t.decimal  "cart_total",       default: 0.0
    t.integer  "user_id"
    t.integer  "order_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "carts", ["order_id"], name: "index_carts_on_order_id", using: :btree
  add_index "carts", ["user_id"], name: "index_carts_on_user_id", using: :btree

  create_table "cash_and_carries", force: :cascade do |t|
    t.string   "name"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "label"
    t.string   "image_url"
    t.integer  "department_id"
    t.integer  "parent_category_id"
    t.integer  "status"
    t.string   "description"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "priority"
  end

  add_index "categories", ["department_id"], name: "index_categories_on_department_id", using: :btree
  add_index "categories", ["parent_category_id"], name: "index_categories_on_parent_category_id", using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "initials"
  end

  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "cluster_contexts", force: :cascade do |t|
    t.integer  "level_id"
    t.integer  "level"
    t.integer  "cluster_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "cluster_contexts", ["cluster_id"], name: "index_cluster_contexts_on_cluster_id", using: :btree

  create_table "clusters", force: :cascade do |t|
    t.string   "label"
    t.integer  "status"
    t.integer  "cluster_type"
    t.integer  "images",       default: [],              array: true
    t.text     "description"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "clusters_marketplace_selling_packs", force: :cascade do |t|
    t.integer "cluster_id"
    t.integer "marketplace_selling_pack_id"
  end

  add_index "clusters_marketplace_selling_packs", ["cluster_id"], name: "index_clusters_marketplace_selling_packs_on_cluster_id", using: :btree
  add_index "clusters_marketplace_selling_packs", ["marketplace_selling_pack_id"], name: "mpsp", using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "logo_url"
    t.string   "vat_number"
    t.string   "tin_number"
    t.string   "cst"
    t.string   "account_details"
    t.string   "trade_promotion_margin"
    t.string   "data_sharing_margin"
    t.string   "incentives"
    t.string   "legal_document_url"
    t.string   "initials"
    t.integer  "status"
    t.datetime "reconciliation_period"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "initials"
  end

  create_table "departments", force: :cascade do |t|
    t.string   "label"
    t.string   "image_url"
    t.integer  "status"
    t.string   "description"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "priority"
    t.integer  "images",      default: [],              array: true
  end

  create_table "devices", force: :cascade do |t|
    t.string   "device_id"
    t.string   "platform"
    t.string   "app_version"
    t.datetime "last_login"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "devices", ["user_id"], name: "index_devices_on_user_id", using: :btree

  create_table "distributors", force: :cascade do |t|
    t.string   "name"
    t.integer  "status"
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.string   "cst_no"
    t.string   "tin_no"
    t.integer  "vendor_code", default: "nextval('vendor_code_seq'::regclass)"
    t.integer  "company_id"
  end

  add_index "distributors", ["company_id"], name: "index_distributors_on_company_id", using: :btree

  create_table "emails", force: :cascade do |t|
    t.string   "email_id"
    t.integer  "account_type"
    t.string   "authentication_token"
    t.boolean  "verified"
    t.datetime "expires_at"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "user_id"
    t.boolean  "is_primary",           default: false
  end

  add_index "emails", ["user_id"], name: "index_emails_on_user_id", using: :btree

  create_table "employees", force: :cascade do |t|
    t.string   "designation"
    t.string   "team"
    t.integer  "manager_id"
    t.integer  "workflow_state"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "employees", ["manager_id"], name: "index_employees_on_manager_id", using: :btree
  add_index "employees", ["user_id"], name: "index_employees_on_user_id", using: :btree

  create_table "generics", force: :cascade do |t|
    t.string   "password"
    t.integer  "provider_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "reset_flag",  default: false
  end

  add_index "generics", ["provider_id"], name: "index_generics_on_provider_id", using: :btree

  create_table "images", force: :cascade do |t|
    t.string   "image_local_file_name"
    t.string   "image_local_content_type"
    t.integer  "image_local_file_size"
    t.datetime "image_local_updated_at"
    t.string   "image_s3_file_name"
    t.string   "image_s3_content_type"
    t.integer  "image_s3_file_size"
    t.datetime "image_s3_updated_at"
    t.integer  "status"
    t.string   "resource"
  end

  create_table "inventories", force: :cascade do |t|
    t.string   "name"
    t.integer  "status"
    t.integer  "distributor_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "inventories", ["distributor_id"], name: "index_inventories_on_distributor_id", using: :btree

  create_table "inventory_addresses", force: :cascade do |t|
    t.string   "inventory_name"
    t.string   "address_line1"
    t.string   "address_line2"
    t.string   "landmark"
    t.integer  "inventory_id"
    t.integer  "area_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "inventory_addresses", ["area_id"], name: "index_inventory_addresses_on_area_id", using: :btree
  add_index "inventory_addresses", ["inventory_id"], name: "index_inventory_addresses_on_inventory_id", using: :btree

  create_table "inventory_brand_packs", force: :cascade do |t|
    t.integer  "status"
    t.integer  "inventory_id"
    t.integer  "brand_pack_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "inventory_brand_packs", ["brand_pack_id"], name: "index_inventory_brand_packs_on_brand_pack_id", using: :btree
  add_index "inventory_brand_packs", ["inventory_id"], name: "index_inventory_brand_packs_on_inventory_id", using: :btree

  create_table "inventory_brand_packs_warehouse_brand_packs", id: false, force: :cascade do |t|
    t.integer "warehouse_brand_pack_id"
    t.integer "inventory_brand_pack_id"
  end

  add_index "inventory_brand_packs_warehouse_brand_packs", ["inventory_brand_pack_id"], name: "idx_through_on_ibp", using: :btree
  add_index "inventory_brand_packs_warehouse_brand_packs", ["warehouse_brand_pack_id"], name: "idx_through_on_wbp", using: :btree

  create_table "inventory_order_payments", force: :cascade do |t|
    t.decimal  "total_mrp",          default: 0.0
    t.decimal  "total_cost_price",   default: 0.0
    t.decimal  "total_cst_value",    default: 0.0
    t.decimal  "total_octrai_value", default: 0.0
    t.decimal  "total_vat_value",    default: 0.0
    t.decimal  "total_tax",          default: 0.0
    t.decimal  "net_total",          default: 0.0
    t.integer  "status"
    t.integer  "inventory_order_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "inventory_order_payments", ["inventory_order_id"], name: "index_inventory_order_payments_on_inventory_order_id", using: :btree

  create_table "inventory_order_products", force: :cascade do |t|
    t.integer  "quantity_ordered",        default: 0
    t.integer  "quantity_received"
    t.decimal  "mrp",                     default: 0.0
    t.decimal  "cost_price",              default: 0.0
    t.decimal  "vat",                     default: 0.0
    t.decimal  "vat_tax_value",           default: 0.0
    t.decimal  "service_tax",             default: 0.0
    t.decimal  "service_tax_value",       default: 0.0
    t.decimal  "cst",                     default: 0.0
    t.decimal  "cst_value",               default: 0.0
    t.decimal  "octrai",                  default: 0.0
    t.decimal  "octrai_value",            default: 0.0
    t.integer  "buying_status"
    t.integer  "inventory_order_id"
    t.integer  "inventory_brand_pack_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "inventory_order_products", ["inventory_brand_pack_id"], name: "index_inventory_order_products_on_inventory_brand_pack_id", using: :btree
  add_index "inventory_order_products", ["inventory_order_id"], name: "index_inventory_order_products_on_inventory_order_id", using: :btree

  create_table "inventory_orders", force: :cascade do |t|
    t.string   "order_id"
    t.integer  "status"
    t.integer  "user_id"
    t.integer  "warehouse_id"
    t.integer  "inventory_id"
    t.integer  "billing_address_id"
    t.integer  "shipping_address_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "pdf_link"
    t.boolean  "is_inter_state"
  end

  add_index "inventory_orders", ["inventory_id"], name: "index_inventory_orders_on_inventory_id", using: :btree
  add_index "inventory_orders", ["user_id"], name: "index_inventory_orders_on_user_id", using: :btree
  add_index "inventory_orders", ["warehouse_id"], name: "index_inventory_orders_on_warehouse_id", using: :btree

  create_table "inventory_pricings", force: :cascade do |t|
    t.decimal  "net_landing_price",       default: 0.0
    t.decimal  "margin",                  default: 0.0
    t.decimal  "service_tax",             default: 0.0
    t.decimal  "vat",                     default: 0.0
    t.decimal  "spat_per_unit",           default: 0.0
    t.integer  "inventory_brand_pack_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.decimal  "cst",                     default: 0.0
    t.decimal  "octrai",                  default: 0.0
  end

  add_index "inventory_pricings", ["inventory_brand_pack_id"], name: "index_inventory_pricings_on_inventory_brand_pack_id", using: :btree

  create_table "marketplace_brand_pack_pricings", force: :cascade do |t|
    t.decimal  "spat",                      default: 0.0
    t.decimal  "margin",                    default: 0.0
    t.decimal  "service_tax",               default: 0.0
    t.decimal  "vat",                       default: 0.0
    t.decimal  "discount",                  default: 0.0
    t.decimal  "buying_price",              default: 0.0
    t.decimal  "selling_price",             default: 0.0
    t.decimal  "mrp",                       default: 0.0
    t.decimal  "savings",                   default: 0.0
    t.integer  "marketplace_brand_pack_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "marketplace_brand_pack_pricings", ["marketplace_brand_pack_id"], name: "idx_mpbp_pricing", using: :btree

  create_table "marketplace_brand_pack_seller_brand_packs", force: :cascade do |t|
    t.integer  "seller_brand_pack_id"
    t.integer  "marketplace_brand_pack_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "marketplace_brand_pack_seller_brand_packs", ["marketplace_brand_pack_id"], name: "idx_through_mpbp_sbp", using: :btree
  add_index "marketplace_brand_pack_seller_brand_packs", ["seller_brand_pack_id"], name: "idx_through_sbp", using: :btree

  create_table "marketplace_brand_packs", force: :cascade do |t|
    t.integer  "status"
    t.integer  "brand_pack_id"
    t.boolean  "is_on_sale",               default: false
    t.boolean  "is_ladder_pricing_active", default: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "marketplace_brand_packs", ["brand_pack_id"], name: "index_marketplace_brand_packs_on_brand_pack_id", using: :btree

  create_table "marketplace_selling_pack_ladder_pricings", force: :cascade do |t|
    t.integer  "quantity",                            default: 0
    t.decimal  "selling_price",                       default: 0.0
    t.decimal  "additional_savings",                  default: 0.0
    t.integer  "marketplace_selling_pack_pricing_id"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.boolean  "checkpoint",                          default: false
  end

  add_index "marketplace_selling_pack_ladder_pricings", ["marketplace_selling_pack_pricing_id"], name: "idx_mpsp_ladder_pricing", using: :btree

  create_table "marketplace_selling_pack_marketplace_brand_packs", force: :cascade do |t|
    t.integer  "marketplace_selling_pack_id"
    t.integer  "marketplace_brand_pack_id"
    t.integer  "quantity",                    default: 0
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "marketplace_selling_pack_marketplace_brand_packs", ["marketplace_brand_pack_id"], name: "idx_through_mpbp", using: :btree
  add_index "marketplace_selling_pack_marketplace_brand_packs", ["marketplace_selling_pack_id"], name: "idx_through_mpsp", using: :btree

  create_table "marketplace_selling_pack_pricings", force: :cascade do |t|
    t.decimal  "mrp",                         default: 0.0
    t.decimal  "base_selling_price",          default: 0.0
    t.decimal  "savings",                     default: 0.0
    t.decimal  "vat",                         default: 0.0
    t.decimal  "service_tax",                 default: 0.0
    t.decimal  "taxes",                       default: 0.0
    t.decimal  "discount",                    default: 0.0
    t.integer  "marketplace_selling_pack_id"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "marketplace_selling_pack_pricings", ["marketplace_selling_pack_id"], name: "idx_mpsp_pricing", using: :btree

  create_table "marketplace_selling_packs", force: :cascade do |t|
    t.integer  "max_quantity",             default: 0
    t.boolean  "is_on_sale",               default: false
    t.boolean  "is_ladder_pricing_active", default: false
    t.integer  "status"
    t.string   "display_name"
    t.string   "display_pack_size"
    t.string   "primary_tags"
    t.string   "secondary_tags"
    t.text     "description"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "mpsp_sub_category_id"
    t.integer  "images",                   default: [],                 array: true
    t.boolean  "dirty_bit",                default: false
  end

  add_index "marketplace_selling_packs", ["mpsp_sub_category_id"], name: "index_marketplace_selling_packs_on_mpsp_sub_category_id", using: :btree

  create_table "membership_payments", force: :cascade do |t|
    t.integer  "mode"
    t.decimal  "grand_total",          default: 0.0
    t.decimal  "billing_amount",       default: 0.0
    t.decimal  "refund"
    t.decimal  "total_vat_tax",        default: 0.0
    t.decimal  "total_service_tax",    default: 0.0
    t.decimal  "payment_mode_savings", default: 0.0
    t.decimal  "discount",             default: 0.0
    t.decimal  "gift_card",            default: 0.0
    t.decimal  "total_savings",        default: 0.0
    t.decimal  "net_total",            default: 0.0
    t.integer  "status"
    t.integer  "membership_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "membership_payments", ["membership_id"], name: "index_membership_payments_on_membership_id", using: :btree

  create_table "memberships", force: :cascade do |t|
    t.datetime "starts_at"
    t.datetime "expires_at"
    t.integer  "workflow_state"
    t.integer  "plan_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "quantity"
  end

  add_index "memberships", ["plan_id"], name: "index_memberships_on_plan_id", using: :btree

  create_table "memberships_users", force: :cascade do |t|
    t.integer  "membership_id"
    t.integer  "user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "memberships_users", ["membership_id"], name: "index_memberships_users_on_membership_id", using: :btree
  add_index "memberships_users", ["user_id"], name: "index_memberships_users_on_user_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.string   "otp"
    t.datetime "expires_at"
    t.integer  "provider_id"
    t.integer  "resend_count"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "messages", ["provider_id"], name: "index_messages_on_provider_id", using: :btree

  create_table "mpsp_categories", force: :cascade do |t|
    t.string   "label"
    t.integer  "mpsp_department_id"
    t.integer  "mpsp_parent_category_id"
    t.integer  "status"
    t.string   "description"
    t.integer  "priority"
    t.integer  "images",                  default: [],              array: true
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "mpsp_categories", ["mpsp_department_id"], name: "index_mpsp_categories_on_mpsp_department_id", using: :btree
  add_index "mpsp_categories", ["mpsp_parent_category_id"], name: "index_mpsp_categories_on_mpsp_parent_category_id", using: :btree

  create_table "mpsp_departments", force: :cascade do |t|
    t.string   "label"
    t.integer  "status"
    t.string   "description"
    t.integer  "priority"
    t.integer  "images",      default: [],              array: true
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "order_products", force: :cascade do |t|
    t.integer  "quantity",                    default: 0
    t.integer  "buying_status"
    t.integer  "cart_id"
    t.integer  "marketplace_selling_pack_id"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.decimal  "mrp",                         default: 0.0
    t.decimal  "selling_price",               default: 0.0
    t.decimal  "savings",                     default: 0.0
    t.decimal  "vat",                         default: 0.0
    t.decimal  "service_tax",                 default: 0.0
    t.decimal  "taxes",                       default: 0.0
    t.decimal  "discount",                    default: 0.0
  end

  add_index "order_products", ["cart_id"], name: "index_order_products_on_cart_id", using: :btree
  add_index "order_products", ["marketplace_selling_pack_id"], name: "index_order_products_on_marketplace_selling_pack_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.string   "order_id"
    t.string   "ship_by"
    t.string   "status"
    t.integer  "user_id"
    t.integer  "address_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "state_attributes",        default: ""
    t.date     "preferred_delivery_date"
    t.integer  "time_slot_id"
    t.datetime "order_date"
  end

  add_index "orders", ["address_id"], name: "index_orders_on_address_id", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "payments", force: :cascade do |t|
    t.integer  "mode"
    t.decimal  "grand_total",          default: 0.0
    t.decimal  "total_vat_tax",        default: 0.0
    t.decimal  "total_service_tax",    default: 0.0
    t.decimal  "payment_mode_savings", default: 0.0
    t.decimal  "discount",             default: 0.0
    t.decimal  "gift_card",            default: 0.0
    t.decimal  "total_savings",        default: 0.0
    t.decimal  "net_total",            default: 0.0
    t.integer  "status"
    t.integer  "order_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.decimal  "billing_amount"
    t.decimal  "delivery_charges"
    t.integer  "payment_instance"
  end

  add_index "payments", ["order_id"], name: "index_payments_on_order_id", using: :btree

  create_table "plans", force: :cascade do |t|
    t.string   "name"
    t.text     "details"
    t.integer  "duration"
    t.text     "validity_label"
    t.integer  "fee"
    t.integer  "status"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "plan_key"
    t.integer  "images",         default: [],              array: true
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.string   "logo_url"
    t.string   "initials"
    t.string   "product_code"
    t.integer  "status"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "sub_brand_id"
    t.integer  "variant_id"
  end

  add_index "products", ["sub_brand_id"], name: "index_products_on_sub_brand_id", using: :btree
  add_index "products", ["variant_id"], name: "index_products_on_variant_id", using: :btree

  create_table "profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.decimal  "savings",    default: 0.0
    t.string   "pincode"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree

  create_table "providers", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "session_token"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "providers", ["user_id"], name: "index_providers_on_user_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "role_name"
    t.integer  "permissions", default: [],              array: true
    t.integer  "parent_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "roles", ["parent_id"], name: "index_roles_on_parent_id", using: :btree

  create_table "seller_brand_packs", force: :cascade do |t|
    t.integer  "status"
    t.integer  "seller_id"
    t.integer  "brand_pack_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "seller_brand_packs", ["brand_pack_id"], name: "index_seller_brand_packs_on_brand_pack_id", using: :btree
  add_index "seller_brand_packs", ["seller_id"], name: "index_seller_brand_packs_on_seller_id", using: :btree

  create_table "seller_brand_packs_warehouse_brand_packs", id: false, force: :cascade do |t|
    t.integer "warehouse_brand_pack_id"
    t.integer "seller_brand_pack_id"
  end

  add_index "seller_brand_packs_warehouse_brand_packs", ["seller_brand_pack_id"], name: "idx_through_on_sbp_wbp", using: :btree
  add_index "seller_brand_packs_warehouse_brand_packs", ["warehouse_brand_pack_id"], name: "idx_through_on_wbp_sbp", using: :btree

  create_table "seller_pricings", force: :cascade do |t|
    t.decimal  "spat",                 default: 0.0
    t.decimal  "margin",               default: 0.0
    t.decimal  "service_tax",          default: 0.0
    t.decimal  "vat",                  default: 0.0
    t.decimal  "spat_per_unit",        default: 0.0
    t.integer  "seller_brand_pack_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "seller_pricings", ["seller_brand_pack_id"], name: "index_seller_pricings_on_seller_brand_pack_id", using: :btree

  create_table "sellers", force: :cascade do |t|
    t.string   "name"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "initials"
  end

  add_index "states", ["country_id"], name: "index_states_on_country_id", using: :btree

  create_table "sub_brands", force: :cascade do |t|
    t.string   "name"
    t.string   "logo_url"
    t.string   "initials"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "brand_id"
  end

  add_index "sub_brands", ["brand_id"], name: "index_sub_brands_on_brand_id", using: :btree

  create_table "time_slots", force: :cascade do |t|
    t.time     "from_time"
    t.time     "to_time"
    t.string   "label"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_roles", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_roles", ["role_id"], name: "index_user_roles_on_role_id", using: :btree
  add_index "user_roles", ["user_id"], name: "index_user_roles_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "phone_number"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "referral_code"
    t.string   "referred_by"
    t.integer  "referral_limit", default: 0
    t.integer  "workflow_state"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "benefit_limit",  default: 0
  end

  add_index "users", ["referral_code"], name: "index_users_on_referral_code", using: :btree

  create_table "variants", force: :cascade do |t|
    t.string   "name"
    t.string   "initials"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "warehouse_addresses", force: :cascade do |t|
    t.string   "warehouse_name"
    t.string   "address_line1"
    t.string   "address_line2"
    t.string   "landmark"
    t.integer  "warehouse_id"
    t.integer  "area_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "warehouse_addresses", ["area_id"], name: "index_warehouse_addresses_on_area_id", using: :btree
  add_index "warehouse_addresses", ["warehouse_id"], name: "index_warehouse_addresses_on_warehouse_id", using: :btree

  create_table "warehouse_brand_packs", force: :cascade do |t|
    t.integer  "status"
    t.integer  "warehouse_id"
    t.integer  "brand_pack_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "warehouse_brand_packs", ["brand_pack_id"], name: "index_warehouse_brand_packs_on_brand_pack_id", using: :btree
  add_index "warehouse_brand_packs", ["warehouse_id"], name: "index_warehouse_brand_packs_on_warehouse_id", using: :btree

  create_table "warehouse_orders", force: :cascade do |t|
    t.string   "warhouse_order_id"
    t.string   "ship_by"
    t.string   "status"
    t.integer  "order_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "warehouse_orders", ["order_id"], name: "index_warehouse_orders_on_order_id", using: :btree

  create_table "warehouse_pricings", force: :cascade do |t|
    t.decimal  "mrp",                     default: 0.0
    t.decimal  "selling_price",           default: 0.0
    t.decimal  "service_tax",             default: 0.0
    t.decimal  "vat",                     default: 0.0
    t.decimal  "cst",                     default: 0.0
    t.integer  "warehouse_brand_pack_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "warehouse_pricings", ["warehouse_brand_pack_id"], name: "index_warehouse_pricings_on_warehouse_brand_pack_id", using: :btree

  create_table "warehouse_stocks", force: :cascade do |t|
    t.integer  "quantity"
    t.integer  "date_of_purchase"
    t.integer  "status"
    t.integer  "warehouse_pricing_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "warehouse_stocks", ["warehouse_pricing_id"], name: "index_warehouse_stocks_on_warehouse_pricing_id", using: :btree

  create_table "warehouses", force: :cascade do |t|
    t.string   "name"
    t.integer  "status"
    t.integer  "cash_and_carry_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "warehouses", ["cash_and_carry_id"], name: "index_warehouses_on_cash_and_carry_id", using: :btree

  add_foreign_key "addresses", "areas"
  add_foreign_key "addresses", "users"
  add_foreign_key "areas", "cities"
  add_foreign_key "brand_packs", "products"
  add_foreign_key "brands", "companies"
  add_foreign_key "carts", "orders"
  add_foreign_key "carts", "users"
  add_foreign_key "categories", "departments"
  add_foreign_key "cities", "states"
  add_foreign_key "cluster_contexts", "clusters"
  add_foreign_key "devices", "users"
  add_foreign_key "distributors", "companies"
  add_foreign_key "emails", "users"
  add_foreign_key "employees", "users"
  add_foreign_key "generics", "providers"
  add_foreign_key "inventories", "distributors"
  add_foreign_key "inventory_addresses", "areas"
  add_foreign_key "inventory_addresses", "inventories"
  add_foreign_key "inventory_brand_packs", "brand_packs"
  add_foreign_key "inventory_brand_packs", "inventories"
  add_foreign_key "inventory_order_payments", "inventory_orders"
  add_foreign_key "inventory_order_products", "inventory_brand_packs"
  add_foreign_key "inventory_order_products", "inventory_orders"
  add_foreign_key "inventory_orders", "inventories"
  add_foreign_key "inventory_orders", "users"
  add_foreign_key "inventory_orders", "warehouses"
  add_foreign_key "inventory_pricings", "inventory_brand_packs"
  add_foreign_key "marketplace_brand_packs", "brand_packs"
  add_foreign_key "membership_payments", "memberships"
  add_foreign_key "memberships", "plans"
  add_foreign_key "messages", "providers"
  add_foreign_key "mpsp_categories", "mpsp_departments"
  add_foreign_key "order_products", "carts"
  add_foreign_key "order_products", "marketplace_selling_packs"
  add_foreign_key "orders", "addresses"
  add_foreign_key "orders", "users"
  add_foreign_key "payments", "orders"
  add_foreign_key "products", "sub_brands"
  add_foreign_key "products", "variants"
  add_foreign_key "profiles", "users"
  add_foreign_key "providers", "users"
  add_foreign_key "seller_brand_packs", "brand_packs"
  add_foreign_key "seller_brand_packs", "sellers"
  add_foreign_key "seller_pricings", "seller_brand_packs"
  add_foreign_key "states", "countries"
  add_foreign_key "sub_brands", "brands"
  add_foreign_key "warehouse_addresses", "areas"
  add_foreign_key "warehouse_addresses", "warehouses"
  add_foreign_key "warehouse_brand_packs", "brand_packs"
  add_foreign_key "warehouse_brand_packs", "warehouses"
  add_foreign_key "warehouse_orders", "orders"
  add_foreign_key "warehouse_pricings", "warehouse_brand_packs"
  add_foreign_key "warehouse_stocks", "warehouse_pricings"
  add_foreign_key "warehouses", "cash_and_carries"
end

module AddressModule
  module V1
    require 'rails_helper'
    RSpec.describe AddressModule::V1::AreaService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:area_service) { AddressModule::V1::AreaService.new }

      let(:city_service) { AddressModule::V1::CityService }

      let(:area_dao) { AddressModule::V1::AreaDao }

      let(:pincode) { Faker::Number.number(6) }

      let(:city) { FactoryGirl.build_stubbed(:city) }
      let(:area) { FactoryGirl.build_stubbed(:area) }

      let(:country_name) { Faker::Address.country }
      let(:state_name) { Faker::Address.state }

      let(:pincode) { area_reader.get_pincode }

      let(:event) { AddressModule::V1::ModelStates::V1::AreaEvents::ACTIVATE }
      let(:area) { FactoryGirl.build_stubbed(:area) }
      let(:area_reader) { AddressModule::V1::AreaReader.new(area) }
      let(:pincode_index_service) { SearchModule::V1::PincodeIndexService }

      let(:change_status_request) {
        {
          event: event,
          pincode: pincode
        }
      }

      let(:request) {
        {
          pincode: Faker::Number.number(6),
          area: Faker::Address.street_name,
          country: Faker::Address.country,
          state: Faker::Address.state,
          city: Faker::Address.city,
          is_active: true
        }
      }

      context 'load area from pincode' do
        it 'with valid pincode' do
          area_dao.any_instance.stub(:get_area_by_pincode).and_return(area)
          response = area_service.get_area_from_pincode(pincode)

          expect(response.get_pincode).to eq(area.pincode)
        end

        it 'with invalid pincode' do
          expect_any_instance_of(area_dao).to receive(:get_area_by_pincode).
            with(pincode).and_raise(custom_error_util::RecordNotFoundError.new)
          expect { area_service.get_area_from_pincode(pincode) }.
            to raise_error(custom_error_util::RecordNotFoundError)
        end
      end

      context 'create area' do
        it 'with existing area' do
          expect_any_instance_of(area_dao).to receive(:get_area_by_pincode).with(request[:pincode]).and_return(area)

          expect { area_service.create_area(request) }.to raise_error(custom_error_util::RecordAlreadyExistsError)
        end

        it 'with invalid city arguments' do
          expect_any_instance_of(city_service).to receive(:create_city).
            with(request[:city],request[:state], request[:country]).
            and_raise(custom_error_util::RecordNotFoundError.new)
          expect { area_service.create_area(request) }.to raise_error(custom_error_util::RecordNotFoundError)
        end

        it 'with invalid pincode' do
          expect_any_instance_of(area_dao).to receive(:get_area_by_pincode).
            with(request[:pincode]).and_raise(custom_error_util::RecordNotFoundError.new)
          expect_any_instance_of(city_service).to receive(:create_city).
            with(request[:city],request[:state], request[:country]).and_return(city)
          area_dao.any_instance.stub(:create).and_raise(custom_error_util::InvalidArgumentsError.new)
          expect { area_service.create_area(request) }.to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with valid params' do
          expect_any_instance_of(area_dao).to receive(:get_area_by_pincode).
            with(request[:pincode]).and_raise(custom_error_util::RecordNotFoundError.new)
          expect_any_instance_of(city_service).to receive(:create_city).
            with(request[:city],request[:state], request[:country]).and_return(city)
          expect_any_instance_of(pincode_index_service).to receive(:add_document).and_return(true)
          area_dao.any_instance.stub(:create).and_return(area)
          response = area_service.create_area(request)
          expect(response.get_pincode).to eq(area.pincode)
        end
      end

      context 'change area status' do

        it 'with not existing area' do
          expect { area_service.get_area_from_pincode(pincode) }
            .to raise_error(custom_error_util::RecordNotFoundError)
          expect { area_service.change_area_status(change_status_request) }
            .to raise_error(custom_error_util::RecordNotFoundError)
        end

        it 'with invalid event with correct area' do
          change_status_request[:event] = "100"
          expect_any_instance_of(area_dao).to receive(:get_area_by_pincode).with(pincode).and_return(area)
          expect_any_instance_of(area_dao).to receive(:change_state).
            with(area, change_status_request[:event]).and_raise(custom_error_util::InvalidArgumentsError.new)
          expect { area_service.change_area_status(change_status_request) }
            .to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'transition event to the same state' do
          expect_any_instance_of(area_dao).to receive(:get_area_by_pincode).with(pincode).and_return(area)
          expect_any_instance_of(area_dao).to receive(:change_state)
            .with(area, event).and_raise(custom_error_util::PreConditionRequiredError.new)
          expect { area_service.change_area_status(change_status_request) }
            .to raise_error(custom_error_util::PreConditionRequiredError)

        end

        it 'with valid area and valid event' do
          expect_any_instance_of(area_dao).to receive(:get_area_by_pincode).with(pincode).and_return(area)
          expect_any_instance_of(area_dao).to receive(:change_state)
            .with(area, event).and_return(area)
          expect_any_instance_of(pincode_index_service).to receive(:update_area_status).and_return(true)
          data = area_service.change_area_status(change_status_request)
          expect(data).to eq(area)
        end
      end

      context 'update area' do
        it 'with wrong id' do
          expect_any_instance_of(area_dao).to receive(:get_area_by_id)
            .with(area.id).and_raise(custom_error_util::RecordNotFoundError)
          expect{ area_service.update_area(
            {name: area.name, id: area.id}) }.to raise_error(
            custom_error_util::RecordNotFoundError)
        end

        it 'wrong update params' do
          expect(area_service).to receive(:get_area_by_id).with(area.id).and_return(area)
          expect_any_instance_of(area_dao).to receive(:update)
            .with({name: area.name}, area).and_raise(custom_error_util::InvalidArgumentsError.new)
          expect{ area_service.update_area(
            {name: area.name, id: area.id}) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end
      end

    end
  end
end

module AddressModule
  module V1
    require 'rails_helper'
    RSpec.describe AddressModule::V1::StateService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:state_service) { AddressModule::V1::StateService.new }

      let(:country_dao) { AddressModule::V1::CountryDao }
      let(:state_dao) { AddressModule::V1::StateDao }

      let(:country) { FactoryGirl.build_stubbed(:country) }
      let(:state) { FactoryGirl.build_stubbed(:state) }

      let(:country_name) { Faker::Address.country }
      let(:state_name) { Faker::Address.state }

      context 'create state' do
        it 'with pre-existing country and state' do
          expect_any_instance_of(country_dao).to receive(:get_country_by_name).and_return(country)
          expect_any_instance_of(state_dao).to receive(:get_state_by_name_and_country_id).and_return(state)

          response = state_service.create_state(state_name, country_name)

          expect(response).to equal(state)
        end

        it 'with pre-existing state ' do
          expect_any_instance_of(country_dao).to receive(:get_country_by_name).and_return(nil)
          expect_any_instance_of(country_dao).to receive(:create).and_return(country)
          expect_any_instance_of(state_dao).to receive(:get_state_by_name_and_country_id).and_return(state)

          response = state_service.create_state(state_name, country_name)

          expect(response).to equal(state)
        end

        it 'with pre-existing country ' do
          expect_any_instance_of(country_dao).to receive(:create).and_return(country)
          expect_any_instance_of(state_dao).to receive(:get_state_by_name_and_country_id).and_return(nil)
          expect_any_instance_of(state_dao).to receive(:create).and_return(state)

          response = state_service.create_state(state_name, country_name)

          expect(response).to equal(state)
        end

        it 'with nil country name ' do
          expect_any_instance_of(country_dao).to receive(:get_country_by_name).and_return(nil)
          expect_any_instance_of(country_dao).to receive(:create).
            and_raise(custom_error_util::InvalidArgumentsError.new)

          expect{state_service.create_state(state_name, country_name)}.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with nil state ' do
          expect_any_instance_of(country_dao).to receive(:get_country_by_name).and_return(nil)
          expect_any_instance_of(country_dao).to receive(:create).and_return(country)

          expect_any_instance_of(state_dao).to receive(:get_state_by_name_and_country_id).and_return(nil)
          expect_any_instance_of(state_dao).to receive(:create).and_raise(custom_error_util::InvalidArgumentsError.new)

          expect{state_service.create_state(state_name, country_name)}.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end

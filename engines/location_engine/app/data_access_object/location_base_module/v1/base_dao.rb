#
# Module to handle all the functionalities related to Dao
#
module LocationBaseModule
  #
  # Version1 for base module
  #
  module V1
    #
    # Base module to inject generic utils
    #
    class BaseDao

      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize
        @custom_error_util = CommonModule::V1::CustomErrors
        @content = CommonModule::V1::Content
      end
    end
  end
end

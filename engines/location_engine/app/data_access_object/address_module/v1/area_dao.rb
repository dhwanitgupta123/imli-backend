#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Data Access Object to interact with AreaModel
    #
    class AreaDao < LocationBaseModule::V1::BaseDao
      
      AREA_MODEL_EVENTS = AddressModule::V1::ModelStates::V1::AreaEvents

      def initialize
        super
        @area_model = AddressModule::V1::Area
      end

      #
      # Create the area
      #
      # @param args [hash] args contain area_name, pincode
      #  city and status(optional)
      #
      # @return [AreaModel] new model
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        new_area = @area_model.new(args)
        begin
          new_area.save!
          return new_area
        rescue ActiveRecord::RecordInvalid => e
          raise @custom_error_util::InvalidArgumentsError.new(e.message)
        end
      end

      #
      # Update the given city with args
      #
      # @param args [hash] contain fields and their values
      # @param area [Model] area model
      #
      # @return [area] updated area
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, area)
        raise @custom_error_util::InvalidArgumentsError.new(@content::AREA_MISSING) if area.nil? || args.nil?

        begin
          area.update_attributes!(args)
          return area
        rescue ActiveRecord::RecordInvalid => e
          raise @custom_error_util::InvalidArgumentsError.new(e.message)
        end
      end

      #
      # Load the area by pincode
      #
      # @param pincode [String] unique id for area model
      #
      # @return [AreaModel] area model
      #
      def get_area_by_pincode(pincode)
        area = @area_model.find_by(pincode: pincode)
        raise @custom_error_util::RecordNotFoundError.new(@content::NON_EXISTENT_AREA) if area.nil?
        return area
      end

      #
      # Load the area by area_id
      #
      # @param area_id [String] unique area_id for area model in db
      #
      # @return [AreaModel] area model
      #
      def get_area_by_id(area_id)
        area = @area_model.find_by(id: area_id)
        raise @custom_error_util::RecordNotFoundError.
          new(@content::NON_EXISTENT_AREA) if area.nil?
        return area
      end
      
      # 
      # returns all area records
      # 
      # @return [Array] array of area model
      # 
      def get_all_area
        @area_model.all
      end

      #
      # This function change the state of the model
      # 
      # @param model [Area Model] Any active record model
      # @param event [Integer] event to trigger 
      #
      # @error [InvalidArgumentsError] if it fails to change state of the model
      # 
      # @error [PreConditionRequiredError] if the event is not defined in Event States
      #
      def change_state(area, event)
        begin
          case event.to_i
            when AREA_MODEL_EVENTS::ACTIVATE
              area.activate!
            when AREA_MODEL_EVENTS::DEACTIVATE
              area.deactivate!
            else
              raise @custom_error_util::InvalidArgumentsError.new(@content::EVENT_MISSING)
          end
        rescue Workflow::NoTransitionAllowed => e
          raise @custom_error_util::PreConditionRequiredError.new(@content::INVALID_EVENT)
        end
        return area
      end
    end
  end
end

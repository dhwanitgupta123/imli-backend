#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Data Access Object to interact with CountryModel
    #
    class CountryDao < LocationBaseModule::V1::BaseDao
      def initialize
        super
        @country_model = AddressModule::V1::Country
      end

      #
      # Create the country
      #
      # @param args [name] country name
      #
      # @return [CountryModel]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(name)
        new_country = @country_model.new(name: name)

        begin
          new_country.save!
          return new_country
        rescue ActiveRecord::RecordInvalid
          raise @custom_error_util::InvalidArgumentsError.new('Already exists country')
        end
      end

      #
      # Update the given country with name
      #
      # @param name [String] new country name
      # @param country [Model] CountryModel
      #
      # @return [CountryModel] updated country
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(name, country)
        begin
          country.update_attributes!(name: name)
          return country
        rescue ActiveRecord::RecordInvalid => e
          raise @custom_error_util::InvalidArgumentsError.new('Not able to update country')
        end
      end

      #
      # Find the country by name
      #
      # @param name [String] country name
      #
      # @return [CountryModel] return country model else nil
      #
      def get_country_by_name(name)
        @country_model.find_by(name: name)
      end
    end
  end
end

#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Data Access Object to interact with AddressModel
    #
    class AddressDao < LocationBaseModule::V1::BaseDao

      ADDRESS_EVENTS = AddressModule::V1::ModelStates::V1::AddressEvents
      ADDRESS_STATES = AddressModule::V1::ModelStates::V1::AddressStates

      def initialize
        super
        @address_model = AddressModule::V1::Address
      end

      #
      # Create the address
      #
      # @param args [hash] args to create address, args contain
      #  address_line1, address_line2(optional), area, nickname
      #
      # @return [AddressModel] new model
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        new_address = @address_model.new(args)
        begin
          new_address.save!
          return new_address
        rescue ActiveRecord::RecordInvalid => e
          raise @custom_error_util::InvalidArgumentsError.new(e.message)
        end
      end

      #
      # Update the given address with args
      #
      # @param args [hash] args contain keys which need to be update like
      #   address_line1, address_line2(optional), area, nickname
      # @param area [AddressModel] address model
      #
      # @return [AddressModel] updated address
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, address)
        raise @custom_error_util::InvalidArgumentsError.new('address cannot be nil') if address.nil? || args.nil?

        begin
          address.update_attributes!(args)
          return address
        rescue ActiveRecord::RecordInvalid => e
          raise @custom_error_util::InvalidArgumentsError.new(e.message)
        end
      end

      # 
      # Adds address for user
      # 
      # @param address_params [hash] contains user [object], area [object], address [hash]
      # 
      # @return [AddressModel] added address
      #
      # @error [InsufficientDataError] if invalid params passed
      # 
      def add_address_for_user(address_params)
        validate_address_params(address_params[:address])
        new_address = @address_model.new(address_params[:address])
        new_address.area = address_params[:area]
        new_address.user = address_params[:user]
        new_address.save!
        return new_address
      end

      # 
      # Updates an existing address for a user
      # 
      # @param address_params [hash] contains user [object], area [object], address [hash]
      # 
      # @return [AddressModel] added address
      # 
      def update_address_for_user(address_params)
        present_address = address_params[:present_address]
        initialize_address_params(address_params[:new_address])
        initialize_area_params(address_params[:area])
        present_address.recipient_name = @recipient_name if @recipient_name.present?
        present_address.nickname = @nickname if @nickname.present?
        present_address.address_line1 = @address_line1 if @address_line1.present?
        present_address.address_line2 = @address_line2 if @address_line2.present?
        present_address.landmark = @landmark if @landmark.present?
        present_address.area = @area if @area.present?
        present_address.save!
        return present_address
      end

      # 
      # Gets address from address_id
      # 
      # @param address_id [String]
      # 
      # @return [AddressModel] the address found
      # 
      def get_address_from_id(address_id)
        address = @address_model.find_by_id(address_id)
        raise @custom_error_util::RecordNotFoundError.new('Address with address_id ' + address_id + ' not exists') if address.nil?
        return address
      end

      def get_address_for_user(address_id, user)
        user.addresses.where(id: address_id).first
      end

      def get_active_addresses_of_user(user)
        user.addresses.where(status: ADDRESS_STATES::ACTIVE)
      end

      #
      # Trigger PAYMENT change_state as per the event passed
      #
      def change_state(time_slot, event)
        case event.to_i
        when ADDRESS_EVENTS::ACTIVATE
          time_slot.trigger_event('activate')
        when ADDRESS_EVENTS::DEACTIVATE
          time_slot.trigger_event('deactivate')
        when ADDRESS_EVENTS::SOFT_DELETE
          time_slot.trigger_event('soft_delete')
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
        end
        return time_slot
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      # 
      # Initialize address parameters
      # 
      # @param address_params [hash] address params needed for Address model
      # 
      def initialize_address_params(address_params)
        @nickname = address_params[:nickname] if address_params[:nickname].present?
        @address_line1 = address_params[:address_line1] if address_params[:address_line1].present?
        @address_line2 = address_params[:address_line2] if address_params[:address_line2].present?
        @landmark = address_params[:landmark] if address_params[:landmark].present?
        @recipient_name = address_params[:recipient_name] if address_params[:recipient_name].present?
      end

      def initialize_area_params(area_reader)
        @area = area_reader.get_area_model if area_reader.present?
      end

      # 
      # Validates address params
      # 
      # @param address_params [hash] address params needed for Address model
      # 
      # @error [InsufficientDataError] if required params are not present
      # 
      def validate_address_params(address_params)
        error_message = ""

        # When user does not enter address line1 with flat and building number
        error_message << " Add Flat, Building name!" if address_params[:address_line1].blank?
        # When user does not enter address line2 with locality name
        error_message << " Add Locality!" if address_params[:address_line2].blank?
        raise @custom_error_util::InsufficientDataError.new(error_message) if address_params[:address_line1].blank? or address_params[:address_line2].blank?
      end

    end
  end
end

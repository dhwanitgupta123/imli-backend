#
# Module to handle all the functionalities related to address
# 
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Module to keep information of area state
    #
    module ModelStates
      module V1
        #
        # AddressStates has enums corresponding to the possible states of area model
        #
        class AddressStates
          ACTIVE = 1
          INACTIVE = 2
          DELETED = 3
        end
      end
    end
  end
end

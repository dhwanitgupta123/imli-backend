#
# Module to handle all the functionalities related to address
# 
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Model for cities table
    #
    class City < ActiveRecord::Base
      has_many :areas, dependent: :destroy

      belongs_to :state

      validates :name, presence: true

      validates :state_id, presence: true
    end
  end
end

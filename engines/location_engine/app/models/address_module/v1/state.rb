#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    # Model for states table
    class State < ActiveRecord::Base
      has_many :cities, dependent: :destroy

      belongs_to :country

      validates :name, presence: true

      validates :country_id, presence: true
    end
  end
end

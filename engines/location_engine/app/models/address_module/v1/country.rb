#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Model for countries table
    #
    class Country < ActiveRecord::Base
      has_many :states, dependent: :destroy

      validates :name, presence: true, uniqueness: { case_sensitive: false }
    end
  end
end

#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Model for areas table
    #
    class Area < ActiveRecord::Base
      AREA_MODEL_STATES = AddressModule::V1::ModelStates::V1::AreaStates

      VALID_PINCODE_REGEX = CommonModule::V1::RegexUtil::PINCODE_REGEX

      validates :pincode, presence: true, uniqueness: true,
                          format: { with: VALID_PINCODE_REGEX }, length: { is: 6 }

      validates :name, presence: true

      belongs_to :city

      has_many :addresses

      validates :city_id, presence: true

      #
      # Workflow to define states of the Area
      #
      # Initial State => Inactive
      #
      # State Diagram::
      #   Inactive --activate--> Active
      #
      # * activate is the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, AREA_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
        end

        state :active, AREA_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
        end
      end

      #
      # Set area state to active by triggering
      # required event
      #
      def set_state_to_active
        self.activate! if self.inactive?
      end

      #
      # Set area state to deleted by triggering
      # required event
      #
      def set_state_to_deactive
        self.deactivate! if self.active?
      end

      #
      # Get current state of area
      #
      # @return [String] Current state
      def get_current_state
        self.current_state.name
      end
    end
  end
end

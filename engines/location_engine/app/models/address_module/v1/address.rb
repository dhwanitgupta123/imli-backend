#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Model for addresses table
    #
    class Address < ActiveRecord::Base

      ADDRESS_STATES = AddressModule::V1::ModelStates::V1::AddressStates
      CONTENT = CommonModule::V1::Content
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      has_many :orders, class_name: '::MarketplaceOrdersModule::V1::Order'

      belongs_to :area

      belongs_to :user, class_name: '::UsersModule::V1::User'

      validates :address_line1, presence: true

      validates :area_id, presence: true

      validates :user_id, presence: true

      #
      # Workflow to define states of the Address
      #
      include Workflow
      workflow_column :status
      workflow do
        state :active, ADDRESS_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :inactive, ADDRESS_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, ADDRESS_STATES::DELETED
      end

      #
      # Trigger time slot state change
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end

    end # End of class
  end
end
#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Logic layer for address specific queries
    #
    class AddressService < BaseModule::V1::BaseService

      ADDRESS_EVENTS = AddressModule::V1::ModelStates::V1::AddressEvents

      def initialize(params = '')
        super
        @address_dao = AddressModule::V1::AddressDao
        @area_service = AddressModule::V1::AreaService
      end


      # 
      # Add address of the user
      # @param args [Hash] Hash of parameters required for address creation
      # Must Params:
      #  * address_line1 : flat and building name of user
      #  * address_line2 : locality name of user
      #  * area_id : Area id of the user location
      #  * user : user to which this address should belong to
      # 
      # @return [Address object] if successfully created
      # @raise [@custom_error_util::RecordNotFoundError] if the are is not found
      def add_address_for_user(args)
      	address_dao_class = @address_dao.new
        area_service = @area_service.new

      	begin
          area = area_service.get_area_by_id(args[:address][:area_id])
          address = address_dao_class.add_address_for_user({
            user: args[:user],
            area: area,
            address: args[:address]
          })
        rescue @custom_error_util::RecordNotFoundError => e
          raise e
        end
        return address
      end

      # 
      # Update address of the user
      # @param args [Hash] Hash of parameters required for address updation
      # Must Params:
      #  * area_id : Area id of the user location
      #  * address_id : Address id of the address of user being updated
      # 
      # @return [Address object] if successfully updated
      # @raise [@custom_error_util::RecordNotFoundError] if the area is not found
      def update_address_for_user(args)
        address_dao_class = @address_dao.new
        area_service = @area_service.new
        
        begin
          area = area_service.get_area_by_id(args[:address][:area_id]) if args[:address][:area_id].present?
          @present_address = address_dao_class.get_address_from_id(args[:address][:id])
          @user = args[:user]
          validate_address_owner

          address = address_dao_class.update_address_for_user({
            user: @user,
            area: area || '',
            new_address: args[:address], 
            present_address: @present_address
          })
        rescue @custom_error_util::RecordNotFoundError => e
          raise e
        end
        return address    
      end

      #
      # Create new address and persist old address (move it to SOFT DELETE state)
      #
      # @param args [Hash] Hash of parameters required for address updation
      # Params:
      #  * area_id/pincode : Area id or pincode of the user location
      #  * address_id : Address id of the address of user being updated
      #  * All other address attributes: nickname, recepient_name, address_line1, address_line2, etc
      # 
      # @return [Address object] if successfully updated
      # @raise [@custom_error_util::RecordNotFoundError] if the area is not found
      def create_new_and_persist_old_address_for_user(args)
        address_dao_class = @address_dao.new
        area_service = @area_service.new

        begin
          area = nil
          if args[:address][:area_id].present?
            area = area_service.get_area_by_id(args[:address][:area_id])
          elsif args[:address][:pincode].present?
            area = area_service.get_area_from_pincode(args[:address][:pincode])
          end
          @present_address = address_dao_class.get_address_from_id(args[:address][:id])
          @user = args[:user]
          validate_address_owner
          # Duplicate the already existing address and update its attributes
          duplicate_address = @present_address.dup

          address = address_dao_class.update_address_for_user({
            user: @user,
            area: area || '',
            new_address: args[:address], 
            present_address: duplicate_address
          })
          # Move old address to DELETED state
          address_dao_class.change_state(@present_address, ADDRESS_EVENTS::SOFT_DELETE)

        rescue => e
          # For now, raising the same issue
          raise e
        end
        return address
      end

      def get_active_addresses_of_user(user)
        address_dao = @address_dao.new
        return address_dao.get_active_addresses_of_user(user) 
      end

      def validate_address_owner
        unless @present_address.user == (@user)
          raise @custom_error_util::UnAuthorizedUserError.new
        end
      end

     end
  end
end
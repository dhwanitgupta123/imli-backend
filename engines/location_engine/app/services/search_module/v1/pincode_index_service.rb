#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # Logic layer which is responsible to create pincode index
    # and retrieve results
    #
    class PincodeIndexService < BaseModule::V1::BaseService
      QUERY_HELPER = SearchModule::V1::QueryHelper
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      AREA_MODEL_STATES = AddressModule::V1::ModelStates::V1::AreaStates

      def initialize
        @area_dao = AddressModule::V1::AreaDao
        @area_reader = AddressModule::V1::AreaReader
        @pincode_model = SearchModule::V1::Pincode
        @pincode_model_reader = AddressModule::V1::PincodeModelReader
        initialize_pincode_repository
      end

      #
      # This function checks if pincode-index already present or not
      # if not then retrieve data from Area table and index the data
      #
      # but if force == 'true' then it will always re-index the data
      #
      # @param force [String] to specify force re-indexing
      #
      def initialize_index(force = 'false')
        return false if @pincode_repository.nil? || !repository_exists?
        return false if (force != 'true') && index_exists?
        
        delete_previous_index

        area_dao_class = @area_dao.new

        all_area = area_dao_class.get_all_area

        all_area.each do |area|
          pincode = get_pincode_from_area(area)
          save(pincode)
        end
      end

      #
      # save the document in repositroy
      #
      def save(pincode)
        @pincode_repository.save(pincode)
      end

      #
      # This function breaks the query by white space convert it into tokens
      # and corresponding to each token it creates
      # 'fuzzy query'
      # 'regexp query'
      # and concat them in boolean query through OR ('should') operator
      #
      # Example : query = '4000 pow'
      # 1. tokens = ['4000', 'pow']
      # 2. let regexp_query for token1 = regexp1 and for token2 = regexp2
      # 3. let fuzzy_query for token1 = fuzzy1 and for token2 = fuzzy2
      # 4. then boolean_query = {"should": [regexp1, regexp2, fuzzy1, fuzzy2]}
      # where
      #   => regexp1 = { regexp => { '4000.*', 2 } }, here 2 is to boost query which means
      #                                               we are giving 2 times weight to this field 
      #   => regexp2 = { regexp => {'pow.*', 2 } } same as above
      #   => fuzzy1 = {fuzzy => { '4000', 1 } } here first param is value and second is fuzzyness
      #                                        fuzzyness specify edit distance parameter, which means
      #                                        if fuzzyness = 1 then query which can be converted to
      #                                        indexed string using one operation then it will return true
      #   => fuzzy2 = { fuzzy => {'pow', 1 } } same as above
      #   => boolean_query => {boolean => { 'should': [regexp1, regexp2, fuzzy1, fuzzy2] } }
      #                       here should specify OR operation
      #
      # @param query [String] query string
      # @param n = 10 [Integer] top n results
      # @param status [Integer] status of areas to be fetched
      #
      # @return [Array] Array of pincode object
      #
      def get_results(query, n = 10, status)
        raise CUSTOM_ERRORS_UTIL::RunTimeError.new('Repository not exists')  if @pincode_repository.nil? ||
                                                                            !repository_exists? || !index_exists?

        query = query.downcase
        boolean_query = QUERY_HELPER.get_boolean_query

        tokens = query.split(/[\/, ]/)
        
        tokens.each do |token|
          regexp_query = QUERY_HELPER.get_regexp_query('text', token + '.*', 2)
          fuzzy_query = QUERY_HELPER.get_fuzzy_query('text', token, 1)
          boolean_query = QUERY_HELPER.add_clause_in_boolean_query('should', regexp_query, boolean_query)
          boolean_query = QUERY_HELPER.add_clause_in_boolean_query('should', fuzzy_query, boolean_query)
        end
        
        if status != AREA_MODEL_STATES::ALL
          match_status_query = QUERY_HELPER.get_match_query('status', status)
          boolean_query = QUERY_HELPER.add_clause_in_boolean_query('must', match_status_query, boolean_query)
        end
        results = search(boolean_query, n)
        @pincode_model_reader.new(results)
      end

      #
      # Return all the active areas
      # 
      def get_active_areas(n=10)
        raise CUSTOM_ERRORS_UTIL::RunTimeError.new('Repository not exists')  if @pincode_repository.nil? ||
                                                                            !repository_exists? || !index_exists?
      
        match_status_query = QUERY_HELPER.get_match_query('status', AREA_MODEL_STATES::ACTIVE)
        results = search(match_status_query, n)
        @pincode_model_reader.new(results)
      end

      #
      # Search query in repository
      #
      def search(query, n)
        @pincode_repository.search(query: query, size: n).to_a
      end

      #
      # This will delete the index if it exists
      #
      def delete_index
        return true unless @pincode_repository.client.indices.exists(index: @pincode_repository.index)
        @pincode_repository.client.indices.delete(index: @pincode_repository.index)
      end

      #
      # update area status corresponding to the area id in repository
      #
      # @param area_id [Integer] area id to update
      # @param [Integer] status of area to update
      #
      # @error [RunTimeError] If it is not able to find the document with area_id
      #
      def update_area_status(area_id, status)
        begin
          @pincode_repository.update({id: area_id, status: status})
        rescue => e
          raise CUSTOM_ERRORS_UTIL::RunTimeError.new(CONTENT_UTIL::MISSING_DOCUMENT)
        end
      end
      
      #
      # This function add the area in pincode_index
      #
      # @param area [Model] model object of area
      #
      def add_document(area)

        return unless get_document_by_id(area.id) == false

        pincode = get_pincode_from_area(area)
        begin
          @pincode_repository.save(pincode)
        rescue => e
          raise CUSTOM_ERRORS_UTIL::RunTimeError.new(CONTENT_UTIL::UNABLE_TO_ADD_DOCUMENT)
        end
      end

      #
      # This function update the index with given area
      #
      # @param area [Model] updated model of area
      #
      # @error [RunTimeError] if document with given not found
      #
      def update_document(area)
        pincode = get_pincode_from_area(area)
        begin
          @pincode_repository.update(pincode)
        rescue => e
          raise CUSTOM_ERRORS_UTIL::RunTimeError.new(CONTENT_UTIL::MISSING_DOCUMENT)
        end
      end

      #
      # Return document by given id
      #
      # @param id [Integer] id of area
      # 
      # @return [Document] this return document if it succeed to find
      # @return [False] if document not found in the repository
      #
      def get_document_by_id(id)
        begin
          @pincode_repository.find(id)
        rescue Elasticsearch::Persistence::Repository::DocumentNotFound => e
          return false
        end
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # initializing pincode repository
      #
      def initialize_pincode_repository
        @pincode_repository = PINCODE_INDEX_REPOSITORY
      end

      #
      # This function checks if pincode repository connected successfully to elastic search server
      # it returns true if connected successfully else false
      #
      # @return [Boolean] true if exists else false
      #
      def repository_exists?
        begin
          @pincode_repository.client.cluster.health
        rescue Faraday::ConnectionFailed
          return false
        end
        true
      end

      #
      # This function check if index with index-name specified in pincode repository exists in
      # elastic search server of not
      #
      # @return [Boolean] true if index exists else false
      #
      def index_exists?
        begin
          return false unless @pincode_repository.client.indices.exists(index: @pincode_repository.index)
          return true if @pincode_repository.count > 0
        rescue Faraday::ConnectionFailed
          return false
        end
        return false
      end

      #
      # map area to pincode model object
      #
      # @param area [Object] Area object
      #
      # @return [AreaReader] area_reader object
      #
      def get_pincode_from_area(area)
        area_reader = @area_reader.new(area)
        attributes = {}
        attributes[:id] = area_reader.get_area_id
        attributes[:text] = area_reader.get_pincode + ' ' + area_reader.get_area
        attributes[:pincode] = area_reader.get_pincode
        attributes[:area] = area_reader.get_area
        attributes[:city] = area_reader.get_city
        attributes[:state] = area_reader.get_state
        attributes[:status] = area_reader.get_status
        @pincode_model.new(attributes)
      end

      #
      # This function re-initialize index, delete the existing one
      #
      def delete_previous_index
        @pincode_repository.create_index! force: true
      end
    end
  end
end

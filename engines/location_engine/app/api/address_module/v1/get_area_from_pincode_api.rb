#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # AddArea api, it validates the request, call the
    # area service and return the JsonResponse
    #
    class GetAreaFromPincodeApi < LocationBaseModule::V1::BaseApi
      def initialize(params)
        super
        @params = params
        @address_helper = AddressModule::V1::AddressHelper
        @area_service = AddressModule::V1::AreaService
      end

      #
      # Function returns the error or success get area from pincode response
      #
      # @param request [hash] hash contain pincode string only
      #
      # @return [JsonResponse]
      #
      def enact(request)
        area_service_class = @area_service.new

        begin
          validate_request(request)
          pincode = request[:pincode]
          area_reader = area_service_class.get_area_from_pincode(pincode)
          return @address_response_decorator.create_ok_response(area_reader)
        rescue @custom_error_util::RecordNotFoundError, @custom_error_util::InvalidArgumentsError => e
          return @address_response_decorator.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # validate the request params
      #
      # @param request [hash] request
      #
      # @error [InvalidArgumentsError] raise error if request is not valid
      #
      def validate_request(request)
        if request.blank?
          raise @custom_error_util::InvalidArgumentsError.new(@content::INSUFFICIENT_DATA)
        end
        if request[:pincode].blank?
          raise @custom_error_util::InvalidArgumentsError.new(@content::MISSING_PINCODE_PARAM)
        end
        if !@address_helper.valid_pincode?(request[:pincode])
          raise @custom_error_util::InvalidArgumentsError.new(@content::INVALID_PINCODE)
        end
      end
    end
  end
end

#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for address module
  #
  module V1

    CONTENT_UTIL = CommonModule::V1::Content
    
    #
    # SearchAreaByQuery api, it validates the request, call the
    # pincode_index_service and return the JsonResponse
    # 
    # We are using pincode-index that is saved in elastic search repository
    # our indexed field contain "pincode area_name"
    # this API breaks the query in tokens by whitespace and return the top_n 
    # matching results which is specified by user or it take default value from config
    #
    class SearchAreaByQueryApi < LocationBaseModule::V1::BaseApi
      CACHE_UTIL = CommonModule::V1::Cache
      AREA_MODEL_STATES = AddressModule::V1::ModelStates::V1::AreaStates
      def initialize(params)
        super
        @params = params
        @pincode_index_service = SearchModule::V1::PincodeIndexService
      end

      #
      # Function takes input corresponding to create the area and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have query
      # and top_n (optional)
      #
      # @return [JsonResponse]
      #
      def enact(request)
        begin
          pincode_index_service = @pincode_index_service.new
          top_n = request[:top_n] || CACHE_UTIL.read('DEFAULT_N_RESULTS')
          @status = request[:status] || AREA_MODEL_STATES::ACTIVE
          
          if request.present? && request[:query].blank?
            pincode_model_reader =  pincode_index_service.get_active_areas
          else
            validate_request(request)
            pincode_model_reader = pincode_index_service.get_results(request[:query], top_n.to_i, @status.to_i)
          end
          @address_response_decorator.create_search_area_by_query_response(pincode_model_reader)
        rescue @custom_error_util::InvalidArgumentsError => e
          return @address_response_decorator.create_response_invalid_arguments_error(e.message)
        rescue @custom_error_util::RunTimeError
          return @address_response_decorator.create_response_runtime_error
        end
      end

      def validate_request(request)
        if !AREA_MODEL_STATES::ALLOWED_STATES.include?(@status.to_i)
          raise @custom_error_util::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_STATE)
        end
        raise @custom_error_util::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_PINCODE_QUERY) if request.nil? || request[:query].blank? || request[:query].length < 3
      end
    end
  end
end

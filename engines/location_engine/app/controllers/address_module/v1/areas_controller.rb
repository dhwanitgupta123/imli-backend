#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Area controller service class to route panel API calls to the Area Apis
    #
    class AreasController < BaseModule::V1::ApplicationController

      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper

      before_action do |controller|
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end
      #
      # initialize AreaController Class
      #
      def initialize
        @get_area_from_pincode = AddressModule::V1::GetAreaFromPincodeApi
        @change_area_status = AddressModule::V1::ChangeAreaStatus
        @update_area_api = AddressModule::V1::UpdateAreaApi
      end

      #
      # function to get area details
      #
      # GET /areas
      #
      # Request::
      #   * pincode [string] pincode
      #
      # Response::
      #   * send details of area
      #
      def get_area_from_pincode
        get_area_from_pincode_api = @get_area_from_pincode.new(@deciding_params)
        response = get_area_from_pincode_api.enact({ pincode: get_pincode_from_params })
        send_response(response)
      end

      swagger_controller :areas, 'AddressModule APIs'
      swagger_api :get_area_from_pincode do
        summary 'It returns details of area matching the pincode'
        notes 'get_area_details API returns details of area matching the pincode'
        param :query, :pincode, :string, :required, 'pincode'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :unauthorized, "User not having correct Role to access the Page"
        response :bad_request, "Parameters missing/invalid parameters"
      end

      #
      # function to change state of area
      #
      # PUT /areas/state
      #
      # Request::
      #   * pincode [string] pincode
      #   * event [string] event to trigger
      #
      # Response::
      #   * send details of updated area state
      #
      _LogActivity_
      def change_area_status
        change_area_status_api = @change_area_status.new(@deciding_params)
        response = change_area_status_api.enact(change_area_status_params)
        send_response(response)
      end

      swagger_controller :areas, 'AddressModule APIs'
      swagger_api :change_area_status do
        summary 'It returns details of updated area matching the pincode'
        notes 'get_area_details API returns updated details of area matching the pincode'
        param :body, :change_area_status_request, :change_area_status_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or area not found or it fails to change state'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_area_status_request do
        description 'request schema for /change_state API'
        property :area, :change_area_status_request_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_area_status_request_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
        property :pincode, :string, :required, 'pincode'
      end

      #
      # function to update area name
      #
      # PUT /areas/:id
      #
      # Request::
      #   * id [Integer] area id
      #
      # Response::
      #   * send details of updated area state
      #
      _LogActivity_
      def update_area
        update_area_api = @update_area_api.new(@deciding_params)
        response = update_area_api.enact(update_area_params)
        send_response(response)
      end

      swagger_controller :areas, 'AddressModule APIs'
      swagger_api :update_area do
        summary 'It returns details of updated area matching the area id'
        notes 'update_area API returns updated details of area matching the area id'
        param :path, :id, :integer, :required, 'area to update'
        param :body, :update_area_request, :update_area_request, :required, 'update_area request'
        response :bad_request, 'wrong parameters or area not found or it fails to update area'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :update_area_request do
        description 'request schema for /update_area API'
        property :area, :update_area_request_hash, :required, 'hash of area to to do the change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :update_area_request_hash do
        description 'hash having the parameters to change the area params'
        property :name, :string, :required, 'area name to be changed'
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      def get_pincode_from_params
        return params.require(:pincode)
      end

      def change_area_status_params
        params.require(:area).permit(:event, :pincode)
      end

      def update_area_params
        params.require(:area).permit(:name).merge({ id: get_id_from_params})
      end

      def get_id_from_params
        return params.require(:id)
      end
    end
  end
end

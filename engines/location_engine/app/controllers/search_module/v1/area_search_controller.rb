#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for address module
  #
  module V1
    #
    # AreaSearch controller service class to route panel API calls to the SearchApi
    #
    class AreaSearchController < BaseModule::V1::ApplicationController
      #
      # initialize SearchAreaByQuery Class
      #
      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
        @search_area_by_query = SearchModule::V1::SearchAreaByQueryApi
      end

      #
      # function to get all pincodes which match query
      #
      # GET /get_area_by_query
      #
      # Request::
      #   * query [string] query string
      #   * top_n [string] number of results to return
      #   * status [string] status of the areas to be searched
      #
      # Response::
      #   * send list of areas
      #
      _LogActivity_
      def get_area_by_query
        deciding_params = @application_helper.get_deciding_params(params)
        search_area_by_query_api = @search_area_by_query.new deciding_params
        response = search_area_by_query_api.enact(params)
        send_response(response)
      end

      swagger_controller :area_search, 'SearchModule APIs'
      swagger_api :get_area_by_query do
        summary 'It returns list of area mactching the query'
        notes 'get_area_by_query API returns list of area mactching the query'
        param :query, :query, :string, :required, 'query string'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :top_n, :string, :optional, 'number of results'
        param :query, :status, :string, :optional, 'state of areas required'
        response :bad_request, 'wrong parameters, query length < 3 or request in blank'
        response :internal_server_error, 'run time error if elastic search server fails'
      end
    end
  end
end

#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for address module
  #
  module V1
    #
    # This class contain basic query object that helps to create
    # complex queries
    #
    module QueryHelper
      #
      # Create fuzzy query which generates all possible matching terms that are within
      # the maximum edit distance specified in fuzziness
      #
      # @param key [String] field name to search
      # @param value [String] query to search
      # @param fuzziness = 2 [Integer] maximum edit distance
      # @param boost = 1.0 [Integer] boost query to promote the results
      #
      # @return [hash] query hash
      #
      def self.get_fuzzy_query(key, value, fuzziness = 2, boost = 1.0)
        fuzzy = {}
        fuzzy[key] = {
          value: value,
          boost: boost,
          fuzziness: fuzziness
        }
        { fuzzy: fuzzy }
      end

      #
      # Create regex query which allows to use regular expression term queries
      #
      # @param key [String] field name to search
      # @param value [String] query to search
      # @param boost = 1.0 [Integer] boost query to promote the results
      #
      # @return [hash] query hash
      #
      def self.get_regexp_query(key, value, boost = 1.0)
        regexp = {}
        regexp[key] = {
          value: value,
          boost: boost
        }

        { regexp: regexp }
      end

      #
      # Empty boolean query template
      #
      # @param boost = 1 [Integer] boost query to promote the results
      # @param minimum_should_match = 1 [Integer] minimum number of clauses must be true
      #                                           to include document in result set
      #
      # @return [hash] query hash
      #
      def self.get_boolean_query(boost = 1, minimum_should_match = 1)
        {
          bool:
          {
            boost: boost,
            minimum_should_match: minimum_should_match
          }
        }
      end

      #
      # Add clause(should, must etc) to the boolean query
      #
      # @param clause [String] boolean clause
      # @param value [Hash] any query hash
      # @param boolean_query [Hash] boolean_query
      #
      # @return [hash] query hash
      #
      def self.add_clause_in_boolean_query(clause, value, boolean_query)
        boolean_query = get_boolean_query if boolean_query.nil? || boolean_query[:bool].nil?

        boolean_query[:bool][clause] = [] if boolean_query[:bool][clause].nil?

        boolean_query[:bool][clause] << value

        boolean_query
      end

      #
      # Create match query, which search for the exact match of query
      # 
      # @param key [String] field name to search
      # @param value [String] query to search
      #
      # @return [hash] query hash
      #
      def self.get_match_query(key, value)
        query = {}
        query[key] = value
        { match: query }
      end
    end
  end
end

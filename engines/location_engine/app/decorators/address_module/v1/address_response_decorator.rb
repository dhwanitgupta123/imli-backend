#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Helper class to create JsonResponse
    #
    class AddressResponseDecorator < BaseModule::V1::BaseResponseDecorator
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      ADDRESS_HELPER = AddressModule::V1::AddressHelper
      #
      # Send ok response if area creation succeed
      #
      # @param area_reader [object] containg all fields corresponding to area model
      #
      # @return [hash] Response consisting of all the field in area_object
      #
      def self.create_ok_response(area_reader)
        {
          payload: {
            pincode: {
              id: area_reader.get_area_id,
              pincode: area_reader.get_pincode,
              area: area_reader.get_area,
              city: area_reader.get_city,
              state: area_reader.get_state,
              country: area_reader.get_country,
              status: area_reader.get_status
            }
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      #
      # Create response for invalid arguments
      #
      # @param message [String] consist of message why validaton fails
      #
      # @return [hash] response of invalid arguments
      #
      def self.create_response_invalid_arguments_error(message)
        {
          error: { message: message },
          response: RESPONSE_CODES_UTIL::BAD_REQUEST
        }
      end

      #
      # Send area doesn't exist response
      #
      def self.create_non_existing_area_error
        {
          error: { message: CONTENT_UTIL::NON_EXISTING_PINCODE },
          response: RESPONSE_CODES_UTIL::NOT_FOUND
        }
      end

      #
      # Update area response
      #
      # @param area [object] containg all fields corresponding to area model
      #
      # @return [hash] Response with updated field
      #
      def self.update_pincode_status_response(area)
        {
          payload: {
            pincode: {
              pincode: area.pincode,
              is_active: area.status
            }
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      #
      # Create response for area search, it consists of list of pincode_model
      # each element contain 'pincode', 'area name', 'city name' and 'state name'
      # these fields are retrieved from the pincode-index which is save in elastic search server
      #
      # @param [PincodeModelReader] it is wrapper object which contain list of pincode model
      #
      # @return [Hash] Contain list of search result
      #
      def self.create_search_area_by_query_response(pincode_model_reader)
        {
          payload: {
            pincodes: pincode_model_reader.get_attribute_array
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      #
      # Create response for getting all addresses successfully
      # 
      # @param addresses [hash] containing all addresses of user corresponding to address model
      #
      # @return [hash] Response with requested fields
      #
      def self.get_all_addresses_of_user_response(addresses)
        addresses = ADDRESS_HELPER.get_addresses_hash(addresses)
        {
          payload: {
            user: {
            addresses: addresses
            }
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      # 
      # create hash of address object attributes
      # 
      def self.create_address_hash(address)
        return {} if address.blank?
        response = {
          id: address.id,
          recipient_name: address.recipient_name || "",
          nickname: address.nickname || "",
          address_line1: address.address_line1 || "",
          address_line2: address.address_line2 || "",
          landmark: address.landmark || "",
          area: address.area.name || "",
          city: address.area.city.name || "",
          state: address.area.city.state.name || "",
          country: address.area.city.state.country.name || "",
          pincode: address.area.pincode || ""
        }
        return response
      end

      #
      # Create response for address added successfully
      # 
      # @param address [object] containing all fields corresponding to address model
      #
      # @return [hash] Response with updated field
      #
      def self.add_address_ok_response(address)
        address = create_address_hash(address)
        {
          payload: {
            user: {
            addresses: [ address ]
            }
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      # 
      # create hash of area object attributes
      # 
      def self.create_area_details_response(area)
        response = {
          payload: {

              id: area.id,
              area: area.name,
              city: area.city.name,
              state: area.city.state.name,
              country: area.city.state.country.name,
              pincode: area.pincode,
              status: area.status
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response       
      end
    end
  end
end

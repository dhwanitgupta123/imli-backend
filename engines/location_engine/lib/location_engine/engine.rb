#
# Deals with all location related functionalities
#
module LocationEngine
  #
  # This adds the paths in $LOAD_PATH variable so that module inside these directory gets loaded in the environment
  # LocationEngine belongs to imli-backend-app uses many common functionalities of the application hence
  # added utils, helper, model and decorators of the app
  #
  class Engine < ::Rails::Engine
    config.autoload_paths += %W(
      #{config.root}/app/api
      #{config.root}/app/readers
      #{config.root}/app/data_access_object
      #{config.root}/app/services
      #{config.root}/../../app/services
      #{config.root}/../../app/utils
      #{config.root}/../../app/helpers
      #{config.root}/../../app/models
      #{config.root}/../../app/controllers
      #{config.root}/../../app/decorators
      #{config.root}/../../lib
    )

    # RSpec & Factory Girl configuration
    config.generators do |g|
      g.test_framework :rspec, fixtures: false
      g.fixture_replacement :factory_girl, dir: 'spec/factories'
      g.assets false
      g.helper false
    end
  end
end

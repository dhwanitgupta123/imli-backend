source 'https://rubygems.org'

ruby '2.2.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.3'

# Use postgresql as the database for Active Record
gem 'pg'
#Using mongoid to use MongoDB 
gem 'mongoid', '~> 5.0.0'
# Use SCSS for stylesheets
# 
gem 'sass-rails', '~> 5.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# using thin client instead of WEBrick
gem 'thin'

# using yardoc for documentation
gem 'yard'

# a gem to manage gems :D
gem 'gemrat'

# a gem to manage caching
gem 'redis-rails'

# whenever gem to implement cron jobs
gem 'whenever'

# razorpay gem for processing payment
gem 'razorpay'

# split gem for AB testing
# This gem is modified after forking the repo to 'imlitech'
# git branch: https://github.com/imlitech/split/tree/custom_method
gem 'split', require: 'split/dashboard',
git: 'git@github.com:imlitech/split.git', branch: 'custom_method'

# gem 'workflow', '1.2.0'
gem 'workflow', :git => 'git@github.com:imlitech/workflow.git',
:branch =>'feature_state_to_integer'

# gem to setup active job queue
gem 'sidekiq', '3.2.5'

# gem to get dashboard to display queued job
gem 'sinatra', '>= 1.3.0'

# gem to use GupShup SMS service
gem 'nileshtrivedi-gupshup'

# gem to store logs in sentry
gem 'sentry-raven'

# gem to enable CORS
gem 'rack-cors', '0.4.0'

# gem to send notification through parse
gem 'parse-ruby-client'

# gem for accessing consul
gem 'diplomat', '0.14.0'

# liquid templating engine
gem 'liquid'

# Snedgrid mail sending 
gem 'sendgrid-ruby'

# gem which utilizes wkhtmltopdf-binary for generating pdf
# Reference: https://github.com/mileszs/wicked_pdf
gem 'wicked_pdf'

# way to install all of the binaries (Linux, OSX, Windows)
# Reference: https://rubygems.org/gems/wkhtmltopdf-binary/
# Version usage due to this version supporting S# uploaded images insertion in PDF
gem 'wkhtmltopdf-binary-edge', '~> 0.12.2.1'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # gem for Factory Girl, which is used to populate model data easily while testing with rspec
  gem 'factory_girl_rails'

  #ge to do rspec testing
  gem 'rspec-rails', '3.3.3'

  # Guard gem to test file which is currently being modified in runtime
  gem 'guard-rspec', '4.6.4'
  
  # gem to create fake dummy data
  gem 'faker'
end

group :test do
  # for test cases of sidekiq
  gem 'rspec-sidekiq'
end

# Embed the V8 JavaScript interpreter into Ruby
gem 'therubyracer'

# gem to use aws API's
gem 'aws-sdk', '~> 1.6'

# gem to add functionalities -> attachment type in ActiveRecord:Base, image manipulation and image upload
gem 'paperclip'

gem 'location_engine', path: 'engines/location_engine'

gem 'supply_chain_engine', path: 'engines/supply_chain_engine'

gem 'order_engine', path: "engines/order_engine"

gem 'pricing_engine', path: 'engines/pricing_engine'

# gem to use elastic search API's
gem 'elasticsearch-persistence'

# gem to generate swagger docs
gem 'swagger-docs', :git => 'git@github.com:imlitech/swagger-docs.git', :branch => 'feature/change_path'
